// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APIEndpoint: {
    officeUrl:  'http://10.180.8.77:55501',
    adminUrl: 'http://10.180.8.77:55502',
    publicUrl: 'http://10.180.8.77:55500'
  }
};
