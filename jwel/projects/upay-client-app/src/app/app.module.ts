import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// core module
import { CoreModule} from './core/core.module';

// routing
import { appRoutes } from './app-routing';

// app init
import { AppComponent } from './app.component';

// services
import { LanguageService } from './dal/language.service';
import { SiteDataService } from './dal/site/site-data.service';
import { UserDataService } from './dal/user/user-data.service';
// import { AuthService } from './services/auth.service';
import {ProviderDataService} from './dal/provider/provider-data.service';
import {SubscriberDataService} from './dal/subscriber/subscriber-data.service';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {NoAuthGuardService} from './core/guards/no-auth-guard.service';
import {NewsDataService} from './dal/news/news-data.service';
import {CustomerApi, OfficeApi, setApiUrl} from 'upay-lib';
import {ErrorHandler} from './api/error-handler';
import {environment} from '../environments/environment';
import {TransactionsPageComponent} from '../../../upay-operator-app/src/app/pages/transactions-page/transactions-page.component';



setApiUrl(environment.APIEndpoint.publicUrl);

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    // TransactionsPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    LanguageService,
    SiteDataService,
    UserDataService,
    ProviderDataService,
    SubscriberDataService,
    AuthGuardService,
    NoAuthGuardService,
    NewsDataService,
    // AuthService
    // AuthService
    CustomerApi,
    OfficeApi, // todo mtacel
    ErrorHandler
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
