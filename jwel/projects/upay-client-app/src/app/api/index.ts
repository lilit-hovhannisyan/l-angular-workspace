/* tslint:disable */
import {Observable} from 'rxjs/index';
import {BaseApi} from './base-api';

export class Page {
  index: number
  size: number
}

export class Pair {
  key: string;
  val: boolean;
}

export class Order {
  fields: Array<Pair>
}

export class Paged<T> {
  items: Array<T>;
  page: Page;
  total: number;
}


export enum CustomerStatus {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  BLOCKED = "BLOCKED",
}

export enum OperatorStatus {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  BLOCKED = "BLOCKED",
}

export enum Scope {
  CUSTOMER = "CUSTOMER",
  BRANCH = "BRANCH",
  SUPPORT = "SUPPORT",
  ADMIN = "ADMIN",
  OPERATOR = "OPERATOR",
  TEMPORARY_PASSWORD = "TEMPORARY_PASSWORD",
}

export class AmountRange {
  minAmount: number;
  maxAmount: number;

  constructor() {
  }

  static build(): AmountRangeBuilder {
    return new AmountRangeBuilder();
  }
}

class AmountRangeBuilder {
  private instance: AmountRange;

  constructor() {
    this.instance = new AmountRange();
  }

  minAmount(value: number): AmountRangeBuilder {
    this.instance.minAmount = value;
    return this;
  }

  maxAmount(value: number): AmountRangeBuilder {
    this.instance.maxAmount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Contract {
  identity: string;
  providerId: string;

  constructor() {
  }

  static build(): ContractBuilder {
    return new ContractBuilder();
  }
}

class ContractBuilder {
  private instance: Contract;

  constructor() {
    this.instance = new Contract();
  }

  identity(value: string): ContractBuilder {
    this.instance.identity = value;
    return this;
  }

  providerId(value: string): ContractBuilder {
    this.instance.providerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Principal {
  id: string;
  identity: string;
  scopes: Array<Scope>;

  constructor() {
  }

  static build(): PrincipalBuilder {
    return new PrincipalBuilder();
  }
}

class PrincipalBuilder {
  private instance: Principal;

  constructor() {
    this.instance = new Principal();
  }

  id(value: string): PrincipalBuilder {
    this.instance.id = value;
    return this;
  }

  identity(value: string): PrincipalBuilder {
    this.instance.identity = value;
    return this;
  }

  scopes(value: Array<Scope>): PrincipalBuilder {
    this.instance.scopes = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorInfo {
  id: string;
  forename: string;
  surname: string;
  userName: string;
  email: string;
  phone: string;
  cashboxId: string;
  expiration: string;
  enabled: boolean;
  scope: Array<Scope>;

  constructor() {
  }

  static build(): OperatorInfoBuilder {
    return new OperatorInfoBuilder();
  }
}

class OperatorInfoBuilder {
  private instance: OperatorInfo;

  constructor() {
    this.instance = new OperatorInfo();
  }

  id(value: string): OperatorInfoBuilder {
    this.instance.id = value;
    return this;
  }

  forename(value: string): OperatorInfoBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): OperatorInfoBuilder {
    this.instance.surname = value;
    return this;
  }

  userName(value: string): OperatorInfoBuilder {
    this.instance.userName = value;
    return this;
  }

  email(value: string): OperatorInfoBuilder {
    this.instance.email = value;
    return this;
  }

  phone(value: string): OperatorInfoBuilder {
    this.instance.phone = value;
    return this;
  }

  cashboxId(value: string): OperatorInfoBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  expiration(value: string): OperatorInfoBuilder {
    this.instance.expiration = value;
    return this;
  }

  enabled(value: boolean): OperatorInfoBuilder {
    this.instance.enabled = value;
    return this;
  }

  scope(value: Array<Scope>): OperatorInfoBuilder {
    this.instance.scope = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export enum CommissionRuleType {
  FIXED = "FIXED",
  PERCENT = "PERCENT",
}

export enum CommissionAction {
  SAVE = "SAVE",
  DELETE = "DELETE",
}

export enum EncashmentType {
  INTERNAL = "INTERNAL",
  EXTERNAL = "EXTERNAL",
  UCOM = "UCOM",
}

export enum CashboxAction {
  PAYMENT_IN = "PAYMENT_IN",
  ENCASHMENT_IN = "ENCASHMENT_IN",
  ENCASHMENT_OUT = "ENCASHMENT_OUT",
}

export class CommissionRange {
  min: number;
  max: number;
  value: number;
  type: CommissionRuleType;

  constructor() {
  }

  static build(): CommissionRangeBuilder {
    return new CommissionRangeBuilder();
  }
}

class CommissionRangeBuilder {
  private instance: CommissionRange;

  constructor() {
    this.instance = new CommissionRange();
  }

  min(value: number): CommissionRangeBuilder {
    this.instance.min = value;
    return this;
  }

  max(value: number): CommissionRangeBuilder {
    this.instance.max = value;
    return this;
  }

  value(value: number): CommissionRangeBuilder {
    this.instance.value = value;
    return this;
  }

  type(value: CommissionRuleType): CommissionRangeBuilder {
    this.instance.type = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashboxBalance {
  cashboxId: string;
  name: string;
  amountIn: number;
  amountOut: number;
  openingBalance: number;
  commissions: number;
  currentBalance: number;

  constructor() {
  }

  static build(): CashboxBalanceBuilder {
    return new CashboxBalanceBuilder();
  }
}

class CashboxBalanceBuilder {
  private instance: CashboxBalance;

  constructor() {
    this.instance = new CashboxBalance();
  }

  cashboxId(value: string): CashboxBalanceBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  name(value: string): CashboxBalanceBuilder {
    this.instance.name = value;
    return this;
  }

  amountIn(value: number): CashboxBalanceBuilder {
    this.instance.amountIn = value;
    return this;
  }

  amountOut(value: number): CashboxBalanceBuilder {
    this.instance.amountOut = value;
    return this;
  }

  openingBalance(value: number): CashboxBalanceBuilder {
    this.instance.openingBalance = value;
    return this;
  }

  commissions(value: number): CashboxBalanceBuilder {
    this.instance.commissions = value;
    return this;
  }

  currentBalance(value: number): CashboxBalanceBuilder {
    this.instance.currentBalance = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ContractField {
  key: string;
  label: string;

  constructor() {
  }

  static build(): ContractFieldBuilder {
    return new ContractFieldBuilder();
  }
}

class ContractFieldBuilder {
  private instance: ContractField;

  constructor() {
    this.instance = new ContractField();
  }

  key(value: string): ContractFieldBuilder {
    this.instance.key = value;
    return this;
  }

  label(value: string): ContractFieldBuilder {
    this.instance.label = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ContractDesc {
  idType: string;
  label: string;
  fields: Array<ContractField>;

  constructor() {
  }

  static build(): ContractDescBuilder {
    return new ContractDescBuilder();
  }
}

class ContractDescBuilder {
  private instance: ContractDesc;

  constructor() {
    this.instance = new ContractDesc();
  }

  idType(value: string): ContractDescBuilder {
    this.instance.idType = value;
    return this;
  }

  label(value: string): ContractDescBuilder {
    this.instance.label = value;
    return this;
  }

  fields(value: Array<ContractField>): ContractDescBuilder {
    this.instance.fields = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Debt {
  providerId: string;
  customerId: string;
  debtDate: Date;
  debtAmount: number;

  constructor() {
  }

  static build(): DebtBuilder {
    return new DebtBuilder();
  }
}

class DebtBuilder {
  private instance: Debt;

  constructor() {
    this.instance = new Debt();
  }

  providerId(value: string): DebtBuilder {
    this.instance.providerId = value;
    return this;
  }

  customerId(value: string): DebtBuilder {
    this.instance.customerId = value;
    return this;
  }

  debtDate(value: Date): DebtBuilder {
    this.instance.debtDate = value;
    return this;
  }

  debtAmount(value: number): DebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class UpayFixedBalance {
  total: number;
  internet: number;
  phone: number;
  tv: number;

  constructor() {
  }

  static build(): UpayFixedBalanceBuilder {
    return new UpayFixedBalanceBuilder();
  }
}

class UpayFixedBalanceBuilder {
  private instance: UpayFixedBalance;

  constructor() {
    this.instance = new UpayFixedBalance();
  }

  total(value: number): UpayFixedBalanceBuilder {
    this.instance.total = value;
    return this;
  }

  internet(value: number): UpayFixedBalanceBuilder {
    this.instance.internet = value;
    return this;
  }

  phone(value: number): UpayFixedBalanceBuilder {
    this.instance.phone = value;
    return this;
  }

  tv(value: number): UpayFixedBalanceBuilder {
    this.instance.tv = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class UpayFixedDebt {
  client: string;
  balance: UpayFixedBalance;

  constructor() {
  }

  static build(): UpayFixedDebtBuilder {
    return new UpayFixedDebtBuilder();
  }
}

class UpayFixedDebtBuilder {
  private instance: UpayFixedDebt;

  constructor() {
    this.instance = new UpayFixedDebt();
  }

  client(value: string): UpayFixedDebtBuilder {
    this.instance.client = value;
    return this;
  }

  balance(value: UpayFixedBalance): UpayFixedDebtBuilder {
    this.instance.balance = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtCommunalElectricity {
  serviceId: number;
  customerId: string;
  customerName: string;
  phone: string;
  currentPaymentDate: string;
  currentPaymentDebt: number;
  currentPaymentTraffic: string;
  currentPaymentNightTraffic: string;
  currentPaymentPaid: number;
  lastPaymentDate: string;
  lastPaymentDebt: number;
  lastPaymentPaid: number;

  constructor() {
  }

  static build(): DebtCommunalElectricityBuilder {
    return new DebtCommunalElectricityBuilder();
  }
}

class DebtCommunalElectricityBuilder {
  private instance: DebtCommunalElectricity;

  constructor() {
    this.instance = new DebtCommunalElectricity();
  }

  serviceId(value: number): DebtCommunalElectricityBuilder {
    this.instance.serviceId = value;
    return this;
  }

  customerId(value: string): DebtCommunalElectricityBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtCommunalElectricityBuilder {
    this.instance.customerName = value;
    return this;
  }

  phone(value: string): DebtCommunalElectricityBuilder {
    this.instance.phone = value;
    return this;
  }

  currentPaymentDate(value: string): DebtCommunalElectricityBuilder {
    this.instance.currentPaymentDate = value;
    return this;
  }

  currentPaymentDebt(value: number): DebtCommunalElectricityBuilder {
    this.instance.currentPaymentDebt = value;
    return this;
  }

  currentPaymentTraffic(value: string): DebtCommunalElectricityBuilder {
    this.instance.currentPaymentTraffic = value;
    return this;
  }

  currentPaymentNightTraffic(value: string): DebtCommunalElectricityBuilder {
    this.instance.currentPaymentNightTraffic = value;
    return this;
  }

  currentPaymentPaid(value: number): DebtCommunalElectricityBuilder {
    this.instance.currentPaymentPaid = value;
    return this;
  }

  lastPaymentDate(value: string): DebtCommunalElectricityBuilder {
    this.instance.lastPaymentDate = value;
    return this;
  }

  lastPaymentDebt(value: number): DebtCommunalElectricityBuilder {
    this.instance.lastPaymentDebt = value;
    return this;
  }

  lastPaymentPaid(value: number): DebtCommunalElectricityBuilder {
    this.instance.lastPaymentPaid = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GasBranch {
  branchId: number;
  branchNameAm: string;
  branchNameRu: string;
  branchNameEn: string;

  constructor() {
  }

  static build(): GasBranchBuilder {
    return new GasBranchBuilder();
  }
}

class GasBranchBuilder {
  private instance: GasBranch;

  constructor() {
    this.instance = new GasBranch();
  }

  branchId(value: number): GasBranchBuilder {
    this.instance.branchId = value;
    return this;
  }

  branchNameAm(value: string): GasBranchBuilder {
    this.instance.branchNameAm = value;
    return this;
  }

  branchNameRu(value: string): GasBranchBuilder {
    this.instance.branchNameRu = value;
    return this;
  }

  branchNameEn(value: string): GasBranchBuilder {
    this.instance.branchNameEn = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GasDebt {
  customerId: string;
  phone: string;
  customerName: string;
  debtDate: string;
  costUnit: string;
  counterUnit: string;
  costAmount: string;
  subCostAmount: string;
  paidByIdram: string;
  subPaidByIdram: string;
  prevDate: string;
  prevDebtAmount: string;
  prevPaidAmount: string;
  subPrevDebtAmount: string;
  subPrevPaidAmount: string;
  debt: string;

  constructor() {
  }

  static build(): GasDebtBuilder {
    return new GasDebtBuilder();
  }
}

class GasDebtBuilder {
  private instance: GasDebt;

  constructor() {
    this.instance = new GasDebt();
  }

  customerId(value: string): GasDebtBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): GasDebtBuilder {
    this.instance.phone = value;
    return this;
  }

  customerName(value: string): GasDebtBuilder {
    this.instance.customerName = value;
    return this;
  }

  debtDate(value: string): GasDebtBuilder {
    this.instance.debtDate = value;
    return this;
  }

  costUnit(value: string): GasDebtBuilder {
    this.instance.costUnit = value;
    return this;
  }

  counterUnit(value: string): GasDebtBuilder {
    this.instance.counterUnit = value;
    return this;
  }

  costAmount(value: string): GasDebtBuilder {
    this.instance.costAmount = value;
    return this;
  }

  subCostAmount(value: string): GasDebtBuilder {
    this.instance.subCostAmount = value;
    return this;
  }

  paidByIdram(value: string): GasDebtBuilder {
    this.instance.paidByIdram = value;
    return this;
  }

  subPaidByIdram(value: string): GasDebtBuilder {
    this.instance.subPaidByIdram = value;
    return this;
  }

  prevDate(value: string): GasDebtBuilder {
    this.instance.prevDate = value;
    return this;
  }

  prevDebtAmount(value: string): GasDebtBuilder {
    this.instance.prevDebtAmount = value;
    return this;
  }

  prevPaidAmount(value: string): GasDebtBuilder {
    this.instance.prevPaidAmount = value;
    return this;
  }

  subPrevDebtAmount(value: string): GasDebtBuilder {
    this.instance.subPrevDebtAmount = value;
    return this;
  }

  subPrevPaidAmount(value: string): GasDebtBuilder {
    this.instance.subPrevPaidAmount = value;
    return this;
  }

  debt(value: string): GasDebtBuilder {
    this.instance.debt = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtRostelecom {
  customerId: string;
  customerName: string;
  debt: number;
  debtTv: number;
  debtNoTv: number;
  debtDate: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtRostelecomBuilder {
    return new DebtRostelecomBuilder();
  }
}

class DebtRostelecomBuilder {
  private instance: DebtRostelecom;

  constructor() {
    this.instance = new DebtRostelecom();
  }

  customerId(value: string): DebtRostelecomBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtRostelecomBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtRostelecomBuilder {
    this.instance.debt = value;
    return this;
  }

  debtTv(value: number): DebtRostelecomBuilder {
    this.instance.debtTv = value;
    return this;
  }

  debtNoTv(value: number): DebtRostelecomBuilder {
    this.instance.debtNoTv = value;
    return this;
  }

  debtDate(value: string): DebtRostelecomBuilder {
    this.instance.debtDate = value;
    return this;
  }

  serviceId(value: string): DebtRostelecomBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtInteractive {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtInteractiveBuilder {
    return new DebtInteractiveBuilder();
  }
}

class DebtInteractiveBuilder {
  private instance: DebtInteractive;

  constructor() {
    this.instance = new DebtInteractive();
  }

  customerId(value: string): DebtInteractiveBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtInteractiveBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtInteractiveBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtInteractiveBuilder {
    this.instance.debtDate = value;
    return this;
  }

  serviceId(value: string): DebtInteractiveBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtArpinet {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;

  constructor() {
  }

  static build(): DebtArpinetBuilder {
    return new DebtArpinetBuilder();
  }
}

class DebtArpinetBuilder {
  private instance: DebtArpinet;

  constructor() {
    this.instance = new DebtArpinet();
  }

  customerId(value: string): DebtArpinetBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtArpinetBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtArpinetBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtArpinetBuilder {
    this.instance.debtDate = value;
    return this;
  }

  debtAmountRest(value: number): DebtArpinetBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }

  serviceId(value: string): DebtArpinetBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtWater {
  serviceId: number;
  customerId: string;
  customerName: string;
  phone: string;
  debtDate: string;
  debt: number;
  counterUnit: number;
  costUnit: number;
  paidByIdram: number;
  prevPaidDate: string;
  prevDebtAmount: number;
  prevPaidAmount: number;

  constructor() {
  }

  static build(): DebtWaterBuilder {
    return new DebtWaterBuilder();
  }
}

class DebtWaterBuilder {
  private instance: DebtWater;

  constructor() {
    this.instance = new DebtWater();
  }

  serviceId(value: number): DebtWaterBuilder {
    this.instance.serviceId = value;
    return this;
  }

  customerId(value: string): DebtWaterBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtWaterBuilder {
    this.instance.customerName = value;
    return this;
  }

  phone(value: string): DebtWaterBuilder {
    this.instance.phone = value;
    return this;
  }

  debtDate(value: string): DebtWaterBuilder {
    this.instance.debtDate = value;
    return this;
  }

  debt(value: number): DebtWaterBuilder {
    this.instance.debt = value;
    return this;
  }

  counterUnit(value: number): DebtWaterBuilder {
    this.instance.counterUnit = value;
    return this;
  }

  costUnit(value: number): DebtWaterBuilder {
    this.instance.costUnit = value;
    return this;
  }

  paidByIdram(value: number): DebtWaterBuilder {
    this.instance.paidByIdram = value;
    return this;
  }

  prevPaidDate(value: string): DebtWaterBuilder {
    this.instance.prevPaidDate = value;
    return this;
  }

  prevDebtAmount(value: number): DebtWaterBuilder {
    this.instance.prevDebtAmount = value;
    return this;
  }

  prevPaidAmount(value: number): DebtWaterBuilder {
    this.instance.prevPaidAmount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DeptLoan {
  agrimentCode: string;
  customerName: string;
  penaltyAmount: number;
  capitalAmount: number;
  nextPayment: number;
  nextPaymentStr: string;
  overdueFee: string;
  overdueInterest: string;
  overduePrincipal: string;
  nearFee: string;
  nearInterest: string;
  nearPrincipal: string;
  nextPaymentDate: string;
  debt: number;
  customerId: string;
  debtDate: string;
  passport: string;
  repayType: string;
  commission: string;
  nextRollover: string;
  dayLeft: number;
  serviceId: string;

  constructor() {
  }

  static build(): DeptLoanBuilder {
    return new DeptLoanBuilder();
  }
}

class DeptLoanBuilder {
  private instance: DeptLoan;

  constructor() {
    this.instance = new DeptLoan();
  }

  agrimentCode(value: string): DeptLoanBuilder {
    this.instance.agrimentCode = value;
    return this;
  }

  customerName(value: string): DeptLoanBuilder {
    this.instance.customerName = value;
    return this;
  }

  penaltyAmount(value: number): DeptLoanBuilder {
    this.instance.penaltyAmount = value;
    return this;
  }

  capitalAmount(value: number): DeptLoanBuilder {
    this.instance.capitalAmount = value;
    return this;
  }

  nextPayment(value: number): DeptLoanBuilder {
    this.instance.nextPayment = value;
    return this;
  }

  nextPaymentStr(value: string): DeptLoanBuilder {
    this.instance.nextPaymentStr = value;
    return this;
  }

  overdueFee(value: string): DeptLoanBuilder {
    this.instance.overdueFee = value;
    return this;
  }

  overdueInterest(value: string): DeptLoanBuilder {
    this.instance.overdueInterest = value;
    return this;
  }

  overduePrincipal(value: string): DeptLoanBuilder {
    this.instance.overduePrincipal = value;
    return this;
  }

  nearFee(value: string): DeptLoanBuilder {
    this.instance.nearFee = value;
    return this;
  }

  nearInterest(value: string): DeptLoanBuilder {
    this.instance.nearInterest = value;
    return this;
  }

  nearPrincipal(value: string): DeptLoanBuilder {
    this.instance.nearPrincipal = value;
    return this;
  }

  nextPaymentDate(value: string): DeptLoanBuilder {
    this.instance.nextPaymentDate = value;
    return this;
  }

  debt(value: number): DeptLoanBuilder {
    this.instance.debt = value;
    return this;
  }

  customerId(value: string): DeptLoanBuilder {
    this.instance.customerId = value;
    return this;
  }

  debtDate(value: string): DeptLoanBuilder {
    this.instance.debtDate = value;
    return this;
  }

  passport(value: string): DeptLoanBuilder {
    this.instance.passport = value;
    return this;
  }

  repayType(value: string): DeptLoanBuilder {
    this.instance.repayType = value;
    return this;
  }

  commission(value: string): DeptLoanBuilder {
    this.instance.commission = value;
    return this;
  }

  nextRollover(value: string): DeptLoanBuilder {
    this.instance.nextRollover = value;
    return this;
  }

  dayLeft(value: number): DeptLoanBuilder {
    this.instance.dayLeft = value;
    return this;
  }

  serviceId(value: string): DeptLoanBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BetResponse {
  serviceId: number;
  opCode: number;
  opDesc: string;

  constructor() {
  }

  static build(): BetResponseBuilder {
    return new BetResponseBuilder();
  }
}

class BetResponseBuilder {
  private instance: BetResponse;

  constructor() {
    this.instance = new BetResponse();
  }

  serviceId(value: number): BetResponseBuilder {
    this.instance.serviceId = value;
    return this;
  }

  opCode(value: number): BetResponseBuilder {
    this.instance.opCode = value;
    return this;
  }

  opDesc(value: string): BetResponseBuilder {
    this.instance.opDesc = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtSocialOk {
  rate: number;
  currency: string;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtSocialOkBuilder {
    return new DebtSocialOkBuilder();
  }
}

class DebtSocialOkBuilder {
  private instance: DebtSocialOk;

  constructor() {
    this.instance = new DebtSocialOk();
  }

  rate(value: number): DebtSocialOkBuilder {
    this.instance.rate = value;
    return this;
  }

  currency(value: string): DebtSocialOkBuilder {
    this.instance.currency = value;
    return this;
  }

  customerId(value: string): DebtSocialOkBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtSocialOkBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtSocialOkBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtSocialOkBuilder {
    this.instance.debtDate = value;
    return this;
  }

  serviceId(value: string): DebtSocialOkBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtOnlineGames {
  rate: number;
  currency: string;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  opCode: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtOnlineGamesBuilder {
    return new DebtOnlineGamesBuilder();
  }
}

class DebtOnlineGamesBuilder {
  private instance: DebtOnlineGames;

  constructor() {
    this.instance = new DebtOnlineGames();
  }

  rate(value: number): DebtOnlineGamesBuilder {
    this.instance.rate = value;
    return this;
  }

  currency(value: string): DebtOnlineGamesBuilder {
    this.instance.currency = value;
    return this;
  }

  customerId(value: string): DebtOnlineGamesBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtOnlineGamesBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtOnlineGamesBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtOnlineGamesBuilder {
    this.instance.debtDate = value;
    return this;
  }

  opCode(value: string): DebtOnlineGamesBuilder {
    this.instance.opCode = value;
    return this;
  }

  serviceId(value: string): DebtOnlineGamesBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtEtcMaryKay {
  debt: number;
  customerId: string;
  customerName: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtEtcMaryKayBuilder {
    return new DebtEtcMaryKayBuilder();
  }
}

class DebtEtcMaryKayBuilder {
  private instance: DebtEtcMaryKay;

  constructor() {
    this.instance = new DebtEtcMaryKay();
  }

  debt(value: number): DebtEtcMaryKayBuilder {
    this.instance.debt = value;
    return this;
  }

  customerId(value: string): DebtEtcMaryKayBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtEtcMaryKayBuilder {
    this.instance.customerName = value;
    return this;
  }

  serviceId(value: string): DebtEtcMaryKayBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtEtcOriflame {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;

  constructor() {
  }

  static build(): DebtEtcOriflameBuilder {
    return new DebtEtcOriflameBuilder();
  }
}

class DebtEtcOriflameBuilder {
  private instance: DebtEtcOriflame;

  constructor() {
    this.instance = new DebtEtcOriflame();
  }

  customerId(value: string): DebtEtcOriflameBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtEtcOriflameBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtEtcOriflameBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtEtcOriflameBuilder {
    this.instance.debtDate = value;
    return this;
  }

  debtAmountRest(value: number): DebtEtcOriflameBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }

  serviceId(value: string): DebtEtcOriflameBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtEtcNtv {
  rate: number;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;

  constructor() {
  }

  static build(): DebtEtcNtvBuilder {
    return new DebtEtcNtvBuilder();
  }
}

class DebtEtcNtvBuilder {
  private instance: DebtEtcNtv;

  constructor() {
    this.instance = new DebtEtcNtv();
  }

  rate(value: number): DebtEtcNtvBuilder {
    this.instance.rate = value;
    return this;
  }

  customerId(value: string): DebtEtcNtvBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtEtcNtvBuilder {
    this.instance.customerName = value;
    return this;
  }

  debt(value: number): DebtEtcNtvBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtEtcNtvBuilder {
    this.instance.debtDate = value;
    return this;
  }

  serviceId(value: string): DebtEtcNtvBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DebtEtcEkeng {
  commission: string;
  customerId: string;
  customerName: string;
  customerLastName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;

  constructor() {
  }

  static build(): DebtEtcEkengBuilder {
    return new DebtEtcEkengBuilder();
  }
}

class DebtEtcEkengBuilder {
  private instance: DebtEtcEkeng;

  constructor() {
    this.instance = new DebtEtcEkeng();
  }

  commission(value: string): DebtEtcEkengBuilder {
    this.instance.commission = value;
    return this;
  }

  customerId(value: string): DebtEtcEkengBuilder {
    this.instance.customerId = value;
    return this;
  }

  customerName(value: string): DebtEtcEkengBuilder {
    this.instance.customerName = value;
    return this;
  }

  customerLastName(value: string): DebtEtcEkengBuilder {
    this.instance.customerLastName = value;
    return this;
  }

  debt(value: number): DebtEtcEkengBuilder {
    this.instance.debt = value;
    return this;
  }

  debtDate(value: string): DebtEtcEkengBuilder {
    this.instance.debtDate = value;
    return this;
  }

  debtAmountRest(value: number): DebtEtcEkengBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }

  serviceId(value: string): DebtEtcEkengBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class PoliceDebt {
  serviceId: number;
  customerName: string;
  pinCode: string;
  actNum: string;
  vehicleNumber: string;
  amount: number;
  debtAmount: number;
  debtAmountRest: number;

  constructor() {
  }

  static build(): PoliceDebtBuilder {
    return new PoliceDebtBuilder();
  }
}

class PoliceDebtBuilder {
  private instance: PoliceDebt;

  constructor() {
    this.instance = new PoliceDebt();
  }

  serviceId(value: number): PoliceDebtBuilder {
    this.instance.serviceId = value;
    return this;
  }

  customerName(value: string): PoliceDebtBuilder {
    this.instance.customerName = value;
    return this;
  }

  pinCode(value: string): PoliceDebtBuilder {
    this.instance.pinCode = value;
    return this;
  }

  actNum(value: string): PoliceDebtBuilder {
    this.instance.actNum = value;
    return this;
  }

  vehicleNumber(value: string): PoliceDebtBuilder {
    this.instance.vehicleNumber = value;
    return this;
  }

  amount(value: number): PoliceDebtBuilder {
    this.instance.amount = value;
    return this;
  }

  debtAmount(value: number): PoliceDebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }

  debtAmountRest(value: number): PoliceDebtBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ParkingFineDebt {
  serviceId: number;
  customerName: string;
  actNum: string;
  number: string;
  amount: number;

  constructor() {
  }

  static build(): ParkingFineDebtBuilder {
    return new ParkingFineDebtBuilder();
  }
}

class ParkingFineDebtBuilder {
  private instance: ParkingFineDebt;

  constructor() {
    this.instance = new ParkingFineDebt();
  }

  serviceId(value: number): ParkingFineDebtBuilder {
    this.instance.serviceId = value;
    return this;
  }

  customerName(value: string): ParkingFineDebtBuilder {
    this.instance.customerName = value;
    return this;
  }

  actNum(value: string): ParkingFineDebtBuilder {
    this.instance.actNum = value;
    return this;
  }

  number(value: string): ParkingFineDebtBuilder {
    this.instance.number = value;
    return this;
  }

  amount(value: number): ParkingFineDebtBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class LoanInfo {
  agreementCode: string;
  customerName: string;
  penaltyAmount: number;
  capitalAmount: number;
  nextPayment: number;
  overdueFee: string;
  overdueInterest: string;
  overduePrincipal: string;
  nearFee: string;
  nearInterest: string;
  nearPrincipal: string;
  nextPaymentDate: string;
  debtAmount: number;
  customerId: string;
  deptDate: string;
  passport: string;
  repayType: boolean;
  commission: number;
  nextRollover: string;
  amountRange: AmountRange;
  updated: boolean;
  opCode: number;
  opDescription: string;
  currency: string;
  serviceId: number;

  constructor() {
  }

  static build(): LoanInfoBuilder {
    return new LoanInfoBuilder();
  }
}

class LoanInfoBuilder {
  private instance: LoanInfo;

  constructor() {
    this.instance = new LoanInfo();
  }

  agreementCode(value: string): LoanInfoBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  customerName(value: string): LoanInfoBuilder {
    this.instance.customerName = value;
    return this;
  }

  penaltyAmount(value: number): LoanInfoBuilder {
    this.instance.penaltyAmount = value;
    return this;
  }

  capitalAmount(value: number): LoanInfoBuilder {
    this.instance.capitalAmount = value;
    return this;
  }

  nextPayment(value: number): LoanInfoBuilder {
    this.instance.nextPayment = value;
    return this;
  }

  overdueFee(value: string): LoanInfoBuilder {
    this.instance.overdueFee = value;
    return this;
  }

  overdueInterest(value: string): LoanInfoBuilder {
    this.instance.overdueInterest = value;
    return this;
  }

  overduePrincipal(value: string): LoanInfoBuilder {
    this.instance.overduePrincipal = value;
    return this;
  }

  nearFee(value: string): LoanInfoBuilder {
    this.instance.nearFee = value;
    return this;
  }

  nearInterest(value: string): LoanInfoBuilder {
    this.instance.nearInterest = value;
    return this;
  }

  nearPrincipal(value: string): LoanInfoBuilder {
    this.instance.nearPrincipal = value;
    return this;
  }

  nextPaymentDate(value: string): LoanInfoBuilder {
    this.instance.nextPaymentDate = value;
    return this;
  }

  debtAmount(value: number): LoanInfoBuilder {
    this.instance.debtAmount = value;
    return this;
  }

  customerId(value: string): LoanInfoBuilder {
    this.instance.customerId = value;
    return this;
  }

  deptDate(value: string): LoanInfoBuilder {
    this.instance.deptDate = value;
    return this;
  }

  passport(value: string): LoanInfoBuilder {
    this.instance.passport = value;
    return this;
  }

  repayType(value: boolean): LoanInfoBuilder {
    this.instance.repayType = value;
    return this;
  }

  commission(value: number): LoanInfoBuilder {
    this.instance.commission = value;
    return this;
  }

  nextRollover(value: string): LoanInfoBuilder {
    this.instance.nextRollover = value;
    return this;
  }

  amountRange(value: AmountRange): LoanInfoBuilder {
    this.instance.amountRange = value;
    return this;
  }

  updated(value: boolean): LoanInfoBuilder {
    this.instance.updated = value;
    return this;
  }

  opCode(value: number): LoanInfoBuilder {
    this.instance.opCode = value;
    return this;
  }

  opDescription(value: string): LoanInfoBuilder {
    this.instance.opDescription = value;
    return this;
  }

  currency(value: string): LoanInfoBuilder {
    this.instance.currency = value;
    return this;
  }

  serviceId(value: number): LoanInfoBuilder {
    this.instance.serviceId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class TransactionHistory {
  purpose: string;
  contractNum: string;
  userCode: string;
  paymentId: string;
  date: Date;
  amount: number;

  constructor() {
  }

  static build(): TransactionHistoryBuilder {
    return new TransactionHistoryBuilder();
  }
}

class TransactionHistoryBuilder {
  private instance: TransactionHistory;

  constructor() {
    this.instance = new TransactionHistory();
  }

  purpose(value: string): TransactionHistoryBuilder {
    this.instance.purpose = value;
    return this;
  }

  contractNum(value: string): TransactionHistoryBuilder {
    this.instance.contractNum = value;
    return this;
  }

  userCode(value: string): TransactionHistoryBuilder {
    this.instance.userCode = value;
    return this;
  }

  paymentId(value: string): TransactionHistoryBuilder {
    this.instance.paymentId = value;
    return this;
  }

  date(value: Date): TransactionHistoryBuilder {
    this.instance.date = value;
    return this;
  }

  amount(value: number): TransactionHistoryBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ASClientData {
  outerId: string;
  dateregn: Date;
  name: string;
  ename: string;
  tel: string;
  email: string;

  constructor() {
  }

  static build(): ASClientDataBuilder {
    return new ASClientDataBuilder();
  }
}

class ASClientDataBuilder {
  private instance: ASClientData;

  constructor() {
    this.instance = new ASClientData();
  }

  outerId(value: string): ASClientDataBuilder {
    this.instance.outerId = value;
    return this;
  }

  dateregn(value: Date): ASClientDataBuilder {
    this.instance.dateregn = value;
    return this;
  }

  name(value: string): ASClientDataBuilder {
    this.instance.name = value;
    return this;
  }

  ename(value: string): ASClientDataBuilder {
    this.instance.ename = value;
    return this;
  }

  tel(value: string): ASClientDataBuilder {
    this.instance.tel = value;
    return this;
  }

  email(value: string): ASClientDataBuilder {
    this.instance.email = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ASPaymentData {
  transactrionId: string;
  paymentId: string;
  operatorId: string;
  cashboxId: string;
  branchCode: string;
  accDebet: string;
  accCredit: string;
  completed: Date;
  amount: number;
  debt: number;
  commissionAmount: number;
  providerId: string;
  userNum: string;
  aim: string;

  constructor() {
  }

  static build(): ASPaymentDataBuilder {
    return new ASPaymentDataBuilder();
  }
}

class ASPaymentDataBuilder {
  private instance: ASPaymentData;

  constructor() {
    this.instance = new ASPaymentData();
  }

  transactrionId(value: string): ASPaymentDataBuilder {
    this.instance.transactrionId = value;
    return this;
  }

  paymentId(value: string): ASPaymentDataBuilder {
    this.instance.paymentId = value;
    return this;
  }

  operatorId(value: string): ASPaymentDataBuilder {
    this.instance.operatorId = value;
    return this;
  }

  cashboxId(value: string): ASPaymentDataBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  branchCode(value: string): ASPaymentDataBuilder {
    this.instance.branchCode = value;
    return this;
  }

  accDebet(value: string): ASPaymentDataBuilder {
    this.instance.accDebet = value;
    return this;
  }

  accCredit(value: string): ASPaymentDataBuilder {
    this.instance.accCredit = value;
    return this;
  }

  completed(value: Date): ASPaymentDataBuilder {
    this.instance.completed = value;
    return this;
  }

  amount(value: number): ASPaymentDataBuilder {
    this.instance.amount = value;
    return this;
  }

  debt(value: number): ASPaymentDataBuilder {
    this.instance.debt = value;
    return this;
  }

  commissionAmount(value: number): ASPaymentDataBuilder {
    this.instance.commissionAmount = value;
    return this;
  }

  providerId(value: string): ASPaymentDataBuilder {
    this.instance.providerId = value;
    return this;
  }

  userNum(value: string): ASPaymentDataBuilder {
    this.instance.userNum = value;
    return this;
  }

  aim(value: string): ASPaymentDataBuilder {
    this.instance.aim = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashboxInOut {
  cashboxId: string;
  in: number;
  out: number;

  constructor() {
  }

  static build(): CashboxInOutBuilder {
    return new CashboxInOutBuilder();
  }
}

class CashboxInOutBuilder {
  private instance: CashboxInOut;

  constructor() {
    this.instance = new CashboxInOut();
  }

  cashboxId(value: string): CashboxInOutBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  in(value: number): CashboxInOutBuilder {
    this.instance.in = value;
    return this;
  }

  out(value: number): CashboxInOutBuilder {
    this.instance.out = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export enum ASImportStatus {
  NOT_IMPORTED = "NOT_IMPORTED",
  IMPORTED = "IMPORTED",
}

export class Token {
  id: string;
  identity: string;
  userId: string;
  issued: Date;
  value: string;
  scopes: Array<Scope>;

  constructor() {
  }

  static build(): TokenBuilder {
    return new TokenBuilder();
  }
}

class TokenBuilder {
  private instance: Token;

  constructor() {
    this.instance = new Token();
  }

  id(value: string): TokenBuilder {
    this.instance.id = value;
    return this;
  }

  identity(value: string): TokenBuilder {
    this.instance.identity = value;
    return this;
  }

  userId(value: string): TokenBuilder {
    this.instance.userId = value;
    return this;
  }

  issued(value: Date): TokenBuilder {
    this.instance.issued = value;
    return this;
  }

  value(value: string): TokenBuilder {
    this.instance.value = value;
    return this;
  }

  scopes(value: Array<Scope>): TokenBuilder {
    this.instance.scopes = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Admin {
  id: string;
  email: string;
  password: string;
  scope: Scope;

  constructor() {
  }

  static build(): AdminBuilder {
    return new AdminBuilder();
  }
}

class AdminBuilder {
  private instance: Admin;

  constructor() {
    this.instance = new Admin();
  }

  id(value: string): AdminBuilder {
    this.instance.id = value;
    return this;
  }

  email(value: string): AdminBuilder {
    this.instance.email = value;
    return this;
  }

  password(value: string): AdminBuilder {
    this.instance.password = value;
    return this;
  }

  scope(value: Scope): AdminBuilder {
    this.instance.scope = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Customer {
  id: string;
  email: string;
  number: string;
  phone: string;
  forename: string;
  surname: string;
  status: CustomerStatus;
  isVerified: boolean;
  password: string;
  pincode: string;
  scope: Array<Scope>;

  constructor() {
  }

  static build(): CustomerBuilder {
    return new CustomerBuilder();
  }
}

class CustomerBuilder {
  private instance: Customer;

  constructor() {
    this.instance = new Customer();
  }

  id(value: string): CustomerBuilder {
    this.instance.id = value;
    return this;
  }

  email(value: string): CustomerBuilder {
    this.instance.email = value;
    return this;
  }

  number(value: string): CustomerBuilder {
    this.instance.number = value;
    return this;
  }

  phone(value: string): CustomerBuilder {
    this.instance.phone = value;
    return this;
  }

  forename(value: string): CustomerBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): CustomerBuilder {
    this.instance.surname = value;
    return this;
  }

  status(value: CustomerStatus): CustomerBuilder {
    this.instance.status = value;
    return this;
  }

  isVerified(value: boolean): CustomerBuilder {
    this.instance.isVerified = value;
    return this;
  }

  password(value: string): CustomerBuilder {
    this.instance.password = value;
    return this;
  }

  pincode(value: string): CustomerBuilder {
    this.instance.pincode = value;
    return this;
  }

  scope(value: Array<Scope>): CustomerBuilder {
    this.instance.scope = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Registration {
  id: string;
  regId: string;
  phone: string;
  password: string;
  activationCode: string;

  constructor() {
  }

  static build(): RegistrationBuilder {
    return new RegistrationBuilder();
  }
}

class RegistrationBuilder {
  private instance: Registration;

  constructor() {
    this.instance = new Registration();
  }

  id(value: string): RegistrationBuilder {
    this.instance.id = value;
    return this;
  }

  regId(value: string): RegistrationBuilder {
    this.instance.regId = value;
    return this;
  }

  phone(value: string): RegistrationBuilder {
    this.instance.phone = value;
    return this;
  }

  password(value: string): RegistrationBuilder {
    this.instance.password = value;
    return this;
  }

  activationCode(value: string): RegistrationBuilder {
    this.instance.activationCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Operator {
  id: string;
  email: string;
  number: string;
  phone: string;
  userName: string;
  cashboxId: string;
  forename: string;
  surname: string;
  password: string;
  expiration: Date;
  enabled: boolean;
  status: OperatorStatus;
  scope: Array<Scope>;

  constructor() {
  }

  static build(): OperatorBuilder {
    return new OperatorBuilder();
  }
}

class OperatorBuilder {
  private instance: Operator;

  constructor() {
    this.instance = new Operator();
  }

  id(value: string): OperatorBuilder {
    this.instance.id = value;
    return this;
  }

  email(value: string): OperatorBuilder {
    this.instance.email = value;
    return this;
  }

  number(value: string): OperatorBuilder {
    this.instance.number = value;
    return this;
  }

  phone(value: string): OperatorBuilder {
    this.instance.phone = value;
    return this;
  }

  userName(value: string): OperatorBuilder {
    this.instance.userName = value;
    return this;
  }

  cashboxId(value: string): OperatorBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  forename(value: string): OperatorBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): OperatorBuilder {
    this.instance.surname = value;
    return this;
  }

  password(value: string): OperatorBuilder {
    this.instance.password = value;
    return this;
  }

  expiration(value: Date): OperatorBuilder {
    this.instance.expiration = value;
    return this;
  }

  enabled(value: boolean): OperatorBuilder {
    this.instance.enabled = value;
    return this;
  }

  status(value: OperatorStatus): OperatorBuilder {
    this.instance.status = value;
    return this;
  }

  scope(value: Array<Scope>): OperatorBuilder {
    this.instance.scope = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Sequence {
  id: string;
  name: string;
  last: number;

  constructor() {
  }

  static build(): SequenceBuilder {
    return new SequenceBuilder();
  }
}

class SequenceBuilder {
  private instance: Sequence;

  constructor() {
    this.instance = new Sequence();
  }

  id(value: string): SequenceBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): SequenceBuilder {
    this.instance.name = value;
    return this;
  }

  last(value: number): SequenceBuilder {
    this.instance.last = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Data {
  id: string;
  key: string;
  value: string;

  constructor() {
  }

  static build(): DataBuilder {
    return new DataBuilder();
  }
}

class DataBuilder {
  private instance: Data;

  constructor() {
    this.instance = new Data();
  }

  id(value: string): DataBuilder {
    this.instance.id = value;
    return this;
  }

  key(value: string): DataBuilder {
    this.instance.key = value;
    return this;
  }

  value(value: string): DataBuilder {
    this.instance.value = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Branch {
  id: string;
  name: string;
  code: string;
  address: string;
  policyId: string;
  description: string;
  enabled: boolean;

  constructor() {
  }

  static build(): BranchBuilder {
    return new BranchBuilder();
  }
}

class BranchBuilder {
  private instance: Branch;

  constructor() {
    this.instance = new Branch();
  }

  id(value: string): BranchBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): BranchBuilder {
    this.instance.name = value;
    return this;
  }

  code(value: string): BranchBuilder {
    this.instance.code = value;
    return this;
  }

  address(value: string): BranchBuilder {
    this.instance.address = value;
    return this;
  }

  policyId(value: string): BranchBuilder {
    this.instance.policyId = value;
    return this;
  }

  description(value: string): BranchBuilder {
    this.instance.description = value;
    return this;
  }

  enabled(value: boolean): BranchBuilder {
    this.instance.enabled = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Cashbox {
  id: string;
  name: string;
  accountNumber: string;
  branchId: string;
  branchCode: number;
  note: string;
  certificateId: string;
  loggedInOperatorId: string;
  enabled: boolean;
  balance: number;
  operators: Array<string>;
  lastReportDate: Date;

  constructor() {
  }

  static build(): CashboxBuilder {
    return new CashboxBuilder();
  }
}

class CashboxBuilder {
  private instance: Cashbox;

  constructor() {
    this.instance = new Cashbox();
  }

  id(value: string): CashboxBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): CashboxBuilder {
    this.instance.name = value;
    return this;
  }

  accountNumber(value: string): CashboxBuilder {
    this.instance.accountNumber = value;
    return this;
  }

  branchId(value: string): CashboxBuilder {
    this.instance.branchId = value;
    return this;
  }

  branchCode(value: number): CashboxBuilder {
    this.instance.branchCode = value;
    return this;
  }

  note(value: string): CashboxBuilder {
    this.instance.note = value;
    return this;
  }

  certificateId(value: string): CashboxBuilder {
    this.instance.certificateId = value;
    return this;
  }

  loggedInOperatorId(value: string): CashboxBuilder {
    this.instance.loggedInOperatorId = value;
    return this;
  }

  enabled(value: boolean): CashboxBuilder {
    this.instance.enabled = value;
    return this;
  }

  balance(value: number): CashboxBuilder {
    this.instance.balance = value;
    return this;
  }

  operators(value: Array<string>): CashboxBuilder {
    this.instance.operators = value;
    return this;
  }

  lastReportDate(value: Date): CashboxBuilder {
    this.instance.lastReportDate = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashboxLog {
  id: string;
  operatorId: string;
  cashboxId: string;
  cashboxPaymentId: string;
  action: CashboxAction;
  amountTotal: number;
  serviceQuantity: number;
  amountIn: number;
  commission: number;
  cash: boolean;
  details: string;
  created: Date;

  constructor() {
  }

  static build(): CashboxLogBuilder {
    return new CashboxLogBuilder();
  }
}

class CashboxLogBuilder {
  private instance: CashboxLog;

  constructor() {
    this.instance = new CashboxLog();
  }

  id(value: string): CashboxLogBuilder {
    this.instance.id = value;
    return this;
  }

  operatorId(value: string): CashboxLogBuilder {
    this.instance.operatorId = value;
    return this;
  }

  cashboxId(value: string): CashboxLogBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  cashboxPaymentId(value: string): CashboxLogBuilder {
    this.instance.cashboxPaymentId = value;
    return this;
  }

  action(value: CashboxAction): CashboxLogBuilder {
    this.instance.action = value;
    return this;
  }

  amountTotal(value: number): CashboxLogBuilder {
    this.instance.amountTotal = value;
    return this;
  }

  serviceQuantity(value: number): CashboxLogBuilder {
    this.instance.serviceQuantity = value;
    return this;
  }

  amountIn(value: number): CashboxLogBuilder {
    this.instance.amountIn = value;
    return this;
  }

  commission(value: number): CashboxLogBuilder {
    this.instance.commission = value;
    return this;
  }

  cash(value: boolean): CashboxLogBuilder {
    this.instance.cash = value;
    return this;
  }

  details(value: string): CashboxLogBuilder {
    this.instance.details = value;
    return this;
  }

  created(value: Date): CashboxLogBuilder {
    this.instance.created = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Group {
  id: string;
  name: string;
  desc: string;
  contracts: Array<Contract>;

  constructor() {
  }

  static build(): GroupBuilder {
    return new GroupBuilder();
  }
}

class GroupBuilder {
  private instance: Group;

  constructor() {
    this.instance = new Group();
  }

  id(value: string): GroupBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): GroupBuilder {
    this.instance.name = value;
    return this;
  }

  desc(value: string): GroupBuilder {
    this.instance.desc = value;
    return this;
  }

  contracts(value: Array<Contract>): GroupBuilder {
    this.instance.contracts = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomerGroup {
  id: string;
  name: string;
  customer: string;
  desc: string;
  contracts: Array<Contract>;

  constructor() {
  }

  static build(): CustomerGroupBuilder {
    return new CustomerGroupBuilder();
  }
}

class CustomerGroupBuilder {
  private instance: CustomerGroup;

  constructor() {
    this.instance = new CustomerGroup();
  }

  id(value: string): CustomerGroupBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): CustomerGroupBuilder {
    this.instance.name = value;
    return this;
  }

  customer(value: string): CustomerGroupBuilder {
    this.instance.customer = value;
    return this;
  }

  desc(value: string): CustomerGroupBuilder {
    this.instance.desc = value;
    return this;
  }

  contracts(value: Array<Contract>): CustomerGroupBuilder {
    this.instance.contracts = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Commission {
  id: string;
  providerId: string;
  policyId: string;
  approved: boolean;
  action: CommissionAction;
  ranges: Array<CommissionRange>;

  constructor() {
  }

  static build(): CommissionBuilder {
    return new CommissionBuilder();
  }
}

class CommissionBuilder {
  private instance: Commission;

  constructor() {
    this.instance = new Commission();
  }

  id(value: string): CommissionBuilder {
    this.instance.id = value;
    return this;
  }

  providerId(value: string): CommissionBuilder {
    this.instance.providerId = value;
    return this;
  }

  policyId(value: string): CommissionBuilder {
    this.instance.policyId = value;
    return this;
  }

  approved(value: boolean): CommissionBuilder {
    this.instance.approved = value;
    return this;
  }

  action(value: CommissionAction): CommissionBuilder {
    this.instance.action = value;
    return this;
  }

  ranges(value: Array<CommissionRange>): CommissionBuilder {
    this.instance.ranges = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CommissionPolicy {
  id: string;
  name: string;

  constructor() {
  }

  static build(): CommissionPolicyBuilder {
    return new CommissionPolicyBuilder();
  }
}

class CommissionPolicyBuilder {
  private instance: CommissionPolicy;

  constructor() {
    this.instance = new CommissionPolicy();
  }

  id(value: string): CommissionPolicyBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): CommissionPolicyBuilder {
    this.instance.name = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Provider {
  id: string;
  name: string;
  accountNumber: string;
  desc: string;
  enabled: boolean;
  useFixedAmount: boolean;
  labels: Array<string>;
  contractDescs: Array<ContractDesc>;

  constructor() {
  }

  static build(): ProviderBuilder {
    return new ProviderBuilder();
  }
}

class ProviderBuilder {
  private instance: Provider;

  constructor() {
    this.instance = new Provider();
  }

  id(value: string): ProviderBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): ProviderBuilder {
    this.instance.name = value;
    return this;
  }

  accountNumber(value: string): ProviderBuilder {
    this.instance.accountNumber = value;
    return this;
  }

  desc(value: string): ProviderBuilder {
    this.instance.desc = value;
    return this;
  }

  enabled(value: boolean): ProviderBuilder {
    this.instance.enabled = value;
    return this;
  }

  useFixedAmount(value: boolean): ProviderBuilder {
    this.instance.useFixedAmount = value;
    return this;
  }

  labels(value: Array<string>): ProviderBuilder {
    this.instance.labels = value;
    return this;
  }

  contractDescs(value: Array<ContractDesc>): ProviderBuilder {
    this.instance.contractDescs = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ProviderLabel {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;

  constructor() {
  }

  static build(): ProviderLabelBuilder {
    return new ProviderLabelBuilder();
  }
}

class ProviderLabelBuilder {
  private instance: ProviderLabel;

  constructor() {
    this.instance = new ProviderLabel();
  }

  id(value: string): ProviderLabelBuilder {
    this.instance.id = value;
    return this;
  }

  icon(value: string): ProviderLabelBuilder {
    this.instance.icon = value;
    return this;
  }

  name(value: string): ProviderLabelBuilder {
    this.instance.name = value;
    return this;
  }

  label(value: string): ProviderLabelBuilder {
    this.instance.label = value;
    return this;
  }

  desc(value: string): ProviderLabelBuilder {
    this.instance.desc = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class DayClosure {
  id: string;
  cashboxId: string;
  branchId: string;
  created: Date;
  cashierId: string;
  cashierName: string;
  amountIn: number;
  amountOut: number;
  incassation: number;
  finalBalance: number;
  note: string;

  constructor() {
  }

  static build(): DayClosureBuilder {
    return new DayClosureBuilder();
  }
}

class DayClosureBuilder {
  private instance: DayClosure;

  constructor() {
    this.instance = new DayClosure();
  }

  id(value: string): DayClosureBuilder {
    this.instance.id = value;
    return this;
  }

  cashboxId(value: string): DayClosureBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  branchId(value: string): DayClosureBuilder {
    this.instance.branchId = value;
    return this;
  }

  created(value: Date): DayClosureBuilder {
    this.instance.created = value;
    return this;
  }

  cashierId(value: string): DayClosureBuilder {
    this.instance.cashierId = value;
    return this;
  }

  cashierName(value: string): DayClosureBuilder {
    this.instance.cashierName = value;
    return this;
  }

  amountIn(value: number): DayClosureBuilder {
    this.instance.amountIn = value;
    return this;
  }

  amountOut(value: number): DayClosureBuilder {
    this.instance.amountOut = value;
    return this;
  }

  incassation(value: number): DayClosureBuilder {
    this.instance.incassation = value;
    return this;
  }

  finalBalance(value: number): DayClosureBuilder {
    this.instance.finalBalance = value;
    return this;
  }

  note(value: string): DayClosureBuilder {
    this.instance.note = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Basket {
  id: string;
  providerId: string;
  contractIdentity: string;
  operatorId: string;
  cashboxId: string;

  constructor() {
  }

  static build(): BasketBuilder {
    return new BasketBuilder();
  }
}

class BasketBuilder {
  private instance: Basket;

  constructor() {
    this.instance = new Basket();
  }

  id(value: string): BasketBuilder {
    this.instance.id = value;
    return this;
  }

  providerId(value: string): BasketBuilder {
    this.instance.providerId = value;
    return this;
  }

  contractIdentity(value: string): BasketBuilder {
    this.instance.contractIdentity = value;
    return this;
  }

  operatorId(value: string): BasketBuilder {
    this.instance.operatorId = value;
    return this;
  }

  cashboxId(value: string): BasketBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Payment {
  id: string;
  operatorId: string;
  providerId: string;
  contract: string;
  amount: number;
  issued: Date;
  completed: Date;

  constructor() {
  }

  static build(): PaymentBuilder {
    return new PaymentBuilder();
  }
}

class PaymentBuilder {
  private instance: Payment;

  constructor() {
    this.instance = new Payment();
  }

  id(value: string): PaymentBuilder {
    this.instance.id = value;
    return this;
  }

  operatorId(value: string): PaymentBuilder {
    this.instance.operatorId = value;
    return this;
  }

  providerId(value: string): PaymentBuilder {
    this.instance.providerId = value;
    return this;
  }

  contract(value: string): PaymentBuilder {
    this.instance.contract = value;
    return this;
  }

  amount(value: number): PaymentBuilder {
    this.instance.amount = value;
    return this;
  }

  issued(value: Date): PaymentBuilder {
    this.instance.issued = value;
    return this;
  }

  completed(value: Date): PaymentBuilder {
    this.instance.completed = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Transaction {
  id: string;
  paymentId: string;
  receiptId: string;
  issuer: string;
  issued: Date;
  completed: Date;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  branchName: string;
  providerId: string;
  purpose: string;
  amount: number;
  commission: number;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  userNum: string;

  constructor() {
  }

  static build(): TransactionBuilder {
    return new TransactionBuilder();
  }
}

class TransactionBuilder {
  private instance: Transaction;

  constructor() {
    this.instance = new Transaction();
  }

  id(value: string): TransactionBuilder {
    this.instance.id = value;
    return this;
  }

  paymentId(value: string): TransactionBuilder {
    this.instance.paymentId = value;
    return this;
  }

  receiptId(value: string): TransactionBuilder {
    this.instance.receiptId = value;
    return this;
  }

  issuer(value: string): TransactionBuilder {
    this.instance.issuer = value;
    return this;
  }

  issued(value: Date): TransactionBuilder {
    this.instance.issued = value;
    return this;
  }

  completed(value: Date): TransactionBuilder {
    this.instance.completed = value;
    return this;
  }

  operatorId(value: string): TransactionBuilder {
    this.instance.operatorId = value;
    return this;
  }

  cashboxId(value: string): TransactionBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  branchId(value: string): TransactionBuilder {
    this.instance.branchId = value;
    return this;
  }

  branchName(value: string): TransactionBuilder {
    this.instance.branchName = value;
    return this;
  }

  providerId(value: string): TransactionBuilder {
    this.instance.providerId = value;
    return this;
  }

  purpose(value: string): TransactionBuilder {
    this.instance.purpose = value;
    return this;
  }

  amount(value: number): TransactionBuilder {
    this.instance.amount = value;
    return this;
  }

  commission(value: number): TransactionBuilder {
    this.instance.commission = value;
    return this;
  }

  debt(value: number): TransactionBuilder {
    this.instance.debt = value;
    return this;
  }

  destinationWalletId(value: string): TransactionBuilder {
    this.instance.destinationWalletId = value;
    return this;
  }

  commissionAmount(value: number): TransactionBuilder {
    this.instance.commissionAmount = value;
    return this;
  }

  comment(value: string): TransactionBuilder {
    this.instance.comment = value;
    return this;
  }

  userNum(value: string): TransactionBuilder {
    this.instance.userNum = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Card {
  id: string;
  customerId: string;
  token: string;
  holder: string;
  number: string;
  expires: Date;
  type: string;
  bank: string;

  constructor() {
  }

  static build(): CardBuilder {
    return new CardBuilder();
  }
}

class CardBuilder {
  private instance: Card;

  constructor() {
    this.instance = new Card();
  }

  id(value: string): CardBuilder {
    this.instance.id = value;
    return this;
  }

  customerId(value: string): CardBuilder {
    this.instance.customerId = value;
    return this;
  }

  token(value: string): CardBuilder {
    this.instance.token = value;
    return this;
  }

  holder(value: string): CardBuilder {
    this.instance.holder = value;
    return this;
  }

  number(value: string): CardBuilder {
    this.instance.number = value;
    return this;
  }

  expires(value: Date): CardBuilder {
    this.instance.expires = value;
    return this;
  }

  type(value: string): CardBuilder {
    this.instance.type = value;
    return this;
  }

  bank(value: string): CardBuilder {
    this.instance.bank = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ArmsoftClients {
  id: string;
  created: Date;
  data: ASClientData;
  status: string;

  constructor() {
  }

  static build(): ArmsoftClientsBuilder {
    return new ArmsoftClientsBuilder();
  }
}

class ArmsoftClientsBuilder {
  private instance: ArmsoftClients;

  constructor() {
    this.instance = new ArmsoftClients();
  }

  id(value: string): ArmsoftClientsBuilder {
    this.instance.id = value;
    return this;
  }

  created(value: Date): ArmsoftClientsBuilder {
    this.instance.created = value;
    return this;
  }

  data(value: ASClientData): ArmsoftClientsBuilder {
    this.instance.data = value;
    return this;
  }

  status(value: string): ArmsoftClientsBuilder {
    this.instance.status = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ArmsoftPayments {
  id: string;
  created: Date;
  data: ASPaymentData;
  status: string;

  constructor() {
  }

  static build(): ArmsoftPaymentsBuilder {
    return new ArmsoftPaymentsBuilder();
  }
}

class ArmsoftPaymentsBuilder {
  private instance: ArmsoftPayments;

  constructor() {
    this.instance = new ArmsoftPayments();
  }

  id(value: string): ArmsoftPaymentsBuilder {
    this.instance.id = value;
    return this;
  }

  created(value: Date): ArmsoftPaymentsBuilder {
    this.instance.created = value;
    return this;
  }

  data(value: ASPaymentData): ArmsoftPaymentsBuilder {
    this.instance.data = value;
    return this;
  }

  status(value: string): ArmsoftPaymentsBuilder {
    this.instance.status = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashboxReport {
  id: string;
  cashboxId: string;
  cashboxName: string;
  branchId: string;
  branchName: string;
  operatorId: string;
  operatorName: string;
  created: Date;
  balance: number;
  encashmentIn: number;
  encashmentOut: number;
  dataByProviders: string;

  constructor() {
  }

  static build(): CashboxReportBuilder {
    return new CashboxReportBuilder();
  }
}

class CashboxReportBuilder {
  private instance: CashboxReport;

  constructor() {
    this.instance = new CashboxReport();
  }

  id(value: string): CashboxReportBuilder {
    this.instance.id = value;
    return this;
  }

  cashboxId(value: string): CashboxReportBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  cashboxName(value: string): CashboxReportBuilder {
    this.instance.cashboxName = value;
    return this;
  }

  branchId(value: string): CashboxReportBuilder {
    this.instance.branchId = value;
    return this;
  }

  branchName(value: string): CashboxReportBuilder {
    this.instance.branchName = value;
    return this;
  }

  operatorId(value: string): CashboxReportBuilder {
    this.instance.operatorId = value;
    return this;
  }

  operatorName(value: string): CashboxReportBuilder {
    this.instance.operatorName = value;
    return this;
  }

  created(value: Date): CashboxReportBuilder {
    this.instance.created = value;
    return this;
  }

  balance(value: number): CashboxReportBuilder {
    this.instance.balance = value;
    return this;
  }

  encashmentIn(value: number): CashboxReportBuilder {
    this.instance.encashmentIn = value;
    return this;
  }

  encashmentOut(value: number): CashboxReportBuilder {
    this.instance.encashmentOut = value;
    return this;
  }

  dataByProviders(value: string): CashboxReportBuilder {
    this.instance.dataByProviders = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateAdminCommand {
  id: string;
  email: string;
  password: string;

  constructor() {
  }

  static build(): AdminCreateAdminCommandBuilder {
    return new AdminCreateAdminCommandBuilder();
  }
}

class AdminCreateAdminCommandBuilder {
  private instance: AdminCreateAdminCommand;

  constructor() {
    this.instance = new AdminCreateAdminCommand();
  }

  id(value: string): AdminCreateAdminCommandBuilder {
    this.instance.id = value;
    return this;
  }

  email(value: string): AdminCreateAdminCommandBuilder {
    this.instance.email = value;
    return this;
  }

  password(value: string): AdminCreateAdminCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminAuthenticateCommand {
  authenticationId: string;
  email: string;
  password: string;

  constructor() {
  }

  static build(): AdminAuthenticateCommandBuilder {
    return new AdminAuthenticateCommandBuilder();
  }
}

class AdminAuthenticateCommandBuilder {
  private instance: AdminAuthenticateCommand;

  constructor() {
    this.instance = new AdminAuthenticateCommand();
  }

  authenticationId(value: string): AdminAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  email(value: string): AdminAuthenticateCommandBuilder {
    this.instance.email = value;
    return this;
  }

  password(value: string): AdminAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateBranchCommand {
  id: string;
  name: string;
  code: string;
  address: string;
  desc: string;
  policyId: string;

  constructor() {
  }

  static build(): AdminCreateBranchCommandBuilder {
    return new AdminCreateBranchCommandBuilder();
  }
}

class AdminCreateBranchCommandBuilder {
  private instance: AdminCreateBranchCommand;

  constructor() {
    this.instance = new AdminCreateBranchCommand();
  }

  id(value: string): AdminCreateBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminCreateBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }

  code(value: string): AdminCreateBranchCommandBuilder {
    this.instance.code = value;
    return this;
  }

  address(value: string): AdminCreateBranchCommandBuilder {
    this.instance.address = value;
    return this;
  }

  desc(value: string): AdminCreateBranchCommandBuilder {
    this.instance.desc = value;
    return this;
  }

  policyId(value: string): AdminCreateBranchCommandBuilder {
    this.instance.policyId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateBranchCommand {
  id: string;
  name: string;
  code: string;
  address: string;
  desc: string;
  policyId: string;

  constructor() {
  }

  static build(): AdminUpdateBranchCommandBuilder {
    return new AdminUpdateBranchCommandBuilder();
  }
}

class AdminUpdateBranchCommandBuilder {
  private instance: AdminUpdateBranchCommand;

  constructor() {
    this.instance = new AdminUpdateBranchCommand();
  }

  id(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }

  code(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.code = value;
    return this;
  }

  address(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.address = value;
    return this;
  }

  desc(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.desc = value;
    return this;
  }

  policyId(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.policyId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminEnableBranchCommand {
  id: string;

  constructor() {
  }

  static build(): AdminEnableBranchCommandBuilder {
    return new AdminEnableBranchCommandBuilder();
  }
}

class AdminEnableBranchCommandBuilder {
  private instance: AdminEnableBranchCommand;

  constructor() {
    this.instance = new AdminEnableBranchCommand();
  }

  id(value: string): AdminEnableBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDisableBranchCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDisableBranchCommandBuilder {
    return new AdminDisableBranchCommandBuilder();
  }
}

class AdminDisableBranchCommandBuilder {
  private instance: AdminDisableBranchCommand;

  constructor() {
    this.instance = new AdminDisableBranchCommand();
  }

  id(value: string): AdminDisableBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetAccessTokenQuery {
  authenticationId: string;

  constructor() {
  }

  static build(): AdminGetAccessTokenQueryBuilder {
    return new AdminGetAccessTokenQueryBuilder();
  }
}

class AdminGetAccessTokenQueryBuilder {
  private instance: AdminGetAccessTokenQuery;

  constructor() {
    this.instance = new AdminGetAccessTokenQuery();
  }

  authenticationId(value: string): AdminGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetBranchQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetBranchQueryBuilder {
    return new AdminGetBranchQueryBuilder();
  }
}

class AdminGetBranchQueryBuilder {
  private instance: AdminGetBranchQuery;

  constructor() {
    this.instance = new AdminGetBranchQuery();
  }

  id(value: string): AdminGetBranchQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminFindBranchesQuery {
  name: string;
  address: string;
  desc: string;
  enabled: boolean;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminFindBranchesQueryBuilder {
    return new AdminFindBranchesQueryBuilder();
  }
}

class AdminFindBranchesQueryBuilder {
  private instance: AdminFindBranchesQuery;

  constructor() {
    this.instance = new AdminFindBranchesQuery();
  }

  name(value: string): AdminFindBranchesQueryBuilder {
    this.instance.name = value;
    return this;
  }

  address(value: string): AdminFindBranchesQueryBuilder {
    this.instance.address = value;
    return this;
  }

  desc(value: string): AdminFindBranchesQueryBuilder {
    this.instance.desc = value;
    return this;
  }

  enabled(value: boolean): AdminFindBranchesQueryBuilder {
    this.instance.enabled = value;
    return this;
  }

  page(value: Page): AdminFindBranchesQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminFindBranchesQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateCashboxCommand {
  id: string;
  name: string;
  code: string;
  branch: string;
  note: string;
  certificateId: string;

  constructor() {
  }

  static build(): AdminCreateCashboxCommandBuilder {
    return new AdminCreateCashboxCommandBuilder();
  }
}

class AdminCreateCashboxCommandBuilder {
  private instance: AdminCreateCashboxCommand;

  constructor() {
    this.instance = new AdminCreateCashboxCommand();
  }

  id(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.name = value;
    return this;
  }

  code(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.code = value;
    return this;
  }

  branch(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.branch = value;
    return this;
  }

  note(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.note = value;
    return this;
  }

  certificateId(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.certificateId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateCashboxCommand {
  id: string;
  name: string;
  code: string;
  branch: string;
  note: string;
  certificateId: string;

  constructor() {
  }

  static build(): AdminUpdateCashboxCommandBuilder {
    return new AdminUpdateCashboxCommandBuilder();
  }
}

class AdminUpdateCashboxCommandBuilder {
  private instance: AdminUpdateCashboxCommand;

  constructor() {
    this.instance = new AdminUpdateCashboxCommand();
  }

  id(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.name = value;
    return this;
  }

  code(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.code = value;
    return this;
  }

  branch(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.branch = value;
    return this;
  }

  note(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.note = value;
    return this;
  }

  certificateId(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.certificateId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminEnableCashboxCommand {
  id: string;

  constructor() {
  }

  static build(): AdminEnableCashboxCommandBuilder {
    return new AdminEnableCashboxCommandBuilder();
  }
}

class AdminEnableCashboxCommandBuilder {
  private instance: AdminEnableCashboxCommand;

  constructor() {
    this.instance = new AdminEnableCashboxCommand();
  }

  id(value: string): AdminEnableCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDisableCashboxCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDisableCashboxCommandBuilder {
    return new AdminDisableCashboxCommandBuilder();
  }
}

class AdminDisableCashboxCommandBuilder {
  private instance: AdminDisableCashboxCommand;

  constructor() {
    this.instance = new AdminDisableCashboxCommand();
  }

  id(value: string): AdminDisableCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateOperatorCommand {
  id: string;
  userName: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  password: string;

  constructor() {
  }

  static build(): AdminCreateOperatorCommandBuilder {
    return new AdminCreateOperatorCommandBuilder();
  }
}

class AdminCreateOperatorCommandBuilder {
  private instance: AdminCreateOperatorCommand;

  constructor() {
    this.instance = new AdminCreateOperatorCommand();
  }

  id(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }

  userName(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.userName = value;
    return this;
  }

  email(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.email = value;
    return this;
  }

  phone(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  forename(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.surname = value;
    return this;
  }

  password(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateOperatorCommand {
  id: string;
  forename: string;
  surname: string;
  email: string;
  phone: string;
  scope: Array<Scope>;

  constructor() {
  }

  static build(): AdminUpdateOperatorCommandBuilder {
    return new AdminUpdateOperatorCommandBuilder();
  }
}

class AdminUpdateOperatorCommandBuilder {
  private instance: AdminUpdateOperatorCommand;

  constructor() {
    this.instance = new AdminUpdateOperatorCommand();
  }

  id(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }

  forename(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.surname = value;
    return this;
  }

  email(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.email = value;
    return this;
  }

  phone(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  scope(value: Array<Scope>): AdminUpdateOperatorCommandBuilder {
    this.instance.scope = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminResetPasswordCommand {
  id: string;
  password: string;

  constructor() {
  }

  static build(): AdminResetPasswordCommandBuilder {
    return new AdminResetPasswordCommandBuilder();
  }
}

class AdminResetPasswordCommandBuilder {
  private instance: AdminResetPasswordCommand;

  constructor() {
    this.instance = new AdminResetPasswordCommand();
  }

  id(value: string): AdminResetPasswordCommandBuilder {
    this.instance.id = value;
    return this;
  }

  password(value: string): AdminResetPasswordCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminAttachOperatorCommand {
  cashboxId: string;
  operatorId: string;

  constructor() {
  }

  static build(): AdminAttachOperatorCommandBuilder {
    return new AdminAttachOperatorCommandBuilder();
  }
}

class AdminAttachOperatorCommandBuilder {
  private instance: AdminAttachOperatorCommand;

  constructor() {
    this.instance = new AdminAttachOperatorCommand();
  }

  cashboxId(value: string): AdminAttachOperatorCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  operatorId(value: string): AdminAttachOperatorCommandBuilder {
    this.instance.operatorId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDetachOperatorCommand {
  cashboxId: string;
  operatorId: string;

  constructor() {
  }

  static build(): AdminDetachOperatorCommandBuilder {
    return new AdminDetachOperatorCommandBuilder();
  }
}

class AdminDetachOperatorCommandBuilder {
  private instance: AdminDetachOperatorCommand;

  constructor() {
    this.instance = new AdminDetachOperatorCommand();
  }

  cashboxId(value: string): AdminDetachOperatorCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  operatorId(value: string): AdminDetachOperatorCommandBuilder {
    this.instance.operatorId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateProviderCommand {
  id: string;
  name: string;
  desc: string;
  useFixedAmount: boolean;
  labels: Array<string>;

  constructor() {
  }

  static build(): AdminUpdateProviderCommandBuilder {
    return new AdminUpdateProviderCommandBuilder();
  }
}

class AdminUpdateProviderCommandBuilder {
  private instance: AdminUpdateProviderCommand;

  constructor() {
    this.instance = new AdminUpdateProviderCommand();
  }

  id(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.name = value;
    return this;
  }

  desc(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.desc = value;
    return this;
  }

  useFixedAmount(value: boolean): AdminUpdateProviderCommandBuilder {
    this.instance.useFixedAmount = value;
    return this;
  }

  labels(value: Array<string>): AdminUpdateProviderCommandBuilder {
    this.instance.labels = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminEnableProviderCommand {
  id: string;

  constructor() {
  }

  static build(): AdminEnableProviderCommandBuilder {
    return new AdminEnableProviderCommandBuilder();
  }
}

class AdminEnableProviderCommandBuilder {
  private instance: AdminEnableProviderCommand;

  constructor() {
    this.instance = new AdminEnableProviderCommand();
  }

  id(value: string): AdminEnableProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDisableProviderCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDisableProviderCommandBuilder {
    return new AdminDisableProviderCommandBuilder();
  }
}

class AdminDisableProviderCommandBuilder {
  private instance: AdminDisableProviderCommand;

  constructor() {
    this.instance = new AdminDisableProviderCommand();
  }

  id(value: string): AdminDisableProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateProviderLabelCommand {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;

  constructor() {
  }

  static build(): AdminCreateProviderLabelCommandBuilder {
    return new AdminCreateProviderLabelCommandBuilder();
  }
}

class AdminCreateProviderLabelCommandBuilder {
  private instance: AdminCreateProviderLabelCommand;

  constructor() {
    this.instance = new AdminCreateProviderLabelCommand();
  }

  id(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }

  icon(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.icon = value;
    return this;
  }

  name(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.name = value;
    return this;
  }

  label(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.label = value;
    return this;
  }

  desc(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.desc = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateProviderLabelCommand {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;

  constructor() {
  }

  static build(): AdminUpdateProviderLabelCommandBuilder {
    return new AdminUpdateProviderLabelCommandBuilder();
  }
}

class AdminUpdateProviderLabelCommandBuilder {
  private instance: AdminUpdateProviderLabelCommand;

  constructor() {
    this.instance = new AdminUpdateProviderLabelCommand();
  }

  id(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }

  icon(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.icon = value;
    return this;
  }

  name(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.name = value;
    return this;
  }

  label(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.label = value;
    return this;
  }

  desc(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.desc = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDeleteProviderLabelCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDeleteProviderLabelCommandBuilder {
    return new AdminDeleteProviderLabelCommandBuilder();
  }
}

class AdminDeleteProviderLabelCommandBuilder {
  private instance: AdminDeleteProviderLabelCommand;

  constructor() {
    this.instance = new AdminDeleteProviderLabelCommand();
  }

  id(value: string): AdminDeleteProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminEnableOperatorCommand {
  id: string;

  constructor() {
  }

  static build(): AdminEnableOperatorCommandBuilder {
    return new AdminEnableOperatorCommandBuilder();
  }
}

class AdminEnableOperatorCommandBuilder {
  private instance: AdminEnableOperatorCommand;

  constructor() {
    this.instance = new AdminEnableOperatorCommand();
  }

  id(value: string): AdminEnableOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDisableOperatorCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDisableOperatorCommandBuilder {
    return new AdminDisableOperatorCommandBuilder();
  }
}

class AdminDisableOperatorCommandBuilder {
  private instance: AdminDisableOperatorCommand;

  constructor() {
    this.instance = new AdminDisableOperatorCommand();
  }

  id(value: string): AdminDisableOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCashboxQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetCashboxQueryBuilder {
    return new AdminGetCashboxQueryBuilder();
  }
}

class AdminGetCashboxQueryBuilder {
  private instance: AdminGetCashboxQuery;

  constructor() {
    this.instance = new AdminGetCashboxQuery();
  }

  id(value: string): AdminGetCashboxQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCashboxesQuery {
  name: string;
  branchId: string;
  enabled: boolean;
  loggedInOperatorId: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetCashboxesQueryBuilder {
    return new AdminGetCashboxesQueryBuilder();
  }
}

class AdminGetCashboxesQueryBuilder {
  private instance: AdminGetCashboxesQuery;

  constructor() {
    this.instance = new AdminGetCashboxesQuery();
  }

  name(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.name = value;
    return this;
  }

  branchId(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.branchId = value;
    return this;
  }

  enabled(value: boolean): AdminGetCashboxesQueryBuilder {
    this.instance.enabled = value;
    return this;
  }

  loggedInOperatorId(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.loggedInOperatorId = value;
    return this;
  }

  page(value: Page): AdminGetCashboxesQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetCashboxesQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetProviderQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetProviderQueryBuilder {
    return new AdminGetProviderQueryBuilder();
  }
}

class AdminGetProviderQueryBuilder {
  private instance: AdminGetProviderQuery;

  constructor() {
    this.instance = new AdminGetProviderQuery();
  }

  id(value: string): AdminGetProviderQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetProvidersQuery {
  label: string;
  name: string;
  enabled: boolean;
  hasFixedAmount: boolean;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetProvidersQueryBuilder {
    return new AdminGetProvidersQueryBuilder();
  }
}

class AdminGetProvidersQueryBuilder {
  private instance: AdminGetProvidersQuery;

  constructor() {
    this.instance = new AdminGetProvidersQuery();
  }

  label(value: string): AdminGetProvidersQueryBuilder {
    this.instance.label = value;
    return this;
  }

  name(value: string): AdminGetProvidersQueryBuilder {
    this.instance.name = value;
    return this;
  }

  enabled(value: boolean): AdminGetProvidersQueryBuilder {
    this.instance.enabled = value;
    return this;
  }

  hasFixedAmount(value: boolean): AdminGetProvidersQueryBuilder {
    this.instance.hasFixedAmount = value;
    return this;
  }

  page(value: Page): AdminGetProvidersQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetProvidersQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetOperatorQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetOperatorQueryBuilder {
    return new AdminGetOperatorQueryBuilder();
  }
}

class AdminGetOperatorQueryBuilder {
  private instance: AdminGetOperatorQuery;

  constructor() {
    this.instance = new AdminGetOperatorQuery();
  }

  id(value: string): AdminGetOperatorQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetOperatorsQuery {
  forename: string;
  surname: string;
  phone: string;
  enabled: boolean;
  cashboxId: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetOperatorsQueryBuilder {
    return new AdminGetOperatorsQueryBuilder();
  }
}

class AdminGetOperatorsQueryBuilder {
  private instance: AdminGetOperatorsQuery;

  constructor() {
    this.instance = new AdminGetOperatorsQuery();
  }

  forename(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.forename = value;
    return this;
  }

  surname(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.surname = value;
    return this;
  }

  phone(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  enabled(value: boolean): AdminGetOperatorsQueryBuilder {
    this.instance.enabled = value;
    return this;
  }

  cashboxId(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  page(value: Page): AdminGetOperatorsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetOperatorsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetProviderLabelQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetProviderLabelQueryBuilder {
    return new AdminGetProviderLabelQueryBuilder();
  }
}

class AdminGetProviderLabelQueryBuilder {
  private instance: AdminGetProviderLabelQuery;

  constructor() {
    this.instance = new AdminGetProviderLabelQuery();
  }

  id(value: string): AdminGetProviderLabelQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetProviderLabelsQuery {
  name: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetProviderLabelsQueryBuilder {
    return new AdminGetProviderLabelsQueryBuilder();
  }
}

class AdminGetProviderLabelsQueryBuilder {
  private instance: AdminGetProviderLabelsQuery;

  constructor() {
    this.instance = new AdminGetProviderLabelsQuery();
  }

  name(value: string): AdminGetProviderLabelsQueryBuilder {
    this.instance.name = value;
    return this;
  }

  page(value: Page): AdminGetProviderLabelsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetProviderLabelsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminSaveCommissionCommand {
  providerId: string;
  policyId: string;
  ranges: Array<CommissionRange>;

  constructor() {
  }

  static build(): AdminSaveCommissionCommandBuilder {
    return new AdminSaveCommissionCommandBuilder();
  }
}

class AdminSaveCommissionCommandBuilder {
  private instance: AdminSaveCommissionCommand;

  constructor() {
    this.instance = new AdminSaveCommissionCommand();
  }

  providerId(value: string): AdminSaveCommissionCommandBuilder {
    this.instance.providerId = value;
    return this;
  }

  policyId(value: string): AdminSaveCommissionCommandBuilder {
    this.instance.policyId = value;
    return this;
  }

  ranges(value: Array<CommissionRange>): AdminSaveCommissionCommandBuilder {
    this.instance.ranges = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDeleteCommissionCommand {
  providerId: string;
  policyId: string;

  constructor() {
  }

  static build(): AdminDeleteCommissionCommandBuilder {
    return new AdminDeleteCommissionCommandBuilder();
  }
}

class AdminDeleteCommissionCommandBuilder {
  private instance: AdminDeleteCommissionCommand;

  constructor() {
    this.instance = new AdminDeleteCommissionCommand();
  }

  providerId(value: string): AdminDeleteCommissionCommandBuilder {
    this.instance.providerId = value;
    return this;
  }

  policyId(value: string): AdminDeleteCommissionCommandBuilder {
    this.instance.policyId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCommissionQuery {
  providerId: string;
  policyId: string;
  enabled: boolean;

  constructor() {
  }

  static build(): AdminGetCommissionQueryBuilder {
    return new AdminGetCommissionQueryBuilder();
  }
}

class AdminGetCommissionQueryBuilder {
  private instance: AdminGetCommissionQuery;

  constructor() {
    this.instance = new AdminGetCommissionQuery();
  }

  providerId(value: string): AdminGetCommissionQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  policyId(value: string): AdminGetCommissionQueryBuilder {
    this.instance.policyId = value;
    return this;
  }

  enabled(value: boolean): AdminGetCommissionQueryBuilder {
    this.instance.enabled = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCommissionsQuery {
  providerId: string;
  policyId: string;
  approved: boolean;
  action: boolean;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetCommissionsQueryBuilder {
    return new AdminGetCommissionsQueryBuilder();
  }
}

class AdminGetCommissionsQueryBuilder {
  private instance: AdminGetCommissionsQuery;

  constructor() {
    this.instance = new AdminGetCommissionsQuery();
  }

  providerId(value: string): AdminGetCommissionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  policyId(value: string): AdminGetCommissionsQueryBuilder {
    this.instance.policyId = value;
    return this;
  }

  approved(value: boolean): AdminGetCommissionsQueryBuilder {
    this.instance.approved = value;
    return this;
  }

  action(value: boolean): AdminGetCommissionsQueryBuilder {
    this.instance.action = value;
    return this;
  }

  page(value: Page): AdminGetCommissionsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetCommissionsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminCreateCommissionPolicyCommand {
  id: string;
  name: string;

  constructor() {
  }

  static build(): AdminCreateCommissionPolicyCommandBuilder {
    return new AdminCreateCommissionPolicyCommandBuilder();
  }
}

class AdminCreateCommissionPolicyCommandBuilder {
  private instance: AdminCreateCommissionPolicyCommand;

  constructor() {
    this.instance = new AdminCreateCommissionPolicyCommand();
  }

  id(value: string): AdminCreateCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminCreateCommissionPolicyCommandBuilder {
    this.instance.name = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminUpdateCommissionPolicyCommand {
  id: string;
  name: string;

  constructor() {
  }

  static build(): AdminUpdateCommissionPolicyCommandBuilder {
    return new AdminUpdateCommissionPolicyCommandBuilder();
  }
}

class AdminUpdateCommissionPolicyCommandBuilder {
  private instance: AdminUpdateCommissionPolicyCommand;

  constructor() {
    this.instance = new AdminUpdateCommissionPolicyCommand();
  }

  id(value: string): AdminUpdateCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): AdminUpdateCommissionPolicyCommandBuilder {
    this.instance.name = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminDeleteCommissionPolicyCommand {
  id: string;

  constructor() {
  }

  static build(): AdminDeleteCommissionPolicyCommandBuilder {
    return new AdminDeleteCommissionPolicyCommandBuilder();
  }
}

class AdminDeleteCommissionPolicyCommandBuilder {
  private instance: AdminDeleteCommissionPolicyCommand;

  constructor() {
    this.instance = new AdminDeleteCommissionPolicyCommand();
  }

  id(value: string): AdminDeleteCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCommissionPolicyQuery {
  id: string;

  constructor() {
  }

  static build(): AdminGetCommissionPolicyQueryBuilder {
    return new AdminGetCommissionPolicyQueryBuilder();
  }
}

class AdminGetCommissionPolicyQueryBuilder {
  private instance: AdminGetCommissionPolicyQuery;

  constructor() {
    this.instance = new AdminGetCommissionPolicyQuery();
  }

  id(value: string): AdminGetCommissionPolicyQueryBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetCommissionPoliciesQuery {
  name: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): AdminGetCommissionPoliciesQueryBuilder {
    return new AdminGetCommissionPoliciesQueryBuilder();
  }
}

class AdminGetCommissionPoliciesQueryBuilder {
  private instance: AdminGetCommissionPoliciesQuery;

  constructor() {
    this.instance = new AdminGetCommissionPoliciesQuery();
  }

  name(value: string): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.name = value;
    return this;
  }

  page(value: Page): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminApproveCommissionCommand {
  id: string;

  constructor() {
  }

  static build(): AdminApproveCommissionCommandBuilder {
    return new AdminApproveCommissionCommandBuilder();
  }
}

class AdminApproveCommissionCommandBuilder {
  private instance: AdminApproveCommissionCommand;

  constructor() {
    this.instance = new AdminApproveCommissionCommand();
  }

  id(value: string): AdminApproveCommissionCommandBuilder {
    this.instance.id = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetBranchCommissionQuery {
  branchId: string;
  providerId: string;

  constructor() {
  }

  static build(): AdminGetBranchCommissionQueryBuilder {
    return new AdminGetBranchCommissionQueryBuilder();
  }
}

class AdminGetBranchCommissionQueryBuilder {
  private instance: AdminGetBranchCommissionQuery;

  constructor() {
    this.instance = new AdminGetBranchCommissionQuery();
  }

  branchId(value: string): AdminGetBranchCommissionQueryBuilder {
    this.instance.branchId = value;
    return this;
  }

  providerId(value: string): AdminGetBranchCommissionQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminGetOperatorScopesQuery {
  operatorId: string;

  constructor() {
  }

  static build(): AdminGetOperatorScopesQueryBuilder {
    return new AdminGetOperatorScopesQueryBuilder();
  }
}

class AdminGetOperatorScopesQueryBuilder {
  private instance: AdminGetOperatorScopesQuery;

  constructor() {
    this.instance = new AdminGetOperatorScopesQuery();
  }

  operatorId(value: string): AdminGetOperatorScopesQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersSignUpCommand {
  regId: string;
  password: string;
  phone: string;

  constructor() {
  }

  static build(): CustomersSignUpCommandBuilder {
    return new CustomersSignUpCommandBuilder();
  }
}

class CustomersSignUpCommandBuilder {
  private instance: CustomersSignUpCommand;

  constructor() {
    this.instance = new CustomersSignUpCommand();
  }

  regId(value: string): CustomersSignUpCommandBuilder {
    this.instance.regId = value;
    return this;
  }

  password(value: string): CustomersSignUpCommandBuilder {
    this.instance.password = value;
    return this;
  }

  phone(value: string): CustomersSignUpCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersSendActivationCodeCommand {
  regId: string;

  constructor() {
  }

  static build(): CustomersSendActivationCodeCommandBuilder {
    return new CustomersSendActivationCodeCommandBuilder();
  }
}

class CustomersSendActivationCodeCommandBuilder {
  private instance: CustomersSendActivationCodeCommand;

  constructor() {
    this.instance = new CustomersSendActivationCodeCommand();
  }

  regId(value: string): CustomersSendActivationCodeCommandBuilder {
    this.instance.regId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersActivateCommand {
  regId: string;
  pincode: string;

  constructor() {
  }

  static build(): CustomersActivateCommandBuilder {
    return new CustomersActivateCommandBuilder();
  }
}

class CustomersActivateCommandBuilder {
  private instance: CustomersActivateCommand;

  constructor() {
    this.instance = new CustomersActivateCommand();
  }

  regId(value: string): CustomersActivateCommandBuilder {
    this.instance.regId = value;
    return this;
  }

  pincode(value: string): CustomersActivateCommandBuilder {
    this.instance.pincode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersAuthenticateCommand {
  authenticationId: string;
  phone: string;
  password: string;

  constructor() {
  }

  static build(): CustomersAuthenticateCommandBuilder {
    return new CustomersAuthenticateCommandBuilder();
  }
}

class CustomersAuthenticateCommandBuilder {
  private instance: CustomersAuthenticateCommand;

  constructor() {
    this.instance = new CustomersAuthenticateCommand();
  }

  authenticationId(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  phone(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  password(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetAccessTokenQuery {
  authenticationId: string;

  constructor() {
  }

  static build(): CustomersGetAccessTokenQueryBuilder {
    return new CustomersGetAccessTokenQueryBuilder();
  }
}

class CustomersGetAccessTokenQueryBuilder {
  private instance: CustomersGetAccessTokenQuery;

  constructor() {
    this.instance = new CustomersGetAccessTokenQuery();
  }

  authenticationId(value: string): CustomersGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersForgotPasswordCommand {
  identity: string;
  phone: string;

  constructor() {
  }

  static build(): CustomersForgotPasswordCommandBuilder {
    return new CustomersForgotPasswordCommandBuilder();
  }
}

class CustomersForgotPasswordCommandBuilder {
  private instance: CustomersForgotPasswordCommand;

  constructor() {
    this.instance = new CustomersForgotPasswordCommand();
  }

  identity(value: string): CustomersForgotPasswordCommandBuilder {
    this.instance.identity = value;
    return this;
  }

  phone(value: string): CustomersForgotPasswordCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersConfirmCodeCommand {
  identity: string;
  code: string;

  constructor() {
  }

  static build(): CustomersConfirmCodeCommandBuilder {
    return new CustomersConfirmCodeCommandBuilder();
  }
}

class CustomersConfirmCodeCommandBuilder {
  private instance: CustomersConfirmCodeCommand;

  constructor() {
    this.instance = new CustomersConfirmCodeCommand();
  }

  identity(value: string): CustomersConfirmCodeCommandBuilder {
    this.instance.identity = value;
    return this;
  }

  code(value: string): CustomersConfirmCodeCommandBuilder {
    this.instance.code = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersResetPasswordCommand {
  identity: string;
  newPass: string;

  constructor() {
  }

  static build(): CustomersResetPasswordCommandBuilder {
    return new CustomersResetPasswordCommandBuilder();
  }
}

class CustomersResetPasswordCommandBuilder {
  private instance: CustomersResetPasswordCommand;

  constructor() {
    this.instance = new CustomersResetPasswordCommand();
  }

  identity(value: string): CustomersResetPasswordCommandBuilder {
    this.instance.identity = value;
    return this;
  }

  newPass(value: string): CustomersResetPasswordCommandBuilder {
    this.instance.newPass = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersSetDataCommand {
  key: string;
  value: string;

  constructor() {
  }

  static build(): CustomersSetDataCommandBuilder {
    return new CustomersSetDataCommandBuilder();
  }
}

class CustomersSetDataCommandBuilder {
  private instance: CustomersSetDataCommand;

  constructor() {
    this.instance = new CustomersSetDataCommand();
  }

  key(value: string): CustomersSetDataCommandBuilder {
    this.instance.key = value;
    return this;
  }

  value(value: string): CustomersSetDataCommandBuilder {
    this.instance.value = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetDataQuery {
  key: string;

  constructor() {
  }

  static build(): CustomersGetDataQueryBuilder {
    return new CustomersGetDataQueryBuilder();
  }
}

class CustomersGetDataQueryBuilder {
  private instance: CustomersGetDataQuery;

  constructor() {
    this.instance = new CustomersGetDataQuery();
  }

  key(value: string): CustomersGetDataQueryBuilder {
    this.instance.key = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersSendCodeCommand {

  constructor() {
  }

  static build(): CustomersSendCodeCommandBuilder {
    return new CustomersSendCodeCommandBuilder();
  }
}

class CustomersSendCodeCommandBuilder {
  private instance: CustomersSendCodeCommand;

  constructor() {
    this.instance = new CustomersSendCodeCommand();
  }

  get() {
    return this.instance;
  }
}

export class CustomersConfirmCommand {
  code: string;

  constructor() {
  }

  static build(): CustomersConfirmCommandBuilder {
    return new CustomersConfirmCommandBuilder();
  }
}

class CustomersConfirmCommandBuilder {
  private instance: CustomersConfirmCommand;

  constructor() {
    this.instance = new CustomersConfirmCommand();
  }

  code(value: string): CustomersConfirmCommandBuilder {
    this.instance.code = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersCreateGroupCommand {
  name: string;
  customerId: string;
  description: string;

  constructor() {
  }

  static build(): CustomersCreateGroupCommandBuilder {
    return new CustomersCreateGroupCommandBuilder();
  }
}

class CustomersCreateGroupCommandBuilder {
  private instance: CustomersCreateGroupCommand;

  constructor() {
    this.instance = new CustomersCreateGroupCommand();
  }

  name(value: string): CustomersCreateGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }

  customerId(value: string): CustomersCreateGroupCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  description(value: string): CustomersCreateGroupCommandBuilder {
    this.instance.description = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersDeleteGroupCommand {
  name: string;

  constructor() {
  }

  static build(): CustomersDeleteGroupCommandBuilder {
    return new CustomersDeleteGroupCommandBuilder();
  }
}

class CustomersDeleteGroupCommandBuilder {
  private instance: CustomersDeleteGroupCommand;

  constructor() {
    this.instance = new CustomersDeleteGroupCommand();
  }

  name(value: string): CustomersDeleteGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersAddContractCommand {
  groupId: string;
  contract: Contract;

  constructor() {
  }

  static build(): CustomersAddContractCommandBuilder {
    return new CustomersAddContractCommandBuilder();
  }
}

class CustomersAddContractCommandBuilder {
  private instance: CustomersAddContractCommand;

  constructor() {
    this.instance = new CustomersAddContractCommand();
  }

  groupId(value: string): CustomersAddContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  contract(value: Contract): CustomersAddContractCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersRemoveContractCommand {
  groupId: string;
  contract: Contract;

  constructor() {
  }

  static build(): CustomersRemoveContractCommandBuilder {
    return new CustomersRemoveContractCommandBuilder();
  }
}

class CustomersRemoveContractCommandBuilder {
  private instance: CustomersRemoveContractCommand;

  constructor() {
    this.instance = new CustomersRemoveContractCommand();
  }

  groupId(value: string): CustomersRemoveContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  contract(value: Contract): CustomersRemoveContractCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersAddCardCommand {
  card: Card;

  constructor() {
  }

  static build(): CustomersAddCardCommandBuilder {
    return new CustomersAddCardCommandBuilder();
  }
}

class CustomersAddCardCommandBuilder {
  private instance: CustomersAddCardCommand;

  constructor() {
    this.instance = new CustomersAddCardCommand();
  }

  card(value: Card): CustomersAddCardCommandBuilder {
    this.instance.card = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersRemoveCardCommand {
  cardId: string;

  constructor() {
  }

  static build(): CustomersRemoveCardCommandBuilder {
    return new CustomersRemoveCardCommandBuilder();
  }
}

class CustomersRemoveCardCommandBuilder {
  private instance: CustomersRemoveCardCommand;

  constructor() {
    this.instance = new CustomersRemoveCardCommand();
  }

  cardId(value: string): CustomersRemoveCardCommandBuilder {
    this.instance.cardId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersAddCustomerContractCommand {
  customer: string;
  groupId: string;
  contract: Contract;

  constructor() {
  }

  static build(): CustomersAddCustomerContractCommandBuilder {
    return new CustomersAddCustomerContractCommandBuilder();
  }
}

class CustomersAddCustomerContractCommandBuilder {
  private instance: CustomersAddCustomerContractCommand;

  constructor() {
    this.instance = new CustomersAddCustomerContractCommand();
  }

  customer(value: string): CustomersAddCustomerContractCommandBuilder {
    this.instance.customer = value;
    return this;
  }

  groupId(value: string): CustomersAddCustomerContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  contract(value: Contract): CustomersAddCustomerContractCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersRemoveCustomerContractCommand {
  customer: string;
  groupId: string;
  identity: string;

  constructor() {
  }

  static build(): CustomersRemoveCustomerContractCommandBuilder {
    return new CustomersRemoveCustomerContractCommandBuilder();
  }
}

class CustomersRemoveCustomerContractCommandBuilder {
  private instance: CustomersRemoveCustomerContractCommand;

  constructor() {
    this.instance = new CustomersRemoveCustomerContractCommand();
  }

  customer(value: string): CustomersRemoveCustomerContractCommandBuilder {
    this.instance.customer = value;
    return this;
  }

  groupId(value: string): CustomersRemoveCustomerContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  identity(value: string): CustomersRemoveCustomerContractCommandBuilder {
    this.instance.identity = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersAddFavoriteCommand {
  customerId: string;
  contract: Contract;

  constructor() {
  }

  static build(): CustomersAddFavoriteCommandBuilder {
    return new CustomersAddFavoriteCommandBuilder();
  }
}

class CustomersAddFavoriteCommandBuilder {
  private instance: CustomersAddFavoriteCommand;

  constructor() {
    this.instance = new CustomersAddFavoriteCommand();
  }

  customerId(value: string): CustomersAddFavoriteCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  contract(value: Contract): CustomersAddFavoriteCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersRemoveFavoriteCommand {
  customerId: string;
  contract: Contract;

  constructor() {
  }

  static build(): CustomersRemoveFavoriteCommandBuilder {
    return new CustomersRemoveFavoriteCommandBuilder();
  }
}

class CustomersRemoveFavoriteCommandBuilder {
  private instance: CustomersRemoveFavoriteCommand;

  constructor() {
    this.instance = new CustomersRemoveFavoriteCommand();
  }

  customerId(value: string): CustomersRemoveFavoriteCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  contract(value: Contract): CustomersRemoveFavoriteCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersFindCustomerByEmailQuery {
  email: string;

  constructor() {
  }

  static build(): CustomersFindCustomerByEmailQueryBuilder {
    return new CustomersFindCustomerByEmailQueryBuilder();
  }
}

class CustomersFindCustomerByEmailQueryBuilder {
  private instance: CustomersFindCustomerByEmailQuery;

  constructor() {
    this.instance = new CustomersFindCustomerByEmailQuery();
  }

  email(value: string): CustomersFindCustomerByEmailQueryBuilder {
    this.instance.email = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetProfileQuery {
  customerId: string;

  constructor() {
  }

  static build(): CustomersGetProfileQueryBuilder {
    return new CustomersGetProfileQueryBuilder();
  }
}

class CustomersGetProfileQueryBuilder {
  private instance: CustomersGetProfileQuery;

  constructor() {
    this.instance = new CustomersGetProfileQuery();
  }

  customerId(value: string): CustomersGetProfileQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetTransactionsQuery {
  customerId: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): CustomersGetTransactionsQueryBuilder {
    return new CustomersGetTransactionsQueryBuilder();
  }
}

class CustomersGetTransactionsQueryBuilder {
  private instance: CustomersGetTransactionsQuery;

  constructor() {
    this.instance = new CustomersGetTransactionsQuery();
  }

  customerId(value: string): CustomersGetTransactionsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  page(value: Page): CustomersGetTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): CustomersGetTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetFavoritesQuery {
  customerId: string;

  constructor() {
  }

  static build(): CustomersGetFavoritesQueryBuilder {
    return new CustomersGetFavoritesQueryBuilder();
  }
}

class CustomersGetFavoritesQueryBuilder {
  private instance: CustomersGetFavoritesQuery;

  constructor() {
    this.instance = new CustomersGetFavoritesQuery();
  }

  customerId(value: string): CustomersGetFavoritesQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CustomersGetCardsQuery {

  constructor() {
  }

  static build(): CustomersGetCardsQueryBuilder {
    return new CustomersGetCardsQueryBuilder();
  }
}

class CustomersGetCardsQueryBuilder {
  private instance: CustomersGetCardsQuery;

  constructor() {
    this.instance = new CustomersGetCardsQuery();
  }

  get() {
    return this.instance;
  }
}

export class CustomersFindGroupQuery {
  name: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): CustomersFindGroupQueryBuilder {
    return new CustomersFindGroupQueryBuilder();
  }
}

class CustomersFindGroupQueryBuilder {
  private instance: CustomersFindGroupQuery;

  constructor() {
    this.instance = new CustomersFindGroupQuery();
  }

  name(value: string): CustomersFindGroupQueryBuilder {
    this.instance.name = value;
    return this;
  }

  page(value: Page): CustomersFindGroupQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): CustomersFindGroupQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorAuthenticateCommand {
  authenticationId: string;
  cashboxId: string;
  userName: string;
  password: string;

  constructor() {
  }

  static build(): OperatorAuthenticateCommandBuilder {
    return new OperatorAuthenticateCommandBuilder();
  }
}

class OperatorAuthenticateCommandBuilder {
  private instance: OperatorAuthenticateCommand;

  constructor() {
    this.instance = new OperatorAuthenticateCommand();
  }

  authenticationId(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  cashboxId(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  userName(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.userName = value;
    return this;
  }

  password(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCreateGroupCommand {
  id: string;
  name: string;
  description: string;

  constructor() {
  }

  static build(): OperatorCreateGroupCommandBuilder {
    return new OperatorCreateGroupCommandBuilder();
  }
}

class OperatorCreateGroupCommandBuilder {
  private instance: OperatorCreateGroupCommand;

  constructor() {
    this.instance = new OperatorCreateGroupCommand();
  }

  id(value: string): OperatorCreateGroupCommandBuilder {
    this.instance.id = value;
    return this;
  }

  name(value: string): OperatorCreateGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }

  description(value: string): OperatorCreateGroupCommandBuilder {
    this.instance.description = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorDeleteGroupCommand {
  groupId: string;

  constructor() {
  }

  static build(): OperatorDeleteGroupCommandBuilder {
    return new OperatorDeleteGroupCommandBuilder();
  }
}

class OperatorDeleteGroupCommandBuilder {
  private instance: OperatorDeleteGroupCommand;

  constructor() {
    this.instance = new OperatorDeleteGroupCommand();
  }

  groupId(value: string): OperatorDeleteGroupCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCreateTokenCommand {
  cashbox: string;
  email: string;
  password: string;

  constructor() {
  }

  static build(): OperatorCreateTokenCommandBuilder {
    return new OperatorCreateTokenCommandBuilder();
  }
}

class OperatorCreateTokenCommandBuilder {
  private instance: OperatorCreateTokenCommand;

  constructor() {
    this.instance = new OperatorCreateTokenCommand();
  }

  cashbox(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.cashbox = value;
    return this;
  }

  email(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.email = value;
    return this;
  }

  password(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.password = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorChangePasswordCommand {
  email: string;
  oldPass: string;
  newPass: string;

  constructor() {
  }

  static build(): OperatorChangePasswordCommandBuilder {
    return new OperatorChangePasswordCommandBuilder();
  }
}

class OperatorChangePasswordCommandBuilder {
  private instance: OperatorChangePasswordCommand;

  constructor() {
    this.instance = new OperatorChangePasswordCommand();
  }

  email(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.email = value;
    return this;
  }

  oldPass(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.oldPass = value;
    return this;
  }

  newPass(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.newPass = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorAddContractCommand {
  groupId: string;
  contract: Contract;

  constructor() {
  }

  static build(): OperatorAddContractCommandBuilder {
    return new OperatorAddContractCommandBuilder();
  }
}

class OperatorAddContractCommandBuilder {
  private instance: OperatorAddContractCommand;

  constructor() {
    this.instance = new OperatorAddContractCommand();
  }

  groupId(value: string): OperatorAddContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  contract(value: Contract): OperatorAddContractCommandBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorRemoveContractCommand {
  groupId: string;
  identity: string;

  constructor() {
  }

  static build(): OperatorRemoveContractCommandBuilder {
    return new OperatorRemoveContractCommandBuilder();
  }
}

class OperatorRemoveContractCommandBuilder {
  private instance: OperatorRemoveContractCommand;

  constructor() {
    this.instance = new OperatorRemoveContractCommand();
  }

  groupId(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.groupId = value;
    return this;
  }

  identity(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.identity = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCashInCommand {
  customer: string;
  amount: number;

  constructor() {
  }

  static build(): OperatorCashInCommandBuilder {
    return new OperatorCashInCommandBuilder();
  }
}

class OperatorCashInCommandBuilder {
  private instance: OperatorCashInCommand;

  constructor() {
    this.instance = new OperatorCashInCommand();
  }

  customer(value: string): OperatorCashInCommandBuilder {
    this.instance.customer = value;
    return this;
  }

  amount(value: number): OperatorCashInCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCashOutCommand {
  customer: string;
  amount: number;

  constructor() {
  }

  static build(): OperatorCashOutCommandBuilder {
    return new OperatorCashOutCommandBuilder();
  }
}

class OperatorCashOutCommandBuilder {
  private instance: OperatorCashOutCommand;

  constructor() {
    this.instance = new OperatorCashOutCommand();
  }

  customer(value: string): OperatorCashOutCommandBuilder {
    this.instance.customer = value;
    return this;
  }

  amount(value: number): OperatorCashOutCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCloseCashboxCommand {
  cashboxId: string;
  cashIn: number;
  cashOut: number;
  incassation: number;
  finalBalance: number;

  constructor() {
  }

  static build(): OperatorCloseCashboxCommandBuilder {
    return new OperatorCloseCashboxCommandBuilder();
  }
}

class OperatorCloseCashboxCommandBuilder {
  private instance: OperatorCloseCashboxCommand;

  constructor() {
    this.instance = new OperatorCloseCashboxCommand();
  }

  cashboxId(value: string): OperatorCloseCashboxCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  cashIn(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.cashIn = value;
    return this;
  }

  cashOut(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.cashOut = value;
    return this;
  }

  incassation(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.incassation = value;
    return this;
  }

  finalBalance(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.finalBalance = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorAddToBasketCommand {
  providerId: string;
  contractIdentity: string;

  constructor() {
  }

  static build(): OperatorAddToBasketCommandBuilder {
    return new OperatorAddToBasketCommandBuilder();
  }
}

class OperatorAddToBasketCommandBuilder {
  private instance: OperatorAddToBasketCommand;

  constructor() {
    this.instance = new OperatorAddToBasketCommand();
  }

  providerId(value: string): OperatorAddToBasketCommandBuilder {
    this.instance.providerId = value;
    return this;
  }

  contractIdentity(value: string): OperatorAddToBasketCommandBuilder {
    this.instance.contractIdentity = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorDeleteFromBasketCommand {
  basketId: string;

  constructor() {
  }

  static build(): OperatorDeleteFromBasketCommandBuilder {
    return new OperatorDeleteFromBasketCommandBuilder();
  }
}

class OperatorDeleteFromBasketCommandBuilder {
  private instance: OperatorDeleteFromBasketCommand;

  constructor() {
    this.instance = new OperatorDeleteFromBasketCommand();
  }

  basketId(value: string): OperatorDeleteFromBasketCommandBuilder {
    this.instance.basketId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCashboxPayCommand {
  cashboxPaymentId: string;
  totalAmount: number;
  serviceQuantity: number;
  inAmount: number;
  cash: boolean;

  constructor() {
  }

  static build(): OperatorCashboxPayCommandBuilder {
    return new OperatorCashboxPayCommandBuilder();
  }
}

class OperatorCashboxPayCommandBuilder {
  private instance: OperatorCashboxPayCommand;

  constructor() {
    this.instance = new OperatorCashboxPayCommand();
  }

  cashboxPaymentId(value: string): OperatorCashboxPayCommandBuilder {
    this.instance.cashboxPaymentId = value;
    return this;
  }

  totalAmount(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.totalAmount = value;
    return this;
  }

  serviceQuantity(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.serviceQuantity = value;
    return this;
  }

  inAmount(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.inAmount = value;
    return this;
  }

  cash(value: boolean): OperatorCashboxPayCommandBuilder {
    this.instance.cash = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCashboxInCommand {
  amount: number;
  type: EncashmentType;
  details: string;

  constructor() {
  }

  static build(): OperatorCashboxInCommandBuilder {
    return new OperatorCashboxInCommandBuilder();
  }
}

class OperatorCashboxInCommandBuilder {
  private instance: OperatorCashboxInCommand;

  constructor() {
    this.instance = new OperatorCashboxInCommand();
  }

  amount(value: number): OperatorCashboxInCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  type(value: EncashmentType): OperatorCashboxInCommandBuilder {
    this.instance.type = value;
    return this;
  }

  details(value: string): OperatorCashboxInCommandBuilder {
    this.instance.details = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorCashboxOutCommand {
  amount: number;
  type: EncashmentType;
  details: string;

  constructor() {
  }

  static build(): OperatorCashboxOutCommandBuilder {
    return new OperatorCashboxOutCommandBuilder();
  }
}

class OperatorCashboxOutCommandBuilder {
  private instance: OperatorCashboxOutCommand;

  constructor() {
    this.instance = new OperatorCashboxOutCommand();
  }

  amount(value: number): OperatorCashboxOutCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  type(value: EncashmentType): OperatorCashboxOutCommandBuilder {
    this.instance.type = value;
    return this;
  }

  details(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.details = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorArmsoftImportCommand {

  constructor() {
  }

  static build(): OperatorArmsoftImportCommandBuilder {
    return new OperatorArmsoftImportCommandBuilder();
  }
}

class OperatorArmsoftImportCommandBuilder {
  private instance: OperatorArmsoftImportCommand;

  constructor() {
    this.instance = new OperatorArmsoftImportCommand();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGenerateCashboxReportCommand {

  constructor() {
  }

  static build(): OperatorGenerateCashboxReportCommandBuilder {
    return new OperatorGenerateCashboxReportCommandBuilder();
  }
}

class OperatorGenerateCashboxReportCommandBuilder {
  private instance: OperatorGenerateCashboxReportCommand;

  constructor() {
    this.instance = new OperatorGenerateCashboxReportCommand();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetAccessTokenQuery {
  authenticationId: string;

  constructor() {
  }

  static build(): OperatorGetAccessTokenQueryBuilder {
    return new OperatorGetAccessTokenQueryBuilder();
  }
}

class OperatorGetAccessTokenQueryBuilder {
  private instance: OperatorGetAccessTokenQuery;

  constructor() {
    this.instance = new OperatorGetAccessTokenQuery();
  }

  authenticationId(value: string): OperatorGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorVerifyTokenQuery {
  token: string;

  constructor() {
  }

  static build(): OperatorVerifyTokenQueryBuilder {
    return new OperatorVerifyTokenQueryBuilder();
  }
}

class OperatorVerifyTokenQueryBuilder {
  private instance: OperatorVerifyTokenQuery;

  constructor() {
    this.instance = new OperatorVerifyTokenQuery();
  }

  token(value: string): OperatorVerifyTokenQueryBuilder {
    this.instance.token = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorFindGroupQuery {
  name: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): OperatorFindGroupQueryBuilder {
    return new OperatorFindGroupQueryBuilder();
  }
}

class OperatorFindGroupQueryBuilder {
  private instance: OperatorFindGroupQuery;

  constructor() {
    this.instance = new OperatorFindGroupQuery();
  }

  name(value: string): OperatorFindGroupQueryBuilder {
    this.instance.name = value;
    return this;
  }

  page(value: Page): OperatorFindGroupQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): OperatorFindGroupQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetCashboxBalanceQuery {
  cashboxId: string;

  constructor() {
  }

  static build(): OperatorGetCashboxBalanceQueryBuilder {
    return new OperatorGetCashboxBalanceQueryBuilder();
  }
}

class OperatorGetCashboxBalanceQueryBuilder {
  private instance: OperatorGetCashboxBalanceQuery;

  constructor() {
    this.instance = new OperatorGetCashboxBalanceQuery();
  }

  cashboxId(value: string): OperatorGetCashboxBalanceQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetCashboxTransactionsQuery {
  cashboxId: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): OperatorGetCashboxTransactionsQueryBuilder {
    return new OperatorGetCashboxTransactionsQueryBuilder();
  }
}

class OperatorGetCashboxTransactionsQueryBuilder {
  private instance: OperatorGetCashboxTransactionsQuery;

  constructor() {
    this.instance = new OperatorGetCashboxTransactionsQuery();
  }

  cashboxId(value: string): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }

  page(value: Page): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetTransactionsByPaymentQuery {
  paymentId: string;
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): OperatorGetTransactionsByPaymentQueryBuilder {
    return new OperatorGetTransactionsByPaymentQueryBuilder();
  }
}

class OperatorGetTransactionsByPaymentQueryBuilder {
  private instance: OperatorGetTransactionsByPaymentQuery;

  constructor() {
    this.instance = new OperatorGetTransactionsByPaymentQuery();
  }

  paymentId(value: string): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.paymentId = value;
    return this;
  }

  page(value: Page): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetTransactionQuery {
  transactionId: string;

  constructor() {
  }

  static build(): OperatorGetTransactionQueryBuilder {
    return new OperatorGetTransactionQueryBuilder();
  }
}

class OperatorGetTransactionQueryBuilder {
  private instance: OperatorGetTransactionQuery;

  constructor() {
    this.instance = new OperatorGetTransactionQuery();
  }

  transactionId(value: string): OperatorGetTransactionQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorgetCashboxLogsQuery {
  page: Page;
  order: Order;

  constructor() {
  }

  static build(): OperatorgetCashboxLogsQueryBuilder {
    return new OperatorgetCashboxLogsQueryBuilder();
  }
}

class OperatorgetCashboxLogsQueryBuilder {
  private instance: OperatorgetCashboxLogsQuery;

  constructor() {
    this.instance = new OperatorgetCashboxLogsQuery();
  }

  page(value: Page): OperatorgetCashboxLogsQueryBuilder {
    this.instance.page = value;
    return this;
  }

  order(value: Order): OperatorgetCashboxLogsQueryBuilder {
    this.instance.order = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetCurrentBranchQuery {

  constructor() {
  }

  static build(): OperatorGetCurrentBranchQueryBuilder {
    return new OperatorGetCurrentBranchQueryBuilder();
  }
}

class OperatorGetCurrentBranchQueryBuilder {
  private instance: OperatorGetCurrentBranchQuery;

  constructor() {
    this.instance = new OperatorGetCurrentBranchQuery();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetProviderDescriptorsQuery {
  providerId: string;

  constructor() {
  }

  static build(): OperatorGetProviderDescriptorsQueryBuilder {
    return new OperatorGetProviderDescriptorsQueryBuilder();
  }
}

class OperatorGetProviderDescriptorsQueryBuilder {
  private instance: OperatorGetProviderDescriptorsQuery;

  constructor() {
    this.instance = new OperatorGetProviderDescriptorsQuery();
  }

  providerId(value: string): OperatorGetProviderDescriptorsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetProfileQuery {

  constructor() {
  }

  static build(): OperatorGetProfileQueryBuilder {
    return new OperatorGetProfileQueryBuilder();
  }
}

class OperatorGetProfileQueryBuilder {
  private instance: OperatorGetProfileQuery;

  constructor() {
    this.instance = new OperatorGetProfileQuery();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetBasketItemsCountQuery {

  constructor() {
  }

  static build(): OperatorGetBasketItemsCountQueryBuilder {
    return new OperatorGetBasketItemsCountQueryBuilder();
  }
}

class OperatorGetBasketItemsCountQueryBuilder {
  private instance: OperatorGetBasketItemsCountQuery;

  constructor() {
    this.instance = new OperatorGetBasketItemsCountQuery();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetBasketItemsQuery {

  constructor() {
  }

  static build(): OperatorGetBasketItemsQueryBuilder {
    return new OperatorGetBasketItemsQueryBuilder();
  }
}

class OperatorGetBasketItemsQueryBuilder {
  private instance: OperatorGetBasketItemsQuery;

  constructor() {
    this.instance = new OperatorGetBasketItemsQuery();
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetDebtQuery {
  providerId: string;
  contract: Contract;

  constructor() {
  }

  static build(): OperatorGetDebtQueryBuilder {
    return new OperatorGetDebtQueryBuilder();
  }
}

class OperatorGetDebtQueryBuilder {
  private instance: OperatorGetDebtQuery;

  constructor() {
    this.instance = new OperatorGetDebtQuery();
  }

  providerId(value: string): OperatorGetDebtQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  contract(value: Contract): OperatorGetDebtQueryBuilder {
    this.instance.contract = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorNum2TextQuery {
  number: number;

  constructor() {
  }

  static build(): OperatorNum2TextQueryBuilder {
    return new OperatorNum2TextQueryBuilder();
  }
}

class OperatorNum2TextQueryBuilder {
  private instance: OperatorNum2TextQuery;

  constructor() {
    this.instance = new OperatorNum2TextQuery();
  }

  number(value: number): OperatorNum2TextQueryBuilder {
    this.instance.number = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class OperatorGetProviderCommissionsQuery {
  branchId: string;
  providerId: string;

  constructor() {
  }

  static build(): OperatorGetProviderCommissionsQueryBuilder {
    return new OperatorGetProviderCommissionsQueryBuilder();
  }
}

class OperatorGetProviderCommissionsQueryBuilder {
  private instance: OperatorGetProviderCommissionsQuery;

  constructor() {
    this.instance = new OperatorGetProviderCommissionsQuery();
  }

  branchId(value: string): OperatorGetProviderCommissionsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }

  providerId(value: string): OperatorGetProviderCommissionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayCommand {
  transactionId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayCommandBuilder {
    return new CashPayCommandBuilder();
  }
}

class CashPayCommandBuilder {
  private instance: CashPayCommand;

  constructor() {
    this.instance = new CashPayCommand();
  }

  transactionId(value: string): CashPayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  amount(value: number): CashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEvocaCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEvocaCommandBuilder {
    return new CashPayEvocaCommandBuilder();
  }
}

class CashPayEvocaCommandBuilder {
  private instance: CashPayEvocaCommand;

  constructor() {
    this.instance = new CashPayEvocaCommand();
  }

  agreementCode(value: string): CashPayEvocaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayEvocaCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class EvocaGetLoanDebtQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): EvocaGetLoanDebtQueryBuilder {
    return new EvocaGetLoanDebtQueryBuilder();
  }
}

class EvocaGetLoanDebtQueryBuilder {
  private instance: EvocaGetLoanDebtQuery;

  constructor() {
    this.instance = new EvocaGetLoanDebtQuery();
  }

  agreementCode(value: string): EvocaGetLoanDebtQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayAmeriaCommand {
  agreementCode: string;
  passport: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayAmeriaCommandBuilder {
    return new CashPayAmeriaCommandBuilder();
  }
}

class CashPayAmeriaCommandBuilder {
  private instance: CashPayAmeriaCommand;

  constructor() {
    this.instance = new CashPayAmeriaCommand();
  }

  agreementCode(value: string): CashPayAmeriaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  passport(value: string): CashPayAmeriaCommandBuilder {
    this.instance.passport = value;
    return this;
  }

  amount(value: number): CashPayAmeriaCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AmeriaGetLoanDebtQuery {
  agreementCode: string;
  passport: string;

  constructor() {
  }

  static build(): AmeriaGetLoanDebtQueryBuilder {
    return new AmeriaGetLoanDebtQueryBuilder();
  }
}

class AmeriaGetLoanDebtQueryBuilder {
  private instance: AmeriaGetLoanDebtQuery;

  constructor() {
    this.instance = new AmeriaGetLoanDebtQuery();
  }

  agreementCode(value: string): AmeriaGetLoanDebtQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  passport(value: string): AmeriaGetLoanDebtQueryBuilder {
    this.instance.passport = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayIdBankCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayIdBankCommandBuilder {
    return new CashPayIdBankCommandBuilder();
  }
}

class CashPayIdBankCommandBuilder {
  private instance: CashPayIdBankCommand;

  constructor() {
    this.instance = new CashPayIdBankCommand();
  }

  agreementCode(value: string): CashPayIdBankCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayIdBankCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class IdGetLoanDebtQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): IdGetLoanDebtQueryBuilder {
    return new IdGetLoanDebtQueryBuilder();
  }
}

class IdGetLoanDebtQueryBuilder {
  private instance: IdGetLoanDebtQuery;

  constructor() {
    this.instance = new IdGetLoanDebtQuery();
  }

  agreementCode(value: string): IdGetLoanDebtQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class VivacellBill {
  debt: number;

  constructor() {
  }

  static build(): VivacellBillBuilder {
    return new VivacellBillBuilder();
  }
}

class VivacellBillBuilder {
  private instance: VivacellBill;

  constructor() {
    this.instance = new VivacellBill();
  }

  debt(value: number): VivacellBillBuilder {
    this.instance.debt = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class VivacellObtainBillQuery {
  phone: string;

  constructor() {
  }

  static build(): VivacellObtainBillQueryBuilder {
    return new VivacellObtainBillQueryBuilder();
  }
}

class VivacellObtainBillQueryBuilder {
  private instance: VivacellObtainBillQuery;

  constructor() {
    this.instance = new VivacellObtainBillQuery();
  }

  phone(value: string): VivacellObtainBillQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class VivacellCashPayCommand {
  phone: string;
  amount: number;

  constructor() {
  }

  static build(): VivacellCashPayCommandBuilder {
    return new VivacellCashPayCommandBuilder();
  }
}

class VivacellCashPayCommandBuilder {
  private instance: VivacellCashPayCommand;

  constructor() {
    this.instance = new VivacellCashPayCommand();
  }

  phone(value: string): VivacellCashPayCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): VivacellCashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BeelineBill {
  dept: number;

  constructor() {
  }

  static build(): BeelineBillBuilder {
    return new BeelineBillBuilder();
  }
}

class BeelineBillBuilder {
  private instance: BeelineBill;

  constructor() {
    this.instance = new BeelineBill();
  }

  dept(value: number): BeelineBillBuilder {
    this.instance.dept = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BeelineObtainBillQuery {
  phone: string;

  constructor() {
  }

  static build(): BeelineObtainBillQueryBuilder {
    return new BeelineObtainBillQueryBuilder();
  }
}

class BeelineObtainBillQueryBuilder {
  private instance: BeelineObtainBillQuery;

  constructor() {
    this.instance = new BeelineObtainBillQuery();
  }

  phone(value: string): BeelineObtainBillQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BeelineTelephoneObtainBillQuery {
  telephone: string;

  constructor() {
  }

  static build(): BeelineTelephoneObtainBillQueryBuilder {
    return new BeelineTelephoneObtainBillQueryBuilder();
  }
}

class BeelineTelephoneObtainBillQueryBuilder {
  private instance: BeelineTelephoneObtainBillQuery;

  constructor() {
    this.instance = new BeelineTelephoneObtainBillQuery();
  }

  telephone(value: string): BeelineTelephoneObtainBillQueryBuilder {
    this.instance.telephone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class HiLineBillQuery {
  phoneOrSubscriberNumber: string;

  constructor() {
  }

  static build(): HiLineBillQueryBuilder {
    return new HiLineBillQueryBuilder();
  }
}

class HiLineBillQueryBuilder {
  private instance: HiLineBillQuery;

  constructor() {
    this.instance = new HiLineBillQuery();
  }

  phoneOrSubscriberNumber(value: string): HiLineBillQueryBuilder {
    this.instance.phoneOrSubscriberNumber = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BeelineCashPayCommand {
  phone: string;
  amount: number;

  constructor() {
  }

  static build(): BeelineCashPayCommandBuilder {
    return new BeelineCashPayCommandBuilder();
  }
}

class BeelineCashPayCommandBuilder {
  private instance: BeelineCashPayCommand;

  constructor() {
    this.instance = new BeelineCashPayCommand();
  }

  phone(value: string): BeelineCashPayCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): BeelineCashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class BeelineTelephoneCashPayCommand {
  telephone: string;
  amount: number;

  constructor() {
  }

  static build(): BeelineTelephoneCashPayCommandBuilder {
    return new BeelineTelephoneCashPayCommandBuilder();
  }
}

class BeelineTelephoneCashPayCommandBuilder {
  private instance: BeelineTelephoneCashPayCommand;

  constructor() {
    this.instance = new BeelineTelephoneCashPayCommand();
  }

  telephone(value: string): BeelineTelephoneCashPayCommandBuilder {
    this.instance.telephone = value;
    return this;
  }

  amount(value: number): BeelineTelephoneCashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class HiLineCashPayCommand {
  phoneOrSubscriberNumber: string;
  amount: number;

  constructor() {
  }

  static build(): HiLineCashPayCommandBuilder {
    return new HiLineCashPayCommandBuilder();
  }
}

class HiLineCashPayCommandBuilder {
  private instance: HiLineCashPayCommand;

  constructor() {
    this.instance = new HiLineCashPayCommand();
  }

  phoneOrSubscriberNumber(value: string): HiLineCashPayCommandBuilder {
    this.instance.phoneOrSubscriberNumber = value;
    return this;
  }

  amount(value: number): HiLineCashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class KarabakhTelekom {
  dept: number;

  constructor() {
  }

  static build(): KarabakhTelekomBuilder {
    return new KarabakhTelekomBuilder();
  }
}

class KarabakhTelekomBuilder {
  private instance: KarabakhTelekom;

  constructor() {
    this.instance = new KarabakhTelekom();
  }

  dept(value: number): KarabakhTelekomBuilder {
    this.instance.dept = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class KarabakhTelekomGetDepthQuery {
  phone: string;

  constructor() {
  }

  static build(): KarabakhTelekomGetDepthQueryBuilder {
    return new KarabakhTelekomGetDepthQueryBuilder();
  }
}

class KarabakhTelekomGetDepthQueryBuilder {
  private instance: KarabakhTelekomGetDepthQuery;

  constructor() {
    this.instance = new KarabakhTelekomGetDepthQuery();
  }

  phone(value: string): KarabakhTelekomGetDepthQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class PrepareDepositCommand {
  transactionId: string;
  walletId: string;
  amount: number;

  constructor() {
  }

  static build(): PrepareDepositCommandBuilder {
    return new PrepareDepositCommandBuilder();
  }
}

class PrepareDepositCommandBuilder {
  private instance: PrepareDepositCommand;

  constructor() {
    this.instance = new PrepareDepositCommand();
  }

  transactionId(value: string): PrepareDepositCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): PrepareDepositCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  amount(value: number): PrepareDepositCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class PrepareCreditCommand {
  transactionId: string;
  walletId: string;
  amount: number;

  constructor() {
  }

  static build(): PrepareCreditCommandBuilder {
    return new PrepareCreditCommandBuilder();
  }
}

class PrepareCreditCommandBuilder {
  private instance: PrepareCreditCommand;

  constructor() {
    this.instance = new PrepareCreditCommand();
  }

  transactionId(value: string): PrepareCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): PrepareCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  amount(value: number): PrepareCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ApproveDepositCommand {
  transactionId: string;
  walletId: string;

  constructor() {
  }

  static build(): ApproveDepositCommandBuilder {
    return new ApproveDepositCommandBuilder();
  }
}

class ApproveDepositCommandBuilder {
  private instance: ApproveDepositCommand;

  constructor() {
    this.instance = new ApproveDepositCommand();
  }

  transactionId(value: string): ApproveDepositCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): ApproveDepositCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class ApproveCreditCommand {
  transactionId: string;
  walletId: string;

  constructor() {
  }

  static build(): ApproveCreditCommandBuilder {
    return new ApproveCreditCommandBuilder();
  }
}

class ApproveCreditCommandBuilder {
  private instance: ApproveCreditCommand;

  constructor() {
    this.instance = new ApproveCreditCommand();
  }

  transactionId(value: string): ApproveCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): ApproveCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CancelDepositCommand {
  transactionId: string;
  walletId: string;

  constructor() {
  }

  static build(): CancelDepositCommandBuilder {
    return new CancelDepositCommandBuilder();
  }
}

class CancelDepositCommandBuilder {
  private instance: CancelDepositCommand;

  constructor() {
    this.instance = new CancelDepositCommand();
  }

  transactionId(value: string): CancelDepositCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): CancelDepositCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CancelCreditCommand {
  transactionId: string;
  walletId: string;

  constructor() {
  }

  static build(): CancelCreditCommandBuilder {
    return new CancelCreditCommandBuilder();
  }
}

class CancelCreditCommandBuilder {
  private instance: CancelCreditCommand;

  constructor() {
    this.instance = new CancelCreditCommand();
  }

  transactionId(value: string): CancelCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  walletId(value: string): CancelCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtUcomMobileQuery {
  phone: string;

  constructor() {
  }

  static build(): GetDebtUcomMobileQueryBuilder {
    return new GetDebtUcomMobileQueryBuilder();
  }
}

class GetDebtUcomMobileQueryBuilder {
  private instance: GetDebtUcomMobileQuery;

  constructor() {
    this.instance = new GetDebtUcomMobileQuery();
  }

  phone(value: string): GetDebtUcomMobileQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayUcomMobileCommand {
  transactionId: string;
  paymentId: string;
  phone: string;
  amount: number;
  debt: number;

  constructor() {
  }

  static build(): CashPayUcomMobileCommandBuilder {
    return new CashPayUcomMobileCommandBuilder();
  }
}

class CashPayUcomMobileCommandBuilder {
  private instance: CashPayUcomMobileCommand;

  constructor() {
    this.instance = new CashPayUcomMobileCommand();
  }

  transactionId(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  paymentId(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }

  phone(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): CashPayUcomMobileCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  debt(value: number): CashPayUcomMobileCommandBuilder {
    this.instance.debt = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtUcomFixedQuery {
  number: string;

  constructor() {
  }

  static build(): GetDebtUcomFixedQueryBuilder {
    return new GetDebtUcomFixedQueryBuilder();
  }
}

class GetDebtUcomFixedQueryBuilder {
  private instance: GetDebtUcomFixedQuery;

  constructor() {
    this.instance = new GetDebtUcomFixedQuery();
  }

  number(value: string): GetDebtUcomFixedQueryBuilder {
    this.instance.number = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayUcomFixedCommand {
  transactionId: string;
  paymentId: string;
  number: string;
  amount: number;
  debt: number;

  constructor() {
  }

  static build(): CashPayUcomFixedCommandBuilder {
    return new CashPayUcomFixedCommandBuilder();
  }
}

class CashPayUcomFixedCommandBuilder {
  private instance: CashPayUcomFixedCommand;

  constructor() {
    this.instance = new CashPayUcomFixedCommand();
  }

  transactionId(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }

  paymentId(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }

  number(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.number = value;
    return this;
  }

  amount(value: number): CashPayUcomFixedCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  debt(value: number): CashPayUcomFixedCommandBuilder {
    this.instance.debt = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtCommunalElectricityQuery {
  customerId: string;
  phone: string;

  constructor() {
  }

  static build(): GetDebtCommunalElectricityQueryBuilder {
    return new GetDebtCommunalElectricityQueryBuilder();
  }
}

class GetDebtCommunalElectricityQueryBuilder {
  private instance: GetDebtCommunalElectricityQuery;

  constructor() {
    this.instance = new GetDebtCommunalElectricityQuery();
  }

  customerId(value: string): GetDebtCommunalElectricityQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): GetDebtCommunalElectricityQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayCommunalElectricityCommand {
  customerId: string;
  phone: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayCommunalElectricityCommandBuilder {
    return new CashPayCommunalElectricityCommandBuilder();
  }
}

class CashPayCommunalElectricityCommandBuilder {
  private instance: CashPayCommunalElectricityCommand;

  constructor() {
    this.instance = new CashPayCommunalElectricityCommand();
  }

  customerId(value: string): CashPayCommunalElectricityCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): CashPayCommunalElectricityCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): CashPayCommunalElectricityCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetGasBranchesQuery {

  constructor() {
  }

  static build(): GetGasBranchesQueryBuilder {
    return new GetGasBranchesQueryBuilder();
  }
}

class GetGasBranchesQueryBuilder {
  private instance: GetGasBranchesQuery;

  constructor() {
    this.instance = new GetGasBranchesQuery();
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGasQuery {
  branchId: number;
  customerId: string;
  phone: string;

  constructor() {
  }

  static build(): GetDebtGasQueryBuilder {
    return new GetDebtGasQueryBuilder();
  }
}

class GetDebtGasQueryBuilder {
  private instance: GetDebtGasQuery;

  constructor() {
    this.instance = new GetDebtGasQuery();
  }

  branchId(value: number): GetDebtGasQueryBuilder {
    this.instance.branchId = value;
    return this;
  }

  customerId(value: string): GetDebtGasQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): GetDebtGasQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGasCommand {
  branchId: number;
  customerId: string;
  phone: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGasCommandBuilder {
    return new CashPayGasCommandBuilder();
  }
}

class CashPayGasCommandBuilder {
  private instance: CashPayGasCommand;

  constructor() {
    this.instance = new CashPayGasCommand();
  }

  branchId(value: number): CashPayGasCommandBuilder {
    this.instance.branchId = value;
    return this;
  }

  customerId(value: string): CashPayGasCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): CashPayGasCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): CashPayGasCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtRostelecomQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtRostelecomQueryBuilder {
    return new GetDebtRostelecomQueryBuilder();
  }
}

class GetDebtRostelecomQueryBuilder {
  private instance: GetDebtRostelecomQuery;

  constructor() {
    this.instance = new GetDebtRostelecomQuery();
  }

  customerId(value: string): GetDebtRostelecomQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayRostelecomCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayRostelecomCommandBuilder {
    return new CashPayRostelecomCommandBuilder();
  }
}

class CashPayRostelecomCommandBuilder {
  private instance: CashPayRostelecomCommand;

  constructor() {
    this.instance = new CashPayRostelecomCommand();
  }

  customerId(value: string): CashPayRostelecomCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayRostelecomCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtInteractiveQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtInteractiveQueryBuilder {
    return new GetDebtInteractiveQueryBuilder();
  }
}

class GetDebtInteractiveQueryBuilder {
  private instance: GetDebtInteractiveQuery;

  constructor() {
    this.instance = new GetDebtInteractiveQuery();
  }

  customerId(value: string): GetDebtInteractiveQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayInteractiveCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayInteractiveCommandBuilder {
    return new CashPayInteractiveCommandBuilder();
  }
}

class CashPayInteractiveCommandBuilder {
  private instance: CashPayInteractiveCommand;

  constructor() {
    this.instance = new CashPayInteractiveCommand();
  }

  customerId(value: string): CashPayInteractiveCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayInteractiveCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtArpinetQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtArpinetQueryBuilder {
    return new GetDebtArpinetQueryBuilder();
  }
}

class GetDebtArpinetQueryBuilder {
  private instance: GetDebtArpinetQuery;

  constructor() {
    this.instance = new GetDebtArpinetQuery();
  }

  customerId(value: string): GetDebtArpinetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayArpinetCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayArpinetCommandBuilder {
    return new CashPayArpinetCommandBuilder();
  }
}

class CashPayArpinetCommandBuilder {
  private instance: CashPayArpinetCommand;

  constructor() {
    this.instance = new CashPayArpinetCommand();
  }

  customerId(value: string): CashPayArpinetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayArpinetCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtWaterQuery {
  customerId: string;
  phone: string;

  constructor() {
  }

  static build(): GetDebtWaterQueryBuilder {
    return new GetDebtWaterQueryBuilder();
  }
}

class GetDebtWaterQueryBuilder {
  private instance: GetDebtWaterQuery;

  constructor() {
    this.instance = new GetDebtWaterQuery();
  }

  customerId(value: string): GetDebtWaterQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): GetDebtWaterQueryBuilder {
    this.instance.phone = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayWaterCommand {
  customerId: string;
  phone: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayWaterCommandBuilder {
    return new CashPayWaterCommandBuilder();
  }
}

class CashPayWaterCommandBuilder {
  private instance: CashPayWaterCommand;

  constructor() {
    this.instance = new CashPayWaterCommand();
  }

  customerId(value: string): CashPayWaterCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  phone(value: string): CashPayWaterCommandBuilder {
    this.instance.phone = value;
    return this;
  }

  amount(value: number): CashPayWaterCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtKamurjQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): GetDebtKamurjQueryBuilder {
    return new GetDebtKamurjQueryBuilder();
  }
}

class GetDebtKamurjQueryBuilder {
  private instance: GetDebtKamurjQuery;

  constructor() {
    this.instance = new GetDebtKamurjQuery();
  }

  agreementCode(value: string): GetDebtKamurjQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGoodCreditQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): GetDebtGoodCreditQueryBuilder {
    return new GetDebtGoodCreditQueryBuilder();
  }
}

class GetDebtGoodCreditQueryBuilder {
  private instance: GetDebtGoodCreditQuery;

  constructor() {
    this.instance = new GetDebtGoodCreditQuery();
  }

  agreementCode(value: string): GetDebtGoodCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtFincaQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): GetDebtFincaQueryBuilder {
    return new GetDebtFincaQueryBuilder();
  }
}

class GetDebtFincaQueryBuilder {
  private instance: GetDebtFincaQuery;

  constructor() {
    this.instance = new GetDebtFincaQuery();
  }

  agreementCode(value: string): GetDebtFincaQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtFastCreditQuery {
  agreementCode: string;

  constructor() {
  }

  static build(): GetDebtFastCreditQueryBuilder {
    return new GetDebtFastCreditQueryBuilder();
  }
}

class GetDebtFastCreditQueryBuilder {
  private instance: GetDebtFastCreditQuery;

  constructor() {
    this.instance = new GetDebtFastCreditQuery();
  }

  agreementCode(value: string): GetDebtFastCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayKamurjCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayKamurjCommandBuilder {
    return new CashPayKamurjCommandBuilder();
  }
}

class CashPayKamurjCommandBuilder {
  private instance: CashPayKamurjCommand;

  constructor() {
    this.instance = new CashPayKamurjCommand();
  }

  agreementCode(value: string): CashPayKamurjCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayKamurjCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGoodCreditCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGoodCreditCommandBuilder {
    return new CashPayGoodCreditCommandBuilder();
  }
}

class CashPayGoodCreditCommandBuilder {
  private instance: CashPayGoodCreditCommand;

  constructor() {
    this.instance = new CashPayGoodCreditCommand();
  }

  agreementCode(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayGoodCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayFincaCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayFincaCommandBuilder {
    return new CashPayFincaCommandBuilder();
  }
}

class CashPayFincaCommandBuilder {
  private instance: CashPayFincaCommand;

  constructor() {
    this.instance = new CashPayFincaCommand();
  }

  agreementCode(value: string): CashPayFincaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayFincaCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayFastCreditCommand {
  agreementCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayFastCreditCommandBuilder {
    return new CashPayFastCreditCommandBuilder();
  }
}

class CashPayFastCreditCommandBuilder {
  private instance: CashPayFastCreditCommand;

  constructor() {
    this.instance = new CashPayFastCreditCommand();
  }

  agreementCode(value: string): CashPayFastCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }

  amount(value: number): CashPayFastCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtVivaroBetQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtVivaroBetQueryBuilder {
    return new GetDebtVivaroBetQueryBuilder();
  }
}

class GetDebtVivaroBetQueryBuilder {
  private instance: GetDebtVivaroBetQuery;

  constructor() {
    this.instance = new GetDebtVivaroBetQuery();
  }

  customerId(value: string): GetDebtVivaroBetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayVivaroBetCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayVivaroBetCommandBuilder {
    return new CashPayVivaroBetCommandBuilder();
  }
}

class CashPayVivaroBetCommandBuilder {
  private instance: CashPayVivaroBetCommand;

  constructor() {
    this.instance = new CashPayVivaroBetCommand();
  }

  customerId(value: string): CashPayVivaroBetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayVivaroBetCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtVivaroCasinoQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtVivaroCasinoQueryBuilder {
    return new GetDebtVivaroCasinoQueryBuilder();
  }
}

class GetDebtVivaroCasinoQueryBuilder {
  private instance: GetDebtVivaroCasinoQuery;

  constructor() {
    this.instance = new GetDebtVivaroCasinoQuery();
  }

  customerId(value: string): GetDebtVivaroCasinoQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayVivaroCasinoCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayVivaroCasinoCommandBuilder {
    return new CashPayVivaroCasinoCommandBuilder();
  }
}

class CashPayVivaroCasinoCommandBuilder {
  private instance: CashPayVivaroCasinoCommand;

  constructor() {
    this.instance = new CashPayVivaroCasinoCommand();
  }

  customerId(value: string): CashPayVivaroCasinoCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayVivaroCasinoCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtTotoQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtTotoQueryBuilder {
    return new GetDebtTotoQueryBuilder();
  }
}

class GetDebtTotoQueryBuilder {
  private instance: GetDebtTotoQuery;

  constructor() {
    this.instance = new GetDebtTotoQuery();
  }

  customerId(value: string): GetDebtTotoQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayTotoCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayTotoCommandBuilder {
    return new CashPayTotoCommandBuilder();
  }
}

class CashPayTotoCommandBuilder {
  private instance: CashPayTotoCommand;

  constructor() {
    this.instance = new CashPayTotoCommand();
  }

  customerId(value: string): CashPayTotoCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayTotoCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEurofootballQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEurofootballQueryBuilder {
    return new GetDebtEurofootballQueryBuilder();
  }
}

class GetDebtEurofootballQueryBuilder {
  private instance: GetDebtEurofootballQuery;

  constructor() {
    this.instance = new GetDebtEurofootballQuery();
  }

  customerId(value: string): GetDebtEurofootballQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEurofootballCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEurofootballCommandBuilder {
    return new CashPayEurofootballCommandBuilder();
  }
}

class CashPayEurofootballCommandBuilder {
  private instance: CashPayEurofootballCommand;

  constructor() {
    this.instance = new CashPayEurofootballCommand();
  }

  customerId(value: string): CashPayEurofootballCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEurofootballCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtAdjarabetQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtAdjarabetQueryBuilder {
    return new GetDebtAdjarabetQueryBuilder();
  }
}

class GetDebtAdjarabetQueryBuilder {
  private instance: GetDebtAdjarabetQuery;

  constructor() {
    this.instance = new GetDebtAdjarabetQuery();
  }

  customerId(value: string): GetDebtAdjarabetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayAdjarabetCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayAdjarabetCommandBuilder {
    return new CashPayAdjarabetCommandBuilder();
  }
}

class CashPayAdjarabetCommandBuilder {
  private instance: CashPayAdjarabetCommand;

  constructor() {
    this.instance = new CashPayAdjarabetCommand();
  }

  customerId(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayAdjarabetCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtSocialOkQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtSocialOkQueryBuilder {
    return new GetDebtSocialOkQueryBuilder();
  }
}

class GetDebtSocialOkQueryBuilder {
  private instance: GetDebtSocialOkQuery;

  constructor() {
    this.instance = new GetDebtSocialOkQuery();
  }

  customerId(value: string): GetDebtSocialOkQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPaySocialOkCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPaySocialOkCommandBuilder {
    return new CashPaySocialOkCommandBuilder();
  }
}

class CashPaySocialOkCommandBuilder {
  private instance: CashPaySocialOkCommand;

  constructor() {
    this.instance = new CashPaySocialOkCommand();
  }

  customerId(value: string): CashPaySocialOkCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPaySocialOkCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGamesTankiOnlineQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtGamesTankiOnlineQueryBuilder {
    return new GetDebtGamesTankiOnlineQueryBuilder();
  }
}

class GetDebtGamesTankiOnlineQueryBuilder {
  private instance: GetDebtGamesTankiOnlineQuery;

  constructor() {
    this.instance = new GetDebtGamesTankiOnlineQuery();
  }

  customerId(value: string): GetDebtGamesTankiOnlineQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGamesTankiOnlineCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGamesTankiOnlineCommandBuilder {
    return new CashPayGamesTankiOnlineCommandBuilder();
  }
}

class CashPayGamesTankiOnlineCommandBuilder {
  private instance: CashPayGamesTankiOnlineCommand;

  constructor() {
    this.instance = new CashPayGamesTankiOnlineCommand();
  }

  customerId(value: string): CashPayGamesTankiOnlineCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayGamesTankiOnlineCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGamesWorldOfTanksQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtGamesWorldOfTanksQueryBuilder {
    return new GetDebtGamesWorldOfTanksQueryBuilder();
  }
}

class GetDebtGamesWorldOfTanksQueryBuilder {
  private instance: GetDebtGamesWorldOfTanksQuery;

  constructor() {
    this.instance = new GetDebtGamesWorldOfTanksQuery();
  }

  customerId(value: string): GetDebtGamesWorldOfTanksQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGamesWorldOfTanksCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGamesWorldOfTanksCommandBuilder {
    return new CashPayGamesWorldOfTanksCommandBuilder();
  }
}

class CashPayGamesWorldOfTanksCommandBuilder {
  private instance: CashPayGamesWorldOfTanksCommand;

  constructor() {
    this.instance = new CashPayGamesWorldOfTanksCommand();
  }

  customerId(value: string): CashPayGamesWorldOfTanksCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayGamesWorldOfTanksCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGamesWorldOfWarPlainsQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtGamesWorldOfWarPlainsQueryBuilder {
    return new GetDebtGamesWorldOfWarPlainsQueryBuilder();
  }
}

class GetDebtGamesWorldOfWarPlainsQueryBuilder {
  private instance: GetDebtGamesWorldOfWarPlainsQuery;

  constructor() {
    this.instance = new GetDebtGamesWorldOfWarPlainsQuery();
  }

  customerId(value: string): GetDebtGamesWorldOfWarPlainsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGamesWorldOfWarPlainsCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGamesWorldOfWarPlainsCommandBuilder {
    return new CashPayGamesWorldOfWarPlainsCommandBuilder();
  }
}

class CashPayGamesWorldOfWarPlainsCommandBuilder {
  private instance: CashPayGamesWorldOfWarPlainsCommand;

  constructor() {
    this.instance = new CashPayGamesWorldOfWarPlainsCommand();
  }

  customerId(value: string): CashPayGamesWorldOfWarPlainsCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayGamesWorldOfWarPlainsCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtGamesWorldOfWarShipsQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtGamesWorldOfWarShipsQueryBuilder {
    return new GetDebtGamesWorldOfWarShipsQueryBuilder();
  }
}

class GetDebtGamesWorldOfWarShipsQueryBuilder {
  private instance: GetDebtGamesWorldOfWarShipsQuery;

  constructor() {
    this.instance = new GetDebtGamesWorldOfWarShipsQuery();
  }

  customerId(value: string): GetDebtGamesWorldOfWarShipsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayGamesWorldOfWarShipsCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayGamesWorldOfWarShipsCommandBuilder {
    return new CashPayGamesWorldOfWarShipsCommandBuilder();
  }
}

class CashPayGamesWorldOfWarShipsCommandBuilder {
  private instance: CashPayGamesWorldOfWarShipsCommand;

  constructor() {
    this.instance = new CashPayGamesWorldOfWarShipsCommand();
  }

  customerId(value: string): CashPayGamesWorldOfWarShipsCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayGamesWorldOfWarShipsCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEtcMaryKayQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEtcMaryKayQueryBuilder {
    return new GetDebtEtcMaryKayQueryBuilder();
  }
}

class GetDebtEtcMaryKayQueryBuilder {
  private instance: GetDebtEtcMaryKayQuery;

  constructor() {
    this.instance = new GetDebtEtcMaryKayQuery();
  }

  customerId(value: string): GetDebtEtcMaryKayQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEtcMaryKayCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEtcMaryKayCommandBuilder {
    return new CashPayEtcMaryKayCommandBuilder();
  }
}

class CashPayEtcMaryKayCommandBuilder {
  private instance: CashPayEtcMaryKayCommand;

  constructor() {
    this.instance = new CashPayEtcMaryKayCommand();
  }

  customerId(value: string): CashPayEtcMaryKayCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEtcMaryKayCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEtcOriflameQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEtcOriflameQueryBuilder {
    return new GetDebtEtcOriflameQueryBuilder();
  }
}

class GetDebtEtcOriflameQueryBuilder {
  private instance: GetDebtEtcOriflameQuery;

  constructor() {
    this.instance = new GetDebtEtcOriflameQuery();
  }

  customerId(value: string): GetDebtEtcOriflameQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEtcOriflameCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEtcOriflameCommandBuilder {
    return new CashPayEtcOriflameCommandBuilder();
  }
}

class CashPayEtcOriflameCommandBuilder {
  private instance: CashPayEtcOriflameCommand;

  constructor() {
    this.instance = new CashPayEtcOriflameCommand();
  }

  customerId(value: string): CashPayEtcOriflameCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEtcOriflameCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEtcNtvQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEtcNtvQueryBuilder {
    return new GetDebtEtcNtvQueryBuilder();
  }
}

class GetDebtEtcNtvQueryBuilder {
  private instance: GetDebtEtcNtvQuery;

  constructor() {
    this.instance = new GetDebtEtcNtvQuery();
  }

  customerId(value: string): GetDebtEtcNtvQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEtcNtvCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEtcNtvCommandBuilder {
    return new CashPayEtcNtvCommandBuilder();
  }
}

class CashPayEtcNtvCommandBuilder {
  private instance: CashPayEtcNtvCommand;

  constructor() {
    this.instance = new CashPayEtcNtvCommand();
  }

  customerId(value: string): CashPayEtcNtvCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEtcNtvCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEtcEkengQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEtcEkengQueryBuilder {
    return new GetDebtEtcEkengQueryBuilder();
  }
}

class GetDebtEtcEkengQueryBuilder {
  private instance: GetDebtEtcEkengQuery;

  constructor() {
    this.instance = new GetDebtEtcEkengQuery();
  }

  customerId(value: string): GetDebtEtcEkengQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEtcEkengCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEtcEkengCommandBuilder {
    return new CashPayEtcEkengCommandBuilder();
  }
}

class CashPayEtcEkengCommandBuilder {
  private instance: CashPayEtcEkengCommand;

  constructor() {
    this.instance = new CashPayEtcEkengCommand();
  }

  customerId(value: string): CashPayEtcEkengCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEtcEkengCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtPoliceQuery {
  pinCode: string;

  constructor() {
  }

  static build(): GetDebtPoliceQueryBuilder {
    return new GetDebtPoliceQueryBuilder();
  }
}

class GetDebtPoliceQueryBuilder {
  private instance: GetDebtPoliceQuery;

  constructor() {
    this.instance = new GetDebtPoliceQuery();
  }

  pinCode(value: string): GetDebtPoliceQueryBuilder {
    this.instance.pinCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtPolicemanQuery {
  actNum: string;
  customerName: string;
  badgeId: string;

  constructor() {
  }

  static build(): GetDebtPolicemanQueryBuilder {
    return new GetDebtPolicemanQueryBuilder();
  }
}

class GetDebtPolicemanQueryBuilder {
  private instance: GetDebtPolicemanQuery;

  constructor() {
    this.instance = new GetDebtPolicemanQuery();
  }

  actNum(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.actNum = value;
    return this;
  }

  customerName(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.customerName = value;
    return this;
  }

  badgeId(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.badgeId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayPoliceCommand {
  actNum: string;
  customerName: string;
  badgeId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayPoliceCommandBuilder {
    return new CashPayPoliceCommandBuilder();
  }
}

class CashPayPoliceCommandBuilder {
  private instance: CashPayPoliceCommand;

  constructor() {
    this.instance = new CashPayPoliceCommand();
  }

  actNum(value: string): CashPayPoliceCommandBuilder {
    this.instance.actNum = value;
    return this;
  }

  customerName(value: string): CashPayPoliceCommandBuilder {
    this.instance.customerName = value;
    return this;
  }

  badgeId(value: string): CashPayPoliceCommandBuilder {
    this.instance.badgeId = value;
    return this;
  }

  amount(value: number): CashPayPoliceCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtParkingQuery {
  customerId: string;
  tariffId: number;

  constructor() {
  }

  static build(): GetDebtParkingQueryBuilder {
    return new GetDebtParkingQueryBuilder();
  }
}

class GetDebtParkingQueryBuilder {
  private instance: GetDebtParkingQuery;

  constructor() {
    this.instance = new GetDebtParkingQuery();
  }

  customerId(value: string): GetDebtParkingQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  tariffId(value: number): GetDebtParkingQueryBuilder {
    this.instance.tariffId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayParkingCommand {
  customerId: string;
  tariffId: number;
  amount: number;

  constructor() {
  }

  static build(): CashPayParkingCommandBuilder {
    return new CashPayParkingCommandBuilder();
  }
}

class CashPayParkingCommandBuilder {
  private instance: CashPayParkingCommand;

  constructor() {
    this.instance = new CashPayParkingCommand();
  }

  customerId(value: string): CashPayParkingCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  tariffId(value: number): CashPayParkingCommandBuilder {
    this.instance.tariffId = value;
    return this;
  }

  amount(value: number): CashPayParkingCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtParkingFineQuery {
  pinCode: string;

  constructor() {
  }

  static build(): GetDebtParkingFineQueryBuilder {
    return new GetDebtParkingFineQueryBuilder();
  }
}

class GetDebtParkingFineQueryBuilder {
  private instance: GetDebtParkingFineQuery;

  constructor() {
    this.instance = new GetDebtParkingFineQuery();
  }

  pinCode(value: string): GetDebtParkingFineQueryBuilder {
    this.instance.pinCode = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayParkingFineCommand {
  pinCode: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayParkingFineCommandBuilder {
    return new CashPayParkingFineCommandBuilder();
  }
}

class CashPayParkingFineCommandBuilder {
  private instance: CashPayParkingFineCommand;

  constructor() {
    this.instance = new CashPayParkingFineCommand();
  }

  pinCode(value: string): CashPayParkingFineCommandBuilder {
    this.instance.pinCode = value;
    return this;
  }

  amount(value: number): CashPayParkingFineCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDebtEfuGamesQuery {
  customerId: string;

  constructor() {
  }

  static build(): GetDebtEfuGamesQueryBuilder {
    return new GetDebtEfuGamesQueryBuilder();
  }
}

class GetDebtEfuGamesQueryBuilder {
  private instance: GetDebtEfuGamesQuery;

  constructor() {
    this.instance = new GetDebtEfuGamesQuery();
  }

  customerId(value: string): GetDebtEfuGamesQueryBuilder {
    this.instance.customerId = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayEfuGamesCommand {
  customerId: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayEfuGamesCommandBuilder {
    return new CashPayEfuGamesCommandBuilder();
  }
}

class CashPayEfuGamesCommandBuilder {
  private instance: CashPayEfuGamesCommand;

  constructor() {
    this.instance = new CashPayEfuGamesCommand();
  }

  customerId(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.customerId = value;
    return this;
  }

  amount(value: number): CashPayEfuGamesCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class GetDeptTestQuery {
  code: string;

  constructor() {
  }

  static build(): GetDeptTestQueryBuilder {
    return new GetDeptTestQueryBuilder();
  }
}

class GetDeptTestQueryBuilder {
  private instance: GetDeptTestQuery;

  constructor() {
    this.instance = new GetDeptTestQuery();
  }

  code(value: string): GetDeptTestQueryBuilder {
    this.instance.code = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class CashPayTestCommand {
  code: string;
  amount: number;

  constructor() {
  }

  static build(): CashPayTestCommandBuilder {
    return new CashPayTestCommandBuilder();
  }
}

class CashPayTestCommandBuilder {
  private instance: CashPayTestCommand;

  constructor() {
    this.instance = new CashPayTestCommand();
  }

  code(value: string): CashPayTestCommandBuilder {
    this.instance.code = value;
    return this;
  }

  amount(value: number): CashPayTestCommandBuilder {
    this.instance.amount = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class Foo {
  bar: Page;
  baz: Order;
  orders: Array<Order>;
  integers: Array<number>;
  string: string;

  constructor() {
  }

  static build(): FooBuilder {
    return new FooBuilder();
  }
}

class FooBuilder {
  private instance: Foo;

  constructor() {
    this.instance = new Foo();
  }

  bar(value: Page): FooBuilder {
    this.instance.bar = value;
    return this;
  }

  baz(value: Order): FooBuilder {
    this.instance.baz = value;
    return this;
  }

  orders(value: Array<Order>): FooBuilder {
    this.instance.orders = value;
    return this;
  }

  integers(value: Array<number>): FooBuilder {
    this.instance.integers = value;
    return this;
  }

  string(value: string): FooBuilder {
    this.instance.string = value;
    return this;
  }

  get() {
    return this.instance;
  }
}

export class AdminApi extends BaseApi {

  authenticate(data: AdminAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/authenticate', data, options);
  }

  getAccessToken(data: AdminGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-access-token', data, options);
  }

  createAdmin(data: AdminCreateAdminCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-admin', data, options);
  }

  createBranch(data: AdminCreateBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-branch', data, options);
  }

  updateBranch(data: AdminUpdateBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-branch', data, options);
  }

  enableBranch(data: AdminEnableBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-branch', data, options);
  }

  disableBranch(data: AdminDisableBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-branch', data, options);
  }

  getBranch(data: AdminGetBranchQuery, options?: any): Observable<Branch> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-branch', data, options);
  }

  findBranches(data: AdminFindBranchesQuery, options?: any): Observable<Paged<Branch>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/find-branches', data, options);
  }

  createCashbox(data: AdminCreateCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-cashbox', data, options);
  }

  updateCashbox(data: AdminUpdateCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-cashbox', data, options);
  }

  enableCashbox(data: AdminEnableCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-cashbox', data, options);
  }

  disableCashbox(data: AdminDisableCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-cashbox', data, options);
  }

  getCashbox(data: AdminGetCashboxQuery, options?: any): Observable<Cashbox> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-cashbox', data, options);
  }

  getCashboxes(data: AdminGetCashboxesQuery, options?: any): Observable<Paged<Cashbox>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-cashboxes', data, options);
  }

  createOperator(data: AdminCreateOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-operator', data, options);
  }

  updateOperator(data: AdminUpdateOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-operator', data, options);
  }

  enableOperator(data: AdminEnableOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-operator', data, options);
  }

  disableOperator(data: AdminDisableOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-operator', data, options);
  }

  getOperator(data: AdminGetOperatorQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operator', data, options);
  }

  getOperators(data: AdminGetOperatorsQuery, options?: any): Observable<Paged<OperatorInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operators', data, options);
  }

  attachOperator(data: AdminAttachOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/attach-operator', data, options);
  }

  detachOperator(data: AdminDetachOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/detach-operator', data, options);
  }

  updateProvider(data: AdminUpdateProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-provider', data, options);
  }

  enableProvider(data: AdminEnableProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-provider', data, options);
  }

  disableProvider(data: AdminDisableProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-provider', data, options);
  }

  getProvider(data: AdminGetProviderQuery, options?: any): Observable<Provider> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider', data, options);
  }

  getProviders(data: AdminGetProvidersQuery, options?: any): Observable<Paged<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-providers', data, options);
  }

  createProviderLabel(data: AdminCreateProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-provider-label', data, options);
  }

  updateProviderLabel(data: AdminUpdateProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-provider-label', data, options);
  }

  getProviderLabel(data: AdminGetProviderLabelQuery, options?: any): Observable<ProviderLabel> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-label', data, options);
  }

  getProviderLabels(data: AdminGetProviderLabelsQuery, options?: any): Observable<Paged<ProviderLabel>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-labels', data, options);
  }

  deleteProviderLabel(data: AdminDeleteProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-provider-label', data, options);
  }

  saveCommission(data: AdminSaveCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/save-commission', data, options);
  }

  deleteCommission(data: AdminDeleteCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-commission', data, options);
  }

  getCommission(data: AdminGetCommissionQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission', data, options);
  }

  getCommissions(data: AdminGetCommissionsQuery, options?: any): Observable<Paged<Commission>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commissions', data, options);
  }

  getOperatorScopes(data: AdminGetOperatorScopesQuery, options?: any): Observable<Array<Scope>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operator-scopes', data, options);
  }

  resetPassword(data: AdminResetPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/reset-password', data, options);
  }

  createCommissionPolicy(data: AdminCreateCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-commission-policy', data, options);
  }

  updateCommissionPolicy(data: AdminUpdateCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-commission-policy', data, options);
  }

  deleteCommissionPolicy(data: AdminDeleteCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-commission-policy', data, options);
  }

  getCommissionPolicy(data: AdminGetCommissionPolicyQuery, options?: any): Observable<CommissionPolicy> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission-policy', data, options);
  }

  getCommissionPolicies(data: AdminGetCommissionPoliciesQuery, options?: any): Observable<Paged<CommissionPolicy>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission-policies', data, options);
  }

  approveCommission(data: AdminApproveCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/approve-commission', data, options);
  }

  getBranchCommission(data: AdminGetBranchCommissionQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-branch-commission', data, options);
  }
}

export class CustomerApi extends BaseApi {

  signUp(data: CustomersSignUpCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/sign-up', data, options);
  }

  sendActivationCode(data: CustomersSendActivationCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-activation-code', data, options);
  }

  activate(data: CustomersActivateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/activate', data, options);
  }

  authenticate(data: CustomersAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/authenticate', data, options);
  }

  getAccessToken(data: CustomersGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-access-token', data, options);
  }

  forgotPassword(data: CustomersForgotPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/forgot-password', data, options);
  }

  confirmCode(data: CustomersConfirmCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/confirm-code', data, options);
  }

  resetPassword(data: CustomersResetPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/reset-password', data, options);
  }

  setData(data: CustomersSetDataCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-data', data, options);
  }

  getData(data: CustomersGetDataQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-data', data, options);
  }

  sendCode(data: CustomersSendCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-code', data, options);
  }

  confirm(data: CustomersConfirmCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/confirm', data, options);
  }

  createGroup(data: CustomersCreateGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/create-group', data, options);
  }

  deleteGroup(data: CustomersDeleteGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/delete-group', data, options);
  }

  addContract(data: CustomersAddContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/add-contract', data, options);
  }

  removeContract(data: CustomersRemoveContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/remove-contract', data, options);
  }

  addCard(data: CustomersAddCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/add-card', data, options);
  }

  removeCard(data: CustomersRemoveCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/remove-card', data, options);
  }

  addFavorite(data: CustomersAddFavoriteCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/add-favorite', data, options);
  }

  removeFavorite(data: CustomersRemoveFavoriteCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/remove-favorite', data, options);
  }

  getProfile(data: CustomersGetProfileQuery, options?: any): Observable<Customer> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-profile', data, options);
  }

  findCustomerByEmail(data: CustomersFindCustomerByEmailQuery, options?: any): Observable<Customer> {
    return this.post(BaseApi.apiUrl + '/upay/customers/find-customer-by-email', data, options);
  }

  getTransactions(data: CustomersGetTransactionsQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-transactions', data, options);
  }

  getFavorites(data: CustomersGetFavoritesQuery, options?: any): Observable<Array<Contract>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-favorites', data, options);
  }

  getCards(data: CustomersGetCardsQuery, options?: any): Observable<Paged<Card>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-cards', data, options);
  }

  findGroup(data: CustomersFindGroupQuery, options?: any): Observable<Paged<CustomerGroup>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/find-group', data, options);
  }
}

export class OfficeApi extends BaseApi {

  authenticate(data: OperatorAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/authenticate', data, options);
  }

  getAccessToken(data: OperatorGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-access-token', data, options);
  }

  changePassword(data: OperatorChangePasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/change-password', data, options);
  }

  createGroup(data: OperatorCreateGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/create-group', data, options);
  }

  findGroup(data: OperatorFindGroupQuery, options?: any): Observable<Paged<Group>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/find-group', data, options);
  }

  deleteGroup(data: OperatorDeleteGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/delete-group', data, options);
  }

  addContract(data: OperatorAddContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/add-contract', data, options);
  }

  removeContract(data: OperatorRemoveContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/remove-contract', data, options);
  }

  closeCashbox(data: OperatorCloseCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/close-cashbox', data, options);
  }

  cashIn(data: OperatorCashInCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-in', data, options);
  }

  cashOut(data: OperatorCashOutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out', data, options);
  }

  createToken(data: OperatorCreateTokenCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/create-token', data, options);
  }

  getCashboxBalance(data: OperatorGetCashboxBalanceQuery, options?: any): Observable<CashboxBalance> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-balance', data, options);
  }

  getProviderDescriptors(data: OperatorGetProviderDescriptorsQuery, options?: any): Observable<Array<ContractDesc>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-provider-descriptors', data, options);
  }

  getProfile(data: OperatorGetProfileQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-profile', data, options);
  }

  addToBasket(data: OperatorAddToBasketCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/add-to-basket', data, options);
  }

  deleteFromBasket(data: OperatorDeleteFromBasketCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/delete-from-basket', data, options);
  }

  getBasketItemsCount(data: OperatorGetBasketItemsCountQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-basket-items-count', data, options);
  }

  getBasketItems(data: OperatorGetBasketItemsQuery, options?: any): Observable<Array<Basket>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-basket-items', data, options);
  }

  cashboxPay(data: OperatorCashboxPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-pay', data, options);
  }

  cashboxIn(data: OperatorCashboxInCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-in', data, options);
  }

  cashboxOut(data: OperatorCashboxOutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-out', data, options);
  }

  getCashboxTransactions(data: OperatorGetCashboxTransactionsQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-transactions', data, options);
  }

  getTransactionsByPayment(data: OperatorGetTransactionsByPaymentQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-transactions-by-payment', data, options);
  }

  getTransaction(data: OperatorGetTransactionQuery, options?: any): Observable<Transaction> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-transaction', data, options);
  }

  getCurrentBranch(data: OperatorGetCurrentBranchQuery, options?: any): Observable<Branch> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-current-branch', data, options);
  }

  num2Text(data: OperatorNum2TextQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/num2-text', data, options);
  }

  armsoftImport(data: OperatorArmsoftImportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/armsoft-import', data, options);
  }

  generateCashboxReport(data: OperatorGenerateCashboxReportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/generate-cashbox-report', data, options);
  }

  // getCashboxLogs(data: OperatorGetCashboxLogsQuery, options?: any): Observable<Paged<CashboxLog>> {
  //   return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-logs', data, options);
  // }

  getProvider(data: AdminGetProviderQuery, options?: any): Observable<Provider> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider', data, options);
  }

  getProviders(data: AdminGetProvidersQuery, options?: any): Observable<Paged<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-providers', data, options);
  }

  getProviderLabel(data: AdminGetProviderLabelQuery, options?: any): Observable<ProviderLabel> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-label', data, options);
  }

  getProviderLabels(data: AdminGetProviderLabelsQuery, options?: any): Observable<Paged<ProviderLabel>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-labels', data, options);
  }

  getProviderCommissions(data: OperatorGetProviderCommissionsQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-provider-commissions', data, options);
  }

  vivacellCashPay(data: VivacellCashPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/vivacell-cash-pay', data, options);
  }

  vivacellObtainBill(data: VivacellObtainBillQuery, options?: any): Observable<VivacellBill> {
    return this.post(BaseApi.apiUrl + '/upay/vivacell-obtain-bill', data, options);
  }

  getDebtUcomMobile(data: GetDebtUcomMobileQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ucom-mobile', data, options);
  }

  cashPayUcomMobile(data: CashPayUcomMobileCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ucom-mobile', data, options);
  }

  getDebtUcomFixed(data: GetDebtUcomFixedQuery, options?: any): Observable<UpayFixedDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ucom-fixed', data, options);
  }

  cashPayUcomFixed(data: CashPayUcomFixedCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ucom-fixed', data, options);
  }

  beelineObtainBill(data: BeelineObtainBillQuery, options?: any): Observable<BeelineBill> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-obtain-bill', data, options);
  }

  beelineTelephoneObtainBill(data: BeelineTelephoneObtainBillQuery, options?: any): Observable<BeelineBill> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-telephone-obtain-bill', data, options);
  }

  hiLineBill(data: HiLineBillQuery, options?: any): Observable<BeelineBill> {
    return this.post(BaseApi.apiUrl + '/upay/hi-line-bill', data, options);
  }

  getDebtCommunalElectricity(data: GetDebtCommunalElectricityQuery, options?: any): Observable<DebtCommunalElectricity> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-communal-electricity', data, options);
  }

  cashPayCommunalElectricity(data: CashPayCommunalElectricityCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-communal-electricity', data, options);
  }

  getGasBranches(data: GetGasBranchesQuery, options?: any): Observable<Array<GasBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/get-gas-branches', data, options);
  }

  getDebtGas(data: GetDebtGasQuery, options?: any): Observable<GasDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-gas', data, options);
  }

  cashPayGas(data: CashPayGasCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-gas', data, options);
  }

  getDebtRostelecom(data: GetDebtRostelecomQuery, options?: any): Observable<DebtRostelecom> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-rostelecom', data, options);
  }

  cashPayRostelecom(data: CashPayRostelecomCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-rostelecom', data, options);
  }

  getDebtInteractive(data: GetDebtInteractiveQuery, options?: any): Observable<DebtInteractive> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-interactive', data, options);
  }

  cashPayInteractive(data: CashPayInteractiveCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-interactive', data, options);
  }

  getDebtArpinet(data: GetDebtArpinetQuery, options?: any): Observable<DebtArpinet> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-arpinet', data, options);
  }

  cashPayArpinet(data: CashPayArpinetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-arpinet', data, options);
  }

  getDebtWater(data: GetDebtWaterQuery, options?: any): Observable<DebtWater> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-water', data, options);
  }

  cashPayWater(data: CashPayWaterCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-water', data, options);
  }

  beelineCashPay(data: BeelineCashPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-cash-pay', data, options);
  }

  beelineTelephoneCashPay(data: BeelineTelephoneCashPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-telephone-cash-pay', data, options);
  }

  hiLineCashPay(data: HiLineCashPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/hi-line-cash-pay', data, options);
  }

  karabakhTelekomGetDepth(data: KarabakhTelekomGetDepthQuery, options?: any): Observable<KarabakhTelekom> {
    return this.post(BaseApi.apiUrl + '/upay/karabakh-telekom-get-depth', data, options);
  }

  evocaGetLoanDebt(data: EvocaGetLoanDebtQuery, options?: any): Observable<LoanInfo> {
    return this.post(BaseApi.apiUrl + '/upay/evoca-get-loan-debt', data, options);
  }

  cashPayEvoca(data: CashPayEvocaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca', data, options);
  }

  ameriaGetLoanDebt(data: AmeriaGetLoanDebtQuery, options?: any): Observable<LoanInfo> {
    return this.post(BaseApi.apiUrl + '/upay/ameria-get-loan-debt', data, options);
  }

  cashPayAmeria(data: CashPayAmeriaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ameria', data, options);
  }

  idGetLoanDebt(data: IdGetLoanDebtQuery, options?: any): Observable<LoanInfo> {
    return this.post(BaseApi.apiUrl + '/upay/id-get-loan-debt', data, options);
  }

  cashPayIdBank(data: CashPayIdBankCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-id-bank', data, options);
  }

  getDebtKamurj(data: GetDebtKamurjQuery, options?: any): Observable<DeptLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-kamurj', data, options);
  }

  cashPayKamurj(data: CashPayKamurjCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-kamurj', data, options);
  }

  getDebtGoodCredit(data: GetDebtGoodCreditQuery, options?: any): Observable<DeptLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-good-credit', data, options);
  }

  cashPayGoodCredit(data: CashPayGoodCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-good-credit', data, options);
  }

  getDebtFinca(data: GetDebtFincaQuery, options?: any): Observable<DeptLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-finca', data, options);
  }

  cashPayFinca(data: CashPayFincaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-finca', data, options);
  }

  getDebtFastCredit(data: GetDebtFastCreditQuery, options?: any): Observable<DeptLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-fast-credit', data, options);
  }

  cashPayFastCredit(data: CashPayFastCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-fast-credit', data, options);
  }

  getDebtVivaroBet(data: GetDebtVivaroBetQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-vivaro-bet', data, options);
  }

  cashPayVivaroBet(data: CashPayVivaroBetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-vivaro-bet', data, options);
  }

  getDebtVivaroCasino(data: GetDebtVivaroCasinoQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-vivaro-casino', data, options);
  }

  cashPayVivaroCasino(data: CashPayVivaroCasinoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-vivaro-casino', data, options);
  }

  getDebtToto(data: GetDebtTotoQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-toto', data, options);
  }

  cashPayToto(data: CashPayTotoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-toto', data, options);
  }

  getDebtEurofootball(data: GetDebtEurofootballQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-eurofootball', data, options);
  }

  cashPayEurofootball(data: CashPayEurofootballCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-eurofootball', data, options);
  }

  getDebtAdjarabet(data: GetDebtAdjarabetQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-adjarabet', data, options);
  }

  cashPayAdjarabet(data: CashPayAdjarabetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-adjarabet', data, options);
  }

  getDebtSocialOk(data: GetDebtSocialOkQuery, options?: any): Observable<DebtSocialOk> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-social-ok', data, options);
  }

  cashPaySocialOk(data: CashPaySocialOkCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-social-ok', data, options);
  }

  getDebtGamesTankiOnline(data: GetDebtGamesTankiOnlineQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-tanki-online', data, options);
  }

  cashPayGamesTankiOnline(data: CashPayGamesTankiOnlineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-games-tanki-online', data, options);
  }

  getDebtGamesWorldOfTanks(data: GetDebtGamesWorldOfTanksQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-tanks', data, options);
  }

  cashPayGamesWorldOfTanks(data: CashPayGamesWorldOfTanksCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-games-world-of-tanks', data, options);
  }

  getDebtGamesWorldOfWarPlains(data: GetDebtGamesWorldOfWarPlainsQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-war-plains', data, options);
  }

  cashPayGamesWorldOfWarPlains(data: CashPayGamesWorldOfWarPlainsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-games-world-of-war-plains', data, options);
  }

  getDebtGamesWorldOfWarShips(data: GetDebtGamesWorldOfWarShipsQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-war-ships', data, options);
  }

  cashPayGamesWorldOfWarShips(data: CashPayGamesWorldOfWarShipsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-games-world-of-war-ships', data, options);
  }

  getDebtEtcMaryKay(data: GetDebtEtcMaryKayQuery, options?: any): Observable<DebtEtcMaryKay> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-etc-mary-kay', data, options);
  }

  cashPayEtcMaryKay(data: CashPayEtcMaryKayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-etc-mary-kay', data, options);
  }

  getDebtEtcOriflame(data: GetDebtEtcOriflameQuery, options?: any): Observable<DebtEtcOriflame> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-etc-oriflame', data, options);
  }

  cashPayEtcOriflame(data: CashPayEtcOriflameCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-etc-oriflame', data, options);
  }

  getDebtEtcNtv(data: GetDebtEtcNtvQuery, options?: any): Observable<DebtEtcNtv> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-etc-ntv', data, options);
  }

  cashPayEtcNtv(data: CashPayEtcNtvCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-etc-ntv', data, options);
  }

  getDebtEtcEkeng(data: GetDebtEtcEkengQuery, options?: any): Observable<DebtEtcEkeng> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-etc-ekeng', data, options);
  }

  cashPayEtcEkeng(data: CashPayEtcEkengCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-etc-ekeng', data, options);
  }

  getDebtPolice(data: GetDebtPoliceQuery, options?: any): Observable<PoliceDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-police', data, options);
  }

  getDebtPoliceman(data: GetDebtPolicemanQuery, options?: any): Observable<PoliceDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-policeman', data, options);
  }

  cashPayPolice(data: CashPayPoliceCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-police', data, options);
  }

  getDebtParking(data: GetDebtParkingQuery, options?: any): Observable<ParkingFineDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-parking', data, options);
  }

  cashPayParking(data: CashPayParkingCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-parking', data, options);
  }

  getDebtParkingFine(data: GetDebtParkingFineQuery, options?: any): Observable<ParkingFineDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-parking-fine', data, options);
  }

  cashPayParkingFine(data: CashPayParkingFineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-parking-fine', data, options);
  }

  getDeptTest(data: GetDeptTestQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/get-dept-test', data, options);
  }

  cashPayTest(data: CashPayTestCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-test', data, options);
  }

  getDebtEfuGames(data: GetDebtEfuGamesQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-efu-games', data, options);
  }

  cashPayEfuGames(data: CashPayEfuGamesCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-efu-games', data, options);
  }
}
