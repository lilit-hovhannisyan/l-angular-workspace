import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {ErrorHandler} from './error-handler';

import {environment} from '../../environments/environment';

// const officeUrl = environment.APIEndpoint.officeUrl;
// const adminUrl = environment.APIEndpoint.adminUrl;
const publicUrl = environment.APIEndpoint.publicUrl;

@Injectable()
export class BaseApi {
  constructor(public http: HttpClient,
              public errorHandler: ErrorHandler) {
  }

  static apiUrl = publicUrl;

  post<T>(url: string, data: any, options?: any) {
    const headers = this.getAuthorizationHeader();
    return this.http.post(url, data, {
      headers: headers
    }).pipe(map((res: any) => {
      // return res.data;
        if (res.error) {
          this.errorHandler.handle(res.error);
        } else {
          return res.data;
        }
      }));
  }

  getAuthorizationHeader() {
    const headers = new HttpHeaders();
    const authToken = localStorage.getItem('authToken');
    if (authToken) {
     return headers.append('Authorization', `Bearer ${authToken}`);
    } else {
      return headers;
    }
  }

}
