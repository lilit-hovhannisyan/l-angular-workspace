import {Injectable} from '@angular/core';
// import {SiteDataService} from '../dal/site/site-data.service';
// import {AlertBoxService} from '../widgets/alert-box/alert-box.service';
// import {LoadingService} from '../widgets/loading/loading.service';

@Injectable()
export class ErrorHandler {
  constructor(
    // private siteDataService: SiteDataService,
              // private alertBoxService: AlertBoxService,
              // private loadingService: LoadingService
  ) {
  }

  handle(error) {
    // this.loadingService.showLoader(false);
    // this.alertBoxService.initError({type: 'error', text: error.message} );
    // this.siteDataService.setGlobalErrorMessage(error.message);
    alert(error.message);
    throw error;
  }

}
