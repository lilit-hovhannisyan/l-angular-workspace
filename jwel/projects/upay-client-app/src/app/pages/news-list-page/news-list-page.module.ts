import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {NewsListPageComponent} from './news-list-page.component';
import {newsListRoutes} from './news-list-routing';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    NewsListPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(newsListRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    NewsListPageComponent
  ],
})
export class NewsListPageModule {
}
