import { Routes } from '@angular/router';
import {NewsListPageComponent} from './news-list-page.component';
import {NewsCardListComponent} from '../../widgets/news-card-list/news-card-list.component';
import {NewsDetailsComponent} from '../../widgets/news-details/news-details.component';

export const newsListRoutes: Routes = [
  {
    path: '',
    component: NewsListPageComponent,
    children: [
      {
        path: '',
        component: NewsCardListComponent
      },
      // {
      //   path: 'news',
      //   component: NewsCardListComponent
      // },
      {
        path: ':newsId',
        component: NewsDetailsComponent
      }
    ]
  }
];
