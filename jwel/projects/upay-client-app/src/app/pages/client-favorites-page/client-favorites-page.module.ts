import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {clientFavoritesRoutes} from './client-favorites-page-routing';
import {ClientFavoritesPageComponent} from './client-favorites-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    ClientFavoritesPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientFavoritesRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ClientFavoritesPageComponent
  ],
})
export class ClientFavoritesPageModule {
}
