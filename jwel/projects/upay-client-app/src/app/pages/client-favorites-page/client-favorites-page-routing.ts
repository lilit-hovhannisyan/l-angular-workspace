import { Routes } from '@angular/router';
import {ClientFavoritesPageComponent} from './client-favorites-page.component';

export const clientFavoritesRoutes: Routes = [
  {
    path: '',
    component: ClientFavoritesPageComponent
  }
];
