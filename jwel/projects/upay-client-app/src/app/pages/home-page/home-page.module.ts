import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {homeRoutes} from './hom-page-routing';
import {HomePageComponent} from './home-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';


// lib
import { AtomsModules, MoleculesModule } from 'iu-ui-lib';


@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(homeRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules, MoleculesModule
  ],
  exports: [
    HomePageComponent
  ],
})
export class HomePageModule {
}
