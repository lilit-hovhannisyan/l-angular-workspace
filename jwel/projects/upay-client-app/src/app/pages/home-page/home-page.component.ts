import { Component } from '@angular/core';
import { PopupsService } from '../../popups/popups.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.less']
})
export class HomePageComponent {
  constructor (private popupsService: PopupsService) {}

  title = 'Save Your time by using  upay mobile app';

  infoBlocks = [
    {
      order: 1,
      title: 'Pay',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      order: 2,
      title: 'Topup',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      order: 3,
      title: 'Transfer',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      order: 4,
      title: 'Add payment',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }
  ];

  openPopup( popup ) {
    this.popupsService.openPopUp(popup);
  }

}
