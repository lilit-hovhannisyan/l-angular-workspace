import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NotFoundPageComponent} from './not-found-page.component';
import {notFoundRoutes} from './not-found-routing';
import {TranslateModule} from '@ngx-translate/core';

import { AtomsModules, MoleculesModule } from 'iu-ui-lib';

@NgModule({
  declarations: [
    NotFoundPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(notFoundRoutes),
    TranslateModule,
    MoleculesModule,
    AtomsModules
  ],
  exports: [
    NotFoundPageComponent
  ],
})
export class NotFoundPageModule {
}
