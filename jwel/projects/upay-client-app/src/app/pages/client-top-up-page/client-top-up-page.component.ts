import {Component, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

@Component({
  selector: 'app-client-top-up-page',
  templateUrl: './client-top-up-page.component.html',
  styleUrls: ['./client-top-up-page.component.less']
})
export class ClientTopUpPageComponent implements OnInit {

  constructor(private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.providerDataService.getTopUpProvidersCategoryTree();
  }

}
