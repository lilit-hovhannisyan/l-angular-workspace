import { Routes } from '@angular/router';
import {ClientTopUpPageComponent} from './client-top-up-page.component';
import {ServiceProviderSelectComponent} from '../../widgets/service-provider-select/service-provider-select.component';
import {ServiceProviderPayTerminalComponent} from '../../widgets/service-provider-pay-terminal/service-provider-pay-terminal.component';

export const clientTopUpRoutes: Routes = [
  {
    path: '',
    component: ClientTopUpPageComponent,
    children: [
      {
        path: ':categoryId',
        component: ServiceProviderSelectComponent
      },
      {
        path: ':categoryId/:typeId',
        component: ServiceProviderPayTerminalComponent
      }
    ]
  }
];
