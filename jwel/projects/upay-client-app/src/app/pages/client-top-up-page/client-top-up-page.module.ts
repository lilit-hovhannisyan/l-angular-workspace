import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientTopUpPageComponent} from './client-top-up-page.component';
import {clientTopUpRoutes} from './client-top-up-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';


@NgModule({
  declarations: [
    ClientTopUpPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientTopUpRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ClientTopUpPageComponent
  ],
})
export class ClientTopUpPageModule {
}
