import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientTransferPageComponent} from './client-transfer-page.component';
import {clientTransferRoutes} from './client-transfer-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';


@NgModule({
  declarations: [
    ClientTransferPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientTransferRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ClientTransferPageComponent
  ],
})
export class ClientTransferPageModule {
}
