import { Routes } from '@angular/router';
import {ClientTransferPageComponent} from './client-transfer-page.component';
import {ServiceProviderSelectComponent} from '../../widgets/service-provider-select/service-provider-select.component';
import {ServiceProviderPayTerminalComponent} from '../../widgets/service-provider-pay-terminal/service-provider-pay-terminal.component';

export const clientTransferRoutes: Routes = [
  {
    path: '',
    component: ClientTransferPageComponent,
    children: [
      {
        path: ':categoryId',
        component: ServiceProviderSelectComponent
      },
      {
        path: ':categoryId/:typeId',
        component: ServiceProviderPayTerminalComponent
      }
    ]
  }
];
