import {Component, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

@Component({
  selector: 'app-client-transfer-page',
  templateUrl: './client-transfer-page.component.html',
  styleUrls: ['./client-transfer-page.component.less']
})
export class ClientTransferPageComponent implements OnInit {

  constructor(private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.providerDataService.getTransferProvidersCategoryTree();
  }

}
