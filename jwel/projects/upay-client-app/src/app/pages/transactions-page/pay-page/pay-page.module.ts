import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {PayPageComponent} from './pay-page.component';
import {payPageRoutes} from './pay-page-routing';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {OperatorPayTerminalComponent} from './operator-pay-terminal/operator-pay-terminal.component';
import {OperatorProviderListComponent} from './operator-provider-list/operator-provider-list.component';
import {SharedWidgetsModule} from '../../../../../../upay-lib/src/lib/widgets/shared-widgets.module';
import {ProviderTerminalModule} from 'upay-lib';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    PayPageComponent,
    OperatorPayTerminalComponent,
    OperatorProviderListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(payPageRoutes),
    TranslateModule,
    WidgetsModule,
    ProviderTerminalModule
  ],
  exports: [
    PayPageComponent
  ],
})
export class PayPageModule {
}
