import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../../../dal/user/user-data.service';
// import {BasketDataService} from '../../../../dal/basket/basket-data.service';
import {AlertBoxService, ProviderService} from 'upay-lib';

@Component({
  selector: 'app-operator-pay-terminal',
  templateUrl: 'operator-pay-terminal.component.html',
  styleUrls: []
})
export class OperatorPayTerminalComponent implements OnInit, OnDestroy {

  labelId: string;
  providerId: string;
  branchId: string;

  // alertBoxVisible: boolean;
  alertBoxText: string;
  alertBoxType: string;

  activeRouteSub = Subscription.EMPTY;

  constructor( // private basketDataService: BasketDataService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private userDataService: UserDataService,
    private alertBoxService: AlertBoxService,
    private providerService: ProviderService) {
  }

  ngOnInit() {
    // this.userDataService.getUserBranch().subscribe(branch => {
    //   this.branchId =  branch.id;
    // });

    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.labelId = routeParams['labelId'];
      this.providerId = routeParams['providerId'];
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
  }

  onAddToFavoriteClick(event) {
    // this.alertBoxVisible = false;
    // event.provider.labelId = this.labelId;
    // fields:
    //   customerId
    this.userDataService.addToFavorite(event.provider, event.fields).subscribe(response => {
      this.alertBoxService.initMsg({type: 'success', text: 'Added to favorites'});

    });
  }

  onPayClick(event) {

    this.providerService.providerPayDebt(event).subscribe(data => {
      this.alertBoxService.initMsg({type: 'success', text: 'Your pay is success'});
      this.router.navigate(['/']);

      console.log(data);
    });
    // const isItemInBasket = this.basketDataService.isItemInBasket(event.transferMappedData.customerIdKey, event.providerKey);

    // todo check min max and commission set

    // if (isItemInBasket) {
    //   this.alertBoxService.initMsg({ type: 'error', text: 'Item is already in basket'});
    //   return;
    // }

    // event['checked'] = true;
    // this.basketDataService.addToBasket(event);
  }
}
