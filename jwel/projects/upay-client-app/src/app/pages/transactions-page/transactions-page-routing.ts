import {Routes} from '@angular/router';
import {TransactionsPageComponent} from './transactions-page.component';

export const transactionsPageRoutes: Routes = [
  {
    path: '',
    component: TransactionsPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'terminal'
      },
      {
        path: 'terminal',
        loadChildren: './pay-page/pay-page.module#PayPageModule',
      }
    ]
  }
];
