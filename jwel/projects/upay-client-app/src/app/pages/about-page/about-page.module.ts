import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {AboutPageComponent} from './about-page.component';
import {aboutRoutes} from './about-page-routing';

@NgModule({
  declarations: [
    AboutPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(aboutRoutes),
    TranslateModule,
  ],
  exports: [
    AboutPageComponent
  ],
})
export class AboutPageModule {
}
