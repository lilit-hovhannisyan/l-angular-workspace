import { Routes } from '@angular/router';
import {ClientDashboardPageComponent} from './client-dashboard-page.component';

export const clientDashboardRoutes: Routes = [
  {
    path: '',
    component: ClientDashboardPageComponent
  }
];
