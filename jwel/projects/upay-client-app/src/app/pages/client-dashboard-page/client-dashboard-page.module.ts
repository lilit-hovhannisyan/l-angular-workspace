import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../widgets/widgets.module';
import {ClientDashboardPageComponent} from './client-dashboard-page.component';
import {clientDashboardRoutes} from './client-dashboard-page-routing';
import {AtomsModules} from 'iu-ui-lib';

@NgModule({
  declarations: [
    ClientDashboardPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientDashboardRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules
  ],
  exports: [
    ClientDashboardPageComponent
  ],
})
export class ClientDashboardPageModule {
}
