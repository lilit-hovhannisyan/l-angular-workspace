import { Routes } from '@angular/router';
import {ServicePayPageComponent} from './service-pay-page.component';
import {ServiceProviderInfoListComponent} from '../../widgets/service-provider-info-list/service-provider-info-list.component';

export const servicePayRoutes: Routes = [
  {
    path: '',
    component: ServicePayPageComponent,
    children: [
      {
        path: ':categoryId',
        component: ServiceProviderInfoListComponent
      }
    ]
  }
];
