import {Component, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

@Component({
  selector: 'app-service-pay-page',
  templateUrl: './service-pay-page.component.html'
})
export class ServicePayPageComponent  implements OnInit {

  constructor(private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.providerDataService.getPayProvidersCategoryTree();
  }

}
