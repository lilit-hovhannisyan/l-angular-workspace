import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ServicePayPageComponent} from './service-pay-page.component';
import {servicePayRoutes} from './service-pay-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    ServicePayPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(servicePayRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ServicePayPageComponent
  ],
})
export class ServicePayPageModule {


}
