import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientCardsPageComponent} from './client-cards-page.component';
import {clientCardsRoutes} from './client-cards-routing';
import {AtomsModules} from 'iu-ui-lib';

@NgModule({
  declarations: [
    ClientCardsPageComponent,
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientCardsRoutes),
    TranslateModule,
    AtomsModules
  ],
  exports: [
    ClientCardsPageComponent
  ],
})
export class ClientCardsPageModule {
}
