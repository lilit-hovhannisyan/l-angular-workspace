import { Routes } from '@angular/router';
import {ClientCardsPageComponent} from './client-cards-page.component';

export const clientCardsRoutes: Routes = [
  {
    path: '',
    component: ClientCardsPageComponent
  }
];
