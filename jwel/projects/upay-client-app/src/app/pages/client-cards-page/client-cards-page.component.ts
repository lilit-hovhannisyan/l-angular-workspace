import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-client-cards-page',
  templateUrl: './client-cards-page.component.html'
})
export class ClientCardsPageComponent implements OnInit {

  constructor(private userDataService: UserDataService) { }

  ngOnInit(): void {
    this.userDataService.getUserCards().subscribe(cards => {
    });
  }

  onAddNewCard() {
    this.userDataService.getBindUserCard().subscribe(addData => {
    });
  }
}
