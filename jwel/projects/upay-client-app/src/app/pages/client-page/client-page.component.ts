import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-client-page',
  templateUrl: './client-page.component.html',
  styleUrls: ['./client-page.component.less']
})
export class ClientPageComponent implements OnInit {

  constructor(private userDataService: UserDataService) {}

  ngOnInit() {
    this.userDataService.getUserCards();
  }
}
