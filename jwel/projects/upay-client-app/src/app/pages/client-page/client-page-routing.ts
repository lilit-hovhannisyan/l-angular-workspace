import {Routes} from '@angular/router';
import {ClientPageComponent} from './client-page.component';

export const clientRoutes: Routes = [
  {
    path: '',
    component: ClientPageComponent,
    children: [
      {
        path: '',
        loadChildren: '../transactions-page/transactions-page.module#TransactionsPageModule',
      },
      {
        path: 'pay',
        redirectTo: 'terminal'
      },
      {
        path: 'dashboard',
        loadChildren: '../client-dashboard-page/client-dashboard-page.module#ClientDashboardPageModule',
      },
      // {
      //   path: 'groups',
      //   loadChildren: '../client-groups-page/client-groups-page.module#ClientGroupsPageModule',
      // },
      {
        path: 'cards',
        loadChildren: '../client-cards-page/client-cards-page.module#ClientCardsPageModule',
      },
      {
        path: 'favorites',
        loadChildren: '../client-favorites-page/client-favorites-page.module#ClientFavoritesPageModule',
      },
      {
        path: 'history',
        loadChildren: '../client-history-page/client-history-page.module#ClientHistoryPageModule',
      },
      {
        path: 'top-up',
        loadChildren: '../client-top-up-page/client-top-up-page.module#ClientTopUpPageModule',
      },
      {
        path: 'transfer',
        loadChildren: '../client-transfer-page/client-transfer-page.module#ClientTransferPageModule',
      },
      {
        path: 'profile',
        loadChildren: '../client-profile-page/client-profile-page.module#ClientProfilePageModule',
      },
    ]
  }
];
