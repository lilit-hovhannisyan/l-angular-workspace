import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {clientRoutes} from './client-page-routing';
import {ClientPageComponent} from './client-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    ClientPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ClientPageComponent
  ],
})
export class ClientPageModule {
}
