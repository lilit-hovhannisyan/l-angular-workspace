import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {clientGroupsRoutes} from './client-groups-page-routing';
import {ClientGroupsPageComponent} from './client-groups-page.component';

@NgModule({
  declarations: [
    ClientGroupsPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientGroupsRoutes),
    TranslateModule,
  ],
  exports: [
    ClientGroupsPageComponent
  ],
})
export class ClientGroupsPageModule {
}
