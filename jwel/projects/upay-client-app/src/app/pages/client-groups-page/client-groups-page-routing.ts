import { Routes } from '@angular/router';
import {ClientGroupsPageComponent} from './client-groups-page.component';

export const clientGroupsRoutes: Routes = [
  {
    path: '',
    component: ClientGroupsPageComponent
  }
];
