import {Component, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

@Component({
  selector: 'app-service-transfer-page',
  templateUrl: './service-transfer-page.component.html'
})
export class ServiceTransferPageComponent  implements OnInit {

  constructor(private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.providerDataService.getTransferProvidersCategoryTree();
  }
}
