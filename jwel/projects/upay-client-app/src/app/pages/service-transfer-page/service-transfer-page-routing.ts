import { Routes } from '@angular/router';
import {ServiceTransferPageComponent} from './service-transfer-page.component';
import {ServiceProviderInfoListComponent} from '../../widgets/service-provider-info-list/service-provider-info-list.component';

export const serviceTransferRoutes: Routes = [
  {
    path: '',
    component: ServiceTransferPageComponent,
    children: [
      {
        path: ':categoryId',
        component: ServiceProviderInfoListComponent
      }
    ]
  }
];
