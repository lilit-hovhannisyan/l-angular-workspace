import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ServiceTransferPageComponent} from './service-transfer-page.component';
import {serviceTransferRoutes} from './service-transfer-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    ServiceTransferPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(serviceTransferRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ServiceTransferPageComponent
  ],
})
export class ServiceTransferPageModule {
}
