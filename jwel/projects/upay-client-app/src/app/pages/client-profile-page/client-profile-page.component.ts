import {Component} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-client-profile-page',
  templateUrl: './client-profile-page.component.html'
})
export class ClientProfilePageComponent {
  constructor(userDataService: UserDataService) {
    userDataService.getProfile().subscribe(data => {
      console.log(data);
    });
  }
}
