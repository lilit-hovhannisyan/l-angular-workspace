import {Routes} from '@angular/router';
import {ClientProfilePageComponent} from './client-profile-page.component';

export const clientProfileRoutes: Routes = [
  {
    path: '',
    component: ClientProfilePageComponent
    // children: [
    //   {
    //     path: 'info',
    //     component: ProfileInfo //widget
    //   },
    //   {
    //     path: 'password',
    //     component: ProfilePassword // widget
    //   },
    //   {
    //     path: 'mobile',
    //     component: ProfileMobile // widget
    //   },
    //   {
    //     path: 'password',
    //     component: ProfileEmail // widget
    //   }
    // ]
  }
];
