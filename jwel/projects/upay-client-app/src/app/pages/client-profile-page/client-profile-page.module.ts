import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientProfilePageComponent} from './client-profile-page.component';
import {clientProfileRoutes} from './client-profile-page-routing';


@NgModule({
  declarations: [
    ClientProfilePageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientProfileRoutes),
    TranslateModule,
  ],
  exports: [
    ClientProfilePageComponent
  ],
})
export class ClientProfilePageModule {
}
