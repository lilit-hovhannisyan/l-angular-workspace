import { Routes } from '@angular/router';
import {ClientHistoryPageComponent} from './client-history-page.component';

export const clientHistoryRoutes: Routes = [
  {
    path: '',
    component: ClientHistoryPageComponent
  }
];
