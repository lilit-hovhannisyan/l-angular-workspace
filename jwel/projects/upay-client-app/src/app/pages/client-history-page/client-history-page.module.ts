import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientHistoryPageComponent} from './client-history-page.component';
import {clientHistoryRoutes} from './client-history-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';


@NgModule({
  declarations: [
    ClientHistoryPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(clientHistoryRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ClientHistoryPageComponent
  ],
})
export class ClientHistoryPageModule {
}
