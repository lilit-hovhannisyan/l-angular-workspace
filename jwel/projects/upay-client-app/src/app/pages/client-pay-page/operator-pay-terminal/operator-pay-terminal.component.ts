import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../../dal/user/user-data.service';
// import {BasketDataService} from '../../../dal/basket/basket-data.service';
import {AlertBoxService} from 'upay-lib';

@Component({
  selector: 'app-operator-pay-terminal',
  templateUrl: 'operator-pay-terminal.component.html',
  styleUrls: []
})
export class OperatorPayTerminalComponent implements OnInit, OnDestroy {

  labelId: string;
  providerId: string;
  branchId: string;

  activeRouteSub = Subscription.EMPTY;

  constructor( // private basketDataService: BasketDataService,
              private activeRoute: ActivatedRoute,
              private userDataService: UserDataService,
              private alertBoxService: AlertBoxService) {}

  ngOnInit() {
    // this.userDataService.getUserBranch().subscribe(branch => {
    //   this.branchId =  branch.id;
    // });
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.labelId = routeParams['labelId'];
      this.providerId = routeParams['providerId'];
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
  }

  pay(event) {
    console.log(event);
    // const isItemInBasket = this.basketDataService.isItemInBasket(event.transferMappedData.customerIdKey, event.providerKey);
    //
    // // todo check min max and commission set
    //
    // if (isItemInBasket) {
    //   this.alertBoxService.initMsg({ type: 'error', text: 'Item is already in basket'});
    //   return;
    // }

    event['checked'] = true;
     // this.userDataService.pay(event);
  }
}
