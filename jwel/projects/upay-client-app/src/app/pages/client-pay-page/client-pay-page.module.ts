import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ClientPayPageComponent} from './client-pay-page.component';
import {clientPayRoutes} from './client-pay-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';
import {ProviderTerminalModule} from 'upay-lib';
import {CommonModule} from '@angular/common';
// import {OperatorPayTerminalComponent} from "../transactions-page/pay-page/operator-pay-terminal/operator-pay-terminal.component";
// import {OperatorProviderListComponent} from "../transactions-page/pay-page/operator-provider-list/operator-provider-list.component";
// import {OperatorPayTerminalComponent} from "../../../../../upay-operator-app/src/app/pages/transactions-page/pay-page/operator-pay-terminal/operator-pay-terminal.component";
// import {OperatorProviderListComponent} from "../../../../../upay-operator-app/src/app/pages/transactions-page/pay-page/operator-provider-list/operator-provider-list.component";
import {OperatorProviderListComponent} from './operator-provider-list/operator-provider-list.component';
import {OperatorPayTerminalComponent} from './operator-pay-terminal/operator-pay-terminal.component';

@NgModule({
  declarations: [
    ClientPayPageComponent,
    OperatorPayTerminalComponent,
    OperatorProviderListComponent,
  ],
  imports: [
    CommonModule,
    ProviderTerminalModule,
    RouterModule.forChild(clientPayRoutes),
    WidgetsModule
  ]
})
export class ClientPayPageModule {
}
