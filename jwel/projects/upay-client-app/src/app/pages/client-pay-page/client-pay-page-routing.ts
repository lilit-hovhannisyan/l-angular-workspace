import { Routes } from '@angular/router';
import {ClientPayPageComponent} from './client-pay-page.component';
import {OperatorProviderListComponent} from './operator-provider-list/operator-provider-list.component';
import {OperatorPayTerminalComponent} from './operator-pay-terminal/operator-pay-terminal.component';
// import {ServiceProviderSelectComponent} from '../../widgets/service-provider-select/service-provider-select.component';
// import {ServiceProviderPayTerminalComponent} from '../../widgets/service-provider-pay-terminal/service-provider-pay-terminal.component';
// import {OperatorPayTerminalComponent} from "../../../../../upay-operator-app/src/app/pages/transactions-page/pay-page/operator-pay-terminal/operator-pay-terminal.component";
// import {OperatorProviderListComponent} from "../../../../../upay-operator-app/src/app/pages/transactions-page/pay-page/operator-provider-list/operator-provider-list.component";

export const clientPayRoutes: Routes = [
  {
    path: '',
    component: ClientPayPageComponent,
    children: [
      {
        path: ':labelId',
        component: OperatorProviderListComponent
      },
      {
        path: ':labelId/:providerId',
        component: OperatorPayTerminalComponent
      }
    ]
  }
];
