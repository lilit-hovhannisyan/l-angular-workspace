import {Component, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

@Component({
  selector: 'app-service-top-up-page',
  templateUrl: './service-top-up-page.component.html'
})
export class ServiceTopUpPageComponent  implements OnInit {

  constructor(private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.providerDataService.getTopUpProvidersCategoryTree();
  }

}
