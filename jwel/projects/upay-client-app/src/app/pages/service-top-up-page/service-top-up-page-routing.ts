import { Routes } from '@angular/router';
import {ServiceTopUpPageComponent} from './service-top-up-page.component';
import {ServiceProviderInfoListComponent} from '../../widgets/service-provider-info-list/service-provider-info-list.component';

export const serviceTopUpRoutes: Routes = [
  {
    path: '',
    component: ServiceTopUpPageComponent,
    children: [
      {
        path: ':categoryId',
        component: ServiceProviderInfoListComponent
      }
    ]
  }
];
