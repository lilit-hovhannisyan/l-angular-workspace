import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ServiceTopUpPageComponent} from './service-top-up-page.component';
import {serviceTopUpRoutes} from './service-top-up-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    ServiceTopUpPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(serviceTopUpRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    ServiceTopUpPageComponent
  ],
})
export class ServiceTopUpPageModule {


}
