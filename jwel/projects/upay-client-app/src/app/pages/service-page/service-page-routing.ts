import { Routes } from '@angular/router';
import {ServicePageComponent} from './service-page.component';

export const serviceRoutes: Routes = [
  {
    path: '',
    component: ServicePageComponent,
    children: [
      {
        path: 'pay',
        loadChildren: '../service-pay-page/service-pay-page.module#ServicePayPageModule',
      },
      {
        path: 'transfer',
        loadChildren: '../service-transfer-page/service-transfer-page.module#ServiceTransferPageModule',
      },
      {
        path: 'top-up',
        loadChildren: '../service-top-up-page/service-top-up-page.module#ServiceTopUpPageModule',
      },
    ]
  }
];
