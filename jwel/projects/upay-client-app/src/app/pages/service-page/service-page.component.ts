import { Component } from '@angular/core';

@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.less']
})
export class ServicePageComponent {
  uwalettItems = [
    {
      icon: 'icon_pay',
      title: 'Pay',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      icon: 'icon_topup',
      title: 'Topup',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      icon: 'icon_transfer',
      title: 'Transfer',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }, {
      icon: 'icon_Add-payment',
      title: 'Add payment',
      decsription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eaque fugiat repellendus sequi soluta vitae.'
    }
  ];
}
