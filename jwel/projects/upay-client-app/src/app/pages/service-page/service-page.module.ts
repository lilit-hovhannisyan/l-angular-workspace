import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ServicePageComponent} from './service-page.component';
import {serviceRoutes} from './service-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';
import {AtomsModules} from 'iu-ui-lib';

@NgModule({
  declarations: [
    ServicePageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(serviceRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules
  ],
  exports: [
    ServicePageComponent
  ],
})
export class ServicePageModule {
}
