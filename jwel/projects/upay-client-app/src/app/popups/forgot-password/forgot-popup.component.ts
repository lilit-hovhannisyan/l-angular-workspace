import {Component} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {PopupsService} from '../popups.service';
import {SiteDataService} from '../../dal/site/site-data.service';

@Component({
  selector: 'app-forgot-popup',
  templateUrl: './forgot-popup.component.html'
})
export class ForgotPopupComponent {

  username: string;

  constructor(private userDataService: UserDataService,
              private popupsService: PopupsService,
              private siteDataService: SiteDataService) {

  }

  onUsernameChange(event) {
    this.username = event;
  }

  onSubmit() {
    this.userDataService.forgotPassword(this.username)
      .subscribe(data => {
        this.siteDataService.setGlobalInfoMessag('we will send your new password by sms');
        this.popupsService.openPopUp('login');
      });
  }

  openPopup( popup ) {
    this.popupsService.openPopUp( popup );
  }
}
