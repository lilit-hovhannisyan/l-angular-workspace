import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';
import {PopupsService} from '../popups.service';

@Component({
  selector: 'app-invoice-popup',
  templateUrl: './invoice-popup.component.html',
  styleUrls: ['invoice-popup.component.less']
})
export class InvoicePopupComponent implements OnInit, OnDestroy {

  providerInvoiceHtml: string;

  constructor(private userDataService: UserDataService,
              private router: Router,
              private popupsService: PopupsService) {

  }

  ngOnInit() {

    this.providerInvoiceHtml = this.popupsService.activePopupData;
  }

  ngOnDestroy() {

  }

}
