import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NgxSmartModalService } from 'ngx-smart-modal';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class PopupsService {

  public Popups = {
    none: '',
    login: 'login',
    forgot: 'forgot',
    invoice: 'invoice'
  };

  public activePopup = new BehaviorSubject(this.Popups.none);
  public activePopupData: any = null;

  constructor() {}

  private setActivePopup(popupName) {
    this.closeActivePopup();
    this.activePopup.next(popupName);
  }

  public openPopUp(popupName, data?: any) {
    this.setActivePopup(this.Popups[popupName]);
    this.activePopupData = data;
  }

  public closeActivePopup() {
    this.activePopupData = null;
    this.activePopup.next(this.Popups.none);
  }

}
