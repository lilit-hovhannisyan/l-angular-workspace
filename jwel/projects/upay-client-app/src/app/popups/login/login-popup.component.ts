import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';
import {PopupsService} from '../popups.service';

@Component({
  selector: 'app-login-popup',
  templateUrl: './login-popup.component.html'
})
export class LoginPopupComponent implements OnInit, OnDestroy {

  username: string;
  password: string;
  isAuthSub = Subscription.EMPTY;

  constructor(private userDataService: UserDataService,
              private router: Router,
              private popupsService: PopupsService) {

  }

  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(data => {

      if (data) {
        this.router.navigate(['/client']);
        this.popupsService.closeActivePopup();
      }
    });
  }

  ngOnDestroy() {
    this.isAuthSub.unsubscribe();
  }

  onUsernameChange(event) {
    this.username = event;
  }

  onPasswordChange(event) {
    this.password = event;
  }

  onSignIn() {
    this.userDataService.signIn(this.username, this.password);
  }

  openPopup(popup) {
    this.popupsService.openPopUp(popup);
  }
}
