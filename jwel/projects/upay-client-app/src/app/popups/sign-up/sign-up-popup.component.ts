import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {PopupsService} from '../popups.service';
import {SiteDataService} from '../../dal/site/site-data.service';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up-popup',
  templateUrl: './sign-up-popup.component.html'
})
export class SignUpPopupComponent implements OnInit, OnDestroy {
  // username: string;
  phoneNumber: string;
  password: string;
  pinCode: string;
  isSignUpActivationCodeSent = false;

  signUpActivationCodeSentSub = Subscription.EMPTY;
  isAuthSub = Subscription.EMPTY;


  constructor(private userDataService: UserDataService,
              private popupsService: PopupsService,
              private router: Router,
              private siteDataService: SiteDataService) {

  }

  ngOnInit() {
    this.isSignUpActivationCodeSent = false;

    this.signUpActivationCodeSentSub = this.userDataService.signUpActivationCodeSent
      .subscribe(data => {
        this.isSignUpActivationCodeSent = data;
      });

    this.isAuthSub = this.userDataService.isAuth.subscribe(data => {

      if (data) {
        this.router.navigate(['/client']);
        this.popupsService.closeActivePopup();
      }
    });
  }

  ngOnDestroy() {
    this.signUpActivationCodeSentSub.unsubscribe();
    this.isAuthSub.unsubscribe();
    this.userDataService.resetSigUpData();
  }


  onPhoneNumberChange(event) {
    this.phoneNumber = event;
  }

  onPasswordChange(event) {
    this.password = event;
  }

  onCodeChange(event) {
    this.pinCode = event;
  }

  // onSendCode(){
  //   this.userDataService.sendActivationCode()
  // }

  onConfirmCode() {
    this.userDataService.activate(this.pinCode);
  }

  onResendCode() {
  }

  onSignUp() {
    this.userDataService.signUp(this.phoneNumber, this.password);
  }

  openPopup(popup) {
    this.popupsService.openPopUp(popup);
  }
}
