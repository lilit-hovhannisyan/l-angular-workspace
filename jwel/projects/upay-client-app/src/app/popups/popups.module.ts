import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NgxSmartModalModule } from 'ngx-smart-modal';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';

import { PopupsService } from './popups.service';
import { LoginPopupComponent } from './login/login-popup.component';
import { PopupManagerComponent } from './popup-manager/popup-manager.component';
import { ForgotPopupComponent } from './forgot-password/forgot-popup.component';
import { SignUpPopupComponent } from './sign-up/sign-up-popup.component';
import {InvoicePopupComponent} from './invoice/invoice-popup.component';

@NgModule({
  declarations: [
    LoginPopupComponent,
    PopupManagerComponent,
    ForgotPopupComponent,
    SignUpPopupComponent,
    InvoicePopupComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    // NgxSmartModalModule.forRoot(),
    AtomsModules,
    MoleculesModule
  ],
  exports: [
    PopupManagerComponent
  ],
  entryComponents: [

  ],
  providers: [
    PopupsService,
  ],
})
export class PopupsModule { }
