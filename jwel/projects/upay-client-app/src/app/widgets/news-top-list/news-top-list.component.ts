import {Component, Input} from '@angular/core';
import {NewsCardListComponent} from '../news-card-list/news-card-list.component';

@Component({
  selector: 'app-news-top-list',
  templateUrl: './news-top-list.component.html',
  styleUrls: ['./news-top-list.component.less']
})
export class NewsTopListComponent extends NewsCardListComponent {


  pageSize = 9;
  @Input() mode: 'smol' | 'normla';

}
