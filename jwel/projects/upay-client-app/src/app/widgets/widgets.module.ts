// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AtomsModules, MoleculesModule, OrganismsModule } from 'iu-ui-lib';

// import { ActivityComponent } from './activity/activity.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { PayActionCardComponent } from './pay-action-card/pay-action-card.component';
import { TransferActionCardComponent } from './transfer-action-card/transfer-action-card.component';

import { AvatarComponent } from './avatar/avatar.component';
import { NotificationComponent } from './notification/notification.component';

import { ClientPageTabsComponent } from './client-page-tabs/client-page-tabs.component';
import { ServiceCategoryNavigationComponent } from './service-category-navigation/service-category-navigation.component';
import {ServiceProviderSelectComponent} from './service-provider-select/service-provider-select.component';
import {ServiceProviderPayTerminalComponent} from './service-provider-pay-terminal/service-provider-pay-terminal.component';
import {NewsCardListComponent} from './news-card-list/news-card-list.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import {ServiceProviderInfoListComponent} from './service-provider-info-list/service-provider-info-list.component';
import {NewsTopListComponent} from './news-top-list/news-top-list.component';
import { MainPageSliderComponent } from './main-page-slider/main-page-slider.component';
import { MainPageServicesComponent } from './main-page-services/main-page-services.component';
import { InfoBannerComponent } from './info-banner/info-banner.component';
import {FavoriteLinkComponent} from './favorites/favoriteLink/favoriteLink.component';
import { TransactionDataListItemComponent } from './transaction-data-list/transaction-data-list-item/transaction-data-list-item.component';
import {TransactionDataListComponent} from './transaction-data-list/transaction-data-list.componentts';
// import {TransactionDataListComponent} from './transaction-data-list/transaction-data-list.component';

@NgModule({
  declarations: [
    TransactionDataListItemComponent,
    TransactionDataListComponent,
    // ActivityComponent,
    FavoritesComponent,
    PayActionCardComponent,
    TransferActionCardComponent,
    AvatarComponent,
    NotificationComponent,
    ClientPageTabsComponent,
    ServiceCategoryNavigationComponent,
    ServiceProviderSelectComponent,
    ServiceProviderPayTerminalComponent,
    NewsCardListComponent,
    NewsDetailsComponent,
    ServiceProviderPayTerminalComponent,
    ServiceProviderInfoListComponent,
    NewsTopListComponent,
    MainPageSliderComponent,
    MainPageServicesComponent,
    InfoBannerComponent,
    FavoriteLinkComponent
  ],
  imports: [
    // BrowserModule,
    // RouterModule,
    CommonModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule
  ],
  exports: [
    // ActivityComponent,
    TransactionDataListComponent,
    FavoritesComponent,
    PayActionCardComponent,
    TransferActionCardComponent,
    AvatarComponent,
    NotificationComponent,
    ClientPageTabsComponent,
    ServiceCategoryNavigationComponent,
    ServiceProviderSelectComponent,
    ServiceProviderPayTerminalComponent,
    NewsCardListComponent,
    NewsDetailsComponent,
    ServiceProviderPayTerminalComponent,
    ServiceProviderInfoListComponent,
    NewsTopListComponent,
    MainPageSliderComponent,
    MainPageServicesComponent,
    InfoBannerComponent
  ],
  providers: [],
})
export class WidgetsModule { }
