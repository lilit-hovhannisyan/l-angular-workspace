import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
})

export class AvatarComponent implements OnInit {
  currentUser: any;

  constructor(private userDataService: UserDataService,
              private router: Router) {
  }

  logout() {
    this.userDataService.signOut();
    this.router.navigate(['/']);
  }

  ngOnInit() {
    this.userDataService.getProfile().subscribe(userData => {
      this.currentUser = userData;
    });
  }
}
