import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-favorite-link',
  templateUrl: './favoriteLink.component.html',
  styleUrls: ['favoriteLink.component.less']
})
export class FavoriteLinkComponent implements OnInit {
  @Input() favoriteData;
  linkText = '';
  providerName = '';
  link;
  params = {};

  ngOnInit() {
    this.link = `../../terminal/_all/${this.favoriteData.provider}`;
    this.providerName = this.favoriteData.provider; // this.favoriteData.provider.split('/')[1];
    for (const field of this.favoriteData.fields) {
      this.params[field.name] = field.value;
      if (this.favoriteData.fields[(this.favoriteData.fields.length - 1)].name !== field.name) {
        this.linkText += field.name + ': ' + field.value + ', ';
      } else {
        this.linkText += field.name + ': ' + field.value;
      }
    }
  }
}
