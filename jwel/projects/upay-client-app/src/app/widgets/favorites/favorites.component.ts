import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Subscription} from 'rxjs/index';
import {UserCardData} from '../../dal/user/user-data.models';
import {SubscriberData} from '../../dal/subscriber/subscriber-data.models';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html'
})
export class FavoritesComponent implements OnInit, OnDestroy {
  @Input() title?: string;
  @Input() pageSize?: number;

  pageNumber = 0;

  favoritesData: SubscriberData[];
  userPaymentCards: UserCardData[];

  userPaymentCardsSub = Subscription.EMPTY;
  favoritesSub = Subscription.EMPTY;

  constructor(private userDataService: UserDataService) {
  }

  ngOnInit() {
    this.userPaymentCardsSub = this.userDataService.userCards.subscribe(cards => {
      this.userPaymentCards = cards;
    });

    this.getFavoritesList();
  }

  ngOnDestroy() {
    this.userPaymentCardsSub.unsubscribe();
    this.favoritesSub.unsubscribe();
  }

  getFavoritesList() {
    this.favoritesSub = this.userDataService.getFavorites(this.pageSize).subscribe(data => {
      this.favoritesData = data;
      console.log(this.favoritesData);
    }, err => {
      console.log(err);
      this.favoritesData = [];
    });
  }

  onPayClick(event) {

    if (!event.payment) {
      // todo Error
      alert('You don\'t select card');
      return;
    }
    if (!event.amount) {
      // todo Error
      alert('You don\'t fill amount');
      return;
    }
    this.userDataService
      .pay(event.customerDebitPayData.subscriberId, event.payment.id, event.amount)
      .subscribe(data => {
        // todo Notification
        alert('Your pay success');
      }, error => {
        // todo Error
        alert('Your pay fail');
      });
  }

  onAddNewCard() {
    console.log('onAddNewCard -- ');
  }

  onFavorite(event) {
    // this.userDataService.addToFavorite(event);
    console.log('onFavorite -- ', event);
  }

  onRecurrent(event) {
    console.log('onRecurrent -- ', event);
  }
}
