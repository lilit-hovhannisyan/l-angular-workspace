import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page-services',
  templateUrl: './main-page-services.component.html',
  styleUrls: ['./main-page-services.component.less']
})
export class MainPageServicesComponent {
  services = [
    {
      icon : 'icon_upay-logo',
      title : 'u wallet',
      description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque dolorum nobis quidem rerum tempore velit?',
      path: 'u-wallet'
    }, {
      icon : 'icon_branches',
      title : 'u branch',
      description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque dolorum nobis quidem rerum tempore velit?',
      path: 'u-branch'
    }
  ];

  constructor (private router: Router ) {}

  redirect( path ) {
    this.router.navigate([path]);
  }
}
