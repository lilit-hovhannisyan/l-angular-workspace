import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-info-banner',
  templateUrl: './info-banner.component.html'
})

export class InfoBannerComponent {
  @Input() title: string;
  @Input() items;
  @Input() src?: string;
  @Input() alt?: string;
}

