import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProviderDataService} from '../../dal/provider/provider-data.service';
import {Subscription} from 'rxjs/index';
import {ProviderCategoryData} from '../../dal/provider/provider-data.models';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-service-category-navigation',
  templateUrl: './service-category-navigation.component.html',
  styleUrls: ['./service-category-navigation.component.less']
})
export class ServiceCategoryNavigationComponent implements OnInit, OnDestroy {
  @Input () routPath: string;

  serviceCategories: ProviderCategoryData[] = [];
  serviceCategoriesSub = Subscription.EMPTY;
  activeRouteSub = Subscription.EMPTY;

  constructor(private providerDataService: ProviderDataService,
              public router: Router,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {

    this.serviceCategoriesSub = this.providerDataService.providersCategoryTree.subscribe(ct => {
      this.serviceCategories = ct;
      if (this.routPath === this.router.url) {
        const first = this.serviceCategories[0];
        if (first) {
          this.router.navigateByUrl(`${this.routPath}/${first.name}`);
        }
      }
    });
  }

  ngOnDestroy() {
    this.serviceCategoriesSub.unsubscribe();
    this.activeRouteSub.unsubscribe();
  }

}
