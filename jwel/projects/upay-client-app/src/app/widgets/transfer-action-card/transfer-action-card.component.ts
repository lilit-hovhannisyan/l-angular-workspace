import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-transfer-action-card',
  templateUrl: './transfer-action-card.component.html',
  styleUrls: ['./transfer-action-card.component.less']
})
export class TransferActionCardComponent {

  constructor(private router: Router) {}

  onTransferClick() {
    // this.router.navigate(['/client/pay']);
  }
}
