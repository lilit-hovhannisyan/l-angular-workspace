import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';
import {NewsDataService} from '../../dal/news/news-data.service';
import {NewsData} from '../../dal/news/news-data.models';

@Component({
  selector: 'app-news-card-list',
  templateUrl: './news-card-list.component.html',
  styleUrls: ['./news-card-list.component.less']
})
export class NewsCardListComponent implements OnInit, OnDestroy {

  currentPage: number;
  pageSize = 9;

  newsTotalCountSub = Subscription.EMPTY;
  newsTotalCount: number;
  newsListSub = Subscription.EMPTY;
  newsList: NewsData[];

  constructor(public router: Router,
              private newsDataService: NewsDataService) {
  }

  ngOnInit() {
    this.setCurrentPage(0);
    this.newsTotalCountSub = this.newsDataService.newsTotalCount
      .subscribe(count => {
        this.newsTotalCount = count;
      });
  }

  ngOnDestroy() {
    this.newsListSub.unsubscribe();
    this.newsTotalCountSub.unsubscribe();
  }

  setCurrentPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.getNewsList();
  }

  getNewsList() {
    this.newsListSub.unsubscribe();
    this.newsListSub = this.newsDataService.getNewsList(this.currentPage, this.pageSize)
      .subscribe(data => {
        this.newsList = data;
      });
  }

  readMore(news: NewsData) {
    this.router.navigateByUrl(`/news/${news.id}`);
  }

  getPages(page) {
    this.setCurrentPage(page - 1);
  }
}
