import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProviderDataService} from '../../dal/provider/provider-data.service';
import {Subscription} from 'rxjs/index';
import {ProviderData} from '../../dal/provider/provider-data.models';

@Component({
  selector: 'app-service-provider-info-list',
  templateUrl: './service-provider-info-list.component.html',
})
export class ServiceProviderInfoListComponent implements OnInit, OnDestroy {

  providers:  ProviderData[] = [];
  categoryId: string;
  currentCategorySub =  Subscription.EMPTY;
  activeRouteSub = Subscription.EMPTY;

  constructor(private activeRoute: ActivatedRoute,
              private providerDataService: ProviderDataService) {
  }

  ngOnInit() {
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.categoryId = routeParams['categoryId'];

      this.currentCategorySub.unsubscribe();
      this.currentCategorySub = this.providerDataService
        .getCategoryProviders(this.categoryId)
        .subscribe(d => {
          this.providers = d.services; // d.services;
        });
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
    this.currentCategorySub.unsubscribe();
  }
}
