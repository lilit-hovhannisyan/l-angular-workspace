import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {ProviderDataService} from '../../dal/provider/provider-data.service';
import {ProviderCategoryData} from '../../dal/provider/provider-data.models';
import {SubscriberDataService} from '../../dal/subscriber/subscriber-data.service';
import {SubscriberData} from '../../dal/subscriber/subscriber-data.models';
import {UserDataService} from '../../dal/user/user-data.service';
import {UserCardData} from '../../dal/user/user-data.models';

@Component({
  selector: 'app-service-provider-pay-terminal',
  templateUrl: './service-provider-pay-terminal.component.html',
})
export class ServiceProviderPayTerminalComponent implements OnInit, OnDestroy {

  categoryId: string;
  typeId: string;

  currentCategory: ProviderCategoryData;
  currentCategoryService;
  goBackUrl: string;

  userPaymentCards: UserCardData[];
  customerDebitPayData: SubscriberData;

  activeRouteSub = Subscription.EMPTY;
  currentCategorySub = Subscription.EMPTY;
  subscriberDataSub = Subscription.EMPTY;
  userPaymentCardsSub = Subscription.EMPTY;

  constructor(private providerDataService: ProviderDataService,
              private subscriberDataService: SubscriberDataService,
              private userDataService: UserDataService,
              private activeRoute: ActivatedRoute) {}

  ngOnInit() {
    this.userPaymentCardsSub = this.userDataService.userCards.subscribe(cards => {
      this.userPaymentCards = cards;
    });

    this. activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.categoryId = routeParams['categoryId'];
      this.typeId = routeParams['typeId'];

      this.setBackUrl(this.categoryId);

      this.currentCategorySub.unsubscribe();
      this.currentCategorySub = this.providerDataService
        .getCategoryProviders(this.categoryId)
        .subscribe(d => {
          this.currentCategory = d;

          this.currentCategoryService = d['services'].find(s => {
            return this.typeId === s.name;
          });
        });
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
    this.currentCategorySub.unsubscribe();
    this.subscriberDataSub.unsubscribe();
    this.userPaymentCardsSub.unsubscribe();
  }

  setBackUrl(categoryId) {
    this.goBackUrl =  '/client/pay/' + categoryId;
  }

  onSearch(searchText) {
    this.subscriberDataSub.unsubscribe();
    this.subscriberDataSub = this.subscriberDataService.getSubscriberData(searchText)
      .subscribe(d => {
        this.customerDebitPayData = d;
      });
  }

  onPayClick(event) {
    if (!event.payment) {
      // todo Error
      console.log('You don\'t select card ');
      return;
    }
    if (!event.amount) {
      // todo Error
      console.log('You don\'t fill amount ');
      return;
    }
    this.userDataService
      .pay(event.customerDebitPayData.subscriberId, event.payment.id, event.amount)
      .subscribe(data => {
        // todo Notification
        console.log('Your pay success');
      }, error => {
        // todo Error
        console.log('Your pay fail');
      });
  }
}
