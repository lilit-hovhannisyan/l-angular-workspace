import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-main-page-slider',
  templateUrl: './main-page-slider.component.html',
  styleUrls: ['./main-page-slider.component.less']
})
export class MainPageSliderComponent implements OnInit {

  slideList: any[] = [];

  constructor() {
  }

  buttonClicked() {
    alert('button clicked..');
  }

  ngOnInit() {
    this.slideList.push({
      imageUrl: 'assets/img/slide1.png',
      mainTitle: 'First Slide Main Title',
      title: 'First Slide Title',
      description: 'Եթե  որևէ խնդիր կամ հարց ունեք կապված մեր ծառայությունների և դրանց որակի հետ, կարող եք կապվել մեր:',
      buttonTitle: 'read More',
      buttonCallback: () => {
        console.log('Alert.');
        alert('1th slide button');
      }
    });
    this.slideList.push(
      {
        imageUrl: 'assets/img/slide2.png',
        mainTitle: 'Second Slide Main Title',
        title: 'Second Slide Title',
        description: 'Եթե  որևէ խնդիր կամ հարց ունեք կապված մեր ծառայությունների և դրանց որակի հետ, կարող եք կապվել մեր:',
        buttonTitle: 'read More',
        buttonCallback: () => {
          alert('2th slide button');
        }
      });
    this.slideList.push(
      {
        imageUrl: 'assets/img/slide3.png',
        mainTitle: 'Third Slide Main Title',
        title: 'Third Slide Title',
        description: 'Եթե  որևէ խնդիր կամ հարց ունեք կապված մեր ծառայությունների և դրանց որակի հետ, կարող եք կապվել մեր:',
        buttonTitle: 'read More',
        buttonCallback: () => {
          alert('3th slide button');
        }
      });
    this.slideList.push(
      {
        imageUrl: 'assets/img/slide4.png',
        mainTitle: 'Forth Slide Main Title',
        title: 'Forth Slide Title',
        description: 'Եթե  որևէ խնդիր կամ հարց ունեք կապված մեր ծառայությունների և դրանց որակի հետ, կարող եք կապվել մեր:',
        buttonTitle: 'read More',
        buttonCallback: () => {
          alert('4th slide button');
        }
      });
  }

}
