import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, of, Subscription} from 'rxjs/index';
import {NewsDataService} from '../../dal/news/news-data.service';
import {NewsData} from '../../dal/news/news-data.models';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.less']
})
export class NewsDetailsComponent implements OnInit, OnDestroy {
  cardList$: Observable<any[]>;

  activeRouteSub = Subscription.EMPTY;
  newsDetailsSub = Subscription.EMPTY;
  newsDetails: NewsData;

  constructor(private newsDataService: NewsDataService,
              private activeRoute: ActivatedRoute, ) {

  }

  ngOnInit() {
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      const newsId = routeParams['newsId'];
      this.newsDetailsSub.unsubscribe();
      this.newsDetailsSub = this.newsDataService.getNewsById(newsId)
        .subscribe(details => {
          this.newsDetails = details;
        });
    });

    this.cardList$ = of([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);
  }

  ngOnDestroy() {
    this.newsDetailsSub.unsubscribe();
  }

}
