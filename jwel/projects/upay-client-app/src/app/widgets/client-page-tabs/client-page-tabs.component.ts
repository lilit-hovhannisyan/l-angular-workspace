import { Component, OnInit } from '@angular/core';
import { SiteDataService } from '../../dal/site/site-data.service';
import { ClientTabView } from '../../dal/site/site-data.models';

@Component ({
  selector: 'app-client-page-tabs',
  templateUrl: './client-page-tabs.component.html',
  styleUrls: ['./client-page-tabs.component.less']
})

export class ClientPageTabsComponent implements OnInit {
  tabMainClass = 'link-btn ';
  tabPathPrefix = '/client';
  tabs: ClientTabView[];

  constructor(private siteDataService: SiteDataService) {}

  ngOnInit() {
    this.tabs = this.siteDataService.clientTabs;
  }

}
