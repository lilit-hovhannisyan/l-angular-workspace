import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { IActivityData } from './transaction-data-list-item/transaction-data-list-item.component';
import { Order, Page } from 'upay-lib';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-transaction-data-list',
  templateUrl: './transaction-data-list.component.html',
  styleUrls: ['./transaction-data-list.component.less']
})
export class TransactionDataListComponent implements OnInit {
 // @ContentChild(TemplateRef) itemTemplateRef: TemplateRef<any>;
  activityDataSource: any[]; // IActivityData[];
  @Input() activityIdField?: string;
  @Input() payAgainText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;
  @Input() title?: string;

  @Input() iconClassName_toggle?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;

  @Output() activityPayAgainClick = new EventEmitter<IActivityData>();
  @Output() activityFavoriteClick = new EventEmitter<IActivityData>();
  @Output() activityRecurrentClick = new EventEmitter<IActivityData>();

  pageNumber = 0;
  @Input() pageSize = 10;
  @Input() hasPagination = true;
  totalItems = 0;

  constructor(private userDataService: UserDataService) {}

  ngOnInit() {
    this.getActivityList();
  }

  pageChange(event) {
    this.pageNumber = event - 1;
    this.getActivityList();
  }

  getActivityList() {

    const page = new Page();
    page.index = this.pageNumber;
    page.size =  this.pageSize;


    const order = new Order();
    order['fields'] = []; // this.orderedFieldsArr;

    this.userDataService
      .getUserTransactions(page, order)
      .subscribe(activityData => {
        this.totalItems = activityData.total;
        this.activityDataSource = activityData.items;
      });
  }

  onOpenActivity(event) {
    // todo create logic
    // this.activityClick.emit(activity);
  }

  onActivityPayAgainClick(activity: IActivityData) {
    this.activityPayAgainClick.emit(activity);
  }

  onActivityFavoriteClick(activity: IActivityData) {
    this.activityFavoriteClick.emit(activity);
  }

  onActivityRecurrentClick(activity: IActivityData) {
    this.activityRecurrentClick.emit(activity);
  }

}
