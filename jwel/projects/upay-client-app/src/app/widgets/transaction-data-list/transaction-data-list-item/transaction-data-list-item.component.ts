import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {ProviderService} from 'upay-lib';
import {Router} from '@angular/router';
import {PopupsService} from '../../../popups/popups.service';

export interface IServiceDetail {
  name: string;
  value: string;
}

export interface IActivityData {
  icon: string;
  title: string;
  date: string | number;
  amount: number;
  currency: string;
  isFavorite: boolean;
  isRecurrent: boolean;
  serviceDetails: IServiceDetail[];
}

@Component({
  selector: 'app-transaction-data-list-item',
  templateUrl: './transaction-data-list-item.component.html',
  styleUrls: ['./transaction-data-list-item.component.less']
})
export class TransactionDataListItemComponent implements OnInit {
  @Input() activityData: any; // : IActivityData;
  @Input() payAgainText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;

  @Input() iconClassName_toggle?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;

  @Output() showDetailsClick = new EventEmitter<boolean>();
  @Output() payAgainClick = new EventEmitter<IActivityData>();
  @Output() favoriteClick = new EventEmitter<IActivityData>();
  @Output() recurrentClick = new EventEmitter<IActivityData>();

  public isDetailsOpen = false;
  public provider: any;

  constructor(private providerService: ProviderService,
              private popupsService: PopupsService,
              private router: Router) {}

  ngOnInit() {
    this.providerService.getConfiguredProvidersByProviderKey(this.activityData.providerName).subscribe(p => {
      this.provider = p;
    });
  }

  getAmountType() {
    if ( this.activityData.amount < 0 ) {
      return 'iu-ui-negative';
    }
    return 'iu-ui-positive';
  }

  onShowDetailsClick() {
    this.isDetailsOpen = !this.isDetailsOpen;
    this.showDetailsClick.emit(this.isDetailsOpen);
  }

  onPayAgainClick() {
    let queryParams = null;
    const clientData = JSON.parse(this.activityData.clientData);
    if (clientData) {
      queryParams = clientData.customerDebtData;
    }

    this.router.navigate([
      `client/terminal/_all/${this.provider.config.providerKey}`],
      {queryParams: queryParams});
    // this.payAgainClick.emit(this.activityData);
  }


  onShowInvoiceClick() {
    const clientData = JSON.parse(this.activityData.clientData);
    // const clientData = JSON.parse(invoiceData.clientData);
    const companyData = {companyName: 'company.name', companyTin: 'company.tin'};
    // const branchData = clientData.branch;
    // const operatorData = clientData.operatorData;

    const receiptData = {
      receiptId: this.activityData.receiptId,
      purpose:  this.activityData.purpose,
      issued: this.activityData.issued
    };


    // clientData.customerDebtData,
    //       clientData.amount,
    //       clientData.providerData,
    //       clientData.commission,
    //       receiptData,
    //       companyData

    const invoiceHtml = this.providerService.getProviderInvoiceHtml({
      debtData: clientData.customerDebtData,
      amount: clientData.amount,
      provider: clientData.providerData,
      commissionValue: clientData.commission,
      receiptData: receiptData,
      companyData: companyData
    });


    this.popupsService.openPopUp('invoice', invoiceHtml);
  }

  onFavoriteClick() {

    this.favoriteClick.emit(this.activityData);
  }

  onRecurrentClick() {
    this.recurrentClick.emit(this.activityData);
  }

}
