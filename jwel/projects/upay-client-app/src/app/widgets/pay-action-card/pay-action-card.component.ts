import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pay-action-card',
  templateUrl: './pay-action-card.component.html',
  styleUrls: ['./pay-action-card.component.less']
})
export class PayActionCardComponent {

  constructor(private router: Router) {}

  onPayClick() {
    this.router.navigate(['/client/terminal']);
  }
}
