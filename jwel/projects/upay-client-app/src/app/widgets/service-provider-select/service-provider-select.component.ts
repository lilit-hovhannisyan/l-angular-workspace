import {Component, OnDestroy, OnInit} from '@angular/core';
import {SiteDataService} from '../../dal/site/site-data.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {ProviderDataService} from '../../dal/provider/provider-data.service';
import {ProviderData} from '../../dal/provider/provider-data.models';

@Component({
  selector: 'app-service-provider-select',
  templateUrl: './service-provider-select.component.html',
  styleUrls: ['./service-provider-select.component.less']
})
export class ServiceProviderSelectComponent implements OnInit, OnDestroy {

  providers: ProviderData[] = [];
  categoryId: string;
  activeRouteSub = Subscription.EMPTY;
  currentCategorySub = Subscription.EMPTY;

  constructor(private providerDataService: ProviderDataService,
              private activeRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.categoryId = routeParams['categoryId'];

      this.currentCategorySub.unsubscribe();
      this.currentCategorySub = this.providerDataService
        .getCategoryProviders(this.categoryId)
        .subscribe(d => {
          this.providers = d.services;
        });
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
    this.currentCategorySub.unsubscribe();
  }

  getRoutLink(providerId) {
    return '/client/terminal/' + this.categoryId + '/' + providerId;
  }
}
