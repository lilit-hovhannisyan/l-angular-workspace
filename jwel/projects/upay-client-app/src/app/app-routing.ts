import { Routes } from '@angular/router';
import { MainLayoutComponent } from './core/main-layout/main-layout.component';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {NoAuthGuardService} from './core/guards/no-auth-guard.service';

export const appRoutes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        canActivate: [NoAuthGuardService],
        loadChildren: './pages/home-page/home-page.module#HomePageModule',
      },
      {
        path: 'about',
        loadChildren: './pages/about-page/about-page.module#AboutPageModule',
      },
      {
        path: 'news',
        loadChildren: './pages/news-list-page/news-list-page.module#NewsListPageModule',
      },
      {
        path: 'service',
        loadChildren: './pages/service-page/service-page.module#ServicePageModule',
      },
      {
        path: 'client',
        canActivate: [AuthGuardService],
        loadChildren: './pages/client-page/client-page.module#ClientPageModule'
      },
    ]
  },
  {
    path: 'page-not-found',
    loadChildren: './pages/not-found-page/not-found-page.module#NotFoundPageModule'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/page-not-found'
  }
];

