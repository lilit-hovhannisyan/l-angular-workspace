import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../dal/language.service';

@Component({
  selector: 'app-language-picker',
  templateUrl: './language-picker.component.html'
})
export class LanguagePickerComponent implements OnInit {

  public supportedLanguages = LanguageService.supportedLanguages;
  public selectedLanguage: string;

  constructor(private languageService: LanguageService) {}

  ngOnInit() {
    this.selectedLanguage = this.languageService.selectedLanguage;
  }

  onLanguageChange(e) {
    this.languageService.changeLanguage(e);
  }

}
