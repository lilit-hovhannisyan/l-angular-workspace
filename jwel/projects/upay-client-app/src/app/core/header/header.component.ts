import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import {PopupsService} from '../../popups/popups.service';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  navigation;

  navigationNotAuth =  [
    {
      path: 'general',
      text: 'General'
    },
    {
      path: '/service',
      text: 'Service'
    },
    {
      path: 'u-branch',
      text: 'U-branch'
    },
    {
      path: 'contacts',
      text: 'Contacts'
    }
  ];

  navigationIsAuth =  [
    {
      path: '/client/cards',
      text: 'Cards'
    },
    {
      path: '/client/favorites',
      text: 'Favorites'
    },
    {
      path: '/client/history',
      text: 'History'
    }
  ];

  logoImg = {
    src: 'assets/publicarea/images/logo.svg',
    alt: 'UPay',
  };

  isAuth: boolean;
  isAuthSub = Subscription.EMPTY;
  constructor(private router: Router,
              private popupService:  PopupsService,
              private userDataService: UserDataService) {}

  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(auth => {
      this.isAuth = auth;
      if (this.isAuth) {
        this.navigation = this.navigationIsAuth;
      } else {
        this.navigation = this.navigationNotAuth;
      }
    });
  }

  openPopup( popup ) {
    this.popupService.openPopUp( popup );
  }

}
