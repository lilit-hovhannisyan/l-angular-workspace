export class NewsData {
  id: string;
  imageUrl: string;
  title: string;
  description: string;
  date: Date;

  constructor(data) {
    this.id = data.id;
    this.imageUrl = data.imageUrl;
    this.title = data.title;
    this.description = data.description;
    this.date = new Date(data.date);
  }
}

