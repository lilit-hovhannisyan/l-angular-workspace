/* tslint:disable:max-line-length */
import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {NewsData} from './news-data.models';

// todo Jnjel
const newsListTestData = [
  {
    id: '1',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'What is Lorem Ipsum?',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
  },
  {
    id: '2',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Why do we use it?',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
  {
    id: '3',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '4',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '5',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'What is Lorem Ipsum?',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
  },
  {
    id: '6',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Why do we use it?',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
  {
    id: '7',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '8',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '9',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Why do we use it?',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
  {
    id: '10',
    date: 1544380456176,
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '11',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '12',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '13',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Why do we use it?',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
  {
    id: '14',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '15',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '16',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Why do we use it?',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
  {
    id: '17',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '18',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '19',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where does it come from?',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  },
  {
    id: '20',
    imageUrl: 'assets/clientarea/images/news-card.png',
    title: 'Where can I get some sddvdfgfd',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old'
  }
];

// todo end

@Injectable()
export class NewsDataService {

  _newsTotalCount: number;
  newsTotalCount = new BehaviorSubject(this._newsTotalCount);

  getNewsList(page: number, size: number) {
    const _testData = newsListTestData.slice(page * size, page * size + size);
    // todo api
    const newsList = _testData.map(d => new NewsData(d));
    this.newsTotalCount.next(newsListTestData.length);
    return new BehaviorSubject(newsList);
  }

  getNewsById(newsId: string) {
    // todo api
    const news = newsListTestData.find(n => {
      return n.id === newsId;
    });

    return new BehaviorSubject(new NewsData(news));
  }

}
