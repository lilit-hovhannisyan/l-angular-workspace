import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/index';
import {SubscriberData} from './subscriber-data.models';

// todo Jnjel
const testData = {
  subscriberId: '123456',
  icon: 'icon_utility',
  title: 'Electricity',
  isFavorite: false,
  isRecurrent: false,
  commissionPercent: 0,
  customerDetails: [
    {
      name: 'Subscriber number',
      value: '16457'
    },
    {
      name: 'User',
      value: 'Hovhannes Hovhannisyan'
    },
    {
      name: 'Telephone',
      value: '+37490909090'
    },
    {
      name: 'Address',
      value: 'Nor Norq, Er 7'
    },
  ],
  debtDetails: [
    {
      name: 'Daytime conumption',
      value: '255 kWH'
    },
    {
      name: 'Nighttime conumption',
      value: '255 kWH'
    },
    {
      name: 'Subsidized amount',
      value: '0.00 amd'
    },
    {
      name: 'To be paid',
      value: '10.00 amd'
    },
    {
      name: 'From 01.03 paid via UPay',
      value: '10.00 amd'
    },
  ]
};

// todo end

@Injectable()
export class SubscriberDataService {

  getSubscriberData(subscriberId: string): Observable<SubscriberData> {
    // todo Api
    const subscriberData = new SubscriberData(testData);
    return new BehaviorSubject(subscriberData);
  }
}
