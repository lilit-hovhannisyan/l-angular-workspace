export class SubscriberDetailData {
  name: string;
  value: string;

  constructor(data) {
    this.name = data.name;
    this.value = data.value;
  }
}

export class DebtDetailData {
  name: string;
  value: string;

  constructor(data) {
    this.name = data.name;
    this.value = data.value;
  }
}

export class SubscriberData {
  subscriberId: string;
  icon: string;
  title: string;
  isFavorite: boolean;
  isRecurrent: boolean;
  commissionPercent: number;
  customerDetails: SubscriberDetailData[];
  debtDetails: DebtDetailData[];

  constructor(data) {
    this.subscriberId = data.subscriberId;
    this.icon = data.icon;
    this.title = data.title;
    this.isFavorite = data.isFavorite;
    this.isRecurrent = data.isRecurrent;
    this.commissionPercent = data.commissionPercent;

    this.customerDetails = data.customerDetails.map(d => new SubscriberDetailData(d));
    this.debtDetails = data.debtDetails.map(d => new DebtDetailData(d));
  }
}


