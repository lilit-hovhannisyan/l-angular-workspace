import {Injectable} from '@angular/core';
import {BehaviorSubject, of, Subject} from 'rxjs/index';
import {v4 as uuid} from 'uuid';
import {UserActivityData,  UserCardData} from './user-data.models';
import {HttpClient} from '@angular/common/http';
import {SubscriberData} from '../subscriber/subscriber-data.models';
// import {
//   CustomerApi, CustomersActivateCommand, CustomersGetAccessTokenQuery,
//   CustomersSendActivationCodeCommand,
//   CustomersSignUpCommand
// } from '../../api/index';

import {
  CustomerApi, CustomersActivateCommand, CustomersGetTokenQuery,
  CustomersSendActivationCodeCommand,
  CustomersSignUpCommand,
  CustomersGetFavoritesQuery,
  CustomersGetProfileQuery
} from 'upay-lib';

import {CustomersAuthenticateCommand} from '../../api';
import {map, switchMap} from 'rxjs/operators';
import {UserBranchInfoData, UserInfoData} from '../../../../../upay-operator-app/src/app/dal/user/user-data.models';
import {ICashboxTransactionsFilter} from '../../../../../upay-operator-app/src/app/dal/cashbox/cashbox-data.service';
import {
  CustomersGetTransactionsQuery,
  Field,
  Order,
  Page,
  ProviderService } from 'upay-lib';
import {
  CardsBindCardCommand,
  CardsGetBindingUrlQuery,
  CustomersGetCardsQuery
} from 'upay-lib';
import {FavoriteType} from '../../../../../upay-lib/src/lib/api';

@Injectable()
export class UserDataService {

  _userCards: UserCardData[];
  userCards = new BehaviorSubject(this._userCards);
  _isAuth: boolean;
  isAuth = new BehaviorSubject(this._isAuth);
  _signUpActivationCodeSent = false;
  signUpActivationCodeSent = new BehaviorSubject(this._signUpActivationCodeSent);
  signUpRegId = null;
  _userInfo?: UserInfoData;
  userInfo = new BehaviorSubject(this._userInfo);

  // signUpRegId = uuid();

  constructor(protected http: HttpClient,
              private providerService: ProviderService,
              private customerApi: CustomerApi) {


    if (localStorage.getItem('authToken')) {
      this._isAuth = true;
    } else {
      this._isAuth = false;
    }
    this.isAuth.next(this._isAuth);

  }


  sendActivationCode(regId) {
    const customersSendActivationCodeCommand = new CustomersSendActivationCodeCommand();
    customersSendActivationCodeCommand.regId = regId;
    this.customerApi.sendActivationCode(customersSendActivationCodeCommand).subscribe(r => {
      this._signUpActivationCodeSent = true;
      this.signUpActivationCodeSent.next(this._signUpActivationCodeSent);
      console.log(r);
    });
  }

  signUp(phone: string, password: string) {
    this.signUpRegId = uuid();
    const customersSignUpCommand = new CustomersSignUpCommand();
    customersSignUpCommand.regId = this.signUpRegId;
    customersSignUpCommand.phone = phone;
    customersSignUpCommand.password = password;
    this.customerApi.signUp(customersSignUpCommand).subscribe(r => {
      this.sendActivationCode(this.signUpRegId);
      console.log(r);
    });
  }

  resetSigUpData() {
    this.signUpRegId = null;
    this._signUpActivationCodeSent = false;
    this.signUpActivationCodeSent.next(this._signUpActivationCodeSent);
  }

  activate(pincode: string) {
    const customersActivateCommand = new CustomersActivateCommand();
    customersActivateCommand.regId = this.signUpRegId;
    customersActivateCommand.pincode = pincode;
    this.customerApi.activate(customersActivateCommand).subscribe(r => {
      this.setUserToken(this.signUpRegId);
      this.resetSigUpData();
    });
  }

  setUserToken(id) {
    const requestData = new CustomersGetTokenQuery();
    requestData.authenticationId = id;
    this.customerApi.getToken(requestData).subscribe(authToken => {
      localStorage.setItem('authToken', authToken.accessToken);
      this._isAuth = true;
      this.isAuth.next(this._isAuth);
    });
  }


  signIn(username: string, password: string) {
    const customersAuthenticateCommand = new CustomersAuthenticateCommand();
    const authenticationId = uuid();
    customersAuthenticateCommand.authenticationId = authenticationId;
    customersAuthenticateCommand.password = password;
    customersAuthenticateCommand.phone = username;

    return this.customerApi.authenticate(customersAuthenticateCommand).pipe(map(tokenData => {
      this.setUserToken(authenticationId);

      // const customersGetTokenQuery = new CustomersGetTokenQuery();
      // customersGetTokenQuery.authenticationId = authenticationId;
      // return this.customerApi.getToken(customersGetTokenQuery).subscribe(data => {

      // })
    })).subscribe();

    // todo api;
    // return this.http.get('https://jsonplaceholder.typicode.com/todos/1').subscribe(data => {
    //   if (username === 'upayuser' && password === 'Ap123456' ) {
    //     const authToken = 'djfdisjfjsidfjifje32';
    //     localStorage.setItem('authToken', authToken);
    //     this._isAuth = true;
    //     this.isAuth.next(this._isAuth);
    //   } else {
    //     // todo design
    //     alert('sign in  error');
    //   }
    // });
  }

  forgotPassword(username: string) {
    // todo api
    return this.http.get('https://jsonplaceholder.typicode.com/todos/1');
  }

  signOut() {
    localStorage.removeItem('authToken');
    this._isAuth = false;
    this.isAuth.next(this._isAuth);
  }


  getBindUserCard() {
    const cardId = uuid();
    const cardsBindCardCommand = new CardsBindCardCommand();
    cardsBindCardCommand.cardId = cardId;
    return this.customerApi.bindCard(cardsBindCardCommand).pipe(switchMap(data => {
      return of(data);
    }));

    // subscribe(data => {
    //   const cardsGetBindingUrlQuery = new CardsGetBindingUrlQuery();
    //   cardsGetBindingUrlQuery.cardId = cardId;
    //   return this.customerApi.getBindingUrl(cardsGetBindingUrlQuery);
    //   debugger
    // });
  }


  getUserCards() {
    const customersGetCardsQuery = new CustomersGetCardsQuery();
    return this.customerApi.getCards(customersGetCardsQuery);
    // todo Api
    // this._userCards = cardTestData.map(d => new UserCardData(d));
    // this.userCards.next(this._userCards);
  }

  getProfile() {
    // if (!this._userInfo) {
    const customersGetProfileQuery = new CustomersGetProfileQuery();
    return this.customerApi.getProfile(customersGetProfileQuery).pipe(map(userRes => {
      this._userInfo = new UserInfoData(userRes);
      this.userInfo.next(this._userInfo);
      return userRes;
      // this.languageService.changeLanguage(languageCode) //todo
      // this.getUserBranch().subscribe(branchRes => {
      //   this._branchInfo = new UserBranchInfoData(branchRes);
      //   this.branchInfo.next(this._branchInfo);
      // });
    }));
  }

  pay(subscriberNumber: string, cardId: string, amount: string) {
    // todo Api
    // return this.http.post('https://jsonplaceholder.typicode.com/todos/1', {
    //   subscriberNumber,
    //   cardId,
    //   amount
    // });
    return this.http.get('https://jsonplaceholder.typicode.com/todos/1');

  }


  getFavorites(size: number = 0) {
    const customersGetFavoritesQuery = new CustomersGetFavoritesQuery();
    return this.customerApi.getFavorites(customersGetFavoritesQuery).pipe(map(data => {
      if (size) {
        const favorites = [];
        for (let i = 0; i < size; i++) {
          if (i < data.length) {
            favorites.push(data[i]);
          } else {
            break;
          }
        }
        return favorites;
      } else {
        return data;
      }
    }));
  }

  addToFavorite(provider: any, searchFields: any) {

    const fieldsArr = [];

    for (const fieldName in searchFields) {
      if (searchFields.hasOwnProperty(fieldName)) {
        fieldsArr.push({
          name: fieldName,
          value: searchFields[fieldName]
        });
      }
    }



    return this.getFavorites().pipe(// todo vat api
      switchMap(oldFavorites => {

        const newFavorites = [];
        oldFavorites.map(f => {
          newFavorites.push({
            provider: f.provider,
            fields: f.fields
          });
        });

        const requestData = {
          agreements: [
            {
              provider: provider.config.providerKey,
              // providerIcon: provider.config.logo,
              fields: fieldsArr
            },
            ...newFavorites
          ],
          type: FavoriteType.DEFAULT
        };
        return this.customerApi.setFavorites(requestData);
      })
     );



    // return this.customerApi.setFavorites(requestData)

    // provider: string;
    // fields: Array<Field>;
    // this.customerApi.setFavorites(requestData).pipe(map(data => {
    //   return data;
    // }));
  }


  setFavorites(fieldsData: any) {
    const requestData = {
      agreements: [],
      type: FavoriteType.DEFAULT
    };
    return this.getFavorites().pipe(map(data => {
      requestData.agreements = data;
      const newFavoriteData = {
        provider: fieldsData.provider.labelId + '/' + fieldsData.provider.config.providerKey,
        providerIcon: fieldsData.provider.config.logo,
        fields: []
      };
      for (const fieldName in fieldsData.fields) {
        if (true) {
          newFavoriteData.fields.push({
            name: fieldName,
            value: fieldsData.fields[fieldName]
          });
        }
      }
      requestData.agreements.push(newFavoriteData);
      return this.customerApi.setFavorites(requestData).pipe(map(favorites => {
        return favorites;
      }));
    }, err => {
      requestData.agreements = [];
      const newFavoriteData = {
        provider: fieldsData.provider.labelId + '/' + fieldsData.provider.config.providerKey,
        providerIcon: fieldsData.provider.config.logo,
        fields: []
      };
      for (const fieldName in fieldsData.fields) {
        if (true) {
          newFavoriteData.fields.push({
            name: fieldName,
            value: fieldsData.fields[fieldName]
          });
        }
      }
      requestData.agreements.push(newFavoriteData);
      return this.customerApi.setFavorites(requestData).pipe(map(data => {
        return data;
      }));
    }));
    // this.customerApi.setFavorites({agreements: []}).subscribe(data => {
    // });
  }

  // getUserTransactions(page: Page, order: Order, filter?: ICashboxTransactionsFilter) {
  //   const activityList = activityTestData.map(d => new UserActivityData(d));
  //   return new BehaviorSubject(activityList);
  // }

  getUserTransactions(page: Page, order: Order, filter?: ICashboxTransactionsFilter) {

    const requestData =  new CustomersGetTransactionsQuery();
    requestData.page = page;
    requestData.order = order;

    return this.customerApi['getTransactions'](requestData).pipe(map(data => {
      const userTransactions = data.items.map(t => {

        // const provider = this.providerService.getConfiguredProvidersByProviderKey(t.providerKey);
        // debugger

        // provide providerKey
       // return new UserActivityData(t);
      });
      return {
        ...data
        // ...page,
        // items: userTransactions,
      };
    }));
  }
}
