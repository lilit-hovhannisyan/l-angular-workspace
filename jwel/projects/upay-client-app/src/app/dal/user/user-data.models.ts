import {IPair} from 'iu-ui-lib/lib/global-types';

export class UserActivityData {
  icon: string;
  title: string;
  date: string;
  amount: number;
  currency: string;
  isFavorite: boolean;
  isRecurrent: boolean;
  serviceDetails: IPair[];

  constructor(data) {

      this.icon = data.icon;
      this.title = data.title;
      this.date = data.date;
      this.amount = data.amount;
      this.currency = data.currency;
      this.isFavorite = data.isFavorite;
      this.isRecurrent = data.isRecurrent;

      this.serviceDetails = data.serviceDetails.map(i => {
        return {
          name: i.name,
          value: i.value
        };
      });
  }
}

export class UserCardData {
  id: string;
  type: string;
  text: string;
  src: string;

  constructor(data) {
    this.id = data.id;
    this.type = data.type;
    this.text = data.text;
    this.src = UserCardData.getCardImageUrl(data.type);
  }

  static getCardImageUrl (type) {
    const noCardImage  = 'uwallet-card.svg';
    const cartImageMap =  {
      'u_pay': 'uwallet-card.svg',
      'visa': 'visa-card.svg',
      'master': 'mastercard-card.svg'
    };
    const img = cartImageMap[type] || noCardImage;

    return `assets/img/client/${img}`;
  }
}

