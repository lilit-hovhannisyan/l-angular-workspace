export const transactionPageTabs = [

];


export const clientTabs = [
  {
    iconClass: 'home',
    link: '/dashboard',
    text: 'Dashboard'
  },
  {
    iconClass: 'pay',
    link: '/pay',
    text: 'Pay'
  },
  {
    iconClass: 'topup',
    link: '/top-up',
    text: 'Top up'
  },
  {
    iconClass: 'transfer',
    link: '/transfer',
    text: 'Transfer'
  }
];


export const serviceCategory = [
  {
    iconClass: 'icon_utility',
    text: 'Utility',
    name: 'utility',
    services : [
      {
        iconClass: 'icon_gas',
        logo: 'beeline.png',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: 'beeline.png',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services : [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
  {
    text: 'Mobile Connections',
    iconClass: 'icon_',
    name: 'mobile',
    services: []

  },
  {
    text: 'Internet',
    iconClass: 'icon_',
    name: 'internet',
    services: []
  },
  {
    text: 'TV',
    iconClass: 'icon_',
    name: 'tv',
    services: []
  },
  {
    text: 'Parking',
    iconClass: 'icon_',
    name: 'mobile',
    services: []
  },

];


