import { Injectable } from '@angular/core';

import { clientTabs, serviceCategory } from './static-data';
import {ClientTabView, ServiceCategoryView} from './site-data.models';

@Injectable()
export class SiteDataService {

  public clientTabs: ClientTabView[] = clientTabs;

  public serviceCategory: ServiceCategoryView[] = serviceCategory;

  public getCategoryProviders(providerCategoryId) {
    const category = this.serviceCategory.find(sc => {
      return providerCategoryId === sc.name;
    });

    if (category && category.services) {
      return category.services;
    }

    return [];
  }

  private setGlobalMessage(message: string, type: 'info' | 'error' | 'worning' | 'success') {
    alert('we will send your new password by sms');
  }

  public setGlobalInfoMessag(message: string) {
    this.setGlobalMessage(message, 'info');
  }

}
