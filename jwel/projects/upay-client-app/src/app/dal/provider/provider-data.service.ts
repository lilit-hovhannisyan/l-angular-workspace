import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {ProviderCategoryData} from './provider-data.models';
import {map} from 'rxjs/internal/operators';

// todo Jnjel
export const payServiceCategory = [
  {
    iconClass: 'icon_utility',
    text: 'Utility',
    name: 'utility',
    services : [
      {
        iconClass: 'icon_gas',
        logo: '',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: '',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_gas',
        logo: '',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: '',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_gas',
        logo: '',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: '',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_gas',
        logo: '',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: '',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      },
    ]
  },
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services : [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_my-mobile',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
  {
    text: 'Mobile Connections',
    iconClass: 'icon_mobile',
    name: 'mobile',
    services: []

  },
  {
    text: 'Internet',
    iconClass: 'icon_internet',
    name: 'internet',
    services: []
  },
  {
    text: 'TV',
    iconClass: 'icon_tv',
    name: 'tv',
    services: []
  },
  {
    text: 'Parking',
    iconClass: 'icon_parking',
    name: 'mobile',
    services: []
  },
];

export const transferServiceCategory = [
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services : [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_my-mobile',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
];

export const topupServiceCategory = [
  {
    text: 'Internet',
    iconClass: 'icon_internet',
    name: 'internet',
    services: []
  },
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services : [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_my-mobile',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
];

// todo end

@Injectable()
export class ProviderDataService {

  private _providersCategoryTree: ProviderCategoryData[];
  providersCategoryTree = new BehaviorSubject(this._providersCategoryTree);

  getPayProvidersCategoryTree() {
    // todo Api
    this._providersCategoryTree = payServiceCategory.map(d => new ProviderCategoryData(d));
    this.providersCategoryTree.next(this._providersCategoryTree);
  }

  getTransferProvidersCategoryTree() {
    // todo Api
    this._providersCategoryTree = transferServiceCategory.map(d => new ProviderCategoryData(d));
    this.providersCategoryTree.next(this._providersCategoryTree);
  }

  getTopUpProvidersCategoryTree() {
    // todo Api
    this._providersCategoryTree = topupServiceCategory.map(d => new ProviderCategoryData(d));
    this.providersCategoryTree.next(this._providersCategoryTree);
  }

  getCategoryProviders(categoryId):  Observable<ProviderCategoryData> {
    return this.providersCategoryTree
      .pipe(map(c => {
        return c.find(p => {
          return categoryId === p.name;
        });
      }));
  }

}
