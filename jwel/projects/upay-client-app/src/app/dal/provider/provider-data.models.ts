export class ProviderData {
  iconClass: string;
  logo: string;
  text: string;
  name: string;
  percentage: number;
  price: number;

  constructor(data) {
    this.iconClass = data.iconClass;
    this.logo = data.logo;
    this.text = data.text;
    this.name = data.name;
    this.percentage = data.percentage;
    this.price = data.price;
  }
}

export class ProviderCategoryData {
  iconClass: string;
  text: string;
  name: string;
  services: ProviderData[];

  constructor(data) {
    this.iconClass = data.iconClass;
    this.text = data.text;
    this.name = data.name;
    this.services = data.services.map(s =>  new ProviderData(s));
  }
}

