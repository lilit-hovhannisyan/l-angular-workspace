import groovy.json.JsonSlurper

def devInventory = [:]

pipeline {
    agent {label 'Upay-Sso'}
    options {
        skipDefaultCheckout true
    }

    stages {
        stage("Space Cleanup") {
             steps {
                deleteDir()
                cleanWs()
             }
        }

        stage("Checkout") {
            steps {
                checkout scm
                script {
                    def json = sh(returnStdout: true, script: 'cat projects/upay-accountant-app/deployment/environment/${upayEnv}/hosts.json')
                    def jsonObject = new groovy.json.JsonSlurper().parseText(json)
                    def servers = []
                    for (int i = 0; i < jsonObject.manager.size(); i++) {
                        servers.add(["host": jsonObject.manager.get(i).host, "user": jsonObject.manager.get(i).user])
                    }
                    devInventory.put("manager", servers)
                }
            }
        }
        stage("Docker cleanup") {
            steps {
                sh "docker container prune -f"
                sh "docker image prune -f"
            }
        }
        stage("Docker build") {
            steps {
                script {
                    sh "docker build -t upay-accountant-web --build-arg configuration=${upayEnv} -f projects/upay-accountant-app/deployment/Dockerfile ."
                }
            }
        }
        stage("Docker copy") {
            steps {
                sh "docker save -o ${workspace}/upay-accountant-web.img upay-accountant-web"
                sh "scp -o StrictHostKeyChecking=no ${workspace}/upay-accountant-web.img root@10.16.59.11:~/upay-accountant-web.img"
            }
        }
        stage("Deploy to environment") {
            steps {
                script {
                    def web = devInventory.get("manager")
                    web.each{ server ->
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.11 'docker stop upay-accountant-web || true && docker rm upay-accountant-web || true && docker rmi upay-accountant-web || true'"
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.11 'docker image load -i ~/upay-accountant-web.img'"
                    }
                }
            }
        }
        stage("Docker running image") {
            steps {
                sh "ssh -o StrictHostKeyChecking=no root@10.16.59.11 'docker run -d --name upay-accountant-web -p 81:81 upay-accountant-web'"
            }
        }
    }
}
