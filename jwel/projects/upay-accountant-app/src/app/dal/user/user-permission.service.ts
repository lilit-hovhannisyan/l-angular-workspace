import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {PermissionsGuardService, PermissionTypes} from '../../core/guards/permissions-guard.service';
import {UserDataService} from './user-data.service';
import {of} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';

const headerNavigationConfig = [
  {
    path: '/user/report-history/',
    text: 'navigation.header.reports',
    permissions: [PermissionTypes.ACCOUNTANT]
  },
  {
    path: '/user/transaction/',
    text: 'navigation.header.transactions',
    permissions: [
      PermissionTypes.ACCOUNTANT,
      PermissionTypes.CLAIMIST_HEAD,
      PermissionTypes.CLAIMIST
    ]
  },
  {
    path: '/user/failed-transaction-history/',
    text: 'navigation.header.failedTransactions',
  },
  {
    path: '/user/partner-report-history/',
    text: 'navigation.header.partnerReportHistory',
    permissions: [PermissionTypes.ACCOUNTANT]
  },
  {
    path: '/user/claim-application/',
    text: 'navigation.header.claimApplication',
    permissions: [PermissionTypes.CLAIMIST]

  },
  // {
  //   path: '/client/history',
  //   text: 'navigation.header.cashbox'
  // }
];

const transactionTabsConfig = [
  {
    link: 'history',
    text: 'widgets.transaction_tabs.transactionHistory',
    permissions: [PermissionTypes.ACCOUNTANT]
  },
  {
    link: 'claims',
    text: 'widgets.transaction_tabs.cancellationTransactions',
    permissions: [
      PermissionTypes.CLAIMIST_HEAD,
      PermissionTypes.CLAIMIST
    ]
  }
];

const failedTransactionTabsConfig = [
  {
    link: 'branch',
    text: 'Branch',
    permissions: [
      PermissionTypes.ACCOUNTANT,
      PermissionTypes.CLAIMIST_HEAD,
      PermissionTypes.CLAIMIST
    ]
  },
  {
    link: 'client',
    text: 'Client',
    permissions: [
      PermissionTypes.ACCOUNTANT,
      PermissionTypes.CLAIMIST_HEAD,
      PermissionTypes.CLAIMIST
    ]
  }
];

@Injectable()
export class UserPermissionService {
  dynamicRoot;

  constructor(private router: Router,
              private userDataService: UserDataService,
              private permissionsGuardService: PermissionsGuardService) {}


  navigationListByPermission(navigationConfig) {
    const navigationList  = [];
    const userPermissions = this.userDataService._userInfo.permissions;

    navigationConfig.map((n: any) => {
      if (this.permissionsGuardService.onAllowRoute(userPermissions, n.permissions)) {
        navigationList.push(n);
      }
    });

    return navigationList;
  }

  getNavigationListByPermission(navigationConfig) {
    if (this.userDataService._userInfo) {
      const navigationList = this.navigationListByPermission(navigationConfig);
      return of(navigationList);
    } else {
      return this.userDataService.getProfile().pipe(map(userInfo => {
        const navigationList = this.navigationListByPermission(navigationConfig);
        return navigationList;
      }));
    }
  }

  createHeaderNavigationByPermission() {
    return this.getNavigationListByPermission(headerNavigationConfig);
  }

  createTransactionPageTabs() {
    return this.getNavigationListByPermission(transactionTabsConfig);
  }

  createFailedTransactionPageTabs () {
    return this.getNavigationListByPermission(failedTransactionTabsConfig);
  }

}

