
import {AccountantInfo} from 'upay-lib';

export class UserInfoData {
  email: string;
  enabled: boolean;
  forename: string;
  id: string;
  phone: string;
  status:   string;
  surname: string;
  username: string;
  permissions: any[];

  constructor(data) {
    this.username = data.username;
    this.email = data.email;
    this.phone = data.phone;
    this.forename = data.forename;
    this.surname = data.surname;
    this.permissions = data.scope;
  }
}

export class UserBranchInfoData {
  address: string;
  code: string;
  description: string;
  enabled: boolean;
  id: string;
  name: string;
  policyId: string;

  constructor(data) {
    this.address = data.address;
    this.code = data.code;
    this.description = data.description;
    this.enabled = data.enabled;
    this.id = data.id;
    this.name = data.name;
    this.policyId = data.policyId;
  }
}
