import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import { v4 as uuid } from 'uuid';
import {UserInfoData} from './user-data.models';
import {HttpClient} from '@angular/common/http';
import {LanguageService} from '../language.service';
import {
  AccountantApi,
  AccountantAuthenticateCommand,
  AccountantChangePasswordCommand,
  AccountantGetAccessTokenQuery,
  AccountantGetProfileQuery,
} from 'upay-lib';
import {map} from 'rxjs/internal/operators';
import {PermissionTypes} from '../../core/guards/permissions-guard.service';


@Injectable()
export class UserDataService {
  _isAuth: boolean;
  isAuth = new BehaviorSubject(this._isAuth);

  _cashboxId;
  cashboxId = new BehaviorSubject(this._cashboxId);

  _userInfo?: UserInfoData;
  userInfo = new BehaviorSubject(this._userInfo);

  constructor(protected http: HttpClient,
              private accountantApi: AccountantApi,
              private languageService: LanguageService) {
    if (localStorage.getItem('authToken')) {
      this._isAuth = true;
    } else {
      this._isAuth = false;
    }
    this.isAuth.next(this._isAuth);

  }

  signIn(username: string, password: string) {
    const id = uuid();
    const data = new AccountantAuthenticateCommand();
    data.username = username;
    data.password = password;
    data.accountantId = '856cf752-0963-4b92-95e7-1b584f6bf822'; // TODO
    data.authenticationId = id;

    this.accountantApi.authenticate(data).subscribe(authenticateData => {
      this.setUserToken(id);
    });
  }

  setUserToken(id) {
    const accessTokenRequestData = new AccountantGetAccessTokenQuery();
    accessTokenRequestData.authenticationId = id;
    this.accountantApi.getAccessToken(accessTokenRequestData).subscribe(authToken => {
      localStorage.setItem('authToken', authToken);
      this._isAuth = true;
      this.isAuth.next(this._isAuth);
    });
  }

  signOut() {
    localStorage.removeItem('authToken');
    this._isAuth = false;
    this.isAuth.next(this._isAuth);
    this._userInfo = null;
    this.userInfo.next(this._userInfo);
  }

  // todo api
  resetPassword(oldPassword: string, newPassword: string, email: string) {
    const operatorResetPasswordCommand = new AccountantChangePasswordCommand();
    operatorResetPasswordCommand.oldPass = oldPassword;
    operatorResetPasswordCommand.newPass = newPassword;
    operatorResetPasswordCommand.email = email;

    return this.accountantApi.changePassword(operatorResetPasswordCommand);
  }

  // todo api
  getProfile() {
    const operatorGetProfileQuery = new AccountantGetProfileQuery();
    return this.accountantApi.getProfile(operatorGetProfileQuery).pipe(map(userRes => {
      // hardcode
      // userRes['scope'] = ["CLAIMIST"];
      // userRes['scopeStream'] = [PermissionTypes.CLAIMIST];
      this._userInfo = new UserInfoData(userRes);
      this.userInfo.next(this._userInfo);
      return this._userInfo;
    }));
  }

}
