import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {
  AccountantApi,
  AccountantGetFailedTransactionsQuery,
  AccountantRetryTransactionCommand,
  AccountantCompleteTransactionCommand,
  Order,
  Page,
  AccountantCompleteCustomerTransactionCommand,
  AccountantGetFailedCustomerTransactionsQuery,
  AccountantRetryCustomerTransactionCommand
} from 'upay-lib';
import {map} from 'rxjs/internal/operators';
import {FailedTransactionData} from '../../dal/transaction/transaction-data.models';
import {
  AccountantGetFailedBranchTransactionsQuery,
  AccountantRefundCustomerTransactionCommand
} from 'upay-lib';

@Injectable()
export class FailedTransactionDataService {
  constructor(private accountantApi: AccountantApi) {}

  getBranchFailedTransactions(queryData) {
   return this.accountantApi.getFailedBranchTransactions(queryData);
  }

  getCustomerFailedTransactions(queryData) {
    return this.accountantApi.getFailedCustomerTransactions(queryData);
  }

  retryTransaction (transactionId: string) {
    const accountantRetryTransactionCommand = new AccountantRetryTransactionCommand();
    accountantRetryTransactionCommand.transactionId = transactionId;
    return this.accountantApi.retryTransaction(accountantRetryTransactionCommand);
  }

  retryCustomerTransaction(transactionId: string) {
    const accountantRetryCustomerTransactionCommand = new AccountantRetryCustomerTransactionCommand();
    accountantRetryCustomerTransactionCommand.customerTransactionId = transactionId;
    return this.accountantApi.retryCustomerTransaction(accountantRetryCustomerTransactionCommand);
  }

  completeTransaction (transactionId: string) {
    const accountantCompleteTransactionCommand = new AccountantCompleteTransactionCommand();
    accountantCompleteTransactionCommand.transactionId = transactionId;
    return this.accountantApi.completeTransaction(accountantCompleteTransactionCommand);
  }


  completeCustomerTransaction (transactionId: string) {
    const accountantCompleteCustomerTransactionCommand = new AccountantCompleteCustomerTransactionCommand();
    accountantCompleteCustomerTransactionCommand.customerTransactionId = transactionId;
    return this.accountantApi.completeCustomerTransaction(accountantCompleteCustomerTransactionCommand);
  }

  refundCustomerTransaction (transactionId: string) {
    const accountantRefundCustomerTransactionCommand = new AccountantRefundCustomerTransactionCommand();
    accountantRefundCustomerTransactionCommand.transactionId = transactionId;
    return this.accountantApi.refundCustomerTransaction(accountantRefundCustomerTransactionCommand);
  }
}
