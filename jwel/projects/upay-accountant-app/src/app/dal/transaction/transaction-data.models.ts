import {OperatorLogEntry, ProviderData, splitNumberWithLumas, Transaction} from 'upay-lib';

export class TransactionData {
  id: string;
  paymentId: string;
  receiptId: string;
  // issuer: string;
  issuedDate: Date;
  completed: Date;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  branchName: string;
  providerId: string;
  providerName: string;
  purpose: string;
  amount: number;
  commission: number;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  subscriberNumber: string;
  additionalData: string;
  providerLogo: string;
  contactNum: string;
  isCash: boolean;
  posAuthcode: string;
  transactionId: string;
  totalAmount: string;
  externalId: string;


  constructor(data) {


    if (`${data.action}` === 'ENCASHMENT_IN' || `${data.action}` === 'ENCASHMENT_OUT') {
      this.providerName = `${data.encashmentType}`; // data.provider.text;
    } else {
      this.providerName = data.providerName;
    }


      this.id = data.id;
      // this.amount = data.amount / 100;
      this.totalAmount = splitNumberWithLumas(data.totalAmount / 100);
      this.commissionAmount = data.commissionAmount / 100;
      this.subscriberNumber = data.userNum;
      this.issuedDate = new Date(data.date);
      this.branchName = data.branchName;
      // let providerConfig;
      this.contactNum = data.contactNum ? data.contactNum : '---';
      this.isCash = data.cash;
      this.posAuthcode = data.posAuthcode;
      this.transactionId = data.transactionId;
      this.externalId = data.externalId;
      // if (data.providerName.toLocaleLowerCase() === 'test') {
      //   providerConfig = 'testik';
      // } else {
      //   providerConfig = ProviderData.getConfig(data.providerName);
      // }

     // this.providerName = data.providerName;
      this.receiptId = data.receiptId;
      // if (providerConfig) {
      //   this.providerName = providerConfig.text;
      //   this.providerLogo = 'shared-assets/img/providers/' + providerConfig.logo;
      // }
    }

}

export class FailedTransactionData {
  id: string;
  paymentId: string;
  receiptId: string;
  // issuer: string;
  issuedDate: Date;
  completed: Date;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  branchName: string;
  providerId: string;
  providerName: string;
  purpose: string;
  amount: number;
  commission: number;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  subscriberNumber: string;
  additionalData: string;
  providerLogo: string;
  contactNum: string;
  isCash: boolean;
  totalAmount: number;

  constructor(data) {
    this.id = data.id;
    // this.amount = data.amount / 100;
    this.totalAmount = data.totalAmount / 100;
    this.commissionAmount = data.commissionAmount / 100;
    this.subscriberNumber = data.userNum;
    this.issuedDate = new Date(data.issued);
    this.branchName = data.branchName;
    let providerConfig;
    this.contactNum = data.contactNum ? data.contactNum : '---';
    this.isCash = data.cash;

    if (data.providerName.toLocaleLowerCase() === 'test') {
      providerConfig = 'testik';
    } else {
      providerConfig = ProviderData.getConfig(data.providerName);
    }

    this.providerName = data.providerName;
    this.receiptId = data.receiptId;
    if (providerConfig) {
      this.providerName = providerConfig.text;
      this.providerLogo = 'shared-assets/img/providers/' + providerConfig.logo;
    }
  }

}
