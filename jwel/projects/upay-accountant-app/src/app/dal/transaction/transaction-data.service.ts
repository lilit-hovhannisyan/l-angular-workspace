import {Injectable} from '@angular/core';
import {TransactionData} from './transaction-data.models';
import {AccountantApi, FileDownload} from 'upay-lib';
import {map} from 'rxjs/internal/operators';
import {environment} from '../../../environments/environment';

@Injectable()
export class TransactionDataService {
  constructor(private accountantApi: AccountantApi,
              private fileDownload: FileDownload) {}

  downloadUrl = environment.APIEndpoint.accountantUrl + '/upay/accountant/download-transactions-report';

  getTransactionList (queryData) {
    return this.accountantApi.getTransactions(queryData).pipe(
      map( data => {
        const transactionListData = {
          items: data.items.map(r => {
            return new TransactionData(r);
          }),
          total: data.total
        };

        return transactionListData;
      })
    );
  }

  downLoadTransactionsReport(queryData) {
    return this.fileDownload.post(this.downloadUrl, queryData);
  }
}
