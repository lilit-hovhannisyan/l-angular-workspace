// todo change to language data service

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LanguageService {

  static supportedLanguages = ['en', 'ru', 'hy'];

  static defaultLanguage = 'en';

  public selectedLanguage: string;

  constructor(private translateService: TranslateService) {
     let selectedLanguage = localStorage.getItem('lang');
     if (!selectedLanguage) {
       selectedLanguage = LanguageService.defaultLanguage;
     }
     this.changeLanguage(selectedLanguage);
  }

  changeLanguage(languageCode) {
    this.selectedLanguage = languageCode;
    this.translateService.use(languageCode);
    localStorage.setItem('lang', languageCode);
  }
}
