import {Injectable} from '@angular/core';
import {
  AccountantApi,
  AccountantGetClaimInfoQuery, AccountantAcceptCancellationClaimCommand,
  AccountantDismissCancellationClaimCommand, AccountantPreApproveCancellationClaimCommand, AccountantPreRejectCancellationClaimCommand,
  AccountantApproveCancellationClaimCommand, AccountantRejectCancellationClaimCommand, FileUploadApi, Claim
} from 'upay-lib';
import {Observable} from 'rxjs/index';
import {AccountantGetClaimHistoryQuery} from '../../../../../upay-lib/src/lib/api';

@Injectable()
export class ClaimDataService {
  constructor(private accountantApi: AccountantApi,
              private fileUploadApi: FileUploadApi) {
  }

  // hayteri ejum,erb arajin angam voroshuma nayi te che
  getPendingCancellationClaims(queryData) {
    return this.accountantApi.getPendingCancellationClaims(queryData);
  }

  countPendingCancellationClaims(queryData) {
    return this.accountantApi.countPendingCancellationClaims(queryData);
  }

  acceptCancellationClaim(claimId: string, examinationNotes: string) {
    const acountantAcceptCancellationClaimCommand = new AccountantAcceptCancellationClaimCommand();
    acountantAcceptCancellationClaimCommand.claimId = claimId;
    acountantAcceptCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.acceptCancellationClaim(acountantAcceptCancellationClaimCommand);
  }

  dismissCancellationClaim(claimId: string, examinationNotes: string) {
    const accountantDismissCancellationClaimCommand = new AccountantDismissCancellationClaimCommand();
    accountantDismissCancellationClaimCommand.claimId = claimId;
    accountantDismissCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.dismissCancellationClaim(accountantDismissCancellationClaimCommand);
  }

  preApproveCancellationClaim(claimId: string, examinationNotes: string) {
    const accountantPreApproveCancellationClaimCommand = new AccountantPreApproveCancellationClaimCommand();
    accountantPreApproveCancellationClaimCommand.claimId = claimId;
    accountantPreApproveCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.preApproveCancellationClaim(accountantPreApproveCancellationClaimCommand);
  }

  preRejectCancellationClaim(claimId: string, examinationNotes: string) {
    const accountantPreRejectCancellationClaimCommand = new AccountantPreRejectCancellationClaimCommand();
    accountantPreRejectCancellationClaimCommand.claimId = claimId;
    accountantPreRejectCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.preRejectCancellationClaim(accountantPreRejectCancellationClaimCommand);
  }

  approveCancellationClaim(claimId: string, examinationNotes: string) {
    const accountantApproveCancellationClaimCommand = new AccountantApproveCancellationClaimCommand();
    accountantApproveCancellationClaimCommand.claimId = claimId;
    accountantApproveCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.approveCancellationClaim(accountantApproveCancellationClaimCommand);
  }

  rejectCancellationClaim(claimId: string, examinationNotes: string) {
    const accountantRejectCancellationClaimCommand = new AccountantRejectCancellationClaimCommand();
    accountantRejectCancellationClaimCommand.claimId = claimId;
    accountantRejectCancellationClaimCommand.examinationNotes = examinationNotes;
    return this.accountantApi.rejectCancellationClaim(accountantRejectCancellationClaimCommand);
  }

  getClaimInfo(claimId: string): Observable <Claim> {
    const accountantGetClaimInfoQuery = new AccountantGetClaimInfoQuery();
    accountantGetClaimInfoQuery.claimId = claimId;
    return this.accountantApi.getClaimInfo(accountantGetClaimInfoQuery);
  }

  getClaimHistory(claimId: string) {
    const accountantGetClaimHistoryQuery = new AccountantGetClaimHistoryQuery();
    accountantGetClaimHistoryQuery.claimId = claimId;
    return this.accountantApi.getClaimHistory(accountantGetClaimHistoryQuery);
  }

  getFilterClaims(queryData) {
    return this.accountantApi.getFilteredClaims(queryData);
  }

  getFilesInGroup(groupName) {
    return this.fileUploadApi.getFilesInGroup(groupName);
  }

}
