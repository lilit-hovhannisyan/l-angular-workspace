
import {CashboxReport, splitNumberWithLumas} from 'upay-lib';

export class ReportByProvider {
  amount: any;
  providerDetails: any;

  constructor(data: any) {
    this.amount = splitNumberWithLumas(data.amount / 100);
    this.providerDetails = {
      name: data.providerName
    };
  }
}

export class ReportData {
  id: string;
  cashboxId: string;
  cashboxName: string;
  branchId: string;
  branchName: string;
  operatorId: string;
  operatorName: string;
  created: Date;
  balance: string;
  startBalance: string;
  saldo: number;
  encashmentIn: number;
  encashmentOut: number;
  reportByProviderList: ReportByProvider[];
  date: Date;
  username: string;
  fullName: string;
  paymentIn: number;

  cashInReport: string;
  cashOutReport: string;

  openingBalance: number;


  constructor(data: CashboxReport) {

    const balance = data.balance / 100;
    const encashmentIn = data.encashmentIn / 100;
    const encashmentOut = data.encashmentOut / 100;
    const openingBalance = data.openingBalance / 100;

    this.id = data.id;
    this.branchName = data.branchName;
    this.branchId = data.branchId;
    this.cashboxName = data.cashboxName;
    this.cashboxId = data.cashboxId;
    this.operatorName = data.operatorName;
    this.operatorId = data.operatorId;
    this.date = new Date(data.created);
    this.balance = splitNumberWithLumas(balance);
    // this.startBalance = balance + encashmentIn - encashmentOut;
    this.startBalance = splitNumberWithLumas(openingBalance);
    this.saldo = encashmentIn - encashmentOut;
    this.encashmentIn = encashmentIn;
    this.encashmentOut = encashmentOut;

    this.paymentIn = data.paymentIn / 100;
    // debugger

    this.cashInReport = splitNumberWithLumas(encashmentIn + this.paymentIn);
    this.cashOutReport = splitNumberWithLumas(encashmentOut);


    const dataByProviders = JSON.parse(data.dataByProviders);
    this.reportByProviderList = [];
    for (const reportData of Object.keys(dataByProviders)) {
      this.reportByProviderList.push(new ReportByProvider({
        amount: dataByProviders[reportData],
        providerName: reportData
      }));
    }
    // = data.dataByProviders;


    this.username = data.operatorUsername;
    this.fullName = data.operatorName;
  }
}
