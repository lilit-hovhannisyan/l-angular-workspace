import {Injectable} from '@angular/core';
import {ReportData} from './report-data.models';
import {map} from 'rxjs/internal/operators';
import {
  AccountantApi,
  AccountantGetCashboxReportQuery,
  FileDownload,
  AlertBoxService,
  OfficeApi
} from 'upay-lib';

import {environment} from '../../../environments/environment';

@Injectable()
export class ReportDataService {
  downloadUrl = environment.APIEndpoint.accountantUrl + '/upay/accountant/download-cashbox-reports';

  constructor(private accountantApi: AccountantApi,
              private fileDownload: FileDownload,
              private alertBoxService: AlertBoxService,
              private officeApi: OfficeApi) {
  }

  getCashboxReports(queryData) {
   return this.accountantApi.getCashboxReports(queryData).pipe(
      map(data => {
        const reportData = {
          items: data.items.map(r => {
            return new ReportData(r);
          }),
          total: data.total
        };
        return reportData;
      })
    );
  }

  getReportById(reportId: string) {
    const accountantGetCashboxReportQuery = new AccountantGetCashboxReportQuery();
    accountantGetCashboxReportQuery.id = reportId;
    return this.accountantApi.getCashboxReport(accountantGetCashboxReportQuery)
      .pipe(map(data => {
        return new ReportData(data);
      }));
  }

  getCashboxReportsSummary(queryData) {
    return this.accountantApi.getCashboxReportsSummary(queryData);
  }

  downloadReport(queryData) {
    const url = this.downloadUrl;

    this.fileDownload.post(url, queryData)
      .subscribe(
        (res: any) => {
          if (res) {
            this.alertBoxService.initMsg({type: 'success', text: 'Հաշվետվությունը հաջողությամբ արտածվել է'});
          } else {
            this.alertBoxService.initMsg({type: 'error', text: 'Something went wrong'});
          }
        },
        err => {
          console.error('error excell', err);
        });
  }
}
