import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {
  AdminGetProvidersQuery, OfficeApi, OperatorGetProviderCommissionsQuery, OperatorGetProviderDescriptorsQuery, Order,
  Page, ProviderCategoryData,
  ProviderData,
  AccountantApi,
  FileDownload,
  AlertBoxService,

} from 'upay-lib';
import {AccountantGetProvidersQuery} from 'upay-lib';
import {environment} from '../../../environments/environment';


@Injectable()
export class ProviderDataService {

  constructor (private officeApi: OfficeApi,
               private accountantApi: AccountantApi,
               private fileDownload: FileDownload,
               private alertBoxService: AlertBoxService) {
  }

  _providersCategoryTree: ProviderCategoryData[] = [];
  providersCategoryTree = new BehaviorSubject<ProviderCategoryData[]>(this._providersCategoryTree);
  _providersList: ProviderData[] = [];
  providersList = new BehaviorSubject(this._providersList);

  downloadUrl = environment.APIEndpoint.accountantUrl  + '/upay/accountant/download-partner-report';

  getPayProvidersCategoryTree() {
    if (!this._providersCategoryTree.length) {
      const page = new Page();
      page.size = 100000; // todo change api
      page.index = 0;
      const order = new Order();
      this.officeApi.getProviderLabels({page: page, order: order, name: ''})
        .subscribe(categories => {
          const getProvidersQuery = new AdminGetProvidersQuery();
          // getProvidersQuery.name = '';
          getProvidersQuery.enabled = true;
          // getProvidersQuery.hasFixedAmount = false;
          // getProvidersQuery.label = '';
          getProvidersQuery.order = order;
          getProvidersQuery.page = page;

          this.officeApi.getProviders(getProvidersQuery).subscribe(res => {
            const providersCategoryTreeData = [];

            for (const category of categories.items) {

              const subProviders = res.items.filter(p => {
                return p.labels.find(l => {
                  return l === category.id;
                });
              });

              providersCategoryTreeData.push({
                ...category,
                services: subProviders
              });
            }

            this._providersList = res.items.map(p => ProviderData.createNoThrow(p, this.officeApi)).filter(pd => pd != null);
            this.providersList.next(this._providersList);
            this._providersCategoryTree = providersCategoryTreeData.
            map(d => new ProviderCategoryData(d, this.officeApi));
            this.providersCategoryTree.next(this._providersCategoryTree);
          });
        });
    }
  }

  getProviderCommissions(branchId: string, providerId: string) {
    const operatorGetProviderCommissionsQuery = new OperatorGetProviderCommissionsQuery();
    operatorGetProviderCommissionsQuery.branchId = branchId;
    operatorGetProviderCommissionsQuery.providerId = providerId;
    return this.officeApi.getProviderCommissions(operatorGetProviderCommissionsQuery);
  }

  getProviderDescriptors(providerId: string) {
    const operatorGetProviderDescriptorsQuery = new OperatorGetProviderDescriptorsQuery();
    operatorGetProviderDescriptorsQuery.providerId = providerId;
    this.officeApi.getProviderDescriptors(operatorGetProviderDescriptorsQuery).subscribe(res => {
    });
  }

  getCategoryProviders(categoryId): Observable<ProviderCategoryData> {
    return this.providersCategoryTree
      .pipe(map(c => {
        return c.find(p => {
          return categoryId === p.name;
        });
      }));
  }

  getProvidersList() {
    const accountantGetProvidersQuery = new AccountantGetProvidersQuery();
    return this.accountantApi.getProviders(accountantGetProvidersQuery);
  }

  downloadAllReports(searchCriteriaObj) {
    const url = this.downloadUrl;
    const data: any = {};
    // data.cashboxId = searchCriteriaObj.cashboxId;
    // data.branchId = searchCriteriaObj.branchId;
    // data.operatorId = searchCriteriaObj.operatorId;
    // data.provider = searchCriteriaObj.provider;
    // data.dateFrom = new Date(searchCriteriaObj.dateFrom).getTime();
    // data.dateTo = new Date(searchCriteriaObj.dateTo).getTime();

    this.fileDownload.post(url, data);
      // .subscribe(
      //   (res: any) => {
      //     if ( res ) {
      //       this.alertBoxService.initMsg({type: 'success', text: 'Հաշվետվությունը հաջողությամբ արտածվել է'});
      //     } else {
      //       this.alertBoxService.initMsg({type: 'error', text: 'Something went wrong'});
      //     }
      //   },
      //   err => {
      //     console.error('error excell', err);
      //   });
  }
}
