export class PartnerReport {
  provider: any;
  amount: number;
  count: number;
  externalCommission: null;
  branchName: string;

  constructor(data: any) {
    this.amount = data.amount;
    this.branchName = data.branchName;
    this.provider = data.providerName;
    this.externalCommission = data.commission;
    this.count = data.quantity;
  }
}
