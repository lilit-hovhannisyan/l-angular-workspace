import { Injectable } from '@angular/core';
import { AccountantApi, OfficeApi, FileDownload } from 'upay-lib';
import { map } from 'rxjs/operators';
import { PartnerReport } from './partner-data.models';
import { environment } from '../../../environments/environment';

@Injectable()
export class PartnerDataService {

  downloadUrl = environment.APIEndpoint.accountantUrl  + '/upay/accountant/download-partner-report';

  constructor(private officeApi: OfficeApi,
              private accountantApi: AccountantApi,
              private fileDownload: FileDownload) {
  }

  getPartnersReports(queryData) {
    return this.accountantApi.getPartnerReport(queryData).pipe(
      map(pr => {
          const items = pr.items.map(r => {
            return new PartnerReport(r);
          });
          return {
            items,
            total:  pr.total
          };
        }
      ));
  }

  getPartnerReportSummaryTotal(queryData) {
    return this.accountantApi.getPartnerReportSummary(queryData).pipe(map(pr => {
      return pr;
    }));
  }

  downloadPartnerReport(queryData) {
    const url = this.downloadUrl;
    return this.fileDownload.post(url, queryData);
  }
}
