import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// core module
import { CoreModule} from './core/core.module';

// routing
import { appRoutes } from './app-routing';

// app init
import { AppComponent } from './app.component';

// services
import { LanguageService } from './dal/language.service';
import { UserDataService } from './dal/user/user-data.service';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {NoAuthGuardService} from './core/guards/no-auth-guard.service';


import {PrintService} from './misc-services/print.service';
import {ForcePassChangeService} from './misc-services/forcePassChange.service';
import {ReportDataService} from './dal/report/report-data.service';
import {TransactionDataService} from './dal/transaction/transaction-data.service';
import {FailedTransactionDataService} from './dal/failed-transaction/failed-transaction-data.service';

import {LoadingService, AlertBoxService, SharedSiteDataService, DateTimeService, SharedPrintService, FileUploadApi, fileStorageUrl} from 'upay-lib';
import {BaseApi, FileDownload, AccountantApi, ErrorHandler, OfficeApi, setApiUrl} from 'upay-lib';
import {environment} from '../environments/environment';
import {ProviderDataService} from './dal/provider/provider-data.service';
import {PartnerDataService} from './dal/partner/partner-data.service';
import {PermissionsGuardService} from './core/guards/permissions-guard.service';
import {ClaimDataService} from './dal/claim/claim-data.service';
import {UserPermissionService} from './dal/user/user-permission.service';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

setApiUrl( environment.APIEndpoint.accountantUrl);
fileStorageUrl(environment.APIEndpoint.fileStorageUrl);

export class TranslationLoad implements TranslateLoader {
  constructor(public http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    return forkJoin(
      this.http.get<any>('shared-assets/i18n/' + lang + '.json'),
      this.http.get<any>('assets/i18n/' + lang + '.json'),
    ).pipe(map(response => {
      return {...response[0], ...response[1]};
    }));
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: TranslationLoad,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    FailedTransactionDataService,
    TransactionDataService,
    ReportDataService,
    LanguageService,
    UserDataService,
    ProviderDataService,
    AuthGuardService,
    NoAuthGuardService,
    PermissionsGuardService,
    // AuthService
    OfficeApi,
    AccountantApi,
    FileUploadApi,
    ErrorHandler,
    AlertBoxService,
    LoadingService,
    DateTimeService,
    SharedSiteDataService,
    SharedPrintService,

    PrintService,
    ForcePassChangeService,

    BaseApi,
    FileDownload,
    PartnerDataService,
    ClaimDataService,
    UserPermissionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
