import { Component, OnInit, AfterViewInit } from '@angular/core';

import {PopupsService} from '../../popups/popups.service';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../dal/user/user-data.service';
import {UserPermissionService} from '../../dal/user/user-permission.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class HeaderComponent implements OnInit {
  headerNavigationList = [];

  constructor(
              private popupService:  PopupsService,
              private userDataService: UserDataService,
              private userPermissionService: UserPermissionService) {}

  isAuth: boolean;
  isAuthSub = Subscription.EMPTY;

  user;


  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(auth => {
      this.isAuth = auth;

      if (!this.isAuth) {
        this.headerNavigationList = [];
        return;
      }

      this.userPermissionService.createHeaderNavigationByPermission().subscribe(headerNavigationList => {
        this.headerNavigationList = headerNavigationList;
      });

    });
  }

  openPopup( popup ) {
    this.popupService.openPopUp( popup );
  }
}
