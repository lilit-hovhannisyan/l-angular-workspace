import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {UserDataService} from '../../dal/user/user-data.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public userDataService: UserDataService,
              public router: Router) {}

  canActivate(): boolean {
    if (!this.userDataService._isAuth) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
