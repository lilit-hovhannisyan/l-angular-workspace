import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UserDataService} from '../../dal/user/user-data.service';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import {Scope} from 'upay-lib';

export const PermissionTypes = Scope;

@Injectable()
export class PermissionsGuardService implements CanActivate {

  constructor(public userDataService: UserDataService,
              public router: Router) {}

  canActivate(next: any, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // let allowRoute = false;

    if (!this.userDataService._userInfo) {
      return this.userDataService.getProfile().pipe(map(userInfo => {
        const permissions = userInfo.permissions;
        return this.onAllowRoute(permissions, next.data.permissions);
      }));
      // this.userDataService.getProfile();
      // allowRoute = this.onAllowRoute(next, this.userDataService._userInfo.permissions);
    } else {
      return this.onAllowRoute(this.userDataService._userInfo.permissions,  next.data.permissions);
    }

    // return allowRoute;
  }

  onAllowRoute(userPermissions, activatePermissions) {
    const permissions = this.userDataService._userInfo.permissions;
    if (!activatePermissions || !activatePermissions.length) {
      return true;
    }

    for (const p of activatePermissions) {
      const isPermitted = permissions.find(up => {
        return up === p;
      });
      if (isPermitted) {
        return true;
      }
    }

    return false;
  }
}


