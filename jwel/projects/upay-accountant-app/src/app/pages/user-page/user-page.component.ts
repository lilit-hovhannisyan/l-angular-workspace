import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';
import {ErrorHandler} from 'upay-lib';
import {UserPermissionService} from '../../dal/user/user-permission.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.less']
})
export class UserPageComponent implements OnInit {

  constructor(private userDataService: UserDataService,
              private errorHandler: ErrorHandler,
              private userPermissionService: UserPermissionService,
              private router: Router) {}

  ngOnInit() {
    // todo api
    this.userDataService.getProfile().subscribe(userInfo => {
      if (userInfo) {
        const userPermissions = userInfo.permissions;
        this.userPermissionService.createHeaderNavigationByPermission().subscribe(headerNavigationList => {
          if (this.router.url === '/user') {
            this.router.navigate([headerNavigationList[0].path]);
          }
        });
      }
    });
    // this.userDataService.userInfo.subscribe( user => {
    //   // if ( user && user.status === 'pending') {
    //   //   this.router.navigate(['/user/profile']);
    //   // }
    //   // this.errorHandler.errorData.subscribe(data => {
    //   //   if (data && data.message === 'Token expired') {
    //   //     this.userDataService.signOut();
    //   //     this.router.navigate(['/']);
    //   //     this.errorHandler.clear();
    //   //   }
    //   // });
    //
    // });
  }
}
