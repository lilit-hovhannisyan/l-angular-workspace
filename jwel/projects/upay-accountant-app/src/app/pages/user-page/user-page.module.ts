import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {userRoutes} from './user-page-routing';
import {UserPageComponent} from './user-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    UserPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(userRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    UserPageComponent
  ],
})
export class UserPageModule {
}
