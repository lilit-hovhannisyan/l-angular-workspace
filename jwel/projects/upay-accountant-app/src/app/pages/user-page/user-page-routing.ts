import {Routes} from '@angular/router';
import {ForcePassChangeService} from '../../misc-services/forcePassChange.service';
import {UserPageComponent} from './user-page.component';

export const userRoutes: Routes = [
  {
    path: '',
    component: UserPageComponent,
    children: [
      {
        path: 'report-history',
        loadChildren: '../report-history-page/report-history-page.module#ReportHistoryPageModule',
      },
      {
        path: 'report-view/:reportId',
        loadChildren: '../report-view-page/report-view-page.module#ReportViewPageModule',
      },
      // {
      //   path: 'transaction-history',
      //   loadChildren: '../transaction-history-page/transaction-history-page.module#TransactionHistoryPageModule',
      // },
      {
        path: 'failed-transaction-history',
        loadChildren: '../failed-transaction-history-page/failed-transaction-history-page.module#FailedTransactionHistoryPageModule',
      },
      {
        path: 'partner-report-history',
        loadChildren: '../partner-report-history-page/partner-report-history-page.module#PartnerReportHistoryPageModule',
      },
      {
        path: 'claim-application',
        loadChildren: '../claim-application-page/claim-application-page.module#ClaimApplicationPageModule'
      },
      {
        path: 'transaction',
        loadChildren: '../transaction-page/transaction-page.module#TransactionPageModule'
      },
      {
        path: 'profile',
        // canDeactivate: [ForcePassChangeService],
        loadChildren: '../user-profile-page/user-profile-page.module#UserProfilePageModule'
      },
    ]
  }
];

