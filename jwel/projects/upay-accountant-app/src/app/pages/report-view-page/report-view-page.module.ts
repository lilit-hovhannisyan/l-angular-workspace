import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../widgets/widgets.module';
import {reportViewRoutes} from './report-view-page-routing';
import {ReportViewPageComponent} from './report-view-page.component';
import {SharedWidgetsModule} from 'upay-lib';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    ReportViewPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(reportViewRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    ReportViewPageComponent
  ],
})
export class ReportViewPageModule {
}
