import {Routes} from '@angular/router';
import {ReportViewPageComponent} from './report-view-page.component';

export const reportViewRoutes: Routes = [
  {
    path: '',
    component: ReportViewPageComponent,
  }
];

