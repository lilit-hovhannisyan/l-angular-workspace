import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReportDataService} from '../../dal/report/report-data.service';
import {TranslateService} from '@ngx-translate/core';
import {Report} from '../../widgets/report-card/report-card.component';

@Component({
  selector: 'app-report-view-page',
  templateUrl: './report-view-page.component.html',
})
export class ReportViewPageComponent implements OnInit {
  reportSearchResult = [];

  goBackPath = '/user/report-history';
  goBackText = 'pages.support.claimByOperator';


  columnsConfig = [
    {
      displayValueKey: 'providerDetails',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.provider'),
      type: 'data',
      getDisplayValue: (provider) => {
        return provider.name;
      }
    },
    {
      displayValueKey: 'amount',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.amount'),
      type: 'data',
    }
  ];
  dataSource: any = [];
  reportId;

  constructor(private activatedRoute: ActivatedRoute,
              private reportDataService: ReportDataService,
              private translateService: TranslateService) {}

  ngOnInit() {
    this.reportId = this.activatedRoute.snapshot.params['reportId'];

    this.getReportById(this.reportId);
  }

  getReportById(reportId) {
    this.reportDataService.getReportById(reportId)
      .subscribe(details => {
        if (details) {
          // header
          const reportData = new Report(details);
          this.reportSearchResult.push({
            info: reportData,
          });

          // table
          const source = [];
          details.reportByProviderList.map( i => {
            i.providerDetails.name = this.translateService.instant('providers.' + i.providerDetails.name);
            source.push(i);
          });

          this.dataSource = source;
        }
      });
  }
}
