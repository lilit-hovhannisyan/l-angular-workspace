import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.less']
})
export class HomePageComponent implements OnInit {

  isAuthSub = Subscription.EMPTY;
  isForgotForm = false;

  constructor (private userDataService: UserDataService,
               private router: Router) {}

  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(data => {
      if (data) {
        this.router.navigate(['/user']);
      }
    });
  }


  onSignIn(event) {
    this.userDataService.signIn(event.username, event.password);
  }

  setIsForgotForm(isForgot) {
    this.isForgotForm = isForgot;
  }

}
