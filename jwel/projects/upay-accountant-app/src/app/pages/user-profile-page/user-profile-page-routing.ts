import { Routes } from '@angular/router';
import {UserProfilePageComponent} from './user-profile-page.component';

export const userProfileRoutes: Routes = [
  {
    path: '',
    component: UserProfilePageComponent
    // children: [
    //   {
    //     path: 'info',
    //     component: ProfileInfo //widget
    //   },
    //   {
    //     path: 'password',
    //     component: ProfilePassword // widget
    //   }
    //   {
    //     path: 'mobile',
    //     component: ProfileMobile // widget
    //   }
    //   {
    //     path: 'password',
    //     component: ProfileEmail // widget
    //   }
    // ]
  }
];
