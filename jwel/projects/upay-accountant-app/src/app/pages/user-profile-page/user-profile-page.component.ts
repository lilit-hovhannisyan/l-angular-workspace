import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertBoxService } from 'upay-lib';
import { UserDataService } from '../../dal/user/user-data.service';
import { UserInfoData } from '../../dal/user/user-data.models';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.less']
})
export class UserProfilePageComponent implements OnInit, OnDestroy {
  currentUser: UserInfoData;
  userSub = Subscription.EMPTY;

  oldPass: string;
  newPass: string;
  confirmPass: string;

  constructor(private userDataService: UserDataService, private alertBoxService: AlertBoxService ) {}

  ngOnInit() {
    this.userSub = this.userDataService.userInfo.subscribe(d => {
      this.currentUser = d;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  onResetPassword() {
    if ( this.oldPass && this.newPass && this.confirmPass ) {
      if ( this.newPass.length < 8 ) {
        this.alertBoxService.initMsg({type: 'error', text: 'Գաղտնաբառը պետք է լինի առնվազն 8 նիշ'});
        return;
      }

      if ( this.newPass === this.confirmPass ) {
        // alert('no api');
        // todo api
        this.userDataService.resetPassword(this.oldPass, this.newPass, this.currentUser.email).subscribe(d => {
          this.oldPass = '';
          this.newPass = '';
          this.confirmPass = '';
          this.alertBoxService.initMsg({type: 'success', text: 'Գաղտնաբառը հաջողությամբ փոխված է'});

          // update profile subscription
          this.userDataService.getProfile().subscribe();
        });
      } else {
        this.alertBoxService.initMsg ( {type: 'error', text: 'Գաղտնաբառերը չեն համընկնում'} );
      }
    } else {
      this.alertBoxService.initMsg ( {type: 'error', text: 'Բոլոր դաշտերը պարտադիր են'} );
    }
  }
}
