import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {UserProfilePageComponent} from './user-profile-page.component';
import {userProfileRoutes} from './user-profile-page-routing';

import {AtomsModules} from 'iu-ui-lib';

@NgModule({
  declarations: [
    UserProfilePageComponent
  ],
  imports: [
    FormsModule,
    RouterModule,
    RouterModule.forChild(userProfileRoutes),
    TranslateModule,
    AtomsModules
  ],
  exports: [
    UserProfilePageComponent
  ],
})
export class UserProfilePageModule {
}
