import {Routes} from '@angular/router';
import {PermissionsGuardService, PermissionTypes} from '../../core/guards/permissions-guard.service';
import {ClaimByCustomerDetailsPageComponent} from './claim-by-customer-details-page.component';

export const claimByCustomerDetailsRoutes: Routes = [
  {
    path: '',
    component: ClaimByCustomerDetailsPageComponent,
    canActivate: [PermissionsGuardService],
    data: {
      permissions: [PermissionTypes.CLAIMIST]
    }
  }
];
