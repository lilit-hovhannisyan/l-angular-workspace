import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClaimDataService} from '../../dal/claim/claim-data.service';
import {BusinessDataCardInfo, ClaimStatus, dateFormat, Notes} from 'upay-lib';
import {environment} from '../../../environments/environment';
import {ClaimApplicationActionsAccordingToStateService} from '../claim-application-page/claim-application-actions-according-to-state.service';

class ClaimedTransactionInfo {
  completed: BusinessDataCardInfo;
  receiptId: BusinessDataCardInfo;
  branchName: BusinessDataCardInfo;
  providerName: BusinessDataCardInfo;
  amount: BusinessDataCardInfo;
  userNum: BusinessDataCardInfo;
  operatorName: BusinessDataCardInfo;
  contactNum: BusinessDataCardInfo;

  constructor(data) {
    this.completed = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.date',
      value:  dateFormat(new Date(data.completed), 'dd/mm/yyyy'),
      customClass: 'bold'
    });

    this.receiptId = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.receipt',
      value:  data.receiptId,
      customClass: 'bold'
    });

    this.branchName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.branchName',
      value:  data.branchName,
      customClass: 'bold'
    });

    this.providerName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.provider',
      value:  data.providerName,
      customClass: 'bold'
    });

    this.amount = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.amount',
      value:  data.amount / 100,
      customClass: 'bold'
    });

    this.userNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.userNumber',
      value:  data.userNum,
      customClass: 'bold'
    });

    this.operatorName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.operatorName',
      value:  data.operatorName,
      customClass: 'bold'
    });

    this.contactNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.contactNum',
      value:  data.contactNum,
      customClass: 'bold'
    });
  }
}

@Component({
  selector: 'app-claim-by-customer-details-page',
  templateUrl: './claim-by-customer-details-page.component.html',
  styleUrls: ['./claim-by-customer-details-page.component.less']
})

export class ClaimByCustomerDetailsPageComponent implements OnInit {
  claimId = null;
  notes = '';

  claimSearchResult = [];

  goBackPath;
  goBackText = 'pages.claims.applicationByCUSTOMER';

  // description
  files = [];
  fileStorageUrl = environment.APIEndpoint.fileStorageUrl;

  claimData: any = {};
  customerDataConfig = [
    {
      groupName: 'group1',
      fields: [
        {
          displayValueKey: 'name',
          label: 'Անուն Ազգանուն',
        },
        {
          displayValueKey: 'passport',
          label: 'Անձնագրի համար',
        },
        {
          displayValueKey: 'dateOfIssue',
          label: 'Տրման ժամկետ',
        }
      ]
    },
    {
      groupName: 'group2',
      fields: [
        {
          displayValueKey: 'phone',
          label: 'Հեռախոսահամար',
        },
        {
          displayValueKey: 'issuedBy',
          label: 'Ում կողմից',
        },
        {
          displayValueKey: 'validTo',
          label: 'Վավերական է մինչև',
        }
      ]
    }
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private claimDataService: ClaimDataService,
              private claimApplicationActionsAccordingToStateService: ClaimApplicationActionsAccordingToStateService) {
  }

  ngOnInit() {
    this.crateClaimInfo();
  }

  crateClaimInfo() {
    this.claimId = this.activatedRoute.snapshot.params['claimId'];
    this.claimDataService.getClaimInfo(this.claimId).subscribe( res => {

      // init trancastion info
      const claimedTransactionInfo =  new ClaimedTransactionInfo(res);
      this.claimSearchResult.push({
        label: {
          displayText: 'pages.claims.' + res.status,
          customClass: res.status === ClaimStatus.DISMISSED ||
          res.status === ClaimStatus.PRE_REJECTED ||
          res.status === ClaimStatus.REJECTED ? 'error' : 'success'
        },
        info: claimedTransactionInfo
      });

      this.claimDataService.getClaimHistory(this.claimId).subscribe( notes => {
        // customer data
        const customerData = JSON.parse(res.customerPassportInfo);
        customerData['dateOfIssue'] = dateFormat(new Date(customerData['dateOfIssue']), 'dd/mm/yyyy');
        customerData['validTo'] = dateFormat(new Date(customerData['validTo']), 'dd/mm/yyyy');

        this.claimData = {
          direction: res.customerClaimDescription,
          examinationNotes: res.examinationNotes,
          ...customerData
        };

        this.claimData.notes = notes.map(note => {
          return new Notes(note);
        });

        // init actions && statuses according to claim state
        this.claimApplicationActionsAccordingToStateService.claimApplicationActionsInit(res, this.claimData);
      });

      // files
      this.claimDataService.getFilesInGroup(res.attachmentKey).subscribe( responseData => {
        this.files = responseData.files;
      });
    });
  }

  addNotes(event) {
    this.notes = event;
  }

  onActionClick(action) {
    // action behavior
    this.claimApplicationActionsAccordingToStateService.changeClaimApplicationStatusAccordingAction(action, this.claimId, this.notes, '/user/transaction/claims');
  }
}
