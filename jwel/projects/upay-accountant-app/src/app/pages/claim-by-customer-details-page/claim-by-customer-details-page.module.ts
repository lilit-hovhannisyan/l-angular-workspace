import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {SharedWidgetsModule} from 'upay-lib';
import {AtomsModules, MoleculesModule} from 'iu-ui-lib';

import {ClaimByCustomerDetailsPageComponent} from './claim-by-customer-details-page.component';
import {claimByCustomerDetailsRoutes} from './claim-by-customer-details-page.routing';

@NgModule({
  declarations: [
    ClaimByCustomerDetailsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimByCustomerDetailsRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    ClaimByCustomerDetailsPageComponent
  ]
})

export class ClaimByCustomerDetailsPageModule {
}
