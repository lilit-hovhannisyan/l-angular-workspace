import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';

import {SharedWidgetsModule} from 'upay-lib';

import {claimApplicationRoutes} from './claim-application-page.routing';
import {ClaimApplicationPageComponent} from './claim-application-page.component';

@NgModule({
  declarations: [
    ClaimApplicationPageComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(claimApplicationRoutes),
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    ClaimApplicationPageComponent
  ]
})

export class ClaimApplicationPageModule {
}
