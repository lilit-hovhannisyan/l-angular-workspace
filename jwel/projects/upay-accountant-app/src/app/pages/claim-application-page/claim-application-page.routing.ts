import {Routes} from '@angular/router';
import {ClaimApplicationPageComponent} from './claim-application-page.component';

export const claimApplicationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ClaimApplicationPageComponent,
      },
      {
        path: 'operator-claim-details/:claimId',
        loadChildren: '../claim-by-operator-details-page/claim-by-operator-details-page.module#ClaimByOperatorDetailsPageModule'
      },
      {
        path: 'customer-claim-details/:claimId',
        loadChildren: '../claim-by-customer-details-page/claim-by-customer-details-page.module#ClaimByCustomerDetailsPageModule'
      }
    ]
  }
];
