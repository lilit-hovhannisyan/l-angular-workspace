import {Component, Inject, LOCALE_ID} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ClaimDataService} from '../../dal/claim/claim-data.service';
import {IBusinessDataTableFooterData} from 'upay-lib';
import {formatDate} from '@angular/common';
import {Router} from '@angular/router';
import {CancelledTransactionData} from '../transaction-page/cancelled-transaction-history-page/cancelled-transaction-history-page.component';

@Component({
  selector: 'app-claim-application-page',
  templateUrl: './claim-application-page.component.html',
  styleUrls: []
})

export class ClaimApplicationPageComponent {
  columnsConfig = [
    {
      displayValueKey: 'completed',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.transactionDate'),
      type: 'data',
      // sortKey: 'completed',
      getDisplayValue: (date) => {
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayValueKey: 'claimOpened',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.claimDate'),
      type: 'data',
      // sortKey: 'completed',
      getDisplayValue: (date) => {
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      // sortKey: 'receiptId',
    },
    {
      displayValueKey: 'branchName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
      type: 'data',
      // sortKey: 'branchName'
    },
    {

      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      // sortKey: 'providerName'
    },
    {
      displayValueKey: 'amount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      // sortKey: 'amount',
    },
    {
      displayValueKey: 'userNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userNumber'),
      type: 'data',
      // sortKey: 'userNum'
    },
    {
      displayValueKey: 'operatorName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.nameSurname'),
      type: 'data',
      // sortKey: 'operatorName'
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.null'),
      type: 'action',
      dispatchActionName: 'details',
      actionIcon: 'icon_show'
    }
  ];
  dataSource: any = [];
  totalItems = 0;

  footerDataSource: IBusinessDataTableFooterData[] = [
    {
      label: 'widgets.reportDetails.text.quantity',
      value: 0
    }
  ];

  constructor(private translateService: TranslateService,
              private claimDataService: ClaimDataService,
              private router: Router,
              @Inject(LOCALE_ID) private locale: string) {
  }

  getPendingClaims(queryData) {
    this.claimDataService.countPendingCancellationClaims(queryData).subscribe(res => {
      this.footerDataSource[0].value = res;
    });

    this.claimDataService.getPendingCancellationClaims(queryData).subscribe(res => {
      this.dataSource = res.items.map (item => {
        return new CancelledTransactionData(item);
      });
      this.totalItems = res.total;
    });
  }

  onRowActionClick(event) {
    if (event.actionName === 'details') {
      const claimId = event.row.id;

      if (event.row.cancellationClaimType === 'OPERATOR') {
        this.router.navigate([`/user/claim-application/operator-claim-details/${claimId}`]);
        return;
      }
      this.router.navigate([`/user/claim-application/customer-claim-details/${claimId}`]);
    }
  }
}
