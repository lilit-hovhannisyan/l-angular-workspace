import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ClaimStatus} from 'upay-lib';
import {ClaimDataService} from '../../dal/claim/claim-data.service';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class ClaimApplicationActionsAccordingToStateService {
  constructor(private router: Router,
              private claimDataService: ClaimDataService) {
  }
  claimApplicationActionsInit(responseData, uiComponentObject) {
    // according to status render different blocks
    // --- 1st round
    if ( responseData.status === ClaimStatus.OPENED ) {
      uiComponentObject.actions = [
        {
          key: 'accept',
          name: 'buttons.accept',
          customClass: 'success'
        },
        {
          key: 'dismiss',
          name: 'buttons.dismiss',
          customClass: 'error'
        }
      ];
    }

    // --- 2nd round
    if ( responseData.status === ClaimStatus.ACCEPTED ) {
      uiComponentObject.actions = [
        {
          key: 'preApproveCancellationClaim',
          name: 'buttons.preApproveCancellationClaim',
          customClass: 'success'
        },
        {
          key: 'preRejectCancellationClaim',
          name: 'buttons.preRejectCancellationClaim',
          customClass: 'error'
        }
      ];
    }

    if ( responseData.status === ClaimStatus.DISMISSED ) {
      uiComponentObject.message.text = `pages.claims.${responseData.status}`;
      uiComponentObject.message.customClass = 'error';
    }

    // --- 3rd round
    if ( responseData.status === ClaimStatus.PRE_APPROVED ) {
      uiComponentObject.actions = [
        {
          key: 'approveCancellationClaim',
          name: 'buttons.approveCancellationClaim',
          customClass: 'success'
        }
      ];
    }

    if ( responseData.status === ClaimStatus.PRE_REJECTED ) {
      uiComponentObject.actions = [
        {
          key: 'rejectCancellationClaim',
          name: 'buttons.rejectCancellationClaim',
          customClass: 'error'
        }
      ];
    }

    // --- no round : just readonly messages
    if ( responseData.status === ClaimStatus.APPROVED ) {
      uiComponentObject.message.text = `pages.claims.${responseData.status}`;
    }

    if ( responseData.status === ClaimStatus.REJECTED ) {
      uiComponentObject.message.text = `pages.claims.${responseData.status}`;
      uiComponentObject.message.customClass = 'error';
    }

    // completed by operator
    if ( responseData.status === ClaimStatus.COMPLETED_APPROVED || responseData.status === ClaimStatus.COMPLETED_REJECTED) {
      uiComponentObject.message.text = `pages.claims.${responseData.status}`;
    }
  }

  changeClaimApplicationStatusAccordingAction(action, claimId, notes, redirectPath) {
    if ( action.key === 'accept') {
      this.claimDataService.acceptCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
    if ( action.key === 'dismiss') {
      this.claimDataService.dismissCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
    // 2nd round
    if ( action.key === 'preApproveCancellationClaim') {
      this.claimDataService.preApproveCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
    if ( action.key === 'preRejectCancellationClaim') {
      this.claimDataService.preRejectCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
    // 3nd round
    if ( action.key === 'approveCancellationClaim') {
      this.claimDataService.approveCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
    if ( action.key === 'rejectCancellationClaim') {
      this.claimDataService.rejectCancellationClaim(claimId, notes).subscribe( res => {
        this.router.navigate([redirectPath]);
      });
    }
  }
}
