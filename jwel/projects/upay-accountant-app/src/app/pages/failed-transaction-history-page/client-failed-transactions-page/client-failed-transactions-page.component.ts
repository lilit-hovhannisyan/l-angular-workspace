import {Component, Inject, LOCALE_ID, OnInit, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {formatDate} from '@angular/common';
import {FailedTransactionDataService} from '../../../dal/failed-transaction/failed-transaction-data.service';
import {AlertBoxService, dateFormat, LoadingService} from 'upay-lib';
// import {BusinessDataTableComponent} from 'upay-lib';

// accNumber: "10000031"
// amount: 10000
// commission: 20000
// forename: null
// issued: 1569240159603
// number: "10000050"
// phone: "+37491626208"
// providerId: "12c50da4-7cc4-4530-9902-7bbe818a8881"
// providerName: "UpayCustomerTopup"
// receiptId: "10000610"
// surname: null
// transactionId: "93415d15-0962-47cc-b38d-75cd2a11d7ef"
// userType: "UNIDENTIFIED"

@Component({
  selector: 'app-client-failed-transactions-page',
  templateUrl: './client-failed-transactions-page.component.html',
  styleUrls: ['./client-failed-transactions-page.component.less']
})
export class ClientFailedTransactionsPageComponent implements OnInit {
  columnsConfig = [
    {
      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      // sortKey: 'providerName'
    },
    {
      // displayValueKey: 'branchName',
      getCellValue: (rowData, columnData) => {
        return `${(rowData.forename || '---')} ${(rowData.surname || '---')} `;
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userName'),
      type: 'data',
      // sortKey: 'branchName'
    },
    {
      displayValueKey: 'phone',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.phone'),
      type: 'data',
      // sortKey: 'userNum'
    },
    {
      displayValueKey: 'number',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userNumber'),
      type: 'data',
      // sortKey: 'userNum'
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      // sortKey: 'receiptId',
    },
    {
      displayValueKey: 'totalAmount',
      getDisplayValue: (data) => {
        return data / 100;
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      // sortKey: 'amount',
    },
    {
      displayValueKey: 'issued',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.date'),
      type: 'data',
      // sortKey: 'issued',
      getDisplayValue: (date) => {
        return dateFormat(new Date(date), 'dd/mm/yyyy');
      }
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.retry'),
      type: 'action',
      dispatchActionName: 'retry',
      actionIcon: 'icon_retry'
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.refund'),
      type: 'action',
      dispatchActionName: 'refund',
      displayActionText: 'refund'
      // actionIcon: 'icon_done'
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.complete'),
      type: 'action',
      dispatchActionName: 'complete',
      actionIcon: 'icon_done'
    }
  ];
  dataSource: any = [];
  transactionId;
  totalItems = 0;

  @ViewChild('businessDataTableComponentRef') businessDataTableComponentRef: any;

  constructor(private translateService: TranslateService,
              private failedTransactionDataService: FailedTransactionDataService,
              private alertBoxService: AlertBoxService,
              private loadingService: LoadingService,
              @Inject(LOCALE_ID) private locale: string) {
  }

  ngOnInit() {}

  getFailedTransactions(event) {
    this.failedTransactionDataService.getCustomerFailedTransactions(event).subscribe( res => {
      this.dataSource = res.items;
      this.totalItems = res.total;
    });
  }

  onRowActionClick(event) {
    this.transactionId = event.row.transactionId;
    this.loadingService.showLoader(true);

    if ( event.actionName  === 'retry') {
      this.failedTransactionDataService.retryCustomerTransaction(this.transactionId).subscribe( res => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'success', text: 'Resend succeed'});
        this.updateData();
      }, err => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'error', text: 'Resend failed'});
      });
    }

    if (event.actionName  === 'complete') {
      this.failedTransactionDataService.completeCustomerTransaction(this.transactionId).subscribe( res => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'success', text: 'Complete succeed'});
        this.updateData();
      }, err => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'error', text: 'Complete failed'});
      });
    }

    if (event.actionName  === 'refund') {
      this.failedTransactionDataService.refundCustomerTransaction(this.transactionId).subscribe( res => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'success', text: 'Refund succeed'});
        this.updateData();
      }, err => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'error', text: 'Refund failed'});
      });
    }

  }

  updateData() {
    this.businessDataTableComponentRef.onGetDataSource();
  }
}
