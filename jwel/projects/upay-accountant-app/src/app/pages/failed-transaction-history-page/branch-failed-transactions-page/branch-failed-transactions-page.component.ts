import {Component, Inject, LOCALE_ID, OnInit, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {formatDate} from '@angular/common';
import {FailedTransactionDataService} from '../../../dal/failed-transaction/failed-transaction-data.service';
import {AlertBoxService, LoadingService} from 'upay-lib';

@Component({
  selector: 'app-branch-failed-transactions-page',
  templateUrl: './branch-failed-transactions-page.component.html',
  styleUrls: ['./branch-failed-transactions-page.component.less']
})
export class BranchFailedTransactionsPageComponent implements OnInit {
  activeFilters = ['receiptId', 'operatorId', 'branchId', 'cashboxId', 'providerName', 'userNum'];
  columnsConfig = [
    {
      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      // sortKey: 'providerName'
    },
    {
      displayValueKey: 'branchName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
      type: 'data',
      // sortKey: 'branchName'
    },
    {
      displayValueKey: 'userNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userNumber'),
      type: 'data',
      // sortKey: 'userNum'
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      // sortKey: 'receiptId',
    },
    {
      displayValueKey: 'totalAmount',
      getDisplayValue: (data) => {
        return data / 100;
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      // sortKey: 'amount',
    },
    {
      displayValueKey: 'issued',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.date'),
      type: 'data',
      // sortKey: 'issued',
      getDisplayValue: (date) => {
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.retry'),
      type: 'action',
      dispatchActionName: 'retry',
      actionIcon: 'icon_retry'
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.complete'),
      type: 'action',
      dispatchActionName: 'complete',
      actionIcon: 'icon_done'
    }
  ];
  dataSource: any = [];
  totalItems = 0;
  transactionId;

  @ViewChild('businessDataTableComponentRef') businessDataTableComponentRef: any;

  constructor(private translateService: TranslateService,
              private failedTransactionDataService: FailedTransactionDataService,
              private alertBoxService: AlertBoxService,
              private loadingService: LoadingService,
              @Inject(LOCALE_ID) private locale: string) {
  }

  ngOnInit() {}

  getFailedTransactions(event) {
    this.failedTransactionDataService.getBranchFailedTransactions(event).subscribe( res => {
      this.dataSource = res.items;
      this.totalItems = res.total;
    });
  }

  onRowActionClick(event) {
    this.transactionId = event.row.id;
    this.loadingService.showLoader(true);

    if ( event.actionName  === 'retry') {
      this.failedTransactionDataService.retryTransaction(this.transactionId).subscribe( res => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'success', text: 'Resend succeed'});
        this.updateData();
      }, err => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'error', text: 'Resend failed'});
      });
    }

    if (event.actionName  === 'complete') {
      this.failedTransactionDataService.completeTransaction(this.transactionId).subscribe( res => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'success', text: 'Complete succeed'});
        this.updateData();
      }, err => {
        this.loadingService.showLoader(false);
        this.alertBoxService.initMsg({type: 'error', text: 'Complete failed'});
      });
    }

  }

  updateData() {
    this.businessDataTableComponentRef.onGetDataSource();
  }
}
