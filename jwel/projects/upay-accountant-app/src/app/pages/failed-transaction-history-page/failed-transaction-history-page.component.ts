import {Component, OnInit} from '@angular/core';
import {UserPermissionService} from '../../dal/user/user-permission.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-failed-transaction-history-page',
  templateUrl: './failed-transaction-history-page.component.html',
  styleUrls: ['./failed-transaction-history-page.component.less']
})
export class FailedTransactionHistoryPageComponent implements OnInit {
  tabPathPrefix = '';
  translationprefix = '';
  tabs;

  constructor(private userPermissionService: UserPermissionService,
              private router: Router) {}

  ngOnInit()  {
    this.userPermissionService.createFailedTransactionPageTabs().subscribe(tabsList => {
      this.tabs = tabsList;
      if (this.router.url === '/user/failed-transaction-history') {
        this.router.navigate(['/user/failed-transaction-history', tabsList[0].link]);
      }
    });
  }

}
