import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../widgets/widgets.module';
import {FailedTransactionHistoryPageComponent} from './failed-transaction-history-page.component';
import {failedTransactionHistoryRoutes} from './failed-transaction-history-page-routing';
import {SharedWidgetsModule} from 'upay-lib';
import {ClientFailedTransactionsPageComponent} from './client-failed-transactions-page/client-failed-transactions-page.component';
import {CommonModule} from '@angular/common';
import {BranchFailedTransactionsPageComponent} from './branch-failed-transactions-page/branch-failed-transactions-page.component';

@NgModule({
  declarations: [
    FailedTransactionHistoryPageComponent,
    ClientFailedTransactionsPageComponent,
    BranchFailedTransactionsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(failedTransactionHistoryRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    FailedTransactionHistoryPageComponent
  ],
})
export class FailedTransactionHistoryPageModule {
}
