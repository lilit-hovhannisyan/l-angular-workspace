import { Routes } from '@angular/router';
import {FailedTransactionHistoryPageComponent} from './failed-transaction-history-page.component';
import {ClientFailedTransactionsPageComponent} from './client-failed-transactions-page/client-failed-transactions-page.component';
import {BranchFailedTransactionsPageComponent} from './branch-failed-transactions-page/branch-failed-transactions-page.component';

export const failedTransactionHistoryRoutes: Routes = [
  {
    path: '',
    component: FailedTransactionHistoryPageComponent,
    children: [
      {
        path: 'client',
        component: ClientFailedTransactionsPageComponent
      },
      {
        path: 'branch',
        component: BranchFailedTransactionsPageComponent
      }
    ]
  }
];
