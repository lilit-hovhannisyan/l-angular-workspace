import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClaimDataService} from '../../dal/claim/claim-data.service';
import {BusinessDataCardInfo, ClaimStatus, dateFormat, Notes} from 'upay-lib';
import {
  ClaimApplicationActionsAccordingToStateService
} from '../claim-application-page/claim-application-actions-according-to-state.service';

class ClaimedTransactionInfo {
  completed: BusinessDataCardInfo;
  receiptId: BusinessDataCardInfo;
  branchName: BusinessDataCardInfo;
  providerName: BusinessDataCardInfo;
  amount: BusinessDataCardInfo;
  userNum: BusinessDataCardInfo;
  operatorName: BusinessDataCardInfo;
  contactNum: BusinessDataCardInfo;

  constructor(data) {
    this.completed = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.date',
      value:  dateFormat(new Date(data.completed), 'dd/mm/yyyy'),
      customClass: 'bold'
    });

    this.receiptId = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.receipt',
      value:  data.receiptId,
      customClass: 'bold'
    });

    this.branchName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.branchName',
      value:  data.branchName,
      customClass: 'bold'
    });

    this.providerName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.provider',
      value:  data.providerName,
      customClass: 'bold'
    });

    this.amount = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.amount',
      value:  data.amount / 100,
      customClass: 'bold'
    });

    this.userNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.userNumber',
      value:  data.userNum,
      customClass: 'bold'
    });

    this.operatorName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.operatorName',
      value:  data.operatorName,
      customClass: 'bold'
    });

    this.contactNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.contactNum',
      value:  data.contactNum,
      customClass: 'bold'
    });
  }
}

class ClaimDescriptionInOperatorInterface {
  reason: string;
  examinationNotes: string;
  documents: any[];
  actions: any[];
  message: any;

  constructor(data) {
    this.reason = 'pages.claims.' + data.operatorClaimType;
    this.examinationNotes = data.examinationNotes;
    this.actions = [];
    this.message = {};
  }
}

@Component({
  selector: 'app-claim-by-operator-details-page',
  templateUrl: './claim-by-operator-details-page.component.html',
  styleUrls: ['./claim-by-operator-details-page.component.less']
})

export class ClaimByOperatorDetailsPageComponent implements OnInit {
  claimId = null;
  claimSearchResult = [];

  goBackPath = '/user/transaction/claims';
  goBackText = 'pages.claims.applicationByOPERATOR';

  notes = '';
  allowNewNotes = true;

  claimDescriptionData;



  constructor(private activatedRoute: ActivatedRoute,
              private claimDataService: ClaimDataService,
              private claimApplicationActionsAccordingToStateService: ClaimApplicationActionsAccordingToStateService) {
  }

  ngOnInit() {
    this.claimId = this.activatedRoute.snapshot.params['claimId'];
    this.claimDataService.getClaimInfo(this.claimId).subscribe( res => {
      // init trancastion info
      const claimedTransactionInfo =  new ClaimedTransactionInfo(res);
      this.claimSearchResult.push({
        label: {
          displayText: 'pages.claims.' + res.status,
          customClass: res.status === ClaimStatus.DISMISSED ||
          res.status === ClaimStatus.PRE_REJECTED ||
          res.status === ClaimStatus.REJECTED ? 'error' : 'success'
        },
        info: claimedTransactionInfo
      });

      // main blocks

      this.claimDataService.getClaimHistory(this.claimId).subscribe( notes => {
        this.claimDescriptionData = new ClaimDescriptionInOperatorInterface(res);

        this.claimDescriptionData.notes = notes.map(note => {
          return new Notes(note);
        });

        // init actions && statuses according to claim state
        this.claimApplicationActionsAccordingToStateService.claimApplicationActionsInit(res, this.claimDescriptionData);

        if ( res.status === ClaimStatus.COMPLETED_APPROVED || res.status === ClaimStatus.COMPLETED_REJECTED || res.status === ClaimStatus.DISMISSED) {
         this.allowNewNotes = false;
        }
      });
    });
  }

  addNotes(event) {
    this.notes = event;
  }

  onActionClick(action) {
    // action behavior
    this.claimApplicationActionsAccordingToStateService.changeClaimApplicationStatusAccordingAction(action, this.claimId, this.notes, '/user/transaction/claims');
  }
}
