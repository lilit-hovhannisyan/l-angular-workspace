import {Routes} from '@angular/router';
import {PermissionsGuardService, PermissionTypes} from '../../core/guards/permissions-guard.service';
import {ClaimByOperatorDetailsPageComponent} from './claim-by-operator-details-page.component';

export const claimByOperatorDetailsRoutes: Routes = [
  {
    path: '',
    component: ClaimByOperatorDetailsPageComponent,
    canActivate: [PermissionsGuardService],
    data: {
      permissions: [PermissionTypes.CLAIMIST]
    }
  }
];
