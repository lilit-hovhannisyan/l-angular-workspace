import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {SharedWidgetsModule} from 'upay-lib';


import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {ClaimByOperatorDetailsPageComponent} from './claim-by-operator-details-page.component';
import {claimByOperatorDetailsRoutes} from './claim-by-operator-details-page.routing';

@NgModule({
  declarations: [
    ClaimByOperatorDetailsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimByOperatorDetailsRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    ClaimByOperatorDetailsPageComponent
  ]
})

export class ClaimByOperatorDetailsPageModule {
}
