import { Routes } from '@angular/router';
import {TransactionHistoryPageComponent} from './transaction-history-page.component';
import {PermissionsGuardService, PermissionTypes} from '../../../core/guards/permissions-guard.service';

export const transactionHistoryRoutes: Routes = [
  {
    path: '',
    component: TransactionHistoryPageComponent,
    canActivate: [PermissionsGuardService],
    data: {
      // permissions: [PermissionTypes.SUPERPUPER]
    }
  }
];
