import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {TransactionHistoryPageComponent} from './transaction-history-page.component';
import {transactionHistoryRoutes} from './transaction-history-page-routing';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    TransactionHistoryPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(transactionHistoryRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    TransactionHistoryPageComponent
  ],
})
export class TransactionHistoryPageModule {
}
