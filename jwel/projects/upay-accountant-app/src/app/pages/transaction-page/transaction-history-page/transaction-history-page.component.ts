import { Component, Inject, LOCALE_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {formatDate} from '@angular/common';
import {TransactionDataService} from '../../../dal/transaction/transaction-data.service';
import {dateFormat} from 'upay-lib';

@Component({
  selector: 'app-transaction-history-page',
  templateUrl: './transaction-history-page.component.html',
  styleUrls: []
})
export class TransactionHistoryPageComponent {
  activeFilters = ['receiptId', 'operatorId', 'branchId', 'cashboxId', 'providerName', 'userNum'];
  orderedFieldsArr = [{key: 'date', val: false}];
  columnsConfig = [
    {
      displayValueKey: 'externalId',
      displayName: () =>  this.translateService.instant('widgets.transaction_list.header.externalId'),
      type: 'data',
      sortKey: 'externalId',
    },
    {
      displayValueKey: 'issuedDate',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.date'),
      type: 'data',
      sortKey: 'date',
      getDisplayValue: (date) => {
        return dateFormat(date, 'dd/mm/yyyy HH:MM');
      }
    },
    {
      displayValueKey: 'branchName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
      type: 'data',
      sortKey: 'branchId',
    },
    {
      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      sortKey: 'providerName'
    },
    {
      displayValueKey: 'totalAmount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      sortKey: 'totalAmount',
    },
    {
      displayValueKey: 'contactNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.contactNum'),
      type: 'data',
      sortKey: 'contactNum'
    },
    {
      displayValueKey: 'isCash',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.isCash'),
      type: 'data',
      getDisplayValue: (isCash) => {
        if (isCash) {
          return 'Կանխիկ';
        }
        return 'Անկանխիկ';
      },
      sortKey: 'isCash',
    },
    {
      displayValueKey: 'posAuthcode',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.posAuthcode'),
      type: 'data',
      sortKey: 'posAuthcode',
    },
    {
      displayValueKey: 'subscriberNumber',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userNumber'),
      type: 'data',
      sortKey: 'userNum'
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      sortKey: 'receiptId',
    },
    {
      displayValueKey: 'commissionAmount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.commissionAmount'),
      type: 'data',
      // sortKey: 'userNum'
    },
    {
      displayValueKey: 'details',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.details'),
      type: 'data',
      // sortKey: 'userNum'
    },
    // {
    //   displayName: () => this.translateService.instant('widgets.transaction_list.header.print'),
    //   type: 'action',
    //   dispatchActionName: 'print',
    //   actionIcon: 'icon_print'
    // }
  ];
  dataSource: any = [];
  totalItems = 0;

  constructor(private transactionDataService: TransactionDataService,
              private translateService: TranslateService,
              @Inject(LOCALE_ID) private locale: string) {
  }

  getTransactionList(event) {
    this.transactionDataService.getTransactionList(event).subscribe( res => {
      this.dataSource = res.items;
      this.totalItems = res.total;
    });
  }

  downLoadTransactionsReport(queryData) {
    this.transactionDataService.downLoadTransactionsReport(queryData).subscribe( res => {
    });
  }
}
