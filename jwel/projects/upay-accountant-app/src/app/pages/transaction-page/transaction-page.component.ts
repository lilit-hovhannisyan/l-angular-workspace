import {Component, OnInit} from '@angular/core';
import {UserPermissionService} from '../../dal/user/user-permission.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-transaction-page',
  templateUrl: './transaction-page.component.html',
  styleUrls: ['./transaction-page.component.less']
})

export class TransactionPageComponent implements OnInit {
  tabPathPrefix = '';
  translationprefix = '';
  tabs;

  constructor(private userPermissionService: UserPermissionService,
              private router: Router) {}

  ngOnInit()  {
    this.userPermissionService.createTransactionPageTabs().subscribe(tabsList => {
      this.tabs = tabsList;
      if (this.router.url === '/user/transaction') {
        this.router.navigate(['/user/transaction', tabsList[0].link]);
      }
    });
  }

}
