import {Routes} from '@angular/router';
import {CancelledTransactionHistoryPageComponent} from './cancelled-transaction-history-page.component';

export const cancelledTransactionHistoryRoutes: Routes = [
  {
    path: '',
    component: CancelledTransactionHistoryPageComponent
  }
];
