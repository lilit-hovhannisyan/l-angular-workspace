import {Component, Inject, LOCALE_ID} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ClaimDataService} from '../../../dal/claim/claim-data.service';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';
import {splitNumberWithLumas} from 'upay-lib';

export class CancelledTransactionData {
  providerName: string;
  userNum: string;
  branchName: string;
  completed: Date;
  claimOpened: Date;
  amount: string;
  cancellationClaimType: string;
  status: string;
  receiptId: string;
  id: string;
  operatorName: string;

  constructor (data) {
    this.providerName = data.providerName;
    this.userNum = data.userNum;
    this.branchName = data.branchName;
    this.completed = data.completed;
    this.claimOpened = data.claimOpened;
    this.amount = splitNumberWithLumas(data.amount / 100);
    this.cancellationClaimType = data.cancellationClaimType;
    this.status = data.status;
    this.receiptId = data.receiptId;
    this.id = data.id;
    this.operatorName = data.operatorName;

  }
}

@Component({
  selector: 'app-cancelled-transaction-history-page',
  templateUrl: './cancelled-transaction-history-page.component.html',
  styleUrls: []
})

export class CancelledTransactionHistoryPageComponent {
  columnsConfig = [
    {
      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      sortKey: 'providerName'
    },
    {
      displayValueKey: 'userNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.userNumber'),
      type: 'data',
      sortKey: 'userNum'
    },
    {
      displayValueKey: 'branchName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
      type: 'data',
      sortKey: 'branchName'
    },
    {
      displayValueKey: 'completed',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.transactionDate'),
      type: 'data',
      sortKey: 'completed',
      getDisplayValue: (date) => {
        // debugger
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayValueKey: 'claimOpened',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.claimDate'),
      type: 'data',
      sortKey: 'claimOpened',
      getDisplayValue: (claimOpenedDate) => {
        // debugger
        if ( claimOpenedDate ) {
          return formatDate(claimOpenedDate, 'dd/MM/yyyy', this.locale);
        } else {
          return '---';
        }
      }
    },
    {
      displayValueKey: 'amount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      sortKey: 'amount',
    },
    {
      getCellValue: (rowData, columnData) => {
        const key = 'pages.claims.applicationBy' + rowData.cancellationClaimType;
        return this.translateService.instant(key);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.cancellationClaimType'),
      type: 'data',
      sortKey: 'cancellationClaimType',
    },
    {
      getCellValue: (rowData, columnData) => {
        const key = 'pages.claims.' + rowData.status;
        return this.translateService.instant(key);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.status'),
      type: 'data',
      sortKey: 'status',
      customClass: [
        {className: 'success', conditionKey: 'status', conditionValue: ['APPROVED', 'ACCEPTED', 'OPENED', 'PRE_APPROVED', 'COMPLETED_APPROVED']},
        {className: 'error', conditionKey: 'status', conditionValue: ['REJECTED', 'DISMISSED', 'PRE_REJECTED', 'COMPLETED_REJECTED']}
      ],
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.null'),
      type: 'action',
      dispatchActionName: 'details',
      actionIcon: 'icon_show'
    }
  ];
  dataSource: any = [];
  totalItems = 0;

  footerDataSource;

  activeFilters = ['receiptId', 'branchId', 'providerName', 'userNum'];
  orderedFieldsArr = [{key: 'claimOpened', val: false}];

  constructor(private translateService: TranslateService,
              private claimDataService: ClaimDataService,
              private router: Router,
              @Inject(LOCALE_ID) private locale: string) {
  }

  getPendingClaims(queryData) {
    // this.claimDataService.countPendingCancellationClaims(queryData).subscribe( res => {
    //   this.footerDataSource.push({
    //     label: 'widgets.reportDetails.text.quantity',
    //     value: res
    //   });
    //
    // });

    this.claimDataService.getFilterClaims(queryData).subscribe( res => {
      this.dataSource = res.items.map (item => {
        return new CancelledTransactionData(item);
      });
      this.totalItems = res.total;
    });
  }

  onRowActionClick(event) {
    if (event.actionName === 'details') {
      const claimId = event.row.id;

      if (event.row.cancellationClaimType === 'OPERATOR') {
        this.router.navigate([`/user/claim-application/operator-claim-details/${claimId}`]);
        return;
      }
      this.router.navigate([`/user/claim-application/customer-claim-details/${claimId}`]);
    }
  }

}
