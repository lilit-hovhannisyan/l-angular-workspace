import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {SharedWidgetsModule} from 'upay-lib';
import {CancelledTransactionHistoryPageComponent} from './cancelled-transaction-history-page.component';
import {cancelledTransactionHistoryRoutes} from './cancelled-transaction-history-page.routing';

@NgModule({
  declarations: [
    CancelledTransactionHistoryPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(cancelledTransactionHistoryRoutes),
    TranslateModule,
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    CancelledTransactionHistoryPageComponent
  ]
})
export class CancelledTransactionHistoryPageModule {}
