import {Routes} from '@angular/router';
import {TransactionPageComponent} from './transaction-page.component';

export const transactionRoutes: Routes = [
  {
    path: '',
    component: TransactionPageComponent,
    children: [
      // {
      //   path: '',
      //   pathMatch: 'full',
      //   redirectTo: 'history'
      // },
      {
        path: 'history',
        loadChildren: './transaction-history-page/transaction-history-page.module#TransactionHistoryPageModule'
      },
      {
        path: 'claims',
        loadChildren: './cancelled-transaction-history-page/cancelled-transaction-history-page.module#CancelledTransactionHistoryPageModule'
      }
    ]
  }
];
