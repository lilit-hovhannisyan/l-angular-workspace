import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {TransactionPageComponent} from './transaction-page.component';
import {transactionRoutes} from './transaction-page.routing';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    TransactionPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(transactionRoutes),
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    TransactionPageComponent
  ]
})
export class TransactionPageModule {}
