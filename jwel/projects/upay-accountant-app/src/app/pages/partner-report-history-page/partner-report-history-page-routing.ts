import {Routes} from '@angular/router';
import { PartnerReportHistoryPageComponent } from './partner-report-history-page.component';

export const partnerReportHistoryRoutes: Routes = [
  {
    path: '',
    component: PartnerReportHistoryPageComponent,
  }
];

