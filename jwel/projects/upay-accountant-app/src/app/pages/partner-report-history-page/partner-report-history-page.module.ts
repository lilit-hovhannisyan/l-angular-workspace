import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../widgets/widgets.module';
import {PartnerReportHistoryPageComponent} from './partner-report-history-page.component';
import {partnerReportHistoryRoutes} from './partner-report-history-page-routing';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    PartnerReportHistoryPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(partnerReportHistoryRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    PartnerReportHistoryPageComponent
  ],
})
export class PartnerReportHistoryPageModule {
}
