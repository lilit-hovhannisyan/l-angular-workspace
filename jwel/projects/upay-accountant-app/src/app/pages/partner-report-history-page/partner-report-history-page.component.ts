import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {PartnerDataService} from '../../dal/partner/partner-data.service';
import {IBusinessDataTableFooterData} from 'upay-lib';
import {map} from 'rxjs/operators';
import { AlertBoxService } from 'upay-lib';

@Component({
  selector: 'app-partner-report-history-page',
  templateUrl: './partner-report-history-page.component.html',
  styleUrls: ['./partner-report-history-page.component.less']
})
export class PartnerReportHistoryPageComponent implements OnInit {

  columnsConfig = [
    {
      displayValueKey: 'provider',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.provider'),
      type: 'data',
      sortKey: 'providerName'
    },
    {
      displayValueKey: 'branchName',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
      type: 'data',
      sortKey: 'branchId',
    },
    {
      displayValueKey: 'amount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      sortKey: 'amount'
    },
    {
      displayValueKey: 'externalCommission',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.externalCommission'),
      type: 'data',
      sortKey: 'commission'
    },
    {
      displayValueKey: 'count',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.count'),
      type: 'data',
      sortKey: 'quantity'
    }
  ];
  activeFilters = ['cashboxId', 'branchId', 'providerName'];
  dataSource: any[];
  footerDataSource: IBusinessDataTableFooterData[];
  totalItems = 0;

  constructor(private translateService: TranslateService,
              private partnerDataService: PartnerDataService,
              private alertBoxService: AlertBoxService,
              @Inject(LOCALE_ID) private locale: string, // todo create logick
  ) {
  }

  ngOnInit() {

  }

  getPartnersReports(queryData) {
    this.partnerDataService.getPartnerReportSummaryTotal(queryData)
      .pipe(map(d => {
          const footerDataSource: IBusinessDataTableFooterData[] = [];
          footerDataSource.push({
            label: 'widgets.reportDetails.text.amount',
            value: d.amount,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.commission',
            value: d.commission,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.quantity',
            value: d.quantity,
          });

          return footerDataSource;
        }
      ))
      .subscribe(data => {
        this.footerDataSource = data;
      });

    this.partnerDataService.getPartnersReports(queryData).subscribe(data => {
      this.dataSource = data.items;
      this.totalItems = data.total;
    });
  }

  downloadPartnersReport(queryData) {
    this.partnerDataService.downloadPartnerReport(queryData).subscribe(response => {
      if (response) {
        this.alertBoxService.initMsg({type: 'success', text: 'Հաշվետվությունը հաջողությամբ արտածվել է'});
      } else {
        this.alertBoxService.initMsg({type: 'error', text: 'Something went wrong'});
      }
    });
  }
}
