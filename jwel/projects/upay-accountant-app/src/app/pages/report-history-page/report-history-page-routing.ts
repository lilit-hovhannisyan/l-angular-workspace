import {Routes} from '@angular/router';
import {ReportHistoryPageComponent} from './report-history-page.component';
import {PermissionsGuardService, PermissionTypes} from '../../core/guards/permissions-guard.service';

export const reportHistoryRoutes: Routes = [
  {
    path: '',
    component: ReportHistoryPageComponent,
    canActivate: [PermissionsGuardService],
    data: {
      permissions: [PermissionTypes.ACCOUNTANT]
    }
  }
];

