import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../widgets/widgets.module';
import {reportHistoryRoutes} from './report-history-page-routing';
import {ReportHistoryPageComponent} from './report-history-page.component';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    ReportHistoryPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(reportHistoryRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    ReportHistoryPageComponent
  ],
})
export class ReportHistoryPageModule {
}
