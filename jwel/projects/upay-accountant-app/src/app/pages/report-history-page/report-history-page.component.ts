import {Component, OnInit, Inject, LOCALE_ID} from '@angular/core';
import {ReportDataService} from '../../dal/report/report-data.service';
import {TranslateService} from '@ngx-translate/core';
import {formatDate} from '@angular/common';
import {IBusinessDataTableFooterData} from 'upay-lib';
import {map} from 'rxjs/internal/operators';
import {Router} from '@angular/router';


@Component({
  selector: 'app-report-history-page',
  templateUrl: './report-history-page.component.html',
  styleUrls: ['./report-history-page.component.less']
})
export class ReportHistoryPageComponent implements OnInit {
  columnsConfig = [
    {
      displayValueKey: 'date',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.date'),
      type: 'data',
      sortKey: 'created',
      getDisplayValue: (date) => {
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayValueKey: 'date',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.closingTime'),
      type: 'data',
      sortKey: 'created',
      getDisplayValue: (date) => {
        return formatDate(date, 'HH:mm', this.locale);
      }
    },
    {
      displayValueKey: 'cashboxName',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.cashbox'),
      type: 'data',
      sortKey: 'cashboxName'
    },
    // {
    //   displayValueKey: 'operatorUsername',
    //   displayName: () => this.translateService.instant('widgets.reportDetails.text.username'),
    //   type: 'data',
    //   sortKey: 'operatorUsername',
    // },
    {
      displayValueKey: 'operatorName',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.nameSurname'),
      type: 'data',
      sortKey: 'operatorName',
    },
    {
      displayValueKey: 'startBalance',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.startBalance'),
      type: 'data',
    },
    {
      displayValueKey: 'cashInReport',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.cashInReport'),
      type: 'data',
      // sortKey: 'balance',
    },
    {
      displayValueKey: 'cashOutReport',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.cashOutReport'),
      type: 'data',
      // sortKey: 'balance',
    },
    {
      displayValueKey: 'balance',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.endBalance'),
      type: 'data',
      sortKey: 'balance',
    },
    // {
    //   displayValueKey: 'saldo',
    //   displayName: () => this.translateService.instant('widgets.reportDetails.text.saldo'),
    //   type: 'data',
    //   // sortKey: 'balance',
    // },
    {
      displayName: () => this.translateService.instant('widgets.reportDetails.text.details'),
      type: 'action',
      dispatchActionName: 'details',
      actionIcon: 'icon_show',
    }
  ];
  activeFilters = ['cashboxId', 'operatorId'];
  dataSource: any = [];
  footerDataSource: IBusinessDataTableFooterData[];
  totalItems = 0;

  constructor(private reportDataService: ReportDataService,
              private translateService: TranslateService,
              private router: Router,
              @Inject(LOCALE_ID) private locale: string) {
  }

  ngOnInit() {
  }

  getReportList(queryData) {
    this.reportDataService.getCashboxReportsSummary(queryData)
      .pipe(map(d => {
          const footerDataSource: IBusinessDataTableFooterData[] = [];
          footerDataSource.push({
            label: 'widgets.reportDetails.text.amountIn',
            value: d.amountIn,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.amountOut',
            value: d.amountOut,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.openingBalance',
            value: d.openingBalance,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.balance',
            value: d.balance,
          });
          footerDataSource.push({
            label: 'widgets.reportDetails.text.quantity',
            value: d.quantity,
          });

          return footerDataSource;
        }
      ))
      .subscribe(data => {
        this.footerDataSource = data;
      });

    this.reportDataService.getCashboxReports(queryData)
      .subscribe(data => {
        this.dataSource = data.items;
        this.totalItems = data.total;
      });
  }

  downloadReport(event) {
    this.reportDataService.downloadReport(event);
  }

  onRowActionClick(event) {
    if ( event.actionName === 'details') {
      this.router.navigateByUrl(`/user/report-view/${event.row.id}`);
    }
  }
}
