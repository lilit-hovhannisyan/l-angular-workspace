import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

import {AtomsModules, MoleculesModule, OrganismsModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';

import {AvatarComponent} from './avatar/avatar.component';
import {ReportDetailsComponent} from './report-details/report-details.component';
import {ReportCardComponent} from './report-card/report-card.component';



@NgModule({
  declarations: [
    AvatarComponent,
    ReportDetailsComponent,
    ReportCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule,
    SharedWidgetsModule
  ],
  exports: [
    AvatarComponent,
    ReportDetailsComponent,
    ReportCardComponent
  ],
  providers: [],
})
export class WidgetsModule { }
