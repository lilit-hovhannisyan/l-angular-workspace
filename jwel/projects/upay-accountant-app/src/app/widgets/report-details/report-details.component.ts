import {Component, Inject, Input, LOCALE_ID, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ReportDataService} from '../../dal/report/report-data.service';
import {ReportData} from '../../dal/report/report-data.models';
import {Subscription} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {IColumnConfig} from 'iu-ui-lib/lib/molecules/data-table/data-table.component';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.less']
})
export class ReportDetailsComponent implements OnInit, OnDestroy {

  reportByProviderColumnsConfig: IColumnConfig[] = [
    {
      displayValueKey: 'providerDetails',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.provider'),
      type: 'data',
      getDisplayValue: (provider) => {
        return provider.name;
      }
    },
    {
      displayValueKey: 'amount',
      displayName: () => this.translateService.instant('widgets.reportDetails.text.amount'),
      type: 'data',
    }
  ];

  reportData: ReportData;
  activeRouteSub = Subscription.EMPTY;
  reportDetailsSub = Subscription.EMPTY;

  constructor(@Inject(LOCALE_ID) private locale: string,
              private activeRoute: ActivatedRoute,
              private translateService: TranslateService,
              private _location: Location,
              private reportDataService: ReportDataService) {
  }

  ngOnInit() {
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      const reportId = routeParams['reportId'];
      this.reportDetailsSub.unsubscribe();
      this.reportDetailsSub = this.reportDataService.getReportById(reportId)
        .subscribe(details => {

          this.reportData = details;

          this.reportData.reportByProviderList.map( i => {
              i.providerDetails.name = this.translateService.instant('providers.' + i.providerDetails.name);
          });
        });
    });

  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
    this.reportDetailsSub.unsubscribe();
  }

  goBack() {
    this._location.back();
  }
}
