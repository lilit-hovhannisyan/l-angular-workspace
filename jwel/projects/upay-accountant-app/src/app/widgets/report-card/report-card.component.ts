import {Component, Input} from '@angular/core';
import {BusinessDataCardInfo, dateFormat, getMoneyPriceWithoutLumas} from 'upay-lib';

export class Report {

  cashboxName: BusinessDataCardInfo;
  date: BusinessDataCardInfo;
  closingTime: BusinessDataCardInfo;
  operatorName: BusinessDataCardInfo;
  startBalance: BusinessDataCardInfo;
  balance: BusinessDataCardInfo;
  saldo: BusinessDataCardInfo;


  constructor(data) {
    this.cashboxName = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.cashboxId',
      value:  data.cashboxName,
      customClass: 'bold'
    });

    this.date = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.date',
      value:  dateFormat(new Date(data.date), 'dd/mm/yyyy'),
      customClass: 'bold'
    });

    this.closingTime = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.closingTime',
      value:  dateFormat(new Date(data.date), 'HH:mm'),
      customClass: 'bold'
    });

    this.operatorName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.nameSurname',
      value:  data.operatorName,
      customClass: 'bold'
    });

    this.startBalance = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.startBalance',
      value:  data.startBalance,
      customClass: 'bold'
    });

    this.balance = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.endBalance',
      value:  data.balance,
      customClass: 'bold'
    });

    this.saldo = new BusinessDataCardInfo({
      displayText: 'widgets.reportDetails.text.saldo',
      value:  data.saldo,
      customClass: 'bold'
    });
  }

}

@Component({
  selector: 'app-report-card',
  templateUrl: './report-card.component.html',
  styleUrls: []
})

export class ReportCardComponent {
  @Input() reportSearchResult: Report[] = [];
  @Input() hasGoBack = false;
  @Input() goBackText?: string;
  @Input() goBackPath?: string;
  @Input() goBackIcon?: string;
}
