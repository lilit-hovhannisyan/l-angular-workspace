import {Injectable} from '@angular/core';

import {SharedPrintService} from 'upay-lib';
import {UserDataService} from './../dal/user/user-data.service';

@Injectable()
export class PrintService {
  user;

  constructor(  private userDataService: UserDataService,
                private sharedPrintService: SharedPrintService) {
                this.user = this.userDataService.userInfo;
             }

  createInvoice(list) {
    const USER = this.user._value.forename + ' ' + this.user._value.surname;
    this.sharedPrintService.createInvoice(list, USER);
  }
}
