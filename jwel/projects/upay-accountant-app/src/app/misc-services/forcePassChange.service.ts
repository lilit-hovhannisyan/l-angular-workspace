import {CanDeactivate} from '@angular/router';
import {UserDataService } from './../dal/user/user-data.service';
import {Injectable} from '@angular/core';


export interface CanComponentDeactivate {
  canDeactivate: true;
}

@Injectable()
export class ForcePassChangeService implements CanDeactivate <CanComponentDeactivate> {
  constructor(private userDataService: UserDataService) {}

  canDeactivate(component: CanComponentDeactivate) {
    let state;
    this.userDataService.userInfo.subscribe( user => {
      if ( user && user.status === 'pending') {
        state = false;
      } else {
        state = true;
      }
    });

    return state;
  }
}
