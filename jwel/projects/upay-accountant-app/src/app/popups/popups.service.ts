import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class PopupsService {

  public Popups = {
    none: '',
    login: 'login',
    forgot: 'forgot',
  };

  public activePopup = new BehaviorSubject(this.Popups.none);
  public activePopupData: any = null;

  constructor() {}

  private setActivePopup(popupName) {
    this.closeActivePopup();
    this.activePopup.next(popupName);
  }

  public closeActivePopup() {
    this.activePopupData = null;
    this.activePopup.next(this.Popups.none);
  }

  public openPopUp( popupName ) {
    this.setActivePopup(this.Popups[popupName]);
  }

}
