import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';

import { PopupsService } from './popups.service';
import { PopupManagerComponent } from './popup-manager/popup-manager.component';
import { ForgotPopupComponent } from './forgot-password/forgot-popup.component';

@NgModule({
  declarations: [
    PopupManagerComponent,
    ForgotPopupComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    AtomsModules,
    MoleculesModule,
  ],
  exports: [
    PopupManagerComponent,
  ],
  entryComponents: [
  ],
  providers: [
    PopupsService,
  ],
})
export class PopupsModule { }
