import {Component} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {PopupsService} from '../popups.service';

@Component({
  selector: 'app-forgot-popup',
  templateUrl: './forgot-popup.component.html'
})
export class ForgotPopupComponent {

  username: string;

  constructor(private userDataService: UserDataService,
              private popupsService: PopupsService) {

  }

  onUsernameChange(event) {
    this.username = event;
  }

  onSubmit() {
    // this functional is no longer in business logic
    // this.userDataService.forgotPassword(this.username)
    //   .subscribe(data => {
    //     this.siteDataService.setGlobalInfoMessage('we will send your new password by sms');
    //     this.popupsService.openPopUp('login');
    //   });
  }

  openPopup( popup ) {
    this.popupsService.openPopUp( popup );
  }
}
