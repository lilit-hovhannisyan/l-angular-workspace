import {Component, OnInit} from '@angular/core';
import {PopupsService} from '../popups.service';

@Component({
  selector: 'app-popup-manager',
  templateUrl: './popup-manager.component.html',
  styleUrls: ['./popup-manager.component.less']
})
export class PopupManagerComponent implements OnInit {
  activePopupName: string;

  constructor(public popupsService: PopupsService) {
  }

  ngOnInit() {
    this.activePopupName = this.popupsService.Popups.none;
    this.popupsService.activePopup.subscribe(value => {
      this.activePopupName = value;
    });
  }

  closePopup() {
    this.popupsService.closeActivePopup();
  }
}
