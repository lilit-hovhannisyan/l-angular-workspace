// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APIEndpoint: {
    accountantUrl: 'http://10.180.8.79:55503', // dev
    fileStorageUrl: 'http://10.180.8.79:8081'
  }
};
// http://10.180.8.77:55503/upay/accountant/authenticate

