import groovy.json.JsonSlurper

def devInventory = [:]

pipeline {
    agent {label 'Upay-Sso'}
    options {
        skipDefaultCheckout true
    }

    stages {
        stage("Space Cleanup") {
             steps {
                deleteDir()
                cleanWs()
             }
        }

        stage("Checkout") {
            steps {
                checkout scm
                script {
                    def json = sh(returnStdout: true, script: 'cat projects/upay-operator-app/deployment/environment/${upayEnv}/hosts.json')
                    def jsonObject = new groovy.json.JsonSlurper().parseText(json)
                    def servers = []
                    for (int i = 0; i < jsonObject.manager.size(); i++) {
                        servers.add(["host": jsonObject.manager.get(i).host, "user": jsonObject.manager.get(i).user])
                    }
                    devInventory.put("manager", servers)
                }
            }
        }
        stage("Docker cleanup") {
            steps {
                sh "docker container prune -f"
                sh "docker image prune -f"
            }
        }
        stage("Docker build") {
            steps {
                script {
                    sh "docker build -t upay-web --build-arg configuration=${upayEnv} -f projects/upay-operator-app/deployment/Dockerfile ."
                }
            }
        }
        stage("Docker copy") {
            steps {
                sh "docker save -o ${workspace}/upay-web.img upay-web"
                sh "scp -o StrictHostKeyChecking=no ${workspace}/upay-web.img root@10.16.59.21:~/upay-web.img"
                sh "scp -o StrictHostKeyChecking=no ${workspace}/upay-web.img root@10.16.59.22:~/upay-web.img"
            }
        }
        stage("Deploy to environment") {
            steps {
                script {
                    def web = devInventory.get("manager")
                    web.each{ server ->
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.21 'docker stop upay-web || true && docker rm upay-web || true && docker rmi upay-web || true'"
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.22 'docker stop upay-web || true && docker rm upay-web || true && docker rmi upay-web || true'"
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.21 'docker image load -i ~/upay-web.img'"
                        sh "ssh -o StrictHostKeyChecking=no root@10.16.59.22 'docker image load -i ~/upay-web.img'"
                    }
                }
            }
        }
        stage("Docker running image") {
            steps {
                sh "ssh -o StrictHostKeyChecking=no root@10.16.59.21 'docker run -d --name upay-web -p 80:80 upay-web'"
                sh "ssh -o StrictHostKeyChecking=no root@10.16.59.22 'docker run -d --name upay-web -p 80:80 upay-web'"
            }
        }
    }
}
