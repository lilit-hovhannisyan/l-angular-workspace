import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {homeRoutes} from './home-page-routing';
import {HomePageComponent} from './home-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

// lib
import { AtomsModules, MoleculesModule } from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(homeRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule
  ],
  exports: [
    HomePageComponent
  ],
})
export class HomePageModule {
}
