import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';
import {BasketDataService} from '../../dal/basket/basket-data.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: []
})
export class HomePageComponent implements OnInit, OnDestroy {

  isAuthSub = Subscription.EMPTY;
  isForgotForm = false;

  constructor (private userDataService: UserDataService,
               private router: Router,
               private basketDataService: BasketDataService) {}

  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(data => {
      if (data) {
        this.router.navigate(['/operator']);
      }
    });
  }

  ngOnDestroy() {
    this.isAuthSub.unsubscribe();
  }

  setIsForgotForm(isForgot) {
    this.isForgotForm = isForgot;
  }

  onSignIn(event) {
    this.userDataService.signIn(event.username, event.password);
    this.basketDataService.updateCounter();
  }

}
