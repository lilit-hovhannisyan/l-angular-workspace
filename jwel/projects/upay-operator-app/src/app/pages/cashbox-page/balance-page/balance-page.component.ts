import { Component, OnInit } from '@angular/core';
import {UserDataService} from '../../../dal/user/user-data.service';
import {CashboxDataService} from '../../../dal/cashbox/cashbox-data.service';
import {splitNumberWithLumas} from 'upay-lib';

@Component({
  selector: 'app-balance-page',
  templateUrl: './balance-page.component.html',
  styleUrls: []
})
export class BalancePageComponent implements OnInit {
  openingBalance;
  amountIn;
  amountOut;
  commissions;
  cashlessOut;
  currentBalance;

  constructor( private userDataService: UserDataService,
               private cashboxDataService: CashboxDataService) {}

  ngOnInit() {
    let cashboxId;

    this.userDataService.userInfo.subscribe( res => {
      if ( res ) {
        cashboxId = res.cashBoxId;

        this.cashboxDataService.getCashboxBalance(cashboxId);
        this.cashboxDataService.balance.subscribe( data => {
          if ( data ) {
            this.openingBalance = splitNumberWithLumas(data.openingBalance);
            this.amountIn = splitNumberWithLumas(data.amountIn);
            this.amountOut = splitNumberWithLumas(data.amountOut);
            this.commissions = splitNumberWithLumas(data.commissions);
            this.cashlessOut = splitNumberWithLumas(data.cashlessOut);
            this.currentBalance = splitNumberWithLumas(data.currentBalance);
          }
        });
      }
    });
  }
}
