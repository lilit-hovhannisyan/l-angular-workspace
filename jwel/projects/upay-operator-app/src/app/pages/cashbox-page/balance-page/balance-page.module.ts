import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {balancePageRoutes} from './balance-page-routing';
import {BalancePageComponent} from './balance-page.component';

@NgModule({
  declarations: [
    BalancePageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(balancePageRoutes),
    TranslateModule,
  ],
  exports: [
    BalancePageComponent
  ],
})
export class BalancePageModule {
}
