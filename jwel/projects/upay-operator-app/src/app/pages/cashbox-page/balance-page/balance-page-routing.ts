import { Routes } from '@angular/router';
import {BalancePageComponent} from './balance-page.component';

export const balancePageRoutes: Routes = [
  {
    path: '',
    component: BalancePageComponent
  }
];
