import { Component } from '@angular/core';
import { transactionHistoryPageTabs} from '../../dal/site/static-data';

@Component({
  selector: 'app-cashbox-page',
  templateUrl: './cashbox-page.component.html',
  styleUrls: ['./cashbox-page.component.less']
})
export class CashboxPageComponent {
  transactionHistoryPageTabs = transactionHistoryPageTabs;
}
