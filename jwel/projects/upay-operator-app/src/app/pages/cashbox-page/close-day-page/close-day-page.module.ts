import {NgModule} from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {closeDayPageRoutes} from './close-day-page-routing';
import {CloseDayPageComponent} from './close-day-page.component';

import { AtomsModules, MoleculesModule} from 'iu-ui-lib';

@NgModule({
  declarations: [
    CloseDayPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(closeDayPageRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule
  ],
  exports: [
    CloseDayPageComponent
  ],
})
export class CloseDayPageModule {
}
