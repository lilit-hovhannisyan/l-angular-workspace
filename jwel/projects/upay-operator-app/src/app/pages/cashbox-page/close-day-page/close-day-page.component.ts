import { Component, OnInit, OnDestroy } from '@angular/core';
import {UserDataService} from '../../../dal/user/user-data.service';
import {CashboxDataService} from '../../../dal/cashbox/cashbox-data.service';
import {DateTimeService, LoadingService, AlertBoxService, splitNumberWithLumas} from 'upay-lib';
import { UserInfoData } from '../../../dal/user/user-data.models';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-close-day-page',
  templateUrl: './close-day-page.component.html',
  styleUrls: []
})
export class CloseDayPageComponent implements OnInit, OnDestroy {
  currentUser: UserInfoData;
  userSub = Subscription.EMPTY;
  date;
  currentBalance;

  constructor (private userDataService: UserDataService,
               private dateTimeService: DateTimeService,
               private cashboxDataService: CashboxDataService,
               private loadingService: LoadingService,
               private alertBoxService: AlertBoxService) {}

  ngOnInit() {
    let cashboxId;

    this.userSub = this.userDataService.userInfo.subscribe( res => {
      if ( res ) {
        this.currentUser = res;
        cashboxId = res.cashBoxId;

        this.cashboxDataService.getCashboxBalance(cashboxId);
        this.cashboxDataService.balance.subscribe( data => {
          if ( data ) {
            this.currentBalance = splitNumberWithLumas(data.currentBalance);
          }
        });
      }
    });

    this.date = this.dateTimeService.convertDate( new Date(), '/');
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  print() {
  }

  sendReport() {
    this.loadingService.showLoader(true);
    this.cashboxDataService.generateCashboxReport().subscribe(res => {
      this.loadingService.showLoader(false);
      this.alertBoxService.initMsg({type: 'success', text: 'Հաշվետվությունը հաջողությամբ ուղարկված է'});
    });
  }
}
