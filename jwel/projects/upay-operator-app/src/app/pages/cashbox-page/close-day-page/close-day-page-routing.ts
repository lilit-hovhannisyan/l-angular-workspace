import {Routes} from '@angular/router';
import {CloseDayPageComponent} from './close-day-page.component';

export const closeDayPageRoutes: Routes = [
  {
    path: '',
    component: CloseDayPageComponent
  }
];
