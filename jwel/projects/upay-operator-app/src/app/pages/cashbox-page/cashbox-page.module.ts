import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';


import {WidgetsModule} from '../../widgets/widgets.module';
import {SharedWidgetsModule} from 'upay-lib';

import {CashboxPageComponent} from './cashbox-page.component';
import {cashboxPageRoutes} from './cashbox-page-routing';


@NgModule({
  declarations: [
    CashboxPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(cashboxPageRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    CashboxPageComponent
  ],
})
export class CashboxPageModule {
}
