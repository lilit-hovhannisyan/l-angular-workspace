import { Routes } from '@angular/router';
import {CashboxPageComponent} from './cashbox-page.component';


export const cashboxPageRoutes: Routes = [
  {
    path: '',
    component: CashboxPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'balance'
      },
      {
        path: 'balance',
        loadChildren: './balance-page/balance-page.module#BalancePageModule'
      },
      {
        path: 'close-day',
        loadChildren: './close-day-page/close-day-page.module#CloseDayPageModule'
      },
    ]
  }
];
