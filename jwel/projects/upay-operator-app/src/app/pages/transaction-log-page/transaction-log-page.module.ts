import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {transactionLogPageRoutes} from './transaction-log-page-routing';
import {TransactionLogPageComponent} from './transaction-log-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    TransactionLogPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(transactionLogPageRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    TransactionLogPageComponent
  ],
})
export class TransactionLogPageModule {
}
