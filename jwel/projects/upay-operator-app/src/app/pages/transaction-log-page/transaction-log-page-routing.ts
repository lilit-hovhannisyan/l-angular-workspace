import {Routes} from '@angular/router';
import {TransactionLogPageComponent} from './transaction-log-page.component';

export const transactionLogPageRoutes: Routes = [
  {
    path: '',
    component: TransactionLogPageComponent
  }
];
