import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {BasketPageComponent} from './basket-page.component';
import {basketPageRoutes} from './basket-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';
import {SharedWidgetsModule} from 'upay-lib';


@NgModule({
  declarations: [
    BasketPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(basketPageRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    BasketPageComponent
  ],
})
export class BasketPageModule {
}
