import {Routes} from '@angular/router';
import {BasketPageComponent} from './basket-page.component';

export const basketPageRoutes: Routes = [
  {
    path: '',
    component: BasketPageComponent
  }
];
