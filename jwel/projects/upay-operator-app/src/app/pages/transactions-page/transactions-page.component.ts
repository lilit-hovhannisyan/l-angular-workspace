import { Component } from '@angular/core';
import {transactionPageTabs} from '../../dal/site/static-data';

@Component({
  selector: 'app-transactions-page',
  templateUrl: './transactions-page.component.html'
})
export class TransactionsPageComponent {
  transactionPageTabs = transactionPageTabs;
}
