import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {TransactionsPageComponent} from './transactions-page.component';
import {transactionsPageRoutes} from './transactions-page-routing';
import {WidgetsModule} from '../../widgets/widgets.module';
import {SharedWidgetsModule} from 'upay-lib';



@NgModule({
  declarations: [
    TransactionsPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(transactionsPageRoutes),
    TranslateModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    TransactionsPageComponent
  ],
})
export class TransactionsPageModule {
}
