import {Routes } from '@angular/router';
import {EncashmentPageComponent} from './encashment-page.component';

import {EncashmentListByGroupComponent} from '../../../widgets/encashment/encashment-list-by-group/encashment-list-by-group.component';
import {EncashmentTypeComponent} from '../../../widgets/encashment/encashment-type/encashment-type.component';
import {CashoutComponent} from '../../../widgets/cashout/cashout.component';

export const encashmentPageRoutes: Routes = [
  {
    path: '',
    component: EncashmentPageComponent,
    children: [
      {
        path: ':category',
        component: EncashmentListByGroupComponent
      },
      {
        path: ':category/:type',
        component: EncashmentTypeComponent
      }
    ]
  }
];
