import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {EncashmentPageComponent} from './encashment-page.component';
import {encashmentPageRoutes} from './encashment-page-routing';
import {WidgetsModule} from '../../../widgets/widgets.module';
import { AtomsModules, MoleculesModule, OrganismsModule } from 'iu-ui-lib';

@NgModule({
  declarations: [
    EncashmentPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(encashmentPageRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule
  ],
  exports: [
    EncashmentPageComponent
  ],
})
export class EncashmentPageModule {
}
