import {Routes} from '@angular/router';
import {GroupsPageComponent} from './groups-page.component';

export const groupsPageRoutes: Routes = [
  {
    path: '',
    component: GroupsPageComponent
  }
];
