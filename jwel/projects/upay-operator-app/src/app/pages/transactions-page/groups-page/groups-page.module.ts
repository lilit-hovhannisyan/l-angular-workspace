import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {groupsPageRoutes} from './groups-page-routing';
import {GroupsPageComponent} from './groups-page.component';
import {WidgetsModule} from '../../../widgets/widgets.module';

@NgModule({
  declarations: [
    GroupsPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(groupsPageRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    GroupsPageComponent
  ],
})
export class GroupsPageModule {
}
