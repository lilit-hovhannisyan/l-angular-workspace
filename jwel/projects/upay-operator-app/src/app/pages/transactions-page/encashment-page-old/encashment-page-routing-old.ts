import {Routes } from '@angular/router';
import {
  EncashmentSelectDataComponent
} from '../../../widgets/encashment-select-data/encashment-select-data.component';
import {EncashmentPageOldComponent} from './encashment-page-old.component';

export const encashmentPageRoutes: Routes = [
  {
    path: '',
    component: EncashmentPageOldComponent,
    children: [
      {
        path: ':type',
        component: EncashmentSelectDataComponent
      }
    ]
  }
];
