import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {EncashmentPageOldComponent} from './encashment-page-old.component';
import {encashmentPageRoutes} from './encashment-page-routing-old';
import {WidgetsModule} from '../../../widgets/widgets.module';
import { AtomsModules, MoleculesModule, OrganismsModule } from 'iu-ui-lib';

@NgModule({
  declarations: [
    EncashmentPageOldComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(encashmentPageRoutes),
    TranslateModule,
    WidgetsModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule
  ],
  exports: [
    EncashmentPageOldComponent
  ],
})
export class EncashmentPageOldModule {
}
