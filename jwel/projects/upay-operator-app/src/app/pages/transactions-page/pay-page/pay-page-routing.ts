import {Routes} from '@angular/router';
import {PayPageComponent} from './pay-page.component';
import {OperatorProviderListComponent} from './operator-provider-list/operator-provider-list.component';
import {OperatorPayTerminalComponent} from './operator-pay-terminal/operator-pay-terminal.component';

export const payPageRoutes: Routes = [
  {
    path: '',
    component: PayPageComponent,
    children: [
      {
        path: ':labelId',
        component: OperatorProviderListComponent
      },
      {
        path: ':labelId/:providerId',
        component: OperatorPayTerminalComponent
      }
    ]
  }
];
