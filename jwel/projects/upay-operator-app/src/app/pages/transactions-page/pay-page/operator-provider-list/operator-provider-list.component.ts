import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-operator-provider-list',
  templateUrl: 'operator-provider-list.component.html',
  styleUrls: []
})

export class OperatorProviderListComponent implements OnInit, OnDestroy {

  labelId: string;
  activeRouteSub = Subscription.EMPTY;

  constructor(private activeRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activeRouteSub = this.activeRoute.params.subscribe(routeParams => {
      this.labelId = routeParams['labelId'];
    });
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
  }
}
