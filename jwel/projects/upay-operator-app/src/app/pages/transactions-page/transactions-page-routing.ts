import {Routes} from '@angular/router';
import {TransactionsPageComponent} from './transactions-page.component';

export const transactionsPageRoutes: Routes = [
  {
    path: '',
    component: TransactionsPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'terminal'
      },
      {
        path: 'terminal',
        loadChildren: './pay-page/pay-page.module#PayPageModule',
      },
      {
        path: 'encashment',
        loadChildren: './encashment-page/encashment-page.module#EncashmentPageModule',
      },
      {
        path: 'encashment-old',
        loadChildren: './encashment-page-old/encashment-page-old.module#EncashmentPageOldModule',
      },
      {
        path: 'groups',
        loadChildren: './groups-page/groups-page.module#GroupsPageModule',
      }
    ]
  }
];
