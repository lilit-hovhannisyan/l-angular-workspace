import { Routes } from '@angular/router';
import {ProfilePageComponent} from './profile-page.component';

export const clientProfilePageRoutes: Routes = [
  {
    path: '',
    component: ProfilePageComponent
  }
];
