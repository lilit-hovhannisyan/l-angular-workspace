import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ProfilePageComponent} from './profile-page.component';
import {clientProfilePageRoutes} from './profile-page-routing';

import {AtomsModules} from 'iu-ui-lib';


@NgModule({
  declarations: [
    ProfilePageComponent
  ],
  imports: [
    FormsModule,
    RouterModule,
    RouterModule.forChild(clientProfilePageRoutes),
    TranslateModule,
    AtomsModules
  ],
  exports: [
    ProfilePageComponent
  ],
})
export class ProfilePageModule {
}
