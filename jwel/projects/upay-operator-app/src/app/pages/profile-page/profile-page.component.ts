import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertBoxService } from 'upay-lib';
import { UserDataService } from '../../dal/user/user-data.service';
import { UserInfoData } from '../../dal/user/user-data.models';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.less']
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  currentUser: UserInfoData;
  userSub = Subscription.EMPTY;

  oldPass: string;
  newPass: string;
  confirmPass: string;

  constructor(private userDataService: UserDataService, private alertBoxService: AlertBoxService, private router: Router ) {}

  ngOnInit() {
    this.userSub = this.userDataService.userInfo.subscribe(d => {
      this.currentUser = d;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  onResetPassword() {
    if ( this.oldPass && this.newPass && this.confirmPass ) {
      if ( this.newPass.length < 8 ) {
        this.alertBoxService.initMsg({type: 'error', text: 'Գաղտնաբառը պետք է լինի առնվազն 8 նիշ'});
        return;
      }

      if ( this.newPass === this.confirmPass ) {
        this.userDataService.resetPassword(this.oldPass, this.newPass, this.currentUser.email).subscribe(d => {
          this.oldPass = '';
          this.newPass = '';
          this.confirmPass = '';
          this.alertBoxService.initMsg({type: 'success', text: 'Գաղտնաբառը հաջողությամբ փոխված է'});
          this.router.navigate(['/']);

          // update profile subscription
          this.userDataService.getProfile();
        });
      } else {
        this.alertBoxService.initMsg ( {type: 'error', text: 'Գաղտնաբառերը չեն համընկնում'} );
      }
    } else {
      this.alertBoxService.initMsg ( {type: 'error', text: 'Բոլոր դաշտերը պարտադիր են'} );
    }
  }
}
