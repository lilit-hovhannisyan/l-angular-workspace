import {Component, OnInit} from '@angular/core';
import {supportPageTabs} from '../../dal/site/static-data';
import {ClaimsService} from '../../dal/claims/claims.service';

@Component({
  selector: 'app-support',
  templateUrl: './support-page.component.html',
  styleUrls: []
})


export class SupportPageComponent implements OnInit {
  supportTabs;

  constructor(private claimsService: ClaimsService) {
  }

  ngOnInit() {
    this.claimsService.getNumberOfClaimsToComplete().subscribe( numberOfClaimsToComplete => {
      this.supportTabs = supportPageTabs;

      if (numberOfClaimsToComplete > 0) {
        this.supportTabs.map( tab => {
          if ( tab.link === 'claims/list') {
            tab.notification = numberOfClaimsToComplete;
          }
        });
      }
    });
  }
}
