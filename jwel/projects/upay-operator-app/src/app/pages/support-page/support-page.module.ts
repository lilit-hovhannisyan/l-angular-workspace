import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SupportPageComponent} from './support-page.component';
import {supportPageRoutes} from './support-page.routing';

import {WidgetsModule} from '../../widgets/widgets.module';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    SupportPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(supportPageRoutes),
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    SupportPageComponent
  ]
})

export class SupportPageModule {

}
