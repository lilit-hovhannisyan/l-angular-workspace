import {Routes} from '@angular/router';
import {ClaimListPageComponent} from './claim-list-page.component';

export const claimListRoutes: Routes = [
  {
    path: '',
    component: ClaimListPageComponent
  }
];
