import {Component, Inject, LOCALE_ID} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ClaimsService} from '../../../dal/claims/claims.service';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';
import {root} from '../../../dal/site/static-data';
import {CancellationClaimType} from '../../../../../../upay-lib/src/lib/api';

@Component({
  selector: 'app-claim-list',
  templateUrl: './claim-list-page.component.html',
  styleUrls: ['./claim-list-page.component.less']
})

export class ClaimListPageComponent {
  columnsConfig = [
    {

      displayValueKey: 'providerName',
      getDisplayValue: (displayValueKey) => {
        return this.translateService.instant(`providers.${displayValueKey}`);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.transaction'),
      type: 'data',
      sortKey: 'providerName'
    },
    {
      displayValueKey: 'userNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.user_id'),
      type: 'data',
      sortKey: 'userNum'
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      sortKey: 'receiptId'
    },
    {
      getCellValue: (rowData, columnData) => {
        return (rowData.amount / 100) + ' AMD';
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      customClass: () => 'amount',
      type: 'data',
      sortKey: 'amount',
    },
    {
      displayValueKey: 'customerPhone',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.customerPhone'),
      getDisplayValue: (displayValueKey) => {
        if (!displayValueKey) {
          return '---';
        } else {
          return displayValueKey;
        }
      },
      type: 'data',
      // sortKey: 'amount',
    },
    // {
    //   displayValueKey: 'branchName',
    //   displayName: () => this.translateService.instant('widgets.transaction_list.header.branchName'),
    //   type: 'data',
    //   sortKey: 'branchName'
    // },
    {
      displayValueKey: 'completed',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.dateTransaction'),
      customClass: () => 'bold_text',
      type: 'data',
      sortKey: 'completed',
      getDisplayValue: (date) => {
        return formatDate(date, 'dd/MM/yyyy', this.locale);
      }
    },
    {
      displayValueKey: 'claimOpened',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.dateCreated'),
      customClass: () => 'bold_text',
      type: 'data',
      sortKey: 'claimOpened',
      getDisplayValue: (date) => {
        if (date) {
          return formatDate(date, 'dd/MM/yyyy', this.locale);
        } else {
          return '---';
        }
      }
    },
    {
      // displayValueKey: 'cancellationClaimType',
      getCellValue: (rowData, columnData) => {
        const key = 'pages.support.applicationBy' + rowData.cancellationClaimType;
        return this.translateService.instant(key);
      },
      displayName: () => this.translateService.instant('widgets.transaction_list.header.cancellationClaimType'),
      customClass: () => 'bold_text',
      type: 'data',
      sortKey: 'cancellationClaimType',
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.status'),
      getCellValue: (rowData, columnData) => {
        if (rowData.status === 'DISMISSED') {
          return  this.translateService.instant('widgets.claim.notAccepted');
        } else if (rowData.status === 'OPENED') {
          return  this.translateService.instant('widgets.claim.newClaim');
        }
        return  this.translateService.instant('widgets.claim.accepted');
      },
      type: 'data',
      customClass: [
        {className: 'opened not bold_text', conditionKey: 'status', conditionValue: ['DISMISSED']},
        {className: 'opened new bold_text', conditionKey: 'status', conditionValue: ['OPENED']},
        {className: 'opened bold_text', conditionKey: 'status', conditionValue: ['PRE_APPROVED', 'APPROVED', 'ACCEPTED', 'REJECTED', 'PRE_REJECTED', 'COMPLETED_REJECTED', 'COMPLETED_APPROVED']}
      ],
      sortKey: 'status',
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.null'),
      type: 'action',
      dispatchActionName: 'details',
      actionIcon: 'icon_show',
      // customClass: [{className: 'no-event', conditionKey: 'status', conditionValue: ['OPENED', 'COMPLETED_APPROVED']}],
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.state'),
      getCellValue: (rowData, columnData) => {
        if (rowData.status === 'DISMISSED') {
          return rowData.examinationNotes ?  rowData.examinationNotes : '---';
        }
        return  this.translateService.instant(`widgets.claim.${rowData.status }`);
      },
      type: 'data',
      customClass: [
        {className: 'state_warning', conditionKey: 'status', conditionValue: ['ACCEPTED', 'OPENED']},
        {className: 'state_error', conditionKey: 'status', conditionValue: ['REJECTED', 'PRE_REJECTED']},
        {className: 'state_error rejected_completed', conditionKey: 'status', conditionValue: ['COMPLETED_REJECTED']},
        {className: 'state_success', conditionKey: 'status', conditionValue: ['PRE_APPROVED', 'APPROVED']},
        {className: 'state_success success_completed', conditionKey: 'status', conditionValue: ['COMPLETED_APPROVED']}
      ],
      sortKey: 'status',
    },
  ];

  dataSource: any = [];
  totalItems = 0;
  orderedFieldsArr = [{key: 'claimOpened', val: false}];

  activeFilters = ['receiptId', 'branchId', 'providerName', 'userNum'];

  constructor(private translateService: TranslateService,
              private claimsService: ClaimsService,
              private router: Router,
              @Inject(LOCALE_ID) private locale: string) {
  }

  getPendingClaims(queryData) {
    this.claimsService.getFilteredClaims(queryData).subscribe( res => {
      this.dataSource = res.items;
      this.totalItems = res.total;
    });
  }

  onRowActionClick(event) {
    if (event.actionName === 'details') {
      const claimId = event.row.id;

      if ( event.row.cancellationClaimType === 'OPERATOR') {
        this.router.navigate([`/${root}/support/claim/operator-claim-details/${claimId}`]);
        return;
      }

      this.router.navigate([`/${root}/support/claim/customer-claim-details/${claimId}`]);
    }
  }
}
