import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {SharedWidgetsModule} from 'upay-lib';

import {ClaimListPageComponent} from './claim-list-page.component';
import {claimListRoutes} from './claim-list-page.routing';



@NgModule({
  declarations: [
    ClaimListPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimListRoutes),
    SharedWidgetsModule
  ],
  exports: [
    ClaimListPageComponent
  ]
})

export class ClaimListPageModule {
}
