import {Component, OnDestroy, OnInit} from '@angular/core';
import {SupportService} from '../../../../../dal/support/support.service';
import {
  AlertBoxService,
  CardHelperService,
  DocumentType,
  LoadingService,
  SharedPrintService
} from 'upay-lib';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {FormConfig} from '../../../claim-page/claim-by-user-page/claim-by-user-page.component';
import {environment} from '../../../../../../environments/environment';
import { v4 as uuid } from 'uuid';
import {
  identificationApplicationFormHtml
} from '../../../../../dal/support/identification-application-form.service';
import {SiteDataService} from '../../../../../dal/site/site-data.service';
import {dateFormat} from 'upay-lib';

@Component({
  selector: 'app-user-identification-page',
  templateUrl: './user-identification-page.component.html',
  styleUrls: ['./user-identification-page.component.less']
})

export class UserIdentificationPageComponent implements OnInit, OnDestroy {
  customerId;
  cards = [];

  // radiobtn
  docOptions = [
    {
      key: DocumentType.PASSPORT,
      displayText: 'pages.support.passport',
      selected: true
    },
    {
      key: DocumentType.IDCARD,
      displayText: 'pages.support.idCard',
      selected: false
    }
  ];

  // form
  identificationDataSource;
  identificationData = {
    customerId: null,
    docType: DocumentType.PASSPORT,
    media: '',
  };

  identificationFormDataConfig: FormConfig[] = [
    {
      groupName: 'group1',
      fields: [
        {
          key: 'nameAm',
          component: 'input',
          label: 'pages.support.nameAm',
          placeholder: 'Հայատառ 3+ նիշ',
          required: true,
          regExp: '^[ա-ֆԱ-Ֆ]{2,}[ա-ֆԱ-Ֆ]$'
        },
        {
          key: 'surnameAm',
          component: 'input',
          label: 'pages.support.surnameAm',
          placeholder: 'Հայատառ 3+ նիշ',
          required: true,
          regExp: '^[ա-ֆԱ-Ֆ]{2,}[ա-ֆԱ-Ֆ]$'
        },
        {
          key: 'middleNameAm',
          component: 'input',
          label: 'pages.support.middleNameAm',
          placeholder: 'Հայատառ 3+ նիշ',
          required: true,
          regExp: '^[ա-ֆԱ-Ֆ]{2,}[ա-ֆԱ-Ֆ]$'
        },
        {
          key: 'nameEn',
          component: 'input',
          label: 'pages.support.nameEn',
          placeholder: 'Լատինատառ 3+ նիշ',
          required: true,
          regExp: '^[a-zA-Z]{2,}[a-zA-Z]$'
        },
        {
          key: 'surnameEn',
          component: 'input',
          label: 'pages.support.surnameEn',
          placeholder: 'Լատինատառ 3+ նիշ',
          required: true,
          regExp: '^[a-zA-Z]{2,}[a-zA-Z]$'
        },
        {
          key: 'sex',
          component: 'select',
          label: 'pages.support.sex',
          outputFieldKey: 'id',
          displayFieldKey: 'label',
          options: [
            {
              id: 'Male',
              label: 'Արական'
            },
            {
              id: 'Female',
              label: 'Իգական'
            }
          ],
          required: true
        },
        {
          key: 'citizen',
          component: 'input',
          label: 'pages.support.citizen',
          required: true
        },
      ]
    },
    {
      groupName: 'group2',
      fields: [
        {
          key: 'documentNumber',
          component: 'input',
          label: 'Անձնագրի սերիա կամ ID համար',
          // documentType: DocumentType.PASSPORT,
          required: true
        },
        {
          key: 'dateOfBirth',
          component: 'datePicker',
          label: 'pages.support.dateOfBirth',
          required: true,
        },
        {
          key: 'dateOfIssue',
          component: 'datePicker',
          label: 'Տրման ժամկետ',
          required: true
        },
        {
          key: 'dateOfExpiry',
          component: 'datePicker',
          label: 'Վավերականության ժամկետ',
          required: true
        },
        {
          key: 'authority',
          component: 'input',
          label: 'Ում կողմից',
          required: true
        }
      ]
    },
    {
      groupName: 'group3',
      fields: [
        {
          key: 'country',
          component: 'select',
          label: 'pages.support.country',
          outputFieldKey: 'id',
          displayFieldKey: 'name',
          getOptions: () => this.siteDataService.getCountries(), // '/upay/operator/get-countries',
          required: true
        },
        {
          key: 'city',
          component: 'select',
          label: 'pages.support.city',
          outputFieldKey: 'id',
          displayFieldKey: 'name',
          getOptions: (data) => this.siteDataService.getCities(data),
          dependence: 'country',
          noEvent: true,
          required: true
        },
        {
          key: 'address',
          component: 'input',
          label: 'pages.support.address',
          placeholder: 'Հայատառ մեծատառ 3+ նիշ',
          required: true,
          regExp: '^[ա-ֆԱ-Ֆ, և, \., ․, \\d, /, \\- \s]{2,}[ա-ֆԱ-Ֆ, և, \., ․, \\d, /, \\- \s]$'
        },
        {
          key: 'socCardNumber',
          component: 'input',
          label: 'pages.support.socCardNumber',
          required: false,
          regExp: '.*'
        }
      ]
    }
  ];
  formData: any = {};

  // attached files
  _fileStorageUrl = environment.APIEndpoint.fileStorageUrl;
  objectKeys = Object.keys;
  media = {
    application: {
      id: '',
      accept: '.png,.jpg,.jpeg',
      docs: [],
    },
    passport: {
      id: '',
      accept: 'image/x-png,image/jpg,image/jpeg',
      docs: []
    },
    idCard: {
      id: '',
      accept: 'image/x-png,image/jpg,image/jpeg',
      docs: []
    },
    socCard: {
      id: '',
      accept: 'image/x-png,image/jpg,image/jpeg',
      docs: []
    }
  };


  currentUserSUB = Subscription.EMPTY;

  constructor(private supportService: SupportService,
              private alertBoxService: AlertBoxService,
              private sharedPrintService: SharedPrintService,
              private cardHelperService: CardHelperService,
              private loadingService: LoadingService,
              private siteDataService: SiteDataService) {}

  ngOnInit() {
    this.currentUserSUB = this.supportService.currentUser.subscribe( (user: any) => {
      if (Object.keys(user).length !== 0) {
        this.customerId = user.number;
        this.identificationData.customerId = this.customerId;

        // cards
        this.supportService.getBankingCards(this.customerId).subscribe( cards => {
          // bank: "HSBC-ARMENIA"
          // boundAt: 1559647784904
          // createdAt: 1559647590185
          // expiration: "202203"
          // holder: "MR ASHOT PETROSYAN"
          // id: "bdea2ad4-3ab1-4f84-baf4-ecb1426ad3b4"
          // number: "557115**6787"
          // state: "ACTIVE"
          this.cards = [...cards];
        });

        // identification data init
        this.supportService.getIdentifiedUser(this.customerId).subscribe( identificationData => {
          this.identificationDataSource = identificationData;

          if (this.identificationDataSource.length) {
            this.identificationData.docType = this.identificationDataSource[0].docType;
            this.initRadioBtnAndRelatedData(this.identificationData.docType);
          }
        });
      }
    });
  }

  getSelectedDocTypeDataSource () {
    const selectedData = this.identificationDataSource.find(d => {
      return d.docType === this.identificationData.docType;
    });
    return selectedData;
  }

  initIdentificationData(data) {
    if (!data) {
      this.formData = {};
      return;
    }
    this.formData = data;

    console.log(this.formData);
    console.log(this.identificationData);
    this.initMedia(data['media']);
  }

  ngOnDestroy() {
    this.currentUserSUB.unsubscribe();
  }

  initMedia(identifiedUserMadiaStr) {
    this.media.application.docs = [];
    this.media.passport.docs = [];
    this.media.idCard.docs = [];
    this.media.socCard.docs = [];

    const madiaArr = identifiedUserMadiaStr.split(',');
    madiaArr.map( groupName => {
      const id = groupName.substring(0, groupName.indexOf('__'));
      const type = groupName.substring(groupName.indexOf('__') + 2);

      // todo WTF es inch e
      if ( this.media[type]) {
        this.media[type]['id'] = groupName; // id;
      }

      this.supportService.getFilesInGroup(groupName).subscribe( media => {
        media.files.map ( m => {
          this.media[type]['docs'].push({name: m.name, url: m.url});
        });
      }, err => {
        console.log('media errror:', err);
      });
    });
  }

  // radio btn
  initRadioBtnAndRelatedData(currentRadioBtnKey) {
    this.docOptions.map( docOption => {
      docOption.selected = false;

      if (currentRadioBtnKey === docOption.key) {
        docOption.selected = true;
        this.identificationData.docType = docOption.key;
        this.initIdentificationData(this.getSelectedDocTypeDataSource());
      }
    });
  }
  onSelectDocType(event) {
    this.initRadioBtnAndRelatedData(event.key);
  }

  // selectBoxes
  setSelected(field) {
    if (this.formData[field.key]) {
      delete field.noEvent;
    }
  }

  // print application
  printApplication() {
    const hasError = this.validateForm();
    // check up form validity
    if ( hasError) {
      this.alertBoxService.initMsg({type: 'error', text: 'Fields are required for app form print'});
      return;
    }

    this.collectIdentificationData();
    const html = identificationApplicationFormHtml(this.identificationData);
    this.sharedPrintService.openPrintWindow(html, 'a4');
  }

  // form && validation
  getFormFieldValue(key) {
    // let returnFieldValue;
    // // this.identificationFormDataConfig.map(group => {
    // //   group.fields.map(field => {
    // //      if (field.key === key) {
    // //        returnFieldValue = field.value;
    // //      }
    // //   });
    // // });

    return this.formData[key];
  }

  resolveDependanciesForSelectboxes(field) {
    this.identificationFormDataConfig.map( group => {
      group.fields.map( dependantField => {
        if ((dependantField.dependence === field.key)) {
          dependantField.noEvent = false;
        }
      });
    });
  }

  changeFormValue(event, field) {
    if (!event) {
      return;
    }
    if (field.component === 'select') {
      this.resolveDependanciesForSelectboxes(field);
    }

    field.hasErr = false;
    this.formData[field.key] = event;

    if (field.component === 'datePicker') {
      this.formData[field.key] = dateFormat(event, 'mm-dd-yyyy');
    }

    field.hasErr =  ( field.required && !!this.formData[field.key]) ? false : true;

    if (field.regExp) {
      const pattern = new RegExp(field.regExp, 'g');
      field.hasErr =  pattern.test(event) ? false : true;
    }


    // this.identificationFormDataConfig.map(group => {
    //   group.fields.map(field => {
    //     if (field.key === key) {
    //       // todo shat vat e sarqac aatan chpetq e pahel configuration modeli mej pti pahel arancin form data objecti mej konqret lomac e
    //       if (field.component === 'select') {
    //         field.value = event;
    //         field.hasErr = false;
    //         this.resolveDependanciesForSelectboxes(field);
    //       } else if (field.component === 'datePicker') {
    //         if (event) {
    //           field.value = dateFormat(event, 'mm-dd-yyyy');
    //         }
    //         field.hasErr = false;
    //       } else {
    //
    //         field.value = event.val.toUpperCase();
    //
    //         field.hasErr = false;
    //
    //         if (!event.isValid) {
    //           field.hasErr = true;
    //         }
    //       }
    //       return;
    //     }
    //   });
    // });
  }

  validateForm() {
    const arr = [];
    let hasErr = false;

    this.identificationFormDataConfig.map(group => {
      group.fields.map(field => {
        if (field.required && !this.formData[field.key]) {
          field.hasErr = true;
        }
        if (field.hasErr) {
          hasErr = true;
        }
      });
    });

    return hasErr;
  }

  openExtendedData(key) {
    this.media[key]['visible'] = !this.media[key]['visible'];
  }

  getCardType(cardNumber) {
    return this.cardHelperService.getCardType(cardNumber);
  }

  // attach file
  attachFile(e, type) {
    if (this.media[type].docs.length === 2 ) {
      this.alertBoxService.initMsg({type: 'error', text: 'Դուք չեք կարող ավելացնել 2-ից ավել ֆայլ:'});
      return;
    }

    const files = e;
    let mediaCount = files.length;

    if (mediaCount - this.media[type].docs.length > 2 ) {
      this.alertBoxService.initMsg({type: 'error', text: 'Դուք չեք կարող ավելացնել 2-ից ավել ֆայլ:'});
      return;
    }

    for ( const file of files)  {
      const fileName = file.name;

      // setup uuid for exact media type if not created
      let _groupName = this.media[type]['id'];

      if ( !this.media[type]['id'] ) {
        _groupName = this.media[type]['id'] = uuid() + '__' + type;
      }

      this.supportService.createGroup(_groupName).subscribe( group => {
        this.supportService.fileUpload(_groupName, fileName, file).subscribe( res => {
          const obj: any = {};

          console.log('file upload', res);

          obj['name'] = fileName;
          obj['path'] = res.url;
          this.media[type]['visible'] = true;
          this.media[type]['docs'].push(obj);
          mediaCount--;

          // when all files are uploaded identify user
          if ( mediaCount === 0) {
            this.alertBoxService.initMsg({type: 'success', text: 'Ֆայլերը հաջողությամբ  վերբեռնված են:'});
          }
        });
      });
    }
  }

  // cta : call to action btn's
  identifyUser() {
    const hasError = this.validateForm();
    if ( hasError) {
      return;
    }

    // require the media: application && (passport || idCard)
    if ( !this.media['application'].docs.length ) {
      this.alertBoxService.initMsg({type: 'error', text: 'Դիմումը պետք է պարտադիր կցված լինի'});
      return;
    }

    if ( !this.media['passport'].docs.length && !this.media['idCard'].docs.length) {
      this.alertBoxService.initMsg({type: 'error', text: 'Անձնագիրը կամ ID քարտը պետք է պարտադիր կցված լինի'});
      return;
    }


    this.loadingService.showLoader(true);
    this.collectIdentificationData();

    this.supportService.identifyUser(this.identificationData).subscribe( res => {
      this.loadingService.showLoader(false);
      this.alertBoxService.initMsg({type: 'success', text: 'Դուք նույնականացված եք:'});

      // update user
      this.supportService.updateUser(this.customerId);
    });
  }

  // update user in parental route user-info-page
  // updateStatus() {
  //   this.supportService.changeUserStatus();
  //   this.currentUserSUB = this.supportService.currentUser.subscribe( user => {
  //     this.supportService.updateUser(user);
  //     // this.currentUserSUB.unsubscribe()
  //   });
  // }

  collectIdentificationData() {
    // generate media string for future rendering
    console.log(this.media);
    // debugger
    let mediaStr = '';
    for ( const mediaKey of Object.keys(this.media) ) {
      let spliter = ',';
      if ( !mediaStr ) {
        spliter = '';
      }
      if (this.media[mediaKey]['id'] !== '' ) {
        mediaStr = mediaStr + spliter + this.media[mediaKey]['id'];
      }
    }
    this.identificationData['media'] = mediaStr;

    // get form data
    this.identificationData = {
      ...this.identificationData,
      ...this.formData
    };
  }

  sendForReview(user) {
    this.supportService.sendForReview(user);
  }
}
