import {Routes} from '@angular/router';
import {UserHistoryPageComponent} from './user-history-page.component';

export const userHistoryRoutes: Routes = [
  {
    path: '',
    component: UserHistoryPageComponent
  }
];
