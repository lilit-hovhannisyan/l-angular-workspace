import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {userIdentificationPageRoutes} from './user-identification-page.routing';
import {UserIdentificationPageComponent} from './user-identification-page.component';
import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    UserIdentificationPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userIdentificationPageRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule
  ],
  providers: [],
  exports: [
    UserIdentificationPageComponent
  ]
})

export class UserIdentificationPageModule {}
