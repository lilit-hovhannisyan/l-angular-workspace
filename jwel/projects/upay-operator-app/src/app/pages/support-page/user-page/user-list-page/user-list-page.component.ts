import {Component} from '@angular/core';
import {supportOptions} from '../../../../widgets/user-filter/user-filter.component';
import {AlertBoxService, LoadingService, Page} from 'upay-lib';
import {SupportService} from '../../../../dal/support/support.service';
import {Router} from '@angular/router';
import {root} from '../../../../dal/site/static-data';
import {SupportCustomer} from '../../../../widgets/customer-card/customer-card.component';

@Component({
  selector: 'app-user-list-page',
  templateUrl: './user-list-page.component.html'
})

export class UserListPageComponent {
  // filter
  options = supportOptions;
  searchCriteria: any = {};

  // user list
  customers = null;

  // user info
  totalItems = 0;
  pageSize = 10;
  pageIndex = 0;

  constructor(private supportService: SupportService,
              private loadingService: LoadingService,
              private router: Router,
              private alertBoxService: AlertBoxService) {}

  // search
  getUserList() {
    const page = new Page();
    page.size = this.pageSize;
    page.index = this.pageIndex;
    this.searchCriteria.page = page;

    this.loadingService.showLoader(true);
    this.customers = [];

    this.supportService.getUsers(this.searchCriteria).subscribe( res => {
      this.loadingService.showLoader(false);

      if (!res.items.length) {
        this.totalItems = 0;
        this.alertBoxService.initMsg({type: 'error', text: 'error.noItemFound'});
        return;
      }

      res.items.map( user => {
        const frozenStatus = user.accFrozen;
        const deleted = user.deleted;
        let label = {
          displayText: 'Active',
          customClass: 'active'
        };
        if (frozenStatus) {
          label = {
            displayText: 'Frozen',
            customClass: 'frozen'
          };
        }
        if (deleted) {
          label = {
            displayText: 'Deleted',
            customClass: 'frozen'
          };
        }

        const customerData = new SupportCustomer(user);
        this.customers.push({
          label: label,
          status: [],
          info: customerData,
          actions: []
        });
      });
      this.totalItems = res.total;
      // debugger
    });
  }

  onSearch(event) {
    this.searchCriteria = {};
    this.searchCriteria[event.searchKey] = event.searchVal;

    this.getUserList();
  }

  pageChange(event) {
    this.pageIndex = event - 1;
    this.getUserList();
  }

  //
  selectUser(user) {
    const customerId = user.info.number.value;
    this.router.navigate([`/${root}/support/user/info/${customerId}`]);
  }

}
