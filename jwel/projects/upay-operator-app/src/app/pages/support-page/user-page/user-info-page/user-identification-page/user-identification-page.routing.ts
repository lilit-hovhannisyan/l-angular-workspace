import {Routes} from '@angular/router';
import {UserIdentificationPageComponent} from './user-identification-page.component';

export const userIdentificationPageRoutes:  Routes = [
  {
    path: '',
    component: UserIdentificationPageComponent
  }
];
