import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {userRoutes} from './user-page.routing';
import {WidgetsModule} from '../../../widgets/widgets.module';

import {UserPageComponent} from './user-page.component';

@NgModule({
  declarations: [
    UserPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
    WidgetsModule
  ],
  exports: [
    UserPageComponent
  ]
})

export class UserPageModule {

}
