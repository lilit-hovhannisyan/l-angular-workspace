import {Component} from '@angular/core';
import {SupportService} from '../../../dal/support/support.service';
import {LoadingService, Page} from 'upay-lib';
import {Router} from '@angular/router';
import {supportOptions} from '../../../widgets/user-filter/user-filter.component';

@Component ({
  selector: 'app-support-user',
  templateUrl: './user-page.component.html',
  styleUrls: []
})

export class UserPageComponent {

  constructor(private supportService: SupportService,
              private loadingService: LoadingService,
              private router: Router) {}

  // filter
  options = supportOptions;
  searchCriteria: any = {};

  // user list
  users = null;
  showLabel = true;

  //
  totalItems = 0;
  pageSize = 10;
  pageIndex = 0;

  // search
  getUserList() {
    const page = new Page();
    page.size = this.pageSize;
    page.index = this.pageIndex;
    this.searchCriteria.page = page;

    this.loadingService.showLoader(true);
    this.supportService.getUsers(this.searchCriteria).subscribe( res => {
      this.loadingService.showLoader(false);
      this.users = res.items;
      this.totalItems = res.total;
    });
  }

  onSearch(event) {
    this.showLabel = true;
    this.router.navigate(['/operator/support/user']);
    // this.user = null;

    this.searchCriteria = {};
    this.searchCriteria[event.searchKey] = event.searchVal;

    this.getUserList();
  }

  pageChange(event) {
    this.pageIndex = event - 1;
    this.getUserList();
  }

}
