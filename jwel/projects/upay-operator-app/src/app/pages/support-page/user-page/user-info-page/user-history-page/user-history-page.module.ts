import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {userHistoryRoutes} from './user-history-page.routing';
import {WidgetsModule} from '../../../../../widgets/widgets.module';

import {UserHistoryPageComponent} from './user-history-page.component';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    UserHistoryPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userHistoryRoutes),
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    UserHistoryPageComponent
  ]
})

export class UserHistoryPageModule {

}
