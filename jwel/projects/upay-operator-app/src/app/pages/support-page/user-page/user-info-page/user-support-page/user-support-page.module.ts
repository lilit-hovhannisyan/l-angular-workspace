import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {userSupportRoutes} from './user-support-page.routing';
import {WidgetsModule} from '../../../../../widgets/widgets.module';

import {UserSupportPageComponent} from './user-support-page.component';

@NgModule({
  declarations: [
    UserSupportPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userSupportRoutes),
    WidgetsModule
  ],
  exports: [
    UserSupportPageComponent
  ]
})

export class UserSupportPageModule {

}
