import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SupportService} from '../../../../dal/support/support.service';
import {SupportCustomer} from '../../../../widgets/customer-card/customer-card.component';
import {PopupsService} from '../../../../popups/popups.service';
import {identificationPageTabs, root} from '../../../../dal/site/static-data';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user-info-page',
  templateUrl: './user-info-page.component.html',
  styleUrls: []
})

export class UserInfoPageComponent implements OnInit, OnDestroy {
  customerId = null;
  supportUserSource: any[] = [];

  tabs = identificationPageTabs;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private supportService: SupportService,
              private popupsService: PopupsService) {}

  ngOnInit() {
    this.customerId = this.activatedRoute.snapshot.params['customerId'];
    this.supportService.getUsers({number: this.customerId}).subscribe( res => {
        this.supportService.setUser(res.items[0]);

        res.items.map( (user: any) => {

          const frozenStatus = user.accFrozen;
          const actions = [];
          let status = {};
          // todo jamanakavor comment prelive-i hamar
          // actions = [
          //   {
          //     key: 'terminateAccount',
          //     displayText: 'Փակել հաշիվը',
          //   },
          //   {
          //     key: 'freezeAccount',
          //     displayText: 'Սառեցնել հաշիվը',
          //   }
          // ];

          if (frozenStatus) {
          //   actions = [
          //     {
          //       key: 'unFreeze',
          //       displayText: 'Ապասառեցնել'
          //     }
          //   ];
            status = {
              displayText: 'Ձեր հաշիվը սառեցված է:',
              customClass: 'error'
            };
          }


          const customerData = new SupportCustomer(user);

          this.supportUserSource.push({
            status: status,
            info: customerData,
            actions: actions
          });
        });
      });
    this.supportService.getUser().subscribe( user => {
      const customerData = new SupportCustomer(user);
      this.supportUserSource[0]['info'] = customerData;
    });
  }

  ngOnDestroy() {
    this.supportService.destroyUser();
  }

  onCtaClick(event) {
    const action = event.actionKey;
    const userNumber = event.selectedResult ? event.selectedResult.info.number : null;

    if (action === 'unFreeze') {
      this.unFreeze();
    }

    if (action === 'freezeAccount') {
      this.freeze();
    }

    if (action === 'terminateAccount') {
      this.terminateAccount();
    }
  }

  freeze() {
    this.popupsService.openPopUp('freezeAccount', {
      customerId: this.customerId
    });
  }

  unFreeze() {
    this.popupsService.openPopUp('unFreezeAccount', {
      customerId: this.customerId
    });
  }

  terminateAccount() {
    this.popupsService.openPopUp('terminateAccount', {
      customerId: this.customerId
    });
  }
}
