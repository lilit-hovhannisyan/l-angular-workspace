import {Routes} from '@angular/router';
import {UserInfoPageComponent} from './user-info-page.component';

export const userInfoRoutes: Routes = [
  {
    path: '',
    component: UserInfoPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'identification'
      },
      {
        path: 'identification',
        loadChildren: './user-identification-page/user-identification-page.module#UserIdentificationPageModule'
      },
      {
        path: 'activity-log',
        loadChildren: './user-activity-log-page/user-activity-log-page.module#UserActivityLogPageModule'
      },
      {
        path: 'history',
        loadChildren: './user-history-page/user-history-page.module#UserHistoryPageModule'
      },
      {
        path: 'user-support',
        loadChildren: './user-support-page/user-support-page.module#UserSupportPageModule'
      }
    ]
  }
];
