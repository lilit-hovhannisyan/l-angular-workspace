import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {WidgetsModule} from '../../../../widgets/widgets.module';
import {UserListPageComponent} from './user-list-page.component';
import {userListPageRoutes} from './user-list-page.routing';

@NgModule({
  declarations: [
    UserListPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userListPageRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    WidgetsModule
  ],
  providers: [],
  exports: [
    UserListPageComponent
  ]
})

export class UserListPageModule {}
