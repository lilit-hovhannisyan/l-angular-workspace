import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {userInfoRoutes} from './user-info-page.routing';
import {WidgetsModule} from '../../../../widgets/widgets.module';

import {UserInfoPageComponent} from './user-info-page.component';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    UserInfoPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userInfoRoutes),
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    UserInfoPageComponent
  ]
})

export class UserInfoPageModule {

}
