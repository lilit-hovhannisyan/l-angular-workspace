import {Routes} from '@angular/router';
import {UserSupportPageComponent} from './user-support-page.component';

export const userSupportRoutes: Routes = [
  {
    path: '',
    component: UserSupportPageComponent
  }
];
