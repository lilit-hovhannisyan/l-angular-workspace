import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SupportService} from '../../../../../dal/support/support.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {dateFormat} from '../../../../../../../../upay-lib/src/lib/misc-services/date-time.service';

@Component({
  selector: 'app-user-activity-log-page',
  templateUrl: './user-activity-log-page.component.html',
  styleUrls: []
})

export class UserActivityLogPageComponent implements OnInit, OnDestroy {
  columnsConfig = [
    {
      displayValueKey: 'message',
      displayName: () => this.translateService.instant('widgets.log.table.activity'),
      type: 'data'
    },
    {
      displayValueKey: 'date',
      getDisplayValue: (data) => dateFormat(new Date(data), 'dd.mm.yyyy'),
      displayName: () => this.translateService.instant('widgets.log.table.date'),
      type: 'data'
    }
    ,
    {
      displayValueKey: 'doneBy',
      displayName: () => this.translateService.instant('widgets.log.table.doneBy'),
      type: 'data',
    },
  ];
  dataSource: any = [];
  totalItems = 0;
  customerId;
  // queryData;
  currentUserSUB = Subscription.EMPTY;

  constructor (private translateService: TranslateService,
               private supportService: SupportService,
               private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentUserSUB = this.supportService.currentUser.subscribe((user: any) => {
      if (user) {
        this.customerId = user.number;
      }
    });
  }

  ngOnDestroy() {
    this.currentUserSUB.unsubscribe();
  }

  getActivityLogByCustomerId(queryData) {
      const newQueryData = {
        ...queryData,
        customerId: this.customerId
      };

      this.supportService.getActivityLogByCustomerId(newQueryData).subscribe( res => {
        this.dataSource = res.items;
        this.totalItems = res.total;
      });
  }



}
