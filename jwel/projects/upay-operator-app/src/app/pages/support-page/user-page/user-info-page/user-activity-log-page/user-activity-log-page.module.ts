import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {userActivityLogRoutes} from './user-activity-log-page.routing';
import {UserActivityLogPageComponent} from './user-activity-log-page.component';
import {WidgetsModule} from '../../../../../widgets/widgets.module';
import {SharedWidgetsModule} from 'upay-lib';


@NgModule({
  declarations: [
    UserActivityLogPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userActivityLogRoutes),
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    UserActivityLogPageComponent
  ]
})

export class UserActivityLogPageModule {

}
