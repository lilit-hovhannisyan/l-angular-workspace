import {Routes} from '@angular/router';
import {UserPageComponent} from './user-page.component';

export const userRoutes: Routes = [
  {
    path: '',
    component: UserPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'customer-list'
      },
      {
        path: 'customer-list',
        loadChildren: './user-list-page/user-list-page.module#UserListPageModule'
      },
      {
        path: 'info/:customerId',
        loadChildren: './user-info-page/user-info-page.module#UserInfoPageModule'
      }
    ]
  }
];
