import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {SupportService} from '../../../../../dal/support/support.service';
import {dateFormat} from '../../../../../../../../upay-lib/src/lib/misc-services/date-time.service';

@Component({
  selector: 'app-user-history-page',
  templateUrl: './user-history-page.component.html',
  styleUrls: []
})

export class UserHistoryPageComponent implements OnInit, OnDestroy {
  columnsConfig = [
    {
      displayValueKey: 'providerName',
      getDisplayValue: (data) => {
        if (data) {
          return this.translateService.instant(`providers.${data}`);
        }
        return '---------';
      },
      displayName: () => this.translateService.instant('widgets.log.table.providerName'),
      type: 'data',
      sortKey: 'providerId',
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.log.table.receiptId'),
      type: 'data',
      sortKey: 'receiptId'
    },
    {
      displayValueKey: 'userNum',
      displayName: () =>  this.translateService.instant('widgets.log.table.name'),
      type: 'data',
      sortKey: 'userNum'
    },
    {
      displayValueKey: 'issued',
      getDisplayValue: (data) => dateFormat(new Date(data), 'dd.mm.yyyy'),
      displayName: () => this.translateService.instant('widgets.log.table.date'),
      type: 'data',
      sortKey: 'issued'
    },
    {
      displayValueKey: 'issued',
      getDisplayValue: (data) => dateFormat(new Date(data), 'HH:MM:SS'),
      displayName: () => this.translateService.instant('widgets.log.table.time'),
      type: 'data'
    },
    {
      displayValueKey: 'debt',
      displayName: () => this.translateService.instant('widgets.log.table.debt'),
      type: 'data',
      sortKey: 'debt'
    },
    {
      displayValueKey: 'commissionAmount',
      getDisplayValue: (data) => data / 100,
      displayName: () => this.translateService.instant('widgets.log.table.commission'),
      type: 'data',
      sortKey: 'commissionAmount'
    },
    {
      displayValueKey: 'totalAmount',
      getDisplayValue: (data) => data / 100,
      displayName: () => this.translateService.instant('widgets.log.table.amount'),
      type: 'data',
      sortKey : 'totalAmount'
    },
  ];
  dataSource: any = [];
  totalItems = 0;
  orderedFieldsArr = [{key: 'issued', val: false}];

  customerId;
  queryData;
  currentUserSUB = Subscription.EMPTY;

  constructor (private translateService: TranslateService,
               private supportService: SupportService) {
  }

  ngOnInit() {
    this.currentUserSUB = this.supportService.currentUser.subscribe((user: any) => {
      this.customerId = user.number;
    });
  }

  ngOnDestroy() {
    this.currentUserSUB.unsubscribe();
  }

  getUserTransactionLog(queryData) {
    const newQueryData = {
      ...queryData,
      number: this.customerId
    };
    this.supportService.getTransactionLog(newQueryData).subscribe( res => {
      this.dataSource = res.items;
      this.totalItems = res.total;
    });
  }
}
