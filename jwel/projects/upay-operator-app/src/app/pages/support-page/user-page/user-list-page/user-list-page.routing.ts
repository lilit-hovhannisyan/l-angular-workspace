import {Routes} from '@angular/router';
import {UserListPageComponent} from './user-list-page.component';

export const userListPageRoutes: Routes = [
  {
    path: '',
    component: UserListPageComponent
  }
];
