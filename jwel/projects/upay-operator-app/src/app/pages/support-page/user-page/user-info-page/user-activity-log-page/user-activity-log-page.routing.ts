import {Routes} from '@angular/router';
import {UserActivityLogPageComponent} from './user-activity-log-page.component';

export const userActivityLogRoutes: Routes = [
  {
    path: '',
    component: UserActivityLogPageComponent
  }
];
