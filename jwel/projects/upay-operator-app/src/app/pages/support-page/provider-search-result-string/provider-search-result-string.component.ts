import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProviderService} from 'upay-lib';
import {OfficeApi, ProviderData} from 'upay-lib';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-provider-search-result-string',
  templateUrl: './provider-search-result-string.component.html'
})

export class ProviderSearchResultStringComponent implements OnInit {

  @Output() dynamicStringChange = new EventEmitter<any>();

  providerList = null;
  searchFieldsConfig = null;
  providerId = null;
  formData = {};

  constructor(private providerService: ProviderService,
              private officeApi: OfficeApi,
              private translateService: TranslateService) {

  }

  ngOnInit() {
    this.providerService.getConfiguredProviders().subscribe( res => {
      this.providerList = res.map( provider => {
          return {
            key: provider.id,
            name: provider.config.displayText
          };
      });
    });
  }

  selectProvider(event) {
    this.providerId = event;
    const providerKey = event.substring(event.indexOf('.') + 1);
    this.providerService.getConfiguredProvidersByProviderKey(providerKey).subscribe( providerData => {
      this.searchFieldsConfig = providerData.config.getDebtAction.requestFields;
      this.formData = {};
      this.generateProviderFormString();
    });
  }

  generateProviderFormString() {
    let str = this.translateService.instant(this.providerId); // `${this.providerId}`;
    let completed = false;


    for (const pn in this.formData) {
      if (pn) {
        const field = this.searchFieldsConfig.find(f => f.requestKey === pn);
        const fieldText = this.translateService.instant(field.uiComponent.label);
        str = this.formData[pn] ? str + ` ${fieldText} ${this.formData[pn]}` : str.replace('${fieldText}', '');
      }
    }

    completed = !!Object.keys(this.formData).length;

    this.dynamicStringChange.emit({str: str, completed: completed});
  }

  onSearchFieldValueChange(e, f) {
    if (f.uiComponent.component === 'select') {
      this.formData[f.requestKey] = e[f.uiComponent.valueField];
    } else {
      this.formData[f.requestKey] = e;
    }

    if (!e) {
      delete this.formData[f.requestKey];
    }

    this.generateProviderFormString();
  }

  getApiMethodByName(apiMethodName) {
    return (data?: any) => {
      if (!data) {
        data = {};
      }
      return this.officeApi[apiMethodName](data);
    };
  }

}
