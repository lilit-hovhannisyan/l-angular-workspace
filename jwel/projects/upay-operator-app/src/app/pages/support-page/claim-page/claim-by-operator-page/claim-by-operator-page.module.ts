import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';


import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {WidgetsModule} from '../../../../widgets/widgets.module';

import {ClaimByOperatorPageComponent} from './claim-by-operator-page.component';
import {claimByOperatorRoutes} from './claim-by-operator-page.routing';

@NgModule({
  declarations: [
    ClaimByOperatorPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimByOperatorRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    WidgetsModule
  ],
  exports: [
    ClaimByOperatorPageComponent
  ]
})

export class ClaimByOperatorPageModule {

}
