import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertBoxService, ClaimStatus, dateFormat, Notes} from 'upay-lib';
import {ClaimsService} from '../../../../dal/claims/claims.service';
import {root} from '../../../../dal/site/static-data';
import {ClaimTransaction} from '../../../../widgets/claim-card/claim-card.component';
import {environment} from '../../../../../environments/environment';
import {SupportService} from '../../../../dal/support/support.service';

@Component({
  selector: 'app-customer-claim-details-page',
  templateUrl: './customer-claim-details-page.component.html',
  styleUrls: []
})

export class CustomerClaimDetailsPageComponent implements OnInit {
  claimId = null;

  searchResult = [];
  goBackPath = `/${root}/support/claims/list`;
  goBackText = 'pages.support.goBackCustomerClaim';

  // description
  files = [];
  fileStorageUrl = environment.APIEndpoint.fileStorageUrl;

  claimData: any = {};
  customerDataConfig = [
    {
      groupName: 'group1',
      fields: [
        {
          displayValueKey: 'name',
          label: 'Անուն Ազգանուն',
        },
        {
          displayValueKey: 'passport',
          label: 'Անձնագրի համար',
        },
        {
          displayValueKey: 'dateOfIssue',
          label: 'Տրման ժամկետ',
        }
      ]
    },
    {
      groupName: 'group2',
      fields: [
        {
          displayValueKey: 'phone',
          label: 'Հեռախոսահամար',
        },
        {
          displayValueKey: 'issuedBy',
          label: 'Ում կողմից',
        },
        {
          displayValueKey: 'validTo',
          label: 'Վավերական է մինչև',
        }
      ]
    }
  ];


  constructor(private activatedRoute: ActivatedRoute,
              private claimsService: ClaimsService,
              private supportService: SupportService) {}

  ngOnInit() {

    this.claimId = this.activatedRoute.snapshot.params['claimId'];


    this.claimsService.getClaimInfo(this.claimId).subscribe( res => {
      // init trancastion info
      const claimInfo =  new ClaimTransaction(res);
      this.searchResult.push({
        label: {
          displayText: 'widgets.claim.' + res.status,
          customClass: res.status === ClaimStatus.DISMISSED ||
          res.status === ClaimStatus.PRE_REJECTED ||
          res.status === ClaimStatus.REJECTED ? 'error' : 'success'
        },
        info: claimInfo
      });

      // customer data
      this.claimsService.getClaimHistory(this.claimId).subscribe( notes => {
        const customerData = JSON.parse(res.customerPassportInfo);
        customerData['dateOfIssue'] = dateFormat(new Date(customerData['dateOfIssue']), 'dd/mm/yyyy');
        customerData['validTo'] = dateFormat(new Date(customerData['validTo']), 'dd/mm/yyyy');

        this.claimData = {
          direction: res.customerClaimDescription,
          notes: notes.map(note => {
            return new Notes(note);
          }),
          examinationNotes: res.examinationNotes,
          ...customerData
        };
      });

      // files
      this.supportService.getFilesInGroup(res.attachmentKey).subscribe( responseData => {
        this.files = responseData.files;
      });
    });
  }
}
