import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';


import {WidgetsModule} from '../../../../widgets/widgets.module';

import {customerClaimDetailsPageRoutes} from './customer-claim-details-page.routing';
import {CustomerClaimDetailsPageComponent} from './customer-claim-details-page.component';

@NgModule({
  declarations: [
    CustomerClaimDetailsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(customerClaimDetailsPageRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule,
    WidgetsModule
  ],
  providers: [],
  exports: [
    CustomerClaimDetailsPageComponent
  ]
})

export class CustomerClaimDetailsPageModule {
}
