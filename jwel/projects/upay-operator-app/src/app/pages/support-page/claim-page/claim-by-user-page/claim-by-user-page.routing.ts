import {Routes} from '@angular/router';
import {ClaimByUserPageComponent} from './claim-by-user-page.component';

export const claimByUserRoutes: Routes = [
  {
    path: '',
    component: ClaimByUserPageComponent
  }
];
