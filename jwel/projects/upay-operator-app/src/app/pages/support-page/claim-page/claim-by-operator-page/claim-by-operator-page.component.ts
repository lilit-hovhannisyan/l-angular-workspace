import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClaimsService} from '../../../../dal/claims/claims.service';
import {AlertBoxService, CancellationClaimType, OperatorClaimType} from 'upay-lib';
import {ClaimTransaction} from '../../../../widgets/claim-card/claim-card.component';
import {UserDataService} from '../../../../dal/user/user-data.service';
import {Subscription} from 'rxjs/index';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-claim-by-operator-page',
  templateUrl: './claim-by-operator-page.component.html',
  styleUrls: []
})

export class ClaimByOperatorPageComponent implements OnInit, OnDestroy {
  searchResult = [];
  goBackPath = '/operator/support/claim';
  goBackText = 'pages.support.claimByOperator';

  // radiobutton options :: claimTypes
  options = [
    {
      key: 'WRONG_AMOUNT',
      displayText: 'pages.support.wrongAmount'
    },
    {
      key: 'WRONG_SERVICE',
      displayText: 'pages.support.wrongService',
      selected: true
    },
    {
      key: 'WRONG_CUSTOMER',
      displayText: 'pages.support.wrongSubscriberNumber'
    },
    {
      key: 'APP_PROBLEM',
      displayText: 'pages.support.softProblem'
    },
  ];

  // operator info
  currentOperator = null;
  transactionId = null;
  receiptId = null;
  notes = '';
  cancellationClaimType = 'OPERATOR'; // CancellationClaimType;
  operatorClaimType: string; // OperatorClaimType;

  currentOperatorSUB = Subscription.EMPTY;


  constructor(private activeRoute: ActivatedRoute,
              private claimsService: ClaimsService,
              private alertBoxService: AlertBoxService,
              private userDataService: UserDataService,
              private router: Router,
              private translateService: TranslateService) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe( param => {
      this.receiptId =  param['receiptId'];

      this.claimsService.getTransactionByReceiptId(this.receiptId).subscribe( (transaction: any) => {
        // transaction id
        this.transactionId = transaction.id;

        // get operatorName
        this.userDataService.getOperatorById(transaction.operatorId).subscribe( operator => {
          transaction.operatorName = `${operator.forename} ${operator.surname}`;


          const transactionData = new ClaimTransaction(transaction);
          this.searchResult.push({
            info: transactionData
          });
        });
      });
    });

    this.options.map( option => {
      if (option['selected']) {
        this.operatorClaimType = option.key;
      }
    });

    this.currentOperatorSUB = this.userDataService.userInfo.subscribe( user => {
      this.currentOperator = user ? `${user.forename}  ${user.surname}` : `no user`;
    });
  }

  ngOnDestroy() {
    this.currentOperatorSUB.unsubscribe();
  }

  onSelectClaimType(event) {
    const enumKey = event.key;
    this.operatorClaimType = event.key;
  }

  updateNotes(event) {
    this.notes = event.target.value;
  }

  openClaim() {
    this.claimsService.openCancellationClaim(this.transactionId, this.cancellationClaimType, this.notes, this.operatorClaimType )
      .subscribe( res => {
        this.alertBoxService.initMsg({type: 'success', text: 'Հայտը հաջողությամբ ստեղծվել է:'});
        this.router.navigate(['/operator/support/claim']);
    },
    err => {
      this.alertBoxService.initMsg({type: 'error', text: 'Այս գործարքի համար դիմում ներկայացված է'});
    });
  }
}
