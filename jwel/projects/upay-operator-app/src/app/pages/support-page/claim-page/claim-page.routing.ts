import {Routes} from '@angular/router';
import {ClaimPageComponent} from './claim-page.component';

export const claimRoutes: Routes = [
  {
    path: '',
    component: ClaimPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'create'
      },
      {
        path: 'create',
        loadChildren: './claim-create-page/claim-create-page.module#ClaimCreatePageModule'
      },
      {
        path: 'claim-by-operator/:receiptId',
        loadChildren: './claim-by-operator-page/claim-by-operator-page.module#ClaimByOperatorPageModule'
      },
      {
        path: 'claim-by-customer/:receiptId',
        loadChildren: './claim-by-user-page/claim-by-user-page.module#ClaimByUserPageModule'
      },
      {
        path: 'operator-claim-details/:claimId',
        loadChildren: './operator-claim-details-page/operator-claim-details-page.module#OperatorClaimDetailsPageModule'
      },
      {
        path: 'customer-claim-details/:claimId',
        loadChildren: './customer-claim-details-page/customer-claim-details-page.module#CustomerClaimDetailsPageModule'
      }
    ]
  }
];
