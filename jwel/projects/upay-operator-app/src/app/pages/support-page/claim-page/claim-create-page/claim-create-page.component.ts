import {Component} from '@angular/core';
import {ClaimsService} from '../../../../dal/claims/claims.service';
import {Router} from '@angular/router';
import {ClaimTransaction} from '../../../../widgets/claim-card/claim-card.component';
import {UserDataService} from '../../../../dal/user/user-data.service';

@Component({
  selector: 'app-claim-create-page',
  templateUrl: './claim-create-page.component.html',
  styleUrls: []
})

export class ClaimCreatePageComponent {
  activeFilters = ['receiptId'];
  searchResult = [];

  transactionId = null;
  receiptId = null;
  claimType = null;
  isCancellationClaimAcceptable;

  constructor(private claimsService: ClaimsService,
              private router: Router,
              private userDataService: UserDataService) {
  }

  onSearchData(event) {
    this.receiptId = event.receiptId;

    this.searchResult = [];

    this.claimsService.getTransactionByReceiptId(this.receiptId).subscribe( (transaction: any) => {
      // transaction id
      this.transactionId = transaction.id;

      // get operatorName
      this.userDataService.getOperatorById(transaction.operatorId).subscribe( operator => {
        transaction.operatorName = `${operator.forename} ${operator.surname}`;


        const transactionData = new ClaimTransaction(transaction);
        this.searchResult.push({
          info: transactionData
        });

        this.claimsService.isCancellationClaimAcceptable(this.transactionId).subscribe( isCancellationClaimAcceptable => {
            this.isCancellationClaimAcceptable = isCancellationClaimAcceptable;
        });
      });
    });
  }

  openClaim(claimer) {
    this.claimType = claimer;

    this.router.navigate([`/operator/support/claim/claim-by-${claimer}/${this.receiptId}`]);
  }
}
