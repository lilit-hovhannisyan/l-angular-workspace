import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertBoxService, ClaimStatus} from 'upay-lib';
import {ClaimsService} from '../../../../dal/claims/claims.service';
import {root} from '../../../../dal/site/static-data';
import {ClaimTransaction} from '../../../../widgets/claim-card/claim-card.component';
import {UserDataService} from '../../../../dal/user/user-data.service';
import {Notes} from 'upay-lib';

class ClaimDescriptionInOperatorInterface {
  reason: string;
  examinationNotes: string;
  documents: any[];
  actions: any[];
  message: any;

  constructor(data) {
    this.reason = 'pages.support.' + data.operatorClaimType;
    this.examinationNotes = data.examinationNotes;
    this.actions = [];
    this.message = {};
  }
}

@Component({
  selector: 'app-operator-claim-details-page',
  templateUrl: './operator-claim-details-page.component.html',
  styleUrls: []
})

export class OperatorClaimDetailsPageComponent implements OnInit {
  claimId = null;

  searchResult = [];
  goBackPath = `/${root}/support/claims/list`;
  goBackText = 'pages.support.goBackOperatorClaim';

  notes = '';


  claimDescriptionData;

  constructor(private activatedRoute: ActivatedRoute,
              private claimsService: ClaimsService,
              private alertBoxService: AlertBoxService,
              private userDataService: UserDataService,
              private router: Router) {}

  ngOnInit() {
    this.claimId = this.activatedRoute.snapshot.params['claimId'];

    this.claimsService.getClaimInfo(this.claimId).subscribe( res => {
      // init trancastion info
      const claimInfo =  new ClaimTransaction(res);
      this.searchResult.push({
        label: {
          displayText: 'widgets.claim.' + res.status,
          customClass: res.status === ClaimStatus.DISMISSED ||
          res.status === ClaimStatus.PRE_REJECTED ||
          res.status === ClaimStatus.REJECTED ? 'error' : 'success'
        },
        info: claimInfo
      });

      this.claimsService.getClaimHistory(this.claimId).subscribe( notes => {
        // claim description info
        this.claimDescriptionData = new ClaimDescriptionInOperatorInterface(res);
        this.claimDescriptionData.notes = notes.map( note => {
          return new Notes(note);
        });

        this.setActions(res);
      });
    });
  }

  setActions(res) {
    if (res.status === ClaimStatus.APPROVED || res.status === ClaimStatus.REJECTED) {
      this.claimDescriptionData.actions = [
        {
          key: 'complete',
          name: 'buttons.complete',
          customClass: 'success'
        }
      ];
    } else if (res.status === ClaimStatus.DISMISSED) {
      this.claimDescriptionData.message.text = 'pages.support.claimIsDismissed';
      this.claimDescriptionData.message.customClass = 'error';
    } else if (res.status === ClaimStatus.COMPLETED_REJECTED || res.status === ClaimStatus.COMPLETED_APPROVED) {
      // this.claimDescriptionData.message.text = 'pages.support.COMPLETED_REJECTED';
      // this.claimDescriptionData.message.customClass = 'error';
      // this.claimDescriptionData.message.text = 'pages.support.claimIsInProgress';
    } else {
      this.claimDescriptionData.message.text = 'pages.support.claimIsInProgress';
    }
  }

  addNotes(event) {
    this.notes = event;
  }

  onActionClick(action) {
    if (action.key === 'complete') {
      this.claimsService.completeCancellationClaim(this.claimId).subscribe( res => {
        this.alertBoxService.initMsg({type: 'success', text: 'Չեղարկման հայտը ավարտված է:'});
        this.userDataService.refreshUserInfo();
        this.router.navigate([`${root}/support/claims/list`]);
      });
    }
  }
}
