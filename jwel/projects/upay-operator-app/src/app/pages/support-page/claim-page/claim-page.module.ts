import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';

import {WidgetsModule} from '../../../widgets/widgets.module';

import {ClaimPageComponent} from './claim-page.component';
import {claimRoutes} from './claim-page.routing';



@NgModule({
  declarations: [
    ClaimPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule,
    WidgetsModule
  ],
  exports: [
    ClaimPageComponent
  ]
})

export class ClaimPageModule {}
