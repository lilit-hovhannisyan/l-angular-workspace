import {Routes} from '@angular/router';
import {CustomerClaimDetailsPageComponent} from './customer-claim-details-page.component';

export const customerClaimDetailsPageRoutes: Routes = [
  {
    path: '',
    component: CustomerClaimDetailsPageComponent
  }
];
