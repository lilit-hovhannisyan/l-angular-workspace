import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClaimsService} from '../../../../dal/claims/claims.service';
import {ClaimTransaction} from '../../../../widgets/claim-card/claim-card.component';
import { v4 as uuid } from 'uuid';
import {SupportService} from '../../../../dal/support/support.service';
import {AlertBoxService, DocumentType, FileDownload, SharedPrintService} from 'upay-lib';
import {createClaimByCustomerApplicationForm} from '../../../../dal/claims/claim-application-form';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../../../dal/user/user-data.service';
import {environment} from '../../../../../environments/environment';


export interface FormConfig {
  groupName: string;
  fields: Field[];
}

export interface Field {
  key: string;
  type?: string; // defaut -> text
  value?: string;
  label?: string;
  placeholder?: string;
  hasErr?: boolean;
  required?: boolean;
  errorMsg?: string;
  component?: string;
  options?: any;
  getOptions?: any;
  selectedOption?: any;
  outputFieldKey?: string;
  displayFieldKey?: string;
  dependence?: string;
  regExp?: string;
  documentType?: string;
  noEvent?: boolean;
}

@Component({
  selector: 'app-claim-by-user-page',
  templateUrl: './claim-by-user-page.component.html',
  styleUrls: []
})

export class ClaimByUserPageComponent implements OnInit, OnDestroy {
  searchResult = [];
  goBackPath = '/operator/support/claim';
  goBackText = 'pages.support.claimByCustomer';

  transactionId = null;
  receiptId = null;
  notes = '';
  cancellationClaimType = 'CUSTOMER';

  // form config
  formConfig: FormConfig[] = [
    {
      groupName: 'group1',
      fields: [
        {
          key: 'name',
          component: 'input',
          label: 'Անուն Ազգանուն',
          placeholder: 'Հայատառ մեծատառ 3+ նիշ',
          required: true,
          regExp: '^([Ա-Ֆ]{1,}[ա-ֆ]{2,} | [\\s])([Ա-Ֆ]{1,}[ա-ֆ]{2,})$'
        },
        {
          key: 'passport',
          component: 'input',
          label: 'Անձնագրի համար',
          required: true,
        },
        {
          key: 'dateOfIssue',
          component: 'datePicker',
          label: 'Տրման ժամկետ',
          required: true,
        }
      ]
    },
    {
      groupName: 'group2',
      fields: [
        {
          key: 'phone',
          component: 'phonePicker',
          label: 'Հեռախոսահամար',
          required: true,
        },
        {
          key: 'issuedBy',
          component: 'input',
          label: 'Ում կողմից',
          required: true,
        },
        {
          key: 'validTo',
          component: 'datePicker',
          label: 'Վավերական է մինչև',
          required: true,
        }
      ]
    }
  ];
  formData: any = {};

  //  customer form
  customerClaimDescription = null; // irar kanchox selectboxeri infon
  customerClaimDescriptionCompleted = false; // irar kanchox selectboxeri infon
  customerPassportInfo = {};
  customerPhone = null;


  // file upload
  uploadBtnText = 'pages.support.attachFile';
  fileType = '.jpg, .png, .jpeg';
  files = [];
  groupName = null;
  attachmentKey = null; // string to send

  // current Operator
  currentOperator = null;
  userSUB = Subscription.EMPTY;

  constructor(private activeRoute: ActivatedRoute,
              private claimsService: ClaimsService,
              private supportService: SupportService,
              private alertBoxService: AlertBoxService,
              private router: Router,
              private sharedPrintService: SharedPrintService,
              private userDataService: UserDataService) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(param => {
      this.receiptId = param['receiptId'];

      this.claimsService.getTransactionByReceiptId(this.receiptId).subscribe( (transaction: any) => {
        // transaction id
        this.transactionId = transaction.id;

        // get operatorName
        this.userDataService.getOperatorById(transaction.operatorId).subscribe( operator => {
          transaction.operatorName = `${operator.forename} ${operator.surname}`;

          const transactionData = new ClaimTransaction(transaction);
          this.searchResult.push({
            info: transactionData
          });
        });
      });
    });

    this.userDataService.getProfile();
    this.userSUB = this.userDataService.userInfo.subscribe(user => {
      this.currentOperator = user ? `${user.forename} ${user.surname}` : '';
    });
  }

  ngOnDestroy() {
    this.userSUB.unsubscribe();
  }

  // form && validation
  changeFormValue(event, field) {
    if ( event === null ) {
      field.hasErr = false;
      return;
    }

    field.hasErr = false;
    this.formData[field.key] = event;

    field.hasErr =  field.required && !this.formData[field.key];

    if (field.regExp) {
      const pattern = new RegExp(field.regExp, 'g');
      field.hasErr =  !pattern.test(event);
    }

    if (field.key === 'validTo') {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      field.hasErr = today > event;
      field.errorMsg = today > event  ? 'error.invalidDate' : '';
    }

  }

  validateForm() {
    let hasErr = false;

    this.formConfig.map(group => {
      group.fields.map(field => {
        if (field.required && !this.formData[field.key]) {
          field.hasErr = true;
        }
        if (field.hasErr) {
          hasErr = true;
        }
      });
    });

    return hasErr;
  }

  collectCustomerData() {
    this.formConfig.map(group => {
      group.fields.map(field => {

        this.customerPassportInfo[field.key] = this.formData[field.key];


        if (field.key === 'phone') {
          this.customerPhone = this.formData[field.key];
        }
      });
    });
  }

  // select boxes && print application
  onDynamicStringChange(e) {
    this.customerClaimDescription = e.str;
    this.customerClaimDescriptionCompleted = e.completed;
  }

  printApplication() {
    const hasErr = this.validateForm();

    if (hasErr) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.fieldsRequired'});
      return;
    }

    if (!this.customerClaimDescription) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.mustSelectService'});
      return;
    }

    if (!this.customerClaimDescriptionCompleted) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.mustSelectServiceDirection'});
      return;
    }

    this.collectCustomerData();

    const html = createClaimByCustomerApplicationForm(this.searchResult, this.customerPassportInfo, this.customerClaimDescription, this.currentOperator);

    this.sharedPrintService.openPrintWindow(html, 'a4');
  }

  // notes
  updateNotes(event) {
    this.notes = event.target.value;
  }

  // file upload
  attachFile(event) {
    const files = event;

    if (!this.groupName) {
      this.groupName = uuid();
      this.attachmentKey = this.groupName;
    }

    this.supportService.createGroup(this.groupName).subscribe(group => {
      for (const file of files) {
        const fileName = file.name;

        this.supportService.fileUpload(this.groupName, fileName, file).subscribe(res => {
          this.files.push({name: fileName, url: res.url});
        });
      }
    });
  }

  // download file
  getFileLink(url) {
    return environment.APIEndpoint.fileStorageUrl + url;
  }

  // open claim
  openClaim() {
    const hasErr = this.validateForm();

    if (hasErr) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.fieldsRequired'});
      return;
    }

    if (!this.customerClaimDescription) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.mustSelectService'});
      return;
    }

    if (!this.customerClaimDescriptionCompleted) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.mustSelectServiceDirection'});
      return;
    }

    // if (!this.notes) {
    //   this.alertBoxService.initMsg({type: 'error', text: 'error.notesRequired'});
    //   return;
    // }

    if (!this.files.length) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.mustAttachFiles'});
      return;
    }

    this.collectCustomerData();


    this.claimsService.openCancellationClaim(
      this.transactionId,
      this.cancellationClaimType,
      this.notes,
      null,
      this.attachmentKey,
      this.customerClaimDescription,
      JSON.stringify(this.customerPassportInfo),
      this.customerPhone)
      .subscribe(res => {
          this.alertBoxService.initMsg({type: 'success', text: 'Հայտը հաջողությամբ ստեղծվել է:'});
          this.router.navigate(['/operator/support/claim']);
        },
        err => {
          this.alertBoxService.initMsg({type: 'error', text: 'Այս գործարքի համար դիմում ներկայացված է'});
        });
  }
}
