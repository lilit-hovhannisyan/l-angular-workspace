import {Routes} from '@angular/router';
import {OperatorClaimDetailsPageComponent} from './operator-claim-details-page.component';

export const operatorClaimDetailsPageRoutes: Routes = [
  {
    path: '',
    component: OperatorClaimDetailsPageComponent
  }
];
