import {Routes} from '@angular/router';
import {ClaimCreatePageComponent} from './claim-create-page.component';

export const claimCreateRoutes: Routes = [
  {
    path: '',
    component: ClaimCreatePageComponent
  }
];
