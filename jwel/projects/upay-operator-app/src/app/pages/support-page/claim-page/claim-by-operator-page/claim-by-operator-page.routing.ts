import {Routes} from '@angular/router';
import {ClaimByOperatorPageComponent} from './claim-by-operator-page.component';

export const claimByOperatorRoutes: Routes = [
  {
    path: '',
    component: ClaimByOperatorPageComponent
  }
];
