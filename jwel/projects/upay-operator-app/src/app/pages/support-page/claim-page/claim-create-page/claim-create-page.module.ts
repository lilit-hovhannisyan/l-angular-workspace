import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {claimCreateRoutes} from './claim-create-page.routing';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';
import {WidgetsModule} from '../../../../widgets/widgets.module';

import {ClaimCreatePageComponent} from './claim-create-page.component';

@NgModule({
  declarations: [
    ClaimCreatePageComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(claimCreateRoutes),
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule,
    WidgetsModule
  ],
  providers: [],
  exports: [
    ClaimCreatePageComponent
  ]
})

export class ClaimCreatePageModule {
}
