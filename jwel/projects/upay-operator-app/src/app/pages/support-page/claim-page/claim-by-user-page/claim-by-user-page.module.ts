import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {WidgetsModule} from '../../../../widgets/widgets.module';

import {claimByUserRoutes} from './claim-by-user-page.routing';
import {ClaimByUserPageComponent} from './claim-by-user-page.component';
import {ProviderSearchResultStringComponent} from '../../provider-search-result-string/provider-search-result-string.component';
import {SharedWidgetsModule} from 'upay-lib';

@NgModule({
  declarations: [
    ClaimByUserPageComponent,
    ProviderSearchResultStringComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(claimByUserRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    WidgetsModule,
    SharedWidgetsModule
  ],
  exports: [
    ClaimByUserPageComponent
  ]
})

export class ClaimByUserPageModule {

}
