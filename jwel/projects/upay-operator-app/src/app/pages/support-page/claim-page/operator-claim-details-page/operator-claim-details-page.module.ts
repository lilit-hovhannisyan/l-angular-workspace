import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';


import {WidgetsModule} from '../../../../widgets/widgets.module';
import {OperatorClaimDetailsPageComponent} from './operator-claim-details-page.component';
import {operatorClaimDetailsPageRoutes} from './operator-claim-details-page.routing';

@NgModule({
  declarations: [
    OperatorClaimDetailsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(operatorClaimDetailsPageRoutes),
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    SharedWidgetsModule,
    WidgetsModule
  ],
  providers: [],
  exports: [
    OperatorClaimDetailsPageComponent
  ]
})

export class OperatorClaimDetailsPageModule {
}
