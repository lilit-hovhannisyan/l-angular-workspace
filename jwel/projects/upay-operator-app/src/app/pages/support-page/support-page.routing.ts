import {Routes} from '@angular/router';
import {SupportPageComponent} from './support-page.component';

export const supportPageRoutes: Routes = [
  {
    path: '',
    component: SupportPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        // redirectTo: 'user'
        redirectTo: 'user'
      },
      {
        path: 'user',
        loadChildren: './user-page/user-page.module#UserPageModule'
      },
      {
        path: 'claim',
        loadChildren: './claim-page/claim-page.module#ClaimPageModule'
      },
      {
        path: 'claims/list',
        loadChildren: './claim-list-page/claim-list-page.module#ClaimListPageModule'
      }
    ]
  }
];
