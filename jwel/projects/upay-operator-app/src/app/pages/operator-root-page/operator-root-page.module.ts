import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {operatorRootPageRoutes} from './operator-root-page.routing';
import {OperatorRootPageComponent} from './operator-root-page.component';
import {WidgetsModule} from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    OperatorRootPageComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(operatorRootPageRoutes),
    TranslateModule,
    WidgetsModule
  ],
  exports: [
    OperatorRootPageComponent
  ],
})
export class OperatorRootPageModule {
}
