import {Routes} from '@angular/router';
import {OperatorRootPageComponent} from './operator-root-page.component';
import {ForcePassChangeService} from '../../misc-services/forcePassChange.service';

export const operatorRootPageRoutes: Routes = [
  {
    path: '',
    component: OperatorRootPageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'transactions'
      },
      {
        path: 'transactions',
        loadChildren: '../transactions-page/transactions-page.module#TransactionsPageModule',
      },
      {
        path: 'support',
        loadChildren: '../support-page/support-page.module#SupportPageModule',
      },
      {
        path: 'cashbox',
        loadChildren: '../cashbox-page/cashbox-page.module#CashboxPageModule',
      },
      {
        path: 'transaction-log',
        loadChildren: '../transaction-log-page/transaction-log-page.module#TransactionLogPageModule',
      },
      {
        path: 'basket',
        loadChildren: '../basket-page/basket-page.module#BasketPageModule',
      },
      {
        path: 'my-profile',
        canDeactivate: [ForcePassChangeService],
        loadChildren: '../profile-page/profile-page.module#ProfilePageModule'
      }
    ]
  }
];

