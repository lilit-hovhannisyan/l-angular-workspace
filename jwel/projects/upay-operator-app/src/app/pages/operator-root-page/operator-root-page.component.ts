import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/index';
// import {ErrorHandler} from '../../api/error-handler';
import {BasketDataService} from '../../dal/basket/basket-data.service';
import {ErrorHandler} from 'upay-lib';

@Component({
  selector: 'app-operator-root-page',
  templateUrl: './operator-root-page.component.html',
  styleUrls: ['./operator-root-page.component.less']
})
export class OperatorRootPageComponent implements OnInit, OnDestroy {

  userInfoSub = Subscription.EMPTY;
  errorDataSub = Subscription.EMPTY;

  constructor(private userDataService: UserDataService,
              private basketDataService: BasketDataService,
              private errorHandler: ErrorHandler,
              private router: Router) {

  }

  ngOnInit() {
    this.userDataService.getProfile();
    this.userInfoSub = this.userDataService.userInfo.subscribe( user => {
      if ( user && user.status === 'pending') {
        this.router.navigate(['/operator/my-profile']);
      }
    });



    this.errorHandler.errorData.subscribe(data => {
      const logoutErrors: any[] = [
        'security-fault/invalid-token',
        'security-fault/unauthorized-access'
      ];


      if (data && logoutErrors.includes(`${data.code}`)) {
        this.basketDataService.deleteAllFromBasket();
        this.userDataService.signOut();
        this.router.navigate(['/']);
        this.errorHandler.clear();
      }
    });
  }

  ngOnDestroy() {
    this.userInfoSub.unsubscribe();
    this.errorDataSub.unsubscribe();
  }

}
