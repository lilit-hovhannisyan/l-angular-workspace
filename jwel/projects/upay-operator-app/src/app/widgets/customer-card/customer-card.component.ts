import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BusinessDataCardInfo, dateFormat, splitNumberWithLumas} from 'upay-lib';

export class SupportCustomer {
  status?: BusinessDataCardInfo;
  number: BusinessDataCardInfo;
  phone?: BusinessDataCardInfo;
  customerName: BusinessDataCardInfo;
  balance: BusinessDataCardInfo;


  constructor(data) {
    const status = data.userType === 'CARD_IDENTIFIED' ? 'Identified' :
      data.userType === 'BRANCH_IDENTIFIED' ? 'Premiere' : 'Non-Identified';

    this.status = new BusinessDataCardInfo({
      displayText: 'widgets.support.status',
      value:  status,
      customClass: 'info'
    });
    this.number = new BusinessDataCardInfo({
      displayText: 'widgets.support.number',
      value:  data.number,
      customClass: 'bold'
    });
    this.phone = new BusinessDataCardInfo({
      displayText: 'widgets.support.phone',
      value:  data.phone,
      customClass: 'bold'
    });
    this.customerName = new BusinessDataCardInfo({
      displayText: 'widgets.support.nameSurname',
      value:  `${data.forename} ${data.surname}`,
      customClass: 'bold'
    });
    this.balance = new BusinessDataCardInfo({
      key: 'balance',
      displayText: 'widgets.support.balance',
      value:  splitNumberWithLumas(data.balance / 100),
      customClass: 'success'
    });
    // this.accFrozen = data.accFrozen;
  }
}

@Component({
  selector: 'app-customer-card',
  templateUrl: './customer-card.component.html',
  styleUrls: []
})

export class CustomerCardComponent {
  @Input() customerSearchResult: SupportCustomer[] = [];
  @Input() noEvent = false;
  @Input() infoGroups = ['default-group'];
  @Output() selectUser = new EventEmitter<any>();
  @Output() ctaClick = new EventEmitter<any>();
  @Output() valueChange = new EventEmitter<any>();

  jonsParse = JSON.stringify;

  onSelectUser(event) {
    this.selectUser.emit(event);
  }

  onCtaClick(event) {
    this.ctaClick.emit(event);
  }

  onValueChange(event) {
    this.valueChange.emit(event);
  }
}
