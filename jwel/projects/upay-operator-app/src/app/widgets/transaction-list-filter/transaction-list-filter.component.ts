import {Component, EventEmitter, Output} from '@angular/core';
import {ICashboxTransactionsFilter} from '../../dal/cashbox/cashbox-data.service';
import {AccountantApi, AlertBoxService} from 'upay-lib';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-transaction-list-filter',
  templateUrl: './transaction-list-filter.component.html',
  styleUrls: []
})
export class TransactionListFilterComponent {
  amountText = 'widgets.filters.amount_interval';
  dateText = 'widgets.filters.date_interval';
  resetText = 'buttons.reset';
  searchText = 'buttons.search';
  placeholder = ' -- Ընտրել պարամետրը -- ';
  servicePlaceholder = '-- Ընտրել ծառայությունը --';
  services = [];
  selectedData = null;

  criteria = '';
  by = '';

  amdFrom = null;
  amdTo = null;
  dateFrom = null;
  dateTo = null;

  filterData: ICashboxTransactionsFilter = {};
  @Output() search = new EventEmitter<any>();

  constructor(private alertBoxService: AlertBoxService,
              private translateService: TranslateService,
              private accountantApi: AccountantApi) {}

  optionPrefix = 'Ընտրել ';
  options = [
    {
      id: 'receiptId',
      text: 'ըստ անդորագրի',

    },
    {
      id: 'userNum',
      text: 'ըստ բաժանորդի',
    },
    {
      id: 'providerName',
      text: 'ըստ ծառայության',
    }
  ];

  onOptionClick(event) {
    // this.placeholder = this.optionPrefix + ' ' + event.text;
    this.by = event.text;

    if (event.id === 'providerName') {
      this.services = [];

      // todo inchi e accountantApi-@ vercrac
      this.accountantApi.getServiceNames({}).subscribe(res => {
        for (let i = 0; i < res.length; i++) {
          this.services.push({
            id: res[i],
            text:  this.translateService.instant('providers.' + res[i])
          });
        }
      });
    }

    this.onFilterDataChange('columnKey', event.id);
  }

  onFilterDataChange(key, event) {
    if (key === 'amountFrom' || key === 'amountTo') {
      // amount case
      this.filterData[key] = event * 100;
    } else {
      // date case
      this[key] = event;

      if ( this.dateFrom && this.dateTo && new Date( this.dateFrom ) > new Date (this.dateTo)) {
        this.alertBoxService.initMsg({type: 'error', text: 'Նախորդող ժամկետը չի կարող մեծ լինել հաջորդող ժամկետից'});
        return;
      }

      this.filterData[key] = event;
    }

    console.log('yup', this.filterData);
  }

  onSearch() {
    // prevent search
    if ( this.dateFrom && this.dateTo && new Date( this.dateFrom ) > new Date (this.dateTo)) {
      this.alertBoxService.initMsg({type: 'error', text: 'Նախորդող ժամկետը չի կարող մեծ լինել հաջորդող ժամկետից'});
      return;
    }

    console.log('filte', this.filterData);
    this.search.emit(this.filterData);
  }

  onReset() {
    this.filterData = {};
    this.onSearch();

    // first block
    this.placeholder = ' -- Ընտրել պարամետրը --';
    this.criteria = '';
    this.by = '';

    // second block
    const elList = document.getElementById('transactionLogFilters').getElementsByTagName('input');
    for ( let i = 0; i < elList.length; i++ ) {
      elList[i].value = '';
    }
  }
}
