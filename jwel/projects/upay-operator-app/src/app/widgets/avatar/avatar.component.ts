import {Component, OnInit, OnDestroy} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {Router} from '@angular/router';
import {UserInfoData} from '../../dal/user/user-data.models';
import {Subscription} from 'rxjs/index';
import {BasketDataService} from '../../dal/basket/basket-data.service';


@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: []
})

export class AvatarComponent implements OnInit, OnDestroy {
  currentUser: UserInfoData;
  myProfileText = 'widgets.avatar.text.view_profile';
  linkToMyprofile = '/operator/my-profile';
  logoutBtnText = 'widgets.avatar.button.logout';

  userInfoSub = Subscription.EMPTY;

  constructor(private userDataService: UserDataService,
              private router: Router,
              private basketDataService: BasketDataService) {
  }

  ngOnInit() {
    this.userInfoSub = this.userDataService.userInfo.subscribe(data => {
      this.currentUser = data;
    });
  }

  ngOnDestroy() {
    this.userInfoSub.unsubscribe();
  }

  logout() {
    this.basketDataService.deleteAllFromBasket();
    this.userDataService.signOut();
  }
}
