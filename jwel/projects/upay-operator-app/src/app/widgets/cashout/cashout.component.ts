import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {cashOutOptions} from '../user-filter/user-filter.component';
import {SupportService} from '../../dal/support/support.service';
import {LoadingService, UserType, Validity, DocumentType, ProviderService, dateFormat, AlertBoxService} from 'upay-lib';
import {UserDataService} from '../../dal/user/user-data.service';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import { v1 as uuidv } from 'uuid';
import {PopupsService} from '../../popups/popups.service';
import {EncashmentProviderService} from '../../dal/encashment/encashment-new/encashment-provider.service';
import {Subscription} from 'rxjs/index';

// export const converPhonenNumber = (string, regExp) => {
//   const _regExp = new RegExp(regExp, 'g');
//   return
// }

class CustomerData {
  userType: string;
  number: string;
  balance: number;
  name: string;
  phone: string;

  constructor(data) {
    const forename = data.forename || '- ';
    const surname = data.surname || ' -';
    const userType = data.userType === UserType.UNIDENTIFIED ? 'Չնույնականացված' :
                     data.userType === UserType.CARD_IDENTIFIED ? 'Քարտով նույնականացված' : 'Պրեմիեր';

    this.userType = userType;
    this.number = data.number;
    this.balance = data.balance / 100;
    this.name = forename + ' ' + surname;
    this.phone = data.phone;
  }
}

class DocumentData {
  docType: string;
  documentNumber: string;
  dateOfIssue: string;
  dateOfExpiry: any;
  authority: string;

  constructor(data) {
    const validity = data.validity ? '<span>   Վավեր Է</span>' :
                                     '<span>   Անվավեր Է</span>';

    this.docType = data.text;
    this.documentNumber = data.documentNumber;
    this.dateOfIssue = dateFormat(new Date(data.dateOfIssue), 'dd.mm.yyyy');
    this.dateOfExpiry = dateFormat(new Date(data.dateOfExpiry), 'dd.mm.yyyy') + validity;
    this.authority = data.authority;
  }
}

@Component({
  selector: 'app-cash-out',
  templateUrl: './cashout.component.html',
  styleUrls: ['./cashout.component.less']
})

export class CashoutComponent implements OnInit, OnDestroy {
  // encashment
  category;
  type;
  currentEncashmentProvider;

  branchData: any = {};
  userSub = Subscription.EMPTY;



  // search data
  filterData = {};

  options = cashOutOptions;
  errorMessage = '';

  // probivider data
  amountMin;
  amountMax;
  commissionRanges;
  commission = 0;

  customerDataSource;
  customerData;

  documentDataSource;
  documentData;

  documentSelectPlaceholder = 'widgets.cashout.text.selectDocType';
  documentOptions = [
    {
      key: 'PASSPORT',
      text: 'widgets.support.PASSPORT'
    },
    {
      key: 'IDCARD',
      text: 'widgets.support.IDCARD'
    }];
  documentOptionsData = [];
  allowFillAmount;
  redirectTo = false;


  objectKeys = Object.keys;

  cashBoxBalanceSub;
  cashBoxAvailableMoney;
  amount;

  @Output() cashoutAction = new EventEmitter();

  customerSub = new Subscription();

  constructor(private translateService: TranslateService,
              private supportService: SupportService,
              private loadingService: LoadingService,
              private providerService: ProviderService,
              private alertBoxService: AlertBoxService,
              private userDataService: UserDataService,
              private cashboxDataService: CashboxDataService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private popupsService: PopupsService,
              private encashmentProviderService: EncashmentProviderService) {
    // this.userDataService.userInfo.subscribe(res => {
    //   if (res) {
    //     this.cashBoxBalanceSub = this.cashboxDataService.getCurrentBalance({cashboxId: res.cashBoxId}).subscribe(balance => {
    //       this.cashBoxAvailableMoney = balance / 100;
    //     });
    //   }
    // });
  }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.category = params['category'];
    this.type = params['type'];

    this.currentEncashmentProvider = this.encashmentProviderService.getEncashmentProviderByType(this.type);

    this.userSub = this.userDataService.userInfo.subscribe( user => {
      if (user) {
        this.branchData.operatorId = user.id;
      }

      this.cashboxDataService.getCashboxInfo().subscribe( cashbox => {
        this.cashBoxAvailableMoney = cashbox.balance / 100;
        this.branchData.branch = cashbox.name;
        this.branchData.branchId = cashbox.branchId;
      });
    });

    // this.cashBoxBalanceSub.unsubscribe();
    this.cashboxDataService.getBalanceChange().subscribe( res => {
      this.errorMessage = '';
      this.redirectTo = false;
      this.amount = null;
      this.commission = 0;

      this.customerSub.unsubscribe();
      this.customerSub = this.supportService.getCustomer(this.filterData).subscribe( customerData => {

        this.customerDataSource = customerData;
        this.customerData.balance = customerData.balance / 100;
      });
    });
  }

  ngOnDestroy() {
    // this.cashBoxBalanceSub.unsubscribe();
    this.customerSub.unsubscribe();
    this.userSub.unsubscribe();
  }

  onSearch(event) {
    // reset data
    this.customerData = null;
    this.documentData = null;
    this.documentDataSource = null;
    this.allowFillAmount = false;
    this.errorMessage = '';
    this.redirectTo = false;
    this.amount = null;
    this.commission = 0;

    // call new data
    this.filterData = {};
    this.filterData[event.searchKey] = event.searchVal;

    // this.router.navigate(
    //   [],
    //   {relativeTo: this.activatedRoute,
    //           queryParams: this.filterData}
    // );

    this.getCustomer(this.filterData);
  }

  getCustomer(requestData) {
    this.supportService.getCustomer(requestData).subscribe( customer => {
      this.customerDataSource = customer;
      this.customerData = new CustomerData(customer);

      if (customer.accFrozen) {
        this.errorMessage = 'error.accountIsFrozen';
        this.redirectTo = true;
        this.loadingService.showLoader(false);
        return;
      }
      // if customer is not identified or identified by card, must be identified by branch
      if (customer.userType === UserType.UNIDENTIFIED || customer.userType === UserType.CARD_IDENTIFIED) {
        this.errorMessage = 'error.mustBeIdentifiedInBranch';
        this.redirectTo = true;
        this.loadingService.showLoader(false);
        return;
      }

      // get attached documents
      this.supportService.getIdentifiedUser(customer.number).subscribe( documents => {
        this.documentDataSource = documents;
        if (documents.length === 0) {
          this.errorMessage = 'error.attachedFilesNotFoundForIdentifiedUser';
          this.loadingService.showLoader(false);
          return;
        }

        this.documentOptionsData = [];
        documents.map( i => {
          this.documentOptionsData.push({
            key: i.docType,
            text: 'widgets.support.' + i.docType,
            validity: i.validity === Validity.VALID,
            documentNumber: i.documentNumber,
            dateOfIssue: i.dateOfIssue,
            dateOfExpiry: i.dateOfExpiry,
            authority: i.authority
          });
        });

        this.providerService.getProviders().subscribe( providers => {
          providers.map( provider => {

            if (provider.key === 'upayCustomerCashout') {

              this.amountMin = provider.amountMin;
              this.amountMax = provider.amountMax;
              const providerId = provider.id;
              this.providerService.getProviderCommissionsByProviderId(providerId).subscribe( commissionRanges => {
                this.commissionRanges = commissionRanges;
                this.loadingService.showLoader(false);
                return;
              });
            }
          });
        });
      });
    }, error => {
      this.alertBoxService.initMsg({type: 'error', text: 'error.noItemFound'});
    });
  }

  onDocumentOptionClick(event) {
    this.documentData = null;
    this.allowFillAmount = false;
    this.errorMessage = '';
    this.redirectTo = false;

    const selectedDoc = this.documentOptionsData.find( o => o.key === event.key);

    if (!selectedDoc) {
      this.errorMessage = 'error.notIdentifiedWithCurrentDoc';
      this.redirectTo = true;
      return;
    }

    this.documentDataSource = selectedDoc;

    this.documentData = new DocumentData(selectedDoc);

    if (!selectedDoc['validity']) {
      this.errorMessage = 'error.selectedDocIsInvalid';
      this.redirectTo = true;
      return;
    }

    this.allowFillAmount = true;
  }

  onCashOut(e) {
    const amountForCashout =  this.amount;

    if (!amountForCashout) {
      this.errorMessage = 'error.noValue';
      return;
    }
    const operatorBalance = this.cashBoxAvailableMoney;
    const customerBalanace = this.customerData.balance;


    if ( +amountForCashout < this.amountMin) {
      this.errorMessage = 'error.amountMin';
      return;
    }

    if ( +amountForCashout > this.amountMax) {
      this.errorMessage = 'error.amountMax';
      return;
    }

    if ( +amountForCashout + this.commission > +customerBalanace) {
      this.errorMessage = 'error.customerBalanaceIsBiggerThanAmountForCashout';
      return;
    }

    if ( +amountForCashout + this.commission > +operatorBalance) {
      this.errorMessage = 'error.notEnoughMoneyInCashbox';
      return;
    }


    // const type = this.translateService.instant(this.documentDataSource.text);
    const type = this.documentDataSource.key;
    const documentNumber = this.documentDataSource.documentNumber;
    const dateOfIssue = dateFormat(new Date(this.documentDataSource.dateOfIssue), 'dd/mm/yyyy');
    const authority = this.documentDataSource.authority;
    const dateOfExpiry = dateFormat(new Date(this.documentDataSource.dateOfIssue), 'dd/mm/yyyy');


    const emitData = {
      customerNumber: this.customerDataSource.number,
      name: this.customerDataSource.forename + ' '  + this.customerDataSource.surname,
      transactionId: uuidv(),
      amount:  amountForCashout, // convert to lumas todo jshtel
      commission: this.commission,  // convert to lumas  todo jshtel
      clientdData: 'df',
      contactNum: this.customerDataSource.phone,
      type: type,
      documentNumber: documentNumber,
      dateOfIssue: dateOfIssue,
      authority: authority,
      dateOfExpiry: dateOfExpiry,
    };


    this.encashmentProviderService.numberToText(emitData.amount).subscribe( amountText => {
      // debugger
      const dynamicOrderData = {
        ...this.branchData,
        ...emitData,
        orderId: '~~~',
        date: new Date(),
        _amountText: amountText
      };

      this.popupsService.openPopUp(
        'encashmentOrder',
        {encashmentProviderData: this.currentEncashmentProvider, dynamicOrderData: dynamicOrderData});
    });
  }

  onValueChange(e) {
    this.commission = 0;
    this.amount = e;
    this.errorMessage = '';

    const commissionsRole = this.commissionRanges.ranges;
    this.commission = this.providerService.calculateProviderCommission(this.amount, commissionsRole);
  }

  redirectToIdentificationRoute(customerNumber) {
    this.router.navigate([`/operator/support/user/info/${customerNumber}/identification`]);
  }
}
