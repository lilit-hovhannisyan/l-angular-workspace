export const getSelectedContracts = (dataSource, selectedContractCount) => {
  const selectedContractList = dataSource.filter( item => {
    return item.checked === true;
  });

  selectedContractCount = selectedContractList.length;

  return selectedContractList;
};

export const selectAll = (event, dataSource, selectedContractCount, isCheckedAll) => {
  const checkedState = event.emitedData.checkboxState;

  dataSource.map( item => {
    item.checked = checkedState;
  });

  if (checkedState) {
    selectedContractCount = dataSource.length;
    isCheckedAll = true;
    return;
  }

  selectedContractCount = 0;
  isCheckedAll = false;
};

export const detectIfAllItemsAreSelected = (dataSource, selectedContractCount, isCheckedAll) => {
  const notChecked = [];
  let count = dataSource.length;
  dataSource.filter( item => {
    if (!item.checked) {
      notChecked.push(item);
      count--;
    }
  });

  selectedContractCount = count;

  if (notChecked.length) {
    isCheckedAll = false;
    return;
  }
  isCheckedAll = true;
};
