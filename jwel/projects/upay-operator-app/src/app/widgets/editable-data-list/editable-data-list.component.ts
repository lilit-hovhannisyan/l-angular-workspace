import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {deepFind} from 'upay-lib';
import {TranslateService} from '@ngx-translate/core';
import {isUndefined} from 'util';

interface Action {
  key: string;
  displayText?: string;
  icon?: string;
}
interface TopHeader {
  icon?: string;
  title?: string;
  value?: any;
  action?: Action;
  customClassName?: string;
}

interface Columns {
  columnType: any; // checkbox, string, editableString, action
  columnHeaderDisplayText?: string;
  columnHeaderAction?: Action;
  columnValue?: any;
  columnValueDeepFind?: any;  // todo columnValue gaxapary petq chi, columnValueDeepFind-y bolor casery pkum
  columnValueDeepFindFallBackStr?: string;
  columnComponentConfig: any;
}

@Component({
  selector: 'app-editable-data-list',
  templateUrl: './editable-data-list.component.html',
  styleUrls: ['./editable-data-list.component.less']
})
export class EditableDataListComponent implements OnInit {
  @Input() topHeader: TopHeader[];
  @Input() columns: Columns[];
  @Input() dataSource: any[];
  @Input() isCheckedAll = true;


  @Output() headerActionClick = new EventEmitter<{action: Action, emitedData?: any, selectedContracts: any}>();
  @Output() tableActionClick = new EventEmitter<{action: Action, row?: any, selectedContracts: any}>();
  @Output() editRowClick = new EventEmitter<{item: any, index: number}>();
  @Output() focus = new EventEmitter<{row: any}>();

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    this.checkAll(this.isCheckedAll);
  }

  getValue(column, rowObj) {
    // return arr
    if ( column.columnValueDeepFind ) {
      const _deepFindStringArr = [];
      column.columnValueDeepFind.map( deepFindString => {
        let newString = deepFind(rowObj, deepFindString);
        if (!newString && column.columnValueDeepFindFallBackStr) {
          newString = column.columnValueDeepFindFallBackStr;
        }

        if (isUndefined(newString)) {
          newString = '--';
        }

        _deepFindStringArr.push(newString);
      });

      return _deepFindStringArr;
    }

    // single value
    return rowObj[column['columnValue']];
  }

  onHeaderActionClick(action, emitedData?) {
    this.headerActionClick.emit({
      action: action,
      emitedData: emitedData,
      selectedContracts: this.getSelectedContracts()
    });
  }

  onTableActionClick(action, rowObj) {
    this.tableActionClick.emit({
      action: action,
      row: rowObj,
      selectedContracts: this.getSelectedContracts()
    });
  }


  // checkboxes
  getSelectedContracts() {
    return this.dataSource.filter( item => {
      return item.checked === true;
    });
  }

  onCheckAll(action, event) {
    this.isCheckedAll = !this.isCheckedAll;
    this.checkAll(this.isCheckedAll);

    this.onHeaderActionClick(action, event);
  }

  checkAll(state: boolean) {
    this.dataSource.map( item => {
       item.checked = state;
    });
  }

  onCheckItem(action, rowObj) {
    this.dataSource.filter( item => {
      if (item === rowObj) {
         item.checked = !item.checked;
      }
    });

    this.detectIfAllItemsAreSelected();
    this.onTableActionClick(action, rowObj);
  }

  detectIfAllItemsAreSelected() {
    let notCheckedItemsCount = 0;
    this.dataSource.filter( item => {
      if (!item.checked) {
        notCheckedItemsCount++;
      }
    });

    this.isCheckedAll = !notCheckedItemsCount;
  }


  // edit row
  editRow(rowObj, index) {
    rowObj.editMode = !rowObj.editMode;

    if (!rowObj.editMode) {
      this.editRowClick.emit({item: rowObj, index: index});
    }
  }

  onValueChange(mouseEvent, rowObj, dataSourcePropertyName) {
    rowObj[dataSourcePropertyName] = mouseEvent.target.value;
  }

  // implement if needed
  onFocus(rowObj) {
    this.focus.emit(rowObj);
  }

  onEnter(e, rowObj, index) {
    if (e.keyCode === 13) {
      this.editRow(rowObj, index);
    }
  }
}
