import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

import { AtomsModules, MoleculesModule, OrganismsModule } from 'iu-ui-lib';

import { AvatarComponent } from './avatar/avatar.component';
import { EncashmentCategoryNavigationComponent } from './encashment-category-navigation/encashment-category-navigation.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionListFilterComponent } from './transaction-list-filter/transaction-list-filter.component';
import { EncashmentSelectDataComponent } from './encashment-select-data/encashment-select-data.component';

import {UserFilterComponent} from './user-filter/user-filter.component';
import {ClaimCardComponent} from './claim-card/claim-card.component';
import {SharedWidgetsModule} from 'upay-lib';
import {CustomerCardComponent} from './customer-card/customer-card.component';


import { GroupComponent } from './group/group.component';
import { BasketComponent } from './basket/basket.component';
import { EditableDataListComponent } from './editable-data-list/editable-data-list.component';
import {CashoutComponent} from './cashout/cashout.component';

import {EncashmentTypeComponent} from './encashment/encashment-type/encashment-type.component';
import {EncashmentNavigationComponent} from './encashment/encashment-navigation/encashment-navigation.component';
import {EncashmentListByGroupComponent} from './encashment/encashment-list-by-group/encashment-list-by-group.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AvatarComponent,
    BasketComponent,
    EncashmentCategoryNavigationComponent,
    TransactionListComponent,
    TransactionListFilterComponent,
    EncashmentSelectDataComponent,
    UserFilterComponent,
    ClaimCardComponent,
    CustomerCardComponent,

    GroupComponent,
    BasketComponent,
    EditableDataListComponent,
    CashoutComponent,

    EncashmentTypeComponent,
    EncashmentNavigationComponent,
    EncashmentListByGroupComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule,
    SharedWidgetsModule
  ],
  exports: [
    AvatarComponent,
    BasketComponent,
    EncashmentCategoryNavigationComponent,
    EncashmentSelectDataComponent,
    TransactionListComponent,
    TransactionListFilterComponent,
    UserFilterComponent,
    ClaimCardComponent,
    CustomerCardComponent,

    GroupComponent,
    BasketComponent,
    EditableDataListComponent,
    CashoutComponent,

    EncashmentTypeComponent,
    EncashmentNavigationComponent,
    EncashmentListByGroupComponent
  ],
  providers: [],
})
export class WidgetsModule { }
