import {Component, Inject, Input, LOCALE_ID, OnDestroy, OnInit} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {CashboxDataService, ICashboxTransactionsFilter} from '../../dal/cashbox/cashbox-data.service';
import {Subscription} from 'rxjs/index';
import {TransactionData} from '../../dal/cashbox/cashbox-data.models';
import {IColumnConfig} from 'iu-ui-lib/lib/molecules/data-table/data-table.component';
import {formatDate} from '@angular/common';
import {PrintService} from '../../misc-services/print.service';
import {PopupsService} from '../../popups/popups.service';
import {EncashmentService} from '../../dal/encashment/encashment.service';
import {TranslateService} from '@ngx-translate/core';
import {AlertBoxService, checkIfOneDayIsPassed, ProviderService, SharedPrintService} from 'upay-lib';
import {Order, Page, ProviderData} from 'upay-lib';
import {EncashmentPrintDataLogService} from '../../dal/encashment/encashment-print-data-log.service';
import {Router} from '@angular/router';
import {EncashmentProviderService} from '../../dal/encashment/encashment-new/encashment-provider.service';


@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: []
})
export class TransactionListComponent implements OnInit, OnDestroy {
  @Input() title?: string;

  pageNumber = 0;
  pageSize = 10;
  totalItems = 0;
  orderedFieldsArr = [{key: 'date', val: false}];
  allowPosTerminal = false;

  userInfoSub = Subscription.EMPTY;
  userTransactionListSub = Subscription.EMPTY;
  providersListSub = Subscription.EMPTY;

  cashBoxId = null;
  transactionListData: TransactionData[] = [];
  providersList: ProviderData[] = [];

  filterData: ICashboxTransactionsFilter = {};

  selectedItemsToPrint = [];

  constructor(private userDataService: UserDataService,
              private cashboxDataService: CashboxDataService,
              private popupsService: PopupsService,
              private router: Router,
              private encashmentService: EncashmentService,
              private encashmentPrintDataLogService: EncashmentPrintDataLogService,
              // private providerDataService: ProviderDataService,
              private printService: PrintService,
              @Inject(LOCALE_ID) private locale: string,
              private translateService: TranslateService,
              private alertBoxService: AlertBoxService,
              private providerService: ProviderService,
              private sharedPrintService: SharedPrintService,
              private encashmentProviderService: EncashmentProviderService) {
  }

  columnsConfig: IColumnConfig[] = [
    {
      displayValueKey: 'providerNameKey',
      imageSrcKey: 'providerIcon',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.transaction'),
      type: 'icon',
      sortKey: 'providerId',
    },
    {
      displayValueKey: 'userId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.user_id'),
      type: 'data',
      sortKey: 'userNum',
    },
    {
      displayValueKey: 'receiptId',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.receipt'),
      type: 'data',
      sortKey: 'receiptId',
    },
    {
      displayValueKey: 'totalAmount',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.amount'),
      type: 'data',
      sortKey: 'totalAmount',
    },
    {
      displayValueKey: 'contactNum',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.contactNum'),
      type: 'data',
      sortKey: 'contactNum',
    },
    {
      getVisibility: () => this.allowPosTerminal,
      displayValueKey: 'isCash',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.isCash'),
      type: 'data',
      getDisplayValue: (isCash) => {
        if (isCash) {
          return 'Կանխիկ';
        }
        return 'Անկանխիկ';
      },
      sortKey: 'isCash',
    },
    {
      displayValueKey: 'posAuthcode',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.posAuthcode'),
      type: 'data',
      sortKey: 'posAuthcode',
    },
    {
      displayValueKey: 'date',
      displayName: () => this.translateService.instant('widgets.transaction_list.header.date'),
      type: 'data',
      sortKey: 'date',
      getDisplayValue: (date) => {
        return formatDate(date, 'HH:mm dd/MM/yyyy', this.locale);
      }
    },
    {
      displayName: () => this.translateService.instant('widgets.transaction_list.header.print'),
      // displayIcon: 'icon_print',
      // printSelected: true,
      type: 'action',
      dispatchActionName: 'print',
      actionIcon: 'icon_print',
      customClass: [
        {className: 'hide-column-content', conditionKey: 'encashmentType', conditionValue: ['OP_CLAIM_APPROVED', 'OP_CLAIM_REJECTED']},
        {className: 'hide-column-content', conditionKey: 'providerNameKey', conditionValue: ['UpayCustomerTopup']}
      ]
    },
    {
      displayName: () => this.translateService.instant('...'),
      type: 'action',
      dispatchActionName: (column, row) => {
        if (row.action === 'PAYMENT_IN') {
          return 'newPay'; // transactions
        } else if (row.encashmentType === 'OP_CLAIM_REJECTED' || row.encashmentType === 'OP_CLAIM_APPROVED') {
          return 'claimDetails'; // claims
        } else {
          return 'redirectToEncashment';
        }
      },
      actionIcon: 'icon_show',
      // customClass: (column, row) => {
      //
      //   if (row.action === 'ENCASHMENT_OUT' || row.action === 'ENCASHMENT_IN') {
      //     if (row.encashmentType === 'OP_CLAIM_REJECTED' || row.encashmentType === 'OP_CLAIM_APPROVED') {
      //       return;
      //     }
      //
      //     return 'hide-column-content';
      //   }
      //   return;
      // }
    }
  ];

  ngOnInit() {
    this.cashboxDataService.getCashboxInfo().subscribe(res => {
      this.allowPosTerminal = res.allowPosTerminal;
    });

    this.userInfoSub = this.userDataService.userInfo
      .subscribe(userInfo => {

        if (userInfo) {
          this.cashBoxId = userInfo.cashBoxId;
          this.getActivityList();
        }
      });

    this.userTransactionListSub = this.cashboxDataService.transactionList
      .subscribe(transactionListData => {
        this.totalItems = transactionListData.total;
        this.transactionListData = transactionListData.items;

        this.transactionListData.map(item => {
          item.providerNameKey = this.translateService.instant('providers.' + item.providerNameTranslated);
        });
      });

    // this.providersListSub = this.providerDataService.providersList.subscribe(providers => {
    //   this.providersList = providers;
    // });

    // this.providerDataService.getPayProvidersCategoryTree();
  }

  ngOnDestroy() {
    this.userInfoSub.unsubscribe();
    this.userTransactionListSub.unsubscribe();
  }

  getActivityList() {
    const page = new Page();
    page.index = this.pageNumber;
    page.size = this.pageSize;

    const order = new Order();
    order.fields = this.orderedFieldsArr;

    this.cashboxDataService.getCashboxTransactions(this.cashBoxId, page, order, this.filterData);
  }

  pageChange(event) {
    this.pageNumber = event - 1;
    this.getActivityList();
  }

  addToSelected(row) {
    this.selectedItemsToPrint.push(row);
  }

  deleteFromSelected(row) {
    const rmIndex = this.selectedItemsToPrint.findIndex(r => {
      return row.receiptId === r.receiptId;
    });
    if (rmIndex > -1) {
      this.selectedItemsToPrint.splice(rmIndex, 1);
    }
  }

  onSelectItem(event) {
    if (event.checkboxState) {
      this.addToSelected(event.row);
    } else {
      this.deleteFromSelected(event.row);
    }
  }

  sortData(orderedFieldsArr) {
    this.orderedFieldsArr = orderedFieldsArr;
    this.getActivityList();
  }

  onFilterData(event) {
    this.pageNumber = 0;
    this.filterData = event;
    this.getActivityList();
  }

  newPrintInvoice(invoiceData) {
    if (invoiceData.clientData) {
      const clientData = JSON.parse(invoiceData.clientData);
      const companyData = {companyName: 'company.name', companyTin: 'company.tin'};
      const branchData = clientData.branch;
      const operatorData = clientData.operator;

      const receiptData = {
        receiptId: invoiceData.receiptId,
        purpose:  invoiceData.purpose,
        issued: invoiceData.date
      };

      let invoiceHtml = this.providerService.getProviderInvoiceHtml({
        debtData: clientData.customerDebtData,
        amount: clientData.amount,
        provider: clientData.providerData,
        commissionValue: clientData.commission,
        receiptData: receiptData,
        companyData: companyData,
        branchInfo: branchData,
        operatorInfo: operatorData,
        imputedPayNode: clientData.imputedPayNode
      });

      const oneDayIsPassed = checkIfOneDayIsPassed(invoiceData.date);
      if (oneDayIsPassed) {
        invoiceHtml = invoiceHtml.substring(0, invoiceHtml.lastIndexOf('</div'));
        invoiceHtml += `<div style="display:flex">
                            <img src="/shared-assets/img/copy.png" alt="logo" style="display: block; width: 80px; margin-left: auto;">
                        </div> </div>`;
      }

      this.sharedPrintService.openPrintWindow( invoiceHtml, 'termo');
    }
  }

  oldPrintInvoice(invoiceData) {
    const printList = [];
    let purpose;


    if (invoiceData.purpose) {
      purpose = invoiceData.purpose;
    } else {
      purpose = invoiceData.providerName;
    }

    let fields: any[] = [];
    let transactionData = [
      {
        name: 'Անդորրագիր',
        value: invoiceData.receiptId
      },
      {
        name: 'Վճարման նպատակ',
        value: purpose
      }
    ];

    if (!invoiceData.clientData) {
      // for older checks provide missing fields (Բաժանորդային համար, Պարտք առ.)
      const oldFileds = [
        {
          name: 'Բաժանորդային համար',
          value: invoiceData.userId
        },
        {
          name: 'Պարտք',
          value: invoiceData.debt / 100 // lumanerov a veradarznum
        }
      ];

      transactionData = [...transactionData, ...oldFileds];
    }
    const totalDetails = [
      {
        name: 'Վճարվել է',
        value: invoiceData.amount - invoiceData.commissionAmount / 100
      },
      {
        name: 'Միջնորդավճար',
        value: invoiceData.commissionAmount / 100
      }
    ];

    fields = [...transactionData, 'hr', ...totalDetails];

    if (invoiceData.clientData) {
      const customDetails = JSON.parse(invoiceData.clientData);
      if (customDetails[0] === 'hr') {
        customDetails.shift();
      }

      fields = [...transactionData, 'hr', ...customDetails, 'hr', ...totalDetails];
    }

    const printEl = {
      totallyPaid: invoiceData.amount,
      branchName: invoiceData.branchName,
      date: invoiceData.date,
      operator: invoiceData.operatorName,
      fields: fields,
    };

    printList.push(printEl);

    this.printService.createInvoice(printList);
  }

  newEncashmentOrderPrint(event) {
    const encashmentType = event.row.providerName;
    const encashmentProviderData = this.encashmentProviderService.getEncashmentProviderByType(encashmentType);


    const rawData = JSON.parse(event.row.rawData);

    this.encashmentProviderService.numberToText(rawData.amount).subscribe( amountText => {
      const dynamicOrderData = {
        ...event.row,
        ...rawData,
        ...rawData.document,
        printOrderState: true,
        _amountText: amountText
      };

      // todo: jamanakavor hardcode, minchev bolor encashmentnery darnan luma
      if (dynamicOrderData.providerName === 'UpayCustomerCashout') {
        dynamicOrderData.amount = dynamicOrderData.amount / 100;
      }
      // todo end


      this.popupsService.openPopUp(
        'encashmentOrder',
        {encashmentProviderData: encashmentProviderData, dynamicOrderData: dynamicOrderData});
    });
  }

  encashmentOrderPrint(event) {
    const orderData = event.row;

    const rawData = JSON.parse(orderData.rawData);
    rawData['type'] = orderData.encashmentType;
    const currentItem = this.encashmentService.getEncashmentsByType(rawData['type']);
    this.encashmentPrintDataLogService.collectPrinterData(
      rawData['type'],
      rawData,
      currentItem,
      formatDate(orderData.date, 'dd/MM/yyyy', this.locale),
      event
    );
  }

  actionClick(event) {
    if (event.actionName === 'print') {

      if (event.row.action !== 'PAYMENT_IN') {
        // TODO NOR ZEVOV
        // debugger
        // const rawData = JSON.parse(event.row.rawData);
        // debugger
        this.newEncashmentOrderPrint(event);
        return;
      }

      if (event.row.clientData) {
        const clientData = JSON.parse(event.row.clientData);

        if (clientData instanceof Array) {
          this.oldPrintInvoice(event.row);
          return;
        }

        if (clientData instanceof Object) {
          this.newPrintInvoice(event.row);
          return;
        }

        console.error('Invalid Print Data ');
      }
    }

    if (event.actionName === 'newPay') {
      // debugger
      const clientData = JSON.parse(event.row.clientData);
      // debugger
      this.providerService.redirectToDebtResult(clientData, 'operator/transactions/terminal');
    }

    if (event.actionName === 'claimDetails') {
      // debugger
      const claimId = JSON.parse(event.row.rawData).id;
      this.router.navigate([`/operator/support/claim/operator-claim-details/${claimId}`]);

    }

    if (event.actionName === 'redirectToEncashment') {
      const encashmentType = event.row.providerName;
      const encashment = this.encashmentProviderService.getEncashmentProviderByType(encashmentType);
      const category = encashment.category;

      this.router.navigate([`/operator/transactions/encashment/${category}/${encashmentType}`]);

    }
  }
}
