import {Component, Input} from '@angular/core';
import {BusinessDataCardInfo, dateFormat} from 'upay-lib';

export class ClaimTransaction {
  completed: BusinessDataCardInfo;
  receiptId: BusinessDataCardInfo;
  branchName: BusinessDataCardInfo;
  purpose: BusinessDataCardInfo;
  contactNum: BusinessDataCardInfo;
  amount: BusinessDataCardInfo;
  userNum: BusinessDataCardInfo;
  operatorName: BusinessDataCardInfo; // todo get operator name by id
  id: string;

  constructor(data) {

    const amount = data.totalAmount || data.amount;
    this.completed = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.date',
      value:  dateFormat(new Date(data.completed), 'dd.mm.yyyy'), // formatDate(new Date(data.completed), 'HH:mm dd/MM/yyyy', this.locale),
      customClass: 'bold'
    });

    this.receiptId = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.receiptId',
      value:  data.receiptId,
      customClass: 'bold'
    });

    this.branchName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.branchName',
      value:  data.branchName,
      customClass: 'bold'
    });

    this.purpose = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.purpose',
      value:  data.purpose,
      customClass: 'bold'
    });

    this.amount = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.amount',
      value:  amount / 100,
      customClass: 'bold'
    });

    this.userNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.userNum',
      value:  data.userNum,
      customClass: 'bold'
    });

    this.operatorName = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.operatorName',
      value:  data.operatorName,
      customClass: 'bold'
    });

    this.contactNum = new BusinessDataCardInfo({
      displayText: 'widgets.transaction_list.header.phone',
      value:  data.contactNum ? data.contactNum : '---',
      customClass: 'bold'
    });

    this.id = data.id;
  }
}

@Component({
  selector: 'app-claim-card',
  templateUrl: './claim-card.component.html',
  styleUrls: []
})

export class ClaimCardComponent {
  @Input() claimSearchResult: ClaimTransaction[] = [];
  @Input() hasGoBack = false;
  @Input() goBackText?: string;
  @Input() goBackPath?: string;
  @Input() goBackIcon?: string;
}
