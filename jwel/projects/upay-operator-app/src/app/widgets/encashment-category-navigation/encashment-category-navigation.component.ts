import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { EncashmentService } from '../../dal/encashment/encashment.service';
import { CashboxAction } from 'upay-lib';

@Component({
  selector: 'app-encashment-witdget',
  templateUrl: './encashment-category-navigation.component.html',
  styleUrls: ['./encashment-category-navigation.component.less']
})
export class EncashmentCategoryNavigationComponent implements OnInit {
  @Input () routPath: string;
  incasationTypes = [];
  constructor(public router: Router,
    private encashmentService: EncashmentService) { }

  ngOnInit() {
    this.incasationTypes = this.encashmentService.getEncashmentTypes();
    if (this.routPath === this.router.url) {
      // console.log(this.incasationTypes[0]);
      const first = this.incasationTypes[0];
      if (first) {
        this.router.navigateByUrl(`${this.routPath}/${first.name}`);
      }
    }
  }

  clearFieldsOnRoutChange() {
    this.encashmentService.clearEncashmentFields();
  }
}
