import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {encashmentTypesConfig} from '../../../dal/encashment/encashment-new/encashment-type-config';
import {EncashmentProviderService} from '../../../dal/encashment/encashment-new/encashment-provider.service';

@Component({
  selector: 'app-encashment-list-by-group',
  templateUrl: './encashment-list-by-group.component.html',
  styleUrls: ['./encashment-list-by-group.component.less']
})

export class EncashmentListByGroupComponent implements OnInit {
  groupItems;
  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private encashmentProviderService: EncashmentProviderService) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.initGroupItems();
      }
    });

  }

  ngOnInit() {
    this.initGroupItems();
  }

  initGroupItems() {
    const encashmentCategory = this.activatedRoute.snapshot.params['category'];
    this.groupItems = this.encashmentProviderService.getEncashmentProviderListByCategory(encashmentCategory);
  }
}
