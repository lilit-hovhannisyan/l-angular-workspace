import {Component} from '@angular/core';
import {encashmentCategoryConfig} from '../../../dal/encashment/encashment-new/encashment-category.config';

@Component({
  selector: 'app-encashment-navigation',
  templateUrl: './encashment-navigation.component.html',
  styleUrls: ['./encashment-navigation.component.less']
})

export class EncashmentNavigationComponent {
  navigation = encashmentCategoryConfig;
}
