import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EncashmentProviderService} from '../../../dal/encashment/encashment-new/encashment-provider.service';
import {root} from '../../../dal/site/static-data';
import {PopupsService} from '../../../popups/popups.service';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../../dal/user/user-data.service';
import {CashboxDataService} from '../../../dal/cashbox/cashbox-data.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AlertBoxService, compareDates, evaluate} from 'upay-lib';

const validationMap =  {
  required: (value, ruleValue) => {
    return Boolean(value) === ruleValue;
  },

  regExp: (value, ruleValue) => {
    const pattern = new RegExp(ruleValue, 'g');
    return value && pattern.test(value);
  },

  validateAgainstToday: (value, ruleValue) => {
    const today = new Date();
    return compareDates(today, value);
  },

  olderDateThan: (value, ruleValue, formFieldsValueObject) => {
    const date1 = value;
    const date2 = evaluate(formFieldsValueObject, ruleValue);
    if (!date2 || !date1) {
      return;
    }
    return compareDates(date1, date2);
  },

  newerDateThan: (value, ruleValue, formFieldsValueObject) => {
    const date1 = value;
    const date2 = evaluate(formFieldsValueObject, ruleValue);
    if (!date2 || !date1) {
      return;
    }
    return compareDates(date2, date1);
  }
};

const defaultErrorMsgMap = {
  required: 'error.fieldIsRequired',

  regExp: 'error.fieldDoesNotMatchToRegExp',

  validateAgainstToday: 'error.validateAgainstToday',

  olderDateThan: 'error.olderDateThan',

  newerDateThan: 'error.newerDateThan'
};

@Component({
  selector: 'app-encashment-type',
  templateUrl: './encashment-type.component.html',
  styleUrls: ['./encashment-type.component.less']
})

export class EncashmentTypeComponent implements OnInit, OnDestroy {
  currentEncashmentProvider;
  inputFields = null;
  category;
  type;

  branchData: any = {};
  orderData: any = {};

  formHasError;
  defaultErrorMsgMap = defaultErrorMsgMap;
  cashboxBalance;

  userSub = Subscription.EMPTY;

  @ViewChild('upayArmenianPhonePicker') public upayArmenianPhonePicker;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private encashmentProviderService: EncashmentProviderService,
              private popupsService: PopupsService,
              private userDataService: UserDataService,
              private cashboxDataService: CashboxDataService,
              private sanitized: DomSanitizer,
              private alertBoxService: AlertBoxService) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.category = params['category'];
    this.type = params['type'];

    this.currentEncashmentProvider = this.encashmentProviderService.getEncashmentProviderByType(this.type);
    this.inputFields = [...this.currentEncashmentProvider.inputFields];

    this.userSub = this.userDataService.userInfo.subscribe( user => {
      if (user) {
        this.branchData.operatorId = user.id;
      }

      this.cashboxDataService.getCashboxInfo().subscribe( cashbox => {
        this.cashboxBalance = cashbox.balance / 100;
        this.branchData.branchName = cashbox.name;
        this.branchData.branchId = cashbox.branchId;
      });
    });

    this.encashmentProviderService.getRefreshedEncashmentData().subscribe( response => {
      // reset
      this.orderData = {};
      if ( this.upayArmenianPhonePicker ) {
        this.upayArmenianPhonePicker.reset();
      }
      this.formHasError = null;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  goBack(category) {
    this.router.navigate([`${root}/transactions/encashment/${category}`]);
    this.inputFields.map( f =>  {
      delete f.uiComponent.hasError;
      delete f.uiComponent.errorMsg;
    });
  }

  onValueChange(event, key) {
    this.orderData[key] = event;
    const field = this.inputFields.find( _field => _field.key === key);
    this.validateField(field);


    if (this.formHasError) {
      this.formHasError = this.validateFields();
    }
  }

  onSelectItem(event, key, dependantFields) {
    this.onValueChange(event.id, key);

    if (dependantFields) {
      dependantFields.map( field => {
        delete this.orderData[field['key']];
        this.inputFields.map( (f, index) => {
          if (f.key === field.key) {
            this.inputFields.splice(index, 1);
          }
        });
      });

      this.inputFields = [
        ...this.currentEncashmentProvider.inputFields,
        ...dependantFields
      ];
    }
  }

  validateFields() {
    let errors = 0;
    this.inputFields.map( field => {
      this.validateField(field);
      errors = field.uiComponent.hasError ? errors + 1 : errors;
    });

    return errors;
  }

  validateField(field) {
    // if no value input, but has default data, set it
    if (!this.orderData[field.key] && field.uiComponent.defaultValueForOrder) {
      this.orderData[field.key] =  field.uiComponent.defaultValueForOrder;
    }

    // validate
    const rules = field.uiComponent.validationRules;
    if (rules) {
      for (const ruleKey in rules) {
        if (ruleKey) {
          const value = this.orderData[field.key];
          const ruleValue =  rules[ruleKey];

          const hasError = validationMap[ruleKey](value, ruleValue, this.orderData);
          if (hasError !== undefined) {
            field.uiComponent.hasError = !hasError;
            field.uiComponent.errorMsg = field.uiComponent.hasError ? defaultErrorMsgMap[ruleKey] : '';
          }
        }
      }
    }
  }

  onConfirm() {
    this.formHasError = this.validateFields();

    if ( this.formHasError ) {
      return;
    }

    const isCashboxActionOut = !this.currentEncashmentProvider.getOrderIdAction.directionIn;

    if (isCashboxActionOut && +this.orderData.amount > +this.cashboxBalance) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.notEnoughMoneyInCashbox'});
      return;
    }

    this.encashmentProviderService.numberToText(this.orderData.amount).subscribe( amountText => {
      // debugger
      const dynamicOrderData = {
        ...this.branchData,
        ...this.orderData,
        orderId: '~~~',
        date: new Date(),
        _amountText: amountText
      };

      this.popupsService.openPopUp(
        'encashmentOrder',
        {encashmentProviderData: this.currentEncashmentProvider, dynamicOrderData: dynamicOrderData});
    });
  }
}
