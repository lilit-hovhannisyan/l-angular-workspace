import {Component, OnDestroy, OnInit} from '@angular/core';
import {forkJoin, of, Subscription} from 'rxjs/index';


import {GroupItem} from '../../dal/groups/groups-data.models';
import {GroupsService} from '../../dal/groups/groups.service';
import {SubscriberDataService} from '../../dal/subscriber/subscriber-data.service';
import {BasketDataService} from '../../dal/basket/basket-data.service';
import {UserDataService} from '../../dal/user/user-data.service';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

import {CalculateCommissionService, DialogService, LoadingService, AlertBoxService} from 'upay-lib';
import {TranslateService} from '@ngx-translate/core';
import { v4 as uuid } from 'uuid';
import {PopupsService} from '../../popups/popups.service';
import {ProviderService, splitNumberWithLumas} from 'upay-lib';
import {ActivatedRoute, Router} from '@angular/router';
import {error} from 'ng-packagr/lib/util/log';

export interface IEditData {
  name: string;
  groupId: string;
}

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: [],
})

export class GroupComponent implements OnInit, OnDestroy {
  searchData = null;
  results = []; // wildcard search

  editData: IEditData; // edit group name

  userActionSub = Subscription.EMPTY; // todo normal dialog sarqel

  // new
  lockBtn = 0;
  selectedGroup;
  selectedGroupName = null;
  selectedContractCount = 0;
  selectedContractTotalDebt = 0;
  selectedContractTotalDebtWithCommission = 0;

  dataSource = [];
  isCheckedAll = true;
  topHeader;
  columns;

  //
  triggerSelectedGroup = false;

  splitNumberWithLumas = splitNumberWithLumas;


  initGroupList() {
    this.topHeader = [
      {
        title: 'widgets.group.text.group',
        value: this.selectedGroupName,
        action: {
          key: 'editGroup',
          icon: 'icon_edit',
        },
      },
      {
        title: 'widgets.group.text.groupDeleteText',
        action: {
          key: 'deleteGroup',
          icon: 'icon_delete',
        },
      }
    ];

    this.columns =  [
      {
        columnType: 'checkbox', // checkbox, string, editableString, action
        columnHeaderAction: {
          key: 'selectAll',
        },
        columnComponentConfig: {
          actionKey: 'selectRow'
        }
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.transaction',
        columnValueDeepFind: ['providerData.config.displayText']
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.subscriber',
        columnValueDeepFind: ['transferMappedData.customerIdKey'],
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.subscriberInfo',
        columnValueDeepFind: ['transferMappedData.customerNameKey', 'transferMappedData.addressKey'],
        columnValueDeepFindFallBackStr: '-'
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.amount',
        columnValueDeepFind: ['transferMappedData.debtKey']
      },
      {
        columnType: 'action',
        columnHeaderAction: {
          key: 'deleteSelectedContractsFromGroup',
          icon: 'icon_delete'
        },
        columnComponentConfig: {
          key: 'redirectToGetDebtPage',
          icon: 'icon_show'
        },
      }
    ];

    this.loadingService.showLoader(false);
  }

  getSelectedContracts() {
    this.selectedContractTotalDebt = 0;
    this.selectedContractTotalDebtWithCommission = 0;
    let commissions = 0;
    const selectedContractList = [];
    this.dataSource.map( item => {
      if (item.checked) {
        const debt = item.transferMappedData.debtKey ? +(item.transferMappedData.debtKey.toString().replace(/\,/g, '')) : 0;
        this.selectedContractTotalDebt += debt;
        commissions += +item.commission;
        selectedContractList.push(item);
      }
    });

    this.selectedContractTotalDebtWithCommission += this.selectedContractTotalDebt + commissions;

    return selectedContractList;
  }

  constructor(private groupsService: GroupsService,
              private subscriberDataService: SubscriberDataService,
              private alertBoxService: AlertBoxService,
              private dialogService: DialogService,
              private basketDataService: BasketDataService,
              private loadingService: LoadingService,
              private userDataService: UserDataService,
              private providerDataService: ProviderDataService,
              private calculateCommissionService: CalculateCommissionService,
              private popupsService: PopupsService,
              private translateService: TranslateService,
              private providerService: ProviderService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    const groupName = this.activatedRoute.snapshot['queryParams']['groupname'];
    if (groupName) {
      this.searchData = groupName;

      if (groupName.length === 11) {
        this.triggerSelectedGroup = true;
      }

      this.onSearch();
    }

    // update group name after edit
    this.groupsService.getUpdatedGroupName().subscribe( _groupName => {
      this.selectedGroupName = _groupName;
      this.topHeader[0].value = _groupName;
      this.searchData = _groupName;
      this.results = [];
    });
  }

  ngOnDestroy() {
    this.userActionSub.unsubscribe();
  }

  onPhoneNumberChange(e) {
    this.searchData = e;

    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {groupname: e},
    });
  }

  onSearch() {
    if ( !this.searchData ) {
      this.alertBoxService.initMsg({type: 'error', text: `Դաշտը պարտադիր է լրացման համար`});
      return;
    }

    if (this.searchData.charAt(0) === '0') {
      this.selectedGroupName = this.searchData.substring(1);
    } else {
      this.selectedGroupName = this.searchData;
    }


    this.dataSource = [];
    return this.groupsService.findGroup(this.selectedGroupName).subscribe( res => {
      if ( res.items.length !== 0) {
        this.results = res.items;

        if (this.triggerSelectedGroup) {
          this.itemClick(0);
        }
      } else {
        this.alertBoxService.initMsg({type: 'error', text: 'Այսպիսի խմբեր չեն գտնվել'});
      }
    });

  }

  onEnter(e) {
    return e.keyCode !== 13 ? false : this.onSearch();
  }

  itemClick(index) {
    this.initContractsInGroup(this.results[index]);

    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {groupname: this.selectedGroupName},
      });
  }

  initContractsInGroup(selectedGroup) {
    this.loadingService.showLoader(true);

    // init group
    this.selectedGroup = selectedGroup;
    this.selectedGroupName = this.selectedGroup.name;

    // reset results
    this.results = [];

    if ( this.selectedGroup.contracts.length === 0 ) {
      this.alertBoxService.initMsg({
        type: 'warning',
        text: 'Այս խումբը չունի որևէ կոնտրակտ, խնդրում ենք զամբյուղից կոնտրակտներ ավելացնել'});
      this.loadingService.showLoader(false);
    }


    const source = [];
    let count = this.selectedGroup.contracts.length;
    this.selectedGroup.contracts.map( (i) => {
      const searchData = JSON.parse(i.searchData);
      this.providerService.getConfiguredProvidersByProviderKey(searchData.providerName).subscribe(providerData => {
        if (providerData) {
          this.providerService.getProviderCommissionsByProviderId(providerData.id).subscribe(providerCommissionsRoles => {
            const ranges = providerCommissionsRoles ? providerCommissionsRoles.ranges : [];
            this.providerService.providerSearchDebt(providerData, searchData).subscribe(customerDebtData => {
              const transferMappedData = this.providerService.getTransferMappedData(providerData, customerDebtData);
              let debtKeyStr = '';
              if (transferMappedData['debtKey']) {
                debtKeyStr = transferMappedData['debtKey'].toString();
              }
              debtKeyStr = debtKeyStr.replace(',', '');
              debtKeyStr = debtKeyStr.lastIndexOf('.') >= 0 ? debtKeyStr.substring(0, debtKeyStr.lastIndexOf('.')) : debtKeyStr;
              const debtKey = +debtKeyStr;
              // new logic
              source.push({
                amount: debtKey,
                providerData,
                providerKey: providerData.config.providerKey,
                customerDebtData: {
                  ...searchData,
                  ...customerDebtData,
                },
                commission: this.providerService.calculateProviderCommission(debtKey, ranges),
                providerCommissionsRoles: ranges,
                transferMappedData
              });

              count--;
              if (count === 0) {
                this.dataSource = source;
                this.initGroupList();
              }
            }, errorData => {
              const debtKey = 0;
              const customerDebtData = {};
              const transferMappedData = this.providerService.getTransferMappedData(providerData, customerDebtData);

              source.push({
                // customClassName: 'error no-event',

                amount: debtKey,
                providerData,
                providerKey: providerData.config.providerKey,
                customerDebtData: {
                  ...searchData,
                  ...customerDebtData,
                },
                commission: this.providerService.calculateProviderCommission(debtKey, ranges),
                providerCommissionsRoles: ranges,
                transferMappedData
              });

              count--;
              if (count === 0) {
                this.dataSource = source;
                this.initGroupList();
              }
            });
          });
        }  else {
          count--;
          if (count === 0) {
            this.dataSource = source;
            this.initGroupList();
          }
        }
      });
    });
  }

  // ---- table

  headerActionClick(event) {
    if (event.action.key === 'selectAll') {
      this.getSelectedContracts();
    }

    if (event.action.key === 'deleteGroup') {
      this.onDeleteGroup();
    }

    if (event.action.key === 'deleteSelectedContractsFromGroup') {
      this.onDeleteSelectedContractsFromGroup();
    }

    if (event.action.key === 'editGroup') {
      // debugger
      this.onEditGroup();
    }
  }

  tableActionClick(event) {
    if (event.action.actionKey === 'selectRow') {
      this.getSelectedContracts();
    }

    if (event.action.key === 'redirectToGetDebtPage') {
      const clientData = event.row;
      // debugger
      this.providerService.redirectToDebtResult(clientData, 'operator/transactions/terminal');
    }
  }

  onDeleteGroup() {
    this.dialogService.initDialog('Դուք պատրաստվում եք ջնջել ամբողջ խումբը');

    this.userActionSub.unsubscribe();
    this.userActionSub = this.dialogService.userActionOnDialog.subscribe( confirm => {
      if ( confirm ) {
        this.groupsService.deleteGroup(this.selectedGroupName).subscribe(res => {
          this.alertBoxService.initMsg({type: 'success', text: 'Խումբը հաջողությամբ ջնջված է'});

          // rollback dialog service state
          this.dialogService.rollBackDialog();
          this.onSearch();
        });
      }
    });
  }

  onDeleteSelectedContractsFromGroup() {
    this.dialogService.initDialog('Դուք պատրաստվում եք ջնջել մի քանի կոնտրակտ');

    this.userActionSub.unsubscribe();
    this.userActionSub = this.dialogService.userActionOnDialog.subscribe( confirm => {
      if ( confirm ) {
        const selectedContracts = this.getSelectedContracts();
        selectedContracts.map( contract => {
          this.groupsService.removeContractFromGroup(contract, this.selectedGroupName).subscribe( res => {
            this.alertBoxService.initMsg({type: 'success', text: 'Կոնտրակտը հաջողությամբ ջնջված է խմբից'});

            this.dataSource.filter((item, index) => {
              if ( item === contract ) {
                this.dataSource.splice(index, 1);
              }
            });

          });

          // rollback dialog service state
          this.dialogService.rollBackDialog();
        });
      }
    });
  }

  onEditGroup() {
    this.editData = {
      name: this.selectedGroupName,
      groupId: this.selectedGroup.id
    };
    this.popupsService.openPopUp('editGroup', this.editData);
  }

  onAddToBasket() {
    if (this.lockBtn) {
      return;
    }

    const selectedContracts = this.getSelectedContracts();

    this.lockBtn = selectedContracts.length;
    if ( selectedContracts.length > 0 ) {
      selectedContracts.map( contract => {
        const isItemInBasket = this.basketDataService.isItemInBasket(contract.transferMappedData.customerIdKey, contract.providerData.config.providerKey);

        if (isItemInBasket) {
          this.alertBoxService.initMsg({type: 'error', text: 'Այս գործարքը արդեն զամբյուղում է'});
          return;
        }

        delete contract.checked;
        if (contract && contract.customerDebtData) {
         contract.customerDebtData.transactionId =  uuid();
        }

        this.basketDataService.addToBasket(contract);
        this.basketDataService.updateCounter();
        this.alertBoxService.initMsg({type: 'success', text: 'Գործարքը հաջողությամբ ավելացվել է զամբյուղ'});
        contract.checked = true;
        this.lockBtn--;
      });
    } else {
      this.alertBoxService.initMsg({type: 'error', text: 'Դուք չունեք ընտրված կոնտրակտ'});
    }
  }
}
