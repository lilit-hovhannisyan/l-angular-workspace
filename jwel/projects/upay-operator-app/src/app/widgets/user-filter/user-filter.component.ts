import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

export const supportOptions = [
  {
    id: 'forename',
    text: 'ըստ անունի'
  },
  {
    id: 'surname',
    text: 'ըստ ազգանունի'
  },
  {
    id: 'number',
    text: 'ըստ յուզեր ID-ի'
  },
  {
    id: 'phone',
    text: 'ըստ հեռախոսի'
  }
];
export const cashOutOptions = [
  {
    id: 'customerId',
    text: 'ըստ upay ID-ի'
  },
  {
    id: 'phone',
    text: 'ըստ հեռախոսահամարի'
  }
];

@Component({
  selector: 'app-user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.less']
})

export class UserFilterComponent {
  filterData: any = {};
  @Output() search = new EventEmitter();

  searchText = 'Փնտրել';
  @Input() forcefilterSelect = false;


  // options
  placeholder = ' -- Ընտրել -- ';
  by = '';
  optionPrefix = '';
  @Input() options;

  onFilterDataChange(key, value) {
    if (typeof value === 'string') {
      this.filterData[key] = value.trim();
    } else {
      this.filterData[key] = value;
    }

    this.activateSearchBtn();
  }

  onOptionClick(option) {
    // debugger
    this.by = option.text;
    this.onFilterDataChange('searchKey', option.id);

    this.activateSearchBtn();
  }

  // search
  onSearch() {
    this.search.emit(this.filterData);
  }

  activateSearchBtn() {
    if ( this.forcefilterSelect && this.by && this.filterData.searchVal) {
      this.forcefilterSelect = !this.forcefilterSelect;
    }
  }

}
