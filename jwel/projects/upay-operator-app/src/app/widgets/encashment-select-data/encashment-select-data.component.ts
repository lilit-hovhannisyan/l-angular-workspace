import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PopupsService} from '../../popups/popups.service';
import {EncashmentService} from '../../dal/encashment/encashment.service';
import {EncashmentPrintDataService} from '../../dal/encashment/encashment-print-data.service';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {DatePipe} from '@angular/common';

import {UserDataService} from '../../dal/user/user-data.service';
import {
  DateTimeService,
  AlertBoxService,
  LoadingService,
  Page,
  UserType,
  BusinessDataCardInfo,
  Validity,
  DocumentType,
  dateFormat,
  ProviderService} from 'upay-lib';
import {Subscription, Observable} from 'rxjs/index';
import {SupportService} from '../../dal/support/support.service';
import {cashOutOptions} from '../user-filter/user-filter.component';
import {SupportCustomer} from '../customer-card/customer-card.component';

@Component({
  selector: 'app-client-select-encashment',
  templateUrl: './encashment-select-data.component.html',
  styleUrls: ['./encashment-select-data.component.less'],
  providers: [DatePipe]
})
export class EncashmentSelectDataComponent implements OnInit, OnDestroy  {

  @ViewChild('upayArmenianPhonePicker') public upayArmenianPhonePicker;
  encashmentType: string;
  printView = false;
  activeTab = 'out';
  currentDate;
  user;
  userinfoSubscribe = Subscription.EMPTY;
  getCurrentBalance = Subscription.EMPTY;
  getBalanceChange = Subscription.EMPTY;
  cashBoxAvailableMoney: number;
  currentBalance;
  currentItem: any;
  // selectedPhoneNumber = null;


  constructor(private activeRoute: ActivatedRoute,
              private popupsService: PopupsService,
              private dateTimeService: DateTimeService,
              private userDataService: UserDataService,
              private alertBoxService: AlertBoxService,
              private encashmentService: EncashmentService,
              private encashmentPrintDataService: EncashmentPrintDataService,
              private cashboxDataService: CashboxDataService,
              private loadingService: LoadingService,
              private supportService: SupportService,
              private providerService: ProviderService
  ) {
    this.currentDate = this.dateTimeService.convertDate( new Date(), '/');
    // this.userDataService.userInfo.subscribe(res => {
    //   if (res) {
    //     this.getCurrentBalance = this.cashboxDataService.getCurrentBalance({cashboxId: res.cashBoxId}).subscribe(balance => {
    //         this.cashBoxAvailableMoney = balance / 100;
    //     });
    //   }
    // });
  }

  ngOnDestroy() {
    this.userinfoSubscribe.unsubscribe();
    this.getCurrentBalance.unsubscribe();
    this.getBalanceChange.unsubscribe();
  }

  ngOnInit() {


    this.activeRoute.params.subscribe(routeParams => {
      this.encashmentType = routeParams['type'];
      this.activeTab = this.encashmentType === 'ucom' ? 'in' : 'out';
      this.initDetails();

    });
    this.currentDate = this.dateTimeService.convertDate( new Date(), '/');
    this.userinfoSubscribe = this.userDataService.userInfo.subscribe( res => {
      this.getCurrentBalance.unsubscribe();
      if (res) {
        this.getCurrentBalance = this.cashboxDataService.getCurrentBalance({cashboxId: res.cashBoxId}).subscribe(balance => {
          this.cashBoxAvailableMoney = balance / 100;
        });
      }
    });

    this.getBalanceChange = this.cashboxDataService.getBalanceChange().subscribe( newBalance => {
      this.cashBoxAvailableMoney = newBalance / 100;
    });
  }

  initDetails () {
    let typeForReq = '';
    if (this.encashmentType === 'external' || this.encashmentType === 'internal') {
      typeForReq = this.encashmentType + '_' +  this.activeTab;
    } else {
      typeForReq = this.encashmentType;
    }

    this.currentItem = this.encashmentService.getEncashmentsByType(typeForReq.toUpperCase());
    for (let i = 0; i < this.currentItem['fields'].length; i++) {
      this.currentItem['data'].details = this.currentItem['fields'][i]['placeholder'];
    }
  }

  clearData() {
    this.initDetails();
  }

  seActiveTab(index) {
    this.activeTab = index;
    this.clearData();

    // cash in cash out
    // this.setCashinCashoutData(this.customerData);
  }

  onOptionClick(event) {
    this.currentItem['data'].document['type'] = event.id;
  }

  validate() {
    let success = true;
    for (let i = 0; i < this.currentItem['fields'].length; i++) {
      const key = this.currentItem['fields'][i].key;
      const item =  this.currentItem['data'][key];
      if (item === '' || item === 0 || item === undefined) {
        this.alertBoxService.initMsg( {type: 'error', text: 'Please fill ' + key + ' in field'} );
        success = false;
      }
    }
    if (this.currentItem['data'].amount < 1 || this.currentItem['data'].amount.toString().startsWith('0')) {
      this.alertBoxService.initMsg({type: 'error', text: 'Գումար դաշտի արժեքը չի կարող փոքր լինել 1-ից և սկսվել 0-ով:' });
      success = false;
    }
    if (this.currentItem.encashmentType === 'ENCAHSMENT_OUT' && this.currentItem['data'].amount > this.cashBoxAvailableMoney ) {
      this.alertBoxService.initMsg({type: 'error', text: 'Դուք չեք կարող կանխիկացնել ավելի շատ գումար, քան առկա է դրամարկղում:'});
      success = false;
    }
    if (((this.currentItem.type === 'IDRAM') ||
         (this.currentItem.type === 'LOAN_GOOD_CREDIT') ||
         (this.currentItem.type === 'LOAN_GLOBAL_CREDIT') ||
         (this.currentItem.type === 'LOAN_VARKS_AM'))
         && !this.currentItem.data['contactNum']) {
      this.alertBoxService.initMsg({type: 'error', text: 'Նշված հեռախոսի համարը վավեր չէ:'});
      return;
    }
    return success;
  }

  onActionClick() {
    if (!this.validate()) {
      return;
    }
    this.printView = true;

    this.encashmentPrintDataService.collectPrinterData(
      this.currentItem['type'],
      this.currentItem,
      this.currentDate,
      this.activeTab);

    this.clearData();

    this.upayArmenianPhonePicker.reset();

    console.log('ON COMFIRM from select data ', this.currentItem);
  }


  // UCOM cash out
  onCashOut(e) {

    this.currentItem = {
      ...this.currentItem,
      ...e
    };

    this.encashmentPrintDataService.collectPrinterData(
      this.currentItem['type'],
      this.currentItem,
      this.currentDate,
      this.activeTab);
  }

  onPhoneNumberChange(e) {
    // this.selectedPhoneNumber = e;
    if ((this.currentItem.type === 'IDRAM') ||
        (this.currentItem.type === 'LOAN_GOOD_CREDIT') ||
        (this.currentItem.type === 'LOAN_GLOBAL_CREDIT') ||
        (this.currentItem.type === 'LOAN_VARKS_AM')) {
      this.currentItem.data['contactNum'] = e;
    }
  }
}
