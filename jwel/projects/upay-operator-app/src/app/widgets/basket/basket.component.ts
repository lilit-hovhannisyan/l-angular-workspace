/* tslint:disable */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscriber, Subscription} from 'rxjs/index';

import {UserDataService} from '../../dal/user/user-data.service';
import {BasketDataService} from '../../dal/basket/basket-data.service';
import {PopupsService} from '../../popups/popups.service';
import {ProviderDataService} from '../../dal/provider/provider-data.service';

import {
  CalculateCommissionService,
  AlertBoxService,
  DialogService,
  ProviderService,
  SharedPrintService,
  LoadingService,
  splitNumberWithLumas,
  roundDecimalNumberToTwoSymbols
} from 'upay-lib';
import {PrintService} from '../../misc-services/print.service';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';


@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.less']
})
export class BasketComponent implements OnInit, OnDestroy {
  // pos && contact number
  selectedPhoneNumber = null;
  isPosSelect = false;
  posCheckNumber = null;
  allowPosTerminal = null;

  // calculator
  inAmount: number;
  diff;


  // subscriptions
  basketItemsSub = Subscription.EMPTY;

  // table
  lockBtn = 0; // todo lock action ??
  // selectedContractCount;
  isCheckedAll;

  items = [];
  checkedMap;
  topHeader;
  columns;


  splitNumberWithLumas = splitNumberWithLumas;

  constructor(private basket: BasketDataService,
              private popupsService: PopupsService,
              private alertBoxService: AlertBoxService,
              private dialog: DialogService,
              private providerService: ProviderService,
              private cashboxDataService: CashboxDataService,
              private loadingService: LoadingService) {
  }

  ngOnInit() {
    this.loadingService.showLoader(true);

    this.cashboxDataService.getCashboxInfo().subscribe(res => {
      this.allowPosTerminal = res.allowPosTerminal;
    });

    this.basket.getBasketItems();
    this.basketItemsSub = this.basket.basketItems.subscribe(items => {


      this.items = items.map(item => {
        if(this.checkedMap) {
          this.checkedMap.find( i =>  {
            if (i.providerKey === item.providerKey && i.transferMappedData.customerIdKey === item.transferMappedData.customerIdKey) {
              item.checked = i.checked;
            }
          });
        }
        delete item.customClassName;
        delete item.errorMessage;

        const v = this.basket.validateBasketField(item);
        if( !v.isValid ){
          item.errorMessage = v.error;
          item.customClassName = 'basket-error';
        }

        return item;
      });
      this.initBasketList();
      this.loadingService.showLoader(false);
    });

    this.basket.basketItems.next(this.basket.getBasketItems());
  }

  ngOnChanges() {
    alert('change')
    console.log(this.items)
  }

  getTotalAmount(){
    return this.getSelectedContracts().reduce((total, item) => total + +item.amount + +item.commission, 0);
  }

  ngOnDestroy() {
    this.basketItemsSub.unsubscribe();
  }

  onPhoneNumberChange(e){
    this.selectedPhoneNumber = e;
  }

  onPosPaySelect(){
    this.isPosSelect = !this.isPosSelect;
    this.inAmount = this.isPosSelect ? this.getTotalAmount() : null;
    this.posCheckNumber = null;
    this.diff = null;
  }

  onChangePosCheckNumber(e) {
    this.posCheckNumber = e.target.value;
  }

  validateBeforePay() {
    let hasErrors = false;
    if (!this.selectedPhoneNumber){
      this.alertBoxService.initMsg({type: 'error', text: 'error.notValidPhoneNumber'});
      hasErrors = true;
    }

    if(!this.isPosSelect) {
      if (!this.inAmount) {
        this.alertBoxService.initMsg({type: 'error', text: 'error.cashAmount'});
        hasErrors = true;
      }

      if (this.inAmount && this.inAmount <= 0) {
        this.alertBoxService.initMsg({type: 'error', text: 'error.positiveNumber'});
        hasErrors = true;
      }

      if (this.inAmount && this.inAmount < this.getTotalAmount()) {
        this.alertBoxService.initMsg({type: 'error', text: 'error.diffNumber'});
        hasErrors = true;
      }
    }

    if (this.getSelectedContracts().length === 0) {
      this.alertBoxService.initMsg({type: 'error', text: 'error.noSelection'});
      hasErrors = true;
    }

    if (this.isPosSelect && !this.posCheckNumber) {
      this.alertBoxService.initMsg({type: 'error', text: 'մուտքագրեք անկանխիկ անդորագրի համար'});
      hasErrors = true;
    }

    //this.basket.validateBasketField(item => findByTid(getTid(item)).checked);

    if ( this.getSelectedContracts().find(item => item.errorMessage) ) {
      hasErrors = true;
    }

    return hasErrors;
  }

  onPayClick() {
    if ( this.validateBeforePay() ) {
      return;
    }

    const selectedContracts = this.getSelectedContracts();

    if(this.isPosSelect) {
      this.dialog.show('Դուք կատարում եք անկանխիկ գործարք, շարունակե՞լ', () => {
        this.popupsService.openBasketPayPopup({
          list: selectedContracts,
          inAmount: this.getTotalAmount(),
          totalAmount: this.getTotalAmount(),
          inPhoneNumber: this.selectedPhoneNumber,
          isPos: this.isPosSelect,
          posCheckNumber: this.posCheckNumber
        });
      });

      return;
    }

    this.popupsService.openBasketPayPopup({
      list: selectedContracts,
      inAmount: roundDecimalNumberToTwoSymbols(this.inAmount),
      totalAmount: this.getTotalAmount(),
      diff: roundDecimalNumberToTwoSymbols(this.diff),
      inPhoneNumber: this.selectedPhoneNumber,
      isPos: this.isPosSelect,
    });
  }

  onAddToGroup() {
    const selectedContracts = this.getSelectedContracts();
    this.popupsService.openPopUp('addToGroup', selectedContracts);
  }

  // --------- calculator
  calcDiff() {
    if (this.inAmount <= 0) {
      this.alertBoxService.initMsg({type: 'error', text: 'Թույլատրվում է միայն 0-ից մեծ դրական թիվ'});
      this.diff = null;
      return;
    }
    this.diff = ((this.inAmount * 100) - (this.getTotalAmount() * 100)) / 100;
  }

  // -------------- table
  initBasketList() {
    this.topHeader = [
      {
        icon: 'icon_basket',
        title: 'widgets.basket.text.basket',
      },
      {
        title: 'Ջնջել',
        action: {
          key: 'deleteSelectedContractsFromBasket',
          icon: 'icon_delete',
        },
      }
    ];

    this.columns = [
      {
        columnType: 'checkbox', // checkbox, string, editableString, action
        columnHeaderAction: {
          key: 'selectAll',
        },
        columnComponentConfig: {
          actionKey: 'selectRow'
        }
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.transaction',
        columnValueDeepFind: ['providerData.config.displayText']
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.subscriber',
        columnValueDeepFind: ['transferMappedData.customerIdKey'],
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.subscriberInfo',
        columnValueDeepFind: ['transferMappedData.customerNameKey', 'transferMappedData.addressKey'],
        columnValueDeepFindFallBackStr: '-'
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.amount',
        columnValueDeepFind: ['transferMappedData.debtKey']
      },
      {
        columnType: 'string',
        columnHeaderDisplayText: 'widgets.basket.text.commission',
        columnValueDeepFind: ['commission']
      },
      {
        columnType: 'editableString',
        columnValue: 'amount',
        columnHeaderDisplayText: 'widgets.basket.text.edit_amount',
        columnComponentConfig: {
          type: 'number'
        }
      }
    ];

    this.isCheckedAll = true;
  }

  getSelectedContracts() {
    const selectedContractList = [];
    this.items.map( item => {
      // debugger
      if (item.checked) {
        selectedContractList.push(item);
      }
    });

    return selectedContractList;
  }

  headerActionClick(event) {
    if (event.action.key === 'deleteSelectedContractsFromBasket') {
      if (!this.getSelectedContracts().length) {
        this.alertBoxService.initMsg({type: 'error', text: 'error.noSelection'});
        return;
      }

      this.dialog.show('Դուք պատրաստվում եք ջնջել ընտրված տողերը զամբյուղից, շարունակե՞լ', () => {
        this.basket.deleteItems(item => this.basket.findByTid(this.getSelectedContracts(), this.basket.getTid(item)));
        this.isCheckedAll = this.items.length && this.items.length === this.getSelectedContracts().length;
      });
    }
  }

  tableActionClick(event) {
  }

  // edit row
  editRow({ item, index }) {
    // assertDefined(index, 'Index is not specified');
    // debugger
    item.amount = item.amount.replace(/\./g, '');

    if (!item.amount || +item.amount < 0) {
      item.amount = 0;
    }

    if (item.amount.length > 1 && item.amount.charAt(0) === '0') {
      item.amount = item.amount.substring(1);
    }

    const v = this.basket.validateBasketField(item);
    if( !v.isValid ){
      item.errorMessage = v.error;
      item.customClassName = 'basket-error';
    } else {
      delete item.customClassName;
      delete item.errorMessage;
    }

    const newAmount = item.amount;
    const newCommissionValue = this.providerService.calculateProviderCommission(newAmount, item.providerCommissionsRoles);

    // debugger
    this.checkedMap = JSON.parse(JSON.stringify(this.items));
    this.basket.editAmount(index, newAmount, newCommissionValue);

    if (this.inAmount) {
      this.diff = this.inAmount - this.getTotalAmount();
    }
  }
}



