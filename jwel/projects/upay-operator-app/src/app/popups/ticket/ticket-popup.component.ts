import {Component, OnInit, OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {UserDataService} from '../../dal/user/user-data.service';
import {PopupsService} from '../popups.service';
import {DateTimeService} from 'upay-lib';
import {EncashmentService} from '../../dal/encashment/encashment.service';
import {EncashmentViewService} from '../../dal/encashment/encashment-view.service';
import {PrintService} from '../../misc-services/print.service';
import {SharedPrintService, AlertBoxService} from 'upay-lib';
import { DomSanitizer } from '@angular/platform-browser';
import { IfStmt } from '@angular/compiler/src/output/output_ast';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {LoadingService} from 'upay-lib';
import {Subscription, Observable, BehaviorSubject} from 'rxjs/index';
import {SupportService} from '../../dal/support/support.service';


@Component({
  selector: 'app-ticket-popup',
  templateUrl: './ticket-popup.component.html',
  styleUrls: ['./ticket-popup.component.less']
})
export class TicketPopupComponent implements OnInit, OnDestroy {
  action: String;
  cashBox: Subscription;
  currentItem;
  user;
  data;
  uuidv4: string; // todo v1 nayel jnjel
  getOrderNumber: Subscription;
  list: any[];
  node: string;
  printView;
  printCta = false;
  in: boolean;
  branchId;
  getBalanceChange: Subscription;
  cashoutSUB = Subscription.EMPTY;

  updateCashoutBalanceSUB = Subscription.EMPTY;


  constructor(private userDataService: UserDataService,
              private printService: PrintService,
              private sharedPrintService: SharedPrintService,
              private encashmentService: EncashmentService,
              private encashmentViewService: EncashmentViewService,
              private popupsService: PopupsService,
              private cashboxDataService: CashboxDataService,
              public sanitized: DomSanitizer,
              private dateTimeService: DateTimeService,
              private alertBoxService: AlertBoxService,
              private loadingService: LoadingService,
              private supportService: SupportService) {
  }

  ngOnInit() {

    this.userDataService.userInfo.subscribe(res => this.user = res);
    this.currentItem = this.popupsService.activePopupData;

    this.data = this.currentItem.printData;
    this.data.code = this.currentItem.code;


    this.getBalanceChange = this.cashboxDataService.getBalanceChange().subscribe(boolean => {
      if (boolean) {
        // const cashboxId = document.getElementById('cashboxId').textContent; // todo haskanal es incha
      }
    });
    // console.log(1800)
    this.printView = this.encashmentViewService.getView(this.data.type, this.data, this.user);
  }

  initState(chashBoxtype) {
    if (chashBoxtype === 'ENCASHMENT_IN') {
      this.in = true;
    } else {
      this.in = false;
    }
  }

  onComfirmClick() {

    const isUcomWalletOut = this.currentItem.type ===  'UCOM_WALLET_OUT';
    this.currentItem['printData'].userValue = this.data.userValue;
    if (this.currentItem.type.includes('_IN') || this.currentItem.type === 'UCOM') {
      this.in = true;
    } else {
      this.in = false;
    }
    this.getOrderNumber = this.encashmentService.getOrderNumber({
      branchId: this.data.branchId,
      in: this.in
    }).subscribe(order => {
      this.currentItem.data.orderId = order;
      this.currentItem['printData'] = this.data;
      this.currentItem['printData'].orderId = order;
      // console.log('ON COMFIRM ', this.currentItem, this.data);
      this.printView = this.encashmentViewService.getView(this.currentItem.type, this.currentItem.printData, this.user);
      // console.log('ON COMFIRM ', this.currentItem, this.data);
      this.loadingService.showLoader(true);


      if (isUcomWalletOut) {
        const customerId = this.currentItem.ucomWallet;
        const amount = this.currentItem.amount * 100; // convert to lumas
        const balance = this.currentItem.data.balance;
        const contactNumber = this.currentItem.contactNum;

        this.cashoutSUB.unsubscribe();
        this.updateCashoutBalanceSUB.unsubscribe();

        this.supportService.cashoutCustomer(customerId, amount, order, contactNumber);

        this.cashoutSUB = this.supportService.cashoutSub.subscribe(res => {
          if (res['cashoutAmount']) {
            const newBalance = balance - +res['cashoutAmount'] * 100;
            console.log('balance ' + balance);
            this.supportService.updateBalance(newBalance);
            this.cashboxDataService.updateBalance();
            this.supportService.updateUser(customerId);

            this.printCta = !this.printCta;
            this.loadingService.showLoader(false);
          }
        }, error => {
          alert('error');
        });
      } else {
        this.printCta = !this.printCta;



        this.encashmentService.getCashboxRequest(this.currentItem.type, this.currentItem.data).subscribe(
          result => {
            this.loadingService.showLoader(false);
            this.encashmentService.clearFieldsAndUpdateBalance();
          },
          error => {
            this.loadingService.showLoader(false);
            this.encashmentService.clearFieldsAndUpdateBalance();
          });
      }

    });
  }

  ngOnDestroy() {
    if (this.getOrderNumber) {
      this.getOrderNumber.unsubscribe();
    }
    if (this.getBalanceChange) {
      this.getBalanceChange.unsubscribe();
    }
    this.cashoutSUB.unsubscribe();
  }

  onPrintClick() {
    this.popupsService.closeActivePopup();
    this.sharedPrintService.openPrintWindow(this.printView, 'a4', 'landscape');
    this.alertBoxService.initMsg({type: 'success', text: 'Օրդերը հաջողությամբ տպված է'});
  }
}
