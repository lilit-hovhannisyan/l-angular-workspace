import {Component, OnInit} from '@angular/core';
import {SupportService} from '../../dal/support/support.service';
import {AlertBoxService} from 'upay-lib';
import {PopupsService} from '../popups.service';
import {Router} from '@angular/router';
import {root} from '../../dal/site/static-data';

@Component({
  selector: 'app-freeze-account',
  templateUrl: './freeze-account.component.html',
  styleUrls: []
})

export class FreezeAccountComponent implements OnInit {
  freezeAccountText;
  customerId;

  constructor(private supportService: SupportService,
              private alertBoxService: AlertBoxService,
              private popupsService: PopupsService,
              private router: Router) {
  }

  ngOnInit() {
    this.supportService.currentUser.subscribe( user => {
      this.customerId = this.popupsService.activePopupData['customerId'];
    });
  }

  freezeAccount() {
    if ( !this.freezeAccountText) {
     this.alertBoxService.initMsg({type: 'error', text: 'Սառեցնելու հիմք դաշտը պարտադիր է'});
     return;
    }

    this.supportService.freezeAccount(this.customerId , this.freezeAccountText).subscribe( res => {
      this.alertBoxService.initMsg({type: 'success', text: 'Հաշիվը հաջողությամբ սառեցված է'});
      this.popupsService.closeActivePopup();
      this.router.navigate([`/${root}/support/user/customer-list`]);
    });
  }

}
