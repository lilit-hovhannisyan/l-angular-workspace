import {Component, OnInit} from '@angular/core';
import {SupportService} from '../../dal/support/support.service';
import {PopupsService} from '../popups.service';
import {AlertBoxService} from 'upay-lib';

import { v4 as uuid } from 'uuid';
import {Router} from '@angular/router';

@Component({
  selector: 'app-unfreeze-account',
  templateUrl: './unfreeze-account.component.html',
  styleUrls: ['./unfreeze-account.component.less']
})

export class UnfreezeAccountComponent implements OnInit {
  customerId;
  unfreezeApplicationText = '';
  attachment = null;
  files = [];
  groupName;

  constructor(private supportService: SupportService,
              private popupsService: PopupsService,
              private alertBoxService: AlertBoxService,
              private router: Router) {
  }

  ngOnInit() {
    this.customerId = this.popupsService.activePopupData['customerId'];
    if ( !this.groupName ) {
      this.groupName = this.customerId + '-' + uuid();
    }
  }

  attachFile(e) {
    const file = e;
    const fileName = file.name;

    this.supportService.createGroup(this.groupName).subscribe( group => {
      this.supportService.fileUpload(this.groupName, fileName, file).subscribe( res => {
        this.files.push(fileName);
      });
    });
  }

  unfreezeAccount() {
    if ( this.files.length ) {
      this.attachment = this.groupName;
    }
    // todo uncomment when the claim flow will be accepted
    // this.supportService.openUnfreezeAccountClaim(this.customerId, this.unfreezeApplicationText, this.attachment).subscribe( res => {
    //   this.alertBoxService.initMsg({type: 'success', text: 'Ձեր հայտը հաջողուտյամբ ուղարկվել է'});
    //   this.popupsService.closeActivePopup();
    // });

    this.supportService.unfreezeAccount(this.customerId, this.unfreezeApplicationText).subscribe( res => {
      this.alertBoxService.initMsg({type: 'success', text: 'Ձեր հաշիվը հաջողուտյամբ ապասառեցված է'});
      this.router.navigate(['/operator/support/user/customer-list']);
      this.popupsService.closeActivePopup();
    });
  }

}
