import {Component, OnInit} from '@angular/core';
import {SupportService} from '../../dal/support/support.service';
import {AlertBoxService} from 'upay-lib';
import {PopupsService} from '../popups.service';
import {Router} from '@angular/router';
import {root} from '../../dal/site/static-data';

@Component({
  selector: 'app-terminate-account',
  templateUrl: './terminate-account.component.html',
  styleUrls: []
})

export class TerminateAccountComponent implements OnInit {
  terminateAccountText;
  customerId;

  constructor(private supportService: SupportService,
              private alertBoxService: AlertBoxService,
              private popupsService: PopupsService,
              private router: Router) {
  }

  ngOnInit() {
    this.supportService.currentUser.subscribe( user => {
      this.customerId = this.popupsService.activePopupData['customerId'];
    });
  }

  terminateAccount() {
    if ( !this.terminateAccountText) {
     this.alertBoxService.initMsg({type: 'error', text: 'Դադարեցնելու հիմք դաշտը պարտադիր է'});
     return;
    }

    this.supportService.terminateAccount(this.customerId , this.terminateAccountText).subscribe( res => {
      this.alertBoxService.initMsg({type: 'success', text: 'Հաշիվը հաջողությամբ փակված է'});
      this.router.navigate([`/${root}/support/user/customer-list`]);
    });
  }

}
