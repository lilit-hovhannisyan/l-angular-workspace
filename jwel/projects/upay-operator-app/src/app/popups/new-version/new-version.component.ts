import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-new-version',
  templateUrl: './new-version.component.html',
  styleUrls: ['./new-version.component.less'],
})
export class NewVersionComponent {
  onUpdate() {
    location.reload();
  }
}
