import {Component, Input, OnInit} from '@angular/core';
import {PopupsService} from '../popups.service';
import {DomSanitizer} from '@angular/platform-browser';
import {EncashmentProviderService} from '../../dal/encashment/encashment-new/encashment-provider.service';
import {checkIfOneDayIsPassed, dateFormat, LoadingService, SharedPrintService} from 'upay-lib';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-encashment-order',
  templateUrl: './encashment-order.component.html',
  styleUrls: ['./encashment-order.component.less']
})

export class EncashmentOrderComponent implements OnInit {
  html;
  dynamicOrderData;
  encashmentProviderData;
  encahsmentDate;

  printOrder;


  constructor(private popupsService: PopupsService,
              public sanitized: DomSanitizer,
              private encashmentProviderService: EncashmentProviderService,
              private printService: SharedPrintService,
              private cashboxDataService: CashboxDataService,
              private userDataService: UserDataService,
              private loadingService: LoadingService) {

  }

  ngOnInit() {
    this.encashmentProviderData = this.popupsService.activePopupData.encashmentProviderData;
    this.dynamicOrderData = this.popupsService.activePopupData.dynamicOrderData;

    this.encahsmentDate = this.dynamicOrderData.date + '';
    this.dynamicOrderData.date = dateFormat(new Date(this.dynamicOrderData.date), 'dd/mm/yyyy');

    // bind hardcode data
    this.dynamicOrderData = this.encashmentProviderService.getHtmlDynamicFields(this.encashmentProviderData, this.dynamicOrderData);


    this.userDataService.getOperatorById(this.dynamicOrderData.operatorId).subscribe( (operator: any) => {
      this.dynamicOrderData.operator = operator.forename + ' ' + operator.surname;
      this.getHtml();

      this.printOrder = this.dynamicOrderData.printOrderState;
    });
  }

  getHtml() {
    this.html = this.encashmentProviderService.getOrderHtmlByOrderType(
      this.encashmentProviderData.generateOrderHtmlAction.orderType,
      this.dynamicOrderData);

    const oneDayIsPassed = checkIfOneDayIsPassed(this.encahsmentDate);
    if (oneDayIsPassed) {
      this.html += `<div>
                        <img src="/shared-assets/img/copy.png" alt="logo" style="display: block; width: 200px; position: fixed; top: 450px; left: 50%; margin-left: -80px; opacity: .5;">
                    </div>`;
    }
  }

  onSubmit() {
    // get order id number
    this.loadingService.showLoader(true);
    const branchId = this.dynamicOrderData.branchId;
    const cashboxActionDirectionIn = this.encashmentProviderData.getOrderIdAction.directionIn;

    this.encashmentProviderService.getOrderNumber(branchId, cashboxActionDirectionIn).subscribe( orderId => {
      this.dynamicOrderData.orderId = orderId.toString();
      // update html
      this.getHtml();

      // call cashbox action
      this.encashmentProviderService.callCashboxAction(this.encashmentProviderData, this.dynamicOrderData).subscribe( res => {
        // print order
        this.loadingService.showLoader(false);
        this.printOrder = true;
        this.cashboxDataService.updateBalance();

        // drop data
        this.encashmentProviderService.refreshEncashmentData();
      });
    });
  }

  onPrint() {
    this.printService.openPrintWindow(this.html, 'a4', 'landscape');
    this.popupsService.closeActivePopup();
  }
}
