import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class PopupsService {

  public Popups = {
    none: '',
    login: 'login',
    forgot: 'forgot',
    addToGroup: 'addToGroup',
    basketPay: 'basketPay',
    ticket: 'ticket',
    printInvoice: 'printInvoice',
    unFreezeAccount: 'unFreezeAccount',
    terminateAccount: 'terminateAccount',
    freezeAccount: 'freezeAccount',
    newVersion: 'newVersion',
    editGroup: 'editGroup',
    invoice: 'invoice',
    encashmentOrder: 'encashmentOrder'
  };

  public activePopup = new BehaviorSubject(this.Popups.none);
  public activePopupData: any = null;

  constructor() {
  }

  public closeActivePopup() {
    this.activePopupData = null;
    this.activePopup.next(this.Popups.none);
  }

  public openPopUp(popupName, data?: any) {
    this.setActivePopup(this.Popups[popupName]);
    this.activePopupData = data;
  }

  public openEncasmentionPopUp(data) {

    this.setActivePopup(this.Popups.ticket);
    this.activePopupData = data;
  }

  public openBasketPayPopup(data) {
    this.setActivePopup(this.Popups.basketPay);
    this.activePopupData = data;
  }

  private setActivePopup(popupName) {
    this.closeActivePopup();
    this.activePopup.next(popupName);
  }
}
