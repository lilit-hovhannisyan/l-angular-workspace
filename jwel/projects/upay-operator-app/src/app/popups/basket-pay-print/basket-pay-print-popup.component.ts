import {Component, OnInit, OnDestroy} from '@angular/core';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {PopupsService} from '../popups.service';
import {LoadingService, ProviderService, SharedPrintService} from 'upay-lib';
import {PrintService} from '../../misc-services/print.service';
import {UserDataService} from '../../dal/user/user-data.service';
import {Subscription} from 'rxjs/index';

import { v4 as uuid } from 'uuid';

import {SubscriberDataService} from '../../dal/subscriber/subscriber-data.service';
import {BasketDataService} from '../../dal/basket/basket-data.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-basket-pay-popup',
  templateUrl: './basket-pay-print-popup.component.html',
  styleUrls: ['./basket-pay-print-popup.component.less']
})
export class BasketPayPopupComponent implements OnInit {
  curDate;
  curTime;
  activePrintBtn = false;
  ctaBtn = true;
  selectedList = [];
  failedPayments = [];
  succesedPayments = [];
  totalAmount = 0;
  inPhoneNumber = null;
  isPos = false;
  posCheckNumber = null;
  inAmount = 0;
  diff = 0;
  returnAmount = 0;

  branch = null;
  operator = null;

  basketStateSub = Subscription.EMPTY;
  payFromBasketListSub = Subscription.EMPTY;
  branchSub = Subscription.EMPTY;
  operatorSub = Subscription.EMPTY;

  constructor(private cashboxDataService: CashboxDataService,
              private popupsService: PopupsService,
              private basketDataService: BasketDataService,
              private loadingService: LoadingService,
              private printService: PrintService,
              private userDataService: UserDataService,
              private translateService: TranslateService,
              private providerService: ProviderService,
              private sharedPrintService: SharedPrintService) {
  }

  ngOnInit() {
    this.selectedList = this.popupsService.activePopupData.list;


    this.inAmount = this.popupsService.activePopupData.inAmount;
    this.inPhoneNumber = this.popupsService.activePopupData.inPhoneNumber;
    this.isPos = this.popupsService.activePopupData.isPos;
    this.posCheckNumber = this.popupsService.activePopupData.posCheckNumber;
    this.totalAmount = this.popupsService.activePopupData.totalAmount;
    this.diff = this.popupsService.activePopupData.diff;

    // console.log('selectedList', this.selectedList);

    this.branchSub = this.userDataService.branchInfo.subscribe( branch => {
      this.branch = {
        phone:  '011 444 448', // 'company.phone',
        address: branch.address
      };
    });

    this.operatorSub = this.userDataService.userInfo.subscribe( user => {
      this.operator = {
        fullName: `${user.forename} ${user.surname}`
      };
    });
  }

  onDestroy() {
    this.basketStateSub.unsubscribe();
    this.payFromBasketListSub.unsubscribe();
    this.operatorSub.unsubscribe();
    this.branchSub.unsubscribe();
  }

  onPayClick() {
    this.loadingService.showLoader(true);

    // update cashBox history for BULK pay
    const uuidCashbox = uuid();

    this.payFromBasketListSub.unsubscribe();
    // todo v1 avelacnel this.payFromBasketListSub = this.basketDataService.payFromBasketList(this.selectedList, uuidCashbox, this.inAmount, this.inPhoneNumber, this.isPos).subscribe(isFinished => {
    this.payFromBasketListSub = this.basketDataService.payFromBasketList(
      this.selectedList,
      uuidCashbox,
      this.inAmount,
      this.inPhoneNumber,
      this.isPos,
      this.posCheckNumber,
      this.branch,
      this.operator).subscribe(isFinished => {
      if (isFinished) {
        this.basketStateSub.unsubscribe();
        this.basketStateSub = this.basketDataService.basketPayState.subscribe(res => {
          if (res.state) {
            this.returnAmount = 0;

            this.succesedPayments = res.succesedPayments;
            this.selectedList = res.basketItemList;

            this.failedPayments = res.failedPayments;

            console.log('component failedPayments', res.failedPayments);

            if ( res.failedPayments.length > 0 ) {
              this.failedPayments.map(i => {
                this.returnAmount = +this.returnAmount + +i.amount + +i.commission;
              });
            }

            // and show print btn
            this.ctaBtn = true;
            this.activePrintBtn = true;
            // this.inAmount = null;
            // this.inPhoneNumber = null;
            // this.isPos = false;

            // update balance
            this.cashboxDataService.updateBalance();
          }
        });
      }
    });
  }

  getTotalForItem(item) {
    return +item.amount + +item.commission;
  }

  onPrintClick() {
    let invoiceListHtml = '';
    this.succesedPayments.map( (i, key, index) => {

      const breakLine = `<div style="width: 100%; border-bottom: 3px dotted black;"></div>`;
      /* if( this.succesedPayments.length - 1 === index) {
        breakLine = '';
      } */

      const companyData = {companyName: 'company.name', companyTin: 'company.tin'};
      const branchData = i.branchData;
      const operatorData = i.operatorData;
      // i.customerDebtData, i.amount, i.providerData, i.commission, i.receiptData, companyData, branchData, operatorData
      const invoiceHtml = this.providerService.getProviderInvoiceHtml({
        debtData: i.customerDebtData,
        amount: i.amount,
        provider: i.providerData,
        commissionValue: i.commission,
        receiptData: i.receiptData,
        companyData: companyData,
        branchInfo: branchData,
        operatorInfo: operatorData,
        imputedPayNode: i.imputedPayNode
      });
      invoiceListHtml += invoiceHtml;

        //
        // `<div>
        //                      ${invoiceHtml}
        //                      ${breakLine}
        //                  </div>`;
    });

    this.sharedPrintService.openPrintWindow( invoiceListHtml, 'termo');
    this.popupsService.closeActivePopup();
  }
}
