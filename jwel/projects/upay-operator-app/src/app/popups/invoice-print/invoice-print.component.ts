import {Component, Input, OnInit} from '@angular/core';
import {PopupsService} from '../popups.service';

@Component({
  selector: 'app-invoice-print',
  templateUrl: 'invoice-print.component.html',
  styleUrls: ['invoice-print.component.less']
})

export class InvoicePrintComponent implements OnInit {
  @Input() debtData = null;
  @Input() amount = null;
  @Input() providerData = null;
  @Input() commission = null;
  @Input() receiptData = null;
  @Input() companyData = null;

  constructor(private popupsService: PopupsService) {
  }

  ngOnInit() {

    this.debtData = this.popupsService.activePopupData.debtData;
    this.amount = this.popupsService.activePopupData.amount;
    this.providerData = this.popupsService.activePopupData.providerData;
    this.commission = this.popupsService.activePopupData.commission;
    this.receiptData = this.popupsService.activePopupData.receiptData;
    this.companyData = this.popupsService.activePopupData.companyData;
  }


  onPrintClick() {
    window.print();
  }
}
