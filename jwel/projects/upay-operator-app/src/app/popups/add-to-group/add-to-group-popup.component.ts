import {Component, OnInit, OnDestroy} from '@angular/core';
import {GroupsService} from '../../dal/groups/groups.service';
import {PopupsService} from '../popups.service';

import {SynchronizedCallsService, AlertBoxService} from 'upay-lib';
import {Observable, Subscription} from 'rxjs/index';
import {ProviderService} from 'upay-lib';

@Component({
  selector: 'app-add-to-group-popup',
  templateUrl: './add-to-group-popup.component.html'
})
export class AddToGroupPopupComponent implements OnInit, OnDestroy {
  groupName: string;
  groupDescription: string;
  data;

  createGroupSub = Subscription.EMPTY;

  constructor(private groupsService: GroupsService,
              private popupsService: PopupsService,
              private alertBoxService: AlertBoxService,
              private synchronizedCallsService: SynchronizedCallsService,
              private providerService: ProviderService) {
  }

  ngOnInit() {
    this.data = this.popupsService.activePopupData;
    console.log(this.data);
  }

  ngOnDestroy() {
    this.createGroupSub.unsubscribe();
  }

  onChange(event, model) {
    this[model] = event;
  }

  onPhoneNumberChange(e) {
    this.groupName = e;
  }

  addToGroup() {
    if (!this.groupName) {
      this.alertBoxService.initMsg({type: 'error', text: 'Խմբի անունը չի կարող դատարկ լինել'});
      return;
    }

    // const phoneNumberRegexp = '\(?([0][1-9][0-9])\)?([0-9]{6})$';
    // if (!this.groupName.match(/\(?([0][1-9][0-9])\)?([0-9]{6})$/g) || this.groupName.length > 9 ) {
    //   this.alertBoxService.initMsg({type: 'error', text: 'Խմբի անունը պետք է լինի 0xx xxx xxx ֆորմատով'});
    //   return;
    // }

    this.createGroupSub.unsubscribe();
    this.createGroupSub = this.groupsService.createGroup(this.groupName, this.groupDescription).subscribe(res => {
      this.alertBoxService.initMsg({type: 'success', text: 'Խումբը հաջողությամբ ստեղծվել է'});

      const contracts = this.data.map( item => {

        const searchQuery = this.providerService.getProviderDebtSearchQuery(item.providerData.config, item.customerDebtData);
        const contract = {
          subscriber: item.transferMappedData.customerIdKey,
          // branchId: '', // todo Tikon nayi ira api-i hamar es toxy petqa te che
          searchData: {
            ...searchQuery,
            providerName: item.providerData.config.providerKey
          },
          providerId: item.providerData.id
        };
        return contract;

      });

      const op = (contract) => this.groupsService.addContractToGroup(contract, this.groupName);

      const errors = [];

      const onComplete = () => {
        // all items are added
        if ( !errors.length ) {
          this.alertBoxService.initMsg({
            type: 'success',
            text: 'Նշված ծառայությունները հաջողությամբ ավելացվել են խումբ'});
          this.popupsService.closeActivePopup();
        } else {
          errors.map(c => { alert('error'); });
        }
      };

      const onError = (err, contract) => {
        errors.push(contract);
      };

      this.synchronizedCallsService.performSequentally(contracts, op, onComplete, onError);
    });
  }
}
