import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AtomsModules, MoleculesModule} from 'iu-ui-lib';

import { PopupsService } from './popups.service';
import { PopupManagerComponent } from './popup-manager/popup-manager.component';


import { AddToGroupPopupComponent } from './add-to-group/add-to-group-popup.component';
import { BasketPayPopupComponent } from './basket-pay-print/basket-pay-print-popup.component';
import { TicketPopupComponent } from './ticket/ticket-popup.component';

import {UnfreezeAccountComponent} from './unfreeze-account/unfreeze-account.component';
import {TerminateAccountComponent} from './terminate-account/terminate-account.component';
import {NewVersionComponent} from './new-version/new-version.component';
import {SharedWidgetsModule} from 'upay-lib';
import {EditGroupPopupComponent} from './edit-group/edit-group-popup.component';
import {TranslateModule} from '@ngx-translate/core';
import {InvoicePrintComponent} from './invoice-print/invoice-print.component';
import {ProviderTerminalModule} from 'upay-lib';
import {FreezeAccountComponent} from './freeze-account/freeze-account.component';
import {EncashmentOrderComponent} from './encashment-order/encashment-order.component';

@NgModule({
  declarations: [
    PopupManagerComponent,

    AddToGroupPopupComponent,
    BasketPayPopupComponent,

    TicketPopupComponent,
    UnfreezeAccountComponent,
    TerminateAccountComponent,
    FreezeAccountComponent,
    NewVersionComponent,
    EditGroupPopupComponent,
    InvoicePrintComponent,
    EncashmentOrderComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    FormsModule,
    SharedWidgetsModule,
    ProviderTerminalModule
  ],
  exports: [
    PopupManagerComponent
  ],
  entryComponents: [
    TicketPopupComponent
  ],
  providers: [
    PopupsService,
  ],
})
export class PopupsModule { }
