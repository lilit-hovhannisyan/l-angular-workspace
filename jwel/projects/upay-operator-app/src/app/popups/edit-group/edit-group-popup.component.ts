import {Component, OnInit, OnDestroy} from '@angular/core';
import {GroupsService} from '../../dal/groups/groups.service';
import {PopupsService} from '../popups.service';

import {SynchronizedCallsService, AlertBoxService} from 'upay-lib';
import {Observable, Subscription} from 'rxjs/index';

@Component({
  selector: 'app-edit-group-popup',
  templateUrl: './edit-group-popup.component.html'
})
export class EditGroupPopupComponent implements OnInit, OnDestroy {
  groupName: string;
  groupId: string;
  data;
  lastGroupName: string;

  editGroupSub = Subscription.EMPTY;

  constructor(private groupsService: GroupsService,
              private popupsService: PopupsService,
              private alertBoxService: AlertBoxService) {
  }

  ngOnInit() {
    this.data = this.popupsService.activePopupData;
    this.lastGroupName = this.data.name;
  }

  ngOnDestroy() {
    this.editGroupSub.unsubscribe();
  }

  onChange(event, model) {
    this[model] = event;
  }

  onPhoneNumberChange(e) {
    this.groupName = e;
  }

  editGroup() {
    if (!this.groupName) {
      this.alertBoxService.initMsg({type: 'error', text: 'Խմբի անունը չի կարող դատարկ լինել'});
      return;
    }

    this.editGroupSub.unsubscribe();
    this.editGroupSub = this.groupsService.editGroup(this.groupName, this.data.groupId).subscribe(res => {
      this.alertBoxService.initMsg({type: 'success', text: 'Խումբը հաջողությամբ անվանափոխվել է'});
      this.popupsService.closeActivePopup();
      this.groupsService.updateGroupName(this.groupName);
    });
  }
}
