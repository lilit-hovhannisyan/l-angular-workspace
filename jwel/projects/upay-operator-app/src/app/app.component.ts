import {Component, OnInit} from '@angular/core';
import {VersionCheckService} from './dal/site/version-check.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit {
  // constructor(private versionCheckService: VersionCheckService){}

  ngOnInit() {
    // this.versionCheckService.initVersionCheck('../version.json');
  }
}
