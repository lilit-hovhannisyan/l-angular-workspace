import {Injectable} from '@angular/core';

import {SharedPrintService} from 'upay-lib';
import {UserDataService} from './../dal/user/user-data.service';
import {EncashmentViewService } from './../dal/encashment/encashment-view.service';

@Injectable()
export class PrintService {
  user;

  constructor(private userDataService: UserDataService,
              private encashmentViewService: EncashmentViewService,
              private sharedPrintService: SharedPrintService) {
              this.user = this.userDataService.userInfo;
              }

  createInvoice(list) {
    const USER = this.user._value.forename + ' ' + this.user._value.surname;
    this.sharedPrintService.createInvoice(list, USER);
  }

  // a4Printer(content) {
  //   const mywindow = window.open('', 'PRINT');
  //
  //   mywindow.document.write('<html><head><title>' + 'upay' + '</title><style type="text/css" media="print">  @page { size: landscape; } </style>');
  //   mywindow.document.write('</head><body style="margin: 0; pading: 0; width: 100%;"></body>');
  //
  //   mywindow.document.write(content);
  //
  //   mywindow.document.write('</body></html>');
  //
  //   mywindow.document.close(); // necessary for IE >= 10
  //   mywindow.focus(); // necessary for IE >= 10*/
  //
  //   mywindow.onload = function () {
  //     mywindow.print();
  //     mywindow.close();
  //   };
  //
  // }
  //
  //
  // openPrintWindow(content) {
  //   const mywindow = window.open('', 'PRINT', 'height=400,width=600');
  //
  //   mywindow.document.write('<html><head><title>' + 'upay' + '</title>');
  //   mywindow.document.write('</head><body style="margin: 0; pading: 0; width: 100%;"></body>');
  //
  //   mywindow.document.write(content);
  //
  //   mywindow.document.write('</body></html>');
  //
  //   mywindow.document.close(); // necessary for IE >= 10
  //   mywindow.focus(); // necessary for IE >= 10*/
  //
  //   mywindow.onload = function () {
  //     mywindow.print();
  //     mywindow.close();
  //   };
  //
  // }
}
