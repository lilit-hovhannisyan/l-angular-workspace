import {Component, OnInit} from '@angular/core';
import {VersionCheckService} from '../../dal/site/version-check.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit {
  constructor(private versionCheckService: VersionCheckService) {}

  ngOnInit() {
    if (environment.versionCheck) {
      this.versionCheckService.initVersionCheck('./version.json', 3000);
    }
  }
}
