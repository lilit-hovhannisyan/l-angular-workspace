import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../dal/language.service';
import {UserDataService} from '../../dal/user/user-data.service';

@Component({
  selector: 'app-language-picker',
  templateUrl: './language-picker.component.html',
  styleUrls: ['./language-picker.component.less']
})
export class LanguagePickerComponent implements OnInit {

  public supportedLanguages = LanguageService.supportedLanguages;
  public selectedLanguage: string;

  constructor(private languageService: LanguageService,
              private userDataService: UserDataService) {}

  ngOnInit() {
    this.selectedLanguage = this.languageService.selectedLanguage;
  }

  onLanguageChange(e) {
    this.selectedLanguage = e.target.innerText;
    this.languageService.changeLanguage(e.target.innerText);
    this.userDataService.setUserLanguage(e.target.innerText);
  }
}
