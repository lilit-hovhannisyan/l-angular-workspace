import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

// lib
import {AtomsModules, MoleculesModule} from 'iu-ui-lib';
import {SharedWidgetsModule} from 'upay-lib';


// modules
import { PopupsModule } from '../popups/popups.module';
import { WidgetsModule } from '../widgets/widgets.module';

// components
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HeaderComponent } from './header/header.component';
import { LanguagePickerComponent } from './language-picker/language-picker.component';

@NgModule ({
  declarations: [
    MainLayoutComponent,
    HeaderComponent,
    LanguagePickerComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    PopupsModule,
    SharedWidgetsModule,
    WidgetsModule
  ],
  exports: [
    MainLayoutComponent,
    HeaderComponent,
    LanguagePickerComponent
  ],
  providers: []
})

export class CoreModule { }
