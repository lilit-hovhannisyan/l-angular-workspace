import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';

import {Router} from '@angular/router';
import {PopupsService} from '../../popups/popups.service';
import {Subscription} from 'rxjs/index';
import {UserDataService} from '../../dal/user/user-data.service';
import {BasketDataService} from '../../dal/basket/basket-data.service';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import {splitNumberWithLumas} from 'upay-lib';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class HeaderComponent implements OnInit, OnDestroy, AfterViewInit {
  constructor(private router: Router,
              private popupService:  PopupsService,
              private userDataService: UserDataService,
              private basketDataService: BasketDataService,
              private cashboxDataService: CashboxDataService) {}

  navigation;
  navigationNotAuth =  [];
  navigationIsAuth = {
    leftNav: [
      {
        path: '/operator/transactions/',
        text: 'navigation.header.transactions'
      },
      // {
      //   path: '/operator/support',
      //   text: 'navigation.header.support'
      // },
      {
        path: '/operator/cashbox',
        text: 'navigation.header.cashbox'
      },
      {
        path: '/operator/transaction-log',
        text: 'navigation.header.log'
      }
    ],
    rightNav: {
      balance: {
        icon: 'icon_dram',
        path: '/operator/cashbox/balance',
        text: 'Balance',
        id: 'balance',
        counter: '0'
      },
      basket: {
        icon: 'icon_basket',
        path: '/operator/basket',
        text: 'Basket',
        id: 'basketCount',
        counter: '0'
      }
    }
  };

  balance;
  basketCount = this.basketDataService.getBasketItemsCount();

  isAuth: boolean;
  isAuthSub = Subscription.EMPTY;


  ngOnInit() {
    this.isAuthSub = this.userDataService.isAuth.subscribe(auth => {
      this.isAuth = auth;
      if (this.isAuth) {
        this.navigation = this.navigationIsAuth;
      } else {
        this.navigation = this.navigationNotAuth;
      }
    });

    // innit balance
    this.userDataService.userInfo.subscribe(res => {
      if (res) {
        this.cashboxDataService.getCurrentBalance({cashboxId: res.cashBoxId}).subscribe(balance => {
          this.balance = splitNumberWithLumas(balance / 100);
        });
      }
    });
  }

  ngOnDestroy(): void {
  }

  ngAfterViewInit() {
    // observe counter change
    this.basketDataService.getCounter().subscribe( basketCount => {
      this.basketCount = basketCount;
    });

    // observe balance change
    this.cashboxDataService.getBalanceChange().subscribe( newBalance => {
      this.balance = newBalance / 100;
    });
  }

  openPopup( popup ) {
    this.popupService.openPopUp( popup );
  }
}
