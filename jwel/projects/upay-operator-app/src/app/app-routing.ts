import { Routes } from '@angular/router';
import { MainLayoutComponent } from './core/main-layout/main-layout.component';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {NoAuthGuardService} from './core/guards/no-auth-guard.service';

export const appRoutes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        canActivate: [NoAuthGuardService],
        loadChildren: './pages/home-page/home-page.module#HomePageModule',
      },
      {
        path: 'operator',
        canActivate: [AuthGuardService],
        loadChildren: './pages/operator-root-page/operator-root-page.module#OperatorRootPageModule'
      },
    ]
  },
  {
    path: 'page-not-found',
    loadChildren: './pages/not-found-page/not-found-page.module#NotFoundPageModule'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/page-not-found'
  }
];

