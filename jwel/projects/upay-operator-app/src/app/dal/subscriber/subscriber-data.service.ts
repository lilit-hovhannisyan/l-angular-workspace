import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {DebtDetailData, SubscriberData, SubscriberDetailData} from 'upay-lib';
import {OfficeApi, ProviderData} from 'upay-lib';
// import {ProviderData} from ;
// import {ProviderData} from "../provider/provider-data.models";


// todo texapoxel shared code
const objectCleanNull = (obj) => {
  const returnObject = {...obj};
  for (const propName in returnObject) {
    if (returnObject[propName] === null || returnObject[propName] === undefined || returnObject[propName].toString() === '') {
      delete returnObject[propName];
    }
  }
  return returnObject;
};
// todo end

@Injectable()
export class SubscriberDataService {

  constructor(private officeApi: OfficeApi) {

  }

  getSubscriberDept(subscriberData: any, providerId: string): Observable<SubscriberData> {
    const subscriberDept = new Subject<SubscriberData>();
    ProviderData.getConfig(providerId, this.officeApi)
      .getSubscriberInfoAction(this.officeApi, subscriberData, subscriberDept);
    return subscriberDept;
  }

  paySubscriberService(subscriberData: any, providerId: string, amount: number, commission: number, infoSource: any, invoiceData?: any, contactNumber?: string, isPos?: boolean) {
    invoiceData = JSON.stringify(invoiceData);
    return ProviderData.getConfig(providerId, this.officeApi)
      .paySubscriberServiceAction(this.officeApi, {
          ...objectCleanNull(subscriberData),
          amount: amount,
          commission: commission,
          clientData: invoiceData,
          contactNum: contactNumber,
          cash: !isPos,
        },
        objectCleanNull(infoSource)
      );
  }

}
