
export class BasketStorage {

  static readonly KEY = '_basketItems';
  static readonly ID = '_basketId';
  subscribers = [];

  getId() {
    return localStorage.getItem(BasketStorage.ID);
  }

  setId(id) {
    localStorage.setItem(BasketStorage.ID, id);
  }

  add(item) {
    const items = this.getAll();
    items.push(item);
    this.save(items);
  }

  getAll() {
    try {
      return JSON.parse(localStorage.getItem(BasketStorage.KEY)) || [];
    } catch ( e ) {
      this.save([]);
      return [];
    }
  }

  getAt(index) {
    return this.getAll()[index];
  }

  length() {
    return this.getAll().length;
  }

  remove(pred) {
    this.save(this.getAll().filter(e => !pred(e)));
  }

  removeAt(index) {
    this.save(this.getAll().filter((e, i) => i !== index));
  }

  clear() {
    this.save([]);
  }

  findMany(pred) {
    return this.getAll().filter(e => pred(e));
  }

  findOne(pred) {
    return this.getAll().find(e => pred(e));
  }

  updateAt(index, fields) {
    const items = this.getAll();
    Object.assign(items[index], fields);
    this.save(items);
  }

  contains(pred) {
    return !!this.findOne(e => pred(e));
  }

  onChange(cb) {
    this.subscribers.push(cb);
  }

  private save(items) {
    // debugger
    localStorage.setItem(BasketStorage.KEY, JSON.stringify(items));
    this.dispatch(items);
  }

  private dispatch(items) {
    this.subscribers.forEach(cb => {
      try {
        cb(items);
      } catch (e) {
      }
    });
  }
}
