import {Injectable, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs/index';
import {OfficeApi, LoadingService, ProviderService} from 'upay-lib';
import {SubscriberDataService} from '../../dal/subscriber/subscriber-data.service';
import {CashboxDataService} from '../../dal/cashbox/cashbox-data.service';
import { v4 as uuid } from 'uuid';
import {TranslateService} from '@ngx-translate/core';
import {BasketStorage} from './basket-storage';

@Injectable()
export class BasketDataService {
  _basket = new BasketStorage();

  basketItems = new BehaviorSubject([]);

  _basketPayState = {state: false, failedPayments: [], succesedPayments: [], basketItemList: []};
  basketPayState = new BehaviorSubject(this._basketPayState);

  @Output() counter = new EventEmitter();

  constructor(private cashboxDataService: CashboxDataService,
              private loadingService: LoadingService,
              private officeApi: OfficeApi,
              private providerService: ProviderService) {
    this._basket.onChange((l) => {
      this.basketItems.next(l);
      this.updateCounter();
    });
  }

  // // commands
  // add(item) {
  // }
  //
  // editAmount(index, amount) {
  // }
  //
  // remove(index) {
  // }
  //
  // enable(index) {
  // }
  //
  // disable(index) {
  // }
  //
  // pay({ inAmount }) {
  // }
  //
  // // queries
  // items({ selected = false }) {
  // }
  //
  // size({ selected = false }) {
  // }
  //
  // amount({ selected = false }) {
  // }
  //
  // validate({ selected = false }) {
  //   return [{index: 0, }];
  // }

  getTid = item => item.customerDebtData.transactionId;
  findByTid = ( list, tid ) => list.find(item => this.getTid(item) === tid);

  getBasketItems() { return this._basket.getAll(); }

  getBasketItemsCount() { return this.getBasketItems().length; }

  isItemInBasket(subscriberId: string, providerId: string) {
    return this._basket.contains(i => subscriberId === i.transferMappedData.customerIdKey && providerId === i.providerKey);
  }

  addToBasket(item) {
    item.paymentState = 'widgets.basket.paymentStates.paymentStatePending';

    // if ( this._basket.length() === 0 ) {
    //   this._basket.setId(uuid());
    // }

    item.customerDebtData.transactionId = uuid();
    this._basket.add(item);
  }

  deleteItems(pred) {
    this._basket.remove(pred);
  }

  deleteAllFromBasket() { // TODO: clear
    this._basket.clear();
  }

  editAmount(index: number, amount: number, commission: number) {
    this._basket.updateAt(index, {amount: amount, commission: commission});
  }

  validateBasketField(item) { // TODO: validateItem
    return this.providerService.validateAmount(item.providerData, item.amount, item.customerDebtData);
  }

  removeList(list) {
    this._basket.remove(item => this.findByTid(list, this.getTid(item)));
  }

  payFromBasketList(basketItemList, uuidCashbox, inAmount, contactNumber, isPos, posCheckNumber, branch, operator) {
    this._basket.setId(uuid());

    const sub = new Subject();

    let failedPayments = [];
    failedPayments =  basketItemList.map((b, index) => {
      return {
        item: b,
        index: index
      };
    }); // failedPayments.concat(basketItemList);
    const succesedPayments = [];
    let succesedPaymentsTotalAmount = 0;

    let count = basketItemList.length;

    basketItemList.map((i, index) => {
      i.paymentState = 'widgets.basket.paymentStates.paymentStateError';

      const payActionMethodName = i.providerData.config.payAction.apiActionName ;

      const invoiceData = {
        amount: i.amount,
        commission: i.commission,
        // inPhoneNumber: , isPos
        customerDebtData: i.customerDebtData,
        providerCommissionsRoles: i.providerCommissionsRoles,
        providerData: i.providerData,
        providerKey: i.providerKey,
        imputedPayNode: i.imputedPayNode,
        branch,
        operator
      };

      // debugger
      // todo v1 transactionId tanel provider terminali mej
      const transactionId = i.customerDebtData.transactionId;
      const requestDta = {
        ...i.customerDebtData,
        paymentId: this._basket.getId(), // uuidCashbox,
        transactionId: transactionId,
        amount: i.amount,
        contactNum: contactNumber,
        cash: !isPos,
        posAuthcode: posCheckNumber,
        commission: i.commission,
        clientData: JSON.stringify(invoiceData)
      };


      this.officeApi[payActionMethodName](requestDta).subscribe(data => {
        const getTransactionSub = this.cashboxDataService
          .getTransaction(transactionId)
          .subscribe(r => {
            if (r) {
              i.paymentState = 'widgets.basket.paymentStates.paymentStateSuccess';
              i.receiptData = r;
              i.branchData = branch;
              i.operatorData = operator;

              failedPayments = failedPayments.filter(b => {
                return b.index !== index;
              });

              succesedPayments.push(i);
              succesedPaymentsTotalAmount = +succesedPaymentsTotalAmount + +i.amount + +i.commission;

              count = count - 1;

              if (count === 0) {
                // // reset basket id
                // this._basket.setId(uuid());

                //
                const basketItemsSelected = {};
                basketItemsSelected['cashboxPaymentId'] =  this._basket.getId();
                basketItemsSelected['totalAmount'] = succesedPaymentsTotalAmount;
                basketItemsSelected['serviceQuantity'] = succesedPayments.length;
                basketItemsSelected['inAmount'] = inAmount;

                const passInfoFromBasketSub = this.cashboxDataService
                  .passInfoFromBasketForTransactionLog(basketItemsSelected, isPos)
                  .subscribe(res => {
                    // empty basket

                    this.removeList(succesedPayments);

                    // get balance for header
                    this.cashboxDataService.updateBalance();

                    // remove loading
                    this.loadingService.showLoader(false);

                    this._basketPayState = {
                      state: true,
                      failedPayments: failedPayments.map(b => {
                        return b.item;
                      }),
                      succesedPayments: succesedPayments,
                      basketItemList: basketItemList
                    };
                    this.basketPayState.next(this._basketPayState);

                    passInfoFromBasketSub.unsubscribe();
                    sub.next(true);
                  }, err => {
                    console.log(err);
                  });
              }

              getTransactionSub.unsubscribe();
            }

          }, (err) => {
            count = count - 1;
            console.log('getTransaction error', err);
          });
      }, error => {
        count = count - 1;

        this.removeList(succesedPayments);

        // get balance for header
        this.cashboxDataService.updateBalance();

        // remove loading
        this.loadingService.showLoader(false);

        this._basketPayState = {
          state: true,
          failedPayments: failedPayments.map(b => {
            return b.item;
          }),
          succesedPayments: succesedPayments,
          basketItemList: basketItemList
        };
        this.basketPayState.next(this._basketPayState);

        sub.next(true);
      });
    });

    return sub;
  }

  // update counter
  updateCounter() {
    this.counter.emit(this.getBasketItemsCount());
  }
  getCounter() {
    return this.counter;
  }
}
