import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
// import {ProviderCategoryData, ProviderData} from './provider-data.models';
import {map} from 'rxjs/internal/operators';
// import {
//   AdminGetProvidersQuery,
//   OfficeApi,
//   OperatorGetProviderCommissionsQuery,
//   OperatorGetProviderDescriptorsQuery,
//   Order,
//   Page
// } from '../../api/index';
import {
  AdminGetProvidersQuery,
  OfficeApi, OperatorGetProviderCommissionsQuery, OperatorGetProviderDescriptorsQuery, Order, Page,
  ProviderCategoryData,
  ProviderData
} from 'upay-lib';


export const transferServiceCategory = [
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_my-mobile',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
];

export const topupServiceCategory = [
  {
    text: 'Internet',
    iconClass: 'icon_internet',
    name: 'internet',
    services: []
  },
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_my-mobile',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
];

// todo end

@Injectable()
export class ProviderDataService {

  constructor(private officeApi: OfficeApi) {
  }

  _providersCategoryTree: ProviderCategoryData[] = [];
  providersCategoryTree = new BehaviorSubject(this._providersCategoryTree);
  _providersList: ProviderData[] = [];
  providersList = new BehaviorSubject(this._providersList);

  getProviderList() {
    const page = new Page();
    page.size = 100000; // todo change api
    page.index = 0;
    const order = new Order();

    const getProvidersQuery = new AdminGetProvidersQuery();
    // getProvidersQuery.name = '';
    getProvidersQuery.enabled = true;
    // getProvidersQuery.hasFixedAmount = false;
    // getProvidersQuery.label = '';
    getProvidersQuery.order = order;
    getProvidersQuery.page = page;

    return this.officeApi.getProviders(getProvidersQuery);
  }

  getPayProvidersCategoryTree() {
    if (!this._providersCategoryTree.length) {
      const page = new Page();
      page.size = 100000; // todo change api
      page.index = 0;
      const order = new Order();
      this.officeApi.getProviderLabels({page: page, order: order, name: ''})
        .subscribe(categories => {
          const getProvidersQuery = new AdminGetProvidersQuery();
          // getProvidersQuery.name = '';
          getProvidersQuery.enabled = true;
          // getProvidersQuery.hasFixedAmount = false;
          // getProvidersQuery.label = '';
          getProvidersQuery.order = order;
          getProvidersQuery.page = page;

          this.officeApi.getProviders(getProvidersQuery).subscribe(res => {
            const providersCategoryTreeData = [];

            for (const category of categories.items) {

              const subProviders = res.items.filter(p => {
                return p.labels.find(l => {
                  return l === category.id;
                });
              });

              providersCategoryTreeData.push({
                ...category,
                services: subProviders
              });
            }

            this._providersList = res.items.map(p => ProviderData.createNoThrow(p, this.officeApi)).filter(pd => pd != null);
            this.providersList.next(this._providersList);
            this._providersCategoryTree = providersCategoryTreeData.map(d => new ProviderCategoryData(d, this.officeApi));
            this.providersCategoryTree.next(this._providersCategoryTree);
          });
        });
    }
  }

  getProviderCommissions(branchId: string, providerId: string) {
    const operatorGetProviderCommissionsQuery = new OperatorGetProviderCommissionsQuery();
    operatorGetProviderCommissionsQuery.branchId = branchId;
    operatorGetProviderCommissionsQuery.providerId = providerId;
    return this.officeApi.getProviderCommissions(operatorGetProviderCommissionsQuery);
  }

  getProviderDescriptors(providerId: string) {
    const operatorGetProviderDescriptorsQuery = new OperatorGetProviderDescriptorsQuery();
    operatorGetProviderDescriptorsQuery.providerId = providerId;
    this.officeApi.getProviderDescriptors(operatorGetProviderDescriptorsQuery).subscribe(res => {
    });
  }

  getTransferProvidersCategoryTree() { // todo jnjel
    this._providersCategoryTree = transferServiceCategory.map(d => new ProviderCategoryData(d, this.officeApi));
    this.providersCategoryTree.next(this._providersCategoryTree);
  }

  getTopUpProvidersCategoryTree() { // todo jnjel
    this._providersCategoryTree = topupServiceCategory.map(d => new ProviderCategoryData(d, this.officeApi));
    this.providersCategoryTree.next(this._providersCategoryTree);
  }

  getCategoryProviders(categoryId): Observable<ProviderCategoryData> {
    return this.providersCategoryTree
      .pipe(map(c => {
        return c.find(p => {
          return categoryId === p.name;
        });
      }));
  }
}
