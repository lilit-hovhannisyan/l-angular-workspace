import {Injectable, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {ProviderDataService} from '../provider/provider-data.service';
import {TransactionData, BalanceData} from './cashbox-data.models';
import {
  OfficeApi, OperatorCashboxPayCommand, OperatorGenerateCashboxReportCommand, OperatorGetCashboxBalanceQuery,
  OperatorGetOperatorLogQuery,
  OperatorGetTransactionQuery,
  OperatorGetTransactionsByPaymentQuery, Order, Page,
  OperatorGetCashboxInfoQuery
} from 'upay-lib';
import {UserDataService} from '../user/user-data.service';
// import {
//   OfficeApi,
//   Page, Order,
//   OperatorCashboxPayCommand, OperatorGetCashboxTransactionsQuery,
//   OperatorGetTransactionQuery, OperatorGetTransactionsByPaymentQuery,
//   OperatorGetCashboxBalanceQuery,
//   OperatorGenerateCashboxReportCommand, OperatorGetOperatorLogQuery
// } from '../../api/index';

export interface ICashboxTransactionsFilter {
  columnKey?: 'userId' | 'providerName' | 'paymentId' | 'userNum' | 'receiptId';
  columnValue?: string;
  amountFrom?: number;
  amountTo?: number;
  dateFrom?: Date;
  dateTo?: Date;
}

@Injectable()
export class CashboxDataService {
  _transactionList = {
    items: [],
    total: 0
  };
  transactionList = new BehaviorSubject(this._transactionList);


  _balance;
  balance = new BehaviorSubject(this._balance);

  updatedBalane = new EventEmitter();

  constructor(private officeApi: OfficeApi,
              private userDataService: UserDataService,
              private providerDataService: ProviderDataService) {
  }


  // դրամարկղի ընդհանուր շարժի թարմացում
  passInfoFromBasketForTransactionLog(basketItemsSelected, isPos) {
    const operatorCashboxPayCommand = new OperatorCashboxPayCommand();

    operatorCashboxPayCommand.cashboxPaymentId = basketItemsSelected.cashboxPaymentId;
    operatorCashboxPayCommand.totalAmount = basketItemsSelected.totalAmount;
    operatorCashboxPayCommand.serviceQuantity = basketItemsSelected.serviceQuantity;
    operatorCashboxPayCommand.inAmount = basketItemsSelected.inAmount;
    operatorCashboxPayCommand.cash = !isPos;

    return this.officeApi.cashboxPay(operatorCashboxPayCommand);
  }

  // անդորագրի տվյալի հետ վերադարձ :: by transaction ID
  getTransaction (transactionId) {
    const operatorGetTransactionQuery = new OperatorGetTransactionQuery();
    operatorGetTransactionQuery.transactionId = transactionId;
    return this.officeApi.getTransaction(operatorGetTransactionQuery);
  }

  // transaction item return by payment Id
  getTransactionsByPayment (paymentId) {
    const operatorGetTransactionsByPaymentQuery = new OperatorGetTransactionsByPaymentQuery();
    operatorGetTransactionsByPaymentQuery.paymentId = paymentId;

    const page = new Page();
    page.index = 0;
    page.size = 10000;

    const order = new Order();
    order.fields = [];
    operatorGetTransactionsByPaymentQuery.page = page;
    operatorGetTransactionsByPaymentQuery.order = order;

    return this.officeApi.getTransactionsByPayment(operatorGetTransactionsByPaymentQuery);
  }

  // all transactions for log page


  getCashboxTransactions(cashBoxId: string, page: Page, order: Order, filter?: ICashboxTransactionsFilter) {
    this.providerDataService.providersList.subscribe(providersList => {
      if (providersList.length) {

        const data = new OperatorGetOperatorLogQuery();
        data.page = page;
        data.order = order;
        if (filter.columnKey) {
          data[filter.columnKey] = filter.columnValue;
        }
        if (filter.amountFrom) {
          data.amountFrom = filter.amountFrom;
        }
        if (filter.amountTo) {
          data.amountTo = filter.amountTo;
        }
        if (filter.dateFrom) {
          data.dateFrom = filter.dateFrom;
        }
        if (filter.dateTo) {
          data.dateTo = filter.dateTo;
        }

        this.officeApi.getOperatorLog(data).subscribe(res => {
          this._transactionList = {
            items: res.items.map(t => {
              const provider = providersList.find(p => {
                return p.name === t.providerName;
              }); // providerId
              return new TransactionData({...t, provider});
            }),
            total: res.total
          };
          this.transactionList.next(this._transactionList);
        });
      } else {
        this.providerDataService.getPayProvidersCategoryTree();
      }
    });
  }

  // cashbox balance data
  getCashboxBalance (cashboxId?: any) {
    console.log(cashboxId);
   const userInfosSub = this.userDataService.userInfo.subscribe(userInfo => {
      const operatorGetCashboxBalanceQuery = new OperatorGetCashboxBalanceQuery();
      operatorGetCashboxBalanceQuery.cashboxId = userInfo.cashBoxId;
      return this.officeApi.getCashboxBalance(operatorGetCashboxBalanceQuery).subscribe( res => {
        this._balance = new BalanceData(res);
        this.balance.next(this._balance);
      });
    });

    userInfosSub.unsubscribe();
  }



  // get current ballance
  getCurrentBalance(cashboxId) {
    return this.officeApi.getCurrentBalance(cashboxId);
  }

  // generate report to send general accountant
  generateCashboxReport() {
    const operatorGenerateCashboxReportCommand = new OperatorGenerateCashboxReportCommand();

    return this.officeApi.generateCashboxReport(operatorGenerateCashboxReportCommand);
  }

  getCashboxInfo() {
    const operatorGetCashboxInfoQuery = new OperatorGetCashboxInfoQuery();
    return this.officeApi.getCashboxInfo(operatorGetCashboxInfoQuery);
  }


  // update balance
  updateBalance() {
    this.userDataService.userInfo.subscribe(res => {
      if (res) {
        this.getCurrentBalance({cashboxId: res.cashBoxId}).subscribe(balance => {
          this.updatedBalane.emit(balance);
          // debugger
        });
      }
    });
  }

  getBalanceChange() {
    return this.updatedBalane;
  }
}
