export class TransactionData {
  providerIcon: string;
  providerName: string;
  providerNameTranslated: string;
  purpose: any;
  date: string;
  debt: number;
  amount: number;
  branchName: string;
  commissionAmount: number;
  userId: string;
  receiptId: string;
  action: string;
  notes: string;
  encashmentType: string;
  rawData: string;
  clientData: string;
  operatorName: string;
  contactNum: string;
  isCash: boolean;
  posAuthcode: string;
  totalAmount: number;
  providerNameKey: string;

  constructor(data) {
    if (data.action === 'ENCASHMENT_IN') {
      this.providerIcon = 'shared-assets/img/providers/' + 'in.png';
      this.providerNameTranslated = 'encashment_' + data.encashmentType; // data.provider.text;
    } else if (data.action === 'ENCASHMENT_OUT') {
      this.providerIcon = 'shared-assets/img/providers/' + 'out.png';
      this.providerNameTranslated = 'encashment_' + data.encashmentType; // data.provider.text;
    } else {
      this.providerIcon = 'shared-assets/img/providers/' + data.providerName.toLocaleLowerCase() + '.png'; // + data.provider.logo;
      this.providerNameTranslated = data.providerName;
    }
    this.providerName = data.providerName;
    this.action = data.action;
    this.purpose = data.purpose;
    this.date = new Date(data.date) + '';
    this.debt = data.debt;
    this.totalAmount = data.totalAmount / 100;
    // this.amount = data.amount / 100; // because back end return big decimals
    this.branchName = data.branchName;
    this.commissionAmount = data.commissionAmount;
    this.userId = data.userNum;
    this.receiptId = data.receiptId;
    this.notes = data.notes;
    this.encashmentType = data.encashmentType;
    this.rawData = data.rawData;
    this.clientData = data.clientData;
    this.operatorName = data.operatorName;
    this.contactNum = data.contactNum ? data.contactNum : '---';
    this.isCash = data.cash;
    this.posAuthcode = data.posAuthcode;
    // id: string;
    // name: string;

  }
}

export class BalanceData {
  amountIn: number;
  amountOut: number;
  cashboxId: string;
  commissions: number;
  currentBalance: number;
  name: string;
  openingBalance: number;
  cashlessOut: number;

// amountIn: 810698
// amountOut: 0
// cashboxId: "92123bbc-0ee5-417e-8bae-c5b947d05898"
// commissions: 0
// currentBalance: 2000
// name: "Amiryan1"
// openingBalance: -808698

  constructor(data) {
    this.amountIn = data.amountIn / 100;
    this.amountOut = data.amountOut / 100;
    this.cashboxId = data.cashboxId;
    this.commissions = data.commissions;
    this.cashlessOut = data.cashlessOut / 100;
    this.currentBalance = data.currentBalance / 100;
    this.name = data.name;
    this.openingBalance = data.openingBalance / 100;
  }
}

