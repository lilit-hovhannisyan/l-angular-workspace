import { Injectable, Output, EventEmitter } from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import { OfficeApi, CashboxAction} from 'upay-lib';
import {CashboxDataService} from '../cashbox/cashbox-data.service';

@Injectable()
export class EncashmentService  {
  encashmentTypes: any;
  @Output() fireFieldClear: EventEmitter<any> = new EventEmitter();
  cashboxIn;
  cashboxOut;
  printData = {
    name: '',
    username: '',
    details: '',
    amount: '',
    type: '',
    branchName: '',
    branchId: '',
    amountText: '',
    orderId: '~~',
    isComfirm: true,
    date: '',
    userValue: '',
    isIn: true,
    totalPrice: '',
    ucomWallet: '',
    surname: ''
  };
  documentData = {
    name: '',
    type: 'PASSPORT',
    documentNumber: '',
    dateOfIssue: '',
    dateOfExpiry: '',
    authority: ''
  };

  constructor(protected http: HttpClient,
              private officeApi: OfficeApi,
              private cashboxDataService: CashboxDataService) {
  }

 getEncashmentTypes() {
    this.encashmentTypes = [
      {
        text: 'Inner encashment',
        armText: 'inner_encashment',
        name: 'internal',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon_encashment',
      },
      {
        text: 'Outer incasacia',
        armText: 'outer_encashment',
        name: 'external',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_encashment'
      },
      {
        text: 'Ucom',
        armText: 'ucom_encashment',
        name: 'ucom',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon_ucom'
      },
      {
        text: 'Wallet_encashment',
        armText: 'wallet_encashment',
        name: 'IDRAM',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_wallet'
      },
      {
        text: 'loan_good_credit',
        armText: 'loan_good_credit',
        name: 'loan_good_credit',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_bank'
      },
      {
        text: 'loan_global_credit',
        armText: 'loan_global_credit',
        name: 'loan_global_credit',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_bank'
      },
      {
        text: 'loan_varks_am',
        armText: 'loan_varks_am',
        name: 'loan_varks_am',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_bank'
      },
      {
        text: 'phone_by_back_service',
        armText: 'phone_by_back_service',
        name: 'phone_by_back_service',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_telephone'
      },
      {
        text: 'wallet_encashment_ucom',
        armText: 'wallet_encashment_ucom',
        name: 'ucom_wallet',
        armLongInnerText: '',
        iconClassName: 'iu-ui-icon icon icon_ucom'
      }
      ];
    return this.encashmentTypes;
  }

  getEncashmentsByType(type) {
    const data = {
      INTERNAL_IN: {
        isIn: true,
        isOut:  true,
        amountText: '',
        type: 'INTERNAL_IN',
        encashmentType: 'ENCAHSMENT_IN',
        fields: [
          {type: 'text', key: 'username', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
        ],
        data: {
          amount: 0,
          orderId: '',
          type: 'INTERNAL_IN',
          contract: 0,
          details: 'Մուտք ներքին ինկասացիայից',
          notes: '',
        },
        printData: this.printData
      },
      INTERNAL_OUT: {
        isIn: true,
        isOut:  true,
        orderNumber: '~~',
        type: 'INTERNAL_OUT',
        code: '228',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'text', key: 'username', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          {type: 'text', key: 'details', defaultValue: '', placeholder: 'Հիմք'},
        ],
        data: {
          amount: 0,
          orderId: '',
          type: 'INTERNAL_OUT',
          details: 'Ելք Արմենիա ինկասացիայի միջոցով',
          notes: '',
        },
        printData: this.printData
       },

      EXTERNAL_IN: {
        isIn: true,
        isOut:  true,
        type: 'EXTERNAL_IN',
        encashmentType: 'ENCAHSMENT_IN',
        fields: [
          // {type: 'text', key: 'username', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          // {type: 'text', key: 'contract', defaultValue: '', placeholder: 'Վկայականի համար'},
        ],
        printData: this.printData,
        data: {
          amount: 0,
          orderId: '',
          type: 'EXTERNAL_IN',
          // contract: 0,
          details: '',
          notes: '',
        }
      },
      EXTERNAL_OUT: {
        isIn: true,
        isOut:  true,
        type: 'EXTERNAL_OUT',
        encashmentType: 'ENCAHSMENT_OUT',
        code: '255',
        fields: [
          // {type: 'text', key: 'username', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          // {type: 'text', key: 'contract', defaultValue: '', placeholder: 'Վկայականի համար'},
          {type: 'text', key: 'details', defaultValue: '', placeholder: 'Հիմք'},
        ],
        printData: this.printData,
        data: {
          amount: 0,
          orderId: '',
          type: 'EXTERNAL_OUT',
          details: 'Ելք Արմենիա ինկասացիայի միջոցով',
          notes: '',
        }
       },

      UCOM: {
        isIn: true,
        isOut:  false,
        type: 'UCOM',
        encashmentType: 'ENCAHSMENT_IN',
        fields: [
          {type: 'text', key: 'username', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'text', key: 'contract', defaultValue: '', placeholder: 'Կնիքի համար'},
          {type: 'namber', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'}
        ],
        printData: this.printData,
        data: {
          amount: 0,
          orderId: '',
          type: 'UCOM',
          details: 'Յուքոմ ՓԲԸ-ից մուտք',
          notes: '',
        }
       },
      IDRAM: {
        isIn: false,
        isOut:  true,
        type: 'IDRAM',
        code: '224',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'text', key: 'name', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          {type: 'select', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագիր / Նույնականացման քարտ',
          options: [
           {
             id: 'PASSPORT',
             text: 'Անձնագիր',
           },
           {
             id: 'IDCARD',
             text: 'Նույնականացման քարտ',
         }]},
          {type: 'text', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագրի / Նույնականացման քարտի համար'},
          {type: 'text', key: 'dateOfIssue', defaultValue: 0, placeholder: 'Տրվել է'},
          {type: 'text', key: 'dateOfExpiry', defaultValue: 0, placeholder: 'Վավերական է մինչև'},
          {type: 'text', key: 'authority', defaultValue: 0, placeholder: 'Ում կողմից'},
          {type: 'text', key: 'idramWallet', defaultValue: 0, placeholder: 'Դրամապանակի համար'},
        ],
        document: this.documentData,
        printData: this.printData,
        data: {
          orderId: '',
          idramWallet: '',
          amount: 0,
          document: this.documentData,
          documentNumber: '',
          dateOfIssue: '',
          dateOfExpiry: '',
          authority: '',
          details: 'Դրամապանակի կանխիկացում',
        }
       },

      UCOM_WALLET: {
        isIn: false,
        isOut:  false,
        type: 'UCOM_WALLET_OUT',
        code: '521',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Կանխիկացման գումարը'},
        ],
        document: this.documentData,
        printData: this.printData,
        data: {
          amount: 0,
          surname: '',
          document: this.documentData,
          ucomWallet: '',
          documentNumber: '',
          dateOfIssue: '',
          dateOfExpiry: '',
          authority: '',
        }
      },
      // UCOM_WALLET_IN: {
      //   isIn: true,
      //   isOut:  true,
      //   type: 'UCOM_WALLET_IN',
      //   encashmentType: 'ENCAHSMENT_IN',
      //   fields: [
      //     {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Դրամապանակի լիցքավորում'},
      //   ],
      //   document: this.documentData,
      //   printData: this.printData,
      //   data: {
      //     amount: 0,
      //   }
      // },

      LOAN_GOOD_CREDIT: {
        isIn: false,
        isOut:  true,
        type: 'LOAN_GOOD_CREDIT',
        code: '523',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'text', key: 'name', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          {type: 'select', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագիր / Նույնականացման քարտ',
           options: [
            {
              id: 'PASSPORT',
              text: 'Անձնագիր',
            },
            {
              id: 'IDCARD',
              text: 'Նույնականացման քարտ',
          }]},
          {type: 'text', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագրի / Նույնականացման քարտի համար'},
          {type: 'text', key: 'dateOfIssue', defaultValue: 0, placeholder: 'Տրվել է'},
          {type: 'text', key: 'dateOfExpiry', defaultValue: 0, placeholder: 'Վավերական է մինչև'},
          {type: 'text', key: 'authority', defaultValue: 0, placeholder: 'Ում կողմից'},
          {type: 'text', key: 'aggreementNumber', defaultValue: 0, placeholder: 'Վարկային պայմանագրի համար'}
        ],
        document: this.documentData,
        printData: this.printData,
        data: {
          orderId: '',
          aggreementNumber: '',
          amount: 0,
          documentNumber: '',
          dateOfIssue: '',
          dateOfExpiry: '',
          authority: '',
          details: 'Գուդկրեդիտ ՈՒՎԿ ՓԲԸ-ի կողմից հաստատաված վարկի տրամադրում',
          document: this.documentData,
        }
      },
      LOAN_GLOBAL_CREDIT: {
        isIn: false,
        isOut:  true,
        type: 'LOAN_GLOBAL_CREDIT',
        code: '523',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'text', key: 'name', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          {type: 'select', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագիր / Նույնականացման քարտ',
            options: [
              {
                id: 'PASSPORT',
                text: 'Անձնագիր',
              },
              {
                id: 'IDCARD',
                text: 'Նույնականացման քարտ',
              }]},
          {type: 'text', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագրի / Նույնականացման քարտի համար'},
          {type: 'text', key: 'dateOfIssue', defaultValue: 0, placeholder: 'Տրվել է'},
          {type: 'text', key: 'dateOfExpiry', defaultValue: 0, placeholder: 'Վավերական է մինչև'},
          {type: 'text', key: 'authority', defaultValue: 0, placeholder: 'Ում կողմից'},
          {type: 'text', key: 'aggreementNumber', defaultValue: 0, placeholder: 'Վարկային պայմանագրի համար'}
        ],
        document: this.documentData,
        printData: this.printData,
        data: {
          orderId: '',
          aggreementNumber: '',
          amount: 0,
          documentNumber: '',
          dateOfIssue: '',
          dateOfExpiry: '',
          authority: '',
          details: 'Գլոբալկրեդիտ ՈՒՎԿ ՓԲԸ-ի կողմից հաստատաված վարկի տրամադրում',
          document: this.documentData,
        }
      },
      LOAN_VARKS_AM: {
        isIn: false,
        isOut:  true,
        type: 'LOAN_VARKS_AM',
        code: '224',
        encashmentType: 'ENCAHSMENT_OUT',
        fields: [
          {type: 'text', key: 'name', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'Գումարը'},
          {type: 'select', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագիր / Նույնականացման քարտ',
           options: [
            {
              id: 'PASSPORT',
              text: 'Անձնագիր',
            },
            {
              id: 'IDCARD',
              text: 'Նույնականացման քարտ',
          }]},
          {type: 'text', key: 'documentNumber', defaultValue: 0, placeholder: 'Անձնագրի / Նույնականացման քարտի համար'},
          {type: 'text', key: 'dateOfIssue', defaultValue: 0, placeholder: 'Տրվել է'},
          {type: 'text', key: 'dateOfExpiry', defaultValue: 0, placeholder: 'Վավերական է մինչև'},
          {type: 'text', key: 'authority', defaultValue: 0, placeholder: 'Ում կողմից'},
          {type: 'text', key: 'aggreementNumber', defaultValue: 0, placeholder: 'Վարկային պայմանագրի համար'}
        ],
        document: this.documentData,
        printData: this.printData,
        data: {
          orderId: '',
          aggreementNumber: '',
          amount: 0,
          documentNumber: '',
          dateOfIssue: '',
          dateOfExpiry: '',
          authority: '',
          details: 'ՎԱՐԿՍ ԷՅ ԱՄ-ի կողմից հաստատաված վարկի տրամադրում Գուդկրեդիտ ՈՒՎԿ ՓԲԸ-ի կողմից հաստատաված վարկի տրամադրում',
          document: this.documentData,
        }
      },

      PHONE_BY_BACK_SERVICE: {
        isIn: true,
        isOut:  false,
        amountText: '',
        type: 'PHONE_BY_BACK_SERVICE',
        encashmentType: 'ENCAHSMENT_IN',
        fields: [
          {type: 'text', key: 'name', defaultValue: '', placeholder: 'Անուն / Ազգանուն'},
          {type: 'text', key: 'model', defaultValue: '', placeholder: 'Նոր հեռախոսի մոդել'},
          {type: 'string', key: 'imei', defaultValue: 0, placeholder: 'Նոր Հեռախոսի IMEI'},
          {type: 'number', key: 'totalPrice', defaultValue: 0, placeholder: 'Նոր Հեռախոսի ամբողջական արժեք'},
          {type: 'number', key: 'amount', defaultValue: 0, placeholder: 'U!Pay-ում վճարման ենթակա գումար'},
        ],
        data: {
          name: '',
          orderId: '',
          model: '',
          imei: '',
          amount: 0,
          totalPrice: 0,
        },
        printData: this.printData
      },
    };
    return data[type];
  }

  getCashboxRequest(type, data) {
    const cashbox = {
      INTERNAL_IN : this.cashBoxIn(data),
      INTERNAL_OUT: this.cashBoxOut(data),
      EXTERNAL_IN: this.cashBoxIn(data),
      EXTERNAL_OUT: this.cashBoxOut(data),
      UCOM: this.cashBoxIn(data),
      LOAN_GOOD_CREDIT: this.CashOutLoanGoodCredit(data),
      LOAN_GLOBAL_CREDIT: this.CashOutLoanGlobalCredit(data),
      IDRAM: this.CashOutIdram(data),
      LOAN_VARKS_AM: this.CashOutLoanVarksAm(data),
      PHONE_BY_BACK_SERVICE: this.CashOutUcomPhoneByBackService(data),
    };
    return cashbox[type] || cashbox.INTERNAL_IN;
  }

  getNumToText(number) {
    return this.officeApi.num2Text( {'number': number});
  }

  cashBoxIn(data) {
    return this.officeApi.cashboxIn(data);
  }

  CashOutIdram(data) {
    return this.officeApi.cashOutIdram(data);
  }

  CashOutLoanGoodCredit(data) {
    return this.officeApi.cashOutLoanGoodCredit(data);
  }

  CashOutLoanGlobalCredit(data) {
    data.amount = +data.amount;
    return this.officeApi.cashOutLoanGlobalCredit(data);
  }

  CashOutLoanVarksAm(data) {
    return this.officeApi.cashOutLoanVarksAm(data);

  }
  CashOutUcomPhoneByBackService(data) {
    return this.officeApi.cashOutUcomPhoneByBackService(data);
  }

  getEncashmentTypesByName(name) {
    const item = this.encashmentTypes.filter(filterItem => filterItem.name === name );
    return item[0];
  }

  getCurrentBranch() {
    return this.officeApi.getCurrentBranch({});
  }

  cashBoxOut(data) {
    return this.officeApi.cashboxOut( data);
  }

  getOrderNumber(data) {
    console.log('***************************', data);
    return this.officeApi.getOrderNumber(data);
  }


  // clear fileds
  clearEncashmentFields() {
    // const fields = document.getElementById('encashment_fields').getElementsByTagName('input');
    // for (let i = 0; i < fields.length; i++) {
    //   fields[i].value = '';
    // }

    this.fireFieldClear.emit(true);
  }

  getEmittedValue() {
    return this.fireFieldClear;
  }

  // update balance on header
  clearFieldsAndUpdateBalance() {
    this.cashboxDataService.updateBalance();
    // clear fileds
    this.clearEncashmentFields();
    // console.log('update Balance');
    // update balance on header
  }
}
