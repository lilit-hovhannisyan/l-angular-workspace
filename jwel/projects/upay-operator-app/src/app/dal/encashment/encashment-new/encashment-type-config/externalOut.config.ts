export const externalOut = {
  key: 'ExternalOut', // id
  displayText: 'providers.encashment_EXTERNAL_OUT', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentOut', // grouping

  inputFields: [
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    },
    {
      key: 'details',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.basis',
        defaultValueForOrder: 'Հիմք', // todo i18n
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderExternalOut', // refers to html file,
    predefinedDataMap: {
      accountNumber: '255',
      purpose: 'Ելք Արմենիա ինկասացիայի միջոցով', // todo i18n
      _usernameData: 'Արմենիա ինկասացիոն ծառայություն ՓԲԸ ինկասատոր',
      _detailsData: {
        valueKey: '`Առաքվող արժեքների ցուցակ ${details}`'
      }
    }
  },

  getOrderIdAction: {
    directionIn: false,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashboxOut',
    predefinedDataMap: {
      type: 'EXTERNAL_OUT', // for networking, predefined value
      notes: '' // todo chi ogtagorcvum es dashty
    }
  }
};
