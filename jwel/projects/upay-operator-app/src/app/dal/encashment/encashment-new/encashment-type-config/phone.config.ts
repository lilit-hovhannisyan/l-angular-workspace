export const encashmentPhone = {
  key: 'UcomPhoneByBackService', // id
  displayText: 'providers.encashment_PHONE_BY_BACK_SERVICE', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentIn', // grouping

  inputFields: [
    {
      key: 'name',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.nameSurname',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'model',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.phoneModel',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'imei',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.imei',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'totalPrice',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.totalPrice',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    },
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amountToPayUpay',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderIn', // refering to html file,
    predefinedDataMap: {
      accountNumber: '',
      purpose: 'Յուքոմ ՓԲԸ, հեռախոսի փոխանակում', // todo i18n
      _usernameData: {
        valueKey: '`${name}-ից`'
      },
      _detailsData: {
        valueKey: '`Հեռախոսի գնում՝ ${model}, IMEI ${imei}, նոր հեռախոսի ամբողջական արժեք՝ ${totalPrice}դր., U!Pay-ում վճարման ենթակա գումար ${amount}դր.`'
      }
    }
  },

  getOrderIdAction: {
    directionIn: true,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashOutUcomPhoneByBackService',
    predefinedDataMap: {
    }
  }
};
