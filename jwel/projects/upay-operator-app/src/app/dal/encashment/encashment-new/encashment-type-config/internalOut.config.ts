export const internalOut = {
  key: 'InternalOut', // id
  displayText: 'providers.encashment_INTERNAL_OUT', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentOut', // grouping

  inputFields: [
    {
      key: 'username',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.nameSurname',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    },
    {
      key: 'details',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.basis',
        defaultValueForOrder: 'Հիմք', // todo i18n
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderOut', // refers to html file,
    predefinedDataMap: {
      accountNumber: '228',
      purpose: 'Ելք ներքին ինկասացիայից', // todo i18n
      _usernameData: {
        valueKey: '`«Յուփեյ» ՓԲԸ պատասխանտու անձ ${username} -ից`'
      },
      _detailsData: {
        valueKey: '`Առաքվող արժեքների ցուցակ ${details}`'
      }
    }
  },

  getOrderIdAction: {
    directionIn: false,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashboxOut',
    predefinedDataMap: {
      type: 'INTERNAL_OUT', // for networking, predefined value
      notes: '' // todo chi ogtagorcvum es dashty
    }
  }
};
