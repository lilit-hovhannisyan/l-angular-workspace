export const internalIn = {
  key: 'InternalIn', // id
  displayText: 'providers.encashment_INTERNAL_IN', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentIn', // grouping

  inputFields: [
    {
      key: 'username',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.nameSurname',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderIn', // refering to html file,
    predefinedDataMap: {
      accountNumber: '',
      purpose: 'Մուտք ներքին ինկասացիայից', // todo i18n
      _usernameData: {
        valueKey: '`«Յուփեյ » ՓԲԸ պատասխանտու անձ ${username}-ից`'
      },
      _detailsData: 'Մուտք դրամարկղ ներքին ինկասացիայից'
    }
  },

  getOrderIdAction: {
    directionIn: true,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashboxIn',
    predefinedDataMap: {
      type: 'INTERNAL_IN', // for networking
      notes: '', // todo chi ogtagorcvum es dashty
      contract: '' // api waits for this element, used only in "ucom" case optional field
    }
  }
};
