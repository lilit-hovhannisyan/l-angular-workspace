import {EventEmitter, Injectable} from '@angular/core';
import {encashmentOrderIn} from './order-html/encashment-order-in.html';
import {encashmentOrderOut} from './order-html/encashment-order-out.html';
import {encashmentOrderExternalIn} from './order-html/encashment-order-external-in.html';
import {encashmentOrderExternalOut} from './order-html/encashment-order-external-out.html';
import {encashmentOrderOutForCreditServices} from './order-html/encashment-order-out-for-credit-services.html';
import {OfficeApi, OperatorGetOrderNumberQuery, OperatorNum2TextQuery} from 'upay-lib';
import {encashmentTypesConfig} from './encashment-type-config';
import {ProviderDebtDataService} from 'upay-lib';

@Injectable()
export class EncashmentProviderService {
  encashmentOrderIn = encashmentOrderIn;
  encashmentOrderOut = encashmentOrderOut;
  encashmentOrderExternalIn = encashmentOrderExternalIn;
  encashmentOrderExternalOut = encashmentOrderExternalOut;
  encashmentOrderOutForCreditServices = encashmentOrderOutForCreditServices;

  dropEncashmentData = new EventEmitter();

  constructor(private officeApi: OfficeApi,
              private providerDebtDataService: ProviderDebtDataService) {
  }

  getEncashmentProviderListByCategory(category: string) {
    const list = [];
    encashmentTypesConfig.map( item => {
     if (item.category === category) {
       list.push(item);
     }
    });

    return list;
  }

  getEncashmentProviderByType(type: string) {
    return encashmentTypesConfig.find( i => {
      return i.key === type;
    });
  }

  getOrderHtmlByOrderType(orderType, dynamicData ) {
    let html = this[orderType]();

    for (const key in dynamicData) {
      if (key) {
        const patternToReplace = '#' + key;
        const pattern = new RegExp(patternToReplace, 'g');
        html = html.replace(pattern, dynamicData[key]);
      }
    }
    return html;
  }

  getHtmlDynamicFields(encashmentData, dynamicOrderData) {
    const predefinedData = encashmentData.generateOrderHtmlAction.predefinedDataMap;
    this.updateDynamicOrderDataAccordingMapping(predefinedData, dynamicOrderData);

    return dynamicOrderData;
  }

  numberToText(number) {
    const operatorNum2TextQuery = new OperatorNum2TextQuery();
    operatorNum2TextQuery.number = number;
    return this.officeApi.num2Text(operatorNum2TextQuery);
  }

  getOrderNumber(branchId: string, _in: boolean) {
    const operatorGetOrderNumberQuery = new OperatorGetOrderNumberQuery();
    operatorGetOrderNumberQuery.branchId = branchId;
    operatorGetOrderNumberQuery.in = _in;
    return this.officeApi.getOrderNumber(operatorGetOrderNumberQuery);
  }

  callCashboxAction(encashmentData: any, dynamicOrderData: any) {
    const cashboxActionName = encashmentData.cashboxSubmitAction.apiEndpointName;

    const predefinedData = encashmentData.cashboxSubmitAction.predefinedDataMap;
    this.updateDynamicOrderDataAccordingMapping(predefinedData, dynamicOrderData);

    return this.officeApi[cashboxActionName](dynamicOrderData);
  }

  updateDynamicOrderDataAccordingMapping(mapperObject, readDataObject, writeDataObject?) {
    if (!writeDataObject) {
      writeDataObject = readDataObject;
    }

    for (const key in mapperObject) {
      if (mapperObject[key] instanceof Object) {
        if (!mapperObject[key]['valueKey']) {
          writeDataObject[key] = {};
          const _mapperObject = mapperObject[key];
          this.updateDynamicOrderDataAccordingMapping(_mapperObject, writeDataObject, writeDataObject[key]);
        } else {
          const expression = mapperObject[key]['valueKey'];
          writeDataObject[key] = this.providerDebtDataService.evaluate(readDataObject, mapperObject[key]['valueKey']);
        }
      } else {
        writeDataObject[key] = mapperObject[key];
      }
    }
  }

  //
  refreshEncashmentData() {
    this.dropEncashmentData.emit();
  }

  getRefreshedEncashmentData() {
    return this.dropEncashmentData;
  }
}
