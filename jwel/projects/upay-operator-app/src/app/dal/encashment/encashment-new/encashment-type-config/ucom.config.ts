export const encashmentUcom = {
  key: 'EncashmentUcom', // id
  displayText: 'providers.encashment_UCOM', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentIn', // grouping

  inputFields: [
    {
      key: 'username',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.nameSurname',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'contract',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.stamp',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderIn', // refering to html file,
    predefinedDataMap: {
      accountNumber: '',
      purpose: 'Յուքոմ ՓԲԸ-ից մուտք', // todo i18n
      _usernameData: {
        valueKey: '`Յուքոմ ՓԲԸ պատասխանատու անձ ${username}-ից, &numero; ${contract}`'
      },
      _detailsData: 'Մուտք դրամարկղ'
    }
  },

  getOrderIdAction: {
    directionIn: true,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashboxIn',
    predefinedDataMap: {
      type: 'UCOM', // for networking
      notes: '', // todo chi ogtagorcvum es dashty
    }
  }
};
