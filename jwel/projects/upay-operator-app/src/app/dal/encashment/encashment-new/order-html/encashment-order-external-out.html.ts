export const encashmentOrderExternalOut = () => {
  const blockHtml = `
  <div style="margin: 0 15px; padding: 10px; border: 1px dotted;">
    <div style="margin-bottom: 15px;">
      <b>«Յուփեյ» ՓԲԸ, #branchName մ/ճ</b>
      <div style="font-size: .7rem; border-top: 1px dotted;">կազմակերպության անվանումը</div>
    </div>
    <div style="display:flex; align-items:center;">
      <b style="font-size: 1rem; flex:1; padding-right: 5px;">Դրամարկղային ելքի օրդեր N #orderId</b>
      <div style="flex:1; padding-left: 5px;">
        <div style="border-bottom: 1px dotted; text-align: center;">#date</div>
        <div style="font-size: .5rem; text-align: center;">Կազմման ամսաթիվ</div>
      </div>
    </div>
    <table style="margin: 15px 0;">
      <tr style="font-size: .8rem;">
        <td style="border: 1px dotted; padding: 0 10px;">Թղթակցող հաշիվը</td>
        <td style="border: 1px dotted; padding: 0 10px;">Վերլուծական հաշվառման ծածկագիրը</td>
        <td style="border: 1px dotted; padding: 0 10px;">Գումարը</td>
        <td style="border: 1px dotted; padding: 0 10px;">Նպատակային նշանակության ծածկագիրը</td>
      </tr>
      <tr>
        <td  style="border: 1px dotted; padding: 0 10px;">1</td>
        <td  style="border: 1px dotted; padding: 0 10px;">2</td>
        <td  style="border: 1px dotted; padding: 0 10px;">3</td>
        <td  style="border: 1px dotted; padding: 0 10px;">4</td>
      </tr>
      <tr>
        <td  style="border: 1px dotted; padding: 0 10px;">#accountNumber</td>
        <td  style="border: 1px dotted; padding: 0 10px;"></td>
        <td  style="border: 1px dotted; padding: 0 10px;">#amount</td>
        <td  style="border: 1px dotted; padding: 0 10px;">#purpose</td>
      </tr>
    </table>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Ստացող</b>
      <div style="flex:3; padding-left: 5px; font-size: .9rem;">
        <div style="border-bottom: 1px dotted;">#_usernameData</div>
      </div>
    </div>
    <div style="display:flex; padding: 10px 0; border-bottom: 1px dotted;"></div>
    <div style="font-size: .5rem; text-align: center;">անուն, ազգանուն</div>
    <div style="display:flex; align-items:center; padding: 10px 0;">
      <b style="flex:1; padding-right: 5px;">Վկայական</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Ստացման հիմքը և նպատակը</b>
      <div style="flex:3; padding-left: 5px; font-size: .9rem;">
        <div style="border-bottom: 1px dotted;">#_detailsData</div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Գումարը</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"><span style="font-size: .8rem;">#amount</span> ՀՀ դրամ</div>
        <div style="border-bottom: 1px dotted;"><span style="font-size: .8rem;">#_amountText</span> ՀՀ դրամ</div>
        <div style="font-size: .5rem; text-align: center;">գումարը տառերով</div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Կազմակերպության ղեկավար</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
        <div style="font-size: .5rem; text-align: center;">ստորագրություն</div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Գլխավոր հաշվապահ</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
        <div style="font-size: .5rem; text-align: center;">ստորագրություն</div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Ստացա</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0;">
      <b style="flex:1; padding-right: 5px;">Վճարող</b>
      <div style="flex:3; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
        <div style="font-size: .5rem; text-align: center;">ստորագրություն</div>
      </div>
    </div>
    <div style="display:flex; align-items:center; padding: 13px 0; align-items: center;">
      <b style="flex:1; padding-right: 5px;">Գանձապահ</b>
      <b style="flex:1; padding-right: 5px;">#operator</b>
      <div style="flex:1; padding-left: 5px;">
        <div style="border-bottom: 1px dotted;"></div>
        <div style="font-size: .5rem; text-align: center;">ստորագրություն</div>
      </div>
    </div>
  </div>
  `;


  const html = `<div style="display: flex; font-size: .8rem; margin-top: 10px;">
                  ${blockHtml}
                  <div style="width: 0; border-right: 1px solid;"></div>
                  ${blockHtml}
                </div>`;
  return html;
};
