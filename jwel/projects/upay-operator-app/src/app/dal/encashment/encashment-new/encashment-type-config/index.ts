import {internalIn} from './internalIn.config';
import {internalOut} from './internalOut.config';
import {externalIn} from './externalIn.config';
import {externalOut} from './externalOut.config';
import {encashmentUcom} from './ucom.config';
import {encashmentPhone} from './phone.config';
import {encashmentIdramWallet} from './idram-wallet.config';
import {encashmentUpayWallet} from './upay-wallet.config';
import {encashmentGoodCredit} from './good-credit.config';
import {encashmentGlobalCredit} from './global-credit.config';
import {encashmentVarksAm} from './varks-am.config';

export const encashmentTypesConfig = [
  internalIn,
  internalOut,
  externalIn,
  externalOut,
  encashmentUcom,
  encashmentPhone,
  encashmentIdramWallet,
  encashmentUpayWallet,
  encashmentGoodCredit,
  encashmentGlobalCredit,
  encashmentVarksAm
];
