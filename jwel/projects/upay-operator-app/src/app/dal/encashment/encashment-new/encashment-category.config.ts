export const encashmentCategoryConfig = [
  {
    key: 'cashboxEncashmentIn',
    displayText: 'encashmentInfo.cashboxEncashmentIn',
    icon: 'icon_encashment-in'
  },
  {
    key: 'cashboxEncashmentOut',
    displayText: 'encashmentInfo.cashboxEncashmentOut',
    icon: 'icon_encashment-out'
  },
  {
    key: 'creditEncashment',
    displayText: 'encashmentInfo.creditEncashment',
    icon: 'icon_bank'
  },
  {
    key: 'walletEncashment',
    displayText: 'encashmentInfo.walletEncashment',
    icon: 'icon_wallet'
  }
];
