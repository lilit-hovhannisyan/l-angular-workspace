export const encashmentVarksAm = {
  key: 'LoanVarksAmCashOut', // id
  displayText: 'providers.encashment_LOAN_VARKS_AM', // ui
  icon: 'icon_encashment',
  category: 'creditEncashment', // grouping

  inputFields: [
    {
      key: 'name',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.nameSurname',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    },
    {
      key: 'aggreementNumber',
      uiComponent: {
        component: 'input',
        placeholder: 'encashmentInfo.aggreementNumber',
        validationRules: {
          required: true
        }
      }
    },
    {
      key: 'contactNum',
      uiComponent: {
        component: 'phonePicker',
        placeholder: 'encashmentInfo.walletId',
        validationRules: {
          required: true,
        }
      }
    },
    {
      key: 'type',
      uiComponent: {
        component: 'select',
        placeholder: 'encashmentInfo.selectDocType',
        options: [
          {
            id: 'PASSPORT',
            text: 'encashmentInfo.passport'
          },
          {
            id: 'IDCARD',
            text: 'encashmentInfo.idCard'
          }
        ],
        validationRules: {
          required: true,
        },
        dependantFields: [
          {
            key: 'documentNumber',
            uiComponent: {
              component: 'input',
              placeholder: 'encashmentInfo.documentNumber',
              validationRules: {
                required: true
              }
            }
          },
          {
            key: 'dateOfIssue',
            uiComponent: {
              component: 'datePicker',
              placeholder: 'encashmentInfo.dateOfIssue',
              validationRules: {
                required: true,
                olderDateThan: 'dateOfExpiry'
              }
            }
          },
          {
            key: 'dateOfExpiry',
            uiComponent: {
              component: 'datePicker',
              placeholder: 'encashmentInfo.dateOfExpiry',
              validationRules: {
                required: true,
                validateAgainstToday: true,
                newerDateThan: 'dateOfIssue'
              }
            }
          },
          {
            key: 'authority',
            uiComponent: {
              component: 'input',
              placeholder: 'encashmentInfo.authority',
              validationRules: {
                required: true
              }
            }
          },
        ]
      }
    },
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderOutForCreditServices', // refering to html file,
    predefinedDataMap: {
      accountNumber: '224',
      purpose: 'ՎԱՐԿՍ ԷՅ ԱՄ-ի կողմից հաստատաված վարկի տրամադրում', // todo i18n
      _usernameData: {
        valueKey: '` ${name}`'
      },
      _detailsData: {
        valueKey: '`Փաստաթուղթ  ${documentNumber}, տրվել է ${dateOfIssue}, վավեր է ${dateOfExpiry}, ${authority}-ի կողմից, վարկային պայմանագիր ${aggreementNumber}`'
      }
    }
  },

  getOrderIdAction: {
    directionIn: false,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashOutLoanVarksAm',
    predefinedDataMap: {
      document: {
        name: {
          valueKey: 'name'
        },
        type: {
          valueKey: 'type'
        },
        documentNumber: {
          valueKey: 'documentNumber'
        },
        dateOfIssue: {
          valueKey: 'dateOfIssue'
        },
        dateOfExpiry: {
          valueKey: 'dateOfExpiry'
        },
        authority: {
          valueKey: 'authority'
        },
      }
    }
  }
};
