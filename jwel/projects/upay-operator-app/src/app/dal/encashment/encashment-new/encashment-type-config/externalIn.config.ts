export const externalIn = {
  key: 'ExternalIn', // id
  displayText: 'providers.encashment_EXTERNAL_IN', // ui
  icon: 'icon_encashment',
  category: 'cashboxEncashmentIn', // grouping

  inputFields: [
    {
      key: 'amount',
      uiComponent: {
        component: 'input',
        type: 'number',
        placeholder: 'encashmentInfo.amount',
        validationRules: {
          required: true,
          regExp: '^[+]?\\d+([.]\\d+)?$'
        }
      }
    }
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderExternalIn', // refering to html file,
    predefinedDataMap: {
      accountNumber: '',
      purpose: 'Արմենիա ինկասացիոն ծառայություն ՓԲԸ', // todo i18n
      _usernameData: 'Արմենիա ինկասացիոն ծառայություն ՓԲԸ ինկասատոր',
      _detailsData: 'Մուտք դրամարկղ Արմենիա ինկասացիայից'
    }
  },

  getOrderIdAction: {
    directionIn: true,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'cashboxIn',
    predefinedDataMap: {
      type: 'EXTERNAL_IN', // for networking
      notes: '', // todo chi ogtagorcvum es dashty
      contract: '' // api waits for this element, used only in "ucom" case optional field
    }
  }
};
