export const encashmentUpayWallet = {
  key: 'UpayCustomerCashout', // id
  displayText: 'providers.encashment_UPAY', // ui
  icon: 'icon_encashment',
  category: 'walletEncashment', // grouping

  useCustomComponent: 'CashoutComponent',

  inputFields: [
  ],

  generateOrderHtmlAction: {
    orderType: 'encashmentOrderOut', // refering to html file,
    predefinedDataMap: {
      accountNumber: '521',
      purpose: 'Կանխիկացում', // todo i18n
      _usernameData: {
        valueKey: 'name'
      },
      _detailsData: {
        valueKey: '`${customerNumber} UPAY ID-ով հաճախորդի UPAY դրամապանակի կանխիկացում, ${type} ${documentNumber}, տրվել է ${dateOfIssue}, ${authority}-ի կողմից, վավեր է մինչև ${dateOfExpiry}`'
      }
    }
  },

  getOrderIdAction: {
    directionIn: false,  // for networking to get order number
  },

  cashboxSubmitAction: {
    apiEndpointName: 'customerCashout',
    predefinedDataMap: {
      amount: {
        valueKey: '`${amount*100}`'
      },
      document: {
        name: {
          valueKey: 'name'
        },
        type: {
          valueKey: 'type'
        },
        documentNumber: {
          valueKey: 'documentNumber'
        },
        dateOfIssue: {
          valueKey: 'dateOfIssue'
        },
        dateOfExpiry: {
          valueKey: 'dateOfExpiry'
        },
        authority: {
          valueKey: 'authority'
        },
      }
    }
  }
};
