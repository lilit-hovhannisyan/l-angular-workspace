import {Injectable, Output, EventEmitter, OnInit, OnDestroy} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {CashboxDataService} from '../cashbox/cashbox-data.service';
import {initDomAdapter} from '@angular/platform-browser/src/browser';
import {EncashmentDataService} from './encashment-data.service';
import {EncashmentService} from './encashment.service';
import {PopupsService} from '../../popups/popups.service';
import {Subscription, Observable} from 'rxjs/index';
import {map} from 'rxjs/operators';
import {Branch} from 'upay-lib';


@Injectable()
export class EncashmentPrintDataLogService implements OnDestroy {
  activeTab: String;
  getCurrentBranche: Subscription;
  getNumToText: Subscription;
  printersViewMap: Object;
  subscription: Subscription;

  constructor(
    private encashmentDataService: EncashmentDataService,
    private popupsService: PopupsService,
    private encashmentService: EncashmentService,
  ) {
    this.printersViewMap = {
      INTERNAL_IN: this.internalEncashmentInData,
      INTERNAL_OUT: this.internalEncashmentOutData,
      EXTERNAL_IN: this.externalEncashmentInData,
      EXTERNAL_OUT: this.externalEncashmentOutData,
      UCOM: this.ucomEncushmentInData,
      LOAN_GOOD_CREDIT: this.goodCreditLoansEncashmentOut,
      LOAN_GLOBAL_CREDIT: this.globalCreditLoansEncashmentOut,
      LOAN_VARKS_AM: this.varksAmEncashmentOut,
      IDRAM: this.walletEncashmentOut,
      PHONE_BY_BACK_SERVICE: this.phoneByBackService
    };
  }

  ngOnDestroy() {
    if (this.getCurrentBranche) {
      this.getCurrentBranche.unsubscribe();
    }
    if (this.getNumToText) {
      this.getNumToText.unsubscribe();
    }
  }

  collectPrinterData(type, rawData, currentItem, date, event) {

    this.getCurrentBranche = this.encashmentService.getCurrentBranch().subscribe(res => {
      this.getNumToText = this.encashmentService.getNumToText(Math.abs(rawData.amount)).subscribe(amount => {

        currentItem.printData.branchName = res.name;
        currentItem.printData.branchId = res.id;
        currentItem.printData.amountText = amount;
        currentItem.printData.name = rawData.notes;
        currentItem = this.setPrintData(currentItem, event, rawData, date);
        currentItem = this.printersViewMap[type](currentItem, rawData, date, event);
        this.popupsService.openEncasmentionPopUp(currentItem);

      });
    });
  }

  setPrintData(currentItem, event, rawData, date) {
    currentItem.printData.amount = rawData.amount;
    currentItem.printData.username = rawData.username;
    currentItem.printData.name = rawData.name;
    currentItem.printData.contract = rawData.contract,
    currentItem.printData.date = date,
    currentItem.printData.details = event.row.providerName,
    currentItem.printData.isComfirm = false,
    currentItem.printData.orderId = event.row.receiptId,
    currentItem.printData.type = event.row.encashmentType;
    if (rawData.document) {
      currentItem.printData.name = rawData.document.name;
    }
    return currentItem;
  }

  internalEncashmentInData(currentItem, rawData, date, event) {
    const userValue = '';
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  varksAmEncashmentOut(currentItem, rawData, date, event) {
    let userValue = `${rawData.document.documentNumber},  ${rawData.document.dateOfIssue},
    ${rawData.document.dateOfExpiry},  ${rawData.document.authority}, `;
    userValue = `${userValue} վարկային պայմանագիր N ${rawData.aggreementNumber}`;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  goodCreditLoansEncashmentOut(currentItem, rawData, date, event) {
    let userValue = '';
    userValue = `${rawData.document.documentNumber}, ${rawData.document.dateOfIssue},
    ${rawData.document.dateOfExpiry}, ${rawData.document.authority}, `;
    userValue = `${userValue} վարկային պայմանագիր N ${rawData.aggreementNumber}`;
    currentItem.printData.name = rawData.document.name;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  globalCreditLoansEncashmentOut(currentItem, rawData, date, event) {
    let userValue = '';
    userValue = `${rawData.document.documentNumber}, ${rawData.document.dateOfIssue},
    ${rawData.document.dateOfExpiry}, ${rawData.document.authority}, `;
    userValue = `${userValue} վարկային պայմանագիր N ${rawData.aggreementNumber}`;
    currentItem.printData.name = rawData.document.name;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  walletEncashmentOut(currentItem, rawData, date, event) {
    let userValue = '';
    userValue = `${rawData.document.documentNumber},  ${rawData.document.dateOfIssue},
    ${rawData.document.dateOfExpiry}  ${rawData.document.authority}, `;
    userValue = `${userValue} ${rawData.idramWallet}`;
    currentItem.printData.userValue = userValue;
    currentItem.printData.username = rawData.document.name;
    return currentItem;
  }

  internalEncashmentOutData(currentItem, rawData, date, event) {
    const userValue = '';
    currentItem.printData.name = rawData.notes;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  externalEncashmentInData(currentItem, rawData, date, event) {
    const userValue = '';
    // currentItem.printData.name = rawData.notes;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  externalEncashmentOutData(currentItem, rawData, date, event) {
    const userValue = '';
    // currentItem.printData.name = rawData.notes;
    currentItem.printData.details = rawData.details;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  ucomEncushmentInData(currentItem, rawData, date, event) {
    const userValue = '';
    currentItem.printData.userValue = userValue;
    return currentItem;
  }

  phoneByBackService(currentItem, rawData, date, event) {
    let userValue = '';
    userValue = ` Հեռախոսի գնում՝  ${rawData.model}, IMEI ${rawData.imei},
        Նոր հեռախոսի ամբողջական արժեք՝  ${rawData.totalPrice}, U!Pay-ում վճարման ենթակա  գումար՝  ${rawData.amount}`;
    currentItem.printData.name = rawData.name;
    currentItem.userValue = userValue;
    currentItem.printData.userValue = userValue;
    return currentItem;
  }
}
