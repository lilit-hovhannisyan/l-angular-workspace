import {Injectable, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {CashboxDataService} from '../cashbox/cashbox-data.service';
import {initDomAdapter} from '@angular/platform-browser/src/browser';
import {EncashmentDataService} from './encashment-data.service';

@Injectable()
export class EncashmentViewService {
  data;
  labeles;
  firtsParts = [];
  secondParts = [];
  firstPartsStr = '';
  secondPartsStr = '';
  outerContent = '';
  action = '';
  user;
  trStrleft = '';
  trStrRight = '';
  bools = {
    INTERNAL_IN : true,
    INTERNAL_OUT: true,
    EXTERNAL_IN: true,
    EXTERNAL_OUT: false,
    UCOM: true,
    LOAN_GOOD_CREDIT: false,
    LOAN_GLOBAL_CREDIT: false,
    LOAN_VARKS_AM: false,
    IDRAM: true,
    PHONE_BY_BACK_SERVICE: true
  };

  constructor(private encashmentDataService: EncashmentDataService) {
  }
  getView(type, data, user) {
    this.data = data;
    this.user = user;
    this.trStrRight = '';
    this.trStrleft = '';
    const printersViewMap = {
      INTERNAL_IN : this.encashmentDataService.internalEncashmentInData(data, user),
      INTERNAL_OUT: this.encashmentDataService.internalEncashmentOutData(data, user),
      EXTERNAL_IN: this.encashmentDataService.externalEncashmentInData(data, user),
      EXTERNAL_OUT: this.encashmentDataService.externalEncashmentOutData(data, user),
      UCOM: this.encashmentDataService.ucomEncushmentInData(data, user),
      LOAN_GOOD_CREDIT: this.encashmentDataService.goodCreditLoansEncashmentOut(data, user),
      LOAN_GLOBAL_CREDIT: this.encashmentDataService.globalCreditLoansEncashmentOut(data, user),
      LOAN_VARKS_AM: this.encashmentDataService.varksAmEncashmentOut(data, user),
      IDRAM: this.encashmentDataService.walletEncashmentOut(data, user),
      PHONE_BY_BACK_SERVICE: this.encashmentDataService.phoneByBackService(data, user),
      UCOM_WALLET_OUT: this.encashmentDataService.ucomWalletEncashmentOut(data, user)
    };
    const array = printersViewMap[type];
    this.trStrleft += array[0];
    this.trStrRight += array[1];
    return this.generateHtml(this.bools[type]);
  }

  generateHtml(rightPartBool = true) {
    this.firstPartsStr = this.cretatTheadPart() + this.trStrleft + this.createTableFooterPart();
    this.secondPartsStr = this.cretatTheadPart() + this.trStrRight + this.createTableFooterPart();
    if (rightPartBool) {
      return `<div style="display: flex;flex-direction: row;">
        <div style="width:50%;">${this.firstPartsStr}</div>
        <div style="width:50%; ">${this.secondPartsStr}</div>
      </div>`;
    } else {
      return `<div style="display: flex;flex-direction: row;">
        <div style="width:50%;">${this.firstPartsStr}</div>
      </div>`;
    }
  }

  cretatTheadPart() {
    return `
      <table><thead>
        <tr style="height: 40px;">
          <th colspan="1" style="font-size: 11px;border-bottom: dotted 1px;">«ՅՈւՓԵՅ»   ՓԲԸ,  ${this.data['branchName']} մ/ճ</th>
          <th colspan="3" style="font-size: 12px;border-bottom: dotted 1px;"></th>
        </tr>
      </thead>
      <tbody><tr><td></td><td style="font-size: 8px;border:none;">/կազմակերպության անվանումը/</td><td colspan="2"></td></tr>`;
  }

  createTableFooterPart() {
    return `</tbody></table> `;
  }
}
