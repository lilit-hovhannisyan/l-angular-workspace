import {Injectable, Output, EventEmitter, OnDestroy} from '@angular/core';
import {BehaviorSubject, Subscription, Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {CashboxDataService} from '../cashbox/cashbox-data.service';
import {initDomAdapter} from '@angular/platform-browser/src/browser';
import {EncashmentDataService} from './encashment-data.service';
import {EncashmentService} from './encashment.service';
import {PopupsService} from '../../popups/popups.service';
import {Branch} from 'upay-lib';


@Injectable()
export class EncashmentPrintDataService implements OnDestroy {
  currentDate: any;
  activeTab: any;
  getCurrentBranche: Subscription;
  getNumToText: Subscription;
  printersViewMap: any;

  constructor(
    private encashmentDataService: EncashmentDataService,
    private popupsService: PopupsService,
    private encashmentService: EncashmentService,
  ) {
    this.printersViewMap = {
      INTERNAL_IN: this.internalEncashmentInData,
      INTERNAL_OUT: this.internalEncashmentOutData,
      EXTERNAL_IN: this.externalEncashmentInData,
      EXTERNAL_OUT: this.externalEncashmentOutData,
      UCOM: this.ucomEncushmentInData,
      LOAN_GOOD_CREDIT: this.goodCreditLoansEncashmentOut,
      LOAN_GLOBAL_CREDIT: this.globalCreditLoansEncashmentOut,
      LOAN_VARKS_AM: this.varksAmEncashmentOut,
      IDRAM: this.walletEncashmentOut,
      PHONE_BY_BACK_SERVICE: this.phoneByBackService,
      UCOM_WALLET_OUT: this.ucomWalletOut
    };
  }


  // getPrintTemplate(templateType) {}console.log(' goodCreditLoansEncashmentOut');
  //   console.log(data['userValue']);



  collectPrinterData(type, currentItem, currentDate, activeTab) {
    currentItem = currentItem;
    this.currentDate = currentDate;
    this.activeTab = activeTab;
    if (currentItem['data'].document) {
      currentItem.document = currentItem['data'].document;
      currentItem['data'].document.documentNumber = currentItem['data'].documentNumber;
      delete currentItem['data'].documentNumber;
      currentItem['data'].document.dateOfIssue = currentItem['data'].dateOfIssue;
      delete currentItem['data'].dateOfIssue;
      currentItem['data'].document.dateOfExpiry = currentItem['data'].dateOfExpiry;
      delete currentItem['data'].dateOfExpiry;
      currentItem['data'].document.authority = currentItem['data'].authority;
      delete currentItem['data'].authority;
    }


    this.getCurrentBranche = this.encashmentService.getCurrentBranch().subscribe(res => {
      let _amount = currentItem.amount || currentItem['data']['amount'];
      _amount = + _amount;

      this.getNumToText = this.encashmentService.getNumToText(Math.abs(_amount)).subscribe(amount => {
        currentItem.printData = this.setPrintData(currentItem, res, amount, currentDate);
        currentItem.printData.branchName = res.name;
        currentItem.printData.branchId = res.id;
        currentItem.printData.amountText = amount;
        currentItem.printData.amount = _amount;

        // console.log('currentItem.printData', currentItem.printData);
        currentItem = this.printersViewMap[type](type, currentItem, this.currentDate, this.activeTab);

        this.popupsService.openEncasmentionPopUp(currentItem);
      });
    });
  }

  ngOnDestroy() {
    if (this.getCurrentBranche) {
      this.getCurrentBranche.unsubscribe();
    }
    if (this.getNumToText) {
      this.getNumToText.unsubscribe();
    }
  }

  setPrintData(currentItem, res, amount, currentDate) {
    return {
      name: currentItem['data'].name,
      username: currentItem['data'].username,
      surname: currentItem['data'].surname,
      details: currentItem['data'].details,
      amount: currentItem['data'].amount,
      type: currentItem['type'],
      branchName: res.name,
      branchId: res.id,
      amountText: amount,
      orderId: '~~',
      contract: currentItem['data'].contract || 0,
      isComfirm: true,
      date: currentDate.date,
      userValue: '',
      contactNum: currentItem.contactNum,
    };
  }

  internalEncashmentInData(data, currentItem, currentDate, activeTab) {
    const userValue = '';
    currentItem['printData'].in = activeTab === 'in' ? true : false;
    return currentItem;
  }

  internalEncashmentOutData(data, currentItem, currentDate, activeTab) {
    const userValue = '';
    currentItem.data.username = currentItem['data'].username;
    currentItem['printData'].in = activeTab === 'in' ? true : false;
    return currentItem;
  }

  externalEncashmentInData(data, currentItem, currentDate, activeTab) {
    const userValue = '';
    // currentItem['data'].username = currentItem['data'].username;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }

  externalEncashmentOutData(type, currentItem, currentDate, activeTab) {
    const userValue = '';
    // currentItem['data'].username = currentItem['data'].username;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }

  ucomEncushmentInData(type, currentItem, currentDate, activeTab) {
    const userValue = '';
    currentItem['data'].username = currentItem['data'].username;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }

  varksAmEncashmentOut(data, currentItem, currentDate, activeTab) {
    let userValue = '';
    userValue = `${currentItem.document.documentNumber}, ${currentItem.document.dateOfIssue},
      ${currentItem.document.dateOfExpiry}, ${currentItem.document.authority}, վարկային պայմանագիր N  ${currentItem['data'].aggreementNumber} `;
    currentItem['data'].notes = currentItem['data'].name;
    currentItem['printData'].userValue = userValue;
    currentItem['data'].document.name = currentItem['data'].name;
    delete currentItem['data'].name;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }

  goodCreditLoansEncashmentOut(data, currentItem, currentDate, activeTab) {
    let userValue = '';
    currentItem['data'].notes = currentItem['data'].name;
    userValue = `${currentItem.document.documentNumber}, ${currentItem.document.dateOfIssue},
      ${currentItem.document.dateOfExpiry},
      ${currentItem.document.authority}, վարկային պայմանագիր N  ${currentItem['data'].aggreementNumber}`;
    currentItem['printData'].userValue = userValue;
    currentItem['data'].document.name = currentItem['data'].name;
    delete currentItem['data'].name;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    // console.log('printData++++++++++', currentItem['printData']);
    return currentItem;
  }

  globalCreditLoansEncashmentOut(data, currentItem, currentDate, activeTab) {
    let userValue = '';
    currentItem['data'].notes = currentItem['data'].name;
    userValue = `${currentItem.document.documentNumber}, ${currentItem.document.dateOfIssue},
      ${currentItem.document.dateOfExpiry},
      ${currentItem.document.authority}, վարկային պայմանագիր N  ${currentItem['data'].aggreementNumber}`;
    currentItem['printData'].userValue = userValue;
    currentItem['data'].document.name = currentItem['data'].name;
    delete currentItem['data'].name;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    // console.log('printData++++++++++', currentItem['printData']);
    return currentItem;
  }

  walletEncashmentOut(data, currentItem, currentDate, activeTab) {
    let userValue = '';
    userValue = `${currentItem.document.documentNumber}, ${currentItem.document.dateOfIssue},
    ${currentItem.document.dateOfExpiry}, ${currentItem.document.authority}, `;
    userValue += ` ${currentItem['data'].idramWallet}`;
    currentItem['printData'].userValue = userValue;
    currentItem['printData'].username = currentItem['data'].name;
    currentItem['data'].document.name = currentItem['data'].name;
    delete currentItem['data'].name;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }

  phoneByBackService(data, currentItem, currentDate, activeTab) {
    let userValue = '';
    userValue = ` Հեռախոսի գնում՝ ${currentItem['data'].model} , IMEI ${currentItem['data'].imei}
      , Նոր հեռախոսի ամբողջական արժեք՝  ${currentItem['data'].totalPrice}
      , U!Pay-ում վճարման ենթակա  գումար  ${currentItem['data'].amount}`;
    currentItem['printData'].userValue = userValue;
    currentItem['data'].name = currentItem['data'].name;
    currentItem['printData'].in = activeTab === 'in' ? true : false;
    return currentItem;
  }

  ucomWalletOut(data, currentItem, currentDate, activeTab) {
    const userValue = `${currentItem.ucomWallet} ID-ով հաճախորդի դրամապանակի կանխիկացում,
    ${currentItem.docType} ${currentItem.documentNumber},
    տրվել է ${currentItem.dateOfIssue} ${currentItem.authority}-ի կողմից,
    վավեր է մինչև ${currentItem.dateOfExpiry}`;

    currentItem['printData'].userValue = userValue;
    currentItem['printData'].username = currentItem.name + ' ' + currentItem.surname;
    currentItem['printData'].amountText = currentItem.printData.amountText;
    currentItem['printData'].in = this.activeTab === 'in' ? true : false;
    return currentItem;
  }
}
