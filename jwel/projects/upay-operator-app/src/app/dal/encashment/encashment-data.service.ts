import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class EncashmentDataService {
  styles = {};
  leftPart = '';
  rightPart = '';
  data: any;

  constructor() {
    this.styles = {
      height30: 'height:23px',
      width70: 'width:70px;height:50px;',
      noBorderBoldFont12: 'font-size:12px;border:none;font-weight: 600;',
      noBorderBoldFont18: 'font-size:18px;border:none;font-weight: 600;',
      noBorderFont8: 'font-size:8px;border:none;',
      noBorder: 'font-size:12px;border:none',
      dottedBottom: 'font-size:12px;border-bottom: dotted 1px',
      dottedBoldBottom: 'font-size:12px;border-bottom: dotted 1px;font-weight: 600;',
      dottedTopFont8: 'font-size:8px;border-top: dotted 1px',
      dotted: 'font-size:12px;border: dotted 1px',
      marginLeftdottedBottom: 'margin-left:15px;font-size:12px;border-bottom: dotted 1px',
      marginRightdottedBottom: 'margin-right:0px;font-size:12px;border-bottom:dotted 1px'
    };
  }

  createTextWithUnderLineText(lableArray, underLineText, isRight = true, isLeft = true) {
    this.createTableRow([1, 3],
      [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], lableArray, isRight, isLeft);
    this.createTableRow([2, 1, 1], [null, null, this.styles['noBorderFont8']], ['', '', underLineText], isRight, isLeft);
  }

  createEmptyLine(isRight = true, isLeft = true) {
    this.createTableRow([4], [this.styles['dottedBottom']], [''], isRight, isLeft);
  }

  createlableWithEmptyLine(lables, underText, isRight = true, isLeft = true) {
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], lables, isRight, isLeft);
    this.createTableRow([4], [this.styles['dottedBottom']], [''], isRight, isLeft),
      this.createTableRow([2, 1, 1], [null, null, this.styles['noBorderFont8']], ['', '', underText], isRight, isLeft);
  }

  createFooterPart(user) {
    this.createTableRow([1, 2, 1], [this.styles['noBorderBoldFont12'], this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Գանձապահ', user['forename'] + '  ' + user['surname'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8']], ['', 'ստորագրություն']);
  }

  createHeaderPart(data, title, detalis, rightBool = true, leftBool = true) {
    this.createTableRow([2, 1, 1],
      [this.styles['noBorderBoldFont18'], this.styles['dottedBottom'], this.styles['marginRightdottedBottom']],
      [title, '', data['date']]);
    this.createTableRow([2, 1, 1], [null, null, this.styles['noBorderFont8']], ['', '', 'Կազմման ամսաթիվ']);

    this.createTableRow([1], [this.styles['dotted']], ['Թղթակցող հաշիվը', 'Վերլուծական հաշվառման ծածկագիրը', 'Գումարը', 'Նպատակային նշանակության ծածկագիրը'], rightBool);
    this.createTableRow([1], [this.styles['dotted']], ['1', '2', '3', '4'], rightBool);
    this.createTableRow([1], [this.styles['dotted']], [data['code'] || '', '', data['amount'], detalis], rightBool);
  }

  createTableRow(cols, styleStr, texts, rightBool = true, leftBool = true, trStyle = this.styles['height30']) {
    let str = `<tr style="${trStyle}">`;
    for (let i = 0; i < texts.length; i++) {
      const style = styleStr.length < texts.length ? styleStr[0] : styleStr[i];
      const colspan = cols.length < texts.length ? cols[0] : cols[i];
      str += `<td colspan="${colspan}"  style="${style}">${texts[i]}</td>`;
    }
    str += '</tr>';
    if (rightBool) {
      this.rightPart += str;
    }
    if (leftBool) {
      this.leftPart += str;
    }
  }

  createSignatureBlok(text1, text2, undertext1, undertext2, isRight = true, isLeft = true) {
    this.createTableRow([1],
      [this.styles['noBorderBoldFont12'], this.styles['dottedBottom'], this.styles['noBorderBoldFont12'], this.styles['marginLeftdottedBottom']],
      [text1, '', text2, ''], isRight, isLeft);
    this.createTableRow([1],
      [null, this.styles['noBorderFont8'], this.styles['noBorder'], this.styles['noBorderFont8']],
      ['', undertext1, '', undertext2], isRight, isLeft);
  }

  walletEncashmentOut(data, user) {
    // console.log(' walletEncashmentOut');
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Կանխիկացում');
    this.createTextWithUnderLineText(['Ստացող', data['username']], '/անուն, ազգանուն/');
    this.createTextWithUnderLineText(['Հիմքը', data['userValue']], '');
    this.createTextWithUnderLineText(['Գումարը տառերով', data['amountText'] + ' ՀՀ դրամ'], '/գումարը տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա', ''], '/տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  varksAmEncashmentOut(data, user) {
    // console.log(' varksAmEncashmentOut.............');
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, '«ՎԱՐԿՍ ԷՅ ԱՄ» ունիվերսալ վարկային կազմակերպության վարկի տրամադրում');
    this.createTextWithUnderLineText(['Ստացող', data['name']], '/անուն, ազգանուն/');
    this.createTextWithUnderLineText(['Հիմքը', data['userValue']], '');
    this.createTextWithUnderLineText(['Գումարը տառերով', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա', ''], '/գումարը տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  goodCreditLoansEncashmentOut(data, user) {
    // console.log(' goodCreditLoansEncashmentOut');
    // console.log(data['userValue']);
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Գուդկրեդիտ ՈՒՎԿ ՓԲԸ-ի կողմից հաստատաված վարկի տրամադրում');
    this.createTextWithUnderLineText(['Ստացող', data['name']], '/անուն, ազգանուն/');
    this.createTextWithUnderLineText(['Հիմքը', data['userValue']], '');
    this.createTextWithUnderLineText(['Գումարը տառերով', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա', ''], '/գումարը տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  globalCreditLoansEncashmentOut(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Գլոբալկրեդիտ ՈՒՎԿ ՓԲԸ-ի կողմից հաստատաված վարկի տրամադրում');
    this.createTextWithUnderLineText(['Ստացող', data['name']], '/անուն, ազգանուն/');
    this.createTextWithUnderLineText(['Հիմքը', data['userValue']], '');
    this.createTextWithUnderLineText(['Գումարը տառերով', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա', ''], '/գումարը տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  externalEncashmentOutData(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Արմենիա ինկասացիոն ծառայություն ՓԲԸ');
    this.createTextWithUnderLineText(['Ստացող', 'Արմենիա Ինկասացիոն Ծառայություն ՓԲԸ ինկասատոր ` '], '');
    this.createTextWithUnderLineText(['', ''], 'անուն, ազգանուն/');
    this.createTableRow([1, 2, 1], [this.styles['dottedBoldBottom'], this.styles['dottedBottom']], ['', 'վկայական՝   ', '']);
    this.createTextWithUnderLineText(['Հիմքը', 'Առաքվող արժեքների ցուցակ` ' + data.details], '');
    this.createTextWithUnderLineText(['Գումարը', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Ստացա']);
    this.createTableRow([4], [this.styles['dottedBottom']], [''], true);
    this.createTableRow([2, 1, 3], [null, null, this.styles['noBorderFont8']], ['', '', '/տառերով/']);
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  externalEncashmentInData(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային մուտքի օրդեր N ' + data.orderId, 'Արմենիա ինկասացիոն ծառայություն ՓԲԸ', false);
    this.createTextWithUnderLineText(['Ստացող', 'Արմենիա Ինկասացիոն Ծառայություն ՓԲԸ ինկասատոր ` '], '');
    this.createTextWithUnderLineText(['', ''], 'անուն, ազգանուն/');
    this.createTableRow([1, 2, 1], [this.styles['dottedBoldBottom'], this.styles['dottedBottom']], ['', 'վկայական՝', '']);
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Ստացման հիմքը և նպատակը ', 'Մուտք դրամարկղ Արմենիա ինկասացիայից']);
    this.createTableRow([4], [this.styles['dottedBottom']], ['']);
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Գումարը', data['amount'] + ' ՀՀ դրամ']);
    this.createTextWithUnderLineText(['', data['amountText']], '/տառերով/');
    this.createTableRow([1, 1, 2], [this.styles['noBorderBoldFont12'], this.styles['noBorderBoldFont12'], this.styles['marginLeftdottedBottom']],
      ['Կ.Տ.', 'Գլխավոր հաշվապահ', ''], true, false, this.styles['width70']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8']], ['', 'ստորագրություն'], true, false);
    this.createTableRow([4], [this.styles['dottedBottom']], [''], false);
    this.createTextWithUnderLineText(['Կցվում են', ''], '/կցվող փաստաթղթերի էջերի թիվը/', false);
    this.createSignatureBlok('Վճարող', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն', false);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  internalEncashmentOutData(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Ելք  ներքին ինկասացիայից');
    this.createTextWithUnderLineText(['Ստացող', '«Յուփեյ » ՓԲԸ  պատասխանտու անձ ` ' + data['username']], '/անուն, ազգանուն/');
    this.createEmptyLine();
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Հիմքը', 'Առաքվող արժեքների ցուցակ  ' + data.details]);
    this.createEmptyLine();
    this.createTextWithUnderLineText(['Գումարը', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա'], '/տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  internalEncashmentInData(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային մուտքի օրդեր N ' + data.orderId, 'Մուտք ներքին ինկասացիայից', false);
    this.createTextWithUnderLineText(['Ստացված է', 'Յուփեյ ՓԲԸ պատասխանատու անձ ` ' + data['username'] + '-ից'], '/անուն, ազգանուն/');
    this.createEmptyLine();
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Ստացման հիմքը և նպատակը ', 'Մուտք դրամարկղ ներքին ինկասացիայից']);
    this.createEmptyLine();
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Գումարը', data['amount'] + ' ՀՀ դրամ']);
    this.createTextWithUnderLineText(['', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createTableRow([1, 1, 2], [this.styles['noBorderBoldFont12'], this.styles['noBorderBoldFont12'], this.styles['marginLeftdottedBottom']],
      ['Կ.Տ.', 'Գլխավոր հաշվապահ', ''], true, false, this.styles['width70']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8']], ['', 'ստորագրություն'], true, false);
    this.createTextWithUnderLineText(['Կցվում են', ''], '/կցվող փաստաթղթերի էջերի թիվը/', false);
    this.createSignatureBlok('Վճարող', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն', false);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  ucomEncushmentInData(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային մուտքի օրդեր N ' + data.orderId, 'Յուքոմ ՓԲԸ-ից մուտք', false);
    this.createTextWithUnderLineText(['Ստացված է', 'Յուքոմ ՓԲԸ պատասխանատու անձ ` ' + data['username'] + '-ից N ' + data['contract']], '/անուն, ազգանուն/');
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Ստացման հիմքը և նպատակը ', 'Մուտք դրամարկղ']);
    this.createEmptyLine();
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Գումարը', data['amount'] + ' ՀՀ դրամ']);
    this.createTextWithUnderLineText(['', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    this.createTextWithUnderLineText(['Կցվում են', ''], '/կցվող փաստաթղթերի էջերի թիվը/', false);
    this.createSignatureBlok('Վճարող', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն', false, true);
    this.createTableRow([1, 1, 2], [this.styles['noBorderBoldFont12'], this.styles['noBorderBoldFont12'], this.styles['marginLeftdottedBottom']],
      ['Կ.Տ.', 'Գլխավոր հաշվապահ', ''], true, false, this.styles['width70']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8']], ['', 'ստորագրություն'], true, false);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }

  phoneByBackService(data, user) {
    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային մուտքի օրդեր N ' + data.orderId, 'Յուքոմ ՓԲԸ, հեռախոսի գնում', false);
    this.createTextWithUnderLineText(['Ստացված է',  data['name'] + '-ից'], '/անուն, ազգանուն/');
    this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['Ստացման հիմքը և նպատակը ', data.userValue]);
    this.createEmptyLine();
    this.createTextWithUnderLineText(['Գումարը', data['amountText'] + ' ՀՀ դրամ'], '/տառերով/');
    // this.createTableRow([1, 3], [this.styles['noBorderBoldFont12'], this.styles['dottedBottom']], ['', data['amount'] + ' ՀՀ դրամ']);
    this.createEmptyLine();
    this.createSignatureBlok('Վճարող', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն', false, true);
    this.createTableRow([1, 1, 2], [this.styles['noBorderBoldFont12'], this.styles['noBorderBoldFont12'], this.styles['marginLeftdottedBottom']],
      ['Կ.Տ.', 'Գլխավոր հաշվապահ', ''], true, false, this.styles['width70']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8']], ['', 'ստորագրություն'], true, false);
    this.createFooterPart(user);
    return [this.leftPart, this.rightPart];
  }


  ucomWalletEncashmentOut(data, user) {

    this.leftPart = '';
    this.rightPart = '';
    this.createHeaderPart(data, 'Դրամարկղային ելքի օրդեր N ' + data.orderId, 'Կանխիկացում');
    this.createTextWithUnderLineText(['Ստացող', data['username'] + ' ' + data['surname']], '/անուն, ազգանուն/');
    this.createTextWithUnderLineText(['Հիմքը', data['userValue']], '');
    this.createTextWithUnderLineText(['Գումարը տառերով', data['amountText'] + ' ՀՀ դրամ'], '/գումարը տառերով/');
    this.createSignatureBlok('Կազմակերպության ղեկավար ', 'Գլխավոր հաշվապահ', 'ստորագրություն ', 'ստորագրություն');
    this.createlableWithEmptyLine(['Ստացա', ''], '/տառերով/');
    this.createTableRow([1], [this.styles['noBorder']], ['', data['date'], '']);
    this.createTableRow([3, 1], [this.styles['noBorderFont8'], this.styles['dottedTopFont8']], ['', 'ստորագրություն']);
    this.createFooterPart(user);

    return [this.leftPart, this.rightPart];
  }
}
