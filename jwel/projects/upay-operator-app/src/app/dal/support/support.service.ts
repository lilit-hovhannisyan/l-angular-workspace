import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs/index';
import {UserDataService} from '../user/user-data.service';
import {EncashmentService} from '../encashment/encashment.service';
import {CashboxDataService} from '../cashbox/cashbox-data.service';
import {PrintService} from '../../misc-services/print.service';
import {
  AlertBoxService,

  OperatorGetActivityLogByCustomerIdQuery,
  OperatorGetCitiesQuery,
  OperatorGetCountriesQuery,
  OperatorGetCustomerCardsQuery
} from 'upay-lib';
import { v4 as uuid } from 'uuid';


import {FileUploadApi} from 'upay-lib';

import {
  OfficeApi, Page, Order,
  OperatorGetCustomerQuery,
  OperatorGetCustomersQuery,
  OperatorGetIdentificationDataQuery, OperatorSetIdentificationDataCommand,
  CustomerCashoutCommand,
  OperatorTerminateCustomerCommand,
  OperatorGetCustomerTransactionsQuery,
  // CashPayUpayCustomerTopupCommand,

  AccountantFreezeAccountCommand,
  AccountantUnfreezeAccountCommand
} from 'upay-lib';


@Injectable()
export class SupportService {

  _user = {};
  currentUser = new BehaviorSubject(this._user);

  _cashout = {};
  cashoutSub = new BehaviorSubject(this._cashout);

  _cashoutSubUpdate = {};
  cashoutSubUpdate = new BehaviorSubject(this._cashoutSubUpdate);

  _cashin = {};
  cashinSub = new BehaviorSubject(this._cashin);

  @Output() updatedUserEvent = new EventEmitter();

  constructor(private officeApi: OfficeApi,
              private userDataService: UserDataService,
              private encashmentService: EncashmentService,
              private printService: PrintService,
              private alertBoxService: AlertBoxService,
              private cashboxDataService: CashboxDataService,
              private fileUploadApi: FileUploadApi) {}


  // user search
  getUsers(query) {
    // const page = new Page();
    // page.index = 0;
    // page.size = 10;
    const order = new Order();
    order.fields = [];

    const operatorGetCustomersQuery = new OperatorGetCustomersQuery();

    operatorGetCustomersQuery.surname = query.surname;
    operatorGetCustomersQuery.forename = query.forename;
    operatorGetCustomersQuery.phone = query.phone;
    operatorGetCustomersQuery.number = query.number;
    operatorGetCustomersQuery.page = query.page;
    operatorGetCustomersQuery.order = order;

    return this.officeApi.getCustomers(operatorGetCustomersQuery);
  }

  getCustomer(query) {
    const operatorGetCustomerQuery = new OperatorGetCustomerQuery();

    operatorGetCustomerQuery.phone = query.phone;
    operatorGetCustomerQuery.customerId = query.customerId;

    return this.officeApi.getCustomer(operatorGetCustomerQuery);
  }

  setUser(user) {
    this._user = user;
    this.currentUser.next(this._user);
  }

  destroyUser() {
    this._user = {};
    this.currentUser.next(this._user);
  }

  updateUser(userId) {
    this.getIdentifiedUser(userId).subscribe( identificationData => {
      const userData = identificationData[0];
      this._user['forename'] = userData.nameAm;
      this._user['surname'] = userData.surnameAm;
      this._user['userType'] = 'BRANCH_IDENTIFIED';
      this.updatedUserEvent.emit(this._user);
    });
  }

  getUser() {
    return this.updatedUserEvent;
  }

  // changeUserStatus() {
  //   const customerId = this._user['number'];
  //   this._user['userType'] = 'BRANCH_IDENTIFIED';
  //   this.currentUser.next(this._user);
  // }

  // user info
  getBankingCards( customerNum: string) {

    const operatorGetCustomerCardsQuery = new OperatorGetCustomerCardsQuery();
    operatorGetCustomerCardsQuery.customerNum = customerNum;
    return this.officeApi.getCustomerCards(operatorGetCustomerCardsQuery);

  }

  // unfreezeAccount claim
  openUnfreezeAccountClaim(customerId: string, comment?: string, attachment?: string) {
    // const operatorOpenUnfreezeClaimCommand = new OperatorOpenUnfreezeClaimCommand();
    // operatorOpenUnfreezeClaimCommand.customerNum = customerId;
    // operatorOpenUnfreezeClaimCommand.comment = comment;
    // operatorOpenUnfreezeClaimCommand.attachment = attachment;
    // return this.officeApi.openUnfreezeClaim(operatorOpenUnfreezeClaimCommand);
  }

  unfreezeAccount(customerId: string, comment?: string) {
    // temporary solution untill the claim flow will be approved
    const accountantUnfreezeAccountCommand = new AccountantUnfreezeAccountCommand();
    accountantUnfreezeAccountCommand.customerNumber = customerId;
    accountantUnfreezeAccountCommand.comment = comment;

    return this.officeApi.unfreezeAccount(accountantUnfreezeAccountCommand);

  }

  // identify user
  getIdentifiedUser(id: string) {
    const operatorGetIdentificationDataQuery = new OperatorGetIdentificationDataQuery();
    operatorGetIdentificationDataQuery.customerId = id;

    return this.officeApi.getIdentificationData(operatorGetIdentificationDataQuery);
  }

  identifyUser(identificationData) {
    console.log('idenfityUser', identificationData);
    const operatorSetIdentificationDataCommand = new OperatorSetIdentificationDataCommand();
    operatorSetIdentificationDataCommand.customerId = identificationData.customerId;
    operatorSetIdentificationDataCommand.nameAm = identificationData.nameAm;
    operatorSetIdentificationDataCommand.surnameAm = identificationData.surnameAm;
    operatorSetIdentificationDataCommand.middleNameAm = identificationData.middleNameAm;
    operatorSetIdentificationDataCommand.nameEn = identificationData.nameEn;
    operatorSetIdentificationDataCommand.surnameEn = identificationData.surnameEn;
    operatorSetIdentificationDataCommand.sex = identificationData.sex;
    operatorSetIdentificationDataCommand.citizen = identificationData.citizen;
    operatorSetIdentificationDataCommand.address = identificationData.address;
    operatorSetIdentificationDataCommand.city = identificationData.city;
    operatorSetIdentificationDataCommand.country = identificationData.country;
    operatorSetIdentificationDataCommand.docType = identificationData.docType;
    operatorSetIdentificationDataCommand.media = identificationData.media;
    operatorSetIdentificationDataCommand.documentNumber = identificationData.documentNumber;
    operatorSetIdentificationDataCommand.socCardNumber = identificationData.socCardNumber;
    operatorSetIdentificationDataCommand.dateOfBirth = new Date(identificationData.dateOfBirth);
    operatorSetIdentificationDataCommand.dateOfIssue = new Date(identificationData.dateOfIssue);
    operatorSetIdentificationDataCommand.dateOfExpiry = new Date(identificationData.dateOfExpiry);
    operatorSetIdentificationDataCommand.authority = identificationData.authority;

    return this.officeApi.setIdentificationData(operatorSetIdentificationDataCommand);

  }

  // send for review user
  sendForReview(user) {
    alert('no api');
    console.log('sendForReview', user);
  }

  // cashout
  cashoutCustomer(customerId: string, amount: number, order, contactNum) {
    this._cashout = {};
    this.cashoutSub.next(this._cashout);


    const customerCashoutCommand = new CustomerCashoutCommand();
    customerCashoutCommand.orderId = order;
    customerCashoutCommand.amount = amount;
    customerCashoutCommand.customerNumber = customerId;
    customerCashoutCommand.contactNum = contactNum;

    this.officeApi.customerCashout(customerCashoutCommand).subscribe( res => {
      this._cashout = { cashoutAmount: amount};
      this.cashoutSub.next(this._cashout);
    });

  }

  updateBalance(updateAmount: number) {
    this._cashoutSubUpdate = { cashoutAmount: updateAmount };
    console.log('updateAmount ' + updateAmount);

    this.cashoutSubUpdate.next(this._cashoutSubUpdate);
  }


  // closeAccount
  terminateAccount(customerId: string, reason: string) {
    const operatorTerminateCustomerCommand = new OperatorTerminateCustomerCommand();
    operatorTerminateCustomerCommand.customerId = customerId;
    operatorTerminateCustomerCommand.reason = reason;

    return this.officeApi.terminateCustomer(operatorTerminateCustomerCommand);
  }

  // freezeAccount
  freezeAccount(customerId: string, reason: string) {
    const accountantFreezeAccountCommand = new AccountantFreezeAccountCommand();
    accountantFreezeAccountCommand.customerNumber = customerId;
    accountantFreezeAccountCommand.comment = reason;

    return this.officeApi.freezeAccount(accountantFreezeAccountCommand);
  }


  // // cashin
  // cashinCustomer(customerId: string, amount: number) {
  //   this._cashin = {};
  //   this.cashinSub.next(this._cashin);
  //
  //   const cashPayUpayCustomerTopupCommand = new CashPayUpayCustomerTopupCommand();
  //   cashPayUpayCustomerTopupCommand.customerNumber = customerId;
  //   cashPayUpayCustomerTopupCommand.amount = amount;
  //   cashPayUpayCustomerTopupCommand.transactionId = uuid();
  //   cashPayUpayCustomerTopupCommand.clientData = `{"value":"${customerId}", "name":"Upay դրամապանակ"}`;
  //   cashPayUpayCustomerTopupCommand.commission = 0;
  //
  //   this.officeApi.cashPayUpayCustomerTopup(cashPayUpayCustomerTopupCommand).subscribe( res => {
  //
  //     this.cashboxDataService.getTransaction(cashPayUpayCustomerTopupCommand.transactionId).subscribe( transaction => {
  //       if ( transaction ) {
  //         this.encashmentService.getCurrentBranch().subscribe( branch => {
  //           this.alertBoxService.initMsg({type: 'success', text: `Համալրեցիք ձեր դրամապանակը ${amount} դրամով`});
  //           const printList = [];
  //           const commission = 0; // todo yet, hardcoded
  //
  //           let fields: any[] = [];
  //           const transactionData = [
  //             {
  //               name: 'Upay դրամապանակ',
  //               value: customerId
  //             },
  //             {
  //               name: 'Անդորրագիր',
  //               value: transaction.receiptId
  //             },
  //             {
  //               name: 'Վճարման նպատակ',
  //               value: transaction.purpose
  //             }
  //           ]; // check header | comes from getTransaction api call
  //           const totalDetails = [
  //             {
  //               name: 'Վճարվել է',
  //               value: amount + ''
  //             },
  //             {
  //               name: 'Միջնորդավճար',
  //               value: commission
  //             }
  //           ];
  //           fields = [...transactionData, 'hr', ...totalDetails];
  //
  //           const printEl = {
  //             totallyPaid: +amount + +commission ,
  //             branchName: branch.name,
  //             fields: fields
  //           };
  //
  //           printList.push(printEl);
  //           this.printService.createInvoice(printList);
  //
  //
  //           this._cashin = {cashinAmount: amount, transaction: transaction};
  //           this.cashinSub.next(this._cashin);
  //         });
  //       }
  //     });
  //   }, err => {
  //
  //   });
  // }
  // activity log
  getActivityLogByCustomerId(queryData) {
    // const page = new Page();
    // const order = new Order();
    //
    // const operatorGetActivityLogByCustomerIdQuery = new OperatorGetActivityLogByCustomerIdQuery();
    // operatorGetActivityLogByCustomerIdQuery.customerId = customerId;
    // operatorGetActivityLogByCustomerIdQuery.page = page;
    // operatorGetActivityLogByCustomerIdQuery.order = order;
    return this.officeApi.getActivityLogByCustomerId(queryData);
  }

  // transaction log
  getTransactionLog(queryData) {
    return this.officeApi.getCustomerTransactions(queryData);
  }

  // file upload
  createGroup(groupName: string) {
    return this.fileUploadApi.createGroup(groupName);
  }

  fileUpload(groupName: string, fileName: string, file: File) {
    const options = [
      { key: 'reportProgress'},
      { observe: 'events'}
    ];

    const formData = new FormData();
    formData.append('file', file);

    return this.fileUploadApi.addFilesInGroup(groupName, fileName, formData, options);
  }

  getFilesInGroup(groupName) {
    return this.fileUploadApi.getFilesInGroup(groupName);
  }

  // select boxes
  getCountries() {
    const operatorGetCountriesQuery = new OperatorGetCountriesQuery();
    return this.officeApi.getCountries(operatorGetCountriesQuery);
  }

  getCities(countryId: string) {
    const operatorGetCitiesQuery = new OperatorGetCitiesQuery();
    operatorGetCitiesQuery.countryId = countryId;
    return this.officeApi.getCities(operatorGetCitiesQuery);
  }
}
