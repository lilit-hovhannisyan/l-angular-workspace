import {IPair} from 'iu-ui-lib/lib/global-types';
import {SubscriberData} from 'upay-lib';
import {DateTimeService} from 'upay-lib';

export class UserInfoData {
  cashBoxId: string;
  email: string;
  enabled: boolean;
  forename: string;
  id: string;
  number: string;
  // password: string;
  phone: string;
  scope: string[];
  scopeStream: string[];
  status:   string;
  surname: string;
  username: string;

  constructor(data) {
    this.id = data.id;
    this.username = data.userName;
    this.cashBoxId = data.cashboxId;
    this.email = data.email;
    this.number = data.number;
    this.phone = data.phone;
    this.forename = data.forename;
    this.status = data.status.toLocaleLowerCase();
    this.surname = data.surname;
    this.scope = data.scope;
  }
}

export class UserBranchInfoData {
  address: string;
  code: string;
  description: string;
  enabled: boolean;
  id: string;
  name: string;
  policyId: string;

  constructor(data) {
    this.address = data.address;
    this.code = data.code;
    this.description = data.description;
    this.enabled = data.enabled;
    this.id = data.id;
    this.name = data.name;
    this.policyId = data.policyId;
  }
}
