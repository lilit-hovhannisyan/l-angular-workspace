import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs/index';

import { v4 as uuid } from 'uuid';
import {UserBranchInfoData, UserInfoData} from './user-data.models';
import {HttpClient} from '@angular/common/http';

import {SubscriberDataService} from '../../dal/subscriber/subscriber-data.service';
import {
  OfficeApi,
  OperatorAuthenticateCommand,
  OperatorGetAccessTokenQuery,
  OperatorGetProfileQuery,
  OperatorChangePasswordCommand,
  OperatorGetCurrentBranchQuery,
  OperatorGetOperatorByIdQuery,
  ResponseCacheService,
} from 'upay-lib';
import {LanguageService} from '../language.service';



@Injectable()
export class UserDataService {
  _isAuth: boolean;
  isAuth = new BehaviorSubject(this._isAuth);

  _cashboxId;
  cashboxId = new BehaviorSubject(this._cashboxId);

  _userInfo?: UserInfoData;
  userInfo = new BehaviorSubject(this._userInfo);

  _branchInfo?: UserBranchInfoData;
  branchInfo = new BehaviorSubject(this._branchInfo);

  constructor(protected http: HttpClient,
              private officeApi: OfficeApi,
              private responseCacheService: ResponseCacheService,
              private languageService: LanguageService,
              private subscriberDataService: SubscriberDataService) {


    if (localStorage.getItem('authToken')) {
      this._isAuth = true;
    } else {
      this._isAuth = false;
    }
    this.isAuth.next(this._isAuth);
  }

  signIn(username: string, password: string) {
    const id = uuid();
    const data = new OperatorAuthenticateCommand();
    data.password = password;
    data.userName = username;
    data.authenticationId = id;
    data.cashboxId = 'fd21b910-8399-42c0-b44f-a845a689a312'; // todo

    this.officeApi.authenticate(data).subscribe(authenticateData => {
      this.setUserToken(id);
    });
  }

  setUserToken(id) {
    const accessTokenRequestData = new OperatorGetAccessTokenQuery();
    accessTokenRequestData.authenticationId = id;
    this.officeApi.getAccessToken(accessTokenRequestData).subscribe(authToken => {
      localStorage.setItem('authToken', authToken);
      this._isAuth = true;
      this.isAuth.next(this._isAuth);
    });
  }

  signOut() {
    localStorage.removeItem('authToken');
    this._isAuth = false;
    this.isAuth.next(this._isAuth);
    this._userInfo = null;
    this.userInfo.next(this._userInfo);
    this._branchInfo = null;
    this.branchInfo.next(this._branchInfo);
  }

  resetPassword(oldPassword: string, newPassword: string, email: string) {
    const operatorResetPasswordCommand = new OperatorChangePasswordCommand();
    operatorResetPasswordCommand.oldPass = oldPassword;
    operatorResetPasswordCommand.newPass = newPassword;
    operatorResetPasswordCommand.email = email;

    return this.officeApi.changePassword(operatorResetPasswordCommand);
  }

  getProfile() {
    // if (!this._userInfo) {
    const operatorGetProfileQuery = new OperatorGetProfileQuery();
    this.officeApi.getProfile(operatorGetProfileQuery).subscribe(userRes => {
      this._userInfo = new UserInfoData(userRes);
      this.userInfo.next(this._userInfo);
      // this.languageService.changeLanguage(languageCode) //todo
      this.getUserBranch().subscribe(branchRes => {
        this._branchInfo = new UserBranchInfoData(branchRes);
        this.branchInfo.next(this._branchInfo);
      });
    });
  }

  refreshUserInfo() {
    this._userInfo = { ...this._userInfo };
    this.userInfo.next(this._userInfo);
  }

  getUserBranch() {
    const operatorGetCurrentBranchQuery = new OperatorGetCurrentBranchQuery();
    // return this.responseCacheService.cache(this.officeApi, 'getCurrentBranch', operatorGetCurrentBranchQuery);
    return this.officeApi.getCurrentBranch(operatorGetCurrentBranchQuery);
  }

  setUserLanguage(languageCode) {
    console.log('no api');
  }

  getOperatorById(operatorId: string) {
    const operatorGetOperatorByIdQuery = new OperatorGetOperatorByIdQuery();
    operatorGetOperatorByIdQuery.operatorId = operatorId;
    return this.officeApi.getOperatorById(operatorGetOperatorByIdQuery);
  }
}
