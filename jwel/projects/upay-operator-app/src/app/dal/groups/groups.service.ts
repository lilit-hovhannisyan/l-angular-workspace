import {EventEmitter, Injectable, Output} from '@angular/core';

import {
  OfficeApi,
  OperatorCreateGroupCommand,
  OperatorAddContractCommand, Contract,
  OperatorFindGroupQuery, Page, Order,
  OperatorRemoveContractCommand,
  OperatorDeleteGroupCommand,
  OperatorUpdateGroupCommand
} from 'upay-lib';

@Injectable()
export class GroupsService {
  constructor(private officeApi: OfficeApi) {
  }

  @Output() groupNameUpdate = new EventEmitter();

  getGroupsList() {
    console.log('no api');
  }

  createGroup( name: string, description: string) {
    const createGroupCommand = new OperatorCreateGroupCommand();
    createGroupCommand.name = name;
    // createGroupCommand.description = description;

    return this.officeApi.createGroup(createGroupCommand);
  }

  editGroup( name: string, groupId: string) {
    const operatorUpdateGroupCommand = new OperatorUpdateGroupCommand();
    operatorUpdateGroupCommand.name = name;
    operatorUpdateGroupCommand.groupId = groupId;

    return this.officeApi.updateGroup(operatorUpdateGroupCommand);
  }

  addContractToGroup(contract: Contract, groupName: string) {
    const _contract = new Contract();
    _contract.subscriber = contract.subscriber;
    _contract.branchId = contract.branchId;
    _contract.searchData = JSON.stringify(contract.searchData);
    _contract.providerId = contract.providerId;

    const operatorAddContractCommand = new OperatorAddContractCommand();
    operatorAddContractCommand.groupName = groupName;
    operatorAddContractCommand.contract = _contract;

    // debugger

    return this.officeApi.addContract(operatorAddContractCommand);
  }

  findGroup(name: string) {
    const operatorFindGroupQuery = new OperatorFindGroupQuery();

    const page = new Page();
    page.index = 0;
    page.size = 10000;

    const order = new Order();
    order.fields = [];

    operatorFindGroupQuery.name = name;
    operatorFindGroupQuery.page = page;
    operatorFindGroupQuery.order = order;

    return this.officeApi.findGroup(operatorFindGroupQuery);
  }

  removeContractFromGroup(contract, groupName: string) {
    const operatorRemoveContractCommand = new OperatorRemoveContractCommand();
    operatorRemoveContractCommand.subscriber = contract.transferMappedData.customerIdKey;
    operatorRemoveContractCommand.branchId = contract.customerDebtData.branchId || '';
    operatorRemoveContractCommand.providerId = contract.providerData.id;
    operatorRemoveContractCommand.groupName = groupName;

    return this.officeApi.removeContract( operatorRemoveContractCommand );
  }

  deleteGroup(groupName: string) {
    const operatorDeleteGroupCommand = new OperatorDeleteGroupCommand();
    operatorDeleteGroupCommand.name = groupName;

    return this.officeApi.deleteGroup(operatorDeleteGroupCommand);

    // this.officeApi.deleteGroup({groupId}).subscribe(res => {
    //   this.getGroupsList();
    // });
  }

  //
  updateGroupName(groupNewName) {
    this.groupNameUpdate.emit(groupNewName);
  }

  getUpdatedGroupName() {
    return this.groupNameUpdate;
  }
}
