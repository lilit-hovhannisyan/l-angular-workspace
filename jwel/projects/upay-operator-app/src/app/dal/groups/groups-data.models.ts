export class GroupItem {
  subscriber: string;
  providerName: string;
  providerId: string;
  branchId: string;
  amount: string;
  searchData: any;
  customerName?: string;
  address?: string;


  constructor(data) {
    let searchData = data.searchData;
    if ( typeof data.searchData === 'string' ) {
      searchData = JSON.parse(data.searchData);
    }
    this.subscriber = data.subscriber;
    this.providerName = searchData.providerName;
    this.providerId = data.providerId;
    this.branchId = data.branchId;
    this.amount = data.amount;
    this.searchData = searchData;
  }
}
