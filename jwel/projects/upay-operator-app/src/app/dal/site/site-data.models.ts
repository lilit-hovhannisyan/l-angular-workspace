export interface IClientTabData {
  iconClass: 'home' | 'pay' | 'topup' | 'transfer';
  link: string;
  text: string;
}

export class ClientTabView {
  iconClass: string;
  link: string;
  text: string;

  constructor(data: IClientTabData) {
    /// map data to this properties
  }

}

export interface IServiceCategoryData {
  categoryId: string;
  name: string;
  text: string;
  services: any[];
}

export class ServiceCategoryView {
  iconClass: string;
  name: string;
  text: string;
  services: any[];

  constructor(data: IServiceCategoryData) {
    /// map data to this properties
  }
}
