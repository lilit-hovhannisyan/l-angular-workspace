export const root = 'operator';

interface TabInterface {
  text: string;
  link: string;
  dynamicPart?: string;
  iconClass?: string;
  notification?: number;
}

export const transactionPageTabs: TabInterface[] = [
  {
    iconClass: 'pay-46',
    link: 'terminal',
    text: 'pay'
  },
  {
     iconClass: 'transfer',
     link: 'encashment',
     text: 'encashment'
   },
  {
    iconClass: 'groups',
    link: 'groups',
    text: 'groups'
  }
];

export const transactionHistoryPageTabs: TabInterface[] = [
  {
    link: 'balance',
    text: 'balance'
  },
  {
    link: 'close-day',
    text: 'report'
  },
];

export const supportPageTabs: TabInterface[] = [
  {
    link: 'user',
    text: 'Բաժանորդներ'
  },
  {
    link: 'claim',
    text: 'Չեղարկման հայտեր'
  },
  {
    link: 'claims/list',
    text: 'Չեղարկված գործարքներ'
  },
];

export const identificationPageTabs: TabInterface[] = [
  {
    iconClass: '',
    link: 'identification',
    text: 'identification'
  },
  {
    iconClass: '',
    link: 'activity-log',
    text: 'activityLog',
  },
  {
    iconClass: '',
    link: 'history',
    text: 'history'
  },
  // {
  //   iconClass: '',
  //   link: 'user-support',
  //   text: 'Support'
  // },
];

export const serviceCategory = [
  {
    iconClass: 'icon_utility',
    text: 'Utility',
    name: 'utility',
    services : [
      {
        iconClass: 'icon_gas',
        logo: 'beeline.png',
        text: 'Gas',
        name: 'gas',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: 'icon_utility',
        logo: 'beeline.png',
        text: 'Electricity',
        name: 'electricity',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    iconClass: 'icon_betting',
    text: 'Betting',
    name: 'betting',
    services : [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Vivaro',
        name: 'vivaro',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Toto',
        name: 'toto',
        percentage: 0,
        price: 10000
      },
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'Eurofootball',
        name: 'eurofootball',
        percentage: 0,
        price: 10000
      }
    ]
  },
  {
    text: 'Pay for my phone',
    iconClass: 'icon_',
    name: 'pay-for-phone',
    services: [
      {
        iconClass: '',
        logo: 'beeline.png',
        text: 'beeline',
        name: 'beeline',
        percentage: 1,
        price: 10000
      },
    ]
  },
  {
    text: 'Mobile Connections',
    iconClass: 'icon_',
    name: 'mobile',
    services: []

  },
  {
    text: 'Internet',
    iconClass: 'icon_',
    name: 'internet',
    services: []
  },
  {
    text: 'TV',
    iconClass: 'icon_',
    name: 'tv',
    services: []
  },
  {
    text: 'Parking',
    iconClass: 'icon_',
    name: 'mobile',
    services: []
  },

];




