import {Injectable} from '@angular/core';

import { serviceCategory} from './static-data';
import {ClientTabView, ServiceCategoryView} from './site-data.models';
import {OfficeApi, OperatorGetCitiesQuery, OperatorGetCountriesQuery} from 'upay-lib';
import {map} from 'rxjs/operators';

@Injectable()
export class SiteDataService {
  public serviceCategory: ServiceCategoryView[] = serviceCategory;

  constructor(private officeApi: OfficeApi) {}

  public getCategoryProviders(providerCategoryId) {
    const category = this.serviceCategory.find(sc => {
      return providerCategoryId === sc.name;
    });

    if (category && category.services) {
      return category.services;
    }

    return [];
  }

  public getCountries () {
    const operatorGetCountriesQuery = new OperatorGetCountriesQuery();
    return this.officeApi.getCountries(operatorGetCountriesQuery).pipe(map(response => {
      return response;
    }));
  }


  public getCities (data) {
    const operatorGetCitiesQuery = new OperatorGetCitiesQuery();
    operatorGetCitiesQuery.countryId = data;
    return this.officeApi.getCities(operatorGetCitiesQuery).pipe(map(response => {
      return response;
    }));
  }
}
