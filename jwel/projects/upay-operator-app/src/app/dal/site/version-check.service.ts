import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {first, map} from 'rxjs/operators';
import {of, Subscription} from 'rxjs';

import {DialogService} from 'upay-lib';
import {UserDataService} from '../user/user-data.service';
import {Router} from '@angular/router';
import {BasketDataService} from '../basket/basket-data.service';
import {AlertBoxService} from 'upay-lib';
import {PopupsService} from '../../popups/popups.service';

@Injectable()
export class VersionCheckService {
  private currentHash = '{{POST_BUILD_ENTERS_HASH_HERE}}';
  userActionOnDialogSub = Subscription.EMPTY;
  constructor(private http: HttpClient,
              private userDataService: UserDataService,
              private router: Router,
              private dialogService: DialogService,
              private basketDataService: BasketDataService,
              private alertBoxService: AlertBoxService,
              private popupService:  PopupsService) {
  }

  public initVersionCheck(url, frequency = 1000 * 60 * 30) {
    setInterval(() => {
      this.checkVersion(url);
    }, frequency);
  }

  public checkVersionAsync() {
    const url = './version.json';
    return this.http.get(url + '?t=' + new Date().getTime()).pipe(
      first(),
      map((response: any) => {
        const hash = response.hash;
        return of(this.hasHashChanged(this.currentHash, hash));
      })
    );
  }

  private checkVersion(url) {
    console.log('url');
    this.http.get(url + '?t=' + new Date().getTime()).pipe(first())
      .subscribe(
        (response: any) => {
          const hash = response.hash;
          const hashChanged = this.hasHashChanged(this.currentHash, hash);
console.log(response);
          if (hashChanged) {
            this.basketDataService.deleteAllFromBasket();
            this.userDataService.signOut();
            this.router.navigate(['/']);
           // this.alertBoxService.initMsg({type: 'success', text: 'The version '});

            this.popupService.openPopUp( 'newVersion' );
            // this.dialogService.initDialog('New Version');
            // this.userActionOnDialogSub.unsubscribe();
            // this.userActionOnDialogSub = this.dialogService.userActionOnDialog.subscribe(userAction => {
            //   if(userAction){
            //
            //     this.popupService.openPopUp( 'newVersion' );
            //     // location.reload();
            //   }
            // });
          }
          this.currentHash = hash;
        },
        (err) => {
          console.error(err, 'Could not get version');
        }
      );
  }

  private hasHashChanged(currentHash, newHash) {
    if (!currentHash || currentHash === '{{POST_BUILD_ENTERS_HASH_HERE}}') {
      return false;
    }

    return currentHash !== newHash;
  }
}
