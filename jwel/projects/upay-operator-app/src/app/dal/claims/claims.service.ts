import {Injectable} from '@angular/core';

import { v4 as uuid } from 'uuid';

import {
  AccountantGetClaimInfoQuery,
  CancellationClaimType, OfficeApi, OperatorClaimType, OperatorCompleteCancellationClaimCommand, OperatorGetNumberOfClaimsToCompleteQuery,
  OperatorGetTransactionByReceiptIdQuery, OperatorOpenCancellationClaimCommand
} from 'upay-lib';
import {AccountantGetClaimHistoryQuery, OperatorIsCancellationClaimAcceptableQuery} from '../../../../../upay-lib/src/lib/api';

@Injectable()
export class ClaimsService {
  constructor(private officeApi: OfficeApi) {}

  getTransactionByReceiptId(receiptId: string) {
    const operatorGetTransactionByReceiptIdQuery = new OperatorGetTransactionByReceiptIdQuery();
    operatorGetTransactionByReceiptIdQuery.receiptId = receiptId;
    return this.officeApi.getTransactionByReceiptId(operatorGetTransactionByReceiptIdQuery);
  }

  openCancellationClaim(transactionId: string, cancellationClaimType: string, notes: string,
                        operatorClaimType?: string,
                        attachmentKey?: string, customerClaimDescription?: string, customerPassportInfo?: string, customerPhone?: string ) {

    const operatorOpenCancellationClaimCommand = new OperatorOpenCancellationClaimCommand();

    // common to both types :: operator && customer
    //  CancellationClaimType enum { OPERATOR = "OPERATOR", CUSTOMER = "CUSTOMER"}
    operatorOpenCancellationClaimCommand.id = uuid();
    operatorOpenCancellationClaimCommand.transactionId = transactionId;
    operatorOpenCancellationClaimCommand.cancellationClaimType = CancellationClaimType[cancellationClaimType];
    operatorOpenCancellationClaimCommand.notes = notes;
    // specefic to operator claim only
    // OperatorClaimType enum { WRONG_AMOUNT = "WRONG_AMOUNT", WRONG_CUSTOMER = "WRONG_CUSTOMER", WRONG_SERVICE = "WRONG_SERVICE", APP_PROBLEM = "APP_PROBLEM"}
    operatorOpenCancellationClaimCommand.operatorClaimType = OperatorClaimType[operatorClaimType];
    // specefic to cutomer claim only
    operatorOpenCancellationClaimCommand.attachmentKey = attachmentKey;
    operatorOpenCancellationClaimCommand.customerClaimDescription = customerClaimDescription;
    operatorOpenCancellationClaimCommand.customerPassportInfo = customerPassportInfo;
    operatorOpenCancellationClaimCommand.customerPhone = customerPhone;


    return this.officeApi.openCancellationClaim(operatorOpenCancellationClaimCommand);
  }

  getFilteredClaims(queryData) {
    return this.officeApi.getFilteredClaims(queryData);
  }

  getClaimInfo(claimId: string) {
    const accountantGetClaimInfoQuery = new AccountantGetClaimInfoQuery();
    accountantGetClaimInfoQuery.claimId = claimId;
    return this.officeApi.getClaimInfo(accountantGetClaimInfoQuery);
  }

  getClaimHistory(claimId: string) {
    const accountantGetClaimHistoryQuery = new AccountantGetClaimHistoryQuery();
    accountantGetClaimHistoryQuery.claimId = claimId;
    return this.officeApi.getClaimHistory(accountantGetClaimHistoryQuery);
  }

  completeCancellationClaim(claimId: string) {
    const operatorCompleteCancellationClaimCommand = new OperatorCompleteCancellationClaimCommand();
    operatorCompleteCancellationClaimCommand.id = claimId;

    return this.officeApi.completeCancellationClaim(operatorCompleteCancellationClaimCommand);

  }

  // todo nkarel karmir ketik
  getNumberOfClaimsToComplete() {
    const operatorGetNumberOfClaimsToCompleteQuery = new OperatorGetNumberOfClaimsToCompleteQuery();
    return this.officeApi.getNumberOfClaimsToComplete(operatorGetNumberOfClaimsToCompleteQuery);
  }

  isCancellationClaimAcceptable( transactionId: string) {
    const operatorIsCancellationClaimAcceptableQuery = new OperatorIsCancellationClaimAcceptableQuery();
    operatorIsCancellationClaimAcceptableQuery.transactionId = transactionId;

    return this.officeApi.isCancellationClaimAcceptable(operatorIsCancellationClaimAcceptableQuery);
  }
}
