
import {dateFormat} from 'upay-lib';

export const createClaimByCustomerApplicationForm = (transactionData: any, customerData: any, dynamicString: string, currentOperator: string) => {
  const curDate = dateFormat(new Date(), 'dd/mm/yyyy');
  const header = `
    <div style="text-align: right;">
      <p>«Յուփեյ» ՓԲԸ տնօրեն</p>
      <p>Ռուբեն Սաղոյանին</p>
    </div>
    <div style="text-align: center; margin-top: 25px;">Դիմում</div>
    <div style="text-align: center; margin-bottom: 20px;">(մասնաճյուղում կատարված վճարման ուղղման)</div>
    <div style="text-align: right; font-style: italic; margin-bottom: 20px;">${curDate}</div>
  `;


  const _transactionData = transactionData[0].info;
  const transactionHtml = `
    <div style="margin-bottom: 20px; margin-top: 20px;">Խնդրում եմ չեղարկել իմ կողմից կատարված հետևյալ գործարքը (Գործարք ${_transactionData.receiptId.value})՝</div>
    <div style="font-style: italic;">Վճարման ամսաթիվ <b>${_transactionData.completed.value}</b></div>
    <div style="font-style: italic;">Անդորրագրի համար <b>${_transactionData.receiptId.value}</b></div>
    <div style="font-style: italic;">Մասնաճյուղ <b>${_transactionData.branchName.value}</b></div>
    <div style="font-style: italic;">Ծառայություն <b>${_transactionData.purpose.value}</b></div>
    <div style="font-style: italic;">Գումար <b>${_transactionData.amount.value}</b></div>
    <div style="font-style: italic;">Բաժանորդային համար <b>${_transactionData.userNum.value}</b></div>
    <div style="font-style: italic;">Գանձապահ <b>${_transactionData.operatorName.value}</b></div>
  `;

  const newTransactionDataHtml = `
   <div style="margin-top: 20px;">և կատարել նոր գործարք հետևյալ ուղղությամբ՝</div>
   <div style="font-style: italic;">ծառայության <b>${dynamicString}</b></div>
  `;


  const dateOfIssue = dateFormat(customerData.dateOfIssue, 'dd/mm/yyyy');
  const validTo = dateFormat(customerData.validTo, 'dd/mm/yyyy');
  const phoneNumber = customerData.phone.replace('374', '0');
  const customerDataHtml = `
    <div style="margin-top: 20px;">Ես՝ դիմող</div>
    <div style="font-style: italic;">Անուն Ազգանուն <b>${customerData.name}</b></div>
    <div style="font-style: italic;">Անձնագրի համար <b>${customerData.passport}</b></div>
    <div style="font-style: italic;">Տրված <b>${dateOfIssue}</b></div>
    <div style="font-style: italic;">Վավերական է մինչև <b>${validTo}</b></div>
    <div style="font-style: italic;">Ում կողմից <b>${customerData.issuedBy}</b></div>
    <div style="font-style: italic;">Հեռախոսահամար <b>${phoneNumber}</b></div>
  `;

  const footer = `
  <div style="margin: 20px 0;">*Ստորև ստորագրելով՝ հաստատում եմ, որ իմ կողմից ներկայացված բոլոր տեղեկությունները լիարժեք են և արժանահավատ:
  Տեղեկացված եմ և համաձայն եմ, որ «Յուփեյ» ՓԲԸ-ի կողմից  նոր ճիշտ գործարքը կարող է կատարվել միայն գործարք ${_transactionData.receiptId.value}-ի չեղարկման դեպքում:
  Ընդունում եմ, որ սխալ վճարում կատարված լինելու դեպքում սույն դիմումի համաձայն «Յուփեյ» ՓԲԸ-ն փորձելու է համապատասխան ուղղումը կատարել
  (գործարք ${_transactionData.receiptId.value}-ը չեղարկել) համապատասխան ծառայությունը մատուցող ընկերության միջոցով, և «Յուփեյ» ՓԲԸ-ն պատասխանատու չէ
  համապատասխան ծառայություն մատուցող ընկերության կողմից չեղարկման դիմում(ներ)ի մերժման դեպքում:</div>`;

  const signatures = `
      <div style="display: flex; margin-top: 20px;">
    <p>Դիմող՝ </p>
    <p style="display:flex; flex-direction: column; margin-left: 10px;">
      <span>_________________________</span>
      <span style="font-size: .6rem;">անուն, ազգանուն</span>
    </p>
    <p style="display:flex; flex-direction: column; margin-left: 10px;">
      <span>______________</span>
      <span style="font-size: .6rem;">ստորագրություն</span>
    </p>
  </div>
  <div style="font-style: italic;">Դիմումը ընդունող գանձապահ՝  <b>${currentOperator}</b> <span>_______________</span></div>
  `;

  const applicationHtml = `<div style="padding: 20px; font-size: 12px;">
                             ${header}
                             ${customerDataHtml}
                             ${transactionHtml}
                             ${newTransactionDataHtml}
                             ${footer}
                             ${signatures}
                           </div>`;


  return applicationHtml;
};
