import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// core module
import { CoreModule} from './core/core.module';

// routing
import { appRoutes } from './app-routing';

// app init
import { AppComponent } from './app.component';

// services
import { LanguageService } from './dal/language.service';
import { SiteDataService } from './dal/site/site-data.service';
import { UserDataService } from './dal/user/user-data.service';
import { BasketDataService } from './dal/basket/basket-data.service';
import { CashboxDataService } from './dal/cashbox/cashbox-data.service';
// import { AuthService } from './services/auth.service';
import {ProviderDataService} from './dal/provider/provider-data.service';
import {SubscriberDataService} from './dal/subscriber/subscriber-data.service';
import {GroupsService} from './dal/groups/groups.service';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {NoAuthGuardService} from './core/guards/no-auth-guard.service';

import {PrintService} from './misc-services/print.service';

import {EncashmentService} from './dal/encashment/encashment.service';
import {EncashmentViewService} from './dal/encashment/encashment-view.service';
import {EncashmentDataService} from './dal/encashment/encashment-data.service';
import {EncashmentPrintDataService} from './dal/encashment/encashment-print-data.service';
import {EncashmentPrintDataLogService} from './dal/encashment/encashment-print-data-log.service';
import {SupportService} from './dal/support/support.service';
import {ClaimsService} from './dal/claims/claims.service';


import {ForcePassChangeService} from './misc-services/forcePassChange.service';



import {SharedSiteDataService, AlertBoxService, DialogService, ConvertTypesService, AccountantApi} from 'upay-lib';
import {SynchronizedCallsService, CalculateCommissionService, DateTimeService, SharedPrintService, LoadingService} from 'upay-lib';
import {ErrorHandler, OfficeApi, ClaimistApi,  FileUploadApi, setApiUrl, fileStorageUrl, SharedWidgetsModule, CardHelperService, ProviderService} from 'upay-lib';
import {environment} from '../environments/environment';
import {VersionCheckService} from './dal/site/version-check.service';
import {EncashmentProviderService} from './dal/encashment/encashment-new/encashment-provider.service';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

setApiUrl(environment.APIEndpoint.officeUrl);
fileStorageUrl(environment.APIEndpoint.fileStorageUrl);

export class TranslationLoad implements TranslateLoader {
  constructor(public http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    return forkJoin(
      this.http.get<any>('shared-assets/i18n/' + lang + '.json'),
      this.http.get<any>('assets/i18n/' + lang + '.json'),
    ).pipe(map(response => {
      return {...response[0], ...response[1]};
    }));
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: TranslationLoad,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    LanguageService,
    SiteDataService,
    UserDataService,
    BasketDataService,
    CashboxDataService,
    ProviderDataService,
    SubscriberDataService,
    GroupsService,
    AuthGuardService,
    NoAuthGuardService,
    EncashmentService,
    EncashmentViewService,
    EncashmentDataService,
    SupportService,
    ClaimsService,
    CardHelperService,


    // from upay-web
    SharedSiteDataService,
    EncashmentPrintDataService,
    EncashmentPrintDataLogService,
    // AuthService
    OfficeApi,
    ClaimistApi,
    AccountantApi,
    FileUploadApi,
    ErrorHandler,
    SynchronizedCallsService,
    CalculateCommissionService,
    DateTimeService,
    SharedPrintService,
    SharedWidgetsModule,
    LoadingService,

    AlertBoxService,
    DialogService,
    ConvertTypesService,

    PrintService,
    ForcePassChangeService,
    VersionCheckService,
    ProviderService,

    EncashmentProviderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {

  }
}
