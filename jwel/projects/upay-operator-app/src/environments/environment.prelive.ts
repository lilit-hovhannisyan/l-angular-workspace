export const environment = {
  production: false,
  versionCheck: true,
  APIEndpoint: {
      officeUrl:  'http://10.16.59.53:55501',
      adminUrl: 'http://10.16.59.53:55502',
      publicUrl: 'http://10.16.59.53:55500',
      fileStorageUrl: 'http://10.16.59.53:8081/'
  }
};
