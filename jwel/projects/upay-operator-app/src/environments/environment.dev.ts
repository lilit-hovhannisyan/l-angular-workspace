const serversMap = {
  claudia: 79,
  monika: 77,
};

const currentServer = serversMap.claudia;

export const environment = {
  production: false,
  versionCheck: true,
  APIEndpoint: {
      officeUrl:  'http://10.180.8.79:55501',
      adminUrl: 'http://10.180.8.79:55502',
      publicUrl: 'http://10.180.8.79:55500',
      fileStorageUrl: 'http://10.180.8.79:8080'
  }
};
