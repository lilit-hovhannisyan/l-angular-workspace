// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  versionCheck: false,
  // APIEndpoint: {
  //   officeUrl:  'http://10.180.8.77:55501', // Monica
  //   adminUrl: 'http://10.180.8.77:55502',
  //   publicUrl: 'http://10.180.8.77:55500'
  // },
  APIEndpoint: {
    // officeUrl:  'http://10.180.8.79:55501',  // dev
    officeUrl:  'http://10.16.59.11:55501', // staging
    adminUrl: 'http://10.180.8.79:55502',
    publicUrl: 'http://10.180.8.79:55500',
    fileStorageUrl: 'http://10.180.8.79:8081'
  }
};
