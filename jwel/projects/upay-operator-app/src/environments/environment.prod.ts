export const environment = {
  production: true,
  versionCheck: true,
  APIEndpoint: {
    officeUrl:  'http://10.16.59.22:55501',
    adminUrl: 'http://10.16.59.22:55502',
    publicUrl: 'http://10.16.59.22:55500',
    fileStorageUrl: 'http://10.16.59.22:8081/'
  }
};
