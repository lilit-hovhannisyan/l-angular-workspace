export const environment = {
  production: false,
  versionCheck: true,
  APIEndpoint: {
      officeUrl:  'http://10.16.59.11:55501',
      adminUrl: 'http://10.16.59.11:55502',
      publicUrl: 'http://10.16.59.11:55500',
      fileStorageUrl: 'http://10.16.59.11:8081'
  }
};
