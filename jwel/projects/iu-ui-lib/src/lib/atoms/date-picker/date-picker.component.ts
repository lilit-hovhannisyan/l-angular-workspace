import {Component, EventEmitter, Input, Output, OnInit, AfterViewInit, OnChanges} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import {UI_DATE_FORMATS, UIDateAdapter} from './date.adapter';

@Component({
  selector: 'iu-ui-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.less'],
  providers: [
    {
      provide: DateAdapter,
      useClass: UIDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: UI_DATE_FORMATS
    }
  ]
})
export class DatePickerComponent implements OnInit, OnChanges {
  @Input() value?: Date;
  @Input() placeholder = 'dd/mm/yyyy';
  @Output() valueChange = new EventEmitter<any>();

  selectedValue: Date;

  ngOnInit(): void {
    this.setSelectedValue(this.value);
  }

  ngOnChanges() {
   this.setSelectedValue(this.value);
  }

  onDateChange(e) {
    const value = e.target.value;
    this.setSelectedValue(value);
  }

  setSelectedValue(value: Date) {
    if (value) {
      const dateValue = new Date(value);
      this.selectedValue = dateValue;
    } else {
      this.selectedValue = null;
    }
    this.valueChange.emit(this.selectedValue);
  }

}
