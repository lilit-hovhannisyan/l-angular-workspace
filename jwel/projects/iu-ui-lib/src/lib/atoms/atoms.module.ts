import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { ButtonComponent } from './button/button.component';
import { LinkComponent } from './link/link.component';
import { DownloadLinkComponent } from './download-link/download-link.component';
import { SelectComponent } from './select/select.component';
import { IconComponent } from './icon/icon.component';
import { TextComponent } from './text/text.component';
import { ImgComponent } from './image/img.component';
import { FlexboxComponent } from './flexbox/flexbox.component';
import { InputComponent } from './input/input.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { RadioButtonComponent } from './radio-button/radio-button.component';


import {
  MatDatepickerModule,
  MatFormFieldModule, MatInputModule,
  MatNativeDateModule
} from '@angular/material';

@NgModule({
  declarations: [
    ButtonComponent,
    LinkComponent,
    DownloadLinkComponent,
    SelectComponent,
    IconComponent,
    TextComponent,
    ImgComponent,
    FlexboxComponent,
    InputComponent,
    CheckboxComponent,
    DatePickerComponent,
    AutocompleteComponent,
    RadioButtonComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,

    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule
  ],
  providers: [
    MatDatepickerModule,
  ],
  exports: [
    ButtonComponent,
    LinkComponent,
    DownloadLinkComponent,
    SelectComponent,
    IconComponent,
    TextComponent,
    ImgComponent,
    FlexboxComponent,
    InputComponent,
    CheckboxComponent,
    DatePickerComponent,
    AutocompleteComponent,
    RadioButtonComponent
  ]
})
export class AtomsModules { }
