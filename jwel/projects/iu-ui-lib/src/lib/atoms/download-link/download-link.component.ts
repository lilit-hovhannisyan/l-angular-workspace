import { Component, Input } from '@angular/core';

@Component({
  selector: 'iu-ui-download-link',
  templateUrl: './download-link.component.html',
  styleUrls: ['./download-link.component.less']
})
export class DownloadLinkComponent {
  @Input() link: string;
  @Input() text?: string;
 }
