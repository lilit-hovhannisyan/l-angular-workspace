import { Component, Input } from '@angular/core';

@Component ({
  selector: 'iu-ui-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.less']
})

export class ImgComponent {
  @Input() src: string;
  @Input() alt = 'image';
  @Input() width;
  @Input() height;

  getProportion () {
      if ( this.width < this.height ) {
        return 'heightStreach';
      } else {
        return 'widthStreach';
      }
  }
}
