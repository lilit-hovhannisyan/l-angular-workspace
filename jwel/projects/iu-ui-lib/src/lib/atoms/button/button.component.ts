import { Component, EventEmitter, Input, Output } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'iu-ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ButtonComponent {
  @Input() text: string;
}
