import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'iu-ui-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.less']
})

export class RadioButtonComponent {
  @Input() options: RadioButtonOption[];
  @Output() selectOption = new EventEmitter<RadioButtonOption>();

  onSelectOption(key) {
    this.options.map( option => {
      option.selected = false;

      if ( key === option.key) {
        option.selected = true;
        this.selectOption.emit(option);
      }
    });
  }
}

export interface RadioButtonOption {
  key: string;
  dispalyText: string;
  selected?: boolean;
}
