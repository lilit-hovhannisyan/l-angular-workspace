import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  HostListener,
  ElementRef,
  OnChanges,
  SimpleChanges
} from '@angular/core';

import {Observable} from 'rxjs';

@Component({
  selector: 'iu-ui-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.less']
})
export class AutocompleteComponent implements OnInit,  OnChanges {
  @Input() options = [];
  @Input() getOptions: (any) => Observable<any[]>;
  @Input() dependence: any;
  @Input() hasFiltering = true;
  @Input() placeholder: string;
  @Input() searchPlaceholder: string;
  @Input() outputFieldKey: string;
  @Input() displayFieldKey = 'label';
  @Input() filterFiled: string;
  @Input() selectedOption?: any = null;
  @Input() defaultValue?: any = null;
  @Input() hasErr = false;
  @Input() errMsg?: string;

  @Output() valueSelect = new EventEmitter();


  inputValue = '';
  filteredOptions = [];
  visible: boolean;

  constructor(private dropdownListElRef: ElementRef) { }

  ngOnInit() {
    if (this.filterFiled) {
      this.filterFiled = this.displayFieldKey;
    }
    this.setDefault();
    this.setOptions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('dependence') && changes.dependence.currentValue !== changes.dependence.previousValue) {
      this.setOptions(() => {
        this.setSelectedOptionData(changes.selectedOption);
      });
    } else {
      this.setSelectedOptionData(changes.selectedOption);
    }

  }

  setOptions(cb?) {
    if (this.getOptions) {
      this.getOptions(this.dependence).subscribe(options => {
        this.options = options;
        this.setFilteredOptions();

        if (cb) {
          cb();
        }

      });
    } else {
      this.setFilteredOptions();
    }
  }

  setSelectedOptionData(data) {
    if (!data) {
      this.selectedOption = null;
      return;
    }
    if (data.selectedOption !== undefined && data.selectedOption !== null ) {
      this.selectedOption = null;
      return;
    }
    if (data.currentValue === data.previousValue) {
      return;
    }

    if (this.outputFieldKey) {
      this.selectedOption = this.options.find(o => {
        return this.getPropertyFromOption(o, this.outputFieldKey) ===  data.currentValue;
      });
    } else {
      this.selectedOption = data.currentValue;
    }
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.visible && !this.dropdownListElRef.nativeElement.contains(event.target)) {
      this.visible = false;
    }
  }

  getPropertyFromOption(option, keyName) {
    if (typeof option === 'string') {
      return option;
    }
    return option[keyName];
  }

  setFilteredOptions() {
    this.filteredOptions = this.options.filter(option => {
      const optionSearchFieldValue = this.getPropertyFromOption(option, this.displayFieldKey).toLowerCase();
      const searchInputValue = this.inputValue.toLowerCase();
      return optionSearchFieldValue.startsWith(searchInputValue);
    });
  }

  setDefault() {
    if (this.defaultValue) {
      this.selectedOption = this.defaultValue;
      this.valueSelect.emit(this.selectedOption);
    }
  }

  toggle() {
    this.visible = !this.visible;
  }

  inputValueChange(e) {
    this.inputValue = e.target.value;
    this.setFilteredOptions();
  }

  selectOption(option) {
    this.selectedOption = option;

    if (this.outputFieldKey) {
      this.valueSelect.emit(this.getPropertyFromOption(this.selectedOption, this.outputFieldKey));
    } else {
      this.valueSelect.emit(this.selectedOption);
    }

    this.toggle();
  }
}
