import { Component, EventEmitter, Input, Output, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'iu-ui-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.less']
})
export class InputComponent {
  @Input() placeholder ? = '';
  @Input() type?;

  @Input() value?: string;

  @Input() required?: boolean;
  @Input() regExp?: any;
  @Input() errMsg?: string;

  @Input() hasErr = false;
  @Output() valueChange = new EventEmitter<any>();
  @Output() keyDown = new EventEmitter<any>(); // todo jnjel

  onInput(e) {
    this.valueChange.emit( e.target.value);
  }

  onkeyDown(e) {
    this.keyDown.emit(e);
  }
}
