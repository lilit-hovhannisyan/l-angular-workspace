import {Component, EventEmitter, Input, Output, ElementRef, HostListener, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'iu-ui-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.less']
})
export class SelectComponent implements OnInit, OnDestroy {
  // @Input() placeholder?: string;
  // @Input() options: any[];
  // @Input() viewField?: string; // todo create logic
  // @Input() valueField?: string; // todo create logic
  // @Input() defaultValue: any;
  //
  // @Output() change = new EventEmitter<any>();
  //
  // onChange(e) {
  //   this.change.emit(e.value);
  // }
  @Input() value?: any;
  @Input() visible = false;
  @Input() placeholder: string;
  @Input() options = [];
  @Input() getOptions?: Function;
  @Output() change = new EventEmitter<any>();
  @Output() optionClick = new EventEmitter();
  @Output() resetSelect = new EventEmitter();
  @Input() textField = 'text';
  @Input() valueField = 'id';

  @Input() optionPrefix = '';
  @Input() preSelectedOption?: any = null;
  @Input() iconClass = 'icon_arrow-down';

  optionsSub =  Subscription.EMPTY;

  selectedData = null;

  _options = [];


  constructor(private _eref: ElementRef) {
  }

  ngOnInit() {
    if (this.getOptions) {
      this.optionsSub = this.getOptions().subscribe(data => {
        this._options = data;

        if (this.preSelectedOption) {
          this.setupPreselected();
        }
      });
    } else {
      this._options = this.options;
      if (this.preSelectedOption) {
        this.setupPreselected();
      }
    }
  }

  ngOnDestroy() {
    this.optionsSub.unsubscribe();
  }

  toggle(e) {
    return this.visible = !this.visible;
  }

  onOptionClick(event) {
    this.selectedData = event;
    this.placeholder = this.selectedData[this.textField];
    this.change.emit(this.selectedData);
    return this.optionClick.emit(this.selectedData);
  }

  setupPreselected() {
    if (this.preSelectedOption.hasOwnProperty('order')) {
      const order = this.preSelectedOption['order'];
      const textField = this.preSelectedOption['textField'];
      this.placeholder = this._options[order][textField];
    } else {
      const preSelectedData = this._options.find(o => {
        // tslint:disable-next-line
        return o[this.valueField] == this.preSelectedOption;
      });
      this.selectedData = preSelectedData;
      this.change.emit(this.selectedData);
      this.placeholder = this.selectedData[this.textField];
    }

  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.visible && !this._eref.nativeElement.contains(event.target)) {
      this.visible = false;
    }
  }
}
