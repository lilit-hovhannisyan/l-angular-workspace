import { Component, Input } from '@angular/core';

@Component ({
  selector: 'iu-ui-flexbox',
  templateUrl: './flexbox.component.html',
  styleUrls: ['./flexbox.component.less']
})

export class FlexboxComponent {
  @Input() type?: string;
}
