import { Component, Input } from '@angular/core';

@Component ({
  selector: 'iu-ui-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.less']
})
export class IconComponent {
  @Input() type?: string;
}
