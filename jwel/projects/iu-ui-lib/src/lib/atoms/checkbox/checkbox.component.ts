import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component ({
  selector: 'iu-ui-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.less']
})
export class CheckboxComponent {
  @Input() isChecked: boolean;
  @Output() changeState = new EventEmitter();

  onChangeState( e ) {
    this.isChecked = !this.isChecked;
    this.changeState.emit( { checkboxState: this.isChecked } );
  }
}
