import {Component, Input} from '@angular/core';


@Component({
  selector: 'iu-ui-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.less']
})
export class LinkComponent {
  @Input() link: string;
  @Input() text?: string;
  @Input() params?: any;
}
