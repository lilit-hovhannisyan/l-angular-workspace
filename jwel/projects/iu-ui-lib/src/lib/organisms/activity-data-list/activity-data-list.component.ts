import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IActivityData } from './activity-data-list-item/activity-data-list-item.component';

@Component({
  selector: 'iu-ui-activity-data-list',
  templateUrl: './activity-data-list.component.html',
})
export class ActivityDataListComponent {
 // @ContentChild(TemplateRef) itemTemplateRef: TemplateRef<any>;
  @Input() activityDataSource: IActivityData[];
  @Input() activityIdField?: string;
  @Input() payAgainText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;

  @Input() iconClassName_toggle?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;

  @Output() activityPayAgainClick = new EventEmitter<IActivityData>();
  @Output() activityFavoriteClick = new EventEmitter<IActivityData>();
  @Output() activityRecurrentClick = new EventEmitter<IActivityData>();

  constructor() {}

  onOpenActivity(event) {
    // todo create logic
    // this.activityClick.emit(activity);
  }

  onActivityPayAgainClick(activity: IActivityData) {
    this.activityPayAgainClick.emit(activity);
  }

  onActivityFavoriteClick(activity: IActivityData) {
    this.activityFavoriteClick.emit(activity);
  }

  onActivityRecurrentClick(activity: IActivityData) {
    this.activityRecurrentClick.emit(activity);
  }

}
