import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface IServiceDetail {
  name: string;
  value: string;
}

export interface IActivityData {
  icon: string;
  title: string;
  date: string | number;
  amount: number;
  currency: string;
  isFavorite: boolean;
  isRecurrent: boolean;
  serviceDetails: IServiceDetail[];
}

@Component({
  selector: 'iu-ui-activity-data-list-item',
  templateUrl: './activity-data-list-item.component.html',
  styleUrls: ['./activity-data-list-item.component.less']
})
export class ActivityDataListItemComponent {
  @Input() activityData: IActivityData;
  @Input() payAgainText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;

  @Input() iconClassName_toggle?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;

  @Output() showDetailsClick = new EventEmitter<boolean>();
  @Output() payAgainClick = new EventEmitter<IActivityData>();
  @Output() favoriteClick = new EventEmitter<IActivityData>();
  @Output() recurrentClick = new EventEmitter<IActivityData>();

  public isDetailsOpen = false;

  constructor() {}

  getAmountType () {
    if ( this.activityData.amount < 0 ) {
      return 'iu-ui-negative';
    }
    return 'iu-ui-positive';
  }

  onShowDetailsClick() {
    this.isDetailsOpen = !this.isDetailsOpen;
    this.showDetailsClick.emit(this.isDetailsOpen);
  }

  onPayAgainClick() {
    this.payAgainClick.emit(this.activityData);
  }

  onFavoriteClick() {
    this.favoriteClick.emit(this.activityData);
  }

  onRecurrentClick() {
    this.recurrentClick.emit(this.activityData);
  }

}
