import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IPair, IPaymentCardData } from '../../global-types';

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

export type ICustomerDetail = IPair;
export type IDebtDetail = IPair;

export interface IPayDta {
  amount: number;
  payment: IPaymentCardData;
  customerDebitPayData: ICustomerDebitPayData;
}

export interface ICustomerDebitPayData {
  icon: string;
  title: string;
  isFavorite?: boolean;
  isRecurrent?: boolean;
  customerDetails: ICustomerDetail[];
  debtDetails: IDebtDetail[];
  prevDebtDetails?: IDebtDetail[];
  commissionPercent: number;
  updated?: string;
}

@Component({
  selector: 'iu-ui-customer-debit-pay-form',
  templateUrl: './customer-debit-pay-form.component.html',
})

export class CustomerDebitPayFormComponent {
  @Input() customerDebitPayData: ICustomerDebitPayData;

  @Input() updatedText?: string;
  @Input() iconClassName_new?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;
  @Input() iconClassName_basket?: string;
  @Input() iconClassName_group?: string;

  @Input() paymentMethodTitle?: string;
  @Input() paymentCards: IPaymentCardData[];
  @Input() paymentListText?: string;
  @Input() amountText?: string;
  @Input() commissionText?: string;
  @Input() withCommissionText?: string;
  @Input() payText?: string;
  @Input() addNewCardText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;
  @Input() basketText?: string;
  @Input() groupText?: string;

  @Output() addNewCardClick = new EventEmitter<any>();
  @Output() payClick = new EventEmitter<IPayDta>();
  @Output() favoriteClick = new EventEmitter<ICustomerDebitPayData>();
  @Output() recurrentClick = new EventEmitter<ICustomerDebitPayData>();
  @Output() basketClick = new EventEmitter<ICustomerDebitPayData>();
  @Output() groupClick = new EventEmitter<ICustomerDebitPayData>();
  @Output() amountChange = new EventEmitter<number>();
  @Output() payPurposeChange = new EventEmitter<string>();
  @Output() focus = new EventEmitter<number>();


  // public commissionValue?: number;
  public selectedPayment: IPaymentCardData;
  @Input() amount: number;
  @Input() isFixPay: boolean;
  @Input() hasPayPurpose: boolean;
  @Input() commission?: number;

  constructor() {}

  // calculateCommission(amountValue, commissionPercent) {
  //   if (amountValue && commissionPercent) {
  //     this.commissionValue = ( amountValue * commissionPercent ) / 100;
  //   }
  // }

  onAmountChange(event) {
    this.amount =  event.target.value;
    // this.calculateCommission(this.amount, this.customerDebitPayData.commissionPercent);
    this.amountChange.emit(this.amount);
  }

  onPayPurposeChange(event) {
    this.payPurposeChange.emit(event.target.value);
  }

  onFocus(event) {
    this.focus.emit(event);
  }

  onPaymentCardSelect(paymentCard: IPaymentCardData) {
    this.selectedPayment = paymentCard;
  }

  getResultValue(data) {

    if (data.displayType === 'amount') {

      return formatNumber(+data.value);
    }

    return data.value;
  }

  onAddNewCardClick() {
    this.addNewCardClick.emit();
  }

  onPayClick() {
    this.payClick.emit({
      amount: this.amount,
      payment: this.selectedPayment,
      customerDebitPayData: this.customerDebitPayData
    });
  }

  onFavoriteClick() {
    this.favoriteClick.emit(this.customerDebitPayData);
  }

  onRecurrentClick() {
    this.recurrentClick.emit(this.customerDebitPayData);
  }

  onBasketClick() {
    this.basketClick.emit(this.customerDebitPayData);
  }

  onGroupClick() {
    this.groupClick.emit(this.customerDebitPayData);
  }
}
