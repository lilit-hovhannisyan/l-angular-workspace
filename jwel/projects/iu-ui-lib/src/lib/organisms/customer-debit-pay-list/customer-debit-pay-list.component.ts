import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IPaymentCardData } from '../../global-types';
import { ICustomerDebitPayData, IPayDta } from '../customer-debit-pay-form/customer-debit-pay-form.component';

@Component({
  selector: 'iu-ui-customer-debit-pay-list',
  templateUrl: './customer-debit-pay-list.component.html',
})
export class CustomerDebitPayListComponent {
  // props from debit pay form
  @Input() customerDebitPayDataSource: ICustomerDebitPayData[];
  @Input() toggleIconClass?: string;

  @Input() iconClassName_new?: string;
  @Input() iconClassName_fav?: string;
  @Input() iconClassName_recurrent?: string;

  @Input() paymentMethodTitle?: string;
  @Input() paymentCards: IPaymentCardData[];
  @Input() paymentListText?: string;
  @Input() amountText?: string;
  @Input() commissionText?: string;
  @Input() withCommissionText?: string;
  @Input() payText?: string;
  @Input() addNewCardText?: string;
  @Input() favoriteText?: string;
  @Input() recurrentText?: string;

  @Output() addNewCardClick = new EventEmitter<any>();
  @Output() payClick = new EventEmitter<IPayDta>();
  @Output() favoriteClick = new EventEmitter<ICustomerDebitPayData>();
  @Output() recurrentClick = new EventEmitter<ICustomerDebitPayData>();

  constructor() {}

  onAddNewCardClick(event) {
    this.addNewCardClick.emit(event);
  }

  onPayClick(event) {
    this.payClick.emit(event);
  }

  onFavoriteClick(customerDebitPayData: ICustomerDebitPayData) {
    this.favoriteClick.emit(customerDebitPayData);
  }

  onRecurrentClick(customerDebitPayData: ICustomerDebitPayData) {
    this.recurrentClick.emit(customerDebitPayData);
  }

}
