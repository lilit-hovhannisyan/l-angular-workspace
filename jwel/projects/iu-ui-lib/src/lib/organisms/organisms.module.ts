/* tslint:disable:max-line-length */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AtomsModules } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';

import { ActivityDataListComponent } from './activity-data-list/activity-data-list.component';
import { ActivityDataListItemComponent } from './activity-data-list/activity-data-list-item/activity-data-list-item.component';
import { CustomerDebitPayFormComponent } from './customer-debit-pay-form/customer-debit-pay-form.component';
import { CustomerDebitPayListComponent } from './customer-debit-pay-list/customer-debit-pay-list.component';
import { CustomerDebitPayListItemComponent } from './customer-debit-pay-list/customer-debit-pay-list-item/customer-debit-pay-list-item.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    ActivityDataListComponent,
    ActivityDataListItemComponent,
    CustomerDebitPayFormComponent,
    CustomerDebitPayListComponent,
    CustomerDebitPayListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MoleculesModule,
    AtomsModules,
    TranslateModule
  ],
  exports: [
    ActivityDataListComponent,
    ActivityDataListItemComponent,
    CustomerDebitPayFormComponent,
    CustomerDebitPayListComponent,
    CustomerDebitPayListItemComponent,
  ]
})
export class OrganismsModule { }
