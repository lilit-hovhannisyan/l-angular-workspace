
export interface IPair {
  name: string;
  value: string;
}

export interface IPaymentCardData {
  id: string | number;
  type: string;
  text: string;
}


