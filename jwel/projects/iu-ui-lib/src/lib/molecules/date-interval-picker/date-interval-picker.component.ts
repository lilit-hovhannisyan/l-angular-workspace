import {Component, EventEmitter, Input, Output, OnInit, AfterViewInit, OnChanges} from '@angular/core';

@Component({
  selector: 'iu-ui-date-interval-picker',
  templateUrl: './date-interval-picker.component.html',
  styleUrls: ['./date-interval-picker.component.less']
})
export class DateIntervalPickerComponent implements OnInit, OnChanges {
  // @Input() value?: string;
  // @Input() reset:  boolean;
  @Input() defaultInterval?: {
    from: Date,
    to: Date
  } = {
    from: null,
    to: null
  };
  @Input() fromPlaceholder = '';
  @Input() toPlaceholder = '';

  @Output() valueChange = new EventEmitter<any>();
  @Output() validationError = new EventEmitter<any>();

  selectedDateInterval = {
    from: null,
    to: null
  };

  static validate(dateInterval) {
    const { from, to } = dateInterval;

    if (from && to && new Date(from) > new Date(to)) {
      return false;
    }

    return true;
  }

  ngOnInit(): void {
    this.setDefaultDate();
  }

  ngOnChanges() {
    this.setDefaultDate();
  }

  setDefaultDate() {
    if (this.defaultInterval) {
      this.selectedDateInterval = {
        from: this.defaultInterval.from,
        to: this.defaultInterval.to
      };
    } else {
      this.selectedDateInterval = {
        from: null,
        to: null
      };
    }

    // this.selectedDateInterval = {
    //   from: this.dateFromDefault,
    //   to: this.dateToDefault
    // };

    this.onChange(this.selectedDateInterval);
  }

  onFromDateChange(e) {
    this.selectedDateInterval = {
      from: e,
      to: this.selectedDateInterval.to
    };

    this.onChange(this.selectedDateInterval);
  }

  onToDateChange(e) {
    this.selectedDateInterval = {
      from: this.selectedDateInterval.from,
      to: e
    };

    this.onChange(this.selectedDateInterval);
  }

  onChange(e) {
    console.log(e);
    if (DateIntervalPickerComponent.validate(e)) {
      this.valueChange.emit(e);
    } else {
      this.validationError.emit(e);
    }
  }



}
