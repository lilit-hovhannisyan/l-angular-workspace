import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component ({
  selector: 'iu-ui-button-with-icon',
  templateUrl: './button-with-icon.component.html',
  styleUrls: ['./button-with-icon.component.less']
})

// usage as tabs in app
export class BtnWithIconComponent {
  // btn
  // @Input() link: string;
  @Input() text: string;
  @Input() id?: string;

  // icon
  @Input() iconClassName?: string;

  // image
  @Input() src?: string;
  @Input() alt?: string;


  // btn action
  // @Output() click = new EventEmitter<any>();

  // onClick(e) {
  //   this.click.emit(e);
  // }
}
