import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'iu-ui-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {


  @Input() totalItems: number;
  @Input() itemsPerPage: number;
  @Input() maxSize: number; // number of buttons to display in pagination bar
  @Input() rotate = false; // back to the start on reaching the end
  @Output() pageChange = new EventEmitter<number>(); // called on changing the cursor

  cursor = 1;
  basePage = 1;
  totalPages = 0;
  pageButtonList: number[];
  MIN_PAGE = 1;

  constructor() {
  }

  ngOnInit() {
    this.totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
    this.calculatePagesBar();
  }


  moveCursor(steps: number) {
    if ((this.cursor + steps) < this.MIN_PAGE) {
      this.moveToPage(this.totalPages);
    } else if ((this.cursor + steps) > this.totalPages) {
      this.moveToPage(this.MIN_PAGE);
    } else {
      this.moveToPage(this.cursor + steps);
    }
  }

  moveToPage(pageNumber: number) {
    this.paginate(pageNumber);
    this.calculatePagesBar();
  }

  calculatePagesBar() {
    this.pageButtonList = [];
    this.basePage = this.maxSize * Math.floor((this.cursor - 1) / this.maxSize);
    for (let i = 1; i < this.maxSize + 1; i++) {
      if ((this.basePage + i) > this.totalPages) {
        break;
      }
      this.pageButtonList.push(this.basePage + i);
    }
  }

  paginate(page: number) {
    this.cursor = page;
    this.pageChange.emit(page);
  }

}
