import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'iu-ui-input-with-icon',
  templateUrl: './input-with-icon.component.html',
})
export class InputWithIconComponent {
  @Input() placeholder: string;
  @Input() type: string;

  @Input() iconClassName?: string;

  @Output() valueChange = new EventEmitter<any>();

  onChange(e) {
    this.valueChange.emit(e.target.value);
  }
}
