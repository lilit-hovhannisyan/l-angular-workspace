import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AtomsModules } from '../atoms/atoms.module';

import { DataListComponent } from './data-list/data-list.component';
import { ActionCardComponent } from './action-card/action-card.component';
import { BtnWithIconComponent } from './button-with-icon/button-with-icon.component';
import { LinkWithIconComponent } from './link-with-icon/link-with-icon.component';
import { DownloadLinkWithIconComponent } from './download-link-with-icon/download-link-with-icon.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { InputWithIconComponent } from './input-with-icon/input-with-icon.component';
import { NavigationItemComponent } from './navigation/navigation-item/navigation-item.component';
import { NavigationComponent } from './navigation/navigation.component';

import {ModalWindowComponent} from './modal-window/modal-window.component';
import {PaginationComponent} from './pagination/pagination.component';
import {SliderComponent } from './slider/slider.component';
import {DataTableComponent} from './data-table/data-table.component';
import {WildcardSearchComponent} from './wildcard-search/wildcard-search.component';
import { FileUploadComponent } from '../molecules/file-upload/file-upload.component';
import {DateIntervalPickerComponent} from './date-interval-picker/date-interval-picker.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    DataListComponent,
    ActionCardComponent,
    LinkWithIconComponent,
    DownloadLinkWithIconComponent,
    BtnWithIconComponent,
    DropDownComponent,
    InputWithIconComponent,
    NavigationItemComponent,
    NavigationComponent,
    DropDownComponent,
    ModalWindowComponent,
    PaginationComponent,
    SliderComponent,
    DataTableComponent,
    WildcardSearchComponent,
    FileUploadComponent,
    DateIntervalPickerComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AtomsModules,
    TranslateModule
  ],
  exports: [
    DataListComponent,
    ActionCardComponent,
    LinkWithIconComponent,
    DownloadLinkWithIconComponent,
    BtnWithIconComponent,
    DropDownComponent,
    InputWithIconComponent,
    NavigationItemComponent,
    NavigationComponent,
    DropDownComponent,
    ModalWindowComponent,
    DropDownComponent,
    PaginationComponent,
    SliderComponent,
    DataTableComponent,
    WildcardSearchComponent,
    FileUploadComponent,
    DateIntervalPickerComponent
  ]
})
export class MoleculesModule { }
