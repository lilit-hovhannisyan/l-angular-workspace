import { Component, Input } from '@angular/core';

@Component ({
  selector: 'iu-ui-link-with-icon',
  templateUrl: './link-with-icon.component.html',
  styleUrls: ['./link-with-icon.component.less']
})

// usage as tabs in app
export class LinkWithIconComponent {
  // link
  @Input() link: string;
  @Input() text: string;
  @Input() params?: any;

  // icon
  @Input() iconClassName?: string;

  // image
  @Input() src?: string;
  @Input() alt?: string;
}
