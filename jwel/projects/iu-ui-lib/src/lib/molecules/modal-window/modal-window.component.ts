import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component ({
  selector: 'iu-ui-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.css']
})

export class ModalWindowComponent {
  @Input() iconClassName?: string;
  @Input() customClass?: string;

  @Output() closeModalClick = new EventEmitter();

  closeModal() {
    this.closeModalClick.emit();
  }


}
