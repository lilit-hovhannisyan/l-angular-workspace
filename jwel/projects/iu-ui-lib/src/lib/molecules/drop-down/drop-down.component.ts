import { Component, ElementRef, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component ({
  selector: 'iu-ui-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.less'],
})

export class DropDownComponent {
  @Input() visible = false;

  constructor(private _eref: ElementRef) {
  }

  toggle(e) {
    return this.visible = !this.visible;
  }


  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.visible && !this._eref.nativeElement.contains(event.target)) {
      this.visible = false;
    }
  }
}
