import { Component, Input, Output, ContentChild, TemplateRef, EventEmitter } from '@angular/core';
import { NgForOfContext } from '@angular/common';

export interface IDataListItemEvent {
  id: string | number;
  data: any;
}

@Component({
    selector: 'iu-ui-data-list',
    templateUrl: './data-list.component.html',
    styleUrls: ['./data-list.component.less']
})
export class DataListComponent {
    @ContentChild(TemplateRef) itemTemplateRef: TemplateRef<any>;
    @Input() dataSource: any[];
    @Input() itemIdField?: string;
    @Input() customClass_ul?: string;
    @Input() customClass_li?: string;

    @Output() itemClick = new EventEmitter<IDataListItemEvent>();

    // todo create logic for grouping
    // @Input() groupByField?: string;
    // todo create logic (lode more button or infinite scroll)
    // @Input() seeMoreText?: string;
    // @Output() seeMore = new EventEmitter<>();

    constructor() {}

    onItemClick(event, item) {
      this.itemClick.emit(item);
    }

}
