import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'iu-ui-wildcard-search',
  templateUrl: './wildcard-search.component.html',
  styleUrls: ['./wildcard-search.component.less']
})

export class WildcardSearchComponent implements OnInit {
  inputData: any;
  @Input() model: string;
  @Input() placeholder: string;
  @Input() searchCondition?: any;
  @Input() maxLength?: string;
  @Input() minLength?: string;
  @Input() strictLength?: string;

  @Input() results = [];

  @Output() itemClick = new EventEmitter();
  @Output() search = new EventEmitter();

  ngOnInit() {
  }

  onChange(event) {
    this.model = event;

    this.inputData = {text: this.model};

    if (this.searchCondition) {
      const regexp = new RegExp(this.searchCondition, 'g');

      if ( this.model.match(regexp) ) {
        this.inputData['match'] = true;
      } else {
        this.inputData['match'] = false;
      }
    }

    if (this.maxLength) {
      this.inputData['maxLength'] = this.maxLength;
    }

    if (this.minLength) {
      this.inputData['minLength'] = this.minLength;
    }

    if (this.strictLength) {
      this.inputData['strictLength'] = this.strictLength;
    }

    console.log('wildcard result', this.inputData);

    return this.search.emit(this.inputData);
  }

  onItemClick(index, item) {
    this.itemClick.emit({index: index, item: item});
  }
}
