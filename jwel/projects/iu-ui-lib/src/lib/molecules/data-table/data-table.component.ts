import {Component, Input, Output, EventEmitter} from '@angular/core';

type IDisplayNameGetter = () => string;
type IDispatchActionNameGetter = (column, row) => string;

interface CustomClass {
  className: string;
  conditionKey: string;
  conditionValue: string[];
}

export interface IColumnConfig {
  getVisibility?: (data) => any;
  getCellValue?: (rowData, columnData) => any;
  displayValueKey?: string;
  displayName: string | IDisplayNameGetter;
  displayIcon?: string;
  type?: 'data' | 'icon' | 'action' | 'download';
  imageSrcKey?: string;
  actionIcon?: string;
  dispatchActionName?: string | IDispatchActionNameGetter;
  displayActionText?: string;
  sortKey?: string;
  // printSelected?: boolean;
  getDisplayValue?: (data) => any;
  customClass?: string | IDispatchActionNameGetter | CustomClass[];
}

@Component({
  selector: 'iu-ui-data-table',
  templateUrl: './data-table.component.html'
})

export class DataTableComponent {
  @Input() columnsConfig: IColumnConfig[];
  @Input() tableData: any[];
  // @Input() haveCheckboxColumn: boolean;
  @Input() asc_icon: string;
  @Input() desc_icon: string;

  // @Output() sortOrprintSelected = new EventEmitter<any>();
  @Output() sort = new EventEmitter<any>();
  @Output() selectItem = new EventEmitter<any>();
  @Output() actionClick = new EventEmitter<any>();

  sortedIndex = null;
  sortedDuration = true;

  constructor() {
  }

  setCustomClass(column, row) {
    let customClassList = '';
    if (column.customClass ) {
      if (column.customClass instanceof Function) {
        customClassList = column.customClass(column, row);
      }


      if (typeof column.customClass === 'string') {
        customClassList = column.customClass;
      }

      if (column.customClass instanceof Array) {
        // debugger
        column.customClass.map( _customClass => {
          _customClass.conditionValue.map( _conditionValue => {
            const x = row[_customClass.conditionKey];
            // debugger
            if ( row[_customClass.conditionKey] ===  _conditionValue ) {
              customClassList += ` ${_customClass.className}`;
            }
          });
        });
      }

      return customClassList;
    }
  }

  getColumnName(column) {
    if (column.displayName.constructor === Function) {
      return column.displayName();
    }
    return column.displayName;
  }

  getColumnAction(column, row) {
    if (column.dispatchActionName.constructor === Function) {
      return column.dispatchActionName(column, row);
    }

    return column.dispatchActionName;
  }

  getSortIcon(index) {
    if (this.sortedIndex !== index) {
      return this.asc_icon;
    } else if (!this.sortedDuration) {
      return this.desc_icon;
    } else {
      return this.asc_icon;
    }
  }

  onSort(event, item, index) {
    if (item.sortKey) {
      if (this.sortedIndex !== index) {
        this.sortedIndex = index;
        this.sortedDuration = true;
        this.sort.emit( [{
          key: item.sortKey,
          val: this.sortedDuration
        }]);
      } else {
        this.sortedDuration = !this.sortedDuration;
        this.sort.emit( [{
          key: item.sortKey,
          val: this.sortedDuration
        }]);
      }
    }
  }

  isVisible(column) {
    if (column.getVisibility) {
      return column.getVisibility();
    } else {
      return true;
    }
  }

  getCellValue(row, column) {
    if (column.hasOwnProperty('getCellValue')) {
      return column.getCellValue(row, column);
    }
    if (column.hasOwnProperty('getDisplayValue')) {
      return column.getDisplayValue(row[column.displayValueKey]);
    }
    return row[column.displayValueKey];
  }

  onHeaderClick(event, item, index) {
    this.onSort(event, item, index);
  }

  // onSelectItem(event, row) {
  //   this.selectItem.emit({
  //     checkboxState: event.checkboxState,
  //     row
  //   });
  // }

  onActionClick(actionName, column, row) {
    this.actionClick.emit({
      actionName,
      column,
      row
    });
  }
}
