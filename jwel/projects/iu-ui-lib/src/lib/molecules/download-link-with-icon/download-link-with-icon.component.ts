import { Component, Input } from '@angular/core';

@Component ({
  selector: 'iu-ui-download-link-with-icon',
  templateUrl: './download-link-with-icon.component.html',
  styleUrls: ['./download-link-with-icon.component.less']
})

// usage as tabs in app
export class DownloadLinkWithIconComponent {
  // link
  @Input() link: string;
  @Input() text: string;

  // icon
  @Input() iconClassName?: string;

  // image
  @Input() src?: string;
  @Input() alt?: string;
}
