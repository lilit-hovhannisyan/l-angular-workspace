import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {interval, Subject, Subscription} from 'rxjs/index';
import {map} from 'rxjs/operators';
import {takeUntil} from 'rxjs/internal/operators';


@Component({
  selector: 'iu-ui-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.less']
})
export class SliderComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('slideContainer') slideContainer: ElementRef;
  @ViewChild('mainContainer') mainContainer: ElementRef;
  @Input() animation: string;
  @Input() height: string;
  @Input() interval: number;

  cancelInterval = new Subject();
  intervalSubject: Subscription;
  slideIndex: number;
  slides: any[];
  canPress = 1; // a flag to prevent the user from pressing right or left till the end of the animaton.

  constructor() {
  }

  ngOnInit() {
    this.mainContainer.nativeElement.style.height = this.height;
    document.onkeydown = this.checkKey.bind(this);
  }

  ngAfterViewInit() {
    this.slides = this.slideContainer.nativeElement.children;
    switch (this.animation) {
      case 'slide':
        this.slideIndex = 0;
        this.initiateSlideAnimation();
        break;
      case 'fadeIn':
        this.slideIndex = 1;
        this.initiateFadeInAnimation();
        break;
      default:
        this.slideIndex = 1;
        this.animation = 'fadeIn';
        this.initiateFadeInAnimation();
        break;
    }
    this.autoPlay();
  }

  // add fadeIn classes to all slide elements, fadeIn class has the transition effect.
  initiateFadeInAnimation() {
    for (let i = 0; i < this.slides.length; i++) {
      this.slides[i].style.display = 'none';
      this.slides[i].className += ' fadeIn';
    }
    this.showSlides(this.slideIndex);
  }

  // add the transition class and child-slide to all elements except the first one, which is in 0 position.
  initiateSlideAnimation() {
    for (let i = 0; i < this.slides.length; i++) {
      this.registerAnimationEvents(this.slides[i]);
      this.slides[i].classList.add('child-slide');
      this.slides[i].classList.add('transition');
    }
    this.displaceAllExcept(0, '-100%');
  }

  // returns the index in the correct range
  getRealIndex(oldPosition, direction) {
    const newIndex = oldPosition + direction;
    const cycles = Math.ceil(Math.abs(newIndex) / this.slides.length);
    return (newIndex + cycles * this.slides.length) % this.slides.length;
  }


// shifts the elemets to certain direction without animation, excluding the given index.
  displaceAllExcept(exceptionIndex, direction) {
    for (let i = 0; i < this.slides.length; i++) {
      if (i !== exceptionIndex) {
        this.slides[i].classList.remove('transition');
        this.slides[i].style.left = direction;
      }
    }
  }

  registerAnimationEvents(slide) {
    slide.addEventListener('webkitTransitionEnd', this.animationListener.bind(this), false);
    slide.addEventListener('transitionend', this.animationListener.bind(this), false);
    slide.addEventListener('mozTransitionEnd', this.animationListener.bind(this), false);
  }

  animationListener(event) {
    switch (event.type) {
      case 'transitionend':
        this.canPress++;
        break;
      case 'transitionstart':
        this.canPress--;
        break;
    }
  }

  // moves the slide to certain direction, transition class is added if the displace function removes it.
  move(index, direction) {
    if (!this.slides[index].classList.contains('transition')) {
      this.slides[index].classList.add('transition');
    }
    this.slides[index].style.left = direction;
    // transition start event..
    this.animationListener({type: 'transitionstart'});
  }

  // set the new index to '0' position which is the center, change the previous slide position to simulate the sliding effect
  moveSlide(direction) {
    if (this.canPress > 0) {
      this.slideIndex = this.getRealIndex(this.slideIndex, -1 * direction);
      this.displaceByIndex(this.slideIndex, (-1 * direction * 100) + '%');
      setTimeout(() => {
        this.move(this.slideIndex, '0%');
        this.move(this.getRealIndex(this.slideIndex,  direction), (direction * 100) + '%');
      }, 10);
    }
  }

  // shifts the index to certain direction without animation.
  displaceByIndex(index, direction) {
    this.slides[index].classList.remove('transition');
    this.slides[index].style.left = direction;
  }


  // change the slide on left right arrow press.
  checkKey(e) {
    e = e || window.event;
    if (e.keyCode === 37) {
      this.resetInterval();
      this.plusSlides(-1);
    } else if (e.keyCode === 39) {
      this.resetInterval();
      this.plusSlides(1);
    }
  }

  // change the slide
  plusSlides(n) {
    switch (this.animation) {
      case 'slide':
        this.moveSlide(-n);
        break;
      case 'fadeIn':
        this.showSlides(this.slideIndex += n);
        break;
    }
  }

  // this function is called to fadein slides
  showSlides(n) {
    if (this.slides.length > 0) {
      if (n > this.slides.length) {
        this.slideIndex = 1;
      }
      if (n < 1) {
        this.slideIndex = this.slides.length;
      }
      for (let i = 0; i < this.slides.length; i++) {
        this.slides[i].style.display = 'none';
      }
      this.slides[this.slideIndex - 1].style.display = 'block';
    }
  }

  // called from the ui to change the slide plus resting the timer.
  prev() {
    this.resetInterval();
    this.plusSlides(-1);
  }

  next() {
    this.resetInterval();
    this.plusSlides(+1);
  }


  autoPlay() {
    this.intervalSubject = interval(this.interval).pipe(
      takeUntil(this.cancelInterval)
      , map(() => this.plusSlides(1))
    ).subscribe();
  }

  resetInterval() {
    this.cancelInterval.next();
    this.autoPlay(); // start the interval again
  }

  ngOnDestroy() {
    this.intervalSubject.unsubscribe();
    this.cancelInterval.unsubscribe();
  }

}
