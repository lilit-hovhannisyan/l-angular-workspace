import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'iu-ui-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.less']
})

export class FileUploadComponent {
  @Input() text?: string;
  @Input() iconClassName?: string;
  @Input() acceptMultiple = false;
  @Input() accept = '*';

  @Output() fileUpload = new EventEmitter();

  attachFile(e) {
    const fileData = e.target.files;
    if (this.acceptMultiple) {
      this.fileUpload.emit(fileData);
    } else {
      this.fileUpload.emit(fileData[0]);
    }
  }

  uploadFileTrigger(el) {
    el.click();
  }
}
