import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'iu-ui-action-card',
  templateUrl: './action-card.component.html',
  styleUrls: ['./action-card.component.less']
})
export class ActionCardComponent {
  @Input() imageUrl?: string;
  @Input() iconClassName?: string;

  @Input() mainTitle?: string;
  @Input() title?: string;
  @Input() description?: string;
  @Input() actionButtonText?: string;

  @Output() actionButtonClick = new EventEmitter<any>();

  constructor() {
  }

  onActionClick() {
    this.actionButtonClick.emit();
  }

}
