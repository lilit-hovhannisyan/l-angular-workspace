const operatorMap = {
  '+': (v1, v2) => {
    if (!v2) {
      return v1;
    }
    return `${v1} ${v2}`;
  }
};


export const evaluate = (scopeObject, expression) => {
  let dynamicEvalFunStr = '(function(){';

  for (const  v in scopeObject) {
    if (scopeObject.hasOwnProperty(v)) {
      let dynamicValue = scopeObject[v];

      if (typeof dynamicValue === 'string') {
        dynamicValue = '\'' + dynamicValue + '\'';
      }

      if (typeof dynamicValue === 'object') {
        dynamicValue = JSON.stringify(dynamicValue);
        dynamicValue = dynamicValue.replace(/"/g, '\'');
      }

      dynamicEvalFunStr += 'var ' + v + '=' + dynamicValue + ';';
    }
  }
  const evalExpression = 'try { return eval(' + '\'' + expression + '\''  + ') } catch(err) { console.log(err);}';
  dynamicEvalFunStr +=  evalExpression; // 'return eval(' + '\'' + expression + '\''  + ')';
  dynamicEvalFunStr += '})()';

  // tslint:disable-next-line no-eval
  return eval(dynamicEvalFunStr);
};

export function deepFind(obj, operationStr) {

  return evaluate(obj, operationStr);

  // with (obj) {
  //   const returnValue = eval(operationStr);
  //   console.log(returnValue);
  //   return returnValue
  //
  // }

  // const operator = '+';
  // const operands =  operationStr.split(operator);
  //
  // let returnValue;
  //
  // for (let i = operands.length; i--; ) {
  //   const currentOperand = operands[i].trim();
  //   const currentOperandValue = deepPath(obj, currentOperand);
  //   returnValue = operatorMap[operator](currentOperandValue, returnValue);
  // }
  //
  // return returnValue;
}


export function deepPath(obj, path) {

  const paths = path.split('.');
  let current = obj;
  let i;

  for (i = 0; i < paths.length; ++i) {
    if (current[paths[i]] === undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;
}
