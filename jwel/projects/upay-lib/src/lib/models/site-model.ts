export interface IGlobalMessageData {
  message: string;
  type: 'info' | 'error' | 'warning' | 'success';
}
