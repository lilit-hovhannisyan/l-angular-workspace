import {Subject, throwError} from 'rxjs/index';
import {GetGasBranchesQuery, SearchUcomFixedByPhoneQuery, OfficeApi, GetMultiDebtElectricityQuery} from '../api/index';
import {DebtDetailData, SubscriberData, SubscriberDetailData, PrintConfData, ProviderSubTypeConfig} from './subscriber-model';
import {TranslateService} from '@ngx-translate/core';
import {formatDate} from '@angular/common';
import {Inject, LOCALE_ID} from '@angular/core';
import {map} from 'rxjs/operators';
import {renderBankingCardNumber} from '../misc-services/render-banking-card-number.service';

export interface ISearchField {
  name: string;
  component: 'phone' | 'input' | 'select';
  placeholder: string;
  getOptions?: () => {};
  textField?: string;
  valueField?: string;
  validators?: {
    type: 'pattern';
    rule: string;
    message: string;
  }[];
  getCustomData?: () => {};
}

export class ProviderData {
  iconClass: string;
  logo: string;
  text: string;
  descriptionInfo: string;
  transferInfo: string;
  searchFieldsConfig: ISearchField[];

  id: string;
  name: string;

  desc: string;
  percentage: number;
  price: number;

  amountMin: number;
  amountMax: number;
  invoiceNote: string;

  constructor(data, apiInst: OfficeApi,
              @Inject(LOCALE_ID) private locale?: string) {
    const uiObj = ProviderData.getConfig(data.name, apiInst);

    this.iconClass = uiObj.iconClass;
    this.logo = uiObj.logo;
    // this.text = uiObj.text;
    this.searchFieldsConfig = uiObj.searchFieldsConfig;

    this.id = data.id;
    this.name = data.name;
    this.text = data.name;
    this.descriptionInfo = uiObj.descriptionInfo;
    this.transferInfo = uiObj.transferInfo;
    this.desc = data.desc;
    this.percentage = data.percentage; // ?
    this.price = data.price; // ?


    this.amountMin = data.amountMin;
    this.amountMax = data.amountMax;

    this.invoiceNote = data.invoiceNote;
  }

  static getConfig(id, apiInst?: OfficeApi) {
    const uiDataMap = {
      'Gas': {
        logo: 'gas.png',
        text: 'Gas',
        searchFieldsConfig: [
          {
            name: 'branchId',
            component: 'select',
            getOptions: () => {
              const getGasBranchesQuery = new GetGasBranchesQuery();
              return apiInst.getGasBranches(getGasBranchesQuery);
            },
            textField : 'branchNameAm',
            valueField : 'branchId',
            placeholder: 'Մասնաճյուղ',
          },
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդ. համար'
          },
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtGas(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.customerId,
                infoSource: res,
                updated: res.updated,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'paid_by_idram'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'debt_amount_rest'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  }),
                  new SubscriberDetailData({
                    name: 'Address',
                    value: res.address
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Counter Unit',
                    value: res.counterUnit
                  }),
                  new DebtDetailData({
                    name: 'Cost Unit',
                    value: res.costUnit
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'paid_by_idram',
                    value: res.paidByIdram
                  }),
                  new DebtDetailData({
                    name: 'debt_amount_rest',
                    value: res.debtAmountRest
                  })
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Prev Date',
                    value: res.prevDate
                  }),
                  new DebtDetailData({
                    name: 'Prev Debt Amount',
                    value: res.prevDebtAmount
                  }),
                  new DebtDetailData({
                    name: 'Prev Paid Amount',
                    value: res.prevPaidAmount
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            customerId: infoSource.customerId,
            ...requestData,
          };
          return api.cashPayGas(_requestData);
        }
      },
      'GasService': {
        logo: 'gasservice.png',
        text: 'GasService',
        searchFieldsConfig: [
          {
            name: 'branchId',
            component: 'select',
            getOptions: () => {
              const getGasBranchesQuery = new GetGasBranchesQuery();
              return apiInst.getGasBranches(getGasBranchesQuery);
            },
            textField : 'branchNameAm',
            valueField : 'branchId',
            placeholder: 'Մասնաճյուղ',
          },
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդ. համար',
          },
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtGas(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.customerId,
                infoSource: res,
                updated: res.updated,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'paid_by_idram'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'debt_amount_rest'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  }),
                  new SubscriberDetailData({
                    name: 'Address',
                    value: res.address
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Sub Debt Amount',
                    value: res.subDebtAmount
                  }),
                  new DebtDetailData({
                    name: 'Sub Deposit',
                    value: res.subDeposit
                  }),
                  new DebtDetailData({
                    name: 'paid_by_idram',
                    value: res.subPaidByIdram
                  }),
                  new DebtDetailData({
                    name: 'debt_amount_rest',
                    value: res.subDebtAmountRest
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            customerId: infoSource.customerId,
            ...requestData,
          };
          return api.cashPayGasService(_requestData);
        }
      },
      'ArtsakhGas': {
        logo: 'artsakhgas.png',
        text: 'Artsakh Gas',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtArtsakhGas(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'Counter Unit',
                    value: res.counterUnit
                  }),
                  new DebtDetailData({
                    name: 'Cost Unit',
                    value: res.costUnit
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Prev Date',
                    value: res.prevDate
                  }),
                  new DebtDetailData({
                    name: 'Prev Debt Amount',
                    value: res.prevDebtAmount
                  }),
                  new DebtDetailData({
                    name: 'Prev Paid Amount',
                    value: res.prevPaidAmount
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {      const _requestData: any = {
          ...infoSource,
          ...requestData,
        };
          return api.cashPayArtsakhGas(_requestData);
        }
      },
      'Electricity': {
        logo: 'electricity.png',
        text: 'Electricity',
        // hasPayPurpose: true,
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդ. համար'
          },
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Հեռախոսահամար'
          }
        ],
        customSearchFieldsConfig: [
          {
            name: 'searchByNumber',
            component: 'phone',
            placeholder: 'Կոնտակտ. հեռախոսահամար',
            getCustomData: (query) => {
              const getMultiDebtElectricityQuery = new GetMultiDebtElectricityQuery();
              getMultiDebtElectricityQuery.phone = query.searchByNumber;
              return apiInst.getMultiDebtElectricity(getMultiDebtElectricityQuery).pipe(map(data => {
                return data.map(d => {
                  return {
                    ...d,
                    name: d.customerName
                  };
                });
              }));
            }
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtElectricity(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.customerId,
                infoSource: res,
                updated: res.updated,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'paid_by_idram'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'debt_amount_rest'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  }),
                  new SubscriberDetailData({
                    name: 'Address',
                    value: res.address
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.currentPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Payment Counter',
                    value: res.currentPaymentCounter
                  }),
                  new DebtDetailData({
                    name: 'Payment Night Counter',
                    value: res.currentPaymentNightCounter
                  }),
                  new DebtDetailData({
                    name: 'Payment Traffic',
                    value: res.currentPaymentTraffic
                  }),
                  new DebtDetailData({
                    name: 'Night Traffic',
                    value: res.currentPaymentNightTraffic
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.currentPaymentDebt
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'Subsidy Money',
                    value: res.subsidyMoney
                  }),
                  new DebtDetailData({
                    name: 'paid_by_idram',
                    value: res.currentPaymentPaid
                  }),
                  new DebtDetailData({
                    name: 'debt_amount_rest',
                    value: res.debtAmountRest
                  })
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Last Payment Date',
                    value: res.lastPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Last Payment Debt',
                    value: res.lastPaymentDebt
                  }),
                  new DebtDetailData({
                    name: 'Last Payment Paid',
                    value: res.lastPaymentPaid
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            customerId: infoSource.customerId,
            ...requestData,
          };
          return api.cashPayElectricity(_requestData);
        }
      },
      'ArtsakhElectricity': {
        logo: 'artsakhelectricity.png',
        text: 'Artsakh Electricity',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtArtsakhElectricity(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.currentPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.currentPaymentDebt
                  }),
                  new DebtDetailData({
                    name: 'Subsidy Money',
                    value: res.subsidyMoney
                  }),
                  new DebtDetailData({
                    name: 'Payment Counter',
                    value: res.currentPaymentCounter
                  }),
                  new DebtDetailData({
                    name: 'Payment Night Counter',
                    value: res.currentPaymentNightCounter
                  }),
                  new DebtDetailData({
                    name: 'Payment Traffic',
                    value: res.currentPaymentTraffic
                  }),
                  new DebtDetailData({
                    name: 'Night Traffic',
                    value: res.currentPaymentNightTraffic
                  })
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Last Payment Date',
                    value: res.lastPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Last Payment Debt',
                    value: res.lastPaymentDebt
                  }),
                  new DebtDetailData({
                    name: 'Last Payment Paid',
                    value: res.lastPaymentPaid
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData
          };
          return api.cashPayArtsakhElectricity(_requestData);
        }
      },
      'Water': {
        logo: 'water.png',
        text: 'Water',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդ. համար',
          },
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtWater(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.customerId,
                infoSource: res,
                updated: res.updated,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'paid_by_idram'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'debt_amount_rest'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Phone number',
                    value: res.phone
                  }),
                  new SubscriberDetailData({
                    name: 'Address',
                    value: res.address
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Counter Unit',
                    value: res.counterUnit
                  }),
                  new DebtDetailData({
                    name: 'Cost Unit',
                    value: res.costUnit
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'paid_by_idram',
                    value: res.paidByIdram
                  }),
                  new DebtDetailData({
                    name: 'debt_amount_rest',
                    value: res.debtAmountRest
                  })
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Prev Paid Date',
                    value: res.prevPaidDate
                  }),
                  new DebtDetailData({
                    name: 'Prev Debt Amount',
                    value: res.prevDebtAmount
                  }),
                  new DebtDetailData({
                    name: 'Prev Paid Amount',
                    value: res.prevPaidAmount
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            customerId: infoSource.customerId,
            ...requestData
          };
          return api.cashPayWater(_requestData);
        }
      },
      'Beeline': {
        logo: 'beeline.png',
        text: 'Beeline',
        searchFieldsConfig: [
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Բիլայն հեռախոսահամար',
            validators: [
              {
                type: 'pattern',
                rule: '',
                message: 'error message'
              }
            ]
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.beelineObtainBill({phone: data.phone})
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.phone,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData
          };
          return api.cashPayBeeline(_requestData);
        }
      },
      'BeelineTelephone': {
        logo: 'beelinetelephone.png',
        text: 'Beeline Telephone',
        searchFieldsConfig: [
          {
            name: 'telephone',
            component: 'phone',
            placeholder: 'Բիլայն հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.beelineTelephoneObtainBill({telephone: data.telephone})
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.telephone,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Deposit',
                    value: res.deposit
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData
          };
          return api.cashPayBeelineTelephone(_requestData);
        }
      },
      'UcomCorporate': {
        logo: 'ucomcorporate.png',
        text: 'U!come Corporate',
        searchFieldsConfig: [
          {
            name: 'account',
            component: 'input',
            placeholder: 'Յուքոմ հաշիվ',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtUcomCorporate({account: data.account})
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.client,
                subscriberId: data.account,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Account',
                    value: res.account
                  }),
                  new SubscriberDetailData({
                    name: 'Client',
                    value: res.client
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            account: infoSource.account,
            ...requestData
          };
          return api.cashPayUcomCorporate(_requestData);
        }
      },
      'UcomMobile': {
        logo: 'ucommobile.png',
        text: 'U!come Mobile',
        searchFieldsConfig: [
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Յուքոմ հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtUcomMobile({phone: data.phone})
            .subscribe(res => {
              // account: '1013423'
              // debt: 0
              // payMethod: 'Advance'
              // phone: '55661337'
              const subscriberData = new SubscriberData({
                customerName: res.client,
                subscriberId: data.phone,
                infoSource: res,
                printConf: [
                  new PrintConfData({
                    collection: 'customerDetails',
                    key: 'Client'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Phone',
                    value: res.phone
                  }),
                  new SubscriberDetailData({
                    name: 'Client',
                    value: res.client
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            account: infoSource.account,
            ...requestData
          };
          return api.cashPayUcomMobile(_requestData);
        }
      },
      'UcomFixed': {
        logo: 'ucomfixed.png',
        text: 'U!come Fixed',
        searchFieldsConfig: [
          {
            name: 'number',
            component: 'phone',
            placeholder: 'ID',
          }
        ],
        customSearchFieldsConfig: [
          {
            name: 'searchByNumber',
            component: 'phone',
            placeholder: 'Կոնտակտ. հեռախոսահամար',
            getCustomData: (query) => {
              const searchUcomFixedByPhoneQuery = new SearchUcomFixedByPhoneQuery();
              searchUcomFixedByPhoneQuery.number = query.searchByNumber;
              return apiInst.searchUcomFixedByPhone(searchUcomFixedByPhoneQuery);
            }
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtUcomFixed({number: data.number})
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.client,
                subscriberId: data.number,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Client',
                    value: res.client
                  }),
                  new SubscriberDetailData({
                    name: 'Client Number',
                    value: data.number
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'total',
                    value: res.balance.total
                  }),
                  new DebtDetailData({
                    name: 'internet',
                    value: res.balance.internet
                  }),
                  new DebtDetailData({
                    name: 'phone',
                    value: res.balance.phone
                  }),
                  new DebtDetailData({
                    name: 'tv',
                    value: res.balance.tv
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            account: infoSource.account,
            ...requestData
          };
          return api.cashPayUcomFixed(_requestData);
        }
      },
      'VivaCell': {
        logo: 'vivacell.png',
        text: 'VivaCell-MTS',
        searchFieldsConfig: [
          {
            name: 'phone',
            component: 'phone',
            placeholder: 'Վիվասել հեռախոսահամար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.vivacellObtainBill({phone: data.phone})
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.phone,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            type: infoSource.type,
            ...requestData
          };
          return api.cashPayVivaCell(_requestData);
        }
      },
      'HiLine': {
        logo: 'hiline.png',
        text: 'Hi Line',
        searchFieldsConfig: [
          {
            name: 'phoneOrSubscriberNumber',
            component: 'input',
            placeholder: 'Հեռախոսահամար կամ բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.hiLineBill(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.phoneOrSubscriberNumber,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData
          };
          return api.cashPayHiLine(_requestData);
        }
      },
      'Rostelecom': {
        logo: 'rostelecom.png',
        text: 'Rostelecom',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtRostelecom(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                printConf: [
                  new PrintConfData({
                    collection: 'customerDetails',
                    key: 'Customer Name'
                  })
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [new DebtDetailData({
                  name: 'Debt',
                  value: res.debt
                })]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData
          };
          return api.cashPayRostelecom(_requestData);
        }
      },
      'Interactive': {
        logo: 'interactive.png',
        text: 'Interactive',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtInteractive(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayInteractive(_requestData);
        }
      },
      'Arpinet': {
        logo: 'arpinet.png',
        text: 'Arpinet',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtArpinet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Debt Amount Rest',
                    value: res.debtAmountRest
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArpinet(_requestData);
        }
      },
      'Kamurj': {
        logo: 'kamurj.png',
        text: 'Credit Organization Kamurj',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtKamurj(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayKamurj(_requestData);
        }
      },
      'Eurofootball': {
        logo: 'no-logo.png',
        text: 'Eurofootball',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtEurofootball(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayEurofootball(_requestData);
        }
      },
      'TotoGamingSport': {
        logo: 'totogamingsport.png',
        text: 'Toto Gaming Sport',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtTotoSport(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayTotoGamingSport(_requestData);
        }
      },
      'TotoGamingCasino': {
        logo: 'totogamingcasino.png',
        text: 'Toto Gaming Casino',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtTotoCasino(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayTotoGamingCasino(_requestData);
        }
      },
      'EfuGames': {
        logo: 'efugames.png',
        text: 'Efu Games',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtEfuGames(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: [
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayEfuGames(_requestData);
        }
      },
      'GoodWin': {
        logo: 'goodwin.png',
        text: 'GoodWin',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtGoodwin(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Amount',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt Amount Rest',
                    value: res.debtAmountRest
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayGoodWin(_requestData);
        }
      },
      'Vivarocasino': {
        logo: 'vivarocasino.png',
        text: 'Vivaro Casino',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtVivaroCasino(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayVivarocasino(_requestData);
        }
      },
      'Vivarobet': {
        logo: 'vivarobet.png',
        text: 'Vivaro Bet',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtVivaroBet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayVivarobet(_requestData);
        }
      },
      'Adjarabet': {
        logo: 'adjarabet.png',
        text: 'Adjara Bet',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtAdjarabet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ],
                debtDetails: []
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayAdjarabet(_requestData);
        }
      },
      'Parking': {
        logo: 'no-logo.png',
        text: 'Car parking',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          },
          {
            name: 'tariffId',
            component: 'input',
            placeholder: 'Tariff Id'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtParking(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Act Num',
                    value: res.actNum
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Number',
                    value: res.number
                  }),
                  new DebtDetailData({
                    name: 'Amount',
                    value: res.amount
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayParking(_requestData);
        }
      },
      'ParkingFine': {
        logo: 'no-logo.png',
        text: 'Car parking fine',
        searchFieldsConfig: [
          {
            name: 'pinCode',
            component: 'input',
            placeholder: 'Pin Code'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtParkingFine(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.pinCode,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Act Num',
                    value: res.actNum
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Number',
                    value: res.number
                  }),
                  new DebtDetailData({
                    name: 'Amount',
                    value: res.amount
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayParkingFine(_requestData);
        }
      },
      'Police': {
        logo: 'no-logo.png',
        text: 'Police',
        searchFieldsConfig: [
          {
            name: 'pinCode',
            component: 'input',
            placeholder: 'Pin Code'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtPolice(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.pinCode,
                infoSource: res,
                customerDetails: [
                  // new SubscriberDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Pin Code',
                    value: res.pinCode
                  }),
                  new SubscriberDetailData({
                    name: 'Act Num',
                    value: res.actNum
                  }),
                  new SubscriberDetailData({
                    name: 'Vehicle Number',
                    value: res.vehicleNumber
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Amount',
                    value: res.amount
                  }),
                  new DebtDetailData({
                    name: 'Debt Amount',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Amount Rest',
                    value: res.debtAmountRest
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            actNum: infoSource.actNum,
            customerName: infoSource.customerName,
            ...requestData
          };
          return api.cashPayPolice(_requestData);
        }
      },
      'Policeman': {
        logo: 'no-logo.png',
        text: 'Penalty fined by the policeman',
        searchFieldsConfig: [
          {
            name: 'actNum',
            component: 'input',
            placeholder: 'Act Num'
          },
          {
            name: 'customerName',
            component: 'input',
            placeholder: 'Customer Name'
          },
          {
            name: 'badgeId',
            component: 'input',
            placeholder: 'Badge Id'
          },
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtPoliceman(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerName,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                  new SubscriberDetailData({
                    name: 'Vehicle Number',
                    value: res.vehicleNumber
                  }),
                  new SubscriberDetailData({
                    name: 'Act Num',
                    value: res.actNum
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Amount',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Amount Rest',
                    value: res.debtAmountRest
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayPoliceman(_requestData);
        }
      },
      // 'Finca': {
      //   logo: 'finca.png',
      //   text: 'Finca',
      // },
      'GoodCredit': {
        logo: 'goodcredit.png',
        text: 'GoodCredit',
        descriptionInfo: 'description.goodCreditProviderPayDescription',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtGoodCredit(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.agreementCode,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                infoSource: res,
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayGoodCredit(_requestData);
        }
      },
      'VarksAm': {
        logo: 'varksam.png',
        text: 'VarksAm',
        fixPay: true,
        descriptionInfo: 'description.varksProviderPayDescription',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtVarksAm(data)
            .subscribe(res => {
              console.log('getDebtVarksAm', res);
              const preDebtConfigDetailsCount = res.nextRollover.length;
              const _default = new ProviderSubTypeConfig({
                subscriberId: res.agreementCode,
                customerName: res.customerName,
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    fixPayAmount: true,
                    name: 'Debt',
                    value: res.debt
                  })
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ],
                infoSource: {invoice: '', term: ''}
              });
              let preDebtConfigDetails = {};

              if ( preDebtConfigDetailsCount > 0 ) {
                preDebtConfigDetails = {
                  'default': _default,
                  '_7days': new ProviderSubTypeConfig({
                    debtDetails: [
                      new DebtDetailData({
                        name: 'Term',
                        value: res.nextRollover[0].term.value
                      }),
                      new DebtDetailData({
                        name: 'End Date',
                        value: res.nextRollover[0].newEndDate
                      }),
                      new DebtDetailData({
                        name: 'Fee Amount',
                        value: res.nextRollover[0].feeAmount.amount
                      }),
                    ],
                    infoSource: {invoice: res.nextRollover[0].invoice.number, term: res.nextRollover[0].term.value + ''}
                  }),
                  '_14days': new ProviderSubTypeConfig({
                    debtDetails: [
                      new DebtDetailData({
                        name: 'Term',
                        value: res.nextRollover[1].term.value
                      }),
                      new DebtDetailData({
                        name: 'End Date',
                        value: res.nextRollover[1].newEndDate
                      }),
                      new DebtDetailData({
                        name: 'Fee Amount',
                        value: res.nextRollover[1].feeAmount.amount
                      })
                    ],
                    infoSource: {invoice: res.nextRollover[1].invoice.number, term: res.nextRollover[1].term.value + ''}
                  }),
                  '_30days': new ProviderSubTypeConfig({
                    debtDetails: [
                      new DebtDetailData({
                        name: 'Term',
                        value: res.nextRollover[2].term.value
                      }),
                      new DebtDetailData({
                        name: 'End Date',
                        value: res.nextRollover[2].newEndDate
                      }),
                      new DebtDetailData({
                        name: 'Fee Amount',
                        value: res.nextRollover[2].feeAmount.amount
                      })
                    ],
                    infoSource: {invoice: res.nextRollover[2].invoice.number, term: res.nextRollover[2].term.value + ''}
                  })
                };
              } else {
                preDebtConfigDetails = {
                  'default': _default
                };
              }


              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: res.agreementCode,
                infoSource: {invoice: '', term: ''},
                preDebtConfig: {
                  options: [
                    {
                      'key': 'default',
                      'template': 'default'
                    },
                    {
                      'key': '_7days',
                      'template': '_7days'
                    },
                    {
                      'key': '_14days',
                      'template': '_14days'
                    },
                    {
                      'key': '_30days',
                      'template': '_30days'
                    }
                  ],
                  optionsCount: preDebtConfigDetailsCount
                },
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [],
                preDebtConfigDetails: preDebtConfigDetails
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayVarksAm(_requestData);
        }
      },

      'FastCredit': {
        logo: 'fastcredit.png',
        text: 'FastCredit',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtFastCredit(data)
            .subscribe(res => {
              console.log('getDebtFastCredit', res);
              const preDebtConfigDetailsCount = 2;
              const _debtDetails = [
                new DebtDetailData({
                  name: 'Penalty Amount',
                  value: res.penaltyAmount
                }),
                new DebtDetailData({
                  name: 'Overdue Fee',
                  value: res.overdueFee
                }),
                new DebtDetailData({
                  name: 'Overdue Interest',
                  value: res.overdueInterest
                }),
                new DebtDetailData({
                  name: 'Overdue Principal',
                  value: res.overduePrincipal
                }),
                new DebtDetailData({
                  name: 'Capital Amount',
                  value: res.capitalAmount
                }),
                new DebtDetailData({
                  name: 'Debt',
                  value: res.debt
                })
              ];

              const _prevDebtDetails = [
                new DebtDetailData({
                  name: 'Next Payment',
                  value: res.nextPayment
                }),
                new DebtDetailData({
                  name: 'Next Payment Date',
                  value: res.nextPaymentDate
                }),
                new DebtDetailData({
                  name: 'Near Interest',
                  value: res.nearInterest
                }),
                new DebtDetailData({
                  name: 'Near Principal',
                  value: res.nearPrincipal
                }),
              ];

              const preDebtConfigDetails = {
                'principalPayment': new ProviderSubTypeConfig({
                  debtDetails: _debtDetails,
                  prevDebtDetails: _prevDebtDetails,
                  infoSource: {paymentType: 'deposit'}
                }),
                'currentPayment':  new ProviderSubTypeConfig({
                  debtDetails: _debtDetails,
                  prevDebtDetails: _prevDebtDetails,
                  infoSource: {paymentType: ''}
                })
              };

              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.number,
                infoSource: {invoice: '', term: ''},
                preDebtConfig: {
                  options: [
                    {
                      'key': 'principalPayment',
                      'template': 'principalPayment'
                    },
                    {
                      'key': 'currentPayment',
                      'template': 'currentPayment'
                    }
                  ],
                  optionsCount: preDebtConfigDetailsCount
                },
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [],
                preDebtConfigDetails: preDebtConfigDetails
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayFastCredit(_requestData);
        }
        // paySubscriberServiceAction(api: OfficeApi, data: any, amount, subjectObj: Subject<boolean>) {
        //   alert('no api');
        //   // const requestData: any = { ...data, amount: amount};
        //   // api.cashPayPoliceMan(requestData).subscribe(res => {
        //   //   subjectObj.next(true);
        //   // });
        // }
      },

      // 'FincaContract': {
      //   logo: 'fincacontract.png',
      //   text: 'TransferToCard',
      //   searchFieldsConfig: [
      //     {
      //       name: 'code',
      //       component: 'input',
      //       placeholder: 'Պայմանագրի համար',
      //     }
      //   ],
      //   getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
      //     api.getFincaContract(data)
      //       .subscribe(res => {
      //         console.log('getDebtFincaContract', res);
      //         const preDebtConfigDetailsCount = 2;
      //         const _debtDetails = [
      //           new DebtDetailData({
      //             name: 'Principal Rem',
      //             value: Math.floor(+res.contractDebt.principalRem)
      //           }),
      //           new DebtDetailData({
      //             name: 'Overdue Fee',
      //             value: Math.floor(+res.contractDebt.overdueFee)
      //           }),
      //           new DebtDetailData({
      //             name: 'Overdue Interest',
      //             value: Math.floor(+res.contractDebt.overdueInterest)
      //           }),
      //           new DebtDetailData({
      //             name: 'Overdue Principal',
      //             value: Math.floor(+res.contractDebt.overduePrincipal)
      //           }),
      //           new DebtDetailData({
      //             name: 'DebtAmount',
      //             value: Math.floor(+res.contractDebt.debtAmount)
      //           }),
      //         ];
      //         const _prevDebtDetails = [
      //           new DebtDetailData({
      //             name: 'Near Repay Date',
      //             value: res.contractDebt.nearRepayDate
      //           }),
      //           new DebtDetailData({
      //             name: 'Near Fee',
      //             value: Math.floor(+res.contractDebt.nearFee)
      //           }),
      //           new DebtDetailData({
      //             name: 'Near Interest',
      //             value: Math.floor(+res.contractDebt.nearInterest)
      //           }),
      //           new DebtDetailData({
      //             name: 'Near Principal',
      //             value: Math.floor(+res.contractDebt.nearPrincipal)
      //           }),
      //         ];
      //         const preDebtConfigDetails = {
      //           'principalPayment_finca': new ProviderSubTypeConfig({
      //             debtDetails: _debtDetails,
      //             prevDebtDetails: _prevDebtDetails,
      //             descriptionInfo: 'description.fincaProviderPayDescription',
      //             infoSource: {paymentType: '2'}
      //           }),
      //           'currentPayment_finca':  new ProviderSubTypeConfig({
      //             debtDetails: _debtDetails,
      //             prevDebtDetails: _prevDebtDetails,
      //             infoSource: {paymentType: '4'}
      //           })
      //         };
      //         const subscriberData = new SubscriberData({
      //           customerName: res.contractDebt.name,
      //           subscriberId: res.contractDebt.code,
      //           infoSource: {invoice: '', term: ''},
      //           printConf: [
      //             new PrintConfData({
      //               collection: 'debtDetails',
      //               key: 'principal_rem'
      //             }),
      //             new PrintConfData({
      //               collection: 'debtDetails',
      //               key: 'overdue_fee'
      //             }),
      //             new PrintConfData({
      //               collection: 'debtDetails',
      //               key: 'overdue_interest'
      //             }),
      //             new PrintConfData({
      //               collection: 'debtDetails',
      //               key: 'overdue_principal'
      //             }),
      //             new PrintConfData({
      //               collection: 'prevDebtDetails',
      //               key: 'near_repay_date'
      //             }),
      //             new PrintConfData({
      //               collection: 'prevDebtDetails',
      //               key: 'near_fee'
      //             }),
      //             new PrintConfData({
      //               collection: 'prevDebtDetails',
      //               key: 'near_interest'
      //             }),
      //             new PrintConfData({
      //               collection: 'prevDebtDetails',
      //               key: 'near_principal'
      //             }),
      //           ],
      //           preDebtConfig: {
      //             options: [
      //               {
      //                 'key': 'principalPayment_finca',
      //                 'template': 'principalPayment_finca'
      //               },
      //               {
      //                 'key': 'currentPayment_finca',
      //                 'template': 'currentPayment_finca'
      //               }
      //             ],
      //             optionsCount: preDebtConfigDetailsCount
      //           },
      //           customerDetails: [
      //             new SubscriberDetailData({
      //               name: 'Agreement Code',
      //               value: res.contractDebt.code
      //             }),
      //             new SubscriberDetailData({
      //               name: 'Customer Name',
      //               value: res.contractDebt.name
      //             }),
      //           ],
      //           debtDetails: [],
      //           preDebtConfigDetails: preDebtConfigDetails
      //         });
      //
      //         subjectObj.next(subscriberData);
      //       });
      //   },
      //   paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
      //     const _requestData: any = {
      //       ...infoSource,
      //       ...requestData,
      //     };
      //     return api.cashPayFincaContract(_requestData);
      //   }
      // },

      'FincaContractRegular': {
        logo: 'fincacontract.png',
        text: 'TransferToCard',
        searchFieldsConfig: [
          {
            name: 'code',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getFincaContractRegular(data)
            .subscribe(res => {
              const _debtDetails = [
                new DebtDetailData({
                  name: 'Principal Rem',
                  value: Math.floor(+res.contractDebt.principalRem) + ' ' +  res.listOfContracts[0].currency
                }),
                new DebtDetailData({
                  name: 'Overdue Fee',
                  value: Math.floor(+res.contractDebt.overdueFee)
                }),
                new DebtDetailData({
                  name: 'Overdue Interest',
                  value: Math.floor(+res.contractDebt.overdueInterest)
                }),
                new DebtDetailData({
                  name: 'Overdue Principal',
                  value: Math.floor(+res.contractDebt.overduePrincipal)
                }),
                new DebtDetailData({
                  name: 'DebtAmount',
                  value: Math.floor(+res.contractDebt.debtAmount)
                }),
              ];
              const _prevDebtDetails = [
                new DebtDetailData({
                  name: 'Near Repay Date',
                  value: res.contractDebt.nearRepayDate
                }),
                new DebtDetailData({
                  name: 'Near Fee',
                  value: Math.floor(+res.contractDebt.nearFee)
                }),
                new DebtDetailData({
                  name: 'Near Interest',
                  value: Math.floor(+res.contractDebt.nearInterest) + ' ' +  res.listOfContracts[0].currency
                }),
                new DebtDetailData({
                  name: 'Near Principal',
                  value: Math.floor(+res.contractDebt.nearPrincipal) + ' ' +  res.listOfContracts[0].currency
                }),
              ];
              // const preDebtConfigDetails = {
              //   'principalPayment_finca': new ProviderSubTypeConfig({
              //     debtDetails: _debtDetails,
              //     prevDebtDetails: _prevDebtDetails,
              //     descriptionInfo: 'description.fincaProviderPayDescription',
              //     infoSource: {paymentType: '2'}
              //   }),
              //   'currentPayment_finca':  new ProviderSubTypeConfig({
              //     debtDetails: _debtDetails,
              //     prevDebtDetails: _prevDebtDetails,
              //     infoSource: {paymentType: '4'}
              //   })
              // };
              const subscriberData = new SubscriberData({
                customerName: res.contractDebt.name,
                subscriberId: res.contractDebt.code,
                infoSource: {invoice: '', term: ''},
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'principal_rem'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_fee'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_interest'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_principal'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_repay_date'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_fee'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_interest'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_principal'
                  }),
                ],
                // preDebtConfig: {
                //   options: [
                //     {
                //       'key': 'principalPayment_finca',
                //       'template': 'principalPayment_finca'
                //     },
                //     {
                //       'key': 'currentPayment_finca',
                //       'template': 'currentPayment_finca'
                //     }
                //   ],
                //   optionsCount: preDebtConfigDetailsCount
                // },
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.contractDebt.code
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.contractDebt.name
                  }),
                ],
                debtDetails: _debtDetails,
                prevDebtDetails: _prevDebtDetails,
                // preDebtConfigDetails: preDebtConfigDetails
              });

              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayFincaContractRegular(_requestData);
        }
      },

      'FincaContractPrepay': {
        logo: 'fincacontract.png',
        text: 'TransferToCard',
        descriptionInfo: 'description.fincaProviderPayDescription',
        searchFieldsConfig: [
          {
            name: 'code',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getFincaContractPrepay(data)
            .subscribe(res => {
              const _debtDetails = [
                new DebtDetailData({
                  name: 'Principal Rem',
                  value: Math.floor(+res.contractDebt.principalRem) + ' ' +  res.listOfContracts[0].currency
                }),
                new DebtDetailData({
                  name: 'Overdue Fee',
                  value: Math.floor(+res.contractDebt.overdueFee)
                }),
                new DebtDetailData({
                  name: 'Overdue Interest',
                  value: Math.floor(+res.contractDebt.overdueInterest)
                }),
                new DebtDetailData({
                  name: 'Overdue Principal',
                  value: Math.floor(+res.contractDebt.overduePrincipal)
                }),
                new DebtDetailData({
                  name: 'DebtAmount',
                  value: Math.floor(+res.contractDebt.debtAmount)
                }),
              ];
              const _prevDebtDetails = [
                new DebtDetailData({
                  name: 'Near Repay Date',
                  value: res.contractDebt.nearRepayDate
                }),
                new DebtDetailData({
                  name: 'Near Fee',
                  value: Math.floor(+res.contractDebt.nearFee)
                }),
                new DebtDetailData({
                  name: 'Near Interest',
                  value: Math.floor(+res.contractDebt.nearInterest) + ' ' +  res.listOfContracts[0].currency
                }),
                new DebtDetailData({
                  name: 'Near Principal',
                  value: Math.floor(+res.contractDebt.nearPrincipal) + ' ' +  res.listOfContracts[0].currency
                }),
              ];
              // const preDebtConfigDetails = {
              //   'principalPayment_finca': new ProviderSubTypeConfig({
              //     debtDetails: _debtDetails,
              //     prevDebtDetails: _prevDebtDetails,
              //     descriptionInfo: 'description.fincaProviderPayDescription',
              //     infoSource: {paymentType: '2'}
              //   }),
              //   'currentPayment_finca':  new ProviderSubTypeConfig({
              //     debtDetails: _debtDetails,
              //     prevDebtDetails: _prevDebtDetails,
              //     infoSource: {paymentType: '4'}
              //   })
              // };
              const subscriberData = new SubscriberData({
                customerName: res.contractDebt.name,
                subscriberId: res.contractDebt.code,
                infoSource: {invoice: '', term: ''},
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'principal_rem'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_fee'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_interest'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_principal'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_repay_date'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_fee'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_interest'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'near_principal'
                  }),
                ],
                // preDebtConfig: {
                //   options: [
                //     {
                //       'key': 'principalPayment_finca',
                //       'template': 'principalPayment_finca'
                //     },
                //     {
                //       'key': 'currentPayment_finca',
                //       'template': 'currentPayment_finca'
                //     }
                //   ],
                //   optionsCount: preDebtConfigDetailsCount
                // },
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.contractDebt.code
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.contractDebt.name
                  }),
                ],
                debtDetails: _debtDetails,
                prevDebtDetails: _prevDebtDetails,
                // preDebtConfigDetails: preDebtConfigDetails
              });

              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayFincaContractPrepay(_requestData);
        }
      },
      'Evoca': {
        logo: 'evoca.png',
        text: 'Evoca',
      },
      'EvocaTransferToCard': {
        logo: 'evoca.png',
        text: 'Evoca',
      },

      // 'TransferToCard': {
      //   logo: 'no-logo.png',
      //   text: 'TransferToCard',
      // },
      'Amelia': {
        logo: 'no-logo.png',
        text: 'Amelia',
      },
      'IdBank': {
        logo: 'idbank.png',
        text: 'Id Bank',
      },
      'TankiOnline': {
        logo: 'no-logo.png',
        text: 'Tanki Online',
      },
      'WorldOfTanks': {
        logo: 'no-logo.png',
        text: 'World Of Tanks',
      },
      'WorldOfWarPlains': {
        logo: 'no-logo.png',
        text: 'World Of War Plains',
      },
      'WorldOfWarShips': {
        logo: 'no-logo.png',
        text: 'World Of War Ships',
      },
      'SocialOk': {
        logo: 'no-logo.png',
        text: 'Social Ok',
      },
      'MaryKay': {
        logo: 'marykay.png',
        text: 'Mary Kay',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Consultant number',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtMaryKay(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new SubscriberDetailData({
                    name: 'Customer name',
                    value: res.customerName
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayMaryKay(_requestData);
        }
      },
      'Oriflame': {
        logo: 'oriflame.png',
        text: 'Oriflame',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Consultant number',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtOriflame(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new SubscriberDetailData({
                    name: 'Customer name',
                    value: res.customerName
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayOriflame(_requestData);
        }
      },
      'Ekeng': {
        logo: 'ekeng.png',
        text: 'Ekeng',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Սոցիալական քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtEkeng(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer name',
                    value: res.customerName
                  })
                ],
                debtDetails: [
                  new SubscriberDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayEkeng(_requestData);
        }
      },
      'KarabakhTelecom': {
        logo: 'karabakhtelecom.png',
        text: 'Karabakh Telecom',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtKarabakhTelekom(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayKarabakhTelecom(_requestData);
        }
      },
      'NtvPlus': {
        logo: 'no-logo.png',
        text: 'Ntv Plus',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtNtvPlus(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Rate',
                    value: res.rate
                  }),
                  new SubscriberDetailData({
                    name: 'Currency',
                    value: res.currency
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  // new DebtDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayNtvPlus(_requestData);
        }
      },
      'CTV': {
        logo: 'ctv.png',
        text: 'CTV',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtCtv(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  // new DebtDetailData({
                  //   name: 'Debt Amount Rest',
                  //   value: res.debtAmountRest
                  // }),
                  // new DebtDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayCtv(_requestData);
        }
      },
      'YourNet': {
        logo: 'yournet.png',
        text: 'YourNet',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtYournet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  // new DebtDetailData({
                  //   name: 'Debt Amount Rest',
                  //   value: res.debtAmountRest
                  // }),
                  // new DebtDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayYourNet(_requestData);
        }
      },
      'IdramWallet': {
        logo: 'idramwallet.png',
        text: 'Idram Wallet',
        searchFieldsConfig: [
          {
            name: 'accountId',
            component: 'input',
            placeholder: 'Իդրամ Id'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtIdramWallet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.accountId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'debtAmount',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  // new DebtDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayIdramWallet(_requestData);
        }
      },
      'UpayIdramWallet': {
        logo: 'upayidramwallet.png',
        text: 'Upay Idram Wallet',
        searchFieldsConfig: [
          {
            name: 'accountId',
            component: 'input',
            placeholder: 'Account Id'
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtUpayIdramWallet(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.accountId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'debtAmount',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  }),
                  // new DebtDetailData({
                  //   name: 'Service Id',
                  //   value: res.serviceId
                  // })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayUpayIdramWallet(_requestData);
        }
      },
      'Faberlic': {
        logo: 'faberlic.png',
        text: 'Faberlic',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Consultant number',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtFaberlic(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Customer name',
                    value: res.customerName
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayFaberlic(_requestData);
        }
      },

      'GlobalCredit': {
        logo: 'globalcredit.png',
        text: 'Global Credit',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtGlobalCredit(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayGlobalCredit(_requestData);
        }
      },
      'Aregak': {
        logo: 'aregak.png',
        text: 'Aregak',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtAregak(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayAregak(_requestData);
        }
      },
      'NormanCredit': {
        logo: 'normancredit.png',
        text: 'Norman Credit',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtNormanCredit(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayNormanCredit(_requestData);
        }
      },
      'CardAgrocredit': {
        logo: 'cardagrocredit.png',
        text: 'Card Agrocredit',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtCardAgrocredit(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayCardAgrocredit(_requestData);
        }
      },
      'FarmCreditArmenia': {
        logo: 'farmcreditarmenia.png',
        text: 'Farm Credit Armenia',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtFarmCredit(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayFarmCreditArmenia(_requestData);
        }
      },

      'Bless': {
        logo: 'bless.png',
        text: 'Bless',
        descriptionInfo: 'description.blessProviderPayDescription',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtBless(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayBless(_requestData);
        }
      },

      'SefInternational': {
        logo: 'sefinternational.png',
        text: 'SefInternational',
        searchFieldsConfig: [
          {
            name: 'agreementCode',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          },
          {
            name: 'passport',
            component: 'input',
            placeholder: 'Անձնագիր',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtSefInternational(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                customerName: res.customerName,
                subscriberId: data.agreementCode,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Agreement Code',
                    value: res.agreementCode
                  }),
                  new SubscriberDetailData({
                    name: 'Customer Name Credit',
                    value: res.customerName
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Penalty Amount',
                    value: res.penaltyAmount
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: res.overdueFee
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: res.overdueInterest
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: res.overduePrincipal
                  }),
                  new DebtDetailData({
                    name: 'Capital Amount',
                    value: res.capitalAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debt
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'Next Payment',
                    value: res.nextPayment
                  }),
                  new DebtDetailData({
                    name: 'Next Payment Date',
                    value: res.nextPaymentDate
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: res.nearInterest
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.nearPrincipal
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPaySefInternational(_requestData);
        }
      },

      'UpayTopUp': {
        logo: 'no-logo.png',
        text: 'UpayTopUp',
        searchFieldsConfig: [],
      },

      'AmeriaAccount': {
        logo: 'ameriaaccountpay.png',
        text: 'AmeriaAccount',
        // hasPayPurpose: true,
        searchFieldsConfig: [
          {
            name: 'code',
            component: 'input',
            placeholder: 'Հաշվեհամար / քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          // data['WithAdditionalInfo'] = 1;
          // data['WithBlockInfo'] = 1;
          api.getAmeriaAccount(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.name,
                subscriberId: res.accountNumber,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank Ameria',
                    value: res.name
                  }),
                  new SubscriberDetailData({
                    name: 'Account Number',
                    value: res.accountNumber
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayAmeriaAccount(_requestData);
        }
      },

      'AmeriaAccountLegal': {
        logo: 'ameriaaccountpaylegal.png',
        text: 'AmeriaAccountLegal',
        hasPayPurpose: true,
        searchFieldsConfig: [
          {
            name: 'code',
            component: 'input',
            placeholder: 'Հաշվեհամար / քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          // data['WithAdditionalInfo'] = 1;
          // data['WithBlockInfo'] = 1;
          api.getAmeriaAccount(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.name,
                subscriberId: res.accountNumber,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank Ameria',
                    value: res.name
                  }),
                  new SubscriberDetailData({
                    name: 'Account Number',
                    value: res.accountNumber
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayAmeriaAccountLegal(_requestData);
        }
      },

      'AmeriaContract': {
        logo: 'ameriacontractpay.png',
        text: 'AmeriaContract',
        transferInfo: 'description.transferInfoDescriptionAllTime',
        descriptionInfo: 'description.ameriaProviderPayDescription',
        searchFieldsConfig: [
          {
            name: 'creditCode',
            component: 'input',
            placeholder: 'Վարկային կոդ',
          },
          {
            component: 'divider',
            textField: 'kam'
          },
          {
            name: 'code',
            component: 'input',
            placeholder: 'Պայմանագրի համար',
          },
          {
            name: 'passport',
            component: 'input',
            placeholder: 'ԱՆձնագիր',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getAmeriaContracts(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.contractDebt.name,
                subscriberId: res.contractDebt.code,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'principal_rem'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'penAmounts'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_fee'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_interest'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'overdue_principal'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'DebtAmount'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'near_repay_date',
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'near_principal',
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'near_interest',
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'near_fee',
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.contractDebt.name
                  }),
                  new SubscriberDetailData({
                    name: 'Contract Number',
                    value: res.contractDebt.code
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Principal Rem',
                    value: res.contractDebt.principalRem
                  }),
                  new DebtDetailData({
                    name: 'penAmounts',
                    value: Math.floor(+res.contractDebt.penAmounts)
                  }),
                  new DebtDetailData({
                    name: 'Overdue Fee',
                    value: Math.floor(+res.contractDebt.overdueFee)
                  }),
                  new DebtDetailData({
                    name: 'Overdue Interest',
                    value: Math.floor(+res.contractDebt.overdueInterest)
                  }),
                  new DebtDetailData({
                    name: 'Overdue Principal',
                    value: Math.floor(+res.contractDebt.overduePrincipal)
                  }),
                  new DebtDetailData({
                    name: 'DebtAmount',
                    value: Math.floor(+res.contractDebt.debtAmount)
                  }),
                  new DebtDetailData({
                    name: 'Near Repay Date',
                    // value: formatDate( new Date(res.contractDebt.nearRepayDate), 'dd/MM/yyyy', this.locale)
                    value: res.contractDebt.nearRepayDate
                  }),
                  new DebtDetailData({
                    name: 'Near Principal',
                    value: res.contractDebt.nearPrincipal
                  }),
                  new DebtDetailData({
                    name: 'Near Interest',
                    value: Math.floor(+res.contractDebt.nearInterest)
                  }),
                  new DebtDetailData({
                    name: 'Near Fee',
                    value: Math.floor(+res.contractDebt.nearFee)
                  }),
                ],
                prevDebtDetails: [
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          // debugger;
          const _requestData: any = {
            ...infoSource,
            ...requestData,
            code: infoSource.contractDebt.code
          };
          return api.cashPayAmeriaContract(_requestData);
        }
      },

      'ArdshinCard': {
        logo: 'ardshincard.png',
        text: 'ArdshinCard',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getArdshinCardClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm,
                subscriberId: res.cardNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'warning_text'
                  }),
                  new PrintConfData({
                    collection: 'customerDetails',
                    key: 'account'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm,
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                  new SubscriberDetailData({
                    name: 'cardNumber',
                    value: res.cardNumber
                    // value: res.creditId
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'warning text',
                    value: res.warningText
                  }),
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArdshinCard(_requestData);
        }
      },

      'ArdshinAccount': {
        logo: 'ardshinaccount.png',
        text: 'ArdshinAccount',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Հաշվի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getArdshinAccountClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm,
                subscriberId: res.accountNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'warning_text'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm,
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'warning text',
                    value: res.warningText
                  }),
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArdshinAccount(_requestData);
        }
      },

      'ArdshinDeposit': {
        logo: 'ardshindeposit.png',
        text: 'ArdshinDeposit',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Պայմանագրի կոդ',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getArdshinDepositClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm,
                subscriberId: res.creditId,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'warning_text'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm,
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.creditId
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'warning text',
                    value: res.warningText
                  }),
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArdshinDeposit(_requestData);
        }
      },

      'ArdshinLegal': {
        logo: 'ardshinlegal.png',
        text: 'ArdshinLegal',
        hasPayPurpose: true,
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Հաշվեհամար',
          },
          {
            name: 'taxPayer',
            component: 'input',
            placeholder: 'Հավետիրոջ ՀՎՀՀ',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getArdshinLegalClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.nameAm,
                subscriberId: res.accountNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'warning_text'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.nameAm
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                  new SubscriberDetailData({
                    name: 'tax payer',
                    value: res.taxpayer
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'warning text',
                    value: res.warningText
                  }),
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArdshinLegal(_requestData);
        }
      },

      'ArdshinLoan': {
        logo: 'ardshinloan.png',
        text: 'ArdshinLoan',
        transferInfo: 'description.transferInfoDescriptionAllTime',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Պայմանագրի կոդ',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getArdshinLoanClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm,
                subscriberId: res.creditId,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'rep_date'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'rep_amount'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'rep_amount_amd'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'credit_balance'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'credit_balance_amd'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'warning_text'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm,
                  }),
                  new SubscriberDetailData({
                    name: 'credit id',
                    value: res.creditId
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'warning text',
                    value: res.warningText
                  }),
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  }),
                  new DebtDetailData({
                    name: 'credit balance',
                    displayType: 'amount',
                    value:  Math.ceil(+res.creditBalance)
                  }),
                  new DebtDetailData({
                    name: 'credit balance amd',
                    displayType: 'amount',
                    value: Math.ceil((+res.creditBalance * +res.currRate))
                  }),
                  new DebtDetailData({
                    name: 'rep date',
                    value: res.repDate
                  }),
                  new DebtDetailData({
                    name: 'rep amount',
                    displayType: 'amount',
                    value:  Math.ceil(+res.repAmount)
                  }),
                  new DebtDetailData({
                    name: 'rep amount amd ardshin',
                    displayType: 'amount',
                    value:  Math.ceil((+res.repAmount * +res.currRate))
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayArdshinLoan(_requestData);
        }
      },

      'IdBankCard': {
        logo: 'idbankcard.png',
        text: 'IdBankCard',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getIdBankCardClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                subscriberId: res.accountNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                  }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayIdBankCard(_requestData);
        }
      },

      'IdBankAccount': {
        logo: 'idbankaccount.png',
        text: 'IdBankAccount',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Հաշվի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getIdBankAccountClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                subscriberId: res.accountNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                  }),
                  // new SubscriberDetailData({
                  //   name: 'birthday',
                  //   value: res.birthDay
                  // }),
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.accountNumber
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayIdBankAccount(_requestData);
        }
      },

      'IdBankLoan': {
        logo: 'idbankloan.png',
        text: 'IdBankLoan',
        transferInfo: 'description.transferInfoDescriptionAllTime',
        searchFieldsConfig: [
          {
            name: 'productId',
            component: 'input',
            placeholder: 'Կոդ',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getIdBankLoanClientData(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                infoSource: res,
                customerName: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                subscriberId: res.accountNumber,
                printConf: [
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'next_date'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'currency_code'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_rate'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'next_amount'
                  }),
                  new PrintConfData({
                    collection: 'prevDebtDetails',
                    key: 'next_amount_amd'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_amount'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'curr_amount_amd'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'fine_amount'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'fine_amount_amd'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'timed_amount'
                  }),
                  new PrintConfData({
                    collection: 'debtDetails',
                    key: 'timed_amount_amd'
                  }),
                ],
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Name Bank',
                    value: res.surnameAm + ' ' + res.forenameAm + ' ' + res.patronymicAm,
                  }),
                  // new SubscriberDetailData({
                  //   name: 'birthday',
                  //   value: res.birthDay
                  // }),
                  new SubscriberDetailData({
                    name: 'credit id',
                    value: res.creditId
                  }),
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'currency code',
                    value: res.currencyCode
                  }),
                  new DebtDetailData({
                    name: 'curr rate',
                    value: Math.ceil(+res.currRate)
                  }),
                  new DebtDetailData({
                    name: 'fine amount',
                    value: Math.ceil(+res.fineamnt)
                  }),
                  new DebtDetailData({
                    name: 'fine amount amd',
                    value: Math.ceil(+res.fineamnt * +res.currRate)
                  }),
                  new DebtDetailData({
                    name: 'timed amount',
                    value: Math.ceil(+res.timedamnt)
                  }),
                  new DebtDetailData({
                    name: 'timed amount amd',
                    value: Math.ceil(+res.timedamnt * +res.currRate)
                  }),
                  new DebtDetailData({
                    name: 'curr amount',
                    value: Math.ceil(+res.curramnt)
                  }),
                  new DebtDetailData({
                    name: 'curr amount amd',
                    value: Math.ceil(+res.curramnt * +res.currRate)
                  }),
                ],
                prevDebtDetails: [
                  new DebtDetailData({
                    name: 'next date',
                    value: res.nextdate
                  }),
                  new DebtDetailData({
                    name: 'next amount',
                    value: Math.ceil(+res.nextamnt)
                  }),
                  new DebtDetailData({
                    name: 'next amount amd',
                    value: Math.ceil(+res.nextamnt * +res.currRate)
                  }),
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayIdBankLoan(_requestData);
        }
      },

      'StroyMasterDamafon': {
        logo: 'stroymasterdamafon.png',
        text: 'StroyMasterDamafon',
        searchFieldsConfig: [
          {
            name: 'customerId',
            component: 'input',
            placeholder: 'Բաժանորդային համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.getDebtStroyMasterDamafon(data)
            .subscribe(res => {
              const subscriberData = new SubscriberData({
                subscriberId: data.customerId,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'Customer Id',
                    value: res.customerId
                  })
                ],
                debtDetails: [
                  new DebtDetailData({
                    name: 'Customer name',
                    value: res.customerName
                  }),
                  new DebtDetailData({
                    name: 'Debt',
                    value: res.debtAmount
                  }),
                  new DebtDetailData({
                    name: 'Debt Date',
                    value: res.debtDate
                  })
                ]
              });
              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayStroyMasterDamafon(_requestData);
        }
      },

      'TransferToCard': {
        logo: 'transfertocard.png',
        text: 'TransferToCard',
        searchFieldsConfig: [
          {
            name: 'cardNumber',
            component: 'input',
            placeholder: 'Քարտի համար',
          }
        ],
        getSubscriberInfoAction(api: OfficeApi, data: any, subjectObj: Subject<SubscriberData>) {
          api.checkCard(data)
            .subscribe(res => {
              let errorMsg = null;
              if (!res.exist) {
                errorMsg = 'noCard';
              }

              const subscriberData = new SubscriberData({
                subscriberId:  res.cardNumber,
                infoSource: res,
                customerDetails: [
                  new SubscriberDetailData({
                    name: 'account',
                    value: res.cardNumber
                  })

                  // new SubscriberDetailData({
                  //   name: 'Customer Id',
                  //   value: res.customerId
                  // })
                ],
                debtDetails: [
                  // new DebtDetailData({
                  //   name: 'Customer name',
                  //   value: res.customerName
                  // }),
                ],
                erorMsg: errorMsg
              });

              subjectObj.next(subscriberData);
            });
        },
        paySubscriberServiceAction(api: OfficeApi, requestData: any, infoSource: any) {
          const _requestData: any = {
            ...infoSource,
            ...requestData,
          };
          return api.cashPayTransferToCard(_requestData);
        }
      },


      'Test': {
        logo: 'no-logo.png',
        text: 'Test',
        searchFieldsConfig: [
        ]
      }
    };

    if (!uiDataMap[id]) {
      throw new Error(`invalid provider ${id}`);
    } else {
      return uiDataMap[id];
    }
  }

  static createNoThrow(data, api: OfficeApi): ProviderData {
    try {
      return new ProviderData(data, api);
    } catch ( e ) {
      console.warn(`Failed to create ProviderData for ${data.name}:`, e);
      return null;
    }
  }
}

export class ProviderCategoryData {
  iconClass: string;
  text: string;
  name: string;
  services: ProviderData[];
  hasPayPurpose?: boolean;

  constructor(data, api: OfficeApi) {
    this.iconClass = `icon_${data.icon}`; // uiObj.iconClass;
    // this.text = 'navigation.sidebar.payTab.' + data.name.toLocaleLowerCase().replace(/\s/g, '_');
    this.text = data.name; // .text;

    this.name = data.label; // data.name;

    this.services = data.services.map(s => ProviderData.createNoThrow(s, api)).filter(pd => pd != null);
  }
}
