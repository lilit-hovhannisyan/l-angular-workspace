export class SubscriberDetailData {
  value: string;
  name: string;
  key: string;

  constructor(data) {
    this.value = data.value;
    this.key = data.name;
    this.name = data.name.toLocaleLowerCase().replace(/\s/g, '_');
  }
}

function  getFilteredTypedArray (arrayData, DataConstructor) {
  return arrayData.filter(f => {
    return f.value !== null;
  } ).map(d => {
    return new DataConstructor(d);
  });
}

export class DebtDetailData {
  name: string;
  value: string;
  key: string;
  fixPayAmount: boolean;
  displayType: 'amount';

  constructor(data) {
    this.displayType = data.displayType;
    this.value = data.value;
    this.key = data.name;
    this.name = data.name.toLocaleLowerCase().replace(/\s/g, '_');
    this.fixPayAmount = data.fixPayAmount;
  }
}

export class PrintConfData {
  collection: string;
  key: string;

  constructor(data) {
    this.collection = data.collection;
    this.key = data.key.toLocaleLowerCase().replace(/\s/g, '_');
  }
}

export class ProviderSubTypeConfig {
    debtDetails: DebtDetailData[];
    prevDebtDetails: DebtDetailData[];
    infoSource: any;
    descriptionInfo: string;

    constructor(data) {
      const debtDetails = data.debtDetails || [];
      const prevDebtDetails = data.prevDebtDetails || [];

      this.infoSource = data.infoSource;
      this.descriptionInfo = data.descriptionInfo || '';

      this.debtDetails = getFilteredTypedArray(debtDetails, DebtDetailData);
      //   debtDetails.filter(f=> {
      //   return f.value !== null;
      // } ).map(d => {
      //     return new DebtDetailData(d);
      // });


      this.prevDebtDetails =  getFilteredTypedArray(prevDebtDetails, DebtDetailData);
      //   prevDebtDetails.map(d => {
      //   if(d.value !== null) {
      //     return new DebtDetailData(d);
      //   }
      // });
    }
}

export class SubscriberData {
  subscriberId: string;
  customerName: string;
  icon: string;
  title: string;
  isFavorite: boolean;
  isRecurrent: boolean;
  commissionPercent: number;
  customerDetails: SubscriberDetailData[];
  debtDetails: DebtDetailData[];
  prevDebtDetails?: DebtDetailData[];
  infoSource: any;
  printConf?: PrintConfData[];
  updated?: string;
  preDebtConfig?: any;
  preDebtConfigDetails?: any;
  erorMsg?: string;

  constructor(data) {
    if (data.infoSource) {
      this.infoSource = data.infoSource;
    }
    this.updated = data.updated;
    this.subscriberId = data.subscriberId;
    this.customerName = data.customerName;
    this.icon = data.icon;
    this.title = data.title;
    this.isFavorite = data.isFavorite;
    this.isRecurrent = data.isRecurrent;
    this.commissionPercent = data.commissionPercent;
    this.preDebtConfig = data.preDebtConfig;
    this.preDebtConfigDetails = data.preDebtConfigDetails;
    this.erorMsg = data.erorMsg;

    const customerDetails = data.customerDetails || [];
    const debtDetails = data.debtDetails || [];
    const prinConf = data.printConf;
    const prevDebtDetails = data.prevDebtDetails || [];

    this.customerDetails =  getFilteredTypedArray(customerDetails, SubscriberDetailData);
    //   customerDetails.map(d => {
    //   if(d.value !== null) {
    //     return new SubscriberDetailData(d);
    //   }
    // });
    this.debtDetails = getFilteredTypedArray(debtDetails, DebtDetailData);
    //   debtDetails.map(d => {
    //   if(d.value !== null){
    //     return new DebtDetailData(d);
    //   }
    // });

    this.prevDebtDetails = getFilteredTypedArray(prevDebtDetails, DebtDetailData);
    //   prevDebtDetails.map(d => {
    //   if(d.value !== null) {
    //     return new DebtDetailData(d);
    //   }
    // });

    if (prinConf) {
      this.printConf = prinConf.map(d => new PrintConfData(d));
    }
    // this.printConf__ = data.printConf; // todo Lilit no dif with printConf, so remove unnecessary map function'S
  }
}


