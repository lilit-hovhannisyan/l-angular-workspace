import {Injectable} from '@angular/core';
import {IGlobalMessageData} from '../models/site-model';
import {ErrorHandler} from '../api/error-handler';
import {BehaviorSubject} from 'rxjs/index';


@Injectable({
  providedIn: 'root',
})
@Injectable()
export class SharedSiteDataService {
  private _globalMessageData?: IGlobalMessageData = null;
  globalMessageData = new BehaviorSubject(this._globalMessageData);

  constructor(private errorHandler: ErrorHandler) {
    this.errorHandler.errorData.subscribe(data => {
      if (data) {
        this.setGlobalErrorMessage(data.message);
      }
    });
  }

  private setGlobalMessage(message: string, type: 'info' | 'error' | 'warning' | 'success') {
    // alert('we will send your new password by sms');
    this._globalMessageData = {
      message: message,
      type: type,
    };
    this.globalMessageData.next(this._globalMessageData);
  }

  public removeGlobalMessage() {
    this._globalMessageData = null;
    this.globalMessageData.next(this._globalMessageData);
  }

  public setGlobalInfoMessage(message: string) {
    this.setGlobalMessage(message, 'info');
  }

  public setGlobalErrorMessage(message: string) {
    this.setGlobalMessage(message, 'info');
  }
}
