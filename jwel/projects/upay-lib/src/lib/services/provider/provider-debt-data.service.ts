import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {dateDecrementDay, dateDecrementMonth, dateFormat} from '../../misc-services/date-time.service';

@Injectable({
  providedIn: 'root',
})
export class ProviderDebtDataService {

  translate = (translationKey) => {
    return this.translateService.instant(translationKey);
  }

  date = (date) => {
    return new Date(date);
  }

  dateFormat = (date, format) => {
    return dateFormat(date, format);
  }

  dateDecrementDay = (date) => {
    return dateDecrementDay(date);
  }

  dateDecrementMonth = (date) => {
    return dateDecrementMonth(date);
  }

  constructor( private translateService: TranslateService) {}

  evaluate  (scopeObject, expression) {
    const __t =  this.translate; // in config --- "__t(`translationKey`)"
    const __date = this.date;
    const __dateFormat = this.dateFormat; // in config --- "__dateFormat(__date("2019-11-30T20:00:00.000Z"), "dd/mm/yyyy")"
    const __dateDecrementDay = this.dateDecrementDay; // in config --- "__dateFormat(__dateDecrementDay(__date("2019-11-30T20:00:00.000Z")), "dd/mm/yyyy")""
    const __dateDecrementMonth = this.dateDecrementMonth;

    let dynamicEvalFunStr = '(function(){';

    for (const  v in scopeObject) {
      if (scopeObject.hasOwnProperty(v)) {
        let dynamicValue = scopeObject[v];

        if (typeof dynamicValue === 'string') {
          dynamicValue = '\'' + dynamicValue.replace(/\\/g, '\\\\') + '\'';
        }

        if (typeof dynamicValue === 'object') {
          dynamicValue = JSON.stringify(dynamicValue);
          dynamicValue = dynamicValue.replace(/"/g, '\'');
        }

        dynamicEvalFunStr += 'var ' + v + '=' + dynamicValue + ';';
      }
    }

    const evalExpression = 'try { return eval(' + '\'' + expression + '\''  + ') } catch(err) { console.log(err);}';
    dynamicEvalFunStr +=  evalExpression; // 'return eval(' + '\'' + expression + '\''  + ')';
    dynamicEvalFunStr += '})()';

    // tslint:disable-next-line no-eval
    return eval(dynamicEvalFunStr);
  }


   getDebtFieldValue (obj, operationStr) {
    return this.evaluate(obj, operationStr);
   }

}
