import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs/index';
import {map, switchMap, mergeMap, flatMap} from 'rxjs/internal/operators';

import {OfficeApi, CustomerApi, Order, Page, AdminGetProvidersQuery} from '../../api/index';
import {ProviderDataModel, ProviderLabelDataModel} from './providers-data.model';
import {TranslateService} from '@ngx-translate/core';
import {ResponseCacheService} from '../../api/response-cache.service';
import {formatDate} from '@angular/common';
import { v4 as uuid } from 'uuid';
import {Router} from '@angular/router';
import {ProviderDebtDataService} from './provider-debt-data.service';

// todo Texapoxel
const objectCleanEmpty = (obj) => {
  const returnObject = {...obj};
  for (const propName in returnObject) {
    if (returnObject[propName] === null || returnObject[propName] === undefined || returnObject[propName].toString() === '') {
      delete returnObject[propName];
    }
  }
  return returnObject;
};
/// todo end


export interface IInvoiceDataSource {
  debtData: any;
  amount: any;
  provider: any;
  commissionValue: any;
  receiptData: any;
  companyData: any;
  branchInfo?: any;
  operatorInfo?: any;
  imputedPayNode?: any;
}

const platform = 'MOBILE';

let apiType = 'officeApi'; // 'customerApi'; // 'officeApi';
export const setApiType = (_apiType: 'officeApi' | 'customerApi') => {
  apiType = _apiType;
};

@Injectable({
  providedIn: 'root',
})
export class ProviderService {
  providerLabels: any[];

  constructor(private officeApi: OfficeApi,
              private customerApi: CustomerApi,
              @Inject(LOCALE_ID) private locale: string,
              private responseCacheService: ResponseCacheService,
              private translateService: TranslateService,
              private providerDebtDataService: ProviderDebtDataService,
              private router: Router) {
  }

  get ApiClass() {
    if (apiType === 'officeApi') {
      return this.officeApi;
    }
    if (apiType === 'customerApi') {
      return this.customerApi;
    }
    // return this.customerApi; // this[apiType]; todo poxel fili mej orinak this.officeApi ==== this.ApiClass
  }

  getProviderLabels() {
    const page = new Page();
    page.size = 100000; // todo change api
    page.index = 0;
    const order = new Order();

    return this.responseCacheService.cache(this.ApiClass/*this.officeApi*/, 'getProviderLabels', {
      page: page,
      order: order,
      name: ''
    }).pipe(map(data => {

      // todo in customer response data is array in operator data has items fix in backend
      let providerLabelsArr = data;
      if (data.items) {
        providerLabelsArr = data.items;
      }
      ///
      const providerLabels = providerLabelsArr.map(l => {
        return new ProviderLabelDataModel(l);
      });
      return providerLabels;
    }));

    // return this.officeApi.getProviderLabels({page: page, order: order, name: ''})
  }

  getProviders() {
    const page = new Page();
    page.size = 100000; // todo change api
    page.index = 0;
    const order = new Order();
    const getProvidersQuery = new AdminGetProvidersQuery();
    getProvidersQuery.enabled = true;
    getProvidersQuery.order = order;
    getProvidersQuery.page = page;

    return this.responseCacheService.cache(this.ApiClass/*this.officeApi*/, 'getProviders', getProvidersQuery).pipe(map(data => {
      // todo in customer response data is array in operator data has items fix in backend
      let providersArr = data;
      if (data.items) {
        providersArr = data.items;
      }
      ///
      const providers = providersArr.map(p => {
        return new ProviderDataModel(p);
      });
      return providers;
    }));
    // return this.officeApi.getProviders(getProvidersQuery).pipe(map(data => {
    //   const providers = data.items.map(p => {
    //     return new ProviderDataModel(p);
    //   });
    //   return providers;
    // }));
  }

  getConfiguredProviders() {
    return this.getProviders().pipe(map(providers => {
      const configuredProviders = providers.filter(p => {
        const hasConfig = !!ProviderDataModel.ProvidersConfigMap[p.name];
        if (hasConfig) {
          return true;
        } else {
          console.warn(`Provider does'nt have configuration ${p.name}`);
          return false;
        }
      });
      return configuredProviders;
    }));
  }

  getConfiguredProvidersByProviderKey(providerKey) {
    return this.getConfiguredProviders().pipe(map(providers => {
      const selectedProvider = providers.find(p => {
        return p.config.providerKey.toLowerCase() === providerKey.toLowerCase();
      });
      return selectedProvider;
    }));
  }

  getProvidersByLabelId(labelKey) {

    return this.getProviderLabels().pipe(
      switchMap(labels => {
        const selectedLabel = labels.find(l => {
          return l._link === labelKey;
        });
        const selectedLabelId = selectedLabel ? selectedLabel.id : null;
        return of(selectedLabelId);
      }),
      switchMap(selectedLabelId => {
        return this.getConfiguredProviders().pipe(
          map(providers => {
            const labelProviders = providers.filter(p => {
              return p.labels.includes(selectedLabelId);
            });
            console.log('labelProviders', labelProviders);
            return labelProviders;
          }));
      })
    );
  }

  getProviderCommissionsByProviderId(providerId?: string) {
    if (apiType === 'officeApi') {
      // this.officeApi.getCurrentBranch({})
      // this.responseCacheService.cache(this.officeApi, 'getCurrentBranch', {})

      return this.responseCacheService.cache(this.ApiClass/*this.officeApi*/, 'getCurrentBranch', {}).pipe(
        switchMap(branch => {
          return of(branch);
        }),
        switchMap(branch => {
          const requestData = {
            providerId,
            branchId: branch.id
          };

          return this.responseCacheService.cache(
            this.ApiClass/*this.officeApi*/,
            'getProviderCommissions',
            requestData,
            `getProviderCommissions_${providerId}`);
          // return this.officeApi.getProviderCommissions(requestData);
        })
      );
    } else {
      const requestData = {
        providerId,
        platform //  "WEB",
      }; // todo mtacel clienti api hamar
      // return this.customerApi.getProviderCommissions(requestData);
      return this.responseCacheService.cache(this.ApiClass/*this.officeApi*/,
        'getCommission', // todo change whan arshak done //getProviderCommissions
        requestData,
        `getProviderCommissions_${providerId}`);
    }
  }

  getApiMethodByName(apiMethodName) {
    return (data?: any) => {
      if (!data) {
        data = {};
      }
      return this.ApiClass[apiMethodName](data);
    };
  }

  providerSearchDebt(providerData: any, debtSearchData: any) {
    if (apiType === 'officeApi') {
      const getDebtActionName = providerData.config.getDebtAction.apiActionName;
      return this.getApiMethodByName(getDebtActionName)(debtSearchData).pipe(map((d: any) => {
        if (d.constructor === Array) {
          if (!d.length) {
            return {};
          }
          return d[0];
        }
        return d;
      }));
    }
    if (apiType === 'customerApi') {
      const providerKey = providerData.key;
      // wtf
      const fieldsArr = [];

      for (const fieldName in debtSearchData) {
        if (true) {
          fieldsArr.push({
            name: fieldName,
            value: debtSearchData[fieldName]
          });
        }
      }
      const requestData = {
        agreement: {
          provider: providerKey,
          fields: fieldsArr,
        }
      };
      return this.getApiMethodByName('getBill')(requestData).pipe(map((responseData: any) => {
        console.log(responseData);
        const responseObject = {};

        for (const field of responseData.fields) {
          responseObject[field.name] = field.value;
        }

        return responseObject;
      }));
    }

  }

  providerPayDebt(payData: any) {
    if (apiType === 'officeApi') {
      const getPayActionName = payData.providerData.config.payAction.apiActionName;
      // return this.getApiMethodByName(getPayActionName)(payData);
    }
    if (apiType === 'customerApi') {
      const provider = payData.providerData.key;
      const fieldsArr = [];
      for (const fieldName in payData.customerDebtData) {
        if (true) {
          fieldsArr.push({
            name: fieldName,
            value: payData.customerDebtData[fieldName]
          });
        }
      }
      fieldsArr.push({
        name: 'amount',
        value: payData.amount * 100
      });
      fieldsArr.push({
        name: 'debt',
        value: payData.customerDebtData['debtAmount']
      });

      const invoiceData = {
        amount: payData.amount * 100,
        commission: payData.commission,
        customerDebtData: payData.customerDebtData,
        providerCommissionsRoles: payData.providerCommissionsRoles,
        providerData: payData.providerData,
        providerKey: payData.providerKey,
        imputedPayNode: payData.imputedPayNode,
      };

      fieldsArr.push({
        name: 'clientData',
        value: JSON.stringify(invoiceData)
      });

      const requestData = {
        transactionId: payData.customerDebtData.transactionId,
        paymentId:  uuid(), // payData.customerDebtData.transactionId
        platform,
        agreement: {
          provider,
          fields: fieldsArr
        }
      };

      return this.getApiMethodByName('payBill')(requestData);
    }
  }

  calculateProviderCommission(_amount, commissionsRole) {
    const amount  = _amount * 100;
    let commission = 0;
    if (commissionsRole) {
      const role = commissionsRole.find(cr => {
        if (cr.min * 100 < amount && amount <= cr.max * 100) {
          return true;
        }

        if (cr.min * 100 < amount && !cr.max) {
          return true;
        }
        return false;
      });

      if (role && role.type === 'FIXED') {
        commission = role.value * 100;
      }
      if (role && role.type === 'PERCENT') {
        commission = (role.value * amount) / 100;
        commission = parseFloat(commission.toFixed(2));
      }
    }

    commission = Math.ceil(commission);
    return commission / 100;
  }

  validateAmount(providerConfig, value, debtData?) {
    let minValue = providerConfig.amountMin;
    let maxValue = providerConfig.amountMax;

    if (providerConfig.config && providerConfig.config.hasOwnProperty('amountValidationMapper')) {
      const amountValidationMapper = providerConfig.config.amountValidationMapper;

      if (amountValidationMapper.hasOwnProperty('minKey')) {
        minValue =  this.providerDebtDataService.getDebtFieldValue(debtData, amountValidationMapper.minKey);
      }

      if (amountValidationMapper.hasOwnProperty('maxKey')) {
        maxValue =  this.providerDebtDataService.getDebtFieldValue(debtData, amountValidationMapper.maxKey);
      }
    }

    if (minValue !== undefined && minValue !== null && value < minValue) {
      return {
        isValid: false,
        error: `Գումարը փոքր է ${minValue} դրամից:`
      };
    }
    if (maxValue !== undefined && maxValue !== null && value > maxValue) {
      return {
        isValid: false,
        error: `Գումարը մեծ է ${maxValue} դրամից:`
      };
    }
    return {
      isValid: true,
      error: null
    };
  }

  // getProviderDepDataForMapping(provider, debtData) {
  //
  // }

  getTransactionRequestData(providerConfig, searchQuery, debtData) {


    const transactionRequestData = {};

    if (providerConfig.config.transactionRequestMapper) {
      providerConfig.config.transactionRequestMapper.map(trc => {

             if (trc.target === 'fromSearchQuery') {
               transactionRequestData[trc.toKey] = this.providerDebtDataService.getDebtFieldValue(searchQuery, trc.fromKey);
             } else {
               transactionRequestData[trc.toKey] = this.providerDebtDataService.getDebtFieldValue(debtData, trc.fromKey);
             }
      });
    }

    // todo write function to merge 2 objects
    return {
      ...searchQuery,
      ...debtData,
      // ...objectCleanEmpty(searchQuery),
      // ...objectCleanEmpty(debtData),
      ...transactionRequestData,
      transactionId: uuid(),
    };

  }

  getInvoiceFieldPrefixValue(invoiceField, debtData) {
    if (invoiceField.hasOwnProperty('prefixValueKey')) {
      return this.providerDebtDataService.getDebtFieldValue(debtData, invoiceField.prefixValueKey);
    }
    return null;
  }

  getInvoiceFieldValue(invoiceField, debtData) {
    let fieldValue;

    if (invoiceField.hasOwnProperty('dynamicMapKey')) {
      fieldValue = this.providerDebtDataService.getDebtFieldValue(debtData, invoiceField.dynamicMapKey);
    } else {
      fieldValue = this.providerDebtDataService.getDebtFieldValue(debtData, invoiceField.valueKey);
    }

    if (invoiceField.hasOwnProperty('valueMapper')) {
      const fieldValueEx =  this.providerDebtDataService.getDebtFieldValue(invoiceField.valueMapper, fieldValue);
      fieldValue =  this.providerDebtDataService.getDebtFieldValue({}, fieldValueEx);
    }

    return fieldValue;

  }

  isNotEmptyGroupFieldValue(invoiceField, debtData) {
    const fieldValue = this.getInvoiceFieldValue(invoiceField, debtData);
    if (fieldValue === undefined || fieldValue === null || fieldValue === '') {
      return false;
    }
    return true;
  }

  getProviderDepDataMapper(provider, debtData) {
    if (debtData.hasOwnProperty('_dynamicDebtType')) { // vark am is have
      if (debtData._dynamicDebtType !== undefined && debtData._dynamicDebtType !== null) {
        return provider.config.getDebtAction.responseMappersList[1].responseMapper;
        // dynamic mapper
      }
      return provider.config.getDebtAction.responseMappersList[0].responseMapper;
      // static mapper
    }
    return provider.config.getDebtAction.responseMapper;
    // default mapper
  }

  getProviderInvoiceData(invoiceDataSource: IInvoiceDataSource) {

   const { debtData, amount, provider, commissionValue, receiptData, branchInfo, operatorInfo, imputedPayNode} = invoiceDataSource;
    const printMapper = this.getProviderDepDataMapper(provider, debtData);
    let branchPrintData;
    if (branchInfo) {
      branchPrintData = [
        {
          displayText: 'branchInfo.phone',
          value: branchInfo.phone
        },
        {
          displayText: 'branchInfo.address',
          value: branchInfo.address
        }
      ];
    }

    const printData = [];

    /// receipt content
    printData.push({
      groupKey: 'receipt',
      hasBr: true,
      fieldsData: [
        {
          displayText: 'receiptInfo.receipt',
          value: receiptData.receiptId,
          bold: true
        },
        {
          displayText: 'receiptInfo.purpose',
          value: receiptData.purpose,
        }
      ]
    });

    /// dinamic content
    printMapper.map(c => {
      const fieldsData = [];
      c.fieldsMap.map(f => {
        console.log(this.isNotEmptyGroupFieldValue(f, debtData), f,   this.getInvoiceFieldValue(f, debtData));
        if ((f.printable === undefined || f.printable === true) && this.isNotEmptyGroupFieldValue(f, debtData)) {
          if (this.isNotEmptyGroupFieldValue(f, debtData)) {
            fieldsData.push({
              ...f,
              value: this.getInvoiceFieldValue(f, debtData),
              prefixValue: this.getInvoiceFieldPrefixValue(f, debtData)
            });
          }
        }
      });

      if (fieldsData.length) {
        printData.push({
          groupKey: c.groupKey,
          hasBr: true,
          fieldsData: fieldsData
        });
      }
    });

    //// imputed pay node
    if (imputedPayNode) {
      printData.push({
        groupKey: 'note',
        hasBr: true,
        fieldsData: [
          {
            displayText: 'providerInfo.imputedPayNode', // todo kareli e provider configi mijic dnel text@
            value: imputedPayNode,
            bold: true
          }
        ]
      });
    }
  //// invoice Note from admin
    if (provider.invoiceNote) {
      printData.push({
        groupKey: 'invoiceNote',
        hasBr: true,
        fieldsData: [
          {
            value: provider.invoiceNote,
            // bold: true
          }
        ]
      });
    }


    //// amount contant
    printData.push({
      groupKey: 'payedAmount',
      hasBr: true,
      fieldsData: [
        {
          displayText: 'providerInfo.payedAmount',
          value: +amount,
          suffixDisplayText: 'measureUnit.amd',
          // bold: true
        },
        {
          displayText: 'providerInfo.payedCommission',
          value: +commissionValue,
          suffixDisplayText: 'measureUnit.amd',
          // bold: true
        }
      ]
    });

    //// total contant
    printData.push({
      groupKey: 'total',
      fieldsData: [
        {
          displayText: 'providerInfo.totallyPayed',
          value: +amount + +commissionValue,
          suffixDisplayText: 'measureUnit.amd',
          bold: true
        }
      ]
    });

    // operator  content
    // let operatorPrintData;
    if (operatorInfo) {
      printData.push(
        {
          groupKey: 'operator',
          fieldsData: [
            {
              displayText: 'providerInfo.cashier',
              value: ``
            },
            {
              displayText: operatorInfo.fullName,
              value: `________________`
            }
          ]
        });
    }

    printData.push({
      groupKey: 'date',
      fieldsData: [
        {
          displayText: 'providerInfo.thanks',
          value: formatDate(new Date(receiptData.issued), 'HH:mm dd/MM/yyyy', this.locale) // todo formating
        }
      ]
    });


    return {
      branchPrintData,
      printData,
    };
  }

  getProviderInvoiceHtml(invoiceDataSource: IInvoiceDataSource) {
    const { companyData } = invoiceDataSource;
    const invoiceData = this.getProviderInvoiceData(invoiceDataSource);

    let branchPrintHtml = '';
    if (invoiceData.branchPrintData) {
      for (const branchData of invoiceData.branchPrintData) {
        branchPrintHtml += `<div style='display: flex;'>
                                  <span>${this.translateService.instant(branchData.displayText)}</span>
                                  <span>${branchData.value}</span>
                           </div>`;
      }
    }

    let contentPrintHtml = '';
    for (const groupData of invoiceData.printData) {
      let groupHtml = '';
      for (const field of groupData.fieldsData) {
        let suffixDisplayText = '';
        if (field.suffixDisplayText) {
          suffixDisplayText += `${this.translateService.instant(field.suffixDisplayText)}`;
        }
        let value = `${field.value} ${suffixDisplayText}`;
        if (field.bold) {
          value = `<b>${value}</b>`;
        }
        let displayText = '';
        let displayValue = `<span style="padding-left: 15px; text-align:right; margin-left: auto;">${value}</span>`;
        if (field.displayText) {
          displayText = `<span style="">${this.translateService.instant(field.displayText)}  ${field.prefixValue || ''}</span>`;
        } else {
          displayValue = `<span style="margin-left: 10px; text-align: left; width: 100%">${value}</span>`;
        }
        groupHtml += `<div style="display: flex; margin-bottom: 3px;">
                               ${displayText}
                               ${displayValue}
                            </div>`;
      }
      if (groupData.hasBr) {
        groupHtml += `<hr />`;
      }
      contentPrintHtml += `<div>${groupHtml}</div>`;
    }

    const htmlStr = `<div style="width: 7cm; border:1px solid black; padding: 5px; margin: 15px auto; font-size: .75rem;">
                        <div style="display: flex; justify-content: space-between; margin-bottom: 20px;">
                          <div>
                            <div> ${this.translateService.instant(companyData.companyName)}</div>
                            <div>${this.translateService.instant(companyData.companyTin)}</div>
                            ${branchPrintHtml}
                          </div>
                          <div >
                            <img src="/shared-assets/img/logo.svg" alt="logo" style="display: block; width: 80px;">
                          </div>
                        </div>
                        ${contentPrintHtml}
                      </div>`;

    return htmlStr;

  }

  getProviderDebtSearchQuery(providerConfig, invoiceDebtData) {
    const searchQuery = {};
    providerConfig.getDebtAction.requestFields.map(r => {
      if (invoiceDebtData.hasOwnProperty(r.requestKey)) {
        searchQuery[r.requestKey] = invoiceDebtData[r.requestKey];
      }
    });
    return searchQuery;
  }

  getTransferMappedData(provider, customerDebtData) {
    const transferMapper = provider.config.getDebtAction.transferMapper;
    const transferMappedData = {};
    if (transferMapper) {
      for (const i of Object.keys(transferMapper)) {
        transferMappedData[i] = this.providerDebtDataService.getDebtFieldValue(customerDebtData, transferMapper[i]); // customerDebtData[transferMapper[i]];
      }
    }

    return transferMappedData;

  }

  redirectToDebtResult(clientData, redirectPath) {
    const providerConfig = clientData.providerData.config;
    const invoiceDebtData = clientData.customerDebtData;
    const searchQuery = this.getProviderDebtSearchQuery(providerConfig, invoiceDebtData);

    const lableId = clientData.providerData.labels[0];

    this.getProviderLabels().subscribe(lables => {
      const selectedLable = lables.find(l => {
        return l.id === lableId;
      });
      this.router.navigate(
        // operator/transactions/terminal
        [`/${redirectPath}/${selectedLable.label}/${providerConfig.providerKey}`],
        {
          queryParams: {...searchQuery},
        });
    });
  }
}

