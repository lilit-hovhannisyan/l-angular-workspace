// import providersConfig
import { providersConfig } from './providers.config/index';
import { Provider, ProviderLabel } from '../../api/index';


export class ProviderLabelDataModel {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;
  parentId: string;
  _link: string;
  _icon: string;

  constructor(data: ProviderLabel) {
   this.id = data.id;
   this.icon =  data.icon;
   this.name = data.name;
   this.label = data.label;
   this.desc = data.desc;
   this.parentId = data.parentId;
   this._link = this.label;
   this._icon =  `icon_${data.icon}`;
  }
}

////////////////// todo start
// export class UIFormSelectModule {
//   component = 'select';
//   getOptionsApiActionName?: string;
//   // getOptionsUrl?: string;
//   // options: [
//   //   {
//   //     branchNameAm: 'a',
//   //     branchId: '1'
//   //   },
//   //   {
//   //     branchNameAm: 'b',
//   //     branchId: '2'
//   //   }
//   // ],
//   textField : string;
//   valueField : string;
//   label?: string;
//   placeholder?: string;
// }
//
//
//
// export class ActionRequestFieldModel {
//   requestKey: string;
// }
//
// export class DebtActionModel {
//   apiActionName: string
// }
//
// export class ProviderConfigModel {
//   providerKey: string;
//   logo: string;
//   displayText: string;
// }


///////////////////////// todo end

// @dynamic
export class ProviderDataModel {
  static providersConfigMap = null;

  id: string;
  name: string;
  accountNumber: string;
  commissionAccountNumber: string;
  key: string;
  desc: string;
  enabled: boolean;
  useFixedAmount: boolean;
  labels: any[];
  paymentPurpose: string;
  // contractDescs: Array<ContractDesc>;
  amountMin: number;
  amountMax: number;
  invoiceNote?: string;

  config: any;

  constructor(data: Provider) {
    this.id = data.id;
    this.name = data.name;
    this.accountNumber = data.accountNumber;
    this.commissionAccountNumber = data.commissionAccountNumber;
    this.key = data.key;
    this.desc = data.desc;
    this.enabled = data.enabled;
    this.useFixedAmount = data.useFixedAmount;
    this.labels = data.labels;
    this.paymentPurpose = data.paymentPurpose;
    this.amountMin = data.amountMin;
    this.amountMax = data.amountMax;
    this.invoiceNote = data.invoiceNote;

    this.config = this.getProviderConfig();

  }


  static get ProvidersConfigMap() {

    if (!this.providersConfigMap) {
      const providersConfigMap = {};
      providersConfig.map(pc => {
        return providersConfigMap[pc.providerKey] = pc;
      });
      this.providersConfigMap = providersConfigMap;
    }

    return this.providersConfigMap;
  }

  getProviderConfig() {
    return ProviderDataModel.ProvidersConfigMap[this.name];
  }

}
