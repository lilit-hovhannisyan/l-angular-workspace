export const yourNet = {
  providerKey: 'YourNet',
  logo: 'yournet.png',
  displayText: 'providers.YourNet',
  getDebtAction: {
    apiActionName: 'getDebtYournet',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayYourNet'
  },
  // billAction : {}
};
