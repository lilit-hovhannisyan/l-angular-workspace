export const totoGamingSport = {
  providerKey: 'TotoGamingSport',
  logo: 'totogamingsport.png',
  displayText: 'providers.TotoGamingSport',
  getDebtAction: {
    apiActionName: 'getDebtTotoSport',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayTotoGamingSport'
  },
  // billAction : {}
};
