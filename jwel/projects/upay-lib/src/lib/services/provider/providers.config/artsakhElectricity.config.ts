export const artsakhElectricity = {
  providerKey: 'ArtsakhElectricity',
  logo: 'artsakhelectricity.png',
  displayText: 'providers.ArtsakhElectricity',
  getDebtAction: {
    apiActionName: 'getDebtArtsakhElectricity',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currentPaymentDate',
            valueKey: 'currentPaymentDate',
          },
          {
            displayText: 'providerInfo.currentPaymentCounter',
            valueKey: 'currentPaymentCounter',
          },
          {
            displayText: 'providerInfo.currentPaymentNightCounter',
            valueKey: 'currentPaymentNightCounter',
          },
          {
            displayText: 'providerInfo.currentPaymentTraffic',
            valueKey: 'currentPaymentTraffic',
          },
          {
            displayText: 'providerInfo.currentPaymentNightTraffic',
            valueKey: 'currentPaymentNightTraffic',
          },
          {
            displayText: 'providerInfo.deposit',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.currentPaymentDebt',
            valueKey: 'currentPaymentDebt',
          },
          {
            displayText: 'providerInfo.subsidyMoney',
            valueKey: 'subsidyMoney',
          },
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.lastPaymentDate',
            valueKey: 'lastPaymentDate',
          },
          {
            displayText: 'providerInfo.lastPaymentDebt',
            valueKey: 'lastPaymentDebt',
          },
          {
            displayText: 'providerInfo.lastPaymentPaid',
            valueKey: 'lastPaymentPaid',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'currentPaymentDebt'
    }
  },
  payAction: {
    apiActionName: 'cashPayArtsakhElectricity'
  },
  // billAction : {}
};
