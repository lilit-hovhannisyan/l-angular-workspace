export const transferToCard = {
  providerKey: 'TransferToCard',
  logo: 'transfertocard.png',
  displayText: 'providers.TransferToCard',
  getDebtAction: {
    apiActionName: 'checkCard',
    requestFields: [
      {
        requestKey: 'cardNumber',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.cardNumber',
          placeholder: 'providerInfo.cardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNumber',
          }
        ]
      }
    ],

    responseErrorMapper: [
      {
        displayText: 'providerInfo.noCardError',
        valueKey: '!exist',
      }
    ],
    transferMapper: {
      customerIdKey: 'cardNumber',
      // customerNameKey: 'surnameAm + forenameAm'
    }
  },
  payAction: {
    apiActionName: 'cashPayTransferToCard'
  },
  // billAction : {}
};
