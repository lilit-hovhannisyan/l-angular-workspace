export const CTV = {
  providerKey: 'CTV',
  logo: 'ctv.png',
  displayText: 'providers.Ctv',
  getDebtAction: {
    apiActionName: 'getDebtCtv',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo․customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo․customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo․debt',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo․debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo․serviceId',
            valueKey: 'serviceId',
          }
        ]
      },
    ]
  },
  payAction: {
    apiActionName: 'cashPayCtv'
  },
  // billAction : {}
};
