export const ardshinLegal = {
  providerKey: 'ArdshinLegal',
  logo: 'ardshinlegal.png',
  displayText: 'providers.ArdshinLegal',
  hasPayImputedNode: true,
  getDebtAction: {
    apiActionName: 'getArdshinLegalClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      },
      {
        requestKey: 'taxPayer',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.taxPayer',
          placeholder: 'providerInfo.taxPayer'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'nameAm',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          },
          {
            displayText: 'providerInfo.taxPayer',
            valueKey: 'taxpayer',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warningText',
            printable: false
          },
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'nameAm'
    }
  },
  payAction: {
    apiActionName: 'cashPayArdshinLegal'
  },
  // billAction : {}
};
