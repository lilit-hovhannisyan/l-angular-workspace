export const rostelecom = {
  providerKey: 'Rostelecom',
  logo: 'rostelecom.png',
  displayText: 'providers.Rostelecom',
  getDebtAction: {
    apiActionName: 'getDebtRostelecom',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayRostelecom'
  },
  // billAction : {}
};
