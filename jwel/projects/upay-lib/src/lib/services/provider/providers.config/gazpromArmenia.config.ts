export const gazpromArmeniaConfig =  {
  providerKey: 'GazpromArmenia',
  logo: 'gas.png',
  displayText: 'providers.GazpromArmenia',
  getDebtAction: {
    hidden: true,
    apiActionName: 'getDebtGazpromArmenia',
    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGazpromBranches',
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: ''
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerCard',
            valueKey: 'customerCard',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.prevDebt',
            prefixValueKey: 'prevDate',
            valueKey: 'prevDebt',
          },
          {
            displayText: 'providerInfo.prevPaid',
            prefixValueKey: 'prevMonth',
            valueKey: 'prevPaid',
          },
          {
            displayText: 'providerInfo.prevCounter',
            prefixValueKey: 'prevDate',
            valueKey: 'prevCounter',
          },
          {
            displayText: 'providerInfo.counter',
            prefixValueKey: 'debtDate',
            valueKey: 'counter',
          },
          {
            displayText: 'providerInfo.expense',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'expense',
          },
          {
            displayText: 'providerInfo.tariff',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'tariff',
          },
          {
            displayText: 'providerInfo.expenseAmount',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'expenseAmount',
          },
          {
            displayText: 'providerInfo.violation',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'violation',
          },
          {
            displayText: 'providerInfo.violationAmount',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'violationAmount',
          },
          {
            displayText: 'providerInfo.fines',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'fines',
          },
          {
            displayText: 'providerInfo.prevYearAmount',
            prefixValueKey: 'prevYear',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'prevYearAmount',
          },
          {
            displayText: 'providerInfo.debtAmount',
            prefixValueKey: 'debtDate',
            valueKey: 'debtAmount',
          },
          {
            displayText: 'providerInfo.remainder',
            valueKey: '+debtAmount - +paidAmount',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'debtAmount'
    }
  },
  getContractList: {
    apiActionName: 'getDebtGazpromArmenia',

    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGazpromBranches',
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: ''
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    columnConfig: [
      {
        displayText: 'providerInfo.customerName',
        valueKey: 'customerName',
      },
      {
        displayText: 'providerInfo.customerId',
        valueKey: 'customerId',
      },
      {
        displayText: 'providerInfo.phone',
        valueKey: 'phone',
      },
      {
        displayText: 'providerInfo.address',
        valueKey: 'address',
      },
      {
        displayText: 'providerInfo.showMore',
        action: 'getDebtAction',
        actionDataMapper: [
          {
            fromKey: 'branchId',
            toKey: 'branchId'
          },
          {
            fromKey: 'phone',
            toKey: 'phone'
          },
          {
            fromKey: 'customerId',
            toKey: 'customerId'
          },
        ],
        icon: 'show'
      }
    ]
  },
  payAction: {
    apiActionName: 'cashPayGazpromArmenia'
  },
};
