export const oriflame = {
  providerKey: 'Oriflame',
  logo: 'oriflame.png',
  displayText: 'providers.Oriflame',
  getDebtAction: {
    apiActionName: 'getDebtOriflame',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'Customer Info',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName'
    }
  },
  payAction: {
    apiActionName: 'cashPayOriflame'
  },
  // billAction : {}
};
