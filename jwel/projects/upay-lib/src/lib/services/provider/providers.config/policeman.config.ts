export const policeman = {
  providerKey: 'Policeman',
  logo: 'policeman.png',
  displayText: 'providers.Policeman',
  getDebtAction: {
    apiActionName: 'getDebtPoliceman',
    requestFields: [
      {
        requestKey: 'actNum',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.actNumber',
          placeholder: 'providerInfo.actNumber',
        }
      },
      {
        requestKey: 'customerName',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerName',
          placeholder: 'providerInfo.customerName',
        }
      },
      {
        requestKey: 'badgeId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.badgeId',
          placeholder: 'providerInfo.badgeId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          },
          {
            displayText: 'providerInfo.vehicleNumber',
            valueKey: 'vehicleNumber',
          },
          {
            displayText: 'providerInfo.actNumber',
            valueKey: 'actNum',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtAmount',
            valueKey: 'debtAmount',
          }
          // {
          //   displayText: 'providerInfo.debtAmountRest',
          //   valueKey: 'debtAmountRest',
          // }
        ]
      }
    ],
    transferMapper: {
      customerIdKey: 'actNum',
      customerNameKey: 'customerName',
      debtKey: 'debtAmount'
    },
  },

  payAction: {
    apiActionName: 'cashPayPoliceman'
  },
  // billAction : {}
};
