export const ucomMobile = {
  providerKey: 'UcomMobile',
  logo: 'ucommobile.png',
  displayText: 'providers.UcomMobile',
  getDebtAction: {
    apiActionName: 'getDebtUcomMobile',
    requestFields: [
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone'
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'client',
            bold: true,
            printable: false
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'phone',
      customerNameKey: 'client',
      debtKey: 'debt'
    },
  },
  payAction: {
    apiActionName: 'cashPayUcomMobile'
  },
  // billAction : {}
};
