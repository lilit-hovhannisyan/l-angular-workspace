export const adjaraBet = {
  providerKey: 'Adjarabet',
  logo: 'adjarabet.png',
  displayText: 'providers.Adjarabet',
  getDebtAction: {
    apiActionName: 'getDebtAdjarabet',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayAdjarabet'
  },
  // billAction : {}
};
