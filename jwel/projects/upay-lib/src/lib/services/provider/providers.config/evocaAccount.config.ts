export const evocaAccount = {
  providerKey: 'EvocaAccount',
  logo: 'evocaaccount.png',
  displayText: 'providers.EvocaAccount',
  getDebtAction: {
    apiActionName: 'getEvocaAccount',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumberOrCardNumber',
          placeholder: 'providerInfo.accountNumberOrCardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'name',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          }
        ]
      }
    ],

    transferMapper: {
      customerNameKey: 'name',
      customerIdKey: 'accountNumber',
    }
  },
  payAction: {
    apiActionName: 'cashPayEvocaAccount'
  },
  // billAction : {}
};
