export const beelineTelephone = {
  providerKey: 'BeelineTelephone',
  logo: 'beelinetelephone.png',
  displayText: 'providers.BeelineTelephone',
  getDebtAction: {
    apiActionName: 'beelineTelephoneObtainBill',
    requestFields: [
      {
        requestKey: 'telephone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.deposit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayBeelineTelephone'
  },
  // billAction : {}
};
