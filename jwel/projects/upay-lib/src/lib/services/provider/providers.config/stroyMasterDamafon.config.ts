export const stroyMasterDamafon = {
  providerKey: 'StroyMasterDamafon',
  logo: 'stroymasterdamafon.png',
  displayText: 'providers.StroyMasterDamafon',
  getDebtAction: {
    apiActionName: 'getDebtStroyMasterDamafon',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debtAmount',
          },
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debtAmount'
    }
  },
  payAction: {
    apiActionName: 'cashPayStroyMasterDamafon'
  },
  // billAction : {}
};
