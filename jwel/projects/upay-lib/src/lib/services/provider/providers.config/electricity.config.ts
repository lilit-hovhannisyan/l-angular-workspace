export const electricity = {
  providerKey: 'Electricity',
  logo: 'electricity.png',
  displayText: 'providers.Electricity',
  getContractList: {
    apiActionName: 'getMultiDebtElectricity',
    requestFields: [
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.contactPhoneNumber',
          placeholder: 'providerInfo.contactPhoneNumber',
        },
      },
    ],

    columnConfig: [
      {
        valueKey: 'customerName',
        displayText: 'providerInfo.customerName',
      },
      {
        valueKey: 'address',
        displayText: 'providerInfo.address',
      },
      {
        valueKey: 'customerId',
        displayText: 'providerInfo.customerId',
      },
      {
        displayText: 'providerInfo.showMore',
        action: 'getDebtAction',
        actionDataMapper: [
          {
            fromKey: 'customerId',
            toKey: 'customerId'
          }
        ],
        icon: 'show'
      }
    ]
  },
  getDebtAction: {
    apiActionName: 'getDebtElectricity',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currentPaymentDate',
            valueKey: 'currentPaymentDate',
          },
          {
            displayText: 'providerInfo.currentPaymentCounter',
            // suffixDisplayText: 'measureUnit.kwh',
            valueKey: 'currentPaymentCounter',
          },
          {
            displayText: 'providerInfo.currentPaymentNightCounter',
            // suffixDisplayText: 'measureUnit.kwh',
            valueKey: 'currentPaymentNightCounter',
          },
          {
            displayText: 'providerInfo.currentPaymentTraffic',
            suffixDisplayText: 'measureUnit.kwh',
            valueKey: 'currentPaymentTraffic',
          },
          {
            displayText: 'providerInfo.currentPaymentNightTraffic',
            suffixDisplayText: 'measureUnit.kwh',
            valueKey: 'currentPaymentNightTraffic',
          },
          {
            displayText: 'providerInfo.currentPaymentDebt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'currentPaymentDebt',
          },
          {
            displayText: 'providerInfo.deposit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.subsidyMoney',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subsidyMoney',
          },
          {
            displayText: 'providerInfo.currentPaymentPaid',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'currentPaymentPaid',
            printable: false
          },
          {
            displayText: 'providerInfo.debtAmountRest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debtAmountRest',
          },
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.lastPaymentDate',
            valueKey: 'lastPaymentDate',
          },
          {
            displayText: 'providerInfo.lastPaymentDebt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'lastPaymentDebt',
          },
          {
            displayText: 'providerInfo.lastPaymentPaid',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'lastPaymentPaid',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'debtAmountRest'
    }
  },
  payAction: {
    apiActionName: 'cashPayElectricity'
  },
  // billAction : {}
};
