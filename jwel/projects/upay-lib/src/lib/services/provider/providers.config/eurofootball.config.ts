export const eurofootball = {
  providerKey: 'Eurofootball',
  logo: 'no-logo.png',
  displayText: 'Eurofootball',
  getDebtAction: {
    apiActionName: 'getDebtEurofootball',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayEurofootball'
  },
  // billAction : {}
};
