export const karabakhTelecom = {
  providerKey: 'KarabakhTelecom',
  logo: 'karabakhtelecom.png',
  displayText: 'providers.KarabakhTelecom',
  getDebtAction: {
    apiActionName: 'getDebtKarabakhTelekom',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      },
    ]
  },
  payAction: {
    apiActionName: 'cashPayKarabakhTelecom'
  },
  // billAction : {}
};
