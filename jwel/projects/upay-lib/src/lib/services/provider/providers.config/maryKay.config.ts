export const maryKay = {
  providerKey: 'MaryKay',
  logo: 'marykay.png',
  displayText: 'providers.MaryKay',
  getDebtAction: {
    apiActionName: 'getDebtMaryKay',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'Customer Info',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName'
    }
  },
  payAction: {
    apiActionName: 'cashPayMaryKay'
  },
  // billAction : {}
};
