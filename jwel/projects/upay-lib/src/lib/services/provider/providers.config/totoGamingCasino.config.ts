export const totoGamingCasino = {
  providerKey: 'TotoGamingCasino',
  logo: 'totogamingcasino.png',
  displayText: 'providers.TotoGamingCasino',
  getDebtAction: {
    apiActionName: 'getDebtTotoCasino',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayTotoGamingCasino'
  },
  // billAction : {}
};
