export const ardshinLoan = {
  providerKey: 'ArdshinLoan',
  logo: 'ardshinloan.png',
  infoTitleText: 'providerInfo.ardshinLoanPayInfoTitle',
  displayText: 'providers.ArdshinLoan',
  getDebtAction: {
    apiActionName: 'getArdshinLoanClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'surnameAm + forenameAm',
          },
          {
            displayText: 'providerInfo.creditId',
            valueKey: 'creditId',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
            printable: false,
          },
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNumber',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warningText',
            printable: false,
          },
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false,
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey:  ' Math.ceil(+currRate)',
            printable: false,
          },
          {
            displayText: 'providerInfo.creditBalance',
            valueKey: 'Math.ceil(+creditBalance)',
            printable: false,
          },
          {
            displayText: 'providerInfo.creditBalanceAmd',
            valueKey:  'Math.ceil((+creditBalance * +currRate))',
            printable: false,
          },
          {
            displayText: 'providerInfo.repDate',
            valueKey:  'repDate',
            printable: false,
          },
          {
            displayText: 'providerInfo.repAmount',
            valueKey:  'Math.ceil(+repAmount)',
            printable: false,
          },
          {
            displayText: 'providerInfo.repAmountAmdArdshin',
            valueKey:  'Math.ceil((+repAmount * +currRate))',
            printable: false,
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'surnameAm + forenameAm',
      debtKey: 'Math.c(+repAmount)'
    }
  },
  payAction: {
    apiActionName: 'cashPayArdshinLoan'
  },
  // billAction : {}
};
