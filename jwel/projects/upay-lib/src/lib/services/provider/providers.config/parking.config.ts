export const parking = {
  providerKey: 'Parking',
  logo: 'no-logo.png',
  displayText: 'providers.Parking',
  getDebtAction: {
    apiActionName: 'getDebtParking',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      },
      {
        requestKey: 'tariffId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.tariffId',
          placeholder: 'providerInfo.tariffId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo․serviceId',
            valueKey: 'serviceId',
          },
          {
            displayText: 'providerInfo․customerName',
            valueKey: 'customerName',
          },
          {
            displayText: 'providerInfo․actNumber',
            valueKey: 'actNum',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo․number',
            valueKey: 'number',
          },
          {
            displayText: 'providerInfo.amount',
            valueKey: 'amount',
          }
        ]
      }
    ]
  },
  payAction: {
    apiActionName: 'cashPayParking'
  },
  // billAction : {}
};
