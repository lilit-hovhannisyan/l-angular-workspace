import {gasConfig} from './gas.config';
import {gasServiceConfig} from './gasService.config';
import {gazpromArmeniaConfig} from './gazpromArmenia.config';
import {gazpromArmeniaServiceConfig} from './gazpromArmeniaService.config';
import {artsakhGasConfig} from './artsakhGas.config';
import {electricity} from './electricity.config';
import {artsakhElectricity} from './artsakhElectricity.config';
import {water} from './water.config';
import {beeline} from './beeline.config';
import {beelineTelephone} from './beelineTelephone.config';
import {ucomCorporate} from './ucomCorporate.config';
import {ucomMobile} from './ucomMobile.config';
import {ucomFixed} from './ucomFixed.config';
import {vivaCell} from './vivaCell.config';
import {hiLine} from './hiLine.config';
import {rostelecom} from './rostelecom.config';
import {interactive} from './interactive.config';
import {arpinet} from './arpinet.config';
import {kamurj} from './kamurj.config';
import {eurofootball} from './eurofootball.config';
import {totoGamingSport} from './totoGamingSport.config';
import {totoGamingCasino} from './totoGamingCasino.config';
import {efuGames} from './efuGames.config';
import {goodWin} from './goodWin.config';
import {vivaroCasino} from './vivaroCasino.config';
import {vivaroBet} from './vivaroBet.config';
import {adjaraBet} from './adjaraBet.config';
import {parking} from './parking.config';
import {parkingFine} from './parkingFine.config';
import {police} from './police.config';
import {policeman} from './policeman.config';
import {goodCredit} from './goodCredit.config';
import {maryKay} from './maryKay.config';
import {oriflame} from './oriflame.config';
import {ekeng} from './ekeng.config';
import {karabakhTelecom} from './karabakhTelecom.config';
import {ntvPlus} from './ntvPlus.config';
import {CTV} from './CTV.config';
import {yourNet} from './yourNet.config';
import {idramWallet} from './idramWallet.config';
import {upayIdramWallet} from './upayIdramWallet.config';
import {faberlic} from './faberlic.config';
import {globalCredit} from './globalCredit.config';
import {aregak} from './aregak.config';
import {normanCredit} from './normanCredit.config';
import {cardAgroCredit} from './cardAgroCredit.config';
import {farmCreditArmenia} from './farmCreditArmenia.config';
import {bless} from './bless.config';
import {sefinternational} from './sefInternational.config';
import {varksAm} from './varksAm.config';
import {fastCredit} from './fastCredit.config';
import {ameriaAccount} from './ameriaAccount.config';
import {ameriaLegalAccount} from './ameriaLegalAccount.config';
import {ameriaContract} from './ameriaContract.config';
import {ardshinLoan} from './ardshinLoan.config';
import {ardshinCard} from './ardshinCard.config';
import {ardshinAccount} from './ardshinAccount.config';
import {ardshinDeposit} from './ardshinDeposit.config';
import {ardshinLegal} from './ardshinLegal.config';
import {stroyMasterDamafon} from './stroyMasterDamafon.config';
import {idBankCard} from './idBankCard.config';
import {idBankAccount} from './idBankAccount.config';
import {idBankLoan} from './idBankLoan.config';
import {transferToCard} from './transferToCard.config';
import {fincaContractRegular} from './fincaContractRegular.config';
import {fincaContractPrepay} from './fincaContractPrepay.config';
import {normanCreditContractPrepay} from './normanCreditContractPrepay.config';
import {normanCreditContractRegular} from './normanCreditContractRegular.config';
import {upayCustomer} from './upayCustomer.config';
import {evocaBankAccountPhysical} from './evocaBankAccountPhysical.config';
import {evocaBankAccountLegal} from './evocaBankAccountLegal.config';
import {evocaBankLoanEarlyRepayment} from './evocaBankLoanEarlyRepayment.config';
import {evocaBankLoanRepayment} from './evocaBankLoanRepayment.config';
import {armatisDamafon} from './armatisDamafon.config';
import {AcbaAccount} from './acbaAccount.config';
import {AcbaCard} from './acbaCard.config';
import {AcbaLoan} from './acbaLoan.config';

export const providersConfig = [
  gasConfig,
  gasServiceConfig,
  gazpromArmeniaConfig,
  gazpromArmeniaServiceConfig,
  artsakhGasConfig,
  electricity,
  artsakhElectricity,
  water,
  beeline,
  beelineTelephone,
  vivaCell,
  hiLine,
  rostelecom,
  interactive,
  totoGamingSport,
  totoGamingCasino,
  efuGames,
  goodWin,
  vivaroCasino,
  vivaroBet,
  adjaraBet,
  parking,
  parkingFine,
  police,
  policeman,
  goodCredit,
  maryKay,
  oriflame,
  faberlic,
  ekeng,
  karabakhTelecom,
  ntvPlus,
  CTV,
  yourNet,
  idramWallet,
  upayIdramWallet,
  globalCredit,
  aregak,
  normanCredit,
  cardAgroCredit,
  farmCreditArmenia,
  bless,
  sefinternational,
  varksAm,
  fastCredit,
  ucomCorporate,
  ucomMobile,
  ucomFixed,
  arpinet,
  kamurj,
  eurofootball,
  ameriaAccount,
  ameriaLegalAccount,
  ameriaContract,
  ardshinLoan,
  ardshinCard,
  ardshinAccount,
  ardshinDeposit,
  ardshinLegal,
  stroyMasterDamafon,
  idBankCard,
  idBankAccount,
  idBankLoan,
  transferToCard,
  fincaContractRegular,
  fincaContractPrepay,
  normanCreditContractPrepay,
  normanCreditContractRegular,
  upayCustomer,
  evocaBankAccountPhysical,
  evocaBankAccountLegal,
  evocaBankLoanEarlyRepayment,
  evocaBankLoanRepayment,
  armatisDamafon,
  AcbaAccount,
  AcbaCard,
  AcbaLoan
];
