export const goodWin = {
  providerKey: 'GoodWin',
  logo: 'goodwin.png',
  displayText: 'providers.GoodWin',
  getDebtAction: {
    apiActionName: 'getDebtGoodwin',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayGoodWin'
  },
  // billAction : {}
};
