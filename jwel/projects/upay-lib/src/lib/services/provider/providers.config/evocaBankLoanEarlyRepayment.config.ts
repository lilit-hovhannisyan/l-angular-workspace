export const evocaBankLoanEarlyRepayment = {
  providerKey: 'EvocaContractPrepay',
  logo: 'evoca.png',
  displayText: 'providers.EvocaContractPrepay',
  getDebtAction: {
    apiActionName: 'getEvocaContractPrepay',
    requestFields: [
      {
        requestKey: 'creditCode',
        uiComponent: {
          component: 'input',
          label: 'creditCode',
          placeholder: 'providerInfo.creditCode'
        }
      },
      {
        uiComponent: {
          component: 'divider',
        }
      },
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      },
      {
        requestKey: 'passport',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.passport',
          placeholder: 'providerInfo.passport'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.code',
            valueKey: 'contractDebt.code',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'contractDebt.name',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtAmount',
            valueKey: 'contractDebt.debtAmount',
          },
          {
            displayText: 'providerInfo.principalRem',
            valueKey: 'contractDebt.principalRem',
            printable: false,
          },
          {
            displayText: 'providerInfo.penAmounts',
            valueKey: 'contractDebt.penAmounts',
            printable: false,
          },
          {
            displayText: 'providerInfo.overdueFee',
            valueKey: 'contractDebt.overdueFee',
            printable: false,
          },
          {
            displayText: 'providerInfo.overdueInterest',
            valueKey: 'contractDebt.overdueInterest',
            printable: false,
          },
          {
            displayText: 'providerInfo.overduePrincipal',
            valueKey: 'contractDebt.overduePrincipal',
            printable: false,
          },
          {
            displayText: 'providerInfo.nearRepayDate',
            valueKey: 'contractDebt.nearRepayDate',
            printable: false,
          },
          {
            displayText: 'providerInfo.nearFee',
            valueKey: 'contractDebt.nearFee',
            printable: false,
          },
          {
            displayText: 'providerInfo.nearInterest',
            valueKey: 'contractDebt.nearInterest',
            printable: false,
          },
          {
            displayText: 'providerInfo.nearPrincipal',
            valueKey: 'contractDebt.nearPrincipal',
            printable: false,
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'contractDebt.code',
      customerNameKey: 'contractDebt.name',
    }
  },
  payAction: {
    apiActionName: 'cashPayEvocaContractPrepay'
  },
  // billAction : {}
};
