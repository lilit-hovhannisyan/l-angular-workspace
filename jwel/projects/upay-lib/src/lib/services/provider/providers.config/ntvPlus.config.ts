export const ntvPlus = {
  providerKey: 'NtvPlus',
  logo: 'no-logo.png',
  displayText: 'providers.NtvPlus',
  getDebtAction: {
    apiActionName: 'getDebtNtvPlus',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          },
        ]
      },
      {
        groupKey: 'rateInfo',
        fieldsMap: [
          {
            displayText: 'providerInfo.rate',
            valueKey: 'rate',
          },
          {
            displayText: 'providerInfo.currency',
            valueKey: 'currency',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayNtvPlus'
  },
  // billAction : {}
};
