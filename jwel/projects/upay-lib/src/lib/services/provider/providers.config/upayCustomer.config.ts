export const upayCustomer = {
  providerKey: 'UpayCustomer',
  logo: 'upaycustomer.png',
  displayText: 'providers.UpayCustomer',
  getDebtAction: {
    apiActionName: 'getDebtUpayCustomer',
    requestFields: [
      {
        requestKey: 'number',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      },
      {
        uiComponent: {
          component: 'divider',
        }
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: '`${forename || ""} ${surname || ""}`',
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
            printable: false,
          },
          {
            displayText: 'providerInfo.status',
            valueKey: 'userType',
            printable: false,
            valueMapper: {
              BRANCH_IDENTIFIED : '__t(`providerInfo.BRANCH_IDENTIFIED`)',
              CARD_IDENTIFIED : '__t(`providerInfo.CARD_IDENTIFIED`)',
              UNIDENTIFIED: '__t(`providerInfo.UNIDENTIFIED`)'
            },
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'number',
          },
          {
            printable: false,
            displayText: 'providerInfo.balance',
            valueKey: 'balance / 100',
          },
          {
            displayText: 'providerInfo.passportOrId',
            valueKey: 'passportId',
          },
          {
            displayText: 'providerInfo.dateOfIssue',
            valueKey: 'dateOfIssue',
          },
          {
            displayText: 'providerInfo.dateOfExpiry',
            valueKey: 'dateOfExpiry',
          },
          {
            displayText: 'providerInfo.authority',
            valueKey: 'authority',
          },
        ]
      },
    ],

    responseErrorMapper: [
      {
        displayText: 'providerInfo.deletedAccount',
        valueKey: 'deleted',
      },
      {
        displayText: 'providerInfo.frozenAccount',
        valueKey: 'accFrozen',
      },
      {
        displayText: 'providerInfo.refillOneTime',
        valueKey: 'refillOneTime',
      },
      {
        displayText: 'providerInfo.mustBeIdentifiedInBranch',
        valueKey: 'userType',
        valueMapper: {
          CARD_IDENTIFIED : true,
          UNIDENTIFIED: true
        },
      },
      // {
      //   displayText: 'providerInfo.documentExpired',
      //   valueKey: 'new Date().setHours(0,0,0,0) >= new Date (dateOfExpiry)',
      // }
    ],

    transferMapper: {
      customerIdKey: 'number',
      customerNameKey: '`${forename || ""} ${surname || ""}`',
    },
  },

  amountValidationMapper: {
    // minKey:
    maxKey: '(balanceLimit - balance) / 100'
  },

  payAction: {
    apiActionName: 'cashPayUpayCustomer'
  },
  // billAction : {}
};
