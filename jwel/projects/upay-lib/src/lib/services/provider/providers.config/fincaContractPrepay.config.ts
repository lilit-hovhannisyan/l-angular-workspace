export const fincaContractPrepay = {
  providerKey: 'FincaContractPrepay',
  logo: 'fincacontract.png',
  displayText: 'providers.FincaContractPrepay',
  infoDescriptionText: 'providerInfo.fincaProviderPayDescription',
  getDebtAction: {
    apiActionName: 'getFincaContractPrepay',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.agreementCode',
            valueKey: 'contractDebt.code',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'contractDebt.name',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.principalRem',
            valueKey: '`${Math.floor(+contractDebt.principalRem)} ${listOfContracts[0].currency}`',
          },
          {
            displayText: 'providerInfo.penAmounts',
            valueKey: '`${+contractDebt.penAmounts} ${listOfContracts[0].currency}`',
          },
          {
            displayText: 'providerInfo.overdueFee',
            valueKey: 'Math.floor(+contractDebt.overdueFee)',
          },
          {
            displayText: 'providerInfo.overdueInterest',
            valueKey: 'Math.floor(+contractDebt.overdueInterest)',
          },
          {
            displayText: 'providerInfo.overduePrincipal',
            valueKey: 'Math.floor(+contractDebt.overduePrincipal)',
          },
          {
            displayText: 'providerInfo.debtAmount',
            valueKey: 'Math.floor(+contractDebt.debtAmount)',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.nearRepayDate',
            valueKey: 'contractDebt.nearRepayDate',
          },
          {
            displayText: 'providerInfo.nearFee',
            valueKey: 'Math.floor(+contractDebt.nearFee)',
          },
          {
            displayText: 'providerInfo.nearInterest',
            valueKey: '`${Math.floor(+contractDebt.nearInterest)} ${listOfContracts[0].currency}`',
          },
          {
            displayText: 'providerInfo.nearPrincipal',
            valueKey: '`${Math.floor(+contractDebt.nearPrincipal)} ${listOfContracts[0].currency}`',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'contractDebt.code',
      customerNameKey: 'contractDebt.name',
      debtKey: 'Math.floor(+contractDebt.debtAmount)'
    }
  },
  payAction: {
    apiActionName: 'cashPayFincaContractPrepay'
  },
  // billAction : {}
};
