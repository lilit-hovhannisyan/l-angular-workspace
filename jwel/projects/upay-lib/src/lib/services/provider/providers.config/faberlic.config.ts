export const faberlic = {
  providerKey: 'Faberlic',
  logo: 'faberlic.png',
  displayText: 'providers.Faberlic',
  getDebtAction: {
    apiActionName: 'getDebtFaberlic',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.consultantNumber',
          placeholder: 'providerInfo.consultantNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'Customer info',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName'
    }
  },
  payAction: {
    apiActionName: 'cashPayFaberlic'
  },
  // billAction : {}
};
