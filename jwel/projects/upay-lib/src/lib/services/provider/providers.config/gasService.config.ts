// idram gas
export const gasServiceConfig = {
  providerKey: 'GasService',
  logo: 'gasservice.png',
  displayText: 'providers.GasService',
  getDebtAction: {
    apiActionName: 'getDebtGas',
    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGasBranches',
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: 10
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true,
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true,
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.subDebtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subDebtAmount',
          },
          {
            displayText: 'providerInfo.subDeposit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subDeposit',
          },
          {
            displayText: 'providerInfo.subPaidByIdram',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subPaidByIdram',
            printable: false
          },
          {
            displayText: 'providerInfo.subDebtAmountRest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subDebtAmountRest',
          }
        ]
      }
    ],

    /// pay info mapper ex. basket table
    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'subDebtAmountRest'
    }
  },
  payAction: {
    apiActionName: 'cashPayGasService'
  },
  // billAction : {}
};
