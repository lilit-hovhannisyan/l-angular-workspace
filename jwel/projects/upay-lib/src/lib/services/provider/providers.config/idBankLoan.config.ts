export const idBankLoan = {
  providerKey: 'IdBankLoan',
  logo: 'idbankloan.png',
  displayText: 'providers.IdBankLoan',
  infoDescriptionText: 'providerInfo.idBankLoanInfoDescription',
  getDebtAction: {
    apiActionName: 'getIdBankLoanClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`',
          },
          {
            displayText: 'providerInfo.creditId',
            valueKey: 'creditId',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
          {
            displayText: 'providerInfo.fineAmount',
            valueKey: 'Math.ceil(+fineamnt)',
            printable: false
          },
          {
            displayText: 'providerInfo.fineAmountAmd',
            valueKey: 'Math.ceil(+fineamnt * +currRate)',
            printable: false
          },
          {
            displayText: 'providerInfo.timedAmount',
            valueKey: 'Math.ceil(+timedamnt)',
            printable: false
          },
          {
            displayText: 'providerInfo.timedAmountAmd',
            valueKey: 'Math.ceil(+timedamnt * +currRate)',
            printable: false
          },
          {
            displayText: 'providerInfo.currAmount',
            valueKey: 'Math.ceil(+curramnt)',
            printable: false
          },
          {
            displayText: 'providerInfo.currAmountAmd',
            valueKey: 'Math.ceil(+curramnt * +currRate)',
            printable: false
          },
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.nextDate',
            valueKey: 'nextdate',
            printable: false
          },
          {
            displayText: 'providerInfo.nextAmount',
            valueKey: 'Math.ceil(+nextamnt)',
            printable: false
          },
          {
            displayText: 'providerInfo.nextAmountAmd',
            valueKey: 'Math.ceil(+nextamnt * +currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'creditId',
      customerNameKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`',
      debtKey: 'Math.ceil(+curramnt * +currRate)'
    }
  },
  payAction: {
    apiActionName: 'cashPayIdBankLoan'
  },
  // billAction : {}
};
