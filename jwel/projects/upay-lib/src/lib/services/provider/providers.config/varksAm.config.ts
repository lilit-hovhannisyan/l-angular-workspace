export const varksAm = {
  providerKey: 'VarksAm',
  logo: 'varksam.png',
  infoDescriptionText: 'providerInfo.varksAmInfoDescription',
  displayText: 'providers.VarksAm',
  getDebtAction: {
    apiActionName: 'getDebtVarksAm',
    requestFields: [
      {
        requestKey: 'agreementCode',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    responseMappersList: [
      {
        displayText: 'providerInfo.loanRepayment',
        responseMapper:  [
          {
            groupKey: 'customer',
            fieldsMap: [
              {
                displayText: 'providerInfo.agreementCode',
                valueKey: 'agreementCode',
              },
              {
                displayText: 'providerInfo.customerName',
                valueKey: 'customerName',
              }
            ]
          },
          {
            groupKey: 'debt',
            fieldsMap: [
              {
                displayText: 'providerInfo.penaltyAmount',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'penaltyAmount',
              },
              {
                displayText: 'providerInfo.overdueFee',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'overdueFee',
              },
              {
                displayText: 'providerInfo.overdueInterest',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'overdueInterest',
              },
              {
                displayText: 'providerInfo.overduePrincipal',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'overduePrincipal',
              },
              {
                displayText: 'providerInfo.capitalAmount',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'capitalAmount',
              },
              {
                displayText: 'providerInfo.debt',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'debt',
              }
            ]
          },
          {
            groupKey: 'prevDebt',
            fieldsMap: [
              {
                displayText: 'providerInfo.nextPayment',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'nextPayment',
              },
              {
                displayText: 'providerInfo.nextPaymentDate',
                valueKey: 'nextPaymentDate',
              },
              {
                displayText: 'providerInfo.nearInterest',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'nearInterest',
              },
              {
                displayText: 'providerInfo.nearPrincipal',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'nearPrincipal',
              }
            ]
          }
        ]
      },
      {
        // mapperType: 'repeat',
        repeatKey: 'nextRollover',
        displayTextKey: 'term.value',
        dynamicTypeKey: 'term.value',
        responseMapper:  [
          {
            groupKey: 'customer',
            fieldsMap: [
              {
                displayText: 'providerInfo.agreementCode',
                from: 'root', // default from index
                valueKey: 'agreementCode',
                dynamicMapKey: 'agreementCode',
              },
              {
                displayText: 'providerInfo.customerName',
                from: 'root',
                valueKey: 'customerName',
                dynamicMapKey: 'customerName',
              }
            ]
          },
          {
            groupKey: 'debt',
            fieldsMap: [
              {
                displayText: 'providerInfo.delayDays',
                // suffixDisplayText: 'measureUnit.amd',
                valueKey: 'term.value',
                dynamicMapKey: 'term',
              },
              {
                displayText: 'providerInfo.newEndDate',
                // suffixDisplayText: 'measureUnit.amd',
                valueKey: 'newEndDate',
                dynamicMapKey: 'newEndDate',
              },
              {
                displayText: 'providerInfo.extensionFee',
                suffixDisplayText: 'measureUnit.amd',
                valueKey: 'extensionFee.amount',
                dynamicMapKey: 'debt',
              },
              {
                displayText: 'providerInfo.invoiceNumber',
                valueKey: 'invoice.number',
                dynamicMapKey: 'invoice',
                printable: false
              }
            ]
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'agreementCode',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    },
  },

  payAction: {
    apiActionName: 'cashPayVarksAm'
  },
  // billAction : {}
};
