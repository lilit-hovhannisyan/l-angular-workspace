export const idramWallet = {
  providerKey: 'IdramWallet',
  logo: 'idramwallet.png',
  displayText: 'providers.IdramWallet',
  getDebtAction: {
    apiActionName: 'getDebtIdramWallet',
    requestFields: [
      {
        requestKey: 'accountId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          // {
          //   displayText: 'providerInfo.debtAmount',
          //   valueKey: 'debtAmount',
          // },
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          // {
          //   displayText: 'providerInfo.serviceId',
          //   valueKey: 'serviceId',
          // }
        ]
      },
    ],
    transferMapper: {
      customerIdKey: 'customerId',
      // customerNameKey: 'customerName'
    }
  },
  payAction: {
    apiActionName: 'cashPayIdramWallet'
  },
  // billAction : {}
};
