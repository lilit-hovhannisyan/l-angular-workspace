export const ameriaLegalAccount = { // todo chi ogtagorcvum AmeriaAccount ashxatum e ev AmeriaAccount-i ev AmeriaAccountLegal-i hamar
  providerKey: 'AmeriaLegalAccount',
  logo: 'ameriaaccountpaylegal.png',
  displayText: 'providers.AmeriaLegalAccount',
  hasPayImputedNode: true,
  getDebtAction: {
    apiActionName: 'getAmeriaAccount',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'name',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'name'
    }
  },
  payAction: {
    apiActionName: 'cashPayAmeriaAccountLegal'
  },
  // billAction : {}
};
