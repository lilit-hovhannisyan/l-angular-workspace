export const AcbaAccount = {
  providerKey: 'AcbaAccount',
  logo: 'acbacard.png',
  displayText: 'providers.AcbaAccount',
  getDebtAction: {
    apiActionName: 'getAcbaAccountProduct',
    requestFields: [
      {
        requestKey: 'identifier',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'recipient',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: '!depositAcc ? identifier : ""',
          },
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNum',
          },
          {
            displayText: 'providerInfo.cardAccount',
            valueKey: 'cardAcc',
          },
          {
            displayText: 'providerInfo.currentAccount',
            valueKey: '!identifier && currentAcc || ""',
          },
          {
            displayText: 'providerInfo.depositAccount',
            valueKey: 'depositAcc',
          },
          {
            displayText: 'providerInfo.depositDocumentNumber',
            valueKey: 'depositDocNum',
          },
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warning',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currency',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+rate)', // todo jshtel
            printable: false
          },
        ]
      }
      ],

    transferMapper: {
      customerIdKey: 'identifier',
      customerNameKey: 'recipient'
    }
  },
  payAction: {
    apiActionName: 'cashPayAcbaAccount'
  },
  // billAction : {}
};
