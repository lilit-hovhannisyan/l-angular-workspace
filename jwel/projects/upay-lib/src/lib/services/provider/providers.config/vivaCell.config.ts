export const vivaCell = {
  providerKey: 'VivaCell',
  logo: 'vivacell.png',
  displayText: 'providers.VivaCell',
  getDebtAction: {
    apiActionName: 'vivacellObtainBill',
    requestFields: [
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ],
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayVivaCell'
  },
  // billAction : {}
};
