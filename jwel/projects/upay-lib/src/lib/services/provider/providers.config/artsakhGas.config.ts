export const artsakhGasConfig = {
  providerKey: 'ArtsakhGas',
  logo: 'artsakhgas.png',
  displayText: 'providers.ArtsakhGas',
  getDebtAction: {
    apiActionName: 'getDebtArtsakhGas',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.deposit',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.counterUnit',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'counterUnit',
          },
          {
            displayText: 'providerInfo.costUnit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'costUnit',
          },
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.prevDate',
            valueKey: 'prevDate',
          },
          {
            displayText: 'providerInfo.prevDebtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevDebtAmount',
          },
          {
            displayText: 'providerInfo.prevPaidAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevPaidAmount',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayArtsakhGas'
  },
  // billAction : {}
};
