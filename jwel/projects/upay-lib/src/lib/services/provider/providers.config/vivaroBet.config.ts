export const vivaroBet = {
  providerKey: 'Vivarobet',
  logo: 'vivarobet.png',
  displayText: 'providers.Vivarobet',
  getDebtAction: {
    apiActionName: 'getDebtVivaroBet',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayVivarobet'
  },
  // billAction : {}
};
