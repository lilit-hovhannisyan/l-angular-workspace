export const ameriaAccount = {
  providerKey: 'AmeriaAccount',
  logo: 'ameriaaccountpay.png',
  displayText: 'providers.AmeriaAccount',
  getDebtAction: {
    apiActionName: 'getAmeriaAccount',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumberOrCardNumber',
          placeholder: 'providerInfo.accountNumberOrCardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'name',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          }
        ]
      }
    ],

    transferMapper: {
      customerNameKey: 'name',
      customerIdKey: 'accountNumber',
    }
  },
  payAction: {
    apiActionName: 'cashPayAmeriaAccount'
  },
  // billAction : {}
};
