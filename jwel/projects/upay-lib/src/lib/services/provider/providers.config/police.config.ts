export const police = {
  providerKey: 'Police',
  logo: 'police.png',
  displayText: 'providers.Police',
  getDebtAction: {
    apiActionName: 'getDebtPolice',
    requestFields: [
      {
        requestKey: 'pinCode',
        uiComponent: {
          component: 'input',
          label: 'providerInfo․pinCode',
          placeholder: 'providerInfo.pinCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.serviceId',
            valueKey: 'serviceId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          },
          {
            displayText: 'providerInfo.pinCode',
            valueKey: 'pinCode',
          },
          {
            displayText: 'providerInfo.vehicleNumber',
            valueKey: 'vehicleNumber',
          },
          {
            displayText: 'providerInfo.actNumber',
            valueKey: 'actNum',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.amount',
            valueKey: 'amount',
          },
          {
            displayText: 'providerInfo.debtAmountRest',
            valueKey: 'debtAmountRest',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'pinCode',
      customerNameKey: 'customerName',
      debtKey: 'amount'
    },
  },


  payAction: {
    apiActionName: 'cashPayPolice'
  },
  // billAction : {}
};
