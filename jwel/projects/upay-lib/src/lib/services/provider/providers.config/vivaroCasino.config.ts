export const vivaroCasino = {
  providerKey: 'Vivarocasino',
  logo: 'vivarocasino.png',
  displayText: 'providers.Vivarocasino',
  getDebtAction: {
    apiActionName: 'getDebtVivaroCasino',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayVivarocasino'
  },
  // billAction : {}
};
