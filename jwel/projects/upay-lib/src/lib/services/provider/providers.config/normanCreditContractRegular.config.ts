export const normanCreditContractRegular = {
  providerKey: 'NormanCreditContractRegular',
  logo: 'normancredit.png',
  displayText: 'providers.NormanCreditContractRegular',
  getDebtAction: {
    apiActionName: 'getNormanCreditContractRegular',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.agreementCode',
            valueKey: 'contractDebt.code',
          },
          {
            displayText: 'providerInfo.borrower',
            valueKey: 'contractDebt.name',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.principalRem',
            valueKey: '`${Math.floor(+contractDebt.principalRem)} ${listOfContracts[0].currency}`',
            printable: false
          },
          {
            displayText: 'providerInfo.overdueFee',
            valueKey: ' Math.floor(+contractDebt.overdueFee)',
            printable: false
          },
          {
            displayText: 'providerInfo.overdueInterest',
            valueKey: 'Math.floor(+contractDebt.overdueInterest)',
            printable: false
          },
          {
            displayText: 'providerInfo.overduePrincipal',
            valueKey: 'Math.floor(+contractDebt.overduePrincipal)',
            printable: false
          },
          {
            displayText: 'providerInfo.debtAmount',
            valueKey: '`${Math.floor(+contractDebt.debtAmount)} AMD`',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.nearRepayDate',
            valueKey: 'contractDebt.nearRepayDate',
            printable: false
          },
          {
            displayText: 'providerInfo.nearFee',
            valueKey: 'Math.floor(+contractDebt.nearFee)',
            printable: false
          },
          {
            displayText: 'providerInfo.nearInterest',
            valueKey: '`${Math.floor(+contractDebt.nearInterest)} ${listOfContracts[0].currency}`',
            printable: false
          },
          {
            displayText: 'providerInfo.nearPrincipal',
            valueKey: '`${Math.floor(+contractDebt.nearPrincipal)} ${listOfContracts[0].currency}`',
            printable: false
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'contractDebt.code',
      customerNameKey: 'contractDebt.name',
      debtKey: 'Math.floor(+contractDebt.debtAmount)'
    }
  },
  payAction: {
    apiActionName: 'cashPayNormanCreditContractRegular'
  },
  // billAction : {}
};
