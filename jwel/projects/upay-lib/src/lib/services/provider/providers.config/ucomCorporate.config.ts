export const ucomCorporate = {
  providerKey: 'UcomCorporate',
  logo: 'ucomcorporate.png',
  displayText: 'providers.UcomCorporate',
  getDebtAction: {
    apiActionName: 'getDebtUcomCorporate',
    requestFields: [
      {
        requestKey: 'account',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'account',
          }, {
            displayText: 'providerInfo.customerName',
            valueKey: 'client',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'account',
      customerNameKey: 'client',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayUcomCorporate'
  },
  // billAction : {}
};
