export const AcbaLoan = {
  providerKey: 'AcbaLoan',
  logo: 'acbacard.png',
  displayText: 'providers.AcbaLoan',
  getDebtAction: {
    apiActionName: 'getAcbaLoanProduct',
    requestFields: [
      {
        requestKey: 'identifier',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'recipient',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: '!depositAcc && identifier || null',
          },
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNum',
          },
          {
            displayText: 'providerInfo.cardAccount',
            valueKey: 'cardAcc',
          },
          {
            displayText: 'providerInfo.currentAccount',
            valueKey: '!identifier && currentAcc || null',
          },
          {
            displayText: 'providerInfo.depositAccount',
            valueKey: 'depositAcc',
          },
          {
            displayText: 'providerInfo.depositDocumentNumber',
            valueKey: 'depositDocNum',
          },
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warning',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currency',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+rate)', // todo jshtel
            printable: false
          },
        ]
      }
      ],

    transferMapper: {
      customerIdKey: 'identifier',
      customerNameKey: 'recipient'
    }
  },
  payAction: {
    apiActionName: 'cashPayAcbaLoan'
  },
  // billAction : {}
};
