export const ardshinAccount = {
  providerKey: 'ArdshinAccount',
  logo: 'ardshinaccount.png',
  displayText: 'providers.ArdshinAccount',
  getDebtAction: {
    apiActionName: 'getArdshinAccountClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'surnameAm + forenameAm',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warningText',
            printable: false
          },
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'surnameAm + forenameAm'
    }
  },
  payAction: {
    apiActionName: 'cashPayArdshinAccount'
  },
  // billAction : {}
};
