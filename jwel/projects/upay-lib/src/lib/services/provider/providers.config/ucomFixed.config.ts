export const ucomFixed = {
  providerKey: 'UcomFixed',
  logo: 'ucomfixed.png',
  displayText: 'providers.UcomFixed',
  getContractList: {
    apiActionName: 'searchUcomFixedByPhone',
    requestFields: [
      {
        requestKey: 'number',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.contactPhoneNumber',
          placeholder: 'providerInfo.contactPhoneNumber',
        },
      },
    ],

    columnConfig: [
      {
        valueKey: 'name',
        displayText: 'providerInfo.name',
      },
      {
        valueKey: 'address',
        displayText: 'providerInfo.address',
      },
      {
        valueKey: 'customerId',
        displayText: 'providerInfo.customerId',
      },
      {
        displayText: 'viewDebt',
        action: 'getDebtAction',
        actionDataMapper: [
          {
            fromKey: 'customerId',
            toKey: 'number'
          }
        ],
        icon: 'show'
      }
    ]
  },
  getDebtAction: {
    apiActionName: 'getDebtUcomFixed',
    // url: '',
    requestFields: [
      {
        requestKey: 'number',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.id',
          placeholder: 'providerInfo.id',
        },
      },
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'client',
            // mapKey: 'value'  // default 'value'
            // printable: false, // default true
            // visible: true, // default true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'number',
            // printable: false
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.total',
            valueKey: 'balance.total'
          },
          {
            displayText: 'providerInfo.internet',
            valueKey: 'balance.internet'
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'balance.phone'
          },
          {
            displayText: 'providerInfo.tv',
            valueKey: 'balance.tv'
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'number',
      customerNameKey: 'client',
      debtKey: 'balance.total'
    }
  },
  payAction: {
    apiActionName: 'cashPayUcomFixed'
  },
};
