export const efuGames = {
  providerKey: 'EfuGames',
  logo: 'efugames.png',
  displayText: 'providers.EfuGames',
  getDebtAction: {
    apiActionName: 'getDebtEfuGames',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayEfuGames'
  },
  // billAction : {}
};
