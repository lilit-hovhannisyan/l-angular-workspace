export const ameriaContract = {
  providerKey: 'AmeriaContract',
  logo: 'ameriacontractpay.png',
  infoTitleText: 'providerInfo.ameriaContractPayInfoTitle',
  infoDescriptionText: 'providerInfo.ameriaContractPayInfoDescription',
  displayText: 'providers.AmeriaContract',
  getDebtAction: {
    apiActionName: 'getAmeriaContracts',
    requestFields: [
      {
        requestKey: 'creditCode',
        uiComponent: {
          component: 'input',
          label: 'creditCode',
          placeholder: 'providerInfo.creditCode'
        }
      },
      {
        uiComponent: {
          component: 'divider',
        }
      },
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      },
      {
        requestKey: 'passport',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.passport',
          placeholder: 'providerInfo.passport'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'contractDebt.name',
          },
          {
            displayText: 'providerInfo.code',
            valueKey: 'contractDebt.code',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.principalRem',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'contractDebt.principalRem',
            printable: false
          },
          {
            displayText: 'providerInfo.penAmounts',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.penAmounts)',
            printable: false
          },
          {
            displayText: 'providerInfo.overdueFee',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.overdueFee)',
            printable: false
          },
          {
            displayText: 'providerInfo.overdueInterest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: ' Math.floor(+contractDebt.overdueInterest)',
            printable: false
          },
          {
            displayText: 'providerInfo.overduePrincipal',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.overduePrincipal)',
            printable: false
          },
          {
            displayText: 'providerInfo.debtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.debtAmount)',
            printable: false
          },
          {
            displayText: 'providerInfo.nearRepayDate',
            valueKey: 'contractDebt.nearRepayDate',
            printable: false
          },
          {
            displayText: 'providerInfo.nearPrincipal',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'contractDebt.nearPrincipal',
            printable: false
          },
          {
            displayText: 'providerInfo.nearInterest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.nearInterest)',
            printable: false
          },
          {
            displayText: 'providerInfo.nearFee',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'Math.floor(+contractDebt.nearFee)',
            printable: false
          },
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'contractDebt.code',
      customerNameKey: 'contractDebt.name',
      debtKey: 'Math.floor(+contractDebt.debtAmount)'
    }
  },

  transactionRequestMapper: [
    {
      fromKey: 'contractDebt.code',
      toKey: 'code'
    },
    {
      fromKey: 'contractDebt.debtAmount',
      toKey: 'debt'
    }
  ],

  payAction: {
    apiActionName: 'cashPayAmeriaContract'
  },
  // billAction : {}
};
