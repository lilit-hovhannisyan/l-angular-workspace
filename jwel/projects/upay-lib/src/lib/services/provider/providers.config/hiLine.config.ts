export const hiLine = {
  providerKey: 'HiLine',
  logo: 'hiline.png',
  displayText: 'providers.HiLine',
  getDebtAction: {
    apiActionName: 'hiLineBill',
    requestFields: [
      {
        requestKey: 'phoneOrSubscriberNumber',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phoneOrCustomerId',
          placeholder: 'providerInfo.phoneOrCustomerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayHiLine'
  },
  // billAction : {}
};
