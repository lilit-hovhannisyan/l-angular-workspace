export const arpinet = {
  providerKey: 'Arpinet',
  logo: 'arpinet.png',
  displayText: 'providers.Arpinet',
  getDebtAction: {
    apiActionName: 'getDebtArpinet',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          },
          // {
          //   displayText: 'providerInfo.serviceId',
          //   valueKey: 'serviceId',
          // }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo.debtAmountRest',
            valueKey: 'debtAmountRest',
          }
        ]
      }
    ],
    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayArpinet'
  },
  // billAction : {}
};
