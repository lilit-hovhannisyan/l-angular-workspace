export const parkingFine = {
  providerKey: 'ParkingFine',
  logo: 'no-logo.png',
  displayText: 'providers.ParkingFine',
  getDebtAction: {
    apiActionName: 'getDebtParkingFine',
    requestFields: [
      {
        requestKey: 'pinCode',
        uiComponent: {
          component: 'input',
          label: 'providerInfo․pinCode',
          placeholder: 'providerInfo․pinCode'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo․serviceId',
            valueKey: 'serviceId',
          },
          {
            displayText: 'providerInfo․customerName',
            valueKey: 'customerName',
          },
          {
            displayText: 'providerInfo․actNumber',
            valueKey: 'actNum',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo․number',
            valueKey: 'number',
          },
          {
            displayText: 'providerInfo․amount',
            valueKey: 'amount',
          }
        ]
      }
    ]
  },
  payAction: {
    apiActionName: 'cashPayParkingFine'
  },
  // billAction : {}
};
