export const interactive = {
  providerKey: 'Interactive',
  logo: 'interactive.png',
  displayText: 'providers.Interactive',
  getDebtAction: {
    apiActionName: 'getDebtInteractive',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.deposit',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayInteractive'
  },
  // billAction : {}
};
