export const water = {
  providerKey: 'Water',
  logo: 'water.png',
  displayText: 'providers.Water',
  getDebtAction: {
    apiActionName: 'getDebtWater',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        }
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.counterUnit',
            // suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'counterUnit',
          },
          {
            displayText: 'providerInfo.costUnit',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'costUnit',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo.deposit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.paidByIdram',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'paidByIdram',
            printable: false
          },
          {
            displayText: 'providerInfo.debtAmountRest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debtAmountRest',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.prevPaidDate',
            valueKey: 'prevPaidDate',
          },
          {
            displayText: 'providerInfo.prevDebtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevDebtAmount',
          },
          {
            displayText: 'providerInfo.prevPaidAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevPaidAmount',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'debtAmountRest'
    }
  },
  payAction: {
    apiActionName: 'cashPayWater'
  },
  // billAction : {}
};
