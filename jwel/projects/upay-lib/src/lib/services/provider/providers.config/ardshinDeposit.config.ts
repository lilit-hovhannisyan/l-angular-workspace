export const ardshinDeposit = {
  providerKey: 'ArdshinDeposit',
  logo: 'ardshindeposit.png',
  displayText: 'providers.ArdshinDeposit',
  getDebtAction: {
    apiActionName: 'getArdshinDepositClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'surName.2.value + firstName.2.value',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          },
          {
            displayText: 'providerInfo.birthday',
            valueKey: 'birthday',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'surName.2.value + firstName.2.value'
    }
  },
  payAction: {
    apiActionName: 'cashPayArdshinDeposit'
  },
  // billAction : {}
};
