export const idBankCard = {
  providerKey: 'IdBankCard',
  logo: 'idbankcard.png',
  displayText: 'providers.IdBankCard',
  getDebtAction: {
    apiActionName: 'getIdBankCardClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.cardNumber',
          placeholder: 'providerInfo.cardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          },
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNumber',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'cardNumber',
      customerNameKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`'
    }
  },
  payAction: {
    apiActionName: 'cashPayIdBankCard'
  },
  // billAction : {}
};
