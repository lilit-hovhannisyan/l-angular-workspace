export const idBankAccount = {
  providerKey: 'IdBankAccount',
  logo: 'idbankaccount.png',
  displayText: 'providers.IdBankAccount',
  getDebtAction: {
    apiActionName: 'getIdBankAccountClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumber',
          placeholder: 'providerInfo.accountNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: '`${surnameAm}  ${forenameAm}  ${patronymicAm}`',
    }
  },
  payAction: {
    apiActionName: 'cashPayIdBankAccount'
  },
  // billAction : {}
};
