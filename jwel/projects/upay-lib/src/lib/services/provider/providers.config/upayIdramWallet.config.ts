export const upayIdramWallet = {
  providerKey: 'UpayIdramWallet',
  logo: 'upay.png',
  displayText: 'providers.UpayIdramWallet',
  getDebtAction: {
    apiActionName: 'getDebtUpayIdramWallet',
    requestFields: [
      {
        requestKey: 'accountId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtAmount',
            valueKey: 'debtAmount',
          },
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'customerId',
    }
  },
  payAction: {
    apiActionName: 'cashPayUpayIdramWallet'
  },
  // billAction : {}
};
