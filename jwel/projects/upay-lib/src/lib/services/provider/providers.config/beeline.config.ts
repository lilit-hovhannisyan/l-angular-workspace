export const beeline = {
  providerKey: 'Beeline',
  logo: 'beeline.png',
  displayText: 'providers.Beeline',
  getDebtAction: {
    apiActionName: 'beelineObtainBill',
    requestFields: [
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
            bold: true
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          }
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      debtKey: 'debt'
    }
  },
  payAction: {
    apiActionName: 'cashPayBeeline'
  },
  // billAction : {}
};
