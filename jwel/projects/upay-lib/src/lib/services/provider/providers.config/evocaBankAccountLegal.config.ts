export const evocaBankAccountLegal = {
  providerKey: 'EvocaLegalAccountPay',
  logo: 'evoca.png',
  displayText: 'providers.EvocaLegalAccountPay',
  getDebtAction: {
    apiActionName: 'getEvocaAccount',
    requestFields: [
      {
        requestKey: 'code',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.accountNumberOrCardNumber',
          placeholder: 'providerInfo.accountNumberOrCardNumber'
        }
      }
    ],

//     errorDescription: string;
// blockInfo: BlockInfo;


    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'name',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phoneInfo.phone',
          },
          {
            displayText: 'providerInfo.mobile',
            valueKey: 'phoneInfo.mobile',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'cur',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+rate)', // todo jshtel
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'accountNumber',
      customerNameKey: 'name',
    }
  },
  payAction: {
    apiActionName: 'cashPayEvocaLegalAccount'
  },
  // billAction : {}
};
