// gas idram
export const gasConfig =  {
  // company: {}, todo mtacel kam company id
  providerKey: 'Gas',
  logo: 'gas.png',
  displayText: 'providers.Gas',
  getDebtAction: {
    apiActionName: 'getDebtGas',
    // url: '',
    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGasBranches',
          // getOptionsUrl: '',
          // options: [
          //   {
          //     branchNameAm: 'a',
          //     branchId: '1'
          //   },
          //   {
          //     branchNameAm: 'b',
          //     branchId: '2'
          //   }
          // ],
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: 10
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true // default false
            // mapKey: 'value'  // default 'value'
            // printable: true, // default true
            // visible: true, // default true
          },
          {
            displayText: 'providerInfo.customerId',
            valueKey: 'customerId',
          },
          {
            displayText: 'providerInfo.phone',
            valueKey: 'phone',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debtDate',
            valueKey: 'debtDate',
          },
          {
            displayText: 'providerInfo.counterUnit',
            // suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'counterUnit',
          },
          {
            displayText: 'providerInfo.costUnit',
            suffixDisplayText: 'measureUnit.cbm',
            valueKey: 'costUnit',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          },
          {
            displayText: 'providerInfo.deposit',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'deposit',
          },
          {
            displayText: 'providerInfo.paidByIdram',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'paidByIdram',
            printable: false
          },
          {
            displayText: 'providerInfo.debtAmountRest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debtAmountRest',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.prevDate',
            // importantType: 'info', // none, info, error, warning /// default none
            valueKey: 'prevDate',
          },
          {
            displayText: 'providerInfo.prevDebtAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevDebtAmount',
          },
          {
            displayText: 'providerInfo.prevPaidAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'prevPaidAmount',
          }
        ]
      }
    ],

    /// pay info mapper ex. basket table
    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'debtAmountRest'
    }
  },
  payAction: {
    apiActionName: 'cashPayGas'
  },
  // billAction : {}
};
