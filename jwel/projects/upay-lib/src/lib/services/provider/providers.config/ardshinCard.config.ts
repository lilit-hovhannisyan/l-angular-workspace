export const ardshinCard = {
  providerKey: 'ArdshinCard',
  logo: 'ardshincard.png',
  displayText: 'providers.ArdshinCard',
  getDebtAction: {
    apiActionName: 'getArdshinCardClientData',
    requestFields: [
      {
        requestKey: 'productId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.cardNumber',
          placeholder: 'providerInfo.cardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'surnameAm + forenameAm',
          },
          {
            displayText: 'providerInfo.accountNumber',
            valueKey: 'accountNumber',
            printable: false
          },
          {
            displayText: 'providerInfo.cardNumber',
            valueKey: 'cardNumber',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.warningText',
            valueKey: 'warningText',
            printable: false
          },
          {
            displayText: 'providerInfo.currencyCode',
            valueKey: 'currencyCode',
            printable: false
          },
          {
            displayText: 'providerInfo.currRate',
            valueKey: 'Math.ceil(+currRate)',
            printable: false
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'cardNumber',
      customerNameKey: 'surnameAm + forenameAm'
    }
  },
  payAction: {
    apiActionName: 'cashPayArdshinCard'
  },
  // billAction : {}
};
