export const ekeng = {
  providerKey: 'Ekeng',
  logo: 'ekeng.png',
  displayText: 'providers.Ekeng',
  getDebtAction: {
    apiActionName: 'getDebtEkeng',
    requestFields: [
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.socialCardNumber',
          placeholder: 'providerInfo.socialCardNumber'
        }
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.debt',
            valueKey: 'debt',
          }
        ]
      },
    ]
  },
  payAction: {
    apiActionName: 'cashPayEkeng'
  },
  // billAction : {}
};
