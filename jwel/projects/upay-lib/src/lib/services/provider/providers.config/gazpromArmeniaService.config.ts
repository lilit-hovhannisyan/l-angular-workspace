export const gazpromArmeniaServiceConfig = {
  providerKey: 'GazpromArmeniaService',
  logo: 'gasservice.png',
  displayText: 'providers.GazpromArmeniaService',
  getDebtAction: {
    apiActionName: 'getDebtGazpromArmenia',
    hidden: true,
    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGazpromBranches',
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: ''
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.customerName',
            valueKey: 'customerName',
            bold: true
          },
          {
            displayText: 'providerInfo.customerCard',
            valueKey: 'customerCard',
          },
          {
            displayText: 'providerInfo.address',
            valueKey: 'address',
          },
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.prevDebt',
            prefixValueKey: 'prevDate',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subPrevDebtAmount',
          },
          {
            displayText: 'providerInfo.prevPaid',
            prefixValueKey: 'prevMonth',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subPrevPaidAmount',
          },
          {
            displayText: 'providerInfo.subExpenseAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subExpenseAmount',
          },
          {
            displayText: 'providerInfo.debtAmount',
            prefixValueKey: 'debtDate',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'subDebtAmount',
          },
          {
            displayText: 'providerInfo.remainder',
            valueKey: '+subDebtAmount - +subPaidAmount',
          },
        ]
      }
    ],

    transferMapper: {
      customerIdKey: 'customerId',
      customerNameKey: 'customerName',
      addressKey: 'address',
      debtKey: 'subDebtAmount'
    }
  },
  getContractList: {
    apiActionName: 'getDebtGazpromArmenia',

    requestFields: [
      {
        requestKey: 'branchId',
        uiComponent: {
          component: 'select',
          getOptionsApiActionName: 'getGazpromBranches',
          textField : 'branchNameAm',
          valueField : 'branchId',
          label: 'providerInfo.branch',
          placeholder: 'providerInfo.branch',
          defaultValue: ''
        },
      },
      {
        requestKey: 'customerId',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.customerId',
          placeholder: 'providerInfo.customerId',
        },
      },
      {
        requestKey: 'phone',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.phone',
          placeholder: 'providerInfo.phone',
        },
      }
    ],

    columnConfig: [
      {
        displayText: 'providerInfo.customerName',
        valueKey: 'customerName',
      },
      {
        displayText: 'providerInfo.customerId',
        valueKey: 'customerId',
      },
      {
        displayText: 'providerInfo.phone',
        valueKey: 'phone',
      },
      {
        displayText: 'providerInfo.address',
        valueKey: 'address',
      },
      {
        displayText: 'providerInfo.showMore',
        action: 'getDebtAction',
        actionDataMapper: [
          {
            fromKey: 'branchId',
            toKey: 'branchId'
          },
          {
            fromKey: 'phone',
            toKey: 'phone'
          },
          {
            fromKey: 'customerId',
            toKey: 'customerId'
          },
        ],
        icon: 'show'
      }
    ]
  },
  payAction: {
    apiActionName: 'cashPayGazpromArmeniaService'
  },
};
