export const fastCredit = {
  providerKey: 'FastCredit',
  logo: 'fastcredit.png',
  displayText: 'providers.FastCredit',
  getDebtAction: {
    apiActionName: 'getDebtFastCredit',

    requestFields: [
      {
        requestKey: 'agreementCode',
        uiComponent: {
          component: 'input',
          label: 'providerInfo.agreementCode',
          placeholder: 'providerInfo.agreementCode'
        }
      }
    ],

    debtTypeSelect: [
      {
        displayText:  'providerInfo.principalPayment',
        toDebtDataMapKey: 'paymentType',
        mapValue: 'deposit'
      },
      {
        displayText: 'providerInfo.currentPayment',
        toDebtDataMapKey: 'paymentType',
        mapValue: ''
      }
    ],

    responseMapper: [
      {
        groupKey: 'customer',
        fieldsMap: [
          {
            displayText: 'providerInfo.agreementCode',
            valueKey: 'agreementCode',
          },
          {
            displayText: 'providerInfo.borrower',
            valueKey: 'customerName',
          }
        ]
      },
      {
        groupKey: 'debt',
        fieldsMap: [
          {
            displayText: 'providerInfo.penaltyAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'penaltyAmount',
          },
          {
            displayText: 'providerInfo.overdueFee',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'overdueFee',
          },
          {
            displayText: 'providerInfo.overdueInterest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'overdueInterest',
          },
          {
            displayText: 'providerInfo.overduePrincipal',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'overduePrincipal',
          },
          {
            displayText: 'providerInfo.capitalAmount',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'capitalAmount',
          },
          {
            displayText: 'providerInfo.debt',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'debt',
          }
        ]
      },
      {
        groupKey: 'prevDebt',
        fieldsMap: [
          {
            displayText: 'providerInfo.nextPayment',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'nextPayment',
          },
          {
            displayText: 'providerInfo.nextPaymentDate',
            valueKey: 'nextPaymentDate',
          },
          {
            displayText: 'providerInfo.nearInterest',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'nearInterest',
          },
          {
            displayText: 'providerInfo.nearPrincipal',
            suffixDisplayText: 'measureUnit.amd',
            valueKey: 'nearPrincipal',
          }
        ]
      },
    ],

    transferMapper: {
      customerIdKey: 'agreementCode',
      customerNameKey: 'customerName',
      debtKey: 'debt'
    },
  },

  payAction: {
    apiActionName: 'cashPayFastCredit'
  },
  // billAction : {}
};
