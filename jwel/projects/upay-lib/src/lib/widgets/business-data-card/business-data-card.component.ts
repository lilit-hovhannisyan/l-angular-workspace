import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'upay-business-data-card',
  templateUrl: './business-data-card.component.html',
  styleUrls: ['./business-data-card.component.less']
})

export class BusinessDataCardComponent {
  @Input() results: BusinessDataCardResult[] = [];
  @Input() actions?: BusinessDataCardAction[]  = [];
  @Input() noEvent ? = false;

  @Input() hasGoBack ? = false;
  @Input() goBackText?: string;
  @Input() goBackPath?: string;
  @Input() goBackIcon?: string;
  @Input() infoGroups = ['default-group'];

  selectedResult: BusinessDataCardResult = null;

  @Output() selectResult = new EventEmitter<BusinessDataCardResult>();
  @Output() goBackClick = new EventEmitter<void>();
  @Output() ctaClick = new EventEmitter<{actionKey: string, selectedResult: BusinessDataCardResult}>();
  @Output() valueChange = new EventEmitter();

  constructor(private router: Router,
              private _location: Location) {}

  getResult(result) {
    this.selectedResult = result;
    this.selectResult.emit(this.selectedResult);
  }

  onValueChange(e, resultIndex, actionIndex, fieldIndex) {
    this.results[resultIndex].actions[actionIndex].fields[fieldIndex].value = e.target.value;
    this.valueChange.emit(e.target.value);
  }

  onCtaClick(actionKey) {
    if (!this.selectedResult) {
      this.selectedResult = this.results[0];
    }
    this.ctaClick.emit({
      actionKey: actionKey,
      selectedResult: this.selectedResult
    });
  }

  onGoBack(path) {
    if (!path) {
      this._location.back();
    } else {
      this.router.navigate([path]);
    }
  }

  objectToArray = (object) => {
    const array = [];

    for (const key in object) {
      if (key) {
        array.push(object[key]);
      }
    }

    return array;
  }
}

export interface BusinessDataCardResult {
  label?: BusinessDataCardLabel;
  status?: BusinessDataCardLabel;
  info: BusinessDataCardInfo;
  actions?: BusinessDataCardAction[];
}

export interface BusinessDataCardLabel {
  displayText: string;
  customClass?: string;
}

export class BusinessDataCardInfo {
  group: string;
  key?: string;
  displayText: string;
  value?: any;
  customClass?: string;

  constructor(data) {
    this.group = data.group  || 'default-group';
    this.displayText = data.displayText;
    this.value = data.value;
    this.customClass = data.customClass;
  }
}

export interface BusinessDataCardAction {
  key: string;
  displayText: string;
  fields?: BusinessDataCardActionFields[];
}

export class BusinessDataCardActionFields {
  type?: string;
  value: string;
  model: string;
  placeholder?: string;
}
