import {Injectable} from '@angular/core';
import {
  AccountantApi,
  AccountantGetOperatorsQuery
} from '../../api/index';


@Injectable({
  providedIn: 'root',
})
export class OperatorDataService {


  constructor(private accountantApi: AccountantApi) {
  }

  getOperatorsList() {

    const accountantGetOperatorsQuery = new AccountantGetOperatorsQuery();

    return this.accountantApi.getOperators(accountantGetOperatorsQuery);
  }
}
