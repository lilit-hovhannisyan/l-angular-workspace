import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OperatorDataService} from './operator-data.service';

@Component({
  selector: 'upay-operator-autocomplete',
  templateUrl: './operator-autocomplete.component.html',
  styleUrls: []
})
export class OperatorAutocompleteComponent implements OnInit {

  operatorsData = null;
  @Output() valueSelect = new EventEmitter();

  constructor(private operatorDataService: OperatorDataService) { }

  ngOnInit() {
    this.operatorDataService.getOperatorsList().subscribe(data => {
      this.operatorsData = data;
    });
  }

}
