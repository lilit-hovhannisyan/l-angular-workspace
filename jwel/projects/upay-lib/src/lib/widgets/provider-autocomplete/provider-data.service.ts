import {Injectable} from '@angular/core';
import {
  AccountantApi,
  AccountantGetProvidersQuery,
  AccountantGetServiceNamesQuery
} from '../../api/index';


@Injectable({
  providedIn: 'root',
})
export class ProviderDataService {

  constructor (private accountantApi: AccountantApi) {
  }

  // getProvidersList() {
  //   const accountantGetProvidersQuery = new AccountantGetProvidersQuery();
  //   return this.accountantApi.getProviders(accountantGetProvidersQuery);
  // }

  getServicesList() {
    const accountantGetServiceNamesQuery = new AccountantGetServiceNamesQuery();
    return this.accountantApi.getServiceNames(accountantGetServiceNamesQuery);
  }
}
