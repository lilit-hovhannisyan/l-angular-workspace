import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProviderDataService} from './provider-data.service';

@Component({
  selector: 'upay-provider-autocomplete',
  templateUrl: './provider-autocomplete.component.html',
  styleUrls: []
})
export class ProviderAutocompleteComponent implements OnInit {
  providerData = null;
  @Output() valueSelect = new EventEmitter();

  constructor(private providerDataService: ProviderDataService) { }

  ngOnInit() {
    this.providerDataService.getServicesList().subscribe(data => {
      this.providerData = data;
    });
  }

}
