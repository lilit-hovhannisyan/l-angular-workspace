import {Component, Input, EventEmitter, Output, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'upay-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.less']
})

export class AvatarComponent {
  @Input() currentUser;
  @Input() myProfileText = 'My profile'; // 'widgets.avatar.text.view_profile'
  @Input() linkToMyprofile = '/';
  @Input() logoutBtnText = 'Logout'; // 'widgets.avatar.button.logout'

  @Output() logOutAction = new EventEmitter();

  constructor(private router: Router) {}

  logout() {
    this.logOutAction.emit(this.currentUser);
    this.router.navigate(['/']);
  }
}
