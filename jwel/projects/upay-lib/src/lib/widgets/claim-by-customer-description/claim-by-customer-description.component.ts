import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ClaimStatusChangeAction, ClaimStatusMessage} from '../claim-by-operator-description/claim-by-operator-description.component';

interface Field {
  label: string;
  displayValueKey: string;
}
interface CustomerDataConfig {
  groupName: string;
  fields: Field[];
  actions: ClaimStatusChangeAction[];
  message: ClaimStatusMessage;
}
interface File {
  name: string;
  url: string;
}

@Component({
  selector: 'upay-claim-by-customer-description',
  templateUrl: './claim-by-customer-description.component.html',
  styleUrls: ['./claim-by-customer-description.component.less']
})

export class ClaimByCustomerDescriptionComponent {
  @Input() claimerTitle = 'Claimer';
  @Input() directionTitle = 'Direction';
  @Input() examinationNotesTitle = 'Examination Notes';
  @Input() attachmentsTitle = 'Attachments';
  @Input() newNotesTitle = 'New notes';
  @Input() allowNewNotes = true;
  @Input() newNotesPlaceholder = 'Notes';
  @Input() noteHistoryDate = 'Date';
  @Input() noteHistoryUsername = 'Username';
  @Input() noteHistoryStatus = 'Status';
  @Input() cancellationTypeTranslationPrefix = '';
  @Input() noteHistoryComment = 'Comment';

  @Input() claimData;
  @Input() customerDataConfig: CustomerDataConfig[];
  @Input() files: File[];

  @Input() fileStorageUrl;

  @Output() notesInput = new EventEmitter();
  @Output() actionClick = new EventEmitter();


  getFileLink(url) {
    return this.fileStorageUrl + url;
  }

  addNotes(event) {
    this.notesInput.emit(event.target.value);
  }

  onActionClick(action) {
    this.actionClick.emit(action);
  }
}
