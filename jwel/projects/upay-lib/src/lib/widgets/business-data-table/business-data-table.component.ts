import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnDestroy, OnInit, Output} from '@angular/core';

import {
  IBusinessDataTableColumnConfig,
  IBusinessDataTableQueryData,
  IBusinessDataTableFooterData
} from './business-data-table.model';

@Component({
  selector: 'upay-business-data-table',
  templateUrl: './business-data-table.component.html',
  styleUrls: []
})
export class BusinessDataTableComponent implements OnInit, OnDestroy {
  /// new
  @Input() activeFilters: string[];
  @Input() hasFilter = true;

  /// filter
  @Input() hasAmountIntervalFilter ? = true;
  @Input() hasDateIntervalFilter ? = true;
  @Input() hasResetButton ? = true;

  /// table
  @Input() reloadTime: number;
  @Input() pageNumber = 0;
  @Input() pageSize = 10;
  @Input() totalItems = 0;
  @Input() columnsConfig: IBusinessDataTableColumnConfig[];
  @Input() dataSource: any[];
  @Input() hasExportExcel = false;
  @Input() downloadExelText = 'Ներբեռնել';

  /// table footer
  @Input() footerDataSource: IBusinessDataTableFooterData[];
  @Input() footerLabelText: string;

  @Output() getDataSource = new EventEmitter();
  @Output() exportExcel = new EventEmitter();
  @Output() rowActionClick = new EventEmitter();
  // @Output() sort = new EventEmitter();

  // @Output() customEvent = new EventEmitter();

  filterData = {};
  @Input() orderedFieldsArr = [{key: 'created', val: false}];

  reloadTimer = null;

  //// new end
  constructor() { }

  ngOnInit() {
    if (!this.hasFilter) {
      this.onGetDataSource();
    }
    if (this.reloadTime) {
      this.reloadTimer = setInterval(() => this.onGetDataSource(), this.reloadTime);
    }
  }

  ngOnDestroy() {
    if (this.reloadTimer) {
      clearInterval(this.reloadTime);
    }
  }

  getQueryData() {
    const queryData =  {
      page: {
        index: this.pageNumber,
        size: this.pageSize
      },
      order: {
        fields: this.orderedFieldsArr
      },
      ...this.filterData
    };
    return queryData;
  }

  onGetDataSource() {
    this.getDataSource.emit(this.getQueryData());
  }

  pageChange(event) {
    this.pageNumber = event - 1;
    this.onGetDataSource();
  }

  sortData(orderedFieldsArr) {
    this.orderedFieldsArr = orderedFieldsArr;
    this.onGetDataSource();
  }

  onFilterData(event) {
    this.pageNumber = 0;
    this.filterData = event;
    this.onGetDataSource();
  }

  actionClick(event) {
    this.rowActionClick.emit(event);
  }

  onDownloadExcel() {
    this.exportExcel.emit(this.getQueryData());
  }
}
