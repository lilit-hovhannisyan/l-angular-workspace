export interface IBusinessDataTableColumnConfig {
  displayValueKey?: string;
  displayName?: () => string; // () => this.translateService.instant('widgets.reportDetails.text.cashbox'),
  type: 'data' | 'action';
  sortKey?: string;
  getDisplayValue?: (any) => string;
  dispatchActionName: string;
  actionIcon: 'icon_show' | 'icon_xls';
}

export interface IBusinessDataTableQueryData {
  page: {
    index: number;
    size: number;
  };
  order: {
    fields: {
      key: string;
      val: any;
    }[];
  };
  filter: any;
}

export interface IBusinessDataTableFooterData {
  label: string;
  value: string | number | boolean;
}
