import {Injectable} from '@angular/core';
import {
  AccountantApi,
  AccountantGetCashboxesQuery
} from '../../api/index';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class CashboxDataService {


  constructor(private accountantApi: AccountantApi) {
  }

  getCashboxList() {

    const accountantGetCashboxesQuery = new AccountantGetCashboxesQuery();

    return this.accountantApi.getCashboxes(accountantGetCashboxesQuery);
  }
}
