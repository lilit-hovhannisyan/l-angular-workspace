import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CashboxDataService} from './cashbox-data.service';

@Component({
  selector: 'upay-cashbox-autocomplete',
  templateUrl: './cashbox-autocomplete.component.html',
  styleUrls: []
})
export class CashboxAutocompleteComponent implements OnInit {

  cashboxData = null;
  @Output() valueSelect =  new EventEmitter();

  constructor(
    private cashboxDataService: CashboxDataService) { }

  ngOnInit() {
    this.cashboxDataService.getCashboxList().subscribe(data => {
      this.cashboxData = data;
    });
  }

}
