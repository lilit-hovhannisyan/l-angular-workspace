import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'upay-armenian-phone-picker',
  templateUrl: './armenian-phone-picker.component.html',
  styleUrls: ['./armenian-phone-picker.component.less']
})
export class ArmenianPhonePickerComponent implements OnInit {
  @Input() label?: string;
  @Input() countryCode = '374';
  @Input() providersCodeData = [
    {
      name: '033',
      value: '33'
    },
    {
      name: '041',
      value: '41'
    },
    {
      name: '043',
      value: '43'
    },
    {
      name: '044',
      value: '44'
    },
    {
      name: '047',
      value: '47'
    },
    {
      name: '055',
      value: '55'
    },
    {
      name: '077',
      value: '77'
    },
    {
      name: '091',
      value: '91'
    },
    {
      name: '093',
      value: '93'
    },
    {
      name: '094',
      value: '94'
    },
    {
      name: '095',
      value: '95'
    },
    {
      name: '096',
      value: '96'
    },
    {
      name: '097',
      value: '97'
    },
    {
      name: '098',
      value: '98'
    },
    {
      name: '099',
      value: '99'
    },

  ];

  @Output() valueChange = new EventEmitter();

  validationRegExp = null;

  selectedProviderCode = null;
  selectedNumber = null;
  errorMessage = '';

  constructor() { }

  ngOnInit() {
    this.createValidationRegExp();
  }

  createValidationRegExp() {
    let providersCodeStr = '(';
    this.providersCodeData.map((item, index) => {
      let suffix = ')';
      if (index < this.providersCodeData.length - 1) {
        suffix = '|';
      }
      providersCodeStr += `${item.value}${suffix}`;
    });


    const  regExpStr =  `^${this.countryCode}${providersCodeStr}[0-9]{6}$`; // '^\\+374(44|43)[0-9]{6}$';

    this.validationRegExp = new RegExp(regExpStr);
  }

  reset() {
    this.selectedProviderCode = null;
    this.selectedNumber = null;
  }

  isValid(phoneNumber) {
    return this.validationRegExp.test(phoneNumber);
  }

  onPhoneValueChange(providerCode, number) {
    const phoneNumber = `${this.countryCode}${providerCode}${number}`;
    if (this.isValid(phoneNumber)) {
      this.valueChange.emit(phoneNumber);
      this.errorMessage = '';
    } else {
      this.errorMessage = 'Հեռախոսահամարը վավեր չէ';
      this.valueChange.emit(null);
    }

    // if(isValid())  {}
  }

  onProviderCodeSelect(e) {
    this.selectedProviderCode = e;
    this.onPhoneValueChange(this.selectedProviderCode, this.selectedNumber);
  }

  onPhoneNumberChange(e) {
    this.selectedNumber = e;
    this.onPhoneValueChange(this.selectedProviderCode, this.selectedNumber);
  }

}
