import {Injectable, Output, EventEmitter} from '@angular/core';

@Injectable()
export class ModalService {
  @Output() openModalAction = new EventEmitter();
  @Output() closeModalAction = new EventEmitter();

  openModal() {
    this.openModalAction.emit('hey');
  }

  closeModal() {
    this.closeModalAction.emit('blah');
  }
}
