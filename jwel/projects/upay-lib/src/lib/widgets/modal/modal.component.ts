import {Component, OnChanges, OnInit} from '@angular/core';
import {ModalService} from './modal.service';

@Component({
  selector: 'upay-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less'],
})
export class ModalComponent implements OnInit, OnChanges {
  visible = true;
  modalContentConfig = [];
  modalActionsConfig = [];
  text = '';

  constructor(private modalService: ModalService) {}

  ngOnInit() {
    // this.modalService.openModal().subscribe( modal => {
    //   console.log('open modal' );
    //   debugger
    // });
  }

  ngOnChanges() {
    // this.modalService.closeModal().subscribe( modal => {
    //   console.log('close modal' );
    //   debugger
    // });
  }

  onAction(str) {

  }




}
