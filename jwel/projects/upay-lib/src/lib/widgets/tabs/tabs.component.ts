import { Component, Input} from '@angular/core';

@Component ({
  selector: 'upay-tabs',
  templateUrl: './tabs.component.html',
})

export class TabsComponent {
  @Input() tabPathPrefix = '';
  @Input() tabs: any[];
  @Input() translationprefix = '';
}

// interface Tab {
//   link: string;
//   text: string;
//   iconClass?: string;
//
// }
