import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AlertBoxService} from '../alert-box/alert-box.service';



@Component({
  selector: 'upay-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent {
  @Input() hasSignUp = false;
  @Input() logoUrl: string;
  username: string;
  password: string;

  @Input() signInText = 'Sign in';
  @Input() signUpText = 'Sign up';

  @Output() signInClick = new EventEmitter<any>();
  @Output() forgotClick = new EventEmitter<any>();
  @Output() signUpClick = new EventEmitter<any>();

  constructor(private alertBoxService: AlertBoxService) {}

  onUsernameChange(event) {
    this.username = event;
  }

  onPasswordChange(event) {
    this.password = event;
  }

  checkValidations() { // todo create normal validation mecanism
    if (!this.username) {
      this.alertBoxService.initMsg({
        type: 'error',
        text: 'username is required'
      });
      return false;
    }

    if (!this.password) {
      this.alertBoxService.initMsg({
        type: 'error',
        text: 'password is required'
      });
      return false;
    }
    return true;
  }

  onSignIn(event: Event) {
    // TODO commented code yet refers to only operator application, but
    // todo was not used code, so it will yet remain here
    // if(environment.versionCheck){
    //   this.versionCheckService.checkVersionAsync().subscribe((data:any) => {
    //     console.log(data.value);
    //     alert(data.value);
    //     if(!data.value) {
    //       this.userDataService.signIn(this.username, this.password);
    //       this.basketDataService.updateCounter();
    //     } else {
    //       alert("new version")
    //     }
    //   });
    // } else {
    //   this.userDataService.signIn(this.username, this.password);
    //   this.basketDataService.updateCounter();
    // }

    event.preventDefault(); // prevent page refresh
    if (this.checkValidations()) {
      this.signInClick.emit({username: this.username, password: this.password}); // todo poxel henc config-ov tarberaky patrast lini
    }
  }

  onForgotClick() {
    this.forgotClick.emit();
  }

  onSignUpClick() {
    this.signUpClick.emit();
  }

}
