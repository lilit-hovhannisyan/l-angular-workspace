import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlertBoxService} from '../alert-box/alert-box.service';
import {TranslateService} from '@ngx-translate/core';

export interface IReportsFilter {
  columnKey?: 'userId' | 'providerName' | 'paymentId' | 'userNum' | 'receiptId';
  columnValue?: string;
  amountFrom?: number;
  amountTo?: number;
  dateFrom?: number;
  dateTo?: number;
}

@Component({
  selector: 'upay-business-data-filter',
  templateUrl: './business-data-filter.component.html',
  styleUrls: ['./business-data-filter.component.less']
})
export class BusinessDataFilterComponent implements OnInit {
  /// new
  @Input() activeFilters?: string[];
  @Input() hasAmountIntervalFilter ? = true;
  @Input() hasDateIntervalFilter ? = true;
  @Input() hasResetButton ? = true;
  @Input() isSearchOnInit ? = true;
  @Output() search = new EventEmitter<any>();

  selectedSwitchingFilter = null;
  selectedSwitchingPlaceholder = null;
  switchingFilterValue = null;
  filterData: IReportsFilter = {};
  switchingFiltersMap = {
    cashboxId: {
      id: 'cashboxId',
      text: 'Դրամարկղ',
    },
    branchId: {
      id: 'branchId',
      text: 'Մասնաճյուղ',
    },
    providerName: {
      id: 'providerName',
      text: 'Ծառայություն',
    },
    operatorId: {
      id: 'operatorId',
      text: 'Գանձապահ',
    },
    receiptId: {
      id: 'receiptId',
      text: 'Անդորրագիր',
    },
    userNum: {
      id: 'userNum',
      text: 'Բաժանորդային համար',
    },


    supportUserNumber: {
      id: 'number',
      text: 'Support User Number',
    },

    customerId: {
      id: 'customerId',
      text: 'Յուզեր ID',
    },
    phone: {
      id: 'phone',
      text: 'Հեռախոսի',
    }
  };
  activeSwitchingFilters = [];

  selectedDateInterval = {
    from: null,
    to: null
  };

  constructor(private alertBoxService: AlertBoxService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.setActiveSwitchingFilters();
    this.setDefaultDate();
    if (this.isSearchOnInit) {
      this.onSearch();
    }

    // remove select if option is single
    if ( this.activeFilters.length === 1) {
      console.log('option', this.switchingFiltersMap[this.activeFilters[0]]);
      this.selectSwitchingFilter(this.switchingFiltersMap[this.activeFilters[0]]);
    }
  }

  setActiveSwitchingFilters() {
    const activeSwitchingFilters = [];
    if (this.activeFilters) {
      this.activeFilters.map(f => {
        const cF = this.switchingFiltersMap[f];
        if (cF) {
          activeSwitchingFilters.push(cF);
        }
      });
      this.activeSwitchingFilters = activeSwitchingFilters;
    }
  }

  selectSwitchingFilter(event) {
    this.selectedSwitchingFilter = event.id;
    this.selectedSwitchingPlaceholder = event.text;
    this.switchingFilterValue = null;
  }

  setSwitchingFilterValue(event) {
    this.switchingFilterValue = event;
  }

  setDefaultDate() {
    const todayMidNight = new Date();
    todayMidNight.setHours(0, 0, 0, 0);
    const nextMidNight = new Date(+todayMidNight + 24 * 60 * 60 * 1000);
    this.selectedDateInterval = {
      from: todayMidNight,
      to: nextMidNight
    };
  }

  onFilterDataChange(key, event) {
      if (key === 'dateInterval') {
        this.filterData.dateFrom = +event.from;
        this.filterData.dateTo = +event.to;
        return;
      }

      if (key === 'amountFrom' || key === 'amountTo') {
        event = event * 100; // convert to lumas
      }
      this.filterData[key] = event;
  }

  onDateIntervalValidationError(e) {
    this.alertBoxService.initMsg({type: 'error', text: 'Նախորդող ժամկետը չի կարող մեծ լինել հաջորդող ժամկետից'});
  }

  onSearch() {
    const queryData = {
      ...this.filterData
    };

    if (this.selectedSwitchingFilter && this.switchingFilterValue) {
      queryData[this.selectedSwitchingFilter] = this.switchingFilterValue;
    }

    this.search.emit(queryData);
  }

  onReset() {
    this.filterData = {};
    this.selectedSwitchingFilter = null;
    this.selectedSwitchingPlaceholder = null;
    this.switchingFilterValue = null;
    this.setDefaultDate();
    this.onSearch();
  }
}
