import {Injectable} from '@angular/core';
import {AccountantApi, AccountantGetBranchesQuery} from '../../api/index';

@Injectable({
  providedIn: 'root',
})
export class BranchDataService {
  constructor (private accountantApi: AccountantApi) {}

  getBranches() {
    const accountantGetBranchesQuery = new AccountantGetBranchesQuery();
    return this.accountantApi.getBranches(accountantGetBranchesQuery);
  }
}
