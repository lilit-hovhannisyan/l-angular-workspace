import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BranchDataService} from './branch-data.service';

@Component({
  selector: 'upay-branch-autocomplete',
  templateUrl: './branch-autocomplete.component.html',
  styleUrls: []
})
export class BranchAutocompleteComponent implements OnInit {

  branchData = null;
  @Output() valueSelect = new EventEmitter();

  constructor(private branchDataService: BranchDataService) { }

  ngOnInit() {
    this.branchDataService.getBranches().subscribe(data => {
      this.branchData = data;
    });
  }

}
