import {Injectable, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class DialogService {
  @Output() openDialogTrigger = new EventEmitter();

  userConfirm = null;
  userActionOnDialog = new BehaviorSubject(this.userConfirm);

  getDialogState() {
    return this.openDialogTrigger;
  }

  initDialog(str) {
    this.openDialogTrigger.emit({state: true, text: str});
  }

  onUserActionOnDialog(confirmOrCancel) {
    this.userConfirm = confirmOrCancel;
    this.userActionOnDialog.next(this.userConfirm);
  }

  rollBackDialog() {
    this.onUserActionOnDialog(null);
  }

  show(message, cb) {
    this.initDialog(message);
    // this.userActionOnDialog.unsubscribe();
    const sb = this.userActionOnDialog.subscribe(action => {
      if ( action ) {
        cb();
        sb.unsubscribe();
        this.rollBackDialog();
      }
    });
  }
}
