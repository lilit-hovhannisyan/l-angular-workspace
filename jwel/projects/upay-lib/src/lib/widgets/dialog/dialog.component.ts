import {Component, OnInit} from '@angular/core';
import {DialogService} from './dialog.service';

@Component({
  selector: 'upay-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.less'],
})
export class DialogComponent implements OnInit {
  vis = false;
  text = '';

  constructor(private dialogService: DialogService) {}

  ngOnInit() {
    this.dialogService.getDialogState().subscribe( dialog => {
      if ( dialog.state ) {
        this.vis = true;
        this.text = dialog.text;
      }
    });
  }

  onAction( action ) {
    let confirm;

    if ( action === 'confirm' ) {
      confirm = true;
      this.dialogService.onUserActionOnDialog(confirm);
    } else {
      confirm = false;
    }

    this.vis = false;
  }
}
