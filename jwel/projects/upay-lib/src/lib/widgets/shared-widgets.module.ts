import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';

import {AtomsModules, MoleculesModule, OrganismsModule} from 'iu-ui-lib';

import {LoadingComponent} from './loading/loading.component';
import {AlertBoxComponent} from './alert-box/alert-box.component';
import {DialogComponent} from './dialog/dialog.component';
import {ProviderDataService} from './provider-autocomplete/provider-data.service';
import {CashboxDataService} from './cashbox-autocomplete/cashbox-data.service';
import {OperatorDataService} from './operator-autocomplete/operator-data.service';
import {BranchDataService} from './branch-autocomplete/branch-data.service';
import {OperatorAutocompleteComponent} from './operator-autocomplete/operator-autocomplete.component';
import {CashboxAutocompleteComponent} from './cashbox-autocomplete/cashbox-autocomplete.component';
import {BranchAutocompleteComponent} from './branch-autocomplete/branch-autocomplete.component';
import {ProviderAutocompleteComponent} from './provider-autocomplete/provider-autocomplete.component';
import {BusinessDataTableComponent} from './business-data-table/business-data-table.component';
import {BusinessDataFilterComponent} from './business-data-filter/business-data-filter.component';
import {BusinessDataCardComponent} from './business-data-card/business-data-card.component';
import {TabsComponent} from './tabs/tabs.component';
import {ArmenianPhonePickerComponent} from './armenian-phone-picker/armenian-phone-picker.component';
import {AvatarComponent} from './avatar/avatar.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {ForgotPasswordFormComponent} from './forgot-password-form/forgot-password-form.component';
import {ModalComponent} from './modal/modal.component';
import {ClaimByOperatorDescriptionComponent} from './claim-by-operator-description/claim-by-operator-description.component';
import {ClaimByCustomerDescriptionComponent} from './claim-by-customer-description/claim-by-customer-description.component';


@NgModule({
  declarations: [
    LoadingComponent,
    AlertBoxComponent,
    DialogComponent,
    ModalComponent,
    OperatorAutocompleteComponent,
    CashboxAutocompleteComponent,
    BranchAutocompleteComponent,
    ProviderAutocompleteComponent,
    BusinessDataTableComponent,
    BusinessDataFilterComponent,
    BusinessDataCardComponent,
    TabsComponent,
    ArmenianPhonePickerComponent,
    AvatarComponent,
    LoginFormComponent,
    ForgotPasswordFormComponent,
    ClaimByOperatorDescriptionComponent,
    ClaimByCustomerDescriptionComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    OrganismsModule
  ],
  providers: [
    ProviderDataService,
    CashboxDataService,
    OperatorDataService,
    BranchDataService,
  ],
  exports: [
    LoadingComponent,
    AlertBoxComponent,
    DialogComponent,
    ModalComponent,
    OperatorAutocompleteComponent,
    CashboxAutocompleteComponent,
    BranchAutocompleteComponent,
    ProviderAutocompleteComponent,
    BusinessDataTableComponent,
    BusinessDataFilterComponent,
    BusinessDataCardComponent,
    TabsComponent,
    ArmenianPhonePickerComponent,
    AvatarComponent,
    LoginFormComponent,
    ForgotPasswordFormComponent,
    ClaimByOperatorDescriptionComponent,
    ClaimByCustomerDescriptionComponent
  ]
})
export class SharedWidgetsModule { }
