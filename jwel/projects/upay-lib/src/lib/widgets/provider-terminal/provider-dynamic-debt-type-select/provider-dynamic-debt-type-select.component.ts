import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {ProviderService} from '../../../services/provider/providers.service';
import {ProviderDebtDataService} from '../../../services/provider/provider-debt-data.service';

@Component({
  selector: 'upay-provider-dynamic-debt-type-select',
  templateUrl: './provider-dynamic-debt-type-select.component.html',
  styleUrls: []
})

export class ProviderDynamicDebtTypeSelectComponent implements OnInit {
  @Input() multiResultSearchedDebtData: any;
  @Input() providerConfig: any;
  @Output() selectDynamicDebtType = new EventEmitter();

  dynamicMultiDebtData = null;

  constructor(private providerService: ProviderService,
              private providerDebtDataService: ProviderDebtDataService) {}

  ngOnInit() {
    const dynamicMultiDebtData = [];
    this.providerConfig.config.getDebtAction.responseMappersList.map(rmConfig => {
      if (rmConfig.repeatKey) {

        this.multiResultSearchedDebtData[rmConfig.repeatKey].map(repeatItem => {

          const dynamicDebtDta = {};
          rmConfig.responseMapper.map(groupMapper => {
            groupMapper.fieldsMap.map(fieldMap => {

              if (fieldMap.from === 'root') {

                dynamicDebtDta[fieldMap.dynamicMapKey] = this.providerDebtDataService.getDebtFieldValue(this.multiResultSearchedDebtData, fieldMap.valueKey);
              } else {
                dynamicDebtDta[fieldMap.dynamicMapKey] = this.providerDebtDataService.getDebtFieldValue(repeatItem, fieldMap.valueKey);
              }
            });
          });

          const dynamicDebtType =   this.providerDebtDataService.getDebtFieldValue(repeatItem, rmConfig.dynamicTypeKey);
          dynamicDebtDta['_dynamicDebtType'] = dynamicDebtType;

          const dynamicDebtConfig = {
            displayText:   this.providerDebtDataService.getDebtFieldValue(repeatItem, rmConfig.displayTextKey),
            // dynamicDebtType: dynamicDebtType,
            responseMapper: rmConfig.responseMapper,
            debtDta: dynamicDebtDta
          };

          dynamicMultiDebtData.push(dynamicDebtConfig);
        });
      } else {

        const staticDebtData = {
          ...this.multiResultSearchedDebtData,
          _dynamicDebtType: null
        };

        const staticDebtConfig = {
          displayText: rmConfig.displayText,
          // dynamicDebtType: null,
          responseMapper: rmConfig.responseMapper,
          debtDta: this.multiResultSearchedDebtData
        };

        dynamicMultiDebtData.push(staticDebtConfig);
      }

    });
    this.dynamicMultiDebtData = dynamicMultiDebtData;
  }

  onSelectDynamicDebtType(e) {
    const emmitData = {
      multiResultSearchedDebtData: this.multiResultSearchedDebtData,
      ...e
     // _dynamicDebtTypeValue: e.dynamicDebtType
    };
    this.selectDynamicDebtType.emit(emmitData);
  }

}


