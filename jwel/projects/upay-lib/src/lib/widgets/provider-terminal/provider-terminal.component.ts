import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'upay-provider-terminal',
  templateUrl: './provider-terminal.component.html',
  styleUrls: ['./provider-terminal.component.less']
})
export class ProviderTerminalComponent implements OnInit {
  @Input() labelId: string;
  @Input() providerId: string;

  constructor() { }

  ngOnInit() {}


}
