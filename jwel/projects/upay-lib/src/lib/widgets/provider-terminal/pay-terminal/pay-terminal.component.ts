import {Component, OnDestroy, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {of, Subscription} from 'rxjs/index';
import {ProviderService} from '../../../services/provider/providers.service';
import {OfficeApi} from '../../../api/index';
import {LoadingService} from '../../loading/loading.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';

// todo texapoxel
const isEmptyObj = (obj) => {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
};

@Component({
  selector: 'upay-pay-terminal',
  templateUrl: './pay-terminal.component.html',
  styleUrls: ['./pay-terminal.component.less']
})
export class PayTerminalComponent implements OnInit, OnChanges, OnDestroy {

  @Input() branchId: string;
  @Input() labelId: string;
  @Input() providerId: string;

  @Output() addToBasketClick = new EventEmitter();
  @Output() addToFavoriteClick = new EventEmitter();
  @Output() payClick = new EventEmitter();

  debtSearchData = {};
  contractsSearchData = {};
  debtDataMapper = null;
  searchedDebtData = null;
  multiResultSearchedDebtData = null;
  selectableDebtData = null;
  searchedContractsData = null;

  selectedProvider = null;
  providerCommissionsRoles: any;
  @Input() hasBasket = true;
  // providerSelectedService = null;
  isMultiServiceProvider = false;

  // activeRouteSub = Subscription.EMPTY;

  constructor(private apiInst: OfficeApi,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private providerService: ProviderService,
              private loadingService: LoadingService,
              private location: Location) {
  }

  ngOnInit() {
    this.setDataForProvider();
  }

  ngOnChanges() {
    this.setDataForProvider();
  }

  ngOnDestroy() {
    // this.activeRouteSub.unsubscribe();
  }

  setDataForProvider() {
    this.debtSearchData = {};
    this.contractsSearchData = {};
    this.providerService.getConfiguredProvidersByProviderKey(this.providerId).pipe(
      switchMap(provider => {
        return this.providerService.getProviderCommissionsByProviderId(provider.id)
          .pipe(map(providerCommissionsRoles => {
            return {
              provider,
              providerCommissionsRoles
            };
          }));
      }),
    ).subscribe(data => {
      this.selectedProvider = data.provider;
      if (data.providerCommissionsRoles && data.providerCommissionsRoles.ranges) {
        this.providerCommissionsRoles = data.providerCommissionsRoles.ranges;
      } else {
        this.providerCommissionsRoles = [];
      }
      this.debtSearchData = this.activatedRoute.snapshot.queryParams;
      if (!isEmptyObj(this.debtSearchData)) {
        this.onSearchDeptData();
      }
    });
  }

  setSelectedProvider(provider) {
    this.selectedProvider = provider;
  }

  getApiMethodByName(apiMethodName) {
    return (data?: any) => {
      if (!data) {
        data = {};
      }
      return this.apiInst[apiMethodName](data);
    };
  }

  onSearchFieldValueChange(searchData, event, fieldConfig) {
    const _searchData = {...searchData};
    if (fieldConfig.uiComponent.component === 'select') {
      _searchData[fieldConfig.requestKey] = event[fieldConfig.uiComponent.valueField];
    } else {
      _searchData[fieldConfig.requestKey] = event;
    }
    return _searchData;
  }

  setRoutQueryParams(searchData) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {...searchData},
      });
  }

  onSearchDebtFieldValueChange(event, fieldConfig) {
    this.searchedDebtData = null;
    this.multiResultSearchedDebtData = null;
    this.debtSearchData = this.onSearchFieldValueChange(this.debtSearchData, event, fieldConfig);
    this.setRoutQueryParams(this.debtSearchData);

    // this.onSearchFieldValueChange(this.debtSearchData, event,  fieldConfig);
  }

  onSearchContractsFieldValueChange(event, fieldConfig) {
    this.contractsSearchData = this.onSearchFieldValueChange(this.contractsSearchData, event, fieldConfig);
  }

  // todo change
  onEnterClick(event, action) {
    if (event.keyCode === 13) {
      action === 'getDebtAction' ? this.onSearchDeptData() : this.onSearchContractsData();
    }
  }

  debtDataPreprocessor(debtData) {

    if (this.selectedProvider.config.getDebtAction.responseMappersList) {
      this.multiResultSearchedDebtData = debtData;
    } else {
      if (this.selectedProvider.config.getDebtAction.debtTypeSelect) {
        this.selectableDebtData = debtData;
      } else {
        this.searchedDebtData = debtData;
      }

      this.debtDataMapper = this.selectedProvider.config.getDebtAction.responseMapper;
    }
  }

  onSearchDeptData() {

    this.searchedContractsData = null;
    this.loadingService.showLoader(true);

    this.providerService.providerSearchDebt(this.selectedProvider, this.debtSearchData).subscribe(debtData => {
      this.debtDataPreprocessor(debtData);
      this.loadingService.showLoader(false);
    });
  }

  onSearchContractsData() {
    this.searchedDebtData = null;
    this.debtDataMapper = null;
    this.multiResultSearchedDebtData = null;
    this.selectableDebtData = null;
    this.loadingService.showLoader(true);

    const getContractsListActionName = this.selectedProvider.config.getContractList.apiActionName;
    this.getApiMethodByName(getContractsListActionName)(this.contractsSearchData).subscribe(contractsList => {
      this.searchedContractsData = contractsList;
      this.loadingService.showLoader(false);
    });
  }

  onContractAction(event) {
    // getDebtAction
    if (event.actionName === 'getDebtAction') {
      const debtSearchData = {};
      event.column.actionDataMapper.map(data => {
        debtSearchData[data.toKey] = event.row[data.fromKey];
      });
      this.debtSearchData = debtSearchData;
      this.onSearchDeptData();
    }
  }

  onGoBack() {
    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
  }

  onSelectDynamicDebtType(dynamicDebtType) {

    this.searchedDebtData = dynamicDebtType.debtDta;
    this.debtDataMapper = dynamicDebtType.responseMapper;
    // this.multiResultSearchedDebtData = null;
  }

  onSelectStaticDebtType(debtDta) {
    this.searchedDebtData = debtDta;
  }

  onAddToBasketClick(event) {
    this.addToBasketClick.emit(event);
    this.onGoBack();
  }

  onAddToFavoriteClick() {
    console.log(this.selectedProvider);
    const data = {
      provider: this.selectedProvider,
      fields: this.debtSearchData
    };
    this.addToFavoriteClick.emit(data);
  }

  onPayClick(event) {
    this.payClick.emit(event);
  }
}
