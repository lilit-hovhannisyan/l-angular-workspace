import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProviderService} from '../../../services/provider/providers.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'upay-contracts-table',
  templateUrl: './contracts-table.component.html'
})

export class ContractsTableComponent implements OnInit {
  @Input() providerConfig: any;
  @Input() contractsData = null;

  @Output() actionClick = new EventEmitter();

  columnsConfig = null;

  constructor(private providerDataService: ProviderService,
              private translateService: TranslateService) {

  }

  ngOnInit() {
    this.createColumnsConfig();
  }

  createColumnsConfig() {
    this.columnsConfig = this.providerConfig.config.getContractList.columnConfig.map(c => {
      const mapObject = {
        displayValueKey: c.valueKey,
        displayName: () => this.translateService.instant(c.displayText),
        type: c.action ? 'action' : 'data',
        dispatchActionName: c.action,
        actionIcon: c.icon ? `icon_${c.icon}` : '',
        actionDataMapper: c.action ? c.actionDataMapper : null
      };
      return mapObject;
    });
  }

  onActionClick(event) {
    this.actionClick.emit(event);
  }
}
