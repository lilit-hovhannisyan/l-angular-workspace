import { Routes } from '@angular/router';

import {ProviderTerminalComponent} from './provider-terminal.component';
import {ProviderListComponent} from './provider-list/provider-list.component';
import {PayTerminalComponent} from './pay-terminal/pay-terminal.component';

export const providerTerminalRoutes: Routes = [
  {
    path: '',
    component: ProviderTerminalComponent,
    children: [
      {
        path: ':labelId',
        component: ProviderListComponent
      },
      {
        path: ':labelId/:providerId',
        component: PayTerminalComponent
      }
    ]
  }
];
