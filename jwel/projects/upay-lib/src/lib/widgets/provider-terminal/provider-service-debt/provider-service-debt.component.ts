import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {ProviderService} from '../../../services/provider/providers.service';

export interface IPayDta {
  amount: number;
  payment: any; // IPaymentCardData;
  customerDebitPayData: ICustomerDebitPayData;
}

export interface ICustomerDebitPayData {
  icon: string;
  title: string;
  isFavorite?: boolean;
  isRecurrent?: boolean;
  customerDetails: any[]; // ICustomerDetail[];
  debtDetails: any[]; // IDebtDetail[];
  prevDebtDetails?: any[]; // IDebtDetail[];
  commissionPercent: number;
  updated?: string;
}

@Component({
  selector: 'upay-provider-service-debt',
  templateUrl: './provider-service-debt.component.html',
})

export class ProviderServiceDebtComponent implements OnInit {
  @Input() providerConfig: any;
  // @Input() providerServiceConfig: any;
  @Input() providerCommissionsRoles: any[];
  @Input() serviceDebtData: any;
  @Input() debtDataMapper: any;
  @Input() customerDebtSearchQuery: any;
  @Input() hasBasket = true;

  @Output() addToBasketClick = new EventEmitter();
  @Output() addToFavoriteClick = new EventEmitter();
  @Output() payClick = new EventEmitter();

  amountValidationError = null;
  amountValue = null;
  imputedPayNode: string;
  commissionValue = 0;

  constructor(private providerService: ProviderService) {}

  ngOnInit() {
    this.getServiceDebtInfo();
  }

  getServiceDebtInfo() {
    this.validateAmount();
    // console.log( this.serviceDebtData, this.providerServiceConfig);
  }
  // getInvoiceFieldValue
  getGroupFieldValue(groupField) {
    return this.providerService.getInvoiceFieldValue(groupField, this.serviceDebtData);
  }

  getGroupFieldPrefixValue(groupField) {
    return this.providerService.getInvoiceFieldPrefixValue(groupField, this.serviceDebtData);
  }

  getErrorsServiceDebtData() {
    const errorData = [];
    const { responseErrorMapper } = this.providerConfig.config.getDebtAction;
    if (responseErrorMapper) {
      responseErrorMapper.map(errorConfig => {
        const errorValue = this.providerService.getInvoiceFieldValue(errorConfig, this.serviceDebtData);
        // this.serviceDebtData[errorConfig.valueKey];
        if (errorValue) {
          errorData.push({
            displayText: errorConfig.displayText,
            errorValue: errorValue
          });
        }
      });
    }
    return errorData;
  }

  isNotEmptyGroupFieldValue(groupField) {
    return this.providerService.isNotEmptyGroupFieldValue(groupField, this.serviceDebtData);
  }

  onAmountChange(event) {
    this.amountValue = event.target.value || null;
    this.validateAmount();
  }

  onImputedPayNode(event) {
    this.imputedPayNode = event.target.value;
  }

  calculateCommission() {
    this.commissionValue = this.providerService.calculateProviderCommission(this.amountValue, this.providerCommissionsRoles);
  }

  validateAmount() {
    const validatedObject = this.providerService.validateAmount(this.providerConfig, this.amountValue, this.serviceDebtData);
    this.amountValidationError = validatedObject.error;
    if (!this.amountValidationError) {
      this.calculateCommission();
    }
  }

  getDataForEmmit() {

    const emitData = {
      customerDebtData: this.providerService.getTransactionRequestData(this.providerConfig, this.customerDebtSearchQuery, this.serviceDebtData),
      amount: this.amountValue || 0,
      imputedPayNode: this.imputedPayNode,
      providerKey: this.providerConfig.config.providerKey,
      //// main part end

      providerData: this.providerConfig,
      commission: this.commissionValue,
      providerCommissionsRoles: this.providerCommissionsRoles,
      transferMappedData:  this.providerService.getTransferMappedData(this.providerConfig,  this.serviceDebtData)
    };

    return emitData;
  }

  onAddToBasketClick() {
    this.addToBasketClick.emit(this.getDataForEmmit());
    // console.log(event)
  }

  onAddToFavoriteClick() {
    this.addToFavoriteClick.emit(this.getDataForEmmit());
  }

  onPayClick() {
    this.payClick.emit(this.getDataForEmmit());
  }

  // onFocus(event) {
  //   this.focus.emit(event);
  // }

  // onPaymentCardSelect(paymentCard: any) {
  //
  // }

  // onAddNewCardClick() {
  //
  // }
  //
  // onRecurrentClick() {
  //   this.recurrentClick.emit(this.customerDebitPayData);
  // }
  //
  //
  // onGroupClick() {
  //
  // }
}
