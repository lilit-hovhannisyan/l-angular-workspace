import {Component, Input, OnDestroy, OnInit} from '@angular/core';

import {Subscription} from 'rxjs/index';

import {ActivatedRoute, Router} from '@angular/router';
// import {ProviderCategoryData} from '';
import {ProviderService} from '../../../services/provider/providers.service';

@Component({
  selector: 'upay-terminal-category-navigation',
  templateUrl: './terminal-category-navigation.component.html',
  styleUrls: ['./terminal-category-navigation.component.less']
})
export class TerminalCategoryNavigationComponent implements OnInit, OnDestroy {
  @Input () routPath: string;

  // serviceCategories: ProviderCategoryData[] = [];
  providerLabels: any[]; // todo create data model
  serviceCategoriesSub = Subscription.EMPTY;
  activeRouteSub = Subscription.EMPTY;

  constructor(private providerDataService: ProviderService,
              public router: Router,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {

    this.providerDataService.getProviderLabels().subscribe(labels => {
      this.providerLabels = labels;
    });
  }

  ngOnDestroy() {
    this.serviceCategoriesSub.unsubscribe();
    this.activeRouteSub.unsubscribe();
  }

}
