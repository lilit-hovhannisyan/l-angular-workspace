import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'upay-provider-static-debt-type-select',
  templateUrl: './provider-static-debt-type-select.component.html'
})

export class ProviderStaticDebtTypeSelectComponent {
  @Input() staticDebtOptions: any[];
  @Input() selectableDebtData: any;

  @Output() selectStaticDebtType = new EventEmitter();

  onSelectStaticDebtType(e) {
    const emitData = {
      ...this.selectableDebtData,
    };
    if (e.toDebtDataMapKey) {
      emitData[e.toDebtDataMapKey] = e.mapValue;
    }
    this.selectStaticDebtType.emit(emitData);
  }

}
