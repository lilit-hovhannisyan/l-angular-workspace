import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {ProviderService} from '../../../services/provider/providers.service';

@Component({
  selector: 'upay-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.less']
})
export class ProviderListComponent implements OnInit, OnChanges, OnDestroy {

  @Input() labelId: string;

  providers: any[] = [];
  activeRouteSub = Subscription.EMPTY;
  currentCategorySub = Subscription.EMPTY;

  constructor(private providerDataService: ProviderService) {}

  ngOnInit() {
    this.setDataForProviders();
  }

  ngOnChanges() {
    this.setDataForProviders();
  }

  ngOnDestroy() {
    this.activeRouteSub.unsubscribe();
    this.currentCategorySub.unsubscribe();
  }

  setDataForProviders() {
    this.providerDataService.getProvidersByLabelId(this.labelId).subscribe(providers => {
      console.log('providers', providers);
      this.providers = providers;
    });
  }

  getRoutLink(providerId) { // todo
    return '/client/transactions/pay/' + this.labelId + '/' + providerId;
  }
}
