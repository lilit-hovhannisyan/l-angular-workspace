import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {ProviderService} from '../../../services/provider/providers.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'upay-provider-invoice-view',
  templateUrl: './provider-invoice-view.component.html',
  styleUrls: ['./provider-invoice-view.component.less']
})
export class ProviderInvoiceViewComponent implements  OnInit {
  @Input() debtData: any;
  @Input() payAmount: number;
  @Input() providerData: any;
  @Input() commissionValue: number;
  @Input() receiptData: {receiptId: string, purpose: string};
  @Input() branchInfo: {phone: string, address: string};
  @Input() operatorInfo: {fullName: string};
  @Input() companyData: {companyName: string, companyTin: string};

  providerInvoiceData: any;

  providerInvoiceHtml: any;

  constructor(private providerService: ProviderService,
              public sanitized: DomSanitizer) { }

  ngOnInit() {
    this.providerInvoiceHtml = this.providerService.getProviderInvoiceHtml({
      debtData: this.debtData,
      amount: this.payAmount,
      provider: this.providerData,
      commissionValue: this.commissionValue,
      receiptData: this.receiptData,
      companyData: this.companyData,
      branchInfo: this.branchInfo,
      operatorInfo: this.operatorInfo
    });
  }

  // this.providerInvoiceHtml = this.sanitized.bypassSecurityTrustHtml(providerInvoiceHtml);
}
