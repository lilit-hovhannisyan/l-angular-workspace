import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AtomsModules, MoleculesModule } from 'iu-ui-lib';

import { ProviderService } from '../../services/provider/providers.service';
import { TerminalCategoryNavigationComponent } from './terminal-category-navigation/terminal-category-navigation.component';
import { providerTerminalRoutes } from './provider-terminal-routing';
import { ProviderTerminalComponent } from './provider-terminal.component';
import { ProviderListComponent } from './provider-list/provider-list.component';
import { PayTerminalComponent } from './pay-terminal/pay-terminal.component';
import { ProviderServiceDebtComponent } from './provider-service-debt/provider-service-debt.component';
import { CustomerApi, OfficeApi } from '../../api/index';
import { ContractsTableComponent } from './contracts-table/contracts-table.component';
import { ProviderInvoiceViewComponent } from './provider-invoice-view/provider-invoice-view.component';
import {ProviderDynamicDebtTypeSelectComponent} from './provider-dynamic-debt-type-select/provider-dynamic-debt-type-select.component';
import {ProviderStaticDebtTypeSelectComponent} from './provider-static-debt-type-select/provider-static-debt-type-select.component';


@NgModule({
  declarations: [
    ProviderTerminalComponent,
    TerminalCategoryNavigationComponent,
    ProviderListComponent,
    PayTerminalComponent,
    ProviderServiceDebtComponent,
    ContractsTableComponent,
    ProviderInvoiceViewComponent,
    ProviderDynamicDebtTypeSelectComponent,
    ProviderStaticDebtTypeSelectComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    AtomsModules,
    MoleculesModule,
    // RouterModule.forChild(providerTerminalRoutes),
  ],
  providers: [
    CustomerApi,
    OfficeApi,
    ProviderService,
  ],
  exports: [
    ProviderTerminalComponent,
    TerminalCategoryNavigationComponent,
    ProviderListComponent,
    PayTerminalComponent,
    ProviderInvoiceViewComponent,
    ProviderDynamicDebtTypeSelectComponent,
    ProviderStaticDebtTypeSelectComponent
  ]
})

export class ProviderTerminalModule {

}
