import {Component, EventEmitter, Input, Output} from '@angular/core';
import {dateFormat} from '../../misc-services/date-time.service';

export class Notes {
  timestamp: string;
  username: string;
  status: string;
  comment: string;

  constructor(data) {
    this.timestamp = dateFormat(new Date(data.timestamp), 'dd.mm.yyyy HH:MM');
    this.username = data.username;
    this.status = data.status;
    this.comment = data.comment;
  }
}

export interface ClaimStatusChangeAction {
  key: string;
  name: string;
  customClass?: string;
}

export interface ClaimStatusMessage {
  text: string;
  customClass?: string;
}

export interface ClaimDescriptionData {
  reason: string;
  examinationNotes: string;
  notes: Notes[];
  actions: ClaimStatusChangeAction[];
  message: ClaimStatusMessage;
}

@Component({
  selector: 'upay-claim-by-operator-description',
  templateUrl: './claim-by-operator-description.component.html',
  styleUrls: ['./claim-by-operator-description.component.less']
})
export class ClaimByOperatorDescriptionComponent {
 @Input() reasonTitle: string;
 @Input() documentsTitle: string;
 @Input() examinationNotesTitle: string;
 @Input() newNotesTitle: string;
 @Input() allowNewNotes = true;
 @Input() claimDescriptionData: ClaimDescriptionData = null;
 @Input() newNotesPlaceholder = 'Notes';
 @Input() noteHistoryDate = 'Date';
 @Input() noteHistoryUsername = 'Username';
 @Input() noteHistoryStatus = 'Status';
 @Input() cancellationTypeTranslationPrefix = '';
 @Input() noteHistoryComment = 'Comment';

 @Output() notesInput = new EventEmitter();
 @Output() actionClick = new EventEmitter();


  addNotes(event) {
    this.notesInput.emit(event.target.value);
  }

  onActionClick(action) {
    this.actionClick.emit(action);
  }
}
