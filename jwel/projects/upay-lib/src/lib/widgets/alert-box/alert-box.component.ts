import { Component, OnInit, Input } from '@angular/core';
import {AlertBoxService} from './alert-box.service';
import {SharedSiteDataService} from '../../model-related-services/shared-site-data.service';

@Component({
  selector: 'upay-alert-box',
  templateUrl: './alert-box.component.html',
  styleUrls: []
})
export class AlertBoxComponent implements OnInit {
  @Input() visible: string;
  @Input() text: string;
  @Input() type: string = 'error' || 'warning' || 'success';
  // private sharedSiteDataService: SharedSiteDataService,
  constructor(
              private alertBoxService: AlertBoxService) {}

  ngOnInit() {
    this.alertBoxService.showMsg.subscribe( error => {
      if ( error.type ) {
        this.visible = 'visible';
        this.text = error.text;
        this.type = error.type;


        setTimeout( () => {
          this.visible = '';
        }, 7000);
      }
    });
  }

  onCloseBox() {
    this.visible = '';
    // this.sharedSiteDataService.removeGlobalMessage();
  }
}
