import { Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';
import {ErrorHandler} from '../../api/error-handler';

@Injectable({
  providedIn: 'root',
})
export class AlertBoxService {
  public showMsg = new BehaviorSubject({type: null, text: ''});

  constructor(private errorHandler: ErrorHandler) {
    this.errorHandler.errorData.subscribe(data => {
      if (data) {
        this.initMsg({type: 'error', text: data.message} );
      }
    });

  }

  initMsg ( error ) {
    this.showMsg.next(error);
  }
}
