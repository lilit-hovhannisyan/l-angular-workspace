import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'upay-forgot-password-form',
  templateUrl: './forgot-password-form.component.html'
})
export class ForgotPasswordFormComponent {
  @Input() logoUrl = null;
  username: string;
  @Output() openLogin = new EventEmitter<any>();
  @Output() submitForgotPasswordForm = new EventEmitter<any>();

  constructor() {}

  onUsernameChange(event) {
    this.username = event;
  }

  onSubmit() {
   this.submitForgotPasswordForm.emit(); // todo
  }

  onOpenLogin() {
    this.openLogin.emit(); // todo
  }
}
