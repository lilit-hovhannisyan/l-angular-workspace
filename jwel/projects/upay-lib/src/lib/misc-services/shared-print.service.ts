import {Injectable} from '@angular/core';
import {DateTimeService} from './date-time.service';


@Injectable()
export class SharedPrintService {
  phone = '011 444 448';
  tin = '02677502';

  // user;
  _date;
  date;
  time;

  USER: any;

  constructor(private dateTimeService: DateTimeService) {}

  createInvoice(list, USER) {
    // alert('yuhuu');
    this._date = this.dateTimeService.convertDate(new Date(), '/');
    this.date = this._date['date'];
    this.time = this._date['time'];


    let content = '';
    for (const i of list) {

      if (i.date) {
        const date = this.dateTimeService.convertDate(new Date(i.date), '/');
        this.date = date['date'];
        this.time = date['time'];
      }

      const upay = `
          <div style="display: flex;">
            <div>
              <div style="padding-top: 5px;">"Յուփեյ" ՓԲԸ</div>
              <div style="padding-top: 5px;">ՀՎՀՀ ${this.tin}</div>
              <div style="padding-top: 5px;">Հեռ. ${this.phone}</div>
              <div style="padding-top: 5px;">մ/ճ ${i.branchName}</div>
            </div>
            <div style="width: 50%; margin-left: auto; margin-top: -20px; text-align: right"><img src='/shared-assets/img/logo.svg'style="display: inline-block; width: 120px;" /></div>
          </div>
        `;

      let operatorName = USER;
      if ( i.operator ) {
        operatorName = i.operator;
      }

      const cashier = `
          <div style="display: flex; justify-content: space-between; margin-top: 15px;">
            Գանձապահ ${operatorName}
            <span>______________</span>
          </div>
          <div style="margin-top: 15px;">
            Շնորհակալություն
            <span>${this.date} ${this.time}</span>
          </div>`;

      let td = '';
      for (const j of i.fields) {

        if ( j === 'hr') {
          td +=  '<td style="display: block; width: 100%; border-bottom: 1px solid; padding-top: 10px; margin-bottom: 5px;"></td>';
        } else if ( j.bold || j.name === 'Բաժանորդային համար') {
          td += `<td style="display: flex; justify-content: space-between; padding-top: 5px;">
            ${j.name}
              <b style="margin-left: 10px; text-align: right; font-size: .9rem;">
                ${j.value}
              </b>
          </td>`;
        } else {
          td += `<td style="display: flex; justify-content: space-between; padding-top: 5px;">
            ${j.name}
              <span style="margin-left: 10px; text-align: right;">
                ${j.value}
              </span>
          </td>`;
        }
      }

      const tr = `<tr style="display: block;">
           ${td}
          </tr>`;

      const totallyPaid = i.totallyPaid;

      content += `
            <div style="display: flex; flex-direction: column; border:1px solid black;
            padding: 10px; font-size: 12px; width: 7cm; margin: 15px auto;">
              ${upay}
              <table style="width:100%; font-size: 12px;">
              ${tr}
              </table>
              <div style="display: flex; justify-content: space-between;
                margin-top: 5px; padding-top: 15px; border-top: 1px solid black;">
                Ընդամենը վճարվել է <b style="font-size: .9rem;"> ${totallyPaid} դր</b>
              </div>
              ${cashier}
            </div>
          `;
    }

    this.openPrintWindow(content, 'termo');
  }

  // getPrinter(type) {
  //   const printersMap = {
  //     a4 : this.a4Printer,
  //     termo: this.openPrintWindow
  //   };
  //   return printersMap[type] || printersMap.termo;
  // }


  openPrintWindow(content, printerType: 'a4' | 'termo', pageDirection = 'portrait') {
    let mywindow;
    // alert(printerType);

    if (printerType === 'termo') {
      mywindow = window.open('', 'PRINT', 'height=400,width=600');
      mywindow.document.write('<html><head><title>' + 'upay' + '</title>');
    } else {
      mywindow = window.open('', 'PRINT');
      mywindow.document.write('<html><head><title>' + 'upay' + `</title><style type="text/css" media="print">  @page { size: ${pageDirection}; } </style>`);
    }


    mywindow.document.write('</head><body style="margin: 0; pading: 0; width: 100%;"></body>');

    mywindow.document.write(content);

    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.onload = function () {
      mywindow.print();
      mywindow.close();
    };

  }
}
