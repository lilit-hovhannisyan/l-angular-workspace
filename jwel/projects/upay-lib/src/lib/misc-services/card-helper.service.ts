import {Injectable} from '@angular/core';

function between(x, min, max) {
  return x >= min && x <= max;
}

export  const CardType  = {
   NONE: 'no-card-logo',
   ARCA: 'arca',
   MAESTRO: 'maestro',
   MASTERCARD: 'mastercard',
   VISA_ELECTRON: 'visaelectron',
   VISA: 'visa',
   AMERICAN_EXPRESS: 'americanexpress',
   JCB: 'jcb',
   DINER_CLUB: 'dinerclub'
};

@Injectable()
export class CardHelperService {
 public getCardType (number: string): string {
   if (!number) {
     return CardType.NONE;
   }

   if (number.substring(0, 4) === '9051') {
    return  CardType.ARCA;
   } else if (['5018', '5020', '5038', '5612', '5893', '6304', '6759', '6761', '6762', '6763', '6770', '0604', '6390'].includes(number.substring(0, 4))) {
     return CardType.MAESTRO;
   } else if (between(number.substring(0, 2), 50, 55) ) {// number.substring(0, 2).toInt() in 50..55
     return CardType.MASTERCARD;
   } else if ( between(number.substring(0, 6), 222100, 272099)) {
     return CardType.MASTERCARD;
   } else if (['4026', '4405', '4508', '4844', '4913', '4917'].includes(number.substring(0, 4))) {
     return CardType.VISA_ELECTRON;
   } else if (number.startsWith('417500')) {
     return CardType.VISA_ELECTRON;
   } else if (number.startsWith('4')) {
     return CardType.VISA;
   } else if (['34', '37'].includes(number.substring(0, 2))) {
     return CardType.AMERICAN_EXPRESS;
   } else if (between(number.substring(0, 4),  3528, 3589)) {
     return CardType.JCB;
   } else if (between(number.substring(0, 3),  300, 305)) {
     return CardType.DINER_CLUB;
   } else if (between(number.substring(0, 2), 38, 39)) {
     return CardType.DINER_CLUB;
   } else if (number.substring(0, 3) === '309') {
     return CardType.DINER_CLUB;
   } else if (number.substring(0, 2) === '36') {
     return CardType.DINER_CLUB;
   } else {
     return CardType.NONE;
   }
 }
}
