import {Injectable} from '@angular/core';

@Injectable()
export class SynchronizedCallsService {

  performSequentally(items, operation, onComplete, onError) {
    // alert('++5500');

    function handler(itemIndex) {
      return response => {
        if ( ++itemIndex < items.length ) {
          callOperation(itemIndex);
        } else {
          onComplete(items);
        }
      };
    }

    function callOperation(itemIndex) {
      operation(items[itemIndex]).subscribe(handler(itemIndex), err => {
        if ( onError ) {
          onError(err, items[itemIndex]);
        }
        handler(itemIndex);
        // handler(itemIndex)();
      });
    }

    callOperation(0);
  }
}
