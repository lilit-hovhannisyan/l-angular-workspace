export const renderNumberWithSpliter = (number, spliter = '.') => {
    if (!number) {
       return;
    }
    // debugger
    let _num = number + '';

    function changeRenderFormat(str) {
        if (str.length <= 3) {
            return _num;
        }

        const lastPart = str.substring(str.length - 3);
        const startPart = str.substring(0, str.length - 3);
        let result;

        if (startPart.length > 3) {
            const newResult = changeRenderFormat(startPart);
            result = `${newResult}${spliter}${lastPart}`;
        } else {
            result = `${startPart}${spliter}${lastPart}`;
        }

        return result;
    }

    _num = changeRenderFormat(_num);

    return _num;
};

export const roundDecimalNumberToTwoSymbols = (number) => {
  return  Math.round(number * 100) / 100 ;
};

export const splitNumberWithLumas = (_number, spliter = ' ') => {
  _number = roundDecimalNumberToTwoSymbols(_number);
  const num = _number + '';

  let decimalPart = '';
  let nonDecimalPart = num;
  if (num.indexOf('.') >= 0) {
    decimalPart += num.substring(num.indexOf('.'));
    nonDecimalPart = num.substring(0, num.indexOf('.'));
  }

  nonDecimalPart = renderNumberWithSpliter(nonDecimalPart, spliter);
  return nonDecimalPart + decimalPart;
};

const getMoneyPriceWithoutLumas = (value) => {
  return Math.floor(value / 100);
};
