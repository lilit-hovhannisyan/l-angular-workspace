import {Injectable} from '@angular/core';

@Injectable()
export class CalculateCommissionService {
  calculateCommission(commissionsRole, _amount) {
    const amount  = _amount * 100;
    let commission = 0;
    if (commissionsRole) {
      const role = commissionsRole.find(cr => {
        if (cr.min < amount && amount <= cr.max) {
          return true;
        }

        if (cr.min < amount && !cr.max) {
          return true;
        }
        return false;
      });

      if (role && role.type === 'FIXED') {
        commission = role.value;
      }
      if (role && role.type === 'PERCENT') {
        commission = (role.value * amount) / 100;
      }
    }

    commission = Math.ceil(commission);
    return commission;
  }
}
