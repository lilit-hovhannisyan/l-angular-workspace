import {Injectable} from '@angular/core';


export const dateDecrementDay = (date, daysCount = 0) => {
  return new Date(new Date(date).setDate(date.getDate() - daysCount));
};

export const dateDecrementMonth = (date, monthsCount = 0) => {
  return new Date(new Date(date).setDate(date.getMonth() - monthsCount));
};

export const dateIncrementDay = (date, daysCount = 0) => {
  return new Date(new Date(date).setDate(date.getDate() + daysCount));
};

export const dateFormat = (date, format = 'dd/mm/yyyy HH:MM:SS') => { // todo format
  const outputDate = {};

  let d, m, y, h, min, sec;

  d = date.getDate();
  if ( d < 10 ) {
    d = '0' + d;
  }
  m = date.getMonth() + 1;
  if ( m < 10 ) {
    m = '0' + m;
  }
  y = date.getFullYear();
  h = date.getHours();
  if ( h < 10 ) {
    h = '0' + h;
  }
  min = date.getMinutes();
  if ( min < 10 ) {
    min = '0' + min;
  }
  sec = date.getSeconds();
  if ( sec < 10 ) {
    sec = '0' + sec;
  }

  return format.replace('dd', d).replace('mm', m).replace('yyyy', y).replace('HH', h).replace('MM', min).replace('SS', sec);

};

export const checkIfOneDayIsPassed = (date) => {
  const oneDay = 24 * 60 * 60 * 1000; // one day in miliseconds
  const today = new Date().getTime();
  const compareAgainstDate = new Date(date).getTime();

  return oneDay < today - compareAgainstDate;
};

export const compareDates = (date1, date2) => {
  date1 = new Date(date1).setHours(0, 0, 0, 0);
  date2 = new Date(date2).setHours(0, 0, 0, 0);

  return date1 <= date2;
};

@Injectable()
export class DateTimeService {
  convertDate( inputDate, dateSpliter ) {
    // alert('datae time');
    const outputDate = {};

    let d, m, y, h, min, sec;

    d = inputDate.getDate();
    if ( d < 10 ) {
      d = '0' + d;
    }
    m = inputDate.getMonth() + 1;
    if ( m < 10 ) {
      m = '0' + m;
    }
    y = inputDate.getFullYear();
    h = inputDate.getHours();
    if ( h < 10 ) {
      h = '0' + h;
    }
    min = inputDate.getMinutes();
    if ( min < 10 ) {
      min = '0' + min;
    }
    sec = inputDate.getSeconds();
    if ( sec < 10 ) {
      sec = '0' + sec;
    }

    outputDate['date'] = d + dateSpliter + m + dateSpliter + y;
    outputDate['time'] = h + ':' + min + ':' + sec;

    return outputDate;
  }
}
