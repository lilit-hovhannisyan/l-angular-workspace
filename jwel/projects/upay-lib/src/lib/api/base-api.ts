import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {ErrorHandler} from './error-handler';
import {throwError} from 'rxjs/index';
import {tap} from 'rxjs/operators';

// import {environment} from '../../environments/environment';
//
// const officeUrl = environment.APIEndpoint.officeUrl;
// const adminUrl = environment.APIEndpoint.adminUrl;
// const publicUrl = environment.APIEndpoint.publicUrl;

let _apiUrl = '';

export const setApiUrl = (url: string) => {

  _apiUrl = url;
};

// @dynamic
@Injectable({
  providedIn: 'root'
})
export class BaseApi {
  constructor(public http: HttpClient,
              public errorHandler: ErrorHandler) {
  }

  static get apiUrl () {
    return _apiUrl;
  }

  post<T>(url: string, data: any, options?: any) {
    const headers = this.getAuthorizationHeader();
    return this.http.post(url, data, {
      headers: headers
    }).pipe(map((res: any) => {
        if (res.error) {
          throwError(res.error);
          this.errorHandler.handle(res.error);
        } else {
          return res.data;
        }
      }));
  }

  get(url: string, queryOptions?: any) {
    const options: any = {};
    const headers = this.getAuthorizationHeader();

    options.headers = headers;

    if (queryOptions) {
      queryOptions.map( o => {
        options[o['key']] = o['value'];
      });
    }

    return this.http.get(url, options).pipe(tap(response => {
      // console.log('response', response);
    }));
  }

  getAuthorizationHeader() {
    const headers = new HttpHeaders();
    const authToken = localStorage.getItem('authToken');
    if (authToken) {
     return headers.append('Authorization', `Bearer ${authToken}`);
    } else {
      return headers;
    }
  }

}
