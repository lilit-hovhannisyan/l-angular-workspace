import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs/index';

// const responseCache = new Map();
// const pendingRequests = new Map();

@Injectable({
  providedIn: 'root',
})
export class ResponseCacheService {
  responseCache = new Map();
  pendingRequests = new Map();

  cache(api, methodName, requestData?, cacheKey?): Observable<any> {
    // debugger
    if (!cacheKey) {
      cacheKey = methodName;
    }

    const dataFromCache = this.responseCache.get(cacheKey);
    if (dataFromCache) {
      return of(dataFromCache);
    }

    // const pendingCall = this.pendingRequests.get(cacheKey);
    // if (pendingCall) {
    //   return pendingCall;
    // }

    const response = api[methodName](requestData);
    // this.pendingRequests.set(cacheKey, response);

    response.subscribe( responseData => {
      this.responseCache.set(cacheKey, responseData);
      // this.pendingRequests.delete(cacheKey);
    }, err => {
      // this.pendingRequests.delete(cacheKey);
    });
    return response;
  }

}
