import {Observable} from 'rxjs/index';
import {BaseApi} from './base-api';
import {RestApi} from './rest-api';
import {FileDownload} from './file-download';

let _fileUploadUrl = 'wtf';

export const fileStorageUrl = (url: string) => {
  console.log('fileStorageUrl', url);
  _fileUploadUrl = url;
};


export class FileUploadApi extends RestApi {
  get fileUploadUrl () {
    return _fileUploadUrl;
  }

  createGroup(groupName: string, data?: any,  options?: any): Observable<any> {
    console.log('create group', this.fileUploadUrl);
    return this.post(this.fileUploadUrl + '/groups/' + groupName, data, options);
  }

  // recursively delete files && directory
  deleteGroup(groupName: string, options?: any) {
    return this.delete(this.fileUploadUrl + '/groups/' + groupName, options);
  }

  getFilesInGroup(groupName: string, options?: any): Observable<any> {
    return this.get(this.fileUploadUrl + '/groups/' + groupName, options);
  }

  addFilesInGroup(groupName: string, fileName: string, data: any, options?: any): Observable<any> {
    console.log('single file', this.fileUploadUrl);
    return this.post(this.fileUploadUrl + '/groups/' + groupName + '/files/' + fileName, data, options);
  }

  getFileFromGroup(groupName: string, fileName: string, options?: any): Observable<any> {
    return this.get(this.fileUploadUrl + '/groups/' + groupName + '/files/' + fileName, options);
  }

  getFile(path: string, options?: any): Observable<any> {
    return this.get(this.fileUploadUrl + path, options);
  }

  deleteFileFromGroup(groupName: string, fileName: string, options?: any) {
    return this.delete(this.fileUploadUrl + '/groups/' + groupName + '/files/' + fileName, options);
  }
}
