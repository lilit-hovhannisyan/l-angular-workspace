import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {map} from 'rxjs/internal/operators';
import {tap} from 'rxjs/operators';

@Injectable()
export class RestApi {
  constructor(public http: HttpClient) {
  }

  post<T>(url: string, data: any, queryOptions?: any) {
    const options = this.setupRequestOptions(queryOptions);

    return this.http.post(url, data, options)
      .pipe(map((res: any) => {
        console.log('rest post: ', res);
        return res;
      }));
  }

  get(url: string, queryOptions?: any) {
    const options = this.setupRequestOptions(queryOptions);

    return this.http.get(url, options).pipe(tap(response => {
      // console.log('rest get', response);
    }));
  }

  delete<T>(url: string, queryOptions?: any) {
    const options = this.setupRequestOptions(queryOptions);

    return this.http.delete(url, options)
      .pipe(map((res: any) => {
        // console.log('rest delete: ', res);
      }));
  }

  setupRequestOptions(queryOptions) {
    const options: any = {};

    if (queryOptions) {
      queryOptions.map( o => {
        options[o['key']] = o['value'];
      });
    }

    return options;
  }
}
