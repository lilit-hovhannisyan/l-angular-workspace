import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

// export const ERROR_CODE = {
//   INVALID_AUTH_TOKEN: 0,
// };

export interface IErrorData {
  code: string;
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class ErrorHandler {

  _errorData?: IErrorData = null;
  errorData = new BehaviorSubject(this._errorData);


  constructor() {
  }

  handle(error) {
    this._errorData = {
      code: error.code,
      message: error.message
    };
    this.errorData.next(this._errorData);
    throw error;
  }

  clear() {
    this._errorData = null;
    this.errorData.next(this._errorData);
  }

}
