/* tslint:disable */
import {Observable} from "rxjs/index";
import {BaseApi} from "./base-api";

export class Page {
  index: number
  size: number
}

export class Pair {
  key: string;
  val: boolean;
}

export class Order {
  fields: Array<Pair>  
}

export class Paged<T> {
  items: Array<T>;
  page: Page;
  total: number;
}


export enum CustomerStatus {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  BLOCKED = "BLOCKED",
}

export enum OperatorStatus {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  BLOCKED = "BLOCKED",
}

export enum FavoriteType {
  DEFAULT = "DEFAULT",
  CARS = "CARS",
  LOANS = "LOANS",
}

export enum Scope {
  CUSTOMER = "CUSTOMER",
  BRANCH = "BRANCH",
  SUPPORT = "SUPPORT",
  ADMIN = "ADMIN",
  OPERATOR = "OPERATOR",
  TEMPORARY_PASSWORD = "TEMPORARY_PASSWORD",
  ACCOUNTANT = "ACCOUNTANT",
  CLAIMIST = "CLAIMIST",
  CLAIMIST_HEAD = "CLAIMIST_HEAD",
  FROZEN_ACC = "FROZEN_ACC",
}

export enum DocumentType {
  PASSPORT = "PASSPORT",
  IDCARD = "IDCARD",
}

export enum UserType {
  UNIDENTIFIED = "UNIDENTIFIED",
  CARD_IDENTIFIED = "CARD_IDENTIFIED",
  BRANCH_IDENTIFIED = "BRANCH_IDENTIFIED",
}

export enum DocumentStatus {
  ACTIVE = "ACTIVE",
  ARCHIVED = "ARCHIVED",
}

export enum Validity {
  VALID = "VALID",
  INVALID = "INVALID",
}

export enum ActivityType {
  CUSTOMER_REG = "CUSTOMER_REG",
  CUSTOMER_TERM = "CUSTOMER_TERM",
  CARD_REG = "CARD_REG",
  CARD_REMOVAL = "CARD_REMOVAL",
  IDENTIFICATION_BY_DOCUMENT = "IDENTIFICATION_BY_DOCUMENT",
  IDENTIFICATION_BY_CARD = "IDENTIFICATION_BY_CARD",
  PERSONAL_DATA_CHANGE = "PERSONAL_DATA_CHANGE",
  PHONE_REG = "PHONE_REG",
  PHONE_CHANGE = "PHONE_CHANGE",
  EMAIL_REG = "EMAIL_REG",
  EMAIL_CHANGE = "EMAIL_CHANGE",
  ACCOUNT_FREEZE = "ACCOUNT_FREEZE",
  ACCOUNT_UNFREEZE = "ACCOUNT_UNFREEZE",
}

export enum SourceType {
  CARD = "CARD",
  BRANCH = "BRANCH",
  TERMINAL = "TERMINAL",
  WALLET = "WALLET",
}

export enum Platform {
  MOBILE = "MOBILE",
  WEB = "WEB",
}

export enum TransactionStatus {
  PENDING = "PENDING",
  UNKNOWN = "UNKNOWN",
  DONE = "DONE",
  FAILED = "FAILED",
  REFUNDED = "REFUNDED",
  CANCELLED = "CANCELLED",
}

// @dynamic
export class TestDebt {
  debt: number;
  paramA: string;
  paramB: string;
  
  constructor() {
  }
  
  static build(): TestDebtBuilder {
    return new TestDebtBuilder();
  }
}

class TestDebtBuilder {
  private instance: TestDebt;
  
  constructor() {
    this.instance = new TestDebt();
  }
  
  debt(value: number): TestDebtBuilder {
    this.instance.debt = value;
    return this;
  }
  
  paramA(value: string): TestDebtBuilder {
    this.instance.paramA = value;
    return this;
  }
  
  paramB(value: string): TestDebtBuilder {
    this.instance.paramB = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashoutDocument {
  type: DocumentType;
  name: string;
  documentNumber: string;
  dateOfIssue: string;
  dateOfExpiry: string;
  authority: string;
  
  constructor() {
  }
  
  static build(): CashoutDocumentBuilder {
    return new CashoutDocumentBuilder();
  }
}

class CashoutDocumentBuilder {
  private instance: CashoutDocument;
  
  constructor() {
    this.instance = new CashoutDocument();
  }
  
  type(value: DocumentType): CashoutDocumentBuilder {
    this.instance.type = value;
    return this;
  }
  
  name(value: string): CashoutDocumentBuilder {
    this.instance.name = value;
    return this;
  }
  
  documentNumber(value: string): CashoutDocumentBuilder {
    this.instance.documentNumber = value;
    return this;
  }
  
  dateOfIssue(value: string): CashoutDocumentBuilder {
    this.instance.dateOfIssue = value;
    return this;
  }
  
  dateOfExpiry(value: string): CashoutDocumentBuilder {
    this.instance.dateOfExpiry = value;
    return this;
  }
  
  authority(value: string): CashoutDocumentBuilder {
    this.instance.authority = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AmountRange {
  minAmount: number;
  maxAmount: number;
  
  constructor() {
  }
  
  static build(): AmountRangeBuilder {
    return new AmountRangeBuilder();
  }
}

class AmountRangeBuilder {
  private instance: AmountRange;
  
  constructor() {
    this.instance = new AmountRange();
  }
  
  minAmount(value: number): AmountRangeBuilder {
    this.instance.minAmount = value;
    return this;
  }
  
  maxAmount(value: number): AmountRangeBuilder {
    this.instance.maxAmount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class TokenInfo {
  accessToken: string;
  refreshToken: string;
  
  constructor() {
  }
  
  static build(): TokenInfoBuilder {
    return new TokenInfoBuilder();
  }
}

class TokenInfoBuilder {
  private instance: TokenInfo;
  
  constructor() {
    this.instance = new TokenInfo();
  }
  
  accessToken(value: string): TokenInfoBuilder {
    this.instance.accessToken = value;
    return this;
  }
  
  refreshToken(value: string): TokenInfoBuilder {
    this.instance.refreshToken = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Contract {
  subscriber: string;
  branchId: string;
  searchData: string;
  providerId: string;
  
  constructor() {
  }
  
  static build(): ContractBuilder {
    return new ContractBuilder();
  }
}

class ContractBuilder {
  private instance: Contract;
  
  constructor() {
    this.instance = new Contract();
  }
  
  subscriber(value: string): ContractBuilder {
    this.instance.subscriber = value;
    return this;
  }
  
  branchId(value: string): ContractBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  searchData(value: string): ContractBuilder {
    this.instance.searchData = value;
    return this;
  }
  
  providerId(value: string): ContractBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Principal {
  id: string;
  identity: string;
  scopes: Array<Scope>;
  userType: UserType;
  
  constructor() {
  }
  
  static build(): PrincipalBuilder {
    return new PrincipalBuilder();
  }
}

class PrincipalBuilder {
  private instance: Principal;
  
  constructor() {
    this.instance = new Principal();
  }
  
  id(value: string): PrincipalBuilder {
    this.instance.id = value;
    return this;
  }
  
  identity(value: string): PrincipalBuilder {
    this.instance.identity = value;
    return this;
  }
  
  scopes(value: Array<Scope>): PrincipalBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  userType(value: UserType): PrincipalBuilder {
    this.instance.userType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorInfo {
  id: string;
  forename: string;
  surname: string;
  userName: string;
  email: string;
  phone: string;
  cashboxId: string;
  expiration: string;
  enabled: boolean;
  status: OperatorStatus;
  scope: Array<Scope>;
  
  constructor() {
  }
  
  static build(): OperatorInfoBuilder {
    return new OperatorInfoBuilder();
  }
}

class OperatorInfoBuilder {
  private instance: OperatorInfo;
  
  constructor() {
    this.instance = new OperatorInfo();
  }
  
  id(value: string): OperatorInfoBuilder {
    this.instance.id = value;
    return this;
  }
  
  forename(value: string): OperatorInfoBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): OperatorInfoBuilder {
    this.instance.surname = value;
    return this;
  }
  
  userName(value: string): OperatorInfoBuilder {
    this.instance.userName = value;
    return this;
  }
  
  email(value: string): OperatorInfoBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): OperatorInfoBuilder {
    this.instance.phone = value;
    return this;
  }
  
  cashboxId(value: string): OperatorInfoBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  expiration(value: string): OperatorInfoBuilder {
    this.instance.expiration = value;
    return this;
  }
  
  enabled(value: boolean): OperatorInfoBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  status(value: OperatorStatus): OperatorInfoBuilder {
    this.instance.status = value;
    return this;
  }
  
  scope(value: Array<Scope>): OperatorInfoBuilder {
    this.instance.scope = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorName {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): OperatorNameBuilder {
    return new OperatorNameBuilder();
  }
}

class OperatorNameBuilder {
  private instance: OperatorName;
  
  constructor() {
    this.instance = new OperatorName();
  }
  
  id(value: string): OperatorNameBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): OperatorNameBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerInfo {
  number: string;
  phone: string;
  email: string;
  forename: string;
  surname: string;
  status: CustomerStatus;
  isVerified: boolean;
  scope: Array<Scope>;
  language: string;
  fcmEnabled: boolean;
  balance: number;
  balanceLimit: number;
  accNumber: string;
  userType: UserType;
  passportScan: string;
  accFrozen: boolean;
  passportId: string;
  dateOfIssue: string;
  dateOfExpiry: string;
  authority: string;
  deleted: boolean;
  refilled: boolean;
  
  constructor() {
  }
  
  static build(): CustomerInfoBuilder {
    return new CustomerInfoBuilder();
  }
}

class CustomerInfoBuilder {
  private instance: CustomerInfo;
  
  constructor() {
    this.instance = new CustomerInfo();
  }
  
  number(value: string): CustomerInfoBuilder {
    this.instance.number = value;
    return this;
  }
  
  phone(value: string): CustomerInfoBuilder {
    this.instance.phone = value;
    return this;
  }
  
  email(value: string): CustomerInfoBuilder {
    this.instance.email = value;
    return this;
  }
  
  forename(value: string): CustomerInfoBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): CustomerInfoBuilder {
    this.instance.surname = value;
    return this;
  }
  
  status(value: CustomerStatus): CustomerInfoBuilder {
    this.instance.status = value;
    return this;
  }
  
  isVerified(value: boolean): CustomerInfoBuilder {
    this.instance.isVerified = value;
    return this;
  }
  
  scope(value: Array<Scope>): CustomerInfoBuilder {
    this.instance.scope = value;
    return this;
  }
  
  language(value: string): CustomerInfoBuilder {
    this.instance.language = value;
    return this;
  }
  
  fcmEnabled(value: boolean): CustomerInfoBuilder {
    this.instance.fcmEnabled = value;
    return this;
  }
  
  balance(value: number): CustomerInfoBuilder {
    this.instance.balance = value;
    return this;
  }
  
  balanceLimit(value: number): CustomerInfoBuilder {
    this.instance.balanceLimit = value;
    return this;
  }
  
  accNumber(value: string): CustomerInfoBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  userType(value: UserType): CustomerInfoBuilder {
    this.instance.userType = value;
    return this;
  }
  
  passportScan(value: string): CustomerInfoBuilder {
    this.instance.passportScan = value;
    return this;
  }
  
  accFrozen(value: boolean): CustomerInfoBuilder {
    this.instance.accFrozen = value;
    return this;
  }
  
  passportId(value: string): CustomerInfoBuilder {
    this.instance.passportId = value;
    return this;
  }
  
  dateOfIssue(value: string): CustomerInfoBuilder {
    this.instance.dateOfIssue = value;
    return this;
  }
  
  dateOfExpiry(value: string): CustomerInfoBuilder {
    this.instance.dateOfExpiry = value;
    return this;
  }
  
  authority(value: string): CustomerInfoBuilder {
    this.instance.authority = value;
    return this;
  }
  
  deleted(value: boolean): CustomerInfoBuilder {
    this.instance.deleted = value;
    return this;
  }
  
  refilled(value: boolean): CustomerInfoBuilder {
    this.instance.refilled = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantInfo {
  id: string;
  forename: string;
  surname: string;
  username: string;
  email: string;
  phone: string;
  enabled: boolean;
  scope: Array<Scope>;
  
  constructor() {
  }
  
  static build(): AccountantInfoBuilder {
    return new AccountantInfoBuilder();
  }
}

class AccountantInfoBuilder {
  private instance: AccountantInfo;
  
  constructor() {
    this.instance = new AccountantInfo();
  }
  
  id(value: string): AccountantInfoBuilder {
    this.instance.id = value;
    return this;
  }
  
  forename(value: string): AccountantInfoBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AccountantInfoBuilder {
    this.instance.surname = value;
    return this;
  }
  
  username(value: string): AccountantInfoBuilder {
    this.instance.username = value;
    return this;
  }
  
  email(value: string): AccountantInfoBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AccountantInfoBuilder {
    this.instance.phone = value;
    return this;
  }
  
  enabled(value: boolean): AccountantInfoBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  scope(value: Array<Scope>): AccountantInfoBuilder {
    this.instance.scope = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export enum CommissionRuleType {
  FIXED = "FIXED",
  PERCENT = "PERCENT",
}

export enum CommissionAction {
  SAVE = "SAVE",
  DELETE = "DELETE",
}

export enum EncashmentType {
  INTERNAL_IN = "INTERNAL_IN",
  INTERNAL_OUT = "INTERNAL_OUT",
  EXTERNAL_IN = "EXTERNAL_IN",
  EXTERNAL_OUT = "EXTERNAL_OUT",
  UCOM = "UCOM",
  IDRAM = "IDRAM",
  LOAN_GOOD_CREDIT = "LOAN_GOOD_CREDIT",
  LOAN_VARKS_AM = "LOAN_VARKS_AM",
  LOAN_GLOBAL_CREDIT = "LOAN_GLOBAL_CREDIT",
  PHONE_BY_BACK_SERVICE = "PHONE_BY_BACK_SERVICE",
  CUSTOMER_CASHOUT = "CUSTOMER_CASHOUT",
  OP_CLAIM_APPROVED = "OP_CLAIM_APPROVED",
  OP_CLAIM_REJECTED = "OP_CLAIM_REJECTED",
}

export enum CashboxAction {
  PAYMENT_IN = "PAYMENT_IN",
  ENCASHMENT_IN = "ENCASHMENT_IN",
  ENCASHMENT_OUT = "ENCASHMENT_OUT",
}

export enum UserTransactionType {
  CASH_IN = "CASH_IN",
  PAYMENT_OUT = "PAYMENT_OUT",
  CASH_OUT = "CASH_OUT",
  REFUND = "REFUND",
}

// @dynamic
export class CommissionRange {
  min: number;
  max: number;
  value: number;
  type: CommissionRuleType;
  
  constructor() {
  }
  
  static build(): CommissionRangeBuilder {
    return new CommissionRangeBuilder();
  }
}

class CommissionRangeBuilder {
  private instance: CommissionRange;
  
  constructor() {
    this.instance = new CommissionRange();
  }
  
  min(value: number): CommissionRangeBuilder {
    this.instance.min = value;
    return this;
  }
  
  max(value: number): CommissionRangeBuilder {
    this.instance.max = value;
    return this;
  }
  
  value(value: number): CommissionRangeBuilder {
    this.instance.value = value;
    return this;
  }
  
  type(value: CommissionRuleType): CommissionRangeBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxBalance {
  cashboxId: string;
  name: string;
  amountIn: number;
  amountOut: number;
  cashlessOut: number;
  openingBalance: number;
  commissions: number;
  currentBalance: number;
  
  constructor() {
  }
  
  static build(): CashboxBalanceBuilder {
    return new CashboxBalanceBuilder();
  }
}

class CashboxBalanceBuilder {
  private instance: CashboxBalance;
  
  constructor() {
    this.instance = new CashboxBalance();
  }
  
  cashboxId(value: string): CashboxBalanceBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  name(value: string): CashboxBalanceBuilder {
    this.instance.name = value;
    return this;
  }
  
  amountIn(value: number): CashboxBalanceBuilder {
    this.instance.amountIn = value;
    return this;
  }
  
  amountOut(value: number): CashboxBalanceBuilder {
    this.instance.amountOut = value;
    return this;
  }
  
  cashlessOut(value: number): CashboxBalanceBuilder {
    this.instance.cashlessOut = value;
    return this;
  }
  
  openingBalance(value: number): CashboxBalanceBuilder {
    this.instance.openingBalance = value;
    return this;
  }
  
  commissions(value: number): CashboxBalanceBuilder {
    this.instance.commissions = value;
    return this;
  }
  
  currentBalance(value: number): CashboxBalanceBuilder {
    this.instance.currentBalance = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ContractField {
  key: string;
  label: string;
  
  constructor() {
  }
  
  static build(): ContractFieldBuilder {
    return new ContractFieldBuilder();
  }
}

class ContractFieldBuilder {
  private instance: ContractField;
  
  constructor() {
    this.instance = new ContractField();
  }
  
  key(value: string): ContractFieldBuilder {
    this.instance.key = value;
    return this;
  }
  
  label(value: string): ContractFieldBuilder {
    this.instance.label = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ContractDesc {
  idType: string;
  label: string;
  fields: Array<ContractField>;
  
  constructor() {
  }
  
  static build(): ContractDescBuilder {
    return new ContractDescBuilder();
  }
}

class ContractDescBuilder {
  private instance: ContractDesc;
  
  constructor() {
    this.instance = new ContractDesc();
  }
  
  idType(value: string): ContractDescBuilder {
    this.instance.idType = value;
    return this;
  }
  
  label(value: string): ContractDescBuilder {
    this.instance.label = value;
    return this;
  }
  
  fields(value: Array<ContractField>): ContractDescBuilder {
    this.instance.fields = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Debt {
  providerId: string;
  customerId: string;
  debtDate: Date;
  debtAmount: number;
  
  constructor() {
  }
  
  static build(): DebtBuilder {
    return new DebtBuilder();
  }
}

class DebtBuilder {
  private instance: Debt;
  
  constructor() {
    this.instance = new Debt();
  }
  
  providerId(value: string): DebtBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  customerId(value: string): DebtBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  debtDate(value: Date): DebtBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmount(value: number): DebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class UpayFixedBalance {
  total: number;
  internet: number;
  phone: number;
  tv: number;
  deposit: number;
  
  constructor() {
  }
  
  static build(): UpayFixedBalanceBuilder {
    return new UpayFixedBalanceBuilder();
  }
}

class UpayFixedBalanceBuilder {
  private instance: UpayFixedBalance;
  
  constructor() {
    this.instance = new UpayFixedBalance();
  }
  
  total(value: number): UpayFixedBalanceBuilder {
    this.instance.total = value;
    return this;
  }
  
  internet(value: number): UpayFixedBalanceBuilder {
    this.instance.internet = value;
    return this;
  }
  
  phone(value: number): UpayFixedBalanceBuilder {
    this.instance.phone = value;
    return this;
  }
  
  tv(value: number): UpayFixedBalanceBuilder {
    this.instance.tv = value;
    return this;
  }
  
  deposit(value: number): UpayFixedBalanceBuilder {
    this.instance.deposit = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class UpayFixedDebt {
  client: string;
  number: string;
  balance: UpayFixedBalance;
  
  constructor() {
  }
  
  static build(): UpayFixedDebtBuilder {
    return new UpayFixedDebtBuilder();
  }
}

class UpayFixedDebtBuilder {
  private instance: UpayFixedDebt;
  
  constructor() {
    this.instance = new UpayFixedDebt();
  }
  
  client(value: string): UpayFixedDebtBuilder {
    this.instance.client = value;
    return this;
  }
  
  number(value: string): UpayFixedDebtBuilder {
    this.instance.number = value;
    return this;
  }
  
  balance(value: UpayFixedBalance): UpayFixedDebtBuilder {
    this.instance.balance = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtUcomMobile {
  phone: string;
  debt: number;
  account: string;
  payMethod: string;
  client: string;
  
  constructor() {
  }
  
  static build(): DebtUcomMobileBuilder {
    return new DebtUcomMobileBuilder();
  }
}

class DebtUcomMobileBuilder {
  private instance: DebtUcomMobile;
  
  constructor() {
    this.instance = new DebtUcomMobile();
  }
  
  phone(value: string): DebtUcomMobileBuilder {
    this.instance.phone = value;
    return this;
  }
  
  debt(value: number): DebtUcomMobileBuilder {
    this.instance.debt = value;
    return this;
  }
  
  account(value: string): DebtUcomMobileBuilder {
    this.instance.account = value;
    return this;
  }
  
  payMethod(value: string): DebtUcomMobileBuilder {
    this.instance.payMethod = value;
    return this;
  }
  
  client(value: string): DebtUcomMobileBuilder {
    this.instance.client = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MobileDebt {
  customerId: string;
  debtDate: string;
  debt: number;
  customerName: string;
  paidByIdram: number;
  type: number;
  idramPayDate: string;
  updated: string;
  serviceId: number;
  
  constructor() {
  }
  
  static build(): MobileDebtBuilder {
    return new MobileDebtBuilder();
  }
}

class MobileDebtBuilder {
  private instance: MobileDebt;
  
  constructor() {
    this.instance = new MobileDebt();
  }
  
  customerId(value: string): MobileDebtBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  debtDate(value: string): MobileDebtBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debt(value: number): MobileDebtBuilder {
    this.instance.debt = value;
    return this;
  }
  
  customerName(value: string): MobileDebtBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  paidByIdram(value: number): MobileDebtBuilder {
    this.instance.paidByIdram = value;
    return this;
  }
  
  type(value: number): MobileDebtBuilder {
    this.instance.type = value;
    return this;
  }
  
  idramPayDate(value: string): MobileDebtBuilder {
    this.instance.idramPayDate = value;
    return this;
  }
  
  updated(value: string): MobileDebtBuilder {
    this.instance.updated = value;
    return this;
  }
  
  serviceId(value: number): MobileDebtBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BeelineBill {
  id: string;
  customerId: string;
  debtDate: string;
  debt: number;
  debtAmountRest: number;
  customerName: string;
  paidByIdram: number;
  idramPayDate: string;
  deposit: number;
  updated: string;
  serviceId: number;
  
  constructor() {
  }
  
  static build(): BeelineBillBuilder {
    return new BeelineBillBuilder();
  }
}

class BeelineBillBuilder {
  private instance: BeelineBill;
  
  constructor() {
    this.instance = new BeelineBill();
  }
  
  id(value: string): BeelineBillBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): BeelineBillBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  debtDate(value: string): BeelineBillBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debt(value: number): BeelineBillBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtAmountRest(value: number): BeelineBillBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  customerName(value: string): BeelineBillBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  paidByIdram(value: number): BeelineBillBuilder {
    this.instance.paidByIdram = value;
    return this;
  }
  
  idramPayDate(value: string): BeelineBillBuilder {
    this.instance.idramPayDate = value;
    return this;
  }
  
  deposit(value: number): BeelineBillBuilder {
    this.instance.deposit = value;
    return this;
  }
  
  updated(value: string): BeelineBillBuilder {
    this.instance.updated = value;
    return this;
  }
  
  serviceId(value: number): BeelineBillBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtElectricity {
  customerId: string;
  customerName: string;
  phone: string;
  address: string;
  debtAmount: number;
  debtAmountRest: number;
  currentPaymentDate: string;
  currentPaymentDebt: number;
  deposit: number;
  subsidyMoney: number;
  currentPaymentTraffic: string;
  currentPaymentNightTraffic: string;
  currentPaymentCounter: string;
  currentPaymentNightCounter: string;
  currentPaymentPaid: number;
  lastPaymentDate: string;
  lastPaymentDebt: number;
  lastPaymentPaid: number;
  updated: string;
  
  constructor() {
  }
  
  static build(): DebtElectricityBuilder {
    return new DebtElectricityBuilder();
  }
}

class DebtElectricityBuilder {
  private instance: DebtElectricity;
  
  constructor() {
    this.instance = new DebtElectricity();
  }
  
  customerId(value: string): DebtElectricityBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtElectricityBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  phone(value: string): DebtElectricityBuilder {
    this.instance.phone = value;
    return this;
  }
  
  address(value: string): DebtElectricityBuilder {
    this.instance.address = value;
    return this;
  }
  
  debtAmount(value: number): DebtElectricityBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtElectricityBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  currentPaymentDate(value: string): DebtElectricityBuilder {
    this.instance.currentPaymentDate = value;
    return this;
  }
  
  currentPaymentDebt(value: number): DebtElectricityBuilder {
    this.instance.currentPaymentDebt = value;
    return this;
  }
  
  deposit(value: number): DebtElectricityBuilder {
    this.instance.deposit = value;
    return this;
  }
  
  subsidyMoney(value: number): DebtElectricityBuilder {
    this.instance.subsidyMoney = value;
    return this;
  }
  
  currentPaymentTraffic(value: string): DebtElectricityBuilder {
    this.instance.currentPaymentTraffic = value;
    return this;
  }
  
  currentPaymentNightTraffic(value: string): DebtElectricityBuilder {
    this.instance.currentPaymentNightTraffic = value;
    return this;
  }
  
  currentPaymentCounter(value: string): DebtElectricityBuilder {
    this.instance.currentPaymentCounter = value;
    return this;
  }
  
  currentPaymentNightCounter(value: string): DebtElectricityBuilder {
    this.instance.currentPaymentNightCounter = value;
    return this;
  }
  
  currentPaymentPaid(value: number): DebtElectricityBuilder {
    this.instance.currentPaymentPaid = value;
    return this;
  }
  
  lastPaymentDate(value: string): DebtElectricityBuilder {
    this.instance.lastPaymentDate = value;
    return this;
  }
  
  lastPaymentDebt(value: number): DebtElectricityBuilder {
    this.instance.lastPaymentDebt = value;
    return this;
  }
  
  lastPaymentPaid(value: number): DebtElectricityBuilder {
    this.instance.lastPaymentPaid = value;
    return this;
  }
  
  updated(value: string): DebtElectricityBuilder {
    this.instance.updated = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GasBranch {
  branchId: number;
  marzCode: string;
  branchNameAm: string;
  branchNameRu: string;
  branchNameEn: string;
  
  constructor() {
  }
  
  static build(): GasBranchBuilder {
    return new GasBranchBuilder();
  }
}

class GasBranchBuilder {
  private instance: GasBranch;
  
  constructor() {
    this.instance = new GasBranch();
  }
  
  branchId(value: number): GasBranchBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  marzCode(value: string): GasBranchBuilder {
    this.instance.marzCode = value;
    return this;
  }
  
  branchNameAm(value: string): GasBranchBuilder {
    this.instance.branchNameAm = value;
    return this;
  }
  
  branchNameRu(value: string): GasBranchBuilder {
    this.instance.branchNameRu = value;
    return this;
  }
  
  branchNameEn(value: string): GasBranchBuilder {
    this.instance.branchNameEn = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GasDebt {
  id: string;
  customerId: string;
  branchId: string;
  phone: string;
  address: string;
  customerName: string;
  debtDate: string;
  costUnit: string;
  counterUnit: string;
  costAmount: string;
  subCostAmount: string;
  debtAmount: string;
  subDebtAmount: string;
  trafficDebtAmount: string;
  paidByIdram: string;
  subPaidByIdram: string;
  prevDate: string;
  deposit: string;
  subDeposit: string;
  prevDebtAmount: string;
  prevPaidAmount: string;
  subPrevDebtAmount: string;
  subPrevPaidAmount: string;
  subDebtAmountRest: string;
  debtAmountRest: string;
  debt: string;
  updated: string;
  
  constructor() {
  }
  
  static build(): GasDebtBuilder {
    return new GasDebtBuilder();
  }
}

class GasDebtBuilder {
  private instance: GasDebt;
  
  constructor() {
    this.instance = new GasDebt();
  }
  
  id(value: string): GasDebtBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): GasDebtBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  branchId(value: string): GasDebtBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  phone(value: string): GasDebtBuilder {
    this.instance.phone = value;
    return this;
  }
  
  address(value: string): GasDebtBuilder {
    this.instance.address = value;
    return this;
  }
  
  customerName(value: string): GasDebtBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debtDate(value: string): GasDebtBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  costUnit(value: string): GasDebtBuilder {
    this.instance.costUnit = value;
    return this;
  }
  
  counterUnit(value: string): GasDebtBuilder {
    this.instance.counterUnit = value;
    return this;
  }
  
  costAmount(value: string): GasDebtBuilder {
    this.instance.costAmount = value;
    return this;
  }
  
  subCostAmount(value: string): GasDebtBuilder {
    this.instance.subCostAmount = value;
    return this;
  }
  
  debtAmount(value: string): GasDebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  subDebtAmount(value: string): GasDebtBuilder {
    this.instance.subDebtAmount = value;
    return this;
  }
  
  trafficDebtAmount(value: string): GasDebtBuilder {
    this.instance.trafficDebtAmount = value;
    return this;
  }
  
  paidByIdram(value: string): GasDebtBuilder {
    this.instance.paidByIdram = value;
    return this;
  }
  
  subPaidByIdram(value: string): GasDebtBuilder {
    this.instance.subPaidByIdram = value;
    return this;
  }
  
  prevDate(value: string): GasDebtBuilder {
    this.instance.prevDate = value;
    return this;
  }
  
  deposit(value: string): GasDebtBuilder {
    this.instance.deposit = value;
    return this;
  }
  
  subDeposit(value: string): GasDebtBuilder {
    this.instance.subDeposit = value;
    return this;
  }
  
  prevDebtAmount(value: string): GasDebtBuilder {
    this.instance.prevDebtAmount = value;
    return this;
  }
  
  prevPaidAmount(value: string): GasDebtBuilder {
    this.instance.prevPaidAmount = value;
    return this;
  }
  
  subPrevDebtAmount(value: string): GasDebtBuilder {
    this.instance.subPrevDebtAmount = value;
    return this;
  }
  
  subPrevPaidAmount(value: string): GasDebtBuilder {
    this.instance.subPrevPaidAmount = value;
    return this;
  }
  
  subDebtAmountRest(value: string): GasDebtBuilder {
    this.instance.subDebtAmountRest = value;
    return this;
  }
  
  debtAmountRest(value: string): GasDebtBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  debt(value: string): GasDebtBuilder {
    this.instance.debt = value;
    return this;
  }
  
  updated(value: string): GasDebtBuilder {
    this.instance.updated = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtRostelecom {
  customerId: string;
  customerName: string;
  debt: number;
  debtTv: number;
  debtNoTv: number;
  debtDate: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtRostelecomBuilder {
    return new DebtRostelecomBuilder();
  }
}

class DebtRostelecomBuilder {
  private instance: DebtRostelecom;
  
  constructor() {
    this.instance = new DebtRostelecom();
  }
  
  customerId(value: string): DebtRostelecomBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtRostelecomBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtRostelecomBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtTv(value: number): DebtRostelecomBuilder {
    this.instance.debtTv = value;
    return this;
  }
  
  debtNoTv(value: number): DebtRostelecomBuilder {
    this.instance.debtNoTv = value;
    return this;
  }
  
  debtDate(value: string): DebtRostelecomBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: string): DebtRostelecomBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtInteractive {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtInteractiveBuilder {
    return new DebtInteractiveBuilder();
  }
}

class DebtInteractiveBuilder {
  private instance: DebtInteractive;
  
  constructor() {
    this.instance = new DebtInteractive();
  }
  
  customerId(value: string): DebtInteractiveBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtInteractiveBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtInteractiveBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtInteractiveBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: string): DebtInteractiveBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtArpinet {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtArpinetBuilder {
    return new DebtArpinetBuilder();
  }
}

class DebtArpinetBuilder {
  private instance: DebtArpinet;
  
  constructor() {
    this.instance = new DebtArpinet();
  }
  
  customerId(value: string): DebtArpinetBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtArpinetBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtArpinetBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtArpinetBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtArpinetBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: string): DebtArpinetBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtYournet {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtYournetBuilder {
    return new DebtYournetBuilder();
  }
}

class DebtYournetBuilder {
  private instance: DebtYournet;
  
  constructor() {
    this.instance = new DebtYournet();
  }
  
  customerId(value: string): DebtYournetBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtYournetBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtYournetBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtYournetBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtYournetBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: string): DebtYournetBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtCtv {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtCtvBuilder {
    return new DebtCtvBuilder();
  }
}

class DebtCtvBuilder {
  private instance: DebtCtv;
  
  constructor() {
    this.instance = new DebtCtv();
  }
  
  customerId(value: string): DebtCtvBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtCtvBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtCtvBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtCtvBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtCtvBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: string): DebtCtvBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtWater {
  serviceId: number;
  branchId: string;
  id: string;
  customerId: string;
  customerName: string;
  phone: string;
  address: string;
  debtDate: string;
  debt: number;
  debtAmount: number;
  yjDebtAmount: number;
  debtAmountRest: number;
  deposit: number;
  counterUnit: string;
  costUnit: number;
  paidByIdram: number;
  idramPayDate: string;
  prevDebtDate: string;
  prevPaidDate: string;
  prevDebtAmount: number;
  prevPaidAmount: number;
  updated: string;
  
  constructor() {
  }
  
  static build(): DebtWaterBuilder {
    return new DebtWaterBuilder();
  }
}

class DebtWaterBuilder {
  private instance: DebtWater;
  
  constructor() {
    this.instance = new DebtWater();
  }
  
  serviceId(value: number): DebtWaterBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  branchId(value: string): DebtWaterBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  id(value: string): DebtWaterBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): DebtWaterBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtWaterBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  phone(value: string): DebtWaterBuilder {
    this.instance.phone = value;
    return this;
  }
  
  address(value: string): DebtWaterBuilder {
    this.instance.address = value;
    return this;
  }
  
  debtDate(value: string): DebtWaterBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debt(value: number): DebtWaterBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtAmount(value: number): DebtWaterBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  yjDebtAmount(value: number): DebtWaterBuilder {
    this.instance.yjDebtAmount = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtWaterBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  deposit(value: number): DebtWaterBuilder {
    this.instance.deposit = value;
    return this;
  }
  
  counterUnit(value: string): DebtWaterBuilder {
    this.instance.counterUnit = value;
    return this;
  }
  
  costUnit(value: number): DebtWaterBuilder {
    this.instance.costUnit = value;
    return this;
  }
  
  paidByIdram(value: number): DebtWaterBuilder {
    this.instance.paidByIdram = value;
    return this;
  }
  
  idramPayDate(value: string): DebtWaterBuilder {
    this.instance.idramPayDate = value;
    return this;
  }
  
  prevDebtDate(value: string): DebtWaterBuilder {
    this.instance.prevDebtDate = value;
    return this;
  }
  
  prevPaidDate(value: string): DebtWaterBuilder {
    this.instance.prevPaidDate = value;
    return this;
  }
  
  prevDebtAmount(value: number): DebtWaterBuilder {
    this.instance.prevDebtAmount = value;
    return this;
  }
  
  prevPaidAmount(value: number): DebtWaterBuilder {
    this.instance.prevPaidAmount = value;
    return this;
  }
  
  updated(value: string): DebtWaterBuilder {
    this.instance.updated = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Invoice {
  number: string;
  
  constructor() {
  }
  
  static build(): InvoiceBuilder {
    return new InvoiceBuilder();
  }
}

class InvoiceBuilder {
  private instance: Invoice;
  
  constructor() {
    this.instance = new Invoice();
  }
  
  number(value: string): InvoiceBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AmountCurrency {
  amount: number;
  currency: string;
  
  constructor() {
  }
  
  static build(): AmountCurrencyBuilder {
    return new AmountCurrencyBuilder();
  }
}

class AmountCurrencyBuilder {
  private instance: AmountCurrency;
  
  constructor() {
    this.instance = new AmountCurrency();
  }
  
  amount(value: number): AmountCurrencyBuilder {
    this.instance.amount = value;
    return this;
  }
  
  currency(value: string): AmountCurrencyBuilder {
    this.instance.currency = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Term {
  value: number;
  unit: string;
  
  constructor() {
  }
  
  static build(): TermBuilder {
    return new TermBuilder();
  }
}

class TermBuilder {
  private instance: Term;
  
  constructor() {
    this.instance = new Term();
  }
  
  value(value: number): TermBuilder {
    this.instance.value = value;
    return this;
  }
  
  unit(value: string): TermBuilder {
    this.instance.unit = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Rollover {
  invoice: Invoice;
  feeAmount: AmountCurrency;
  extensionFee: AmountCurrency;
  debtPayback: AmountCurrency;
  term: Term;
  newEndDate: string;
  
  constructor() {
  }
  
  static build(): RolloverBuilder {
    return new RolloverBuilder();
  }
}

class RolloverBuilder {
  private instance: Rollover;
  
  constructor() {
    this.instance = new Rollover();
  }
  
  invoice(value: Invoice): RolloverBuilder {
    this.instance.invoice = value;
    return this;
  }
  
  feeAmount(value: AmountCurrency): RolloverBuilder {
    this.instance.feeAmount = value;
    return this;
  }
  
  extensionFee(value: AmountCurrency): RolloverBuilder {
    this.instance.extensionFee = value;
    return this;
  }
  
  debtPayback(value: AmountCurrency): RolloverBuilder {
    this.instance.debtPayback = value;
    return this;
  }
  
  term(value: Term): RolloverBuilder {
    this.instance.term = value;
    return this;
  }
  
  newEndDate(value: string): RolloverBuilder {
    this.instance.newEndDate = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtLoan {
  agreementCode: string;
  code: string;
  customerName: string;
  penaltyAmount: number;
  capitalAmount: number;
  nextPayment: number;
  nextPaymentStr: string;
  overdueFee: string;
  overdueInterest: string;
  overduePrincipal: string;
  nearFee: string;
  nearInterest: string;
  nearPrincipal: string;
  nextPaymentDate: string;
  debt: number;
  customerId: string;
  debtDate: string;
  passport: string;
  repayType: string;
  commission: string;
  nextRollover: Array<Rollover>;
  amountRange: AmountRange;
  updated: string;
  isInBox: string;
  memoId: string;
  dayLeft: number;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtLoanBuilder {
    return new DebtLoanBuilder();
  }
}

class DebtLoanBuilder {
  private instance: DebtLoan;
  
  constructor() {
    this.instance = new DebtLoan();
  }
  
  agreementCode(value: string): DebtLoanBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  code(value: string): DebtLoanBuilder {
    this.instance.code = value;
    return this;
  }
  
  customerName(value: string): DebtLoanBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  penaltyAmount(value: number): DebtLoanBuilder {
    this.instance.penaltyAmount = value;
    return this;
  }
  
  capitalAmount(value: number): DebtLoanBuilder {
    this.instance.capitalAmount = value;
    return this;
  }
  
  nextPayment(value: number): DebtLoanBuilder {
    this.instance.nextPayment = value;
    return this;
  }
  
  nextPaymentStr(value: string): DebtLoanBuilder {
    this.instance.nextPaymentStr = value;
    return this;
  }
  
  overdueFee(value: string): DebtLoanBuilder {
    this.instance.overdueFee = value;
    return this;
  }
  
  overdueInterest(value: string): DebtLoanBuilder {
    this.instance.overdueInterest = value;
    return this;
  }
  
  overduePrincipal(value: string): DebtLoanBuilder {
    this.instance.overduePrincipal = value;
    return this;
  }
  
  nearFee(value: string): DebtLoanBuilder {
    this.instance.nearFee = value;
    return this;
  }
  
  nearInterest(value: string): DebtLoanBuilder {
    this.instance.nearInterest = value;
    return this;
  }
  
  nearPrincipal(value: string): DebtLoanBuilder {
    this.instance.nearPrincipal = value;
    return this;
  }
  
  nextPaymentDate(value: string): DebtLoanBuilder {
    this.instance.nextPaymentDate = value;
    return this;
  }
  
  debt(value: number): DebtLoanBuilder {
    this.instance.debt = value;
    return this;
  }
  
  customerId(value: string): DebtLoanBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  debtDate(value: string): DebtLoanBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  passport(value: string): DebtLoanBuilder {
    this.instance.passport = value;
    return this;
  }
  
  repayType(value: string): DebtLoanBuilder {
    this.instance.repayType = value;
    return this;
  }
  
  commission(value: string): DebtLoanBuilder {
    this.instance.commission = value;
    return this;
  }
  
  nextRollover(value: Array<Rollover>): DebtLoanBuilder {
    this.instance.nextRollover = value;
    return this;
  }
  
  amountRange(value: AmountRange): DebtLoanBuilder {
    this.instance.amountRange = value;
    return this;
  }
  
  updated(value: string): DebtLoanBuilder {
    this.instance.updated = value;
    return this;
  }
  
  isInBox(value: string): DebtLoanBuilder {
    this.instance.isInBox = value;
    return this;
  }
  
  memoId(value: string): DebtLoanBuilder {
    this.instance.memoId = value;
    return this;
  }
  
  dayLeft(value: number): DebtLoanBuilder {
    this.instance.dayLeft = value;
    return this;
  }
  
  serviceId(value: string): DebtLoanBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BetResponse {
  serviceId: number;
  opCode: number;
  opDesc: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): BetResponseBuilder {
    return new BetResponseBuilder();
  }
}

class BetResponseBuilder {
  private instance: BetResponse;
  
  constructor() {
    this.instance = new BetResponse();
  }
  
  serviceId(value: number): BetResponseBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  opCode(value: number): BetResponseBuilder {
    this.instance.opCode = value;
    return this;
  }
  
  opDesc(value: string): BetResponseBuilder {
    this.instance.opDesc = value;
    return this;
  }
  
  customerId(value: string): BetResponseBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GoodwinResponse {
  id: string;
  customerId: string;
  customerName: string;
  debtAmount: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: number;
  
  constructor() {
  }
  
  static build(): GoodwinResponseBuilder {
    return new GoodwinResponseBuilder();
  }
}

class GoodwinResponseBuilder {
  private instance: GoodwinResponse;
  
  constructor() {
    this.instance = new GoodwinResponse();
  }
  
  id(value: string): GoodwinResponseBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): GoodwinResponseBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): GoodwinResponseBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debtAmount(value: number): GoodwinResponseBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  debtDate(value: string): GoodwinResponseBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): GoodwinResponseBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: number): GoodwinResponseBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtSocialOk {
  rate: number;
  currency: string;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtSocialOkBuilder {
    return new DebtSocialOkBuilder();
  }
}

class DebtSocialOkBuilder {
  private instance: DebtSocialOk;
  
  constructor() {
    this.instance = new DebtSocialOk();
  }
  
  rate(value: number): DebtSocialOkBuilder {
    this.instance.rate = value;
    return this;
  }
  
  currency(value: string): DebtSocialOkBuilder {
    this.instance.currency = value;
    return this;
  }
  
  customerId(value: string): DebtSocialOkBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtSocialOkBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtSocialOkBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtSocialOkBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: string): DebtSocialOkBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtOnlineGames {
  rate: number;
  currency: string;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  opCode: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtOnlineGamesBuilder {
    return new DebtOnlineGamesBuilder();
  }
}

class DebtOnlineGamesBuilder {
  private instance: DebtOnlineGames;
  
  constructor() {
    this.instance = new DebtOnlineGames();
  }
  
  rate(value: number): DebtOnlineGamesBuilder {
    this.instance.rate = value;
    return this;
  }
  
  currency(value: string): DebtOnlineGamesBuilder {
    this.instance.currency = value;
    return this;
  }
  
  customerId(value: string): DebtOnlineGamesBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtOnlineGamesBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtOnlineGamesBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtOnlineGamesBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  opCode(value: string): DebtOnlineGamesBuilder {
    this.instance.opCode = value;
    return this;
  }
  
  serviceId(value: string): DebtOnlineGamesBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtMaryKay {
  debt: number;
  customerId: string;
  customerName: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtMaryKayBuilder {
    return new DebtMaryKayBuilder();
  }
}

class DebtMaryKayBuilder {
  private instance: DebtMaryKay;
  
  constructor() {
    this.instance = new DebtMaryKay();
  }
  
  debt(value: number): DebtMaryKayBuilder {
    this.instance.debt = value;
    return this;
  }
  
  customerId(value: string): DebtMaryKayBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtMaryKayBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  serviceId(value: string): DebtMaryKayBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtOriflame {
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtOriflameBuilder {
    return new DebtOriflameBuilder();
  }
}

class DebtOriflameBuilder {
  private instance: DebtOriflame;
  
  constructor() {
    this.instance = new DebtOriflame();
  }
  
  customerId(value: string): DebtOriflameBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtOriflameBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtOriflameBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtOriflameBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtOriflameBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: string): DebtOriflameBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtFaberlic {
  customerId: string;
  customerName: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtFaberlicBuilder {
    return new DebtFaberlicBuilder();
  }
}

class DebtFaberlicBuilder {
  private instance: DebtFaberlic;
  
  constructor() {
    this.instance = new DebtFaberlic();
  }
  
  customerId(value: string): DebtFaberlicBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtFaberlicBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  serviceId(value: string): DebtFaberlicBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtNtvPlus {
  rate: number;
  currency: string;
  customerId: string;
  customerName: string;
  debt: number;
  debtDate: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtNtvPlusBuilder {
    return new DebtNtvPlusBuilder();
  }
}

class DebtNtvPlusBuilder {
  private instance: DebtNtvPlus;
  
  constructor() {
    this.instance = new DebtNtvPlus();
  }
  
  rate(value: number): DebtNtvPlusBuilder {
    this.instance.rate = value;
    return this;
  }
  
  currency(value: string): DebtNtvPlusBuilder {
    this.instance.currency = value;
    return this;
  }
  
  customerId(value: string): DebtNtvPlusBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtNtvPlusBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debt(value: number): DebtNtvPlusBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtNtvPlusBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: string): DebtNtvPlusBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtEkeng {
  commission: string;
  customerId: string;
  customerName: string;
  customerLastName: string;
  debt: number;
  debtDate: string;
  debtAmountRest: number;
  serviceId: string;
  amountRange: AmountRange;
  
  constructor() {
  }
  
  static build(): DebtEkengBuilder {
    return new DebtEkengBuilder();
  }
}

class DebtEkengBuilder {
  private instance: DebtEkeng;
  
  constructor() {
    this.instance = new DebtEkeng();
  }
  
  commission(value: string): DebtEkengBuilder {
    this.instance.commission = value;
    return this;
  }
  
  customerId(value: string): DebtEkengBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtEkengBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  customerLastName(value: string): DebtEkengBuilder {
    this.instance.customerLastName = value;
    return this;
  }
  
  debt(value: number): DebtEkengBuilder {
    this.instance.debt = value;
    return this;
  }
  
  debtDate(value: string): DebtEkengBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  debtAmountRest(value: number): DebtEkengBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  serviceId(value: string): DebtEkengBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  amountRange(value: AmountRange): DebtEkengBuilder {
    this.instance.amountRange = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class PoliceDebt {
  serviceId: number;
  customerName: string;
  pinCode: string;
  actNum: string;
  vehicleNumber: string;
  amount: number;
  debtAmount: number;
  debtAmountRest: number;
  amountRange: AmountRange;
  
  constructor() {
  }
  
  static build(): PoliceDebtBuilder {
    return new PoliceDebtBuilder();
  }
}

class PoliceDebtBuilder {
  private instance: PoliceDebt;
  
  constructor() {
    this.instance = new PoliceDebt();
  }
  
  serviceId(value: number): PoliceDebtBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  customerName(value: string): PoliceDebtBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  pinCode(value: string): PoliceDebtBuilder {
    this.instance.pinCode = value;
    return this;
  }
  
  actNum(value: string): PoliceDebtBuilder {
    this.instance.actNum = value;
    return this;
  }
  
  vehicleNumber(value: string): PoliceDebtBuilder {
    this.instance.vehicleNumber = value;
    return this;
  }
  
  amount(value: number): PoliceDebtBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debtAmount(value: number): PoliceDebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  debtAmountRest(value: number): PoliceDebtBuilder {
    this.instance.debtAmountRest = value;
    return this;
  }
  
  amountRange(value: AmountRange): PoliceDebtBuilder {
    this.instance.amountRange = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ParkingFineDebt {
  serviceId: number;
  customerName: string;
  actNum: string;
  number: string;
  amount: number;
  
  constructor() {
  }
  
  static build(): ParkingFineDebtBuilder {
    return new ParkingFineDebtBuilder();
  }
}

class ParkingFineDebtBuilder {
  private instance: ParkingFineDebt;
  
  constructor() {
    this.instance = new ParkingFineDebt();
  }
  
  serviceId(value: number): ParkingFineDebtBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  customerName(value: string): ParkingFineDebtBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  actNum(value: string): ParkingFineDebtBuilder {
    this.instance.actNum = value;
    return this;
  }
  
  number(value: string): ParkingFineDebtBuilder {
    this.instance.number = value;
    return this;
  }
  
  amount(value: number): ParkingFineDebtBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class LoanInfo {
  agreementCode: string;
  customerName: string;
  penaltyAmount: number;
  capitalAmount: number;
  nextPayment: number;
  overdueFee: string;
  overdueInterest: string;
  overduePrincipal: string;
  nearFee: string;
  nearInterest: string;
  nearPrincipal: string;
  nextPaymentDate: string;
  debtAmount: number;
  customerId: string;
  deptDate: string;
  passport: string;
  repayType: boolean;
  commission: number;
  nextRollover: string;
  amountRange: AmountRange;
  updated: boolean;
  opCode: number;
  opDescription: string;
  currency: string;
  serviceId: number;
  
  constructor() {
  }
  
  static build(): LoanInfoBuilder {
    return new LoanInfoBuilder();
  }
}

class LoanInfoBuilder {
  private instance: LoanInfo;
  
  constructor() {
    this.instance = new LoanInfo();
  }
  
  agreementCode(value: string): LoanInfoBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  customerName(value: string): LoanInfoBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  penaltyAmount(value: number): LoanInfoBuilder {
    this.instance.penaltyAmount = value;
    return this;
  }
  
  capitalAmount(value: number): LoanInfoBuilder {
    this.instance.capitalAmount = value;
    return this;
  }
  
  nextPayment(value: number): LoanInfoBuilder {
    this.instance.nextPayment = value;
    return this;
  }
  
  overdueFee(value: string): LoanInfoBuilder {
    this.instance.overdueFee = value;
    return this;
  }
  
  overdueInterest(value: string): LoanInfoBuilder {
    this.instance.overdueInterest = value;
    return this;
  }
  
  overduePrincipal(value: string): LoanInfoBuilder {
    this.instance.overduePrincipal = value;
    return this;
  }
  
  nearFee(value: string): LoanInfoBuilder {
    this.instance.nearFee = value;
    return this;
  }
  
  nearInterest(value: string): LoanInfoBuilder {
    this.instance.nearInterest = value;
    return this;
  }
  
  nearPrincipal(value: string): LoanInfoBuilder {
    this.instance.nearPrincipal = value;
    return this;
  }
  
  nextPaymentDate(value: string): LoanInfoBuilder {
    this.instance.nextPaymentDate = value;
    return this;
  }
  
  debtAmount(value: number): LoanInfoBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  customerId(value: string): LoanInfoBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  deptDate(value: string): LoanInfoBuilder {
    this.instance.deptDate = value;
    return this;
  }
  
  passport(value: string): LoanInfoBuilder {
    this.instance.passport = value;
    return this;
  }
  
  repayType(value: boolean): LoanInfoBuilder {
    this.instance.repayType = value;
    return this;
  }
  
  commission(value: number): LoanInfoBuilder {
    this.instance.commission = value;
    return this;
  }
  
  nextRollover(value: string): LoanInfoBuilder {
    this.instance.nextRollover = value;
    return this;
  }
  
  amountRange(value: AmountRange): LoanInfoBuilder {
    this.instance.amountRange = value;
    return this;
  }
  
  updated(value: boolean): LoanInfoBuilder {
    this.instance.updated = value;
    return this;
  }
  
  opCode(value: number): LoanInfoBuilder {
    this.instance.opCode = value;
    return this;
  }
  
  opDescription(value: string): LoanInfoBuilder {
    this.instance.opDescription = value;
    return this;
  }
  
  currency(value: string): LoanInfoBuilder {
    this.instance.currency = value;
    return this;
  }
  
  serviceId(value: number): LoanInfoBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtCard {
  cardNumber: string;
  cardNumberOriginal: string;
  exist: boolean;
  
  constructor() {
  }
  
  static build(): DebtCardBuilder {
    return new DebtCardBuilder();
  }
}

class DebtCardBuilder {
  private instance: DebtCard;
  
  constructor() {
    this.instance = new DebtCard();
  }
  
  cardNumber(value: string): DebtCardBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  cardNumberOriginal(value: string): DebtCardBuilder {
    this.instance.cardNumberOriginal = value;
    return this;
  }
  
  exist(value: boolean): DebtCardBuilder {
    this.instance.exist = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DebtStroyMasterDamafon {
  customerId: string;
  customerName: string;
  debtAmount: number;
  debtDate: string;
  serviceId: string;
  
  constructor() {
  }
  
  static build(): DebtStroyMasterDamafonBuilder {
    return new DebtStroyMasterDamafonBuilder();
  }
}

class DebtStroyMasterDamafonBuilder {
  private instance: DebtStroyMasterDamafon;
  
  constructor() {
    this.instance = new DebtStroyMasterDamafon();
  }
  
  customerId(value: string): DebtStroyMasterDamafonBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): DebtStroyMasterDamafonBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debtAmount(value: number): DebtStroyMasterDamafonBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  debtDate(value: string): DebtStroyMasterDamafonBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: string): DebtStroyMasterDamafonBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Field {
  name: string;
  value: string;
  
  constructor() {
  }
  
  static build(): FieldBuilder {
    return new FieldBuilder();
  }
}

class FieldBuilder {
  private instance: Field;
  
  constructor() {
    this.instance = new Field();
  }
  
  name(value: string): FieldBuilder {
    this.instance.name = value;
    return this;
  }
  
  value(value: string): FieldBuilder {
    this.instance.value = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Bill {
  fields: Array<Field>;
  
  constructor() {
  }
  
  static build(): BillBuilder {
    return new BillBuilder();
  }
}

class BillBuilder {
  private instance: Bill;
  
  constructor() {
    this.instance = new Bill();
  }
  
  fields(value: Array<Field>): BillBuilder {
    this.instance.fields = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Agreement {
  provider: string;
  fields: Array<Field>;
  
  constructor() {
  }
  
  static build(): AgreementBuilder {
    return new AgreementBuilder();
  }
}

class AgreementBuilder {
  private instance: Agreement;
  
  constructor() {
    this.instance = new Agreement();
  }
  
  provider(value: string): AgreementBuilder {
    this.instance.provider = value;
    return this;
  }
  
  fields(value: Array<Field>): AgreementBuilder {
    this.instance.fields = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class TransactionHistory {
  purpose: string;
  contractNum: string;
  userCode: string;
  paymentId: string;
  date: Date;
  amount: number;
  
  constructor() {
  }
  
  static build(): TransactionHistoryBuilder {
    return new TransactionHistoryBuilder();
  }
}

class TransactionHistoryBuilder {
  private instance: TransactionHistory;
  
  constructor() {
    this.instance = new TransactionHistory();
  }
  
  purpose(value: string): TransactionHistoryBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  contractNum(value: string): TransactionHistoryBuilder {
    this.instance.contractNum = value;
    return this;
  }
  
  userCode(value: string): TransactionHistoryBuilder {
    this.instance.userCode = value;
    return this;
  }
  
  paymentId(value: string): TransactionHistoryBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  date(value: Date): TransactionHistoryBuilder {
    this.instance.date = value;
    return this;
  }
  
  amount(value: number): TransactionHistoryBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ASTransactionData {
  completed: Date;
  outerAccDebet: string;
  accDebet: string;
  amount: number;
  outerAccCredit: string;
  accCredit: string;
  aim: string;
  udrNum: string;
  branchCode: string;
  
  constructor() {
  }
  
  static build(): ASTransactionDataBuilder {
    return new ASTransactionDataBuilder();
  }
}

class ASTransactionDataBuilder {
  private instance: ASTransactionData;
  
  constructor() {
    this.instance = new ASTransactionData();
  }
  
  completed(value: Date): ASTransactionDataBuilder {
    this.instance.completed = value;
    return this;
  }
  
  outerAccDebet(value: string): ASTransactionDataBuilder {
    this.instance.outerAccDebet = value;
    return this;
  }
  
  accDebet(value: string): ASTransactionDataBuilder {
    this.instance.accDebet = value;
    return this;
  }
  
  amount(value: number): ASTransactionDataBuilder {
    this.instance.amount = value;
    return this;
  }
  
  outerAccCredit(value: string): ASTransactionDataBuilder {
    this.instance.outerAccCredit = value;
    return this;
  }
  
  accCredit(value: string): ASTransactionDataBuilder {
    this.instance.accCredit = value;
    return this;
  }
  
  aim(value: string): ASTransactionDataBuilder {
    this.instance.aim = value;
    return this;
  }
  
  udrNum(value: string): ASTransactionDataBuilder {
    this.instance.udrNum = value;
    return this;
  }
  
  branchCode(value: string): ASTransactionDataBuilder {
    this.instance.branchCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ASCustomerData {
  customerId: string;
  created: Date;
  
  constructor() {
  }
  
  static build(): ASCustomerDataBuilder {
    return new ASCustomerDataBuilder();
  }
}

class ASCustomerDataBuilder {
  private instance: ASCustomerData;
  
  constructor() {
    this.instance = new ASCustomerData();
  }
  
  customerId(value: string): ASCustomerDataBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  created(value: Date): ASCustomerDataBuilder {
    this.instance.created = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ASCustomerDataUpdate {
  customerId: string;
  accnote: string;
  accnote2: string;
  udrChngDate: Date;
  
  constructor() {
  }
  
  static build(): ASCustomerDataUpdateBuilder {
    return new ASCustomerDataUpdateBuilder();
  }
}

class ASCustomerDataUpdateBuilder {
  private instance: ASCustomerDataUpdate;
  
  constructor() {
    this.instance = new ASCustomerDataUpdate();
  }
  
  customerId(value: string): ASCustomerDataUpdateBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  accnote(value: string): ASCustomerDataUpdateBuilder {
    this.instance.accnote = value;
    return this;
  }
  
  accnote2(value: string): ASCustomerDataUpdateBuilder {
    this.instance.accnote2 = value;
    return this;
  }
  
  udrChngDate(value: Date): ASCustomerDataUpdateBuilder {
    this.instance.udrChngDate = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxInOut {
  cashboxId: string;
  amountIn: number;
  amountOut: number;
  amountPaymentIn: number;
  amountCashlessOut: number;
  
  constructor() {
  }
  
  static build(): CashboxInOutBuilder {
    return new CashboxInOutBuilder();
  }
}

class CashboxInOutBuilder {
  private instance: CashboxInOut;
  
  constructor() {
    this.instance = new CashboxInOut();
  }
  
  cashboxId(value: string): CashboxInOutBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  amountIn(value: number): CashboxInOutBuilder {
    this.instance.amountIn = value;
    return this;
  }
  
  amountOut(value: number): CashboxInOutBuilder {
    this.instance.amountOut = value;
    return this;
  }
  
  amountPaymentIn(value: number): CashboxInOutBuilder {
    this.instance.amountPaymentIn = value;
    return this;
  }
  
  amountCashlessOut(value: number): CashboxInOutBuilder {
    this.instance.amountCashlessOut = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export enum ASImportStatus {
  NOT_IMPORTED = "NOT_IMPORTED",
  IMPORTED = "IMPORTED",
}

export enum ClaimStatus {
  OPENED = "OPENED",
  DISMISSED = "DISMISSED",
  ACCEPTED = "ACCEPTED",
  PRE_APPROVED = "PRE_APPROVED",
  PRE_REJECTED = "PRE_REJECTED",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
  COMPLETED_APPROVED = "COMPLETED_APPROVED",
  COMPLETED_REJECTED = "COMPLETED_REJECTED",
  PRIMARY_APPROVED = "PRIMARY_APPROVED",
  FINALLY_APPROVED = "FINALLY_APPROVED",
}

export enum ClaimType {
  CANCELLATION = "CANCELLATION",
  UNFREEZE = "UNFREEZE",
}

export enum CustomerClaimType {
  CANCELLATION = "CANCELLATION",
}

export enum CancellationClaimType {
  OPERATOR = "OPERATOR",
  CUSTOMER = "CUSTOMER",
}

export enum OperatorClaimType {
  WRONG_AMOUNT = "WRONG_AMOUNT",
  WRONG_CUSTOMER = "WRONG_CUSTOMER",
  WRONG_SERVICE = "WRONG_SERVICE",
  APP_PROBLEM = "APP_PROBLEM",
}

// @dynamic
export class TransactionDetails {
  paymentId: string;
  receiptId: string;
  issuer: string;
  issued: Date;
  completed: Date;
  operatorId: string;
  operatorName: string;
  cashboxId: string;
  branchId: string;
  branchName: string;
  providerId: string;
  providerName: string;
  purpose: string;
  amount: number;
  commission: number;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  userNum: string;
  contactNum: string;
  additionalData: string;
  
  constructor() {
  }
  
  static build(): TransactionDetailsBuilder {
    return new TransactionDetailsBuilder();
  }
}

class TransactionDetailsBuilder {
  private instance: TransactionDetails;
  
  constructor() {
    this.instance = new TransactionDetails();
  }
  
  paymentId(value: string): TransactionDetailsBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  receiptId(value: string): TransactionDetailsBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  issuer(value: string): TransactionDetailsBuilder {
    this.instance.issuer = value;
    return this;
  }
  
  issued(value: Date): TransactionDetailsBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): TransactionDetailsBuilder {
    this.instance.completed = value;
    return this;
  }
  
  operatorId(value: string): TransactionDetailsBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  operatorName(value: string): TransactionDetailsBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  cashboxId(value: string): TransactionDetailsBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): TransactionDetailsBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchName(value: string): TransactionDetailsBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  providerId(value: string): TransactionDetailsBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  providerName(value: string): TransactionDetailsBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  purpose(value: string): TransactionDetailsBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  amount(value: number): TransactionDetailsBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): TransactionDetailsBuilder {
    this.instance.commission = value;
    return this;
  }
  
  debt(value: number): TransactionDetailsBuilder {
    this.instance.debt = value;
    return this;
  }
  
  destinationWalletId(value: string): TransactionDetailsBuilder {
    this.instance.destinationWalletId = value;
    return this;
  }
  
  commissionAmount(value: number): TransactionDetailsBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  comment(value: string): TransactionDetailsBuilder {
    this.instance.comment = value;
    return this;
  }
  
  userNum(value: string): TransactionDetailsBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  contactNum(value: string): TransactionDetailsBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  additionalData(value: string): TransactionDetailsBuilder {
    this.instance.additionalData = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimDetails {
  transaction: TransactionDetails;
  attachment: string;
  description: string;
  examinationNotes: string;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): ClaimDetailsBuilder {
    return new ClaimDetailsBuilder();
  }
}

class ClaimDetailsBuilder {
  private instance: ClaimDetails;
  
  constructor() {
    this.instance = new ClaimDetails();
  }
  
  transaction(value: TransactionDetails): ClaimDetailsBuilder {
    this.instance.transaction = value;
    return this;
  }
  
  attachment(value: string): ClaimDetailsBuilder {
    this.instance.attachment = value;
    return this;
  }
  
  description(value: string): ClaimDetailsBuilder {
    this.instance.description = value;
    return this;
  }
  
  examinationNotes(value: string): ClaimDetailsBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  status(value: ClaimStatus): ClaimDetailsBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class IdramWalletResponse {
  amountRange: AmountRange;
  customerId: string;
  customerName: string;
  debtAmount: number;
  debtDate: string;
  serviceId: number;
  
  constructor() {
  }
  
  static build(): IdramWalletResponseBuilder {
    return new IdramWalletResponseBuilder();
  }
}

class IdramWalletResponseBuilder {
  private instance: IdramWalletResponse;
  
  constructor() {
    this.instance = new IdramWalletResponse();
  }
  
  amountRange(value: AmountRange): IdramWalletResponseBuilder {
    this.instance.amountRange = value;
    return this;
  }
  
  customerId(value: string): IdramWalletResponseBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerName(value: string): IdramWalletResponseBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  debtAmount(value: number): IdramWalletResponseBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  debtDate(value: string): IdramWalletResponseBuilder {
    this.instance.debtDate = value;
    return this;
  }
  
  serviceId(value: number): IdramWalletResponseBuilder {
    this.instance.serviceId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export enum OrderState {
  PENDING = "PENDING",
  PAID = "PAID",
  FAILED = "FAILED",
}

export enum CardState {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  DISABLED = "DISABLED",
  FAILED = "FAILED",
}

// @dynamic
export class CardInfo {
  id: string;
  state: CardState;
  createdAt: Date;
  boundAt: Date;
  bank: string;
  number: string;
  holder: string;
  expiration: string;
  
  constructor() {
  }
  
  static build(): CardInfoBuilder {
    return new CardInfoBuilder();
  }
}

class CardInfoBuilder {
  private instance: CardInfo;
  
  constructor() {
    this.instance = new CardInfo();
  }
  
  id(value: string): CardInfoBuilder {
    this.instance.id = value;
    return this;
  }
  
  state(value: CardState): CardInfoBuilder {
    this.instance.state = value;
    return this;
  }
  
  createdAt(value: Date): CardInfoBuilder {
    this.instance.createdAt = value;
    return this;
  }
  
  boundAt(value: Date): CardInfoBuilder {
    this.instance.boundAt = value;
    return this;
  }
  
  bank(value: string): CardInfoBuilder {
    this.instance.bank = value;
    return this;
  }
  
  number(value: string): CardInfoBuilder {
    this.instance.number = value;
    return this;
  }
  
  holder(value: string): CardInfoBuilder {
    this.instance.holder = value;
    return this;
  }
  
  expiration(value: string): CardInfoBuilder {
    this.instance.expiration = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class UcomFixedCustomer {
  name: string;
  number: string;
  customerId: string;
  address: string;
  
  constructor() {
  }
  
  static build(): UcomFixedCustomerBuilder {
    return new UcomFixedCustomerBuilder();
  }
}

class UcomFixedCustomerBuilder {
  private instance: UcomFixedCustomer;
  
  constructor() {
    this.instance = new UcomFixedCustomer();
  }
  
  name(value: string): UcomFixedCustomerBuilder {
    this.instance.name = value;
    return this;
  }
  
  number(value: string): UcomFixedCustomerBuilder {
    this.instance.number = value;
    return this;
  }
  
  customerId(value: string): UcomFixedCustomerBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  address(value: string): UcomFixedCustomerBuilder {
    this.instance.address = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BranchInfo {
  id: string;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
  
  constructor() {
  }
  
  static build(): BranchInfoBuilder {
    return new BranchInfoBuilder();
  }
}

class BranchInfoBuilder {
  private instance: BranchInfo;
  
  constructor() {
    this.instance = new BranchInfo();
  }
  
  id(value: string): BranchInfoBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): BranchInfoBuilder {
    this.instance.name = value;
    return this;
  }
  
  address(value: string): BranchInfoBuilder {
    this.instance.address = value;
    return this;
  }
  
  latitude(value: number): BranchInfoBuilder {
    this.instance.latitude = value;
    return this;
  }
  
  longitude(value: number): BranchInfoBuilder {
    this.instance.longitude = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxReportSummary {
  openingBalance: number;
  balance: number;
  amountIn: number;
  amountOut: number;
  quantity: number;
  
  constructor() {
  }
  
  static build(): CashboxReportSummaryBuilder {
    return new CashboxReportSummaryBuilder();
  }
}

class CashboxReportSummaryBuilder {
  private instance: CashboxReportSummary;
  
  constructor() {
    this.instance = new CashboxReportSummary();
  }
  
  openingBalance(value: number): CashboxReportSummaryBuilder {
    this.instance.openingBalance = value;
    return this;
  }
  
  balance(value: number): CashboxReportSummaryBuilder {
    this.instance.balance = value;
    return this;
  }
  
  amountIn(value: number): CashboxReportSummaryBuilder {
    this.instance.amountIn = value;
    return this;
  }
  
  amountOut(value: number): CashboxReportSummaryBuilder {
    this.instance.amountOut = value;
    return this;
  }
  
  quantity(value: number): CashboxReportSummaryBuilder {
    this.instance.quantity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class TransactionReportSummary {
  amount: number;
  commission: number;
  quantity: number;
  
  constructor() {
  }
  
  static build(): TransactionReportSummaryBuilder {
    return new TransactionReportSummaryBuilder();
  }
}

class TransactionReportSummaryBuilder {
  private instance: TransactionReportSummary;
  
  constructor() {
    this.instance = new TransactionReportSummary();
  }
  
  amount(value: number): TransactionReportSummaryBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): TransactionReportSummaryBuilder {
    this.instance.commission = value;
    return this;
  }
  
  quantity(value: number): TransactionReportSummaryBuilder {
    this.instance.quantity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BranchName {
  id: string;
  name: string;
  code: string;
  
  constructor() {
  }
  
  static build(): BranchNameBuilder {
    return new BranchNameBuilder();
  }
}

class BranchNameBuilder {
  private instance: BranchName;
  
  constructor() {
    this.instance = new BranchName();
  }
  
  id(value: string): BranchNameBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): BranchNameBuilder {
    this.instance.name = value;
    return this;
  }
  
  code(value: string): BranchNameBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxName {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): CashboxNameBuilder {
    return new CashboxNameBuilder();
  }
}

class CashboxNameBuilder {
  private instance: CashboxName;
  
  constructor() {
    this.instance = new CashboxName();
  }
  
  id(value: string): CashboxNameBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): CashboxNameBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class PartnerReport {
  id: string;
  providerName: string;
  cashboxName: string;
  cashboxId: string;
  branchId: string;
  date: Date;
  amount: number;
  commission: number;
  quantity: number;
  
  constructor() {
  }
  
  static build(): PartnerReportBuilder {
    return new PartnerReportBuilder();
  }
}

class PartnerReportBuilder {
  private instance: PartnerReport;
  
  constructor() {
    this.instance = new PartnerReport();
  }
  
  id(value: string): PartnerReportBuilder {
    this.instance.id = value;
    return this;
  }
  
  providerName(value: string): PartnerReportBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  cashboxName(value: string): PartnerReportBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  cashboxId(value: string): PartnerReportBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): PartnerReportBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  date(value: Date): PartnerReportBuilder {
    this.instance.date = value;
    return this;
  }
  
  amount(value: number): PartnerReportBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): PartnerReportBuilder {
    this.instance.commission = value;
    return this;
  }
  
  quantity(value: number): PartnerReportBuilder {
    this.instance.quantity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AcbaPartnerReport {
  date: Date;
  type: string;
  note: string;
  amount: number;
  commission: number;
  userNum: string;
  wallerId: string;
  name: string;
  
  constructor() {
  }
  
  static build(): AcbaPartnerReportBuilder {
    return new AcbaPartnerReportBuilder();
  }
}

class AcbaPartnerReportBuilder {
  private instance: AcbaPartnerReport;
  
  constructor() {
    this.instance = new AcbaPartnerReport();
  }
  
  date(value: Date): AcbaPartnerReportBuilder {
    this.instance.date = value;
    return this;
  }
  
  type(value: string): AcbaPartnerReportBuilder {
    this.instance.type = value;
    return this;
  }
  
  note(value: string): AcbaPartnerReportBuilder {
    this.instance.note = value;
    return this;
  }
  
  amount(value: number): AcbaPartnerReportBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): AcbaPartnerReportBuilder {
    this.instance.commission = value;
    return this;
  }
  
  userNum(value: string): AcbaPartnerReportBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  wallerId(value: string): AcbaPartnerReportBuilder {
    this.instance.wallerId = value;
    return this;
  }
  
  name(value: string): AcbaPartnerReportBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class EvocaPartnerReport {
  terminalId: string;
  receiptId: string;
  date: Date;
  amount: number;
  recipient: string;
  type: string;
  
  constructor() {
  }
  
  static build(): EvocaPartnerReportBuilder {
    return new EvocaPartnerReportBuilder();
  }
}

class EvocaPartnerReportBuilder {
  private instance: EvocaPartnerReport;
  
  constructor() {
    this.instance = new EvocaPartnerReport();
  }
  
  terminalId(value: string): EvocaPartnerReportBuilder {
    this.instance.terminalId = value;
    return this;
  }
  
  receiptId(value: string): EvocaPartnerReportBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  date(value: Date): EvocaPartnerReportBuilder {
    this.instance.date = value;
    return this;
  }
  
  amount(value: number): EvocaPartnerReportBuilder {
    this.instance.amount = value;
    return this;
  }
  
  recipient(value: string): EvocaPartnerReportBuilder {
    this.instance.recipient = value;
    return this;
  }
  
  type(value: string): EvocaPartnerReportBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GoodCreditPartnerReport {
  date: Date;
  amount: number;
  contract: string;
  cashboxName: string;
  
  constructor() {
  }
  
  static build(): GoodCreditPartnerReportBuilder {
    return new GoodCreditPartnerReportBuilder();
  }
}

class GoodCreditPartnerReportBuilder {
  private instance: GoodCreditPartnerReport;
  
  constructor() {
    this.instance = new GoodCreditPartnerReport();
  }
  
  date(value: Date): GoodCreditPartnerReportBuilder {
    this.instance.date = value;
    return this;
  }
  
  amount(value: number): GoodCreditPartnerReportBuilder {
    this.instance.amount = value;
    return this;
  }
  
  contract(value: string): GoodCreditPartnerReportBuilder {
    this.instance.contract = value;
    return this;
  }
  
  cashboxName(value: string): GoodCreditPartnerReportBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GlobalCreditPartnerReport {
  date: Date;
  contract: string;
  amount: number;
  receiptId: string;
  order: string;
  cashboxName: string;
  operatorName: string;
  
  constructor() {
  }
  
  static build(): GlobalCreditPartnerReportBuilder {
    return new GlobalCreditPartnerReportBuilder();
  }
}

class GlobalCreditPartnerReportBuilder {
  private instance: GlobalCreditPartnerReport;
  
  constructor() {
    this.instance = new GlobalCreditPartnerReport();
  }
  
  date(value: Date): GlobalCreditPartnerReportBuilder {
    this.instance.date = value;
    return this;
  }
  
  contract(value: string): GlobalCreditPartnerReportBuilder {
    this.instance.contract = value;
    return this;
  }
  
  amount(value: number): GlobalCreditPartnerReportBuilder {
    this.instance.amount = value;
    return this;
  }
  
  receiptId(value: string): GlobalCreditPartnerReportBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  order(value: string): GlobalCreditPartnerReportBuilder {
    this.instance.order = value;
    return this;
  }
  
  cashboxName(value: string): GlobalCreditPartnerReportBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  operatorName(value: string): GlobalCreditPartnerReportBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class PartnerReportSummary {
  amount: number;
  commission: number;
  quantity: number;
  
  constructor() {
  }
  
  static build(): PartnerReportSummaryBuilder {
    return new PartnerReportSummaryBuilder();
  }
}

class PartnerReportSummaryBuilder {
  private instance: PartnerReportSummary;
  
  constructor() {
    this.instance = new PartnerReportSummary();
  }
  
  amount(value: number): PartnerReportSummaryBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): PartnerReportSummaryBuilder {
    this.instance.commission = value;
    return this;
  }
  
  quantity(value: number): PartnerReportSummaryBuilder {
    this.instance.quantity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmSoftBankContract {
  amount: number;
  code: string;
  currency: string;
  feeCurrency: string;
  name: string;
  signDate: string;
  type: string;
  
  constructor() {
  }
  
  static build(): ArmSoftBankContractBuilder {
    return new ArmSoftBankContractBuilder();
  }
}

class ArmSoftBankContractBuilder {
  private instance: ArmSoftBankContract;
  
  constructor() {
    this.instance = new ArmSoftBankContract();
  }
  
  amount(value: number): ArmSoftBankContractBuilder {
    this.instance.amount = value;
    return this;
  }
  
  code(value: string): ArmSoftBankContractBuilder {
    this.instance.code = value;
    return this;
  }
  
  currency(value: string): ArmSoftBankContractBuilder {
    this.instance.currency = value;
    return this;
  }
  
  feeCurrency(value: string): ArmSoftBankContractBuilder {
    this.instance.feeCurrency = value;
    return this;
  }
  
  name(value: string): ArmSoftBankContractBuilder {
    this.instance.name = value;
    return this;
  }
  
  signDate(value: string): ArmSoftBankContractBuilder {
    this.instance.signDate = value;
    return this;
  }
  
  type(value: string): ArmSoftBankContractBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmSoftContractDebt {
  code: string;
  name: string;
  debtAmount: number;
  principalRem: string;
  penAmounts: string;
  overdueFee: string;
  overdueInterest: string;
  overduePrincipal: string;
  nearRepayDate: string;
  nearFee: string;
  nearInterest: string;
  nearPrincipal: string;
  
  constructor() {
  }
  
  static build(): ArmSoftContractDebtBuilder {
    return new ArmSoftContractDebtBuilder();
  }
}

class ArmSoftContractDebtBuilder {
  private instance: ArmSoftContractDebt;
  
  constructor() {
    this.instance = new ArmSoftContractDebt();
  }
  
  code(value: string): ArmSoftContractDebtBuilder {
    this.instance.code = value;
    return this;
  }
  
  name(value: string): ArmSoftContractDebtBuilder {
    this.instance.name = value;
    return this;
  }
  
  debtAmount(value: number): ArmSoftContractDebtBuilder {
    this.instance.debtAmount = value;
    return this;
  }
  
  principalRem(value: string): ArmSoftContractDebtBuilder {
    this.instance.principalRem = value;
    return this;
  }
  
  penAmounts(value: string): ArmSoftContractDebtBuilder {
    this.instance.penAmounts = value;
    return this;
  }
  
  overdueFee(value: string): ArmSoftContractDebtBuilder {
    this.instance.overdueFee = value;
    return this;
  }
  
  overdueInterest(value: string): ArmSoftContractDebtBuilder {
    this.instance.overdueInterest = value;
    return this;
  }
  
  overduePrincipal(value: string): ArmSoftContractDebtBuilder {
    this.instance.overduePrincipal = value;
    return this;
  }
  
  nearRepayDate(value: string): ArmSoftContractDebtBuilder {
    this.instance.nearRepayDate = value;
    return this;
  }
  
  nearFee(value: string): ArmSoftContractDebtBuilder {
    this.instance.nearFee = value;
    return this;
  }
  
  nearInterest(value: string): ArmSoftContractDebtBuilder {
    this.instance.nearInterest = value;
    return this;
  }
  
  nearPrincipal(value: string): ArmSoftContractDebtBuilder {
    this.instance.nearPrincipal = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmSoftBankContractDebts {
  contractDebt: ArmSoftContractDebt;
  errorDescription: string;
  listOfContracts: Array<ArmSoftBankContract>;
  signature: string;
  
  constructor() {
  }
  
  static build(): ArmSoftBankContractDebtsBuilder {
    return new ArmSoftBankContractDebtsBuilder();
  }
}

class ArmSoftBankContractDebtsBuilder {
  private instance: ArmSoftBankContractDebts;
  
  constructor() {
    this.instance = new ArmSoftBankContractDebts();
  }
  
  contractDebt(value: ArmSoftContractDebt): ArmSoftBankContractDebtsBuilder {
    this.instance.contractDebt = value;
    return this;
  }
  
  errorDescription(value: string): ArmSoftBankContractDebtsBuilder {
    this.instance.errorDescription = value;
    return this;
  }
  
  listOfContracts(value: Array<ArmSoftBankContract>): ArmSoftBankContractDebtsBuilder {
    this.instance.listOfContracts = value;
    return this;
  }
  
  signature(value: string): ArmSoftBankContractDebtsBuilder {
    this.instance.signature = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BlockInfo {
  info: string;
  
  constructor() {
  }
  
  static build(): BlockInfoBuilder {
    return new BlockInfoBuilder();
  }
}

class BlockInfoBuilder {
  private instance: BlockInfo;
  
  constructor() {
    this.instance = new BlockInfo();
  }
  
  info(value: string): BlockInfoBuilder {
    this.instance.info = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class PhoneInfo {
  phone: string;
  mobile: string;
  
  constructor() {
  }
  
  static build(): PhoneInfoBuilder {
    return new PhoneInfoBuilder();
  }
}

class PhoneInfoBuilder {
  private instance: PhoneInfo;
  
  constructor() {
    this.instance = new PhoneInfo();
  }
  
  phone(value: string): PhoneInfoBuilder {
    this.instance.phone = value;
    return this;
  }
  
  mobile(value: string): PhoneInfoBuilder {
    this.instance.mobile = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmSoftBankCheckAccount {
  errorDescription: string;
  name: string;
  cur: string;
  rate: string;
  blockInfo: BlockInfo;
  phoneInfo: PhoneInfo;
  accountNumber: string;
  
  constructor() {
  }
  
  static build(): ArmSoftBankCheckAccountBuilder {
    return new ArmSoftBankCheckAccountBuilder();
  }
}

class ArmSoftBankCheckAccountBuilder {
  private instance: ArmSoftBankCheckAccount;
  
  constructor() {
    this.instance = new ArmSoftBankCheckAccount();
  }
  
  errorDescription(value: string): ArmSoftBankCheckAccountBuilder {
    this.instance.errorDescription = value;
    return this;
  }
  
  name(value: string): ArmSoftBankCheckAccountBuilder {
    this.instance.name = value;
    return this;
  }
  
  cur(value: string): ArmSoftBankCheckAccountBuilder {
    this.instance.cur = value;
    return this;
  }
  
  rate(value: string): ArmSoftBankCheckAccountBuilder {
    this.instance.rate = value;
    return this;
  }
  
  blockInfo(value: BlockInfo): ArmSoftBankCheckAccountBuilder {
    this.instance.blockInfo = value;
    return this;
  }
  
  phoneInfo(value: PhoneInfo): ArmSoftBankCheckAccountBuilder {
    this.instance.phoneInfo = value;
    return this;
  }
  
  accountNumber(value: string): ArmSoftBankCheckAccountBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerPeerInfo {
  number: string;
  forename: string;
  surname: string;
  userType: UserType;
  
  constructor() {
  }
  
  static build(): CustomerPeerInfoBuilder {
    return new CustomerPeerInfoBuilder();
  }
}

class CustomerPeerInfoBuilder {
  private instance: CustomerPeerInfo;
  
  constructor() {
    this.instance = new CustomerPeerInfo();
  }
  
  number(value: string): CustomerPeerInfoBuilder {
    this.instance.number = value;
    return this;
  }
  
  forename(value: string): CustomerPeerInfoBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): CustomerPeerInfoBuilder {
    this.instance.surname = value;
    return this;
  }
  
  userType(value: UserType): CustomerPeerInfoBuilder {
    this.instance.userType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export enum TransferType {
  WALLET_TO_WALLET = "WALLET_TO_WALLET",
}

export enum DeliveryState {
  DELIVERED = "DELIVERED",
  SENT = "SENT",
  READ = "READ",
  FAILED = "FAILED",
}

export enum TransferState {
  PENDING = "PENDING",
  AMOUNT_HELD = "AMOUNT_HELD",
  SUCCEEDED = "SUCCEEDED",
  FAILED = "FAILED",
}

export enum AcbaPaymentStatus {
  COMPLETED = "COMPLETED",
  PENDING = "PENDING",
  FAILED = "FAILED",
  AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED",
  OPERATION_UNAVAILABLE = "OPERATION_UNAVAILABLE",
}

export enum AcbaLoanIdentifierType {
  ACC_NUM = "ACC_NUM",
  CODE = "CODE",
}

// @dynamic
export class ArdshinProduct {
  cardNumber: string;
  forenameEn: string;
  forenameAm: string;
  forenameRu: string;
  surnameEn: string;
  surnameAm: string;
  surnameRu: string;
  birthDay: string;
  accountNumber: string;
  currencyCode: string;
  currRate: string;
  creditId: string;
  repDate: string;
  repAmount: string;
  creditBalance: string;
  warningText: string;
  
  constructor() {
  }
  
  static build(): ArdshinProductBuilder {
    return new ArdshinProductBuilder();
  }
}

class ArdshinProductBuilder {
  private instance: ArdshinProduct;
  
  constructor() {
    this.instance = new ArdshinProduct();
  }
  
  cardNumber(value: string): ArdshinProductBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  forenameEn(value: string): ArdshinProductBuilder {
    this.instance.forenameEn = value;
    return this;
  }
  
  forenameAm(value: string): ArdshinProductBuilder {
    this.instance.forenameAm = value;
    return this;
  }
  
  forenameRu(value: string): ArdshinProductBuilder {
    this.instance.forenameRu = value;
    return this;
  }
  
  surnameEn(value: string): ArdshinProductBuilder {
    this.instance.surnameEn = value;
    return this;
  }
  
  surnameAm(value: string): ArdshinProductBuilder {
    this.instance.surnameAm = value;
    return this;
  }
  
  surnameRu(value: string): ArdshinProductBuilder {
    this.instance.surnameRu = value;
    return this;
  }
  
  birthDay(value: string): ArdshinProductBuilder {
    this.instance.birthDay = value;
    return this;
  }
  
  accountNumber(value: string): ArdshinProductBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  currencyCode(value: string): ArdshinProductBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  currRate(value: string): ArdshinProductBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  creditId(value: string): ArdshinProductBuilder {
    this.instance.creditId = value;
    return this;
  }
  
  repDate(value: string): ArdshinProductBuilder {
    this.instance.repDate = value;
    return this;
  }
  
  repAmount(value: string): ArdshinProductBuilder {
    this.instance.repAmount = value;
    return this;
  }
  
  creditBalance(value: string): ArdshinProductBuilder {
    this.instance.creditBalance = value;
    return this;
  }
  
  warningText(value: string): ArdshinProductBuilder {
    this.instance.warningText = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArdshinLegalProduct {
  nameEn: string;
  nameAm: string;
  nameRu: string;
  taxpayer: string;
  birthDay: string;
  accountNumber: string;
  currencyCode: string;
  currRate: string;
  warningText: string;
  
  constructor() {
  }
  
  static build(): ArdshinLegalProductBuilder {
    return new ArdshinLegalProductBuilder();
  }
}

class ArdshinLegalProductBuilder {
  private instance: ArdshinLegalProduct;
  
  constructor() {
    this.instance = new ArdshinLegalProduct();
  }
  
  nameEn(value: string): ArdshinLegalProductBuilder {
    this.instance.nameEn = value;
    return this;
  }
  
  nameAm(value: string): ArdshinLegalProductBuilder {
    this.instance.nameAm = value;
    return this;
  }
  
  nameRu(value: string): ArdshinLegalProductBuilder {
    this.instance.nameRu = value;
    return this;
  }
  
  taxpayer(value: string): ArdshinLegalProductBuilder {
    this.instance.taxpayer = value;
    return this;
  }
  
  birthDay(value: string): ArdshinLegalProductBuilder {
    this.instance.birthDay = value;
    return this;
  }
  
  accountNumber(value: string): ArdshinLegalProductBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  currencyCode(value: string): ArdshinLegalProductBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  currRate(value: string): ArdshinLegalProductBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  warningText(value: string): ArdshinLegalProductBuilder {
    this.instance.warningText = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class IdBankProduct {
  cardNumber: string;
  forenameEn: string;
  forenameAm: string;
  forenameRu: string;
  surnameEn: string;
  surnameAm: string;
  surnameRu: string;
  patronymicEn: string;
  patronymicAm: string;
  patronymicRu: string;
  birthDay: string;
  accountNumber: string;
  currencyCode: string;
  currRate: string;
  creditId: string;
  fineamnt: string;
  timedamnt: string;
  curramnt: string;
  nextamnt: string;
  nextdate: string;
  
  constructor() {
  }
  
  static build(): IdBankProductBuilder {
    return new IdBankProductBuilder();
  }
}

class IdBankProductBuilder {
  private instance: IdBankProduct;
  
  constructor() {
    this.instance = new IdBankProduct();
  }
  
  cardNumber(value: string): IdBankProductBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  forenameEn(value: string): IdBankProductBuilder {
    this.instance.forenameEn = value;
    return this;
  }
  
  forenameAm(value: string): IdBankProductBuilder {
    this.instance.forenameAm = value;
    return this;
  }
  
  forenameRu(value: string): IdBankProductBuilder {
    this.instance.forenameRu = value;
    return this;
  }
  
  surnameEn(value: string): IdBankProductBuilder {
    this.instance.surnameEn = value;
    return this;
  }
  
  surnameAm(value: string): IdBankProductBuilder {
    this.instance.surnameAm = value;
    return this;
  }
  
  surnameRu(value: string): IdBankProductBuilder {
    this.instance.surnameRu = value;
    return this;
  }
  
  patronymicEn(value: string): IdBankProductBuilder {
    this.instance.patronymicEn = value;
    return this;
  }
  
  patronymicAm(value: string): IdBankProductBuilder {
    this.instance.patronymicAm = value;
    return this;
  }
  
  patronymicRu(value: string): IdBankProductBuilder {
    this.instance.patronymicRu = value;
    return this;
  }
  
  birthDay(value: string): IdBankProductBuilder {
    this.instance.birthDay = value;
    return this;
  }
  
  accountNumber(value: string): IdBankProductBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  currencyCode(value: string): IdBankProductBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  currRate(value: string): IdBankProductBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  creditId(value: string): IdBankProductBuilder {
    this.instance.creditId = value;
    return this;
  }
  
  fineamnt(value: string): IdBankProductBuilder {
    this.instance.fineamnt = value;
    return this;
  }
  
  timedamnt(value: string): IdBankProductBuilder {
    this.instance.timedamnt = value;
    return this;
  }
  
  curramnt(value: string): IdBankProductBuilder {
    this.instance.curramnt = value;
    return this;
  }
  
  nextamnt(value: string): IdBankProductBuilder {
    this.instance.nextamnt = value;
    return this;
  }
  
  nextdate(value: string): IdBankProductBuilder {
    this.instance.nextdate = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AcbaProduct {
  identifier: string;
  warning: string;
  cardNum: string;
  currentAcc: string;
  rate: string;
  currency: string;
  recipient: string;
  cardAcc: string;
  depositAcc: string;
  depositDocNum: string;
  
  constructor() {
  }
  
  static build(): AcbaProductBuilder {
    return new AcbaProductBuilder();
  }
}

class AcbaProductBuilder {
  private instance: AcbaProduct;
  
  constructor() {
    this.instance = new AcbaProduct();
  }
  
  identifier(value: string): AcbaProductBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  warning(value: string): AcbaProductBuilder {
    this.instance.warning = value;
    return this;
  }
  
  cardNum(value: string): AcbaProductBuilder {
    this.instance.cardNum = value;
    return this;
  }
  
  currentAcc(value: string): AcbaProductBuilder {
    this.instance.currentAcc = value;
    return this;
  }
  
  rate(value: string): AcbaProductBuilder {
    this.instance.rate = value;
    return this;
  }
  
  currency(value: string): AcbaProductBuilder {
    this.instance.currency = value;
    return this;
  }
  
  recipient(value: string): AcbaProductBuilder {
    this.instance.recipient = value;
    return this;
  }
  
  cardAcc(value: string): AcbaProductBuilder {
    this.instance.cardAcc = value;
    return this;
  }
  
  depositAcc(value: string): AcbaProductBuilder {
    this.instance.depositAcc = value;
    return this;
  }
  
  depositDocNum(value: string): AcbaProductBuilder {
    this.instance.depositDocNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxInfo {
  name: string;
  branchId: string;
  branchCode: string;
  note: string;
  loggedInOperatorId: string;
  enabled: boolean;
  balance: number;
  openingBalance: number;
  operators: Array<string>;
  lastReportDate: Date;
  allowPosTerminal: boolean;
  posAccountNumber: string;
  
  constructor() {
  }
  
  static build(): CashboxInfoBuilder {
    return new CashboxInfoBuilder();
  }
}

class CashboxInfoBuilder {
  private instance: CashboxInfo;
  
  constructor() {
    this.instance = new CashboxInfo();
  }
  
  name(value: string): CashboxInfoBuilder {
    this.instance.name = value;
    return this;
  }
  
  branchId(value: string): CashboxInfoBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchCode(value: string): CashboxInfoBuilder {
    this.instance.branchCode = value;
    return this;
  }
  
  note(value: string): CashboxInfoBuilder {
    this.instance.note = value;
    return this;
  }
  
  loggedInOperatorId(value: string): CashboxInfoBuilder {
    this.instance.loggedInOperatorId = value;
    return this;
  }
  
  enabled(value: boolean): CashboxInfoBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  balance(value: number): CashboxInfoBuilder {
    this.instance.balance = value;
    return this;
  }
  
  openingBalance(value: number): CashboxInfoBuilder {
    this.instance.openingBalance = value;
    return this;
  }
  
  operators(value: Array<string>): CashboxInfoBuilder {
    this.instance.operators = value;
    return this;
  }
  
  lastReportDate(value: Date): CashboxInfoBuilder {
    this.instance.lastReportDate = value;
    return this;
  }
  
  allowPosTerminal(value: boolean): CashboxInfoBuilder {
    this.instance.allowPosTerminal = value;
    return this;
  }
  
  posAccountNumber(value: string): CashboxInfoBuilder {
    this.instance.posAccountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerTransactionData {
  transactionId: string;
  providerId: string;
  receiptId: string;
  providerName: string;
  totalAmount: number;
  commission: number;
  issued: Date;
  phone: string;
  forename: string;
  surname: string;
  number: string;
  accNumber: string;
  userType: UserType;
  
  constructor() {
  }
  
  static build(): CustomerTransactionDataBuilder {
    return new CustomerTransactionDataBuilder();
  }
}

class CustomerTransactionDataBuilder {
  private instance: CustomerTransactionData;
  
  constructor() {
    this.instance = new CustomerTransactionData();
  }
  
  transactionId(value: string): CustomerTransactionDataBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  providerId(value: string): CustomerTransactionDataBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  receiptId(value: string): CustomerTransactionDataBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  providerName(value: string): CustomerTransactionDataBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  totalAmount(value: number): CustomerTransactionDataBuilder {
    this.instance.totalAmount = value;
    return this;
  }
  
  commission(value: number): CustomerTransactionDataBuilder {
    this.instance.commission = value;
    return this;
  }
  
  issued(value: Date): CustomerTransactionDataBuilder {
    this.instance.issued = value;
    return this;
  }
  
  phone(value: string): CustomerTransactionDataBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): CustomerTransactionDataBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): CustomerTransactionDataBuilder {
    this.instance.surname = value;
    return this;
  }
  
  number(value: string): CustomerTransactionDataBuilder {
    this.instance.number = value;
    return this;
  }
  
  accNumber(value: string): CustomerTransactionDataBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  userType(value: UserType): CustomerTransactionDataBuilder {
    this.instance.userType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Recipient {
  customerId: string;
  fcmToken: string;
  
  constructor() {
  }
  
  static build(): RecipientBuilder {
    return new RecipientBuilder();
  }
}

class RecipientBuilder {
  private instance: Recipient;
  
  constructor() {
    this.instance = new Recipient();
  }
  
  customerId(value: string): RecipientBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  fcmToken(value: string): RecipientBuilder {
    this.instance.fcmToken = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Token {
  id: string;
  identity: string;
  userId: string;
  issued: Date;
  lastAccessed: Date;
  accessToken: string;
  refreshToken: string;
  scopes: Array<Scope>;
  userType: UserType;
  
  constructor() {
  }
  
  static build(): TokenBuilder {
    return new TokenBuilder();
  }
}

class TokenBuilder {
  private instance: Token;
  
  constructor() {
    this.instance = new Token();
  }
  
  id(value: string): TokenBuilder {
    this.instance.id = value;
    return this;
  }
  
  identity(value: string): TokenBuilder {
    this.instance.identity = value;
    return this;
  }
  
  userId(value: string): TokenBuilder {
    this.instance.userId = value;
    return this;
  }
  
  issued(value: Date): TokenBuilder {
    this.instance.issued = value;
    return this;
  }
  
  lastAccessed(value: Date): TokenBuilder {
    this.instance.lastAccessed = value;
    return this;
  }
  
  accessToken(value: string): TokenBuilder {
    this.instance.accessToken = value;
    return this;
  }
  
  refreshToken(value: string): TokenBuilder {
    this.instance.refreshToken = value;
    return this;
  }
  
  scopes(value: Array<Scope>): TokenBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  userType(value: UserType): TokenBuilder {
    this.instance.userType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Admin {
  id: string;
  email: string;
  password: string;
  scope: Scope;
  
  constructor() {
  }
  
  static build(): AdminBuilder {
    return new AdminBuilder();
  }
}

class AdminBuilder {
  private instance: Admin;
  
  constructor() {
    this.instance = new Admin();
  }
  
  id(value: string): AdminBuilder {
    this.instance.id = value;
    return this;
  }
  
  email(value: string): AdminBuilder {
    this.instance.email = value;
    return this;
  }
  
  password(value: string): AdminBuilder {
    this.instance.password = value;
    return this;
  }
  
  scope(value: Scope): AdminBuilder {
    this.instance.scope = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Customer {
  id: string;
  email: string;
  number: string;
  phone: string;
  forename: string;
  surname: string;
  status: CustomerStatus;
  isVerified: boolean;
  password: string;
  scope: Array<Scope>;
  verificationCode: string;
  verificationId: string;
  fcmToken: string;
  fcmEnabled: boolean;
  language: string;
  userType: UserType;
  balanceLimit: number;
  balance: number;
  accNumber: string;
  passportScan: string;
  accFrozen: boolean;
  deleted: boolean;
  refilled: boolean;
  
  constructor() {
  }
  
  static build(): CustomerBuilder {
    return new CustomerBuilder();
  }
}

class CustomerBuilder {
  private instance: Customer;
  
  constructor() {
    this.instance = new Customer();
  }
  
  id(value: string): CustomerBuilder {
    this.instance.id = value;
    return this;
  }
  
  email(value: string): CustomerBuilder {
    this.instance.email = value;
    return this;
  }
  
  number(value: string): CustomerBuilder {
    this.instance.number = value;
    return this;
  }
  
  phone(value: string): CustomerBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): CustomerBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): CustomerBuilder {
    this.instance.surname = value;
    return this;
  }
  
  status(value: CustomerStatus): CustomerBuilder {
    this.instance.status = value;
    return this;
  }
  
  isVerified(value: boolean): CustomerBuilder {
    this.instance.isVerified = value;
    return this;
  }
  
  password(value: string): CustomerBuilder {
    this.instance.password = value;
    return this;
  }
  
  scope(value: Array<Scope>): CustomerBuilder {
    this.instance.scope = value;
    return this;
  }
  
  verificationCode(value: string): CustomerBuilder {
    this.instance.verificationCode = value;
    return this;
  }
  
  verificationId(value: string): CustomerBuilder {
    this.instance.verificationId = value;
    return this;
  }
  
  fcmToken(value: string): CustomerBuilder {
    this.instance.fcmToken = value;
    return this;
  }
  
  fcmEnabled(value: boolean): CustomerBuilder {
    this.instance.fcmEnabled = value;
    return this;
  }
  
  language(value: string): CustomerBuilder {
    this.instance.language = value;
    return this;
  }
  
  userType(value: UserType): CustomerBuilder {
    this.instance.userType = value;
    return this;
  }
  
  balanceLimit(value: number): CustomerBuilder {
    this.instance.balanceLimit = value;
    return this;
  }
  
  balance(value: number): CustomerBuilder {
    this.instance.balance = value;
    return this;
  }
  
  accNumber(value: string): CustomerBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  passportScan(value: string): CustomerBuilder {
    this.instance.passportScan = value;
    return this;
  }
  
  accFrozen(value: boolean): CustomerBuilder {
    this.instance.accFrozen = value;
    return this;
  }
  
  deleted(value: boolean): CustomerBuilder {
    this.instance.deleted = value;
    return this;
  }
  
  refilled(value: boolean): CustomerBuilder {
    this.instance.refilled = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class FrozenAccount {
  id: string;
  userId: string;
  accNumber: string;
  freezeDate: Date;
  comment: string;
  
  constructor() {
  }
  
  static build(): FrozenAccountBuilder {
    return new FrozenAccountBuilder();
  }
}

class FrozenAccountBuilder {
  private instance: FrozenAccount;
  
  constructor() {
    this.instance = new FrozenAccount();
  }
  
  id(value: string): FrozenAccountBuilder {
    this.instance.id = value;
    return this;
  }
  
  userId(value: string): FrozenAccountBuilder {
    this.instance.userId = value;
    return this;
  }
  
  accNumber(value: string): FrozenAccountBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  freezeDate(value: Date): FrozenAccountBuilder {
    this.instance.freezeDate = value;
    return this;
  }
  
  comment(value: string): FrozenAccountBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Registration {
  id: string;
  regId: string;
  phone: string;
  password: string;
  activationCode: string;
  
  constructor() {
  }
  
  static build(): RegistrationBuilder {
    return new RegistrationBuilder();
  }
}

class RegistrationBuilder {
  private instance: Registration;
  
  constructor() {
    this.instance = new Registration();
  }
  
  id(value: string): RegistrationBuilder {
    this.instance.id = value;
    return this;
  }
  
  regId(value: string): RegistrationBuilder {
    this.instance.regId = value;
    return this;
  }
  
  phone(value: string): RegistrationBuilder {
    this.instance.phone = value;
    return this;
  }
  
  password(value: string): RegistrationBuilder {
    this.instance.password = value;
    return this;
  }
  
  activationCode(value: string): RegistrationBuilder {
    this.instance.activationCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Operator {
  id: string;
  email: string;
  number: string;
  phone: string;
  userName: string;
  cashboxId: string;
  forename: string;
  surname: string;
  password: string;
  expiration: Date;
  enabled: boolean;
  status: OperatorStatus;
  scope: Array<Scope>;
  
  constructor() {
  }
  
  static build(): OperatorBuilder {
    return new OperatorBuilder();
  }
}

class OperatorBuilder {
  private instance: Operator;
  
  constructor() {
    this.instance = new Operator();
  }
  
  id(value: string): OperatorBuilder {
    this.instance.id = value;
    return this;
  }
  
  email(value: string): OperatorBuilder {
    this.instance.email = value;
    return this;
  }
  
  number(value: string): OperatorBuilder {
    this.instance.number = value;
    return this;
  }
  
  phone(value: string): OperatorBuilder {
    this.instance.phone = value;
    return this;
  }
  
  userName(value: string): OperatorBuilder {
    this.instance.userName = value;
    return this;
  }
  
  cashboxId(value: string): OperatorBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  forename(value: string): OperatorBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): OperatorBuilder {
    this.instance.surname = value;
    return this;
  }
  
  password(value: string): OperatorBuilder {
    this.instance.password = value;
    return this;
  }
  
  expiration(value: Date): OperatorBuilder {
    this.instance.expiration = value;
    return this;
  }
  
  enabled(value: boolean): OperatorBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  status(value: OperatorStatus): OperatorBuilder {
    this.instance.status = value;
    return this;
  }
  
  scope(value: Array<Scope>): OperatorBuilder {
    this.instance.scope = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Sequence {
  id: string;
  name: string;
  last: number;
  
  constructor() {
  }
  
  static build(): SequenceBuilder {
    return new SequenceBuilder();
  }
}

class SequenceBuilder {
  private instance: Sequence;
  
  constructor() {
    this.instance = new Sequence();
  }
  
  id(value: string): SequenceBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): SequenceBuilder {
    this.instance.name = value;
    return this;
  }
  
  last(value: number): SequenceBuilder {
    this.instance.last = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Setting {
  id: string;
  customerId: string;
  key: string;
  value: string;
  
  constructor() {
  }
  
  static build(): SettingBuilder {
    return new SettingBuilder();
  }
}

class SettingBuilder {
  private instance: Setting;
  
  constructor() {
    this.instance = new Setting();
  }
  
  id(value: string): SettingBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): SettingBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  key(value: string): SettingBuilder {
    this.instance.key = value;
    return this;
  }
  
  value(value: string): SettingBuilder {
    this.instance.value = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Branch {
  id: string;
  name: string;
  code: string;
  address: string;
  policyId: string;
  description: string;
  enabled: boolean;
  latitude: number;
  longitude: number;
  
  constructor() {
  }
  
  static build(): BranchBuilder {
    return new BranchBuilder();
  }
}

class BranchBuilder {
  private instance: Branch;
  
  constructor() {
    this.instance = new Branch();
  }
  
  id(value: string): BranchBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): BranchBuilder {
    this.instance.name = value;
    return this;
  }
  
  code(value: string): BranchBuilder {
    this.instance.code = value;
    return this;
  }
  
  address(value: string): BranchBuilder {
    this.instance.address = value;
    return this;
  }
  
  policyId(value: string): BranchBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  description(value: string): BranchBuilder {
    this.instance.description = value;
    return this;
  }
  
  enabled(value: boolean): BranchBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  latitude(value: number): BranchBuilder {
    this.instance.latitude = value;
    return this;
  }
  
  longitude(value: number): BranchBuilder {
    this.instance.longitude = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Cashbox {
  id: string;
  name: string;
  accountNumber: string;
  branchId: string;
  branchCode: string;
  note: string;
  certificateId: string;
  loggedInOperatorId: string;
  enabled: boolean;
  balance: number;
  openingBalance: number;
  operators: Array<string>;
  lastReportDate: Date;
  allowPosTerminal: boolean;
  posAccountNumber: string;
  exceedClosingLimit: boolean;
  
  constructor() {
  }
  
  static build(): CashboxBuilder {
    return new CashboxBuilder();
  }
}

class CashboxBuilder {
  private instance: Cashbox;
  
  constructor() {
    this.instance = new Cashbox();
  }
  
  id(value: string): CashboxBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): CashboxBuilder {
    this.instance.name = value;
    return this;
  }
  
  accountNumber(value: string): CashboxBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  branchId(value: string): CashboxBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchCode(value: string): CashboxBuilder {
    this.instance.branchCode = value;
    return this;
  }
  
  note(value: string): CashboxBuilder {
    this.instance.note = value;
    return this;
  }
  
  certificateId(value: string): CashboxBuilder {
    this.instance.certificateId = value;
    return this;
  }
  
  loggedInOperatorId(value: string): CashboxBuilder {
    this.instance.loggedInOperatorId = value;
    return this;
  }
  
  enabled(value: boolean): CashboxBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  balance(value: number): CashboxBuilder {
    this.instance.balance = value;
    return this;
  }
  
  openingBalance(value: number): CashboxBuilder {
    this.instance.openingBalance = value;
    return this;
  }
  
  operators(value: Array<string>): CashboxBuilder {
    this.instance.operators = value;
    return this;
  }
  
  lastReportDate(value: Date): CashboxBuilder {
    this.instance.lastReportDate = value;
    return this;
  }
  
  allowPosTerminal(value: boolean): CashboxBuilder {
    this.instance.allowPosTerminal = value;
    return this;
  }
  
  posAccountNumber(value: string): CashboxBuilder {
    this.instance.posAccountNumber = value;
    return this;
  }
  
  exceedClosingLimit(value: boolean): CashboxBuilder {
    this.instance.exceedClosingLimit = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxLog {
  id: string;
  operatorId: string;
  cashboxId: string;
  cashboxPaymentId: string;
  action: CashboxAction;
  type: EncashmentType;
  amountTotal: number;
  serviceQuantity: number;
  amountIn: number;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  details: string;
  notes: string;
  created: Date;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): CashboxLogBuilder {
    return new CashboxLogBuilder();
  }
}

class CashboxLogBuilder {
  private instance: CashboxLog;
  
  constructor() {
    this.instance = new CashboxLog();
  }
  
  id(value: string): CashboxLogBuilder {
    this.instance.id = value;
    return this;
  }
  
  operatorId(value: string): CashboxLogBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  cashboxId(value: string): CashboxLogBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  cashboxPaymentId(value: string): CashboxLogBuilder {
    this.instance.cashboxPaymentId = value;
    return this;
  }
  
  action(value: CashboxAction): CashboxLogBuilder {
    this.instance.action = value;
    return this;
  }
  
  type(value: EncashmentType): CashboxLogBuilder {
    this.instance.type = value;
    return this;
  }
  
  amountTotal(value: number): CashboxLogBuilder {
    this.instance.amountTotal = value;
    return this;
  }
  
  serviceQuantity(value: number): CashboxLogBuilder {
    this.instance.serviceQuantity = value;
    return this;
  }
  
  amountIn(value: number): CashboxLogBuilder {
    this.instance.amountIn = value;
    return this;
  }
  
  commission(value: number): CashboxLogBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashboxLogBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashboxLogBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  details(value: string): CashboxLogBuilder {
    this.instance.details = value;
    return this;
  }
  
  notes(value: string): CashboxLogBuilder {
    this.instance.notes = value;
    return this;
  }
  
  created(value: Date): CashboxLogBuilder {
    this.instance.created = value;
    return this;
  }
  
  contactNum(value: string): CashboxLogBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Group {
  id: string;
  name: string;
  createdDate: Date;
  contracts: Array<Contract>;
  
  constructor() {
  }
  
  static build(): GroupBuilder {
    return new GroupBuilder();
  }
}

class GroupBuilder {
  private instance: Group;
  
  constructor() {
    this.instance = new Group();
  }
  
  id(value: string): GroupBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): GroupBuilder {
    this.instance.name = value;
    return this;
  }
  
  createdDate(value: Date): GroupBuilder {
    this.instance.createdDate = value;
    return this;
  }
  
  contracts(value: Array<Contract>): GroupBuilder {
    this.instance.contracts = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerGroup {
  id: string;
  name: string;
  created: Date;
  customerId: string;
  agreements: Array<Agreement>;
  
  constructor() {
  }
  
  static build(): CustomerGroupBuilder {
    return new CustomerGroupBuilder();
  }
}

class CustomerGroupBuilder {
  private instance: CustomerGroup;
  
  constructor() {
    this.instance = new CustomerGroup();
  }
  
  id(value: string): CustomerGroupBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): CustomerGroupBuilder {
    this.instance.name = value;
    return this;
  }
  
  created(value: Date): CustomerGroupBuilder {
    this.instance.created = value;
    return this;
  }
  
  customerId(value: string): CustomerGroupBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  agreements(value: Array<Agreement>): CustomerGroupBuilder {
    this.instance.agreements = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Commission {
  id: string;
  providerId: string;
  policyId: string;
  approved: boolean;
  action: CommissionAction;
  ranges: Array<CommissionRange>;
  
  constructor() {
  }
  
  static build(): CommissionBuilder {
    return new CommissionBuilder();
  }
}

class CommissionBuilder {
  private instance: Commission;
  
  constructor() {
    this.instance = new Commission();
  }
  
  id(value: string): CommissionBuilder {
    this.instance.id = value;
    return this;
  }
  
  providerId(value: string): CommissionBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  policyId(value: string): CommissionBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  approved(value: boolean): CommissionBuilder {
    this.instance.approved = value;
    return this;
  }
  
  action(value: CommissionAction): CommissionBuilder {
    this.instance.action = value;
    return this;
  }
  
  ranges(value: Array<CommissionRange>): CommissionBuilder {
    this.instance.ranges = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CommissionPolicy {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): CommissionPolicyBuilder {
    return new CommissionPolicyBuilder();
  }
}

class CommissionPolicyBuilder {
  private instance: CommissionPolicy;
  
  constructor() {
    this.instance = new CommissionPolicy();
  }
  
  id(value: string): CommissionPolicyBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): CommissionPolicyBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Provider {
  id: string;
  name: string;
  accountNumber: string;
  accountNumberMobile: string;
  commissionAccountNumber: string;
  commissionAccountNumberMobile: string;
  key: string;
  desc: string;
  enabled: boolean;
  useFixedAmount: boolean;
  labels: Array<string>;
  paymentPurpose: string;
  paymentWalletPurpose: string;
  contractDescs: Array<ContractDesc>;
  amountMin: number;
  amountMax: number;
  invoiceNote: string;
  
  constructor() {
  }
  
  static build(): ProviderBuilder {
    return new ProviderBuilder();
  }
}

class ProviderBuilder {
  private instance: Provider;
  
  constructor() {
    this.instance = new Provider();
  }
  
  id(value: string): ProviderBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): ProviderBuilder {
    this.instance.name = value;
    return this;
  }
  
  accountNumber(value: string): ProviderBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  accountNumberMobile(value: string): ProviderBuilder {
    this.instance.accountNumberMobile = value;
    return this;
  }
  
  commissionAccountNumber(value: string): ProviderBuilder {
    this.instance.commissionAccountNumber = value;
    return this;
  }
  
  commissionAccountNumberMobile(value: string): ProviderBuilder {
    this.instance.commissionAccountNumberMobile = value;
    return this;
  }
  
  key(value: string): ProviderBuilder {
    this.instance.key = value;
    return this;
  }
  
  desc(value: string): ProviderBuilder {
    this.instance.desc = value;
    return this;
  }
  
  enabled(value: boolean): ProviderBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  useFixedAmount(value: boolean): ProviderBuilder {
    this.instance.useFixedAmount = value;
    return this;
  }
  
  labels(value: Array<string>): ProviderBuilder {
    this.instance.labels = value;
    return this;
  }
  
  paymentPurpose(value: string): ProviderBuilder {
    this.instance.paymentPurpose = value;
    return this;
  }
  
  paymentWalletPurpose(value: string): ProviderBuilder {
    this.instance.paymentWalletPurpose = value;
    return this;
  }
  
  contractDescs(value: Array<ContractDesc>): ProviderBuilder {
    this.instance.contractDescs = value;
    return this;
  }
  
  amountMin(value: number): ProviderBuilder {
    this.instance.amountMin = value;
    return this;
  }
  
  amountMax(value: number): ProviderBuilder {
    this.instance.amountMax = value;
    return this;
  }
  
  invoiceNote(value: string): ProviderBuilder {
    this.instance.invoiceNote = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ProviderLabel {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;
  parentId: string;
  pos: number;
  type: string;
  data: string;
  
  constructor() {
  }
  
  static build(): ProviderLabelBuilder {
    return new ProviderLabelBuilder();
  }
}

class ProviderLabelBuilder {
  private instance: ProviderLabel;
  
  constructor() {
    this.instance = new ProviderLabel();
  }
  
  id(value: string): ProviderLabelBuilder {
    this.instance.id = value;
    return this;
  }
  
  icon(value: string): ProviderLabelBuilder {
    this.instance.icon = value;
    return this;
  }
  
  name(value: string): ProviderLabelBuilder {
    this.instance.name = value;
    return this;
  }
  
  label(value: string): ProviderLabelBuilder {
    this.instance.label = value;
    return this;
  }
  
  desc(value: string): ProviderLabelBuilder {
    this.instance.desc = value;
    return this;
  }
  
  parentId(value: string): ProviderLabelBuilder {
    this.instance.parentId = value;
    return this;
  }
  
  pos(value: number): ProviderLabelBuilder {
    this.instance.pos = value;
    return this;
  }
  
  type(value: string): ProviderLabelBuilder {
    this.instance.type = value;
    return this;
  }
  
  data(value: string): ProviderLabelBuilder {
    this.instance.data = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class DayClosure {
  id: string;
  cashboxId: string;
  branchId: string;
  created: Date;
  cashierId: string;
  cashierName: string;
  amountIn: number;
  amountOut: number;
  incassation: number;
  finalBalance: number;
  note: string;
  
  constructor() {
  }
  
  static build(): DayClosureBuilder {
    return new DayClosureBuilder();
  }
}

class DayClosureBuilder {
  private instance: DayClosure;
  
  constructor() {
    this.instance = new DayClosure();
  }
  
  id(value: string): DayClosureBuilder {
    this.instance.id = value;
    return this;
  }
  
  cashboxId(value: string): DayClosureBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): DayClosureBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  created(value: Date): DayClosureBuilder {
    this.instance.created = value;
    return this;
  }
  
  cashierId(value: string): DayClosureBuilder {
    this.instance.cashierId = value;
    return this;
  }
  
  cashierName(value: string): DayClosureBuilder {
    this.instance.cashierName = value;
    return this;
  }
  
  amountIn(value: number): DayClosureBuilder {
    this.instance.amountIn = value;
    return this;
  }
  
  amountOut(value: number): DayClosureBuilder {
    this.instance.amountOut = value;
    return this;
  }
  
  incassation(value: number): DayClosureBuilder {
    this.instance.incassation = value;
    return this;
  }
  
  finalBalance(value: number): DayClosureBuilder {
    this.instance.finalBalance = value;
    return this;
  }
  
  note(value: string): DayClosureBuilder {
    this.instance.note = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Basket {
  id: string;
  providerId: string;
  contractIdentity: string;
  operatorId: string;
  cashboxId: string;
  
  constructor() {
  }
  
  static build(): BasketBuilder {
    return new BasketBuilder();
  }
}

class BasketBuilder {
  private instance: Basket;
  
  constructor() {
    this.instance = new Basket();
  }
  
  id(value: string): BasketBuilder {
    this.instance.id = value;
    return this;
  }
  
  providerId(value: string): BasketBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  contractIdentity(value: string): BasketBuilder {
    this.instance.contractIdentity = value;
    return this;
  }
  
  operatorId(value: string): BasketBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  cashboxId(value: string): BasketBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Payment {
  id: string;
  operatorId: string;
  providerId: string;
  contract: string;
  amount: number;
  issued: Date;
  completed: Date;
  
  constructor() {
  }
  
  static build(): PaymentBuilder {
    return new PaymentBuilder();
  }
}

class PaymentBuilder {
  private instance: Payment;
  
  constructor() {
    this.instance = new Payment();
  }
  
  id(value: string): PaymentBuilder {
    this.instance.id = value;
    return this;
  }
  
  operatorId(value: string): PaymentBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  providerId(value: string): PaymentBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  contract(value: string): PaymentBuilder {
    this.instance.contract = value;
    return this;
  }
  
  amount(value: number): PaymentBuilder {
    this.instance.amount = value;
    return this;
  }
  
  issued(value: Date): PaymentBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): PaymentBuilder {
    this.instance.completed = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerTransaction {
  id: string;
  paymentId: string;
  receiptId: string;
  issued: Date;
  completed: Date;
  customerId: string;
  source: string;
  sourceType: SourceType;
  providerId: string;
  providerName: string;
  providerKey: string;
  purpose: string;
  totalAmount: number;
  netAmount: number;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  userNum: string;
  status: string;
  attempts: number;
  lastAttempt: Date;
  commandType: string;
  externalId: string;
  additionalData: string;
  rawData: string;
  clientData: string;
  type: UserTransactionType;
  
  constructor() {
  }
  
  static build(): CustomerTransactionBuilder {
    return new CustomerTransactionBuilder();
  }
}

class CustomerTransactionBuilder {
  private instance: CustomerTransaction;
  
  constructor() {
    this.instance = new CustomerTransaction();
  }
  
  id(value: string): CustomerTransactionBuilder {
    this.instance.id = value;
    return this;
  }
  
  paymentId(value: string): CustomerTransactionBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  receiptId(value: string): CustomerTransactionBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  issued(value: Date): CustomerTransactionBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): CustomerTransactionBuilder {
    this.instance.completed = value;
    return this;
  }
  
  customerId(value: string): CustomerTransactionBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  source(value: string): CustomerTransactionBuilder {
    this.instance.source = value;
    return this;
  }
  
  sourceType(value: SourceType): CustomerTransactionBuilder {
    this.instance.sourceType = value;
    return this;
  }
  
  providerId(value: string): CustomerTransactionBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  providerName(value: string): CustomerTransactionBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  providerKey(value: string): CustomerTransactionBuilder {
    this.instance.providerKey = value;
    return this;
  }
  
  purpose(value: string): CustomerTransactionBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  totalAmount(value: number): CustomerTransactionBuilder {
    this.instance.totalAmount = value;
    return this;
  }
  
  netAmount(value: number): CustomerTransactionBuilder {
    this.instance.netAmount = value;
    return this;
  }
  
  debt(value: number): CustomerTransactionBuilder {
    this.instance.debt = value;
    return this;
  }
  
  destinationWalletId(value: string): CustomerTransactionBuilder {
    this.instance.destinationWalletId = value;
    return this;
  }
  
  commissionAmount(value: number): CustomerTransactionBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  comment(value: string): CustomerTransactionBuilder {
    this.instance.comment = value;
    return this;
  }
  
  userNum(value: string): CustomerTransactionBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  status(value: string): CustomerTransactionBuilder {
    this.instance.status = value;
    return this;
  }
  
  attempts(value: number): CustomerTransactionBuilder {
    this.instance.attempts = value;
    return this;
  }
  
  lastAttempt(value: Date): CustomerTransactionBuilder {
    this.instance.lastAttempt = value;
    return this;
  }
  
  commandType(value: string): CustomerTransactionBuilder {
    this.instance.commandType = value;
    return this;
  }
  
  externalId(value: string): CustomerTransactionBuilder {
    this.instance.externalId = value;
    return this;
  }
  
  additionalData(value: string): CustomerTransactionBuilder {
    this.instance.additionalData = value;
    return this;
  }
  
  rawData(value: string): CustomerTransactionBuilder {
    this.instance.rawData = value;
    return this;
  }
  
  clientData(value: string): CustomerTransactionBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  type(value: UserTransactionType): CustomerTransactionBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Transaction {
  id: string;
  paymentId: string;
  receiptId: string;
  issuer: string;
  issued: Date;
  completed: Date;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  branchName: string;
  providerId: string;
  providerName: string;
  purpose: string;
  totalAmount: number;
  netAmount: number;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  debt: number;
  destinationWalletId: string;
  commissionAmount: number;
  comment: string;
  userNum: string;
  externalId: string;
  additionalData: string;
  rawData: string;
  clientData: string;
  commandType: string;
  status: string;
  attempts: number;
  lastAttempt: Date;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): TransactionBuilder {
    return new TransactionBuilder();
  }
}

class TransactionBuilder {
  private instance: Transaction;
  
  constructor() {
    this.instance = new Transaction();
  }
  
  id(value: string): TransactionBuilder {
    this.instance.id = value;
    return this;
  }
  
  paymentId(value: string): TransactionBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  receiptId(value: string): TransactionBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  issuer(value: string): TransactionBuilder {
    this.instance.issuer = value;
    return this;
  }
  
  issued(value: Date): TransactionBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): TransactionBuilder {
    this.instance.completed = value;
    return this;
  }
  
  operatorId(value: string): TransactionBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  cashboxId(value: string): TransactionBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): TransactionBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchName(value: string): TransactionBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  providerId(value: string): TransactionBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  providerName(value: string): TransactionBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  purpose(value: string): TransactionBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  totalAmount(value: number): TransactionBuilder {
    this.instance.totalAmount = value;
    return this;
  }
  
  netAmount(value: number): TransactionBuilder {
    this.instance.netAmount = value;
    return this;
  }
  
  commission(value: number): TransactionBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): TransactionBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): TransactionBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  debt(value: number): TransactionBuilder {
    this.instance.debt = value;
    return this;
  }
  
  destinationWalletId(value: string): TransactionBuilder {
    this.instance.destinationWalletId = value;
    return this;
  }
  
  commissionAmount(value: number): TransactionBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  comment(value: string): TransactionBuilder {
    this.instance.comment = value;
    return this;
  }
  
  userNum(value: string): TransactionBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  externalId(value: string): TransactionBuilder {
    this.instance.externalId = value;
    return this;
  }
  
  additionalData(value: string): TransactionBuilder {
    this.instance.additionalData = value;
    return this;
  }
  
  rawData(value: string): TransactionBuilder {
    this.instance.rawData = value;
    return this;
  }
  
  clientData(value: string): TransactionBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commandType(value: string): TransactionBuilder {
    this.instance.commandType = value;
    return this;
  }
  
  status(value: string): TransactionBuilder {
    this.instance.status = value;
    return this;
  }
  
  attempts(value: number): TransactionBuilder {
    this.instance.attempts = value;
    return this;
  }
  
  lastAttempt(value: Date): TransactionBuilder {
    this.instance.lastAttempt = value;
    return this;
  }
  
  contactNum(value: string): TransactionBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmsoftTransactions {
  id: string;
  created: Date;
  data: ASTransactionData;
  status: string;
  basedOn: string;
  
  constructor() {
  }
  
  static build(): ArmsoftTransactionsBuilder {
    return new ArmsoftTransactionsBuilder();
  }
}

class ArmsoftTransactionsBuilder {
  private instance: ArmsoftTransactions;
  
  constructor() {
    this.instance = new ArmsoftTransactions();
  }
  
  id(value: string): ArmsoftTransactionsBuilder {
    this.instance.id = value;
    return this;
  }
  
  created(value: Date): ArmsoftTransactionsBuilder {
    this.instance.created = value;
    return this;
  }
  
  data(value: ASTransactionData): ArmsoftTransactionsBuilder {
    this.instance.data = value;
    return this;
  }
  
  status(value: string): ArmsoftTransactionsBuilder {
    this.instance.status = value;
    return this;
  }
  
  basedOn(value: string): ArmsoftTransactionsBuilder {
    this.instance.basedOn = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmsoftRegistration {
  id: string;
  created: Date;
  customerId: string;
  data: ASCustomerData;
  status: string;
  
  constructor() {
  }
  
  static build(): ArmsoftRegistrationBuilder {
    return new ArmsoftRegistrationBuilder();
  }
}

class ArmsoftRegistrationBuilder {
  private instance: ArmsoftRegistration;
  
  constructor() {
    this.instance = new ArmsoftRegistration();
  }
  
  id(value: string): ArmsoftRegistrationBuilder {
    this.instance.id = value;
    return this;
  }
  
  created(value: Date): ArmsoftRegistrationBuilder {
    this.instance.created = value;
    return this;
  }
  
  customerId(value: string): ArmsoftRegistrationBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  data(value: ASCustomerData): ArmsoftRegistrationBuilder {
    this.instance.data = value;
    return this;
  }
  
  status(value: string): ArmsoftRegistrationBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArmsoftAccountUpdate {
  id: string;
  created: Date;
  customerId: string;
  data: ASCustomerDataUpdate;
  status: string;
  
  constructor() {
  }
  
  static build(): ArmsoftAccountUpdateBuilder {
    return new ArmsoftAccountUpdateBuilder();
  }
}

class ArmsoftAccountUpdateBuilder {
  private instance: ArmsoftAccountUpdate;
  
  constructor() {
    this.instance = new ArmsoftAccountUpdate();
  }
  
  id(value: string): ArmsoftAccountUpdateBuilder {
    this.instance.id = value;
    return this;
  }
  
  created(value: Date): ArmsoftAccountUpdateBuilder {
    this.instance.created = value;
    return this;
  }
  
  customerId(value: string): ArmsoftAccountUpdateBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  data(value: ASCustomerDataUpdate): ArmsoftAccountUpdateBuilder {
    this.instance.data = value;
    return this;
  }
  
  status(value: string): ArmsoftAccountUpdateBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashboxReport {
  id: string;
  cashboxId: string;
  cashboxName: string;
  branchId: string;
  branchName: string;
  operatorId: string;
  operatorName: string;
  operatorUsername: string;
  created: Date;
  openingBalance: number;
  openingDate: Date;
  balance: number;
  encashmentIn: number;
  encashmentOut: number;
  paymentIn: number;
  cashlessOut: number;
  dataByProviders: string;
  
  constructor() {
  }
  
  static build(): CashboxReportBuilder {
    return new CashboxReportBuilder();
  }
}

class CashboxReportBuilder {
  private instance: CashboxReport;
  
  constructor() {
    this.instance = new CashboxReport();
  }
  
  id(value: string): CashboxReportBuilder {
    this.instance.id = value;
    return this;
  }
  
  cashboxId(value: string): CashboxReportBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  cashboxName(value: string): CashboxReportBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  branchId(value: string): CashboxReportBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchName(value: string): CashboxReportBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  operatorId(value: string): CashboxReportBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  operatorName(value: string): CashboxReportBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  operatorUsername(value: string): CashboxReportBuilder {
    this.instance.operatorUsername = value;
    return this;
  }
  
  created(value: Date): CashboxReportBuilder {
    this.instance.created = value;
    return this;
  }
  
  openingBalance(value: number): CashboxReportBuilder {
    this.instance.openingBalance = value;
    return this;
  }
  
  openingDate(value: Date): CashboxReportBuilder {
    this.instance.openingDate = value;
    return this;
  }
  
  balance(value: number): CashboxReportBuilder {
    this.instance.balance = value;
    return this;
  }
  
  encashmentIn(value: number): CashboxReportBuilder {
    this.instance.encashmentIn = value;
    return this;
  }
  
  encashmentOut(value: number): CashboxReportBuilder {
    this.instance.encashmentOut = value;
    return this;
  }
  
  paymentIn(value: number): CashboxReportBuilder {
    this.instance.paymentIn = value;
    return this;
  }
  
  cashlessOut(value: number): CashboxReportBuilder {
    this.instance.cashlessOut = value;
    return this;
  }
  
  dataByProviders(value: string): CashboxReportBuilder {
    this.instance.dataByProviders = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorLogEntry {
  id: string;
  paymentId: string;
  cashboxId: string;
  cashboxName: string;
  operatorName: string;
  providerName: string;
  purpose: string;
  userNum: string;
  date: Date;
  totalAmount: number;
  netAmount: number;
  cash: boolean;
  posAuthcode: string;
  debt: number;
  commissionAmount: number;
  userId: string;
  receiptId: string;
  branchName: string;
  action: CashboxAction;
  encashmentType: EncashmentType;
  notes: string;
  transactionId: string;
  rawData: string;
  clientData: string;
  contactNum: string;
  externalId: string;
  
  constructor() {
  }
  
  static build(): OperatorLogEntryBuilder {
    return new OperatorLogEntryBuilder();
  }
}

class OperatorLogEntryBuilder {
  private instance: OperatorLogEntry;
  
  constructor() {
    this.instance = new OperatorLogEntry();
  }
  
  id(value: string): OperatorLogEntryBuilder {
    this.instance.id = value;
    return this;
  }
  
  paymentId(value: string): OperatorLogEntryBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  cashboxId(value: string): OperatorLogEntryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  cashboxName(value: string): OperatorLogEntryBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  operatorName(value: string): OperatorLogEntryBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  providerName(value: string): OperatorLogEntryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  purpose(value: string): OperatorLogEntryBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  userNum(value: string): OperatorLogEntryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  date(value: Date): OperatorLogEntryBuilder {
    this.instance.date = value;
    return this;
  }
  
  totalAmount(value: number): OperatorLogEntryBuilder {
    this.instance.totalAmount = value;
    return this;
  }
  
  netAmount(value: number): OperatorLogEntryBuilder {
    this.instance.netAmount = value;
    return this;
  }
  
  cash(value: boolean): OperatorLogEntryBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): OperatorLogEntryBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  debt(value: number): OperatorLogEntryBuilder {
    this.instance.debt = value;
    return this;
  }
  
  commissionAmount(value: number): OperatorLogEntryBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  userId(value: string): OperatorLogEntryBuilder {
    this.instance.userId = value;
    return this;
  }
  
  receiptId(value: string): OperatorLogEntryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  branchName(value: string): OperatorLogEntryBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  action(value: CashboxAction): OperatorLogEntryBuilder {
    this.instance.action = value;
    return this;
  }
  
  encashmentType(value: EncashmentType): OperatorLogEntryBuilder {
    this.instance.encashmentType = value;
    return this;
  }
  
  notes(value: string): OperatorLogEntryBuilder {
    this.instance.notes = value;
    return this;
  }
  
  transactionId(value: string): OperatorLogEntryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  rawData(value: string): OperatorLogEntryBuilder {
    this.instance.rawData = value;
    return this;
  }
  
  clientData(value: string): OperatorLogEntryBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  contactNum(value: string): OperatorLogEntryBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  externalId(value: string): OperatorLogEntryBuilder {
    this.instance.externalId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class EncashmentSource {
  id: string;
  encashmentId: string;
  sourceName: string;
  accountNumber: string;
  
  constructor() {
  }
  
  static build(): EncashmentSourceBuilder {
    return new EncashmentSourceBuilder();
  }
}

class EncashmentSourceBuilder {
  private instance: EncashmentSource;
  
  constructor() {
    this.instance = new EncashmentSource();
  }
  
  id(value: string): EncashmentSourceBuilder {
    this.instance.id = value;
    return this;
  }
  
  encashmentId(value: string): EncashmentSourceBuilder {
    this.instance.encashmentId = value;
    return this;
  }
  
  sourceName(value: string): EncashmentSourceBuilder {
    this.instance.sourceName = value;
    return this;
  }
  
  accountNumber(value: string): EncashmentSourceBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ProviderCommission {
  providerId: string;
  commission: Commission;
  
  constructor() {
  }
  
  static build(): ProviderCommissionBuilder {
    return new ProviderCommissionBuilder();
  }
}

class ProviderCommissionBuilder {
  private instance: ProviderCommission;
  
  constructor() {
    this.instance = new ProviderCommission();
  }
  
  providerId(value: string): ProviderCommissionBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  commission(value: Commission): ProviderCommissionBuilder {
    this.instance.commission = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Accountant {
  id: string;
  username: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  password: string;
  scopes: Array<Scope>;
  enabled: boolean;
  created: Date;
  
  constructor() {
  }
  
  static build(): AccountantBuilder {
    return new AccountantBuilder();
  }
}

class AccountantBuilder {
  private instance: Accountant;
  
  constructor() {
    this.instance = new Accountant();
  }
  
  id(value: string): AccountantBuilder {
    this.instance.id = value;
    return this;
  }
  
  username(value: string): AccountantBuilder {
    this.instance.username = value;
    return this;
  }
  
  email(value: string): AccountantBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AccountantBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AccountantBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AccountantBuilder {
    this.instance.surname = value;
    return this;
  }
  
  password(value: string): AccountantBuilder {
    this.instance.password = value;
    return this;
  }
  
  scopes(value: Array<Scope>): AccountantBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  enabled(value: boolean): AccountantBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  created(value: Date): AccountantBuilder {
    this.instance.created = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Claimist {
  id: string;
  username: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  password: string;
  scopes: Array<Scope>;
  enabled: boolean;
  created: Date;
  
  constructor() {
  }
  
  static build(): ClaimistBuilder {
    return new ClaimistBuilder();
  }
}

class ClaimistBuilder {
  private instance: Claimist;
  
  constructor() {
    this.instance = new Claimist();
  }
  
  id(value: string): ClaimistBuilder {
    this.instance.id = value;
    return this;
  }
  
  username(value: string): ClaimistBuilder {
    this.instance.username = value;
    return this;
  }
  
  email(value: string): ClaimistBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): ClaimistBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): ClaimistBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): ClaimistBuilder {
    this.instance.surname = value;
    return this;
  }
  
  password(value: string): ClaimistBuilder {
    this.instance.password = value;
    return this;
  }
  
  scopes(value: Array<Scope>): ClaimistBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  enabled(value: boolean): ClaimistBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  created(value: Date): ClaimistBuilder {
    this.instance.created = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Claim {
  id: string;
  transactionId: string;
  cashboxId: string;
  operatorId: string;
  branchId: string;
  providerId: string;
  branchName: string;
  providerName: string;
  operatorName: string;
  receiptId: string;
  purpose: string;
  userNum: string;
  contactNum: string;
  claimOpened: Date;
  completed: Date;
  amount: number;
  cancellationClaimType: CancellationClaimType;
  attachmentKey: string;
  notes: string;
  customerClaimDescription: string;
  customerPassportInfo: string;
  customerPhone: string;
  examinationNotes: string;
  operatorClaimType: OperatorClaimType;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): ClaimBuilder {
    return new ClaimBuilder();
  }
}

class ClaimBuilder {
  private instance: Claim;
  
  constructor() {
    this.instance = new Claim();
  }
  
  id(value: string): ClaimBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): ClaimBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  cashboxId(value: string): ClaimBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  operatorId(value: string): ClaimBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  branchId(value: string): ClaimBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerId(value: string): ClaimBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  branchName(value: string): ClaimBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  providerName(value: string): ClaimBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  operatorName(value: string): ClaimBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  receiptId(value: string): ClaimBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  purpose(value: string): ClaimBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  userNum(value: string): ClaimBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  contactNum(value: string): ClaimBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  claimOpened(value: Date): ClaimBuilder {
    this.instance.claimOpened = value;
    return this;
  }
  
  completed(value: Date): ClaimBuilder {
    this.instance.completed = value;
    return this;
  }
  
  amount(value: number): ClaimBuilder {
    this.instance.amount = value;
    return this;
  }
  
  cancellationClaimType(value: CancellationClaimType): ClaimBuilder {
    this.instance.cancellationClaimType = value;
    return this;
  }
  
  attachmentKey(value: string): ClaimBuilder {
    this.instance.attachmentKey = value;
    return this;
  }
  
  notes(value: string): ClaimBuilder {
    this.instance.notes = value;
    return this;
  }
  
  customerClaimDescription(value: string): ClaimBuilder {
    this.instance.customerClaimDescription = value;
    return this;
  }
  
  customerPassportInfo(value: string): ClaimBuilder {
    this.instance.customerPassportInfo = value;
    return this;
  }
  
  customerPhone(value: string): ClaimBuilder {
    this.instance.customerPhone = value;
    return this;
  }
  
  examinationNotes(value: string): ClaimBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  operatorClaimType(value: OperatorClaimType): ClaimBuilder {
    this.instance.operatorClaimType = value;
    return this;
  }
  
  status(value: ClaimStatus): ClaimBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerClaim {
  id: string;
  transactionId: string;
  providerId: string;
  operatorId: string;
  operatorName: string;
  receiptId: string;
  providerName: string;
  userNum: string;
  amount: number;
  commission: number;
  claimOpened: Date;
  issued: Date;
  completed: Date;
  phone: string;
  forename: string;
  surname: string;
  number: string;
  accNumber: string;
  userType: UserType;
  type: CustomerClaimType;
  notes: string;
  examinationNotes: string;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): CustomerClaimBuilder {
    return new CustomerClaimBuilder();
  }
}

class CustomerClaimBuilder {
  private instance: CustomerClaim;
  
  constructor() {
    this.instance = new CustomerClaim();
  }
  
  id(value: string): CustomerClaimBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): CustomerClaimBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  providerId(value: string): CustomerClaimBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  operatorId(value: string): CustomerClaimBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  operatorName(value: string): CustomerClaimBuilder {
    this.instance.operatorName = value;
    return this;
  }
  
  receiptId(value: string): CustomerClaimBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  providerName(value: string): CustomerClaimBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  userNum(value: string): CustomerClaimBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  amount(value: number): CustomerClaimBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commission(value: number): CustomerClaimBuilder {
    this.instance.commission = value;
    return this;
  }
  
  claimOpened(value: Date): CustomerClaimBuilder {
    this.instance.claimOpened = value;
    return this;
  }
  
  issued(value: Date): CustomerClaimBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): CustomerClaimBuilder {
    this.instance.completed = value;
    return this;
  }
  
  phone(value: string): CustomerClaimBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): CustomerClaimBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): CustomerClaimBuilder {
    this.instance.surname = value;
    return this;
  }
  
  number(value: string): CustomerClaimBuilder {
    this.instance.number = value;
    return this;
  }
  
  accNumber(value: string): CustomerClaimBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  userType(value: UserType): CustomerClaimBuilder {
    this.instance.userType = value;
    return this;
  }
  
  type(value: CustomerClaimType): CustomerClaimBuilder {
    this.instance.type = value;
    return this;
  }
  
  notes(value: string): CustomerClaimBuilder {
    this.instance.notes = value;
    return this;
  }
  
  examinationNotes(value: string): CustomerClaimBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  status(value: ClaimStatus): CustomerClaimBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class UnfreezeAccountClaim {
  id: string;
  claimistId: string;
  claimistName: string;
  isOperatorClaim: boolean;
  customerId: string;
  customerNumber: string;
  phone: string;
  forename: string;
  surname: string;
  attachmentKey: string;
  comment: string;
  examinationNote: string;
  claimOpened: Date;
  userType: UserType;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): UnfreezeAccountClaimBuilder {
    return new UnfreezeAccountClaimBuilder();
  }
}

class UnfreezeAccountClaimBuilder {
  private instance: UnfreezeAccountClaim;
  
  constructor() {
    this.instance = new UnfreezeAccountClaim();
  }
  
  id(value: string): UnfreezeAccountClaimBuilder {
    this.instance.id = value;
    return this;
  }
  
  claimistId(value: string): UnfreezeAccountClaimBuilder {
    this.instance.claimistId = value;
    return this;
  }
  
  claimistName(value: string): UnfreezeAccountClaimBuilder {
    this.instance.claimistName = value;
    return this;
  }
  
  isOperatorClaim(value: boolean): UnfreezeAccountClaimBuilder {
    this.instance.isOperatorClaim = value;
    return this;
  }
  
  customerId(value: string): UnfreezeAccountClaimBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  customerNumber(value: string): UnfreezeAccountClaimBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  phone(value: string): UnfreezeAccountClaimBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): UnfreezeAccountClaimBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): UnfreezeAccountClaimBuilder {
    this.instance.surname = value;
    return this;
  }
  
  attachmentKey(value: string): UnfreezeAccountClaimBuilder {
    this.instance.attachmentKey = value;
    return this;
  }
  
  comment(value: string): UnfreezeAccountClaimBuilder {
    this.instance.comment = value;
    return this;
  }
  
  examinationNote(value: string): UnfreezeAccountClaimBuilder {
    this.instance.examinationNote = value;
    return this;
  }
  
  claimOpened(value: Date): UnfreezeAccountClaimBuilder {
    this.instance.claimOpened = value;
    return this;
  }
  
  userType(value: UserType): UnfreezeAccountClaimBuilder {
    this.instance.userType = value;
    return this;
  }
  
  status(value: ClaimStatus): UnfreezeAccountClaimBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimHistory {
  id: string;
  claimId: string;
  userId: string;
  username: string;
  timestamp: Date;
  comment: string;
  type: ClaimType;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): ClaimHistoryBuilder {
    return new ClaimHistoryBuilder();
  }
}

class ClaimHistoryBuilder {
  private instance: ClaimHistory;
  
  constructor() {
    this.instance = new ClaimHistory();
  }
  
  id(value: string): ClaimHistoryBuilder {
    this.instance.id = value;
    return this;
  }
  
  claimId(value: string): ClaimHistoryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  userId(value: string): ClaimHistoryBuilder {
    this.instance.userId = value;
    return this;
  }
  
  username(value: string): ClaimHistoryBuilder {
    this.instance.username = value;
    return this;
  }
  
  timestamp(value: Date): ClaimHistoryBuilder {
    this.instance.timestamp = value;
    return this;
  }
  
  comment(value: string): ClaimHistoryBuilder {
    this.instance.comment = value;
    return this;
  }
  
  type(value: ClaimType): ClaimHistoryBuilder {
    this.instance.type = value;
    return this;
  }
  
  status(value: ClaimStatus): ClaimHistoryBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerClaimHistory {
  id: string;
  claimId: string;
  userId: string;
  username: string;
  timestamp: Date;
  comment: string;
  type: CustomerClaimType;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): CustomerClaimHistoryBuilder {
    return new CustomerClaimHistoryBuilder();
  }
}

class CustomerClaimHistoryBuilder {
  private instance: CustomerClaimHistory;
  
  constructor() {
    this.instance = new CustomerClaimHistory();
  }
  
  id(value: string): CustomerClaimHistoryBuilder {
    this.instance.id = value;
    return this;
  }
  
  claimId(value: string): CustomerClaimHistoryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  userId(value: string): CustomerClaimHistoryBuilder {
    this.instance.userId = value;
    return this;
  }
  
  username(value: string): CustomerClaimHistoryBuilder {
    this.instance.username = value;
    return this;
  }
  
  timestamp(value: Date): CustomerClaimHistoryBuilder {
    this.instance.timestamp = value;
    return this;
  }
  
  comment(value: string): CustomerClaimHistoryBuilder {
    this.instance.comment = value;
    return this;
  }
  
  type(value: CustomerClaimType): CustomerClaimHistoryBuilder {
    this.instance.type = value;
    return this;
  }
  
  status(value: ClaimStatus): CustomerClaimHistoryBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class UnfreezeAccountClaimHistory {
  id: string;
  claimId: string;
  userId: string;
  username: string;
  timestamp: Date;
  comment: string;
  status: ClaimStatus;
  
  constructor() {
  }
  
  static build(): UnfreezeAccountClaimHistoryBuilder {
    return new UnfreezeAccountClaimHistoryBuilder();
  }
}

class UnfreezeAccountClaimHistoryBuilder {
  private instance: UnfreezeAccountClaimHistory;
  
  constructor() {
    this.instance = new UnfreezeAccountClaimHistory();
  }
  
  id(value: string): UnfreezeAccountClaimHistoryBuilder {
    this.instance.id = value;
    return this;
  }
  
  claimId(value: string): UnfreezeAccountClaimHistoryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  userId(value: string): UnfreezeAccountClaimHistoryBuilder {
    this.instance.userId = value;
    return this;
  }
  
  username(value: string): UnfreezeAccountClaimHistoryBuilder {
    this.instance.username = value;
    return this;
  }
  
  timestamp(value: Date): UnfreezeAccountClaimHistoryBuilder {
    this.instance.timestamp = value;
    return this;
  }
  
  comment(value: string): UnfreezeAccountClaimHistoryBuilder {
    this.instance.comment = value;
    return this;
  }
  
  status(value: ClaimStatus): UnfreezeAccountClaimHistoryBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class IdramPayment {
  id: string;
  transactionId: string;
  data: string;
  
  constructor() {
  }
  
  static build(): IdramPaymentBuilder {
    return new IdramPaymentBuilder();
  }
}

class IdramPaymentBuilder {
  private instance: IdramPayment;
  
  constructor() {
    this.instance = new IdramPayment();
  }
  
  id(value: string): IdramPaymentBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): IdramPaymentBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  data(value: string): IdramPaymentBuilder {
    this.instance.data = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ProviderResponse {
  id: string;
  transactionId: string;
  response: string;
  
  constructor() {
  }
  
  static build(): ProviderResponseBuilder {
    return new ProviderResponseBuilder();
  }
}

class ProviderResponseBuilder {
  private instance: ProviderResponse;
  
  constructor() {
    this.instance = new ProviderResponse();
  }
  
  id(value: string): ProviderResponseBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): ProviderResponseBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  response(value: string): ProviderResponseBuilder {
    this.instance.response = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Card {
  id: string;
  customerId: string;
  state: CardState;
  approvalCode: string;
  createdAt: Date;
  boundAt: Date;
  number: string;
  holder: string;
  expiration: string;
  bank: string;
  arcaBindingId: string;
  arcaOrderId: string;
  arcaOrderNo: string;
  arcaFormUrl: string;
  
  constructor() {
  }
  
  static build(): CardBuilder {
    return new CardBuilder();
  }
}

class CardBuilder {
  private instance: Card;
  
  constructor() {
    this.instance = new Card();
  }
  
  id(value: string): CardBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): CardBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  state(value: CardState): CardBuilder {
    this.instance.state = value;
    return this;
  }
  
  approvalCode(value: string): CardBuilder {
    this.instance.approvalCode = value;
    return this;
  }
  
  createdAt(value: Date): CardBuilder {
    this.instance.createdAt = value;
    return this;
  }
  
  boundAt(value: Date): CardBuilder {
    this.instance.boundAt = value;
    return this;
  }
  
  number(value: string): CardBuilder {
    this.instance.number = value;
    return this;
  }
  
  holder(value: string): CardBuilder {
    this.instance.holder = value;
    return this;
  }
  
  expiration(value: string): CardBuilder {
    this.instance.expiration = value;
    return this;
  }
  
  bank(value: string): CardBuilder {
    this.instance.bank = value;
    return this;
  }
  
  arcaBindingId(value: string): CardBuilder {
    this.instance.arcaBindingId = value;
    return this;
  }
  
  arcaOrderId(value: string): CardBuilder {
    this.instance.arcaOrderId = value;
    return this;
  }
  
  arcaOrderNo(value: string): CardBuilder {
    this.instance.arcaOrderNo = value;
    return this;
  }
  
  arcaFormUrl(value: string): CardBuilder {
    this.instance.arcaFormUrl = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ArcaOrder {
  id: string;
  customerId: string;
  state: OrderState;
  orderId: string;
  approvalCode: string;
  orderNo: string;
  formUrl: string;
  amount: number;
  createdAt: Date;
  raw: string;
  
  constructor() {
  }
  
  static build(): ArcaOrderBuilder {
    return new ArcaOrderBuilder();
  }
}

class ArcaOrderBuilder {
  private instance: ArcaOrder;
  
  constructor() {
    this.instance = new ArcaOrder();
  }
  
  id(value: string): ArcaOrderBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): ArcaOrderBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  state(value: OrderState): ArcaOrderBuilder {
    this.instance.state = value;
    return this;
  }
  
  orderId(value: string): ArcaOrderBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  approvalCode(value: string): ArcaOrderBuilder {
    this.instance.approvalCode = value;
    return this;
  }
  
  orderNo(value: string): ArcaOrderBuilder {
    this.instance.orderNo = value;
    return this;
  }
  
  formUrl(value: string): ArcaOrderBuilder {
    this.instance.formUrl = value;
    return this;
  }
  
  amount(value: number): ArcaOrderBuilder {
    this.instance.amount = value;
    return this;
  }
  
  createdAt(value: Date): ArcaOrderBuilder {
    this.instance.createdAt = value;
    return this;
  }
  
  raw(value: string): ArcaOrderBuilder {
    this.instance.raw = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Document {
  id: string;
  customerId: string;
  docType: DocumentType;
  nameAm: string;
  surnameAm: string;
  middleNameAm: string;
  nameEn: string;
  surnameEn: string;
  sex: string;
  citizen: string;
  documentNumber: string;
  dateOfBirth: Date;
  dateOfIssue: Date;
  dateOfExpiry: Date;
  authority: string;
  address: string;
  city: string;
  country: string;
  documentStatus: DocumentStatus;
  media: string;
  created: Date;
  socCardNumber: string;
  validity: Validity;
  
  constructor() {
  }
  
  static build(): DocumentBuilder {
    return new DocumentBuilder();
  }
}

class DocumentBuilder {
  private instance: Document;
  
  constructor() {
    this.instance = new Document();
  }
  
  id(value: string): DocumentBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): DocumentBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  docType(value: DocumentType): DocumentBuilder {
    this.instance.docType = value;
    return this;
  }
  
  nameAm(value: string): DocumentBuilder {
    this.instance.nameAm = value;
    return this;
  }
  
  surnameAm(value: string): DocumentBuilder {
    this.instance.surnameAm = value;
    return this;
  }
  
  middleNameAm(value: string): DocumentBuilder {
    this.instance.middleNameAm = value;
    return this;
  }
  
  nameEn(value: string): DocumentBuilder {
    this.instance.nameEn = value;
    return this;
  }
  
  surnameEn(value: string): DocumentBuilder {
    this.instance.surnameEn = value;
    return this;
  }
  
  sex(value: string): DocumentBuilder {
    this.instance.sex = value;
    return this;
  }
  
  citizen(value: string): DocumentBuilder {
    this.instance.citizen = value;
    return this;
  }
  
  documentNumber(value: string): DocumentBuilder {
    this.instance.documentNumber = value;
    return this;
  }
  
  dateOfBirth(value: Date): DocumentBuilder {
    this.instance.dateOfBirth = value;
    return this;
  }
  
  dateOfIssue(value: Date): DocumentBuilder {
    this.instance.dateOfIssue = value;
    return this;
  }
  
  dateOfExpiry(value: Date): DocumentBuilder {
    this.instance.dateOfExpiry = value;
    return this;
  }
  
  authority(value: string): DocumentBuilder {
    this.instance.authority = value;
    return this;
  }
  
  address(value: string): DocumentBuilder {
    this.instance.address = value;
    return this;
  }
  
  city(value: string): DocumentBuilder {
    this.instance.city = value;
    return this;
  }
  
  country(value: string): DocumentBuilder {
    this.instance.country = value;
    return this;
  }
  
  documentStatus(value: DocumentStatus): DocumentBuilder {
    this.instance.documentStatus = value;
    return this;
  }
  
  media(value: string): DocumentBuilder {
    this.instance.media = value;
    return this;
  }
  
  created(value: Date): DocumentBuilder {
    this.instance.created = value;
    return this;
  }
  
  socCardNumber(value: string): DocumentBuilder {
    this.instance.socCardNumber = value;
    return this;
  }
  
  validity(value: Validity): DocumentBuilder {
    this.instance.validity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ActivityLog {
  id: string;
  type: ActivityType;
  date: Date;
  customer: string;
  doneBy: string;
  message: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): ActivityLogBuilder {
    return new ActivityLogBuilder();
  }
}

class ActivityLogBuilder {
  private instance: ActivityLog;
  
  constructor() {
    this.instance = new ActivityLog();
  }
  
  id(value: string): ActivityLogBuilder {
    this.instance.id = value;
    return this;
  }
  
  type(value: ActivityType): ActivityLogBuilder {
    this.instance.type = value;
    return this;
  }
  
  date(value: Date): ActivityLogBuilder {
    this.instance.date = value;
    return this;
  }
  
  customer(value: string): ActivityLogBuilder {
    this.instance.customer = value;
    return this;
  }
  
  doneBy(value: string): ActivityLogBuilder {
    this.instance.doneBy = value;
    return this;
  }
  
  message(value: string): ActivityLogBuilder {
    this.instance.message = value;
    return this;
  }
  
  comment(value: string): ActivityLogBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class HangingTransaction {
  id: string;
  paymentId: string;
  issued: Date;
  customerId: string;
  source: string;
  sourceType: SourceType;
  amount: number;
  commissionAmount: number;
  userNum: string;
  additionalData: string;
  rawData: string;
  clientData: string;
  type: UserTransactionType;
  
  constructor() {
  }
  
  static build(): HangingTransactionBuilder {
    return new HangingTransactionBuilder();
  }
}

class HangingTransactionBuilder {
  private instance: HangingTransaction;
  
  constructor() {
    this.instance = new HangingTransaction();
  }
  
  id(value: string): HangingTransactionBuilder {
    this.instance.id = value;
    return this;
  }
  
  paymentId(value: string): HangingTransactionBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  issued(value: Date): HangingTransactionBuilder {
    this.instance.issued = value;
    return this;
  }
  
  customerId(value: string): HangingTransactionBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  source(value: string): HangingTransactionBuilder {
    this.instance.source = value;
    return this;
  }
  
  sourceType(value: SourceType): HangingTransactionBuilder {
    this.instance.sourceType = value;
    return this;
  }
  
  amount(value: number): HangingTransactionBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commissionAmount(value: number): HangingTransactionBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  userNum(value: string): HangingTransactionBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  additionalData(value: string): HangingTransactionBuilder {
    this.instance.additionalData = value;
    return this;
  }
  
  rawData(value: string): HangingTransactionBuilder {
    this.instance.rawData = value;
    return this;
  }
  
  clientData(value: string): HangingTransactionBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  type(value: UserTransactionType): HangingTransactionBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class W2wTransfer {
  id: string;
  fromId: string;
  toId: string;
  amount: number;
  transferType: TransferType;
  transferState: TransferState;
  datemod: Date;
  
  constructor() {
  }
  
  static build(): W2wTransferBuilder {
    return new W2wTransferBuilder();
  }
}

class W2wTransferBuilder {
  private instance: W2wTransfer;
  
  constructor() {
    this.instance = new W2wTransfer();
  }
  
  id(value: string): W2wTransferBuilder {
    this.instance.id = value;
    return this;
  }
  
  fromId(value: string): W2wTransferBuilder {
    this.instance.fromId = value;
    return this;
  }
  
  toId(value: string): W2wTransferBuilder {
    this.instance.toId = value;
    return this;
  }
  
  amount(value: number): W2wTransferBuilder {
    this.instance.amount = value;
    return this;
  }
  
  transferType(value: TransferType): W2wTransferBuilder {
    this.instance.transferType = value;
    return this;
  }
  
  transferState(value: TransferState): W2wTransferBuilder {
    this.instance.transferState = value;
    return this;
  }
  
  datemod(value: Date): W2wTransferBuilder {
    this.instance.datemod = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Country {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): CountryBuilder {
    return new CountryBuilder();
  }
}

class CountryBuilder {
  private instance: Country;
  
  constructor() {
    this.instance = new Country();
  }
  
  id(value: string): CountryBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): CountryBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class City {
  id: string;
  countryId: string;
  name: string;
  
  constructor() {
  }
  
  static build(): CityBuilder {
    return new CityBuilder();
  }
}

class CityBuilder {
  private instance: City;
  
  constructor() {
    this.instance = new City();
  }
  
  id(value: string): CityBuilder {
    this.instance.id = value;
    return this;
  }
  
  countryId(value: string): CityBuilder {
    this.instance.countryId = value;
    return this;
  }
  
  name(value: string): CityBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Notification {
  id: string;
  customerId: string;
  fcmToken: string;
  messageId: string;
  createdAt: Date;
  deliveredAt: Date;
  isDeleted: boolean;
  state: DeliveryState;
  body: string;
  title: string;
  
  constructor() {
  }
  
  static build(): NotificationBuilder {
    return new NotificationBuilder();
  }
}

class NotificationBuilder {
  private instance: Notification;
  
  constructor() {
    this.instance = new Notification();
  }
  
  id(value: string): NotificationBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerId(value: string): NotificationBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  fcmToken(value: string): NotificationBuilder {
    this.instance.fcmToken = value;
    return this;
  }
  
  messageId(value: string): NotificationBuilder {
    this.instance.messageId = value;
    return this;
  }
  
  createdAt(value: Date): NotificationBuilder {
    this.instance.createdAt = value;
    return this;
  }
  
  deliveredAt(value: Date): NotificationBuilder {
    this.instance.deliveredAt = value;
    return this;
  }
  
  isDeleted(value: boolean): NotificationBuilder {
    this.instance.isDeleted = value;
    return this;
  }
  
  state(value: DeliveryState): NotificationBuilder {
    this.instance.state = value;
    return this;
  }
  
  body(value: string): NotificationBuilder {
    this.instance.body = value;
    return this;
  }
  
  title(value: string): NotificationBuilder {
    this.instance.title = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantCashbox {
  id: string;
  merchantId: string;
  merchantBranchId: string;
  certificateName: string;
  name: string;
  qrCode: string;
  isDeleted: boolean;
  
  constructor() {
  }
  
  static build(): MerchantCashboxBuilder {
    return new MerchantCashboxBuilder();
  }
}

class MerchantCashboxBuilder {
  private instance: MerchantCashbox;
  
  constructor() {
    this.instance = new MerchantCashbox();
  }
  
  id(value: string): MerchantCashboxBuilder {
    this.instance.id = value;
    return this;
  }
  
  merchantId(value: string): MerchantCashboxBuilder {
    this.instance.merchantId = value;
    return this;
  }
  
  merchantBranchId(value: string): MerchantCashboxBuilder {
    this.instance.merchantBranchId = value;
    return this;
  }
  
  certificateName(value: string): MerchantCashboxBuilder {
    this.instance.certificateName = value;
    return this;
  }
  
  name(value: string): MerchantCashboxBuilder {
    this.instance.name = value;
    return this;
  }
  
  qrCode(value: string): MerchantCashboxBuilder {
    this.instance.qrCode = value;
    return this;
  }
  
  isDeleted(value: boolean): MerchantCashboxBuilder {
    this.instance.isDeleted = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Merchant {
  id: string;
  name: string;
  logo: string;
  accountNumber: string;
  isDeleted: boolean;
  
  constructor() {
  }
  
  static build(): MerchantBuilder {
    return new MerchantBuilder();
  }
}

class MerchantBuilder {
  private instance: Merchant;
  
  constructor() {
    this.instance = new Merchant();
  }
  
  id(value: string): MerchantBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): MerchantBuilder {
    this.instance.name = value;
    return this;
  }
  
  logo(value: string): MerchantBuilder {
    this.instance.logo = value;
    return this;
  }
  
  accountNumber(value: string): MerchantBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  isDeleted(value: boolean): MerchantBuilder {
    this.instance.isDeleted = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantBranch {
  id: string;
  name: string;
  merchantId: string;
  isDeleted: boolean;
  
  constructor() {
  }
  
  static build(): MerchantBranchBuilder {
    return new MerchantBranchBuilder();
  }
}

class MerchantBranchBuilder {
  private instance: MerchantBranch;
  
  constructor() {
    this.instance = new MerchantBranch();
  }
  
  id(value: string): MerchantBranchBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): MerchantBranchBuilder {
    this.instance.name = value;
    return this;
  }
  
  merchantId(value: string): MerchantBranchBuilder {
    this.instance.merchantId = value;
    return this;
  }
  
  isDeleted(value: boolean): MerchantBranchBuilder {
    this.instance.isDeleted = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantTransaction {
  id: string;
  merchantId: string;
  merchantCashboxId: string;
  issued: Date;
  completed: Date;
  amount: number;
  notes: string;
  customerId: string;
  status: string;
  
  constructor() {
  }
  
  static build(): MerchantTransactionBuilder {
    return new MerchantTransactionBuilder();
  }
}

class MerchantTransactionBuilder {
  private instance: MerchantTransaction;
  
  constructor() {
    this.instance = new MerchantTransaction();
  }
  
  id(value: string): MerchantTransactionBuilder {
    this.instance.id = value;
    return this;
  }
  
  merchantId(value: string): MerchantTransactionBuilder {
    this.instance.merchantId = value;
    return this;
  }
  
  merchantCashboxId(value: string): MerchantTransactionBuilder {
    this.instance.merchantCashboxId = value;
    return this;
  }
  
  issued(value: Date): MerchantTransactionBuilder {
    this.instance.issued = value;
    return this;
  }
  
  completed(value: Date): MerchantTransactionBuilder {
    this.instance.completed = value;
    return this;
  }
  
  amount(value: number): MerchantTransactionBuilder {
    this.instance.amount = value;
    return this;
  }
  
  notes(value: string): MerchantTransactionBuilder {
    this.instance.notes = value;
    return this;
  }
  
  customerId(value: string): MerchantTransactionBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  status(value: string): MerchantTransactionBuilder {
    this.instance.status = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateAdminCommand {
  id: string;
  email: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AdminCreateAdminCommandBuilder {
    return new AdminCreateAdminCommandBuilder();
  }
}

class AdminCreateAdminCommandBuilder {
  private instance: AdminCreateAdminCommand;
  
  constructor() {
    this.instance = new AdminCreateAdminCommand();
  }
  
  id(value: string): AdminCreateAdminCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  email(value: string): AdminCreateAdminCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  password(value: string): AdminCreateAdminCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminAuthenticateCommand {
  authenticationId: string;
  email: string;
  userName: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AdminAuthenticateCommandBuilder {
    return new AdminAuthenticateCommandBuilder();
  }
}

class AdminAuthenticateCommandBuilder {
  private instance: AdminAuthenticateCommand;
  
  constructor() {
    this.instance = new AdminAuthenticateCommand();
  }
  
  authenticationId(value: string): AdminAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  email(value: string): AdminAuthenticateCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  userName(value: string): AdminAuthenticateCommandBuilder {
    this.instance.userName = value;
    return this;
  }
  
  password(value: string): AdminAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateBranchCommand {
  id: string;
  name: string;
  code: string;
  address: string;
  desc: string;
  policyId: string;
  
  constructor() {
  }
  
  static build(): AdminCreateBranchCommandBuilder {
    return new AdminCreateBranchCommandBuilder();
  }
}

class AdminCreateBranchCommandBuilder {
  private instance: AdminCreateBranchCommand;
  
  constructor() {
    this.instance = new AdminCreateBranchCommand();
  }
  
  id(value: string): AdminCreateBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminCreateBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  code(value: string): AdminCreateBranchCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  address(value: string): AdminCreateBranchCommandBuilder {
    this.instance.address = value;
    return this;
  }
  
  desc(value: string): AdminCreateBranchCommandBuilder {
    this.instance.desc = value;
    return this;
  }
  
  policyId(value: string): AdminCreateBranchCommandBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateBranchCommand {
  id: string;
  name: string;
  code: string;
  address: string;
  desc: string;
  policyId: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateBranchCommandBuilder {
    return new AdminUpdateBranchCommandBuilder();
  }
}

class AdminUpdateBranchCommandBuilder {
  private instance: AdminUpdateBranchCommand;
  
  constructor() {
    this.instance = new AdminUpdateBranchCommand();
  }
  
  id(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  code(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  address(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.address = value;
    return this;
  }
  
  desc(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.desc = value;
    return this;
  }
  
  policyId(value: string): AdminUpdateBranchCommandBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminEnableBranchCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminEnableBranchCommandBuilder {
    return new AdminEnableBranchCommandBuilder();
  }
}

class AdminEnableBranchCommandBuilder {
  private instance: AdminEnableBranchCommand;
  
  constructor() {
    this.instance = new AdminEnableBranchCommand();
  }
  
  id(value: string): AdminEnableBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDisableBranchCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDisableBranchCommandBuilder {
    return new AdminDisableBranchCommandBuilder();
  }
}

class AdminDisableBranchCommandBuilder {
  private instance: AdminDisableBranchCommand;
  
  constructor() {
    this.instance = new AdminDisableBranchCommand();
  }
  
  id(value: string): AdminDisableBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateAccountantCommand {
  id: string;
  username: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  password: string;
  scopes: Array<Scope>;
  
  constructor() {
  }
  
  static build(): AdminCreateAccountantCommandBuilder {
    return new AdminCreateAccountantCommandBuilder();
  }
}

class AdminCreateAccountantCommandBuilder {
  private instance: AdminCreateAccountantCommand;
  
  constructor() {
    this.instance = new AdminCreateAccountantCommand();
  }
  
  id(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  username(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.username = value;
    return this;
  }
  
  email(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  password(value: string): AdminCreateAccountantCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  scopes(value: Array<Scope>): AdminCreateAccountantCommandBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateAccountantCommand {
  id: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  scopes: Array<Scope>;
  
  constructor() {
  }
  
  static build(): AdminUpdateAccountantCommandBuilder {
    return new AdminUpdateAccountantCommandBuilder();
  }
}

class AdminUpdateAccountantCommandBuilder {
  private instance: AdminUpdateAccountantCommand;
  
  constructor() {
    this.instance = new AdminUpdateAccountantCommand();
  }
  
  id(value: string): AdminUpdateAccountantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  email(value: string): AdminUpdateAccountantCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AdminUpdateAccountantCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AdminUpdateAccountantCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AdminUpdateAccountantCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  scopes(value: Array<Scope>): AdminUpdateAccountantCommandBuilder {
    this.instance.scopes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDisableAccountantCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDisableAccountantCommandBuilder {
    return new AdminDisableAccountantCommandBuilder();
  }
}

class AdminDisableAccountantCommandBuilder {
  private instance: AdminDisableAccountantCommand;
  
  constructor() {
    this.instance = new AdminDisableAccountantCommand();
  }
  
  id(value: string): AdminDisableAccountantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminEnableAccountantCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminEnableAccountantCommandBuilder {
    return new AdminEnableAccountantCommandBuilder();
  }
}

class AdminEnableAccountantCommandBuilder {
  private instance: AdminEnableAccountantCommand;
  
  constructor() {
    this.instance = new AdminEnableAccountantCommand();
  }
  
  id(value: string): AdminEnableAccountantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminAccountantResetPasswordCommand {
  id: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AdminAccountantResetPasswordCommandBuilder {
    return new AdminAccountantResetPasswordCommandBuilder();
  }
}

class AdminAccountantResetPasswordCommandBuilder {
  private instance: AdminAccountantResetPasswordCommand;
  
  constructor() {
    this.instance = new AdminAccountantResetPasswordCommand();
  }
  
  id(value: string): AdminAccountantResetPasswordCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  password(value: string): AdminAccountantResetPasswordCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAccountantQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetAccountantQueryBuilder {
    return new AdminGetAccountantQueryBuilder();
  }
}

class AdminGetAccountantQueryBuilder {
  private instance: AdminGetAccountantQuery;
  
  constructor() {
    this.instance = new AdminGetAccountantQuery();
  }
  
  id(value: string): AdminGetAccountantQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAccountantsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetAccountantsQueryBuilder {
    return new AdminGetAccountantsQueryBuilder();
  }
}

class AdminGetAccountantsQueryBuilder {
  private instance: AdminGetAccountantsQuery;
  
  constructor() {
    this.instance = new AdminGetAccountantsQuery();
  }
  
  page(value: Page): AdminGetAccountantsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetAccountantsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAccessTokenQuery {
  authenticationId: string;
  
  constructor() {
  }
  
  static build(): AdminGetAccessTokenQueryBuilder {
    return new AdminGetAccessTokenQueryBuilder();
  }
}

class AdminGetAccessTokenQueryBuilder {
  private instance: AdminGetAccessTokenQuery;
  
  constructor() {
    this.instance = new AdminGetAccessTokenQuery();
  }
  
  authenticationId(value: string): AdminGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetBranchQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetBranchQueryBuilder {
    return new AdminGetBranchQueryBuilder();
  }
}

class AdminGetBranchQueryBuilder {
  private instance: AdminGetBranchQuery;
  
  constructor() {
    this.instance = new AdminGetBranchQuery();
  }
  
  id(value: string): AdminGetBranchQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminFindBranchesQuery {
  name: string;
  address: string;
  desc: string;
  enabled: boolean;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminFindBranchesQueryBuilder {
    return new AdminFindBranchesQueryBuilder();
  }
}

class AdminFindBranchesQueryBuilder {
  private instance: AdminFindBranchesQuery;
  
  constructor() {
    this.instance = new AdminFindBranchesQuery();
  }
  
  name(value: string): AdminFindBranchesQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  address(value: string): AdminFindBranchesQueryBuilder {
    this.instance.address = value;
    return this;
  }
  
  desc(value: string): AdminFindBranchesQueryBuilder {
    this.instance.desc = value;
    return this;
  }
  
  enabled(value: boolean): AdminFindBranchesQueryBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  page(value: Page): AdminFindBranchesQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminFindBranchesQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateCashboxCommand {
  id: string;
  name: string;
  accountNumber: string;
  branch: string;
  note: string;
  certificateId: string;
  exceedClosingLimit: boolean;
  allowPosTerminal: boolean;
  posAccountNumber: string;
  
  constructor() {
  }
  
  static build(): AdminCreateCashboxCommandBuilder {
    return new AdminCreateCashboxCommandBuilder();
  }
}

class AdminCreateCashboxCommandBuilder {
  private instance: AdminCreateCashboxCommand;
  
  constructor() {
    this.instance = new AdminCreateCashboxCommand();
  }
  
  id(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  accountNumber(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  branch(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.branch = value;
    return this;
  }
  
  note(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.note = value;
    return this;
  }
  
  certificateId(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.certificateId = value;
    return this;
  }
  
  exceedClosingLimit(value: boolean): AdminCreateCashboxCommandBuilder {
    this.instance.exceedClosingLimit = value;
    return this;
  }
  
  allowPosTerminal(value: boolean): AdminCreateCashboxCommandBuilder {
    this.instance.allowPosTerminal = value;
    return this;
  }
  
  posAccountNumber(value: string): AdminCreateCashboxCommandBuilder {
    this.instance.posAccountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateCashboxCommand {
  id: string;
  name: string;
  accountNumber: string;
  branch: string;
  note: string;
  certificateId: string;
  exceedClosingLimit: boolean;
  allowPosTerminal: boolean;
  posAccountNumber: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateCashboxCommandBuilder {
    return new AdminUpdateCashboxCommandBuilder();
  }
}

class AdminUpdateCashboxCommandBuilder {
  private instance: AdminUpdateCashboxCommand;
  
  constructor() {
    this.instance = new AdminUpdateCashboxCommand();
  }
  
  id(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  accountNumber(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  branch(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.branch = value;
    return this;
  }
  
  note(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.note = value;
    return this;
  }
  
  certificateId(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.certificateId = value;
    return this;
  }
  
  exceedClosingLimit(value: boolean): AdminUpdateCashboxCommandBuilder {
    this.instance.exceedClosingLimit = value;
    return this;
  }
  
  allowPosTerminal(value: boolean): AdminUpdateCashboxCommandBuilder {
    this.instance.allowPosTerminal = value;
    return this;
  }
  
  posAccountNumber(value: string): AdminUpdateCashboxCommandBuilder {
    this.instance.posAccountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminEnableCashboxCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminEnableCashboxCommandBuilder {
    return new AdminEnableCashboxCommandBuilder();
  }
}

class AdminEnableCashboxCommandBuilder {
  private instance: AdminEnableCashboxCommand;
  
  constructor() {
    this.instance = new AdminEnableCashboxCommand();
  }
  
  id(value: string): AdminEnableCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDisableCashboxCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDisableCashboxCommandBuilder {
    return new AdminDisableCashboxCommandBuilder();
  }
}

class AdminDisableCashboxCommandBuilder {
  private instance: AdminDisableCashboxCommand;
  
  constructor() {
    this.instance = new AdminDisableCashboxCommand();
  }
  
  id(value: string): AdminDisableCashboxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateOperatorCommand {
  id: string;
  userName: string;
  email: string;
  phone: string;
  forename: string;
  surname: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AdminCreateOperatorCommandBuilder {
    return new AdminCreateOperatorCommandBuilder();
  }
}

class AdminCreateOperatorCommandBuilder {
  private instance: AdminCreateOperatorCommand;
  
  constructor() {
    this.instance = new AdminCreateOperatorCommand();
  }
  
  id(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  userName(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.userName = value;
    return this;
  }
  
  email(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  password(value: string): AdminCreateOperatorCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateOperatorCommand {
  id: string;
  forename: string;
  surname: string;
  email: string;
  phone: string;
  scope: Array<Scope>;
  
  constructor() {
  }
  
  static build(): AdminUpdateOperatorCommandBuilder {
    return new AdminUpdateOperatorCommandBuilder();
  }
}

class AdminUpdateOperatorCommandBuilder {
  private instance: AdminUpdateOperatorCommand;
  
  constructor() {
    this.instance = new AdminUpdateOperatorCommand();
  }
  
  id(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  forename(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  email(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AdminUpdateOperatorCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  scope(value: Array<Scope>): AdminUpdateOperatorCommandBuilder {
    this.instance.scope = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminResetPasswordCommand {
  id: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AdminResetPasswordCommandBuilder {
    return new AdminResetPasswordCommandBuilder();
  }
}

class AdminResetPasswordCommandBuilder {
  private instance: AdminResetPasswordCommand;
  
  constructor() {
    this.instance = new AdminResetPasswordCommand();
  }
  
  id(value: string): AdminResetPasswordCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  password(value: string): AdminResetPasswordCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminAttachOperatorCommand {
  cashboxId: string;
  operatorId: string;
  
  constructor() {
  }
  
  static build(): AdminAttachOperatorCommandBuilder {
    return new AdminAttachOperatorCommandBuilder();
  }
}

class AdminAttachOperatorCommandBuilder {
  private instance: AdminAttachOperatorCommand;
  
  constructor() {
    this.instance = new AdminAttachOperatorCommand();
  }
  
  cashboxId(value: string): AdminAttachOperatorCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  operatorId(value: string): AdminAttachOperatorCommandBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDetachOperatorCommand {
  cashboxId: string;
  operatorId: string;
  
  constructor() {
  }
  
  static build(): AdminDetachOperatorCommandBuilder {
    return new AdminDetachOperatorCommandBuilder();
  }
}

class AdminDetachOperatorCommandBuilder {
  private instance: AdminDetachOperatorCommand;
  
  constructor() {
    this.instance = new AdminDetachOperatorCommand();
  }
  
  cashboxId(value: string): AdminDetachOperatorCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  operatorId(value: string): AdminDetachOperatorCommandBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateProviderCommand {
  id: string;
  name: string;
  accountNumber: string;
  accountNumberMobile: string;
  commissionAccountNumber: string;
  commissionAccountNumberMobile: string;
  amountMin: number;
  amountMax: number;
  desc: string;
  paymentPurpose: string;
  paymentWalletPurpose: string;
  useFixedAmount: boolean;
  labels: Array<string>;
  invoiceNote: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateProviderCommandBuilder {
    return new AdminUpdateProviderCommandBuilder();
  }
}

class AdminUpdateProviderCommandBuilder {
  private instance: AdminUpdateProviderCommand;
  
  constructor() {
    this.instance = new AdminUpdateProviderCommand();
  }
  
  id(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  accountNumber(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  accountNumberMobile(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.accountNumberMobile = value;
    return this;
  }
  
  commissionAccountNumber(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.commissionAccountNumber = value;
    return this;
  }
  
  commissionAccountNumberMobile(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.commissionAccountNumberMobile = value;
    return this;
  }
  
  amountMin(value: number): AdminUpdateProviderCommandBuilder {
    this.instance.amountMin = value;
    return this;
  }
  
  amountMax(value: number): AdminUpdateProviderCommandBuilder {
    this.instance.amountMax = value;
    return this;
  }
  
  desc(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.desc = value;
    return this;
  }
  
  paymentPurpose(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.paymentPurpose = value;
    return this;
  }
  
  paymentWalletPurpose(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.paymentWalletPurpose = value;
    return this;
  }
  
  useFixedAmount(value: boolean): AdminUpdateProviderCommandBuilder {
    this.instance.useFixedAmount = value;
    return this;
  }
  
  labels(value: Array<string>): AdminUpdateProviderCommandBuilder {
    this.instance.labels = value;
    return this;
  }
  
  invoiceNote(value: string): AdminUpdateProviderCommandBuilder {
    this.instance.invoiceNote = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminEnableProviderCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminEnableProviderCommandBuilder {
    return new AdminEnableProviderCommandBuilder();
  }
}

class AdminEnableProviderCommandBuilder {
  private instance: AdminEnableProviderCommand;
  
  constructor() {
    this.instance = new AdminEnableProviderCommand();
  }
  
  id(value: string): AdminEnableProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDisableProviderCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDisableProviderCommandBuilder {
    return new AdminDisableProviderCommandBuilder();
  }
}

class AdminDisableProviderCommandBuilder {
  private instance: AdminDisableProviderCommand;
  
  constructor() {
    this.instance = new AdminDisableProviderCommand();
  }
  
  id(value: string): AdminDisableProviderCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateProviderLabelCommand {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;
  
  constructor() {
  }
  
  static build(): AdminCreateProviderLabelCommandBuilder {
    return new AdminCreateProviderLabelCommandBuilder();
  }
}

class AdminCreateProviderLabelCommandBuilder {
  private instance: AdminCreateProviderLabelCommand;
  
  constructor() {
    this.instance = new AdminCreateProviderLabelCommand();
  }
  
  id(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  icon(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.icon = value;
    return this;
  }
  
  name(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  label(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.label = value;
    return this;
  }
  
  desc(value: string): AdminCreateProviderLabelCommandBuilder {
    this.instance.desc = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateProviderLabelCommand {
  id: string;
  icon: string;
  name: string;
  label: string;
  desc: string;
  pos: number;
  
  constructor() {
  }
  
  static build(): AdminUpdateProviderLabelCommandBuilder {
    return new AdminUpdateProviderLabelCommandBuilder();
  }
}

class AdminUpdateProviderLabelCommandBuilder {
  private instance: AdminUpdateProviderLabelCommand;
  
  constructor() {
    this.instance = new AdminUpdateProviderLabelCommand();
  }
  
  id(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  icon(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.icon = value;
    return this;
  }
  
  name(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  label(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.label = value;
    return this;
  }
  
  desc(value: string): AdminUpdateProviderLabelCommandBuilder {
    this.instance.desc = value;
    return this;
  }
  
  pos(value: number): AdminUpdateProviderLabelCommandBuilder {
    this.instance.pos = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminSetOrderingProviderLabelCommand {
  id: string;
  pos: number;
  
  constructor() {
  }
  
  static build(): AdminSetOrderingProviderLabelCommandBuilder {
    return new AdminSetOrderingProviderLabelCommandBuilder();
  }
}

class AdminSetOrderingProviderLabelCommandBuilder {
  private instance: AdminSetOrderingProviderLabelCommand;
  
  constructor() {
    this.instance = new AdminSetOrderingProviderLabelCommand();
  }
  
  id(value: string): AdminSetOrderingProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  pos(value: number): AdminSetOrderingProviderLabelCommandBuilder {
    this.instance.pos = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteProviderLabelCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteProviderLabelCommandBuilder {
    return new AdminDeleteProviderLabelCommandBuilder();
  }
}

class AdminDeleteProviderLabelCommandBuilder {
  private instance: AdminDeleteProviderLabelCommand;
  
  constructor() {
    this.instance = new AdminDeleteProviderLabelCommand();
  }
  
  id(value: string): AdminDeleteProviderLabelCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminSaveProviderLabelsCommand {
  json: string;
  
  constructor() {
  }
  
  static build(): AdminSaveProviderLabelsCommandBuilder {
    return new AdminSaveProviderLabelsCommandBuilder();
  }
}

class AdminSaveProviderLabelsCommandBuilder {
  private instance: AdminSaveProviderLabelsCommand;
  
  constructor() {
    this.instance = new AdminSaveProviderLabelsCommand();
  }
  
  json(value: string): AdminSaveProviderLabelsCommandBuilder {
    this.instance.json = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminEnableOperatorCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminEnableOperatorCommandBuilder {
    return new AdminEnableOperatorCommandBuilder();
  }
}

class AdminEnableOperatorCommandBuilder {
  private instance: AdminEnableOperatorCommand;
  
  constructor() {
    this.instance = new AdminEnableOperatorCommand();
  }
  
  id(value: string): AdminEnableOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDisableOperatorCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDisableOperatorCommandBuilder {
    return new AdminDisableOperatorCommandBuilder();
  }
}

class AdminDisableOperatorCommandBuilder {
  private instance: AdminDisableOperatorCommand;
  
  constructor() {
    this.instance = new AdminDisableOperatorCommand();
  }
  
  id(value: string): AdminDisableOperatorCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCashboxQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetCashboxQueryBuilder {
    return new AdminGetCashboxQueryBuilder();
  }
}

class AdminGetCashboxQueryBuilder {
  private instance: AdminGetCashboxQuery;
  
  constructor() {
    this.instance = new AdminGetCashboxQuery();
  }
  
  id(value: string): AdminGetCashboxQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCashboxesQuery {
  name: string;
  branchId: string;
  enabled: boolean;
  loggedInOperatorId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetCashboxesQueryBuilder {
    return new AdminGetCashboxesQueryBuilder();
  }
}

class AdminGetCashboxesQueryBuilder {
  private instance: AdminGetCashboxesQuery;
  
  constructor() {
    this.instance = new AdminGetCashboxesQuery();
  }
  
  name(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  branchId(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  enabled(value: boolean): AdminGetCashboxesQueryBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  loggedInOperatorId(value: string): AdminGetCashboxesQueryBuilder {
    this.instance.loggedInOperatorId = value;
    return this;
  }
  
  page(value: Page): AdminGetCashboxesQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetCashboxesQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetProviderQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetProviderQueryBuilder {
    return new AdminGetProviderQueryBuilder();
  }
}

class AdminGetProviderQueryBuilder {
  private instance: AdminGetProviderQuery;
  
  constructor() {
    this.instance = new AdminGetProviderQuery();
  }
  
  id(value: string): AdminGetProviderQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetProvidersQuery {
  label: string;
  name: string;
  enabled: boolean;
  hasFixedAmount: boolean;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetProvidersQueryBuilder {
    return new AdminGetProvidersQueryBuilder();
  }
}

class AdminGetProvidersQueryBuilder {
  private instance: AdminGetProvidersQuery;
  
  constructor() {
    this.instance = new AdminGetProvidersQuery();
  }
  
  label(value: string): AdminGetProvidersQueryBuilder {
    this.instance.label = value;
    return this;
  }
  
  name(value: string): AdminGetProvidersQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  enabled(value: boolean): AdminGetProvidersQueryBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  hasFixedAmount(value: boolean): AdminGetProvidersQueryBuilder {
    this.instance.hasFixedAmount = value;
    return this;
  }
  
  page(value: Page): AdminGetProvidersQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetProvidersQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetOperatorQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetOperatorQueryBuilder {
    return new AdminGetOperatorQueryBuilder();
  }
}

class AdminGetOperatorQueryBuilder {
  private instance: AdminGetOperatorQuery;
  
  constructor() {
    this.instance = new AdminGetOperatorQuery();
  }
  
  id(value: string): AdminGetOperatorQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetOperatorsQuery {
  forename: string;
  surname: string;
  username: string;
  email: string;
  phone: string;
  enabled: boolean;
  cashboxId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetOperatorsQueryBuilder {
    return new AdminGetOperatorsQueryBuilder();
  }
}

class AdminGetOperatorsQueryBuilder {
  private instance: AdminGetOperatorsQuery;
  
  constructor() {
    this.instance = new AdminGetOperatorsQuery();
  }
  
  forename(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.surname = value;
    return this;
  }
  
  username(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.username = value;
    return this;
  }
  
  email(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.email = value;
    return this;
  }
  
  phone(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  enabled(value: boolean): AdminGetOperatorsQueryBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  cashboxId(value: string): AdminGetOperatorsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  page(value: Page): AdminGetOperatorsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetOperatorsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetProviderLabelQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetProviderLabelQueryBuilder {
    return new AdminGetProviderLabelQueryBuilder();
  }
}

class AdminGetProviderLabelQueryBuilder {
  private instance: AdminGetProviderLabelQuery;
  
  constructor() {
    this.instance = new AdminGetProviderLabelQuery();
  }
  
  id(value: string): AdminGetProviderLabelQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetProviderLabelsQuery {
  name: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetProviderLabelsQueryBuilder {
    return new AdminGetProviderLabelsQueryBuilder();
  }
}

class AdminGetProviderLabelsQueryBuilder {
  private instance: AdminGetProviderLabelsQuery;
  
  constructor() {
    this.instance = new AdminGetProviderLabelsQuery();
  }
  
  name(value: string): AdminGetProviderLabelsQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  page(value: Page): AdminGetProviderLabelsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetProviderLabelsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetProviderLabelTreeQuery {
  
  constructor() {
  }
  
  static build(): AdminGetProviderLabelTreeQueryBuilder {
    return new AdminGetProviderLabelTreeQueryBuilder();
  }
}

class AdminGetProviderLabelTreeQueryBuilder {
  private instance: AdminGetProviderLabelTreeQuery;
  
  constructor() {
    this.instance = new AdminGetProviderLabelTreeQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminSaveCommissionCommand {
  providerId: string;
  policyId: string;
  ranges: Array<CommissionRange>;
  
  constructor() {
  }
  
  static build(): AdminSaveCommissionCommandBuilder {
    return new AdminSaveCommissionCommandBuilder();
  }
}

class AdminSaveCommissionCommandBuilder {
  private instance: AdminSaveCommissionCommand;
  
  constructor() {
    this.instance = new AdminSaveCommissionCommand();
  }
  
  providerId(value: string): AdminSaveCommissionCommandBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  policyId(value: string): AdminSaveCommissionCommandBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  ranges(value: Array<CommissionRange>): AdminSaveCommissionCommandBuilder {
    this.instance.ranges = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteCommissionCommand {
  providerId: string;
  policyId: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteCommissionCommandBuilder {
    return new AdminDeleteCommissionCommandBuilder();
  }
}

class AdminDeleteCommissionCommandBuilder {
  private instance: AdminDeleteCommissionCommand;
  
  constructor() {
    this.instance = new AdminDeleteCommissionCommand();
  }
  
  providerId(value: string): AdminDeleteCommissionCommandBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  policyId(value: string): AdminDeleteCommissionCommandBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCommissionQuery {
  providerId: string;
  policyId: string;
  enabled: boolean;
  
  constructor() {
  }
  
  static build(): AdminGetCommissionQueryBuilder {
    return new AdminGetCommissionQueryBuilder();
  }
}

class AdminGetCommissionQueryBuilder {
  private instance: AdminGetCommissionQuery;
  
  constructor() {
    this.instance = new AdminGetCommissionQuery();
  }
  
  providerId(value: string): AdminGetCommissionQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  policyId(value: string): AdminGetCommissionQueryBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  enabled(value: boolean): AdminGetCommissionQueryBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCommissionsQuery {
  providerId: string;
  policyId: string;
  approved: boolean;
  action: boolean;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetCommissionsQueryBuilder {
    return new AdminGetCommissionsQueryBuilder();
  }
}

class AdminGetCommissionsQueryBuilder {
  private instance: AdminGetCommissionsQuery;
  
  constructor() {
    this.instance = new AdminGetCommissionsQuery();
  }
  
  providerId(value: string): AdminGetCommissionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  policyId(value: string): AdminGetCommissionsQueryBuilder {
    this.instance.policyId = value;
    return this;
  }
  
  approved(value: boolean): AdminGetCommissionsQueryBuilder {
    this.instance.approved = value;
    return this;
  }
  
  action(value: boolean): AdminGetCommissionsQueryBuilder {
    this.instance.action = value;
    return this;
  }
  
  page(value: Page): AdminGetCommissionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetCommissionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateCommissionPolicyCommand {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): AdminCreateCommissionPolicyCommandBuilder {
    return new AdminCreateCommissionPolicyCommandBuilder();
  }
}

class AdminCreateCommissionPolicyCommandBuilder {
  private instance: AdminCreateCommissionPolicyCommand;
  
  constructor() {
    this.instance = new AdminCreateCommissionPolicyCommand();
  }
  
  id(value: string): AdminCreateCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminCreateCommissionPolicyCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateCommissionPolicyCommand {
  id: string;
  name: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateCommissionPolicyCommandBuilder {
    return new AdminUpdateCommissionPolicyCommandBuilder();
  }
}

class AdminUpdateCommissionPolicyCommandBuilder {
  private instance: AdminUpdateCommissionPolicyCommand;
  
  constructor() {
    this.instance = new AdminUpdateCommissionPolicyCommand();
  }
  
  id(value: string): AdminUpdateCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateCommissionPolicyCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteCommissionPolicyCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteCommissionPolicyCommandBuilder {
    return new AdminDeleteCommissionPolicyCommandBuilder();
  }
}

class AdminDeleteCommissionPolicyCommandBuilder {
  private instance: AdminDeleteCommissionPolicyCommand;
  
  constructor() {
    this.instance = new AdminDeleteCommissionPolicyCommand();
  }
  
  id(value: string): AdminDeleteCommissionPolicyCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminApproveCommissionCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminApproveCommissionCommandBuilder {
    return new AdminApproveCommissionCommandBuilder();
  }
}

class AdminApproveCommissionCommandBuilder {
  private instance: AdminApproveCommissionCommand;
  
  constructor() {
    this.instance = new AdminApproveCommissionCommand();
  }
  
  id(value: string): AdminApproveCommissionCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCommissionPolicyQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetCommissionPolicyQueryBuilder {
    return new AdminGetCommissionPolicyQueryBuilder();
  }
}

class AdminGetCommissionPolicyQueryBuilder {
  private instance: AdminGetCommissionPolicyQuery;
  
  constructor() {
    this.instance = new AdminGetCommissionPolicyQuery();
  }
  
  id(value: string): AdminGetCommissionPolicyQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetCommissionPoliciesQuery {
  name: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AdminGetCommissionPoliciesQueryBuilder {
    return new AdminGetCommissionPoliciesQueryBuilder();
  }
}

class AdminGetCommissionPoliciesQueryBuilder {
  private instance: AdminGetCommissionPoliciesQuery;
  
  constructor() {
    this.instance = new AdminGetCommissionPoliciesQuery();
  }
  
  name(value: string): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  page(value: Page): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AdminGetCommissionPoliciesQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetBranchCommissionQuery {
  branchId: string;
  providerId: string;
  
  constructor() {
  }
  
  static build(): AdminGetBranchCommissionQueryBuilder {
    return new AdminGetBranchCommissionQueryBuilder();
  }
}

class AdminGetBranchCommissionQueryBuilder {
  private instance: AdminGetBranchCommissionQuery;
  
  constructor() {
    this.instance = new AdminGetBranchCommissionQuery();
  }
  
  branchId(value: string): AdminGetBranchCommissionQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerId(value: string): AdminGetBranchCommissionQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetOperatorScopesQuery {
  operatorId: string;
  
  constructor() {
  }
  
  static build(): AdminGetOperatorScopesQueryBuilder {
    return new AdminGetOperatorScopesQueryBuilder();
  }
}

class AdminGetOperatorScopesQueryBuilder {
  private instance: AdminGetOperatorScopesQuery;
  
  constructor() {
    this.instance = new AdminGetOperatorScopesQuery();
  }
  
  operatorId(value: string): AdminGetOperatorScopesQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetScopesQuery {
  
  constructor() {
  }
  
  static build(): AdminGetScopesQueryBuilder {
    return new AdminGetScopesQueryBuilder();
  }
}

class AdminGetScopesQueryBuilder {
  private instance: AdminGetScopesQuery;
  
  constructor() {
    this.instance = new AdminGetScopesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetMerchantQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetMerchantQueryBuilder {
    return new AdminGetMerchantQueryBuilder();
  }
}

class AdminGetMerchantQueryBuilder {
  private instance: AdminGetMerchantQuery;
  
  constructor() {
    this.instance = new AdminGetMerchantQuery();
  }
  
  id(value: string): AdminGetMerchantQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAllMerchantsQuery {
  page: Page;
  
  constructor() {
  }
  
  static build(): AdminGetAllMerchantsQueryBuilder {
    return new AdminGetAllMerchantsQueryBuilder();
  }
}

class AdminGetAllMerchantsQueryBuilder {
  private instance: AdminGetAllMerchantsQuery;
  
  constructor() {
    this.instance = new AdminGetAllMerchantsQuery();
  }
  
  page(value: Page): AdminGetAllMerchantsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateMerchantCommand {
  id: string;
  name: string;
  logo: string;
  accountNumber: string;
  
  constructor() {
  }
  
  static build(): AdminCreateMerchantCommandBuilder {
    return new AdminCreateMerchantCommandBuilder();
  }
}

class AdminCreateMerchantCommandBuilder {
  private instance: AdminCreateMerchantCommand;
  
  constructor() {
    this.instance = new AdminCreateMerchantCommand();
  }
  
  id(value: string): AdminCreateMerchantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminCreateMerchantCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  logo(value: string): AdminCreateMerchantCommandBuilder {
    this.instance.logo = value;
    return this;
  }
  
  accountNumber(value: string): AdminCreateMerchantCommandBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateMerchantCommand {
  id: string;
  name: string;
  logo: string;
  accountNumber: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateMerchantCommandBuilder {
    return new AdminUpdateMerchantCommandBuilder();
  }
}

class AdminUpdateMerchantCommandBuilder {
  private instance: AdminUpdateMerchantCommand;
  
  constructor() {
    this.instance = new AdminUpdateMerchantCommand();
  }
  
  id(value: string): AdminUpdateMerchantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateMerchantCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  logo(value: string): AdminUpdateMerchantCommandBuilder {
    this.instance.logo = value;
    return this;
  }
  
  accountNumber(value: string): AdminUpdateMerchantCommandBuilder {
    this.instance.accountNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteMerchantCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteMerchantCommandBuilder {
    return new AdminDeleteMerchantCommandBuilder();
  }
}

class AdminDeleteMerchantCommandBuilder {
  private instance: AdminDeleteMerchantCommand;
  
  constructor() {
    this.instance = new AdminDeleteMerchantCommand();
  }
  
  id(value: string): AdminDeleteMerchantCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetMerchantCashboxQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetMerchantCashboxQueryBuilder {
    return new AdminGetMerchantCashboxQueryBuilder();
  }
}

class AdminGetMerchantCashboxQueryBuilder {
  private instance: AdminGetMerchantCashboxQuery;
  
  constructor() {
    this.instance = new AdminGetMerchantCashboxQuery();
  }
  
  id(value: string): AdminGetMerchantCashboxQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAllMerchantCashboxesQuery {
  page: Page;
  
  constructor() {
  }
  
  static build(): AdminGetAllMerchantCashboxesQueryBuilder {
    return new AdminGetAllMerchantCashboxesQueryBuilder();
  }
}

class AdminGetAllMerchantCashboxesQueryBuilder {
  private instance: AdminGetAllMerchantCashboxesQuery;
  
  constructor() {
    this.instance = new AdminGetAllMerchantCashboxesQuery();
  }
  
  page(value: Page): AdminGetAllMerchantCashboxesQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateMerchantCashBoxCommand {
  id: string;
  merchantBranchId: string;
  name: string;
  qrCode: string;
  certificateName: string;
  
  constructor() {
  }
  
  static build(): AdminCreateMerchantCashBoxCommandBuilder {
    return new AdminCreateMerchantCashBoxCommandBuilder();
  }
}

class AdminCreateMerchantCashBoxCommandBuilder {
  private instance: AdminCreateMerchantCashBoxCommand;
  
  constructor() {
    this.instance = new AdminCreateMerchantCashBoxCommand();
  }
  
  id(value: string): AdminCreateMerchantCashBoxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  merchantBranchId(value: string): AdminCreateMerchantCashBoxCommandBuilder {
    this.instance.merchantBranchId = value;
    return this;
  }
  
  name(value: string): AdminCreateMerchantCashBoxCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  qrCode(value: string): AdminCreateMerchantCashBoxCommandBuilder {
    this.instance.qrCode = value;
    return this;
  }
  
  certificateName(value: string): AdminCreateMerchantCashBoxCommandBuilder {
    this.instance.certificateName = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateMerchantCashBoxCommand {
  id: string;
  name: string;
  qrCode: string;
  certificateName: string;
  merchantBranchId: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateMerchantCashBoxCommandBuilder {
    return new AdminUpdateMerchantCashBoxCommandBuilder();
  }
}

class AdminUpdateMerchantCashBoxCommandBuilder {
  private instance: AdminUpdateMerchantCashBoxCommand;
  
  constructor() {
    this.instance = new AdminUpdateMerchantCashBoxCommand();
  }
  
  id(value: string): AdminUpdateMerchantCashBoxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateMerchantCashBoxCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  qrCode(value: string): AdminUpdateMerchantCashBoxCommandBuilder {
    this.instance.qrCode = value;
    return this;
  }
  
  certificateName(value: string): AdminUpdateMerchantCashBoxCommandBuilder {
    this.instance.certificateName = value;
    return this;
  }
  
  merchantBranchId(value: string): AdminUpdateMerchantCashBoxCommandBuilder {
    this.instance.merchantBranchId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteMerchantCashBoxCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteMerchantCashBoxCommandBuilder {
    return new AdminDeleteMerchantCashBoxCommandBuilder();
  }
}

class AdminDeleteMerchantCashBoxCommandBuilder {
  private instance: AdminDeleteMerchantCashBoxCommand;
  
  constructor() {
    this.instance = new AdminDeleteMerchantCashBoxCommand();
  }
  
  id(value: string): AdminDeleteMerchantCashBoxCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminCreateMerchantBranchCommand {
  id: string;
  merchantId: string;
  name: string;
  
  constructor() {
  }
  
  static build(): AdminCreateMerchantBranchCommandBuilder {
    return new AdminCreateMerchantBranchCommandBuilder();
  }
}

class AdminCreateMerchantBranchCommandBuilder {
  private instance: AdminCreateMerchantBranchCommand;
  
  constructor() {
    this.instance = new AdminCreateMerchantBranchCommand();
  }
  
  id(value: string): AdminCreateMerchantBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  merchantId(value: string): AdminCreateMerchantBranchCommandBuilder {
    this.instance.merchantId = value;
    return this;
  }
  
  name(value: string): AdminCreateMerchantBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetMerchantBranchQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminGetMerchantBranchQueryBuilder {
    return new AdminGetMerchantBranchQueryBuilder();
  }
}

class AdminGetMerchantBranchQueryBuilder {
  private instance: AdminGetMerchantBranchQuery;
  
  constructor() {
    this.instance = new AdminGetMerchantBranchQuery();
  }
  
  id(value: string): AdminGetMerchantBranchQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminGetAllMerchantBranchesQuery {
  page: Page;
  
  constructor() {
  }
  
  static build(): AdminGetAllMerchantBranchesQueryBuilder {
    return new AdminGetAllMerchantBranchesQueryBuilder();
  }
}

class AdminGetAllMerchantBranchesQueryBuilder {
  private instance: AdminGetAllMerchantBranchesQuery;
  
  constructor() {
    this.instance = new AdminGetAllMerchantBranchesQuery();
  }
  
  page(value: Page): AdminGetAllMerchantBranchesQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminUpdateMerchantBranchCommand {
  id: string;
  name: string;
  merchantId: string;
  
  constructor() {
  }
  
  static build(): AdminUpdateMerchantBranchCommandBuilder {
    return new AdminUpdateMerchantBranchCommandBuilder();
  }
}

class AdminUpdateMerchantBranchCommandBuilder {
  private instance: AdminUpdateMerchantBranchCommand;
  
  constructor() {
    this.instance = new AdminUpdateMerchantBranchCommand();
  }
  
  id(value: string): AdminUpdateMerchantBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  name(value: string): AdminUpdateMerchantBranchCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  merchantId(value: string): AdminUpdateMerchantBranchCommandBuilder {
    this.instance.merchantId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AdminDeleteMerchantBranchCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): AdminDeleteMerchantBranchCommandBuilder {
    return new AdminDeleteMerchantBranchCommandBuilder();
  }
}

class AdminDeleteMerchantBranchCommandBuilder {
  private instance: AdminDeleteMerchantBranchCommand;
  
  constructor() {
    this.instance = new AdminDeleteMerchantBranchCommand();
  }
  
  id(value: string): AdminDeleteMerchantBranchCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSignUpCommand {
  regId: string;
  password: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): CustomersSignUpCommandBuilder {
    return new CustomersSignUpCommandBuilder();
  }
}

class CustomersSignUpCommandBuilder {
  private instance: CustomersSignUpCommand;
  
  constructor() {
    this.instance = new CustomersSignUpCommand();
  }
  
  regId(value: string): CustomersSignUpCommandBuilder {
    this.instance.regId = value;
    return this;
  }
  
  password(value: string): CustomersSignUpCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  phone(value: string): CustomersSignUpCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSendActivationCodeCommand {
  regId: string;
  
  constructor() {
  }
  
  static build(): CustomersSendActivationCodeCommandBuilder {
    return new CustomersSendActivationCodeCommandBuilder();
  }
}

class CustomersSendActivationCodeCommandBuilder {
  private instance: CustomersSendActivationCodeCommand;
  
  constructor() {
    this.instance = new CustomersSendActivationCodeCommand();
  }
  
  regId(value: string): CustomersSendActivationCodeCommandBuilder {
    this.instance.regId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersActivateCommand {
  regId: string;
  pincode: string;
  
  constructor() {
  }
  
  static build(): CustomersActivateCommandBuilder {
    return new CustomersActivateCommandBuilder();
  }
}

class CustomersActivateCommandBuilder {
  private instance: CustomersActivateCommand;
  
  constructor() {
    this.instance = new CustomersActivateCommand();
  }
  
  regId(value: string): CustomersActivateCommandBuilder {
    this.instance.regId = value;
    return this;
  }
  
  pincode(value: string): CustomersActivateCommandBuilder {
    this.instance.pincode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersAuthenticateCommand {
  authenticationId: string;
  phone: string;
  password: string;
  
  constructor() {
  }
  
  static build(): CustomersAuthenticateCommandBuilder {
    return new CustomersAuthenticateCommandBuilder();
  }
}

class CustomersAuthenticateCommandBuilder {
  private instance: CustomersAuthenticateCommand;
  
  constructor() {
    this.instance = new CustomersAuthenticateCommand();
  }
  
  authenticationId(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  phone(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  password(value: string): CustomersAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetTokenQuery {
  authenticationId: string;
  
  constructor() {
  }
  
  static build(): CustomersGetTokenQueryBuilder {
    return new CustomersGetTokenQueryBuilder();
  }
}

class CustomersGetTokenQueryBuilder {
  private instance: CustomersGetTokenQuery;
  
  constructor() {
    this.instance = new CustomersGetTokenQuery();
  }
  
  authenticationId(value: string): CustomersGetTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersRefreshCommand {
  authenticationId: string;
  refreshToken: string;
  
  constructor() {
  }
  
  static build(): CustomersRefreshCommandBuilder {
    return new CustomersRefreshCommandBuilder();
  }
}

class CustomersRefreshCommandBuilder {
  private instance: CustomersRefreshCommand;
  
  constructor() {
    this.instance = new CustomersRefreshCommand();
  }
  
  authenticationId(value: string): CustomersRefreshCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  refreshToken(value: string): CustomersRefreshCommandBuilder {
    this.instance.refreshToken = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSignOutCommand {
  
  constructor() {
  }
  
  static build(): CustomersSignOutCommandBuilder {
    return new CustomersSignOutCommandBuilder();
  }
}

class CustomersSignOutCommandBuilder {
  private instance: CustomersSignOutCommand;
  
  constructor() {
    this.instance = new CustomersSignOutCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersForgotPasswordCommand {
  identity: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): CustomersForgotPasswordCommandBuilder {
    return new CustomersForgotPasswordCommandBuilder();
  }
}

class CustomersForgotPasswordCommandBuilder {
  private instance: CustomersForgotPasswordCommand;
  
  constructor() {
    this.instance = new CustomersForgotPasswordCommand();
  }
  
  identity(value: string): CustomersForgotPasswordCommandBuilder {
    this.instance.identity = value;
    return this;
  }
  
  phone(value: string): CustomersForgotPasswordCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersConfirmCodeCommand {
  identity: string;
  code: string;
  
  constructor() {
  }
  
  static build(): CustomersConfirmCodeCommandBuilder {
    return new CustomersConfirmCodeCommandBuilder();
  }
}

class CustomersConfirmCodeCommandBuilder {
  private instance: CustomersConfirmCodeCommand;
  
  constructor() {
    this.instance = new CustomersConfirmCodeCommand();
  }
  
  identity(value: string): CustomersConfirmCodeCommandBuilder {
    this.instance.identity = value;
    return this;
  }
  
  code(value: string): CustomersConfirmCodeCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersResetPasswordCommand {
  identity: string;
  newPass: string;
  
  constructor() {
  }
  
  static build(): CustomersResetPasswordCommandBuilder {
    return new CustomersResetPasswordCommandBuilder();
  }
}

class CustomersResetPasswordCommandBuilder {
  private instance: CustomersResetPasswordCommand;
  
  constructor() {
    this.instance = new CustomersResetPasswordCommand();
  }
  
  identity(value: string): CustomersResetPasswordCommandBuilder {
    this.instance.identity = value;
    return this;
  }
  
  newPass(value: string): CustomersResetPasswordCommandBuilder {
    this.instance.newPass = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetDataCommand {
  key: string;
  value: string;
  
  constructor() {
  }
  
  static build(): CustomersSetDataCommandBuilder {
    return new CustomersSetDataCommandBuilder();
  }
}

class CustomersSetDataCommandBuilder {
  private instance: CustomersSetDataCommand;
  
  constructor() {
    this.instance = new CustomersSetDataCommand();
  }
  
  key(value: string): CustomersSetDataCommandBuilder {
    this.instance.key = value;
    return this;
  }
  
  value(value: string): CustomersSetDataCommandBuilder {
    this.instance.value = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetDataQuery {
  key: string;
  
  constructor() {
  }
  
  static build(): CustomersGetDataQueryBuilder {
    return new CustomersGetDataQueryBuilder();
  }
}

class CustomersGetDataQueryBuilder {
  private instance: CustomersGetDataQuery;
  
  constructor() {
    this.instance = new CustomersGetDataQuery();
  }
  
  key(value: string): CustomersGetDataQueryBuilder {
    this.instance.key = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSendCodeCommand {
  
  constructor() {
  }
  
  static build(): CustomersSendCodeCommandBuilder {
    return new CustomersSendCodeCommandBuilder();
  }
}

class CustomersSendCodeCommandBuilder {
  private instance: CustomersSendCodeCommand;
  
  constructor() {
    this.instance = new CustomersSendCodeCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersConfirmCommand {
  code: string;
  
  constructor() {
  }
  
  static build(): CustomersConfirmCommandBuilder {
    return new CustomersConfirmCommandBuilder();
  }
}

class CustomersConfirmCommandBuilder {
  private instance: CustomersConfirmCommand;
  
  constructor() {
    this.instance = new CustomersConfirmCommand();
  }
  
  code(value: string): CustomersConfirmCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetProfileQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetProfileQueryBuilder {
    return new CustomersGetProfileQueryBuilder();
  }
}

class CustomersGetProfileQueryBuilder {
  private instance: CustomersGetProfileQuery;
  
  constructor() {
    this.instance = new CustomersGetProfileQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSendEmailCodeCommand {
  email: string;
  
  constructor() {
  }
  
  static build(): CustomersSendEmailCodeCommandBuilder {
    return new CustomersSendEmailCodeCommandBuilder();
  }
}

class CustomersSendEmailCodeCommandBuilder {
  private instance: CustomersSendEmailCodeCommand;
  
  constructor() {
    this.instance = new CustomersSendEmailCodeCommand();
  }
  
  email(value: string): CustomersSendEmailCodeCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersChangeEmailCommand {
  code: string;
  
  constructor() {
  }
  
  static build(): CustomersChangeEmailCommandBuilder {
    return new CustomersChangeEmailCommandBuilder();
  }
}

class CustomersChangeEmailCommandBuilder {
  private instance: CustomersChangeEmailCommand;
  
  constructor() {
    this.instance = new CustomersChangeEmailCommand();
  }
  
  code(value: string): CustomersChangeEmailCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSendSmsCodeCommand {
  phone: string;
  
  constructor() {
  }
  
  static build(): CustomersSendSmsCodeCommandBuilder {
    return new CustomersSendSmsCodeCommandBuilder();
  }
}

class CustomersSendSmsCodeCommandBuilder {
  private instance: CustomersSendSmsCodeCommand;
  
  constructor() {
    this.instance = new CustomersSendSmsCodeCommand();
  }
  
  phone(value: string): CustomersSendSmsCodeCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersChangePhoneCommand {
  code: string;
  
  constructor() {
  }
  
  static build(): CustomersChangePhoneCommandBuilder {
    return new CustomersChangePhoneCommandBuilder();
  }
}

class CustomersChangePhoneCommandBuilder {
  private instance: CustomersChangePhoneCommand;
  
  constructor() {
    this.instance = new CustomersChangePhoneCommand();
  }
  
  code(value: string): CustomersChangePhoneCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersChangePasswordCommand {
  currentPass: string;
  newPassword: string;
  
  constructor() {
  }
  
  static build(): CustomersChangePasswordCommandBuilder {
    return new CustomersChangePasswordCommandBuilder();
  }
}

class CustomersChangePasswordCommandBuilder {
  private instance: CustomersChangePasswordCommand;
  
  constructor() {
    this.instance = new CustomersChangePasswordCommand();
  }
  
  currentPass(value: string): CustomersChangePasswordCommandBuilder {
    this.instance.currentPass = value;
    return this;
  }
  
  newPassword(value: string): CustomersChangePasswordCommandBuilder {
    this.instance.newPassword = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetLanguageCommand {
  language: string;
  
  constructor() {
  }
  
  static build(): CustomersSetLanguageCommandBuilder {
    return new CustomersSetLanguageCommandBuilder();
  }
}

class CustomersSetLanguageCommandBuilder {
  private instance: CustomersSetLanguageCommand;
  
  constructor() {
    this.instance = new CustomersSetLanguageCommand();
  }
  
  language(value: string): CustomersSetLanguageCommandBuilder {
    this.instance.language = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetFcmTokenCommand {
  fcmToken: string;
  
  constructor() {
  }
  
  static build(): CustomersSetFcmTokenCommandBuilder {
    return new CustomersSetFcmTokenCommandBuilder();
  }
}

class CustomersSetFcmTokenCommandBuilder {
  private instance: CustomersSetFcmTokenCommand;
  
  constructor() {
    this.instance = new CustomersSetFcmTokenCommand();
  }
  
  fcmToken(value: string): CustomersSetFcmTokenCommandBuilder {
    this.instance.fcmToken = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetNotificationFlagCommand {
  enabled: boolean;
  
  constructor() {
  }
  
  static build(): CustomersSetNotificationFlagCommandBuilder {
    return new CustomersSetNotificationFlagCommandBuilder();
  }
}

class CustomersSetNotificationFlagCommandBuilder {
  private instance: CustomersSetNotificationFlagCommand;
  
  constructor() {
    this.instance = new CustomersSetNotificationFlagCommand();
  }
  
  enabled(value: boolean): CustomersSetNotificationFlagCommandBuilder {
    this.instance.enabled = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetProvidersQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetProvidersQueryBuilder {
    return new CustomersGetProvidersQueryBuilder();
  }
}

class CustomersGetProvidersQueryBuilder {
  private instance: CustomersGetProvidersQuery;
  
  constructor() {
    this.instance = new CustomersGetProvidersQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetProviderLabelsQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetProviderLabelsQueryBuilder {
    return new CustomersGetProviderLabelsQueryBuilder();
  }
}

class CustomersGetProviderLabelsQueryBuilder {
  private instance: CustomersGetProviderLabelsQuery;
  
  constructor() {
    this.instance = new CustomersGetProviderLabelsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetCommissionQuery {
  platform: Platform;
  providerId: string;
  
  constructor() {
  }
  
  static build(): CustomersGetCommissionQueryBuilder {
    return new CustomersGetCommissionQueryBuilder();
  }
}

class CustomersGetCommissionQueryBuilder {
  private instance: CustomersGetCommissionQuery;
  
  constructor() {
    this.instance = new CustomersGetCommissionQuery();
  }
  
  platform(value: Platform): CustomersGetCommissionQueryBuilder {
    this.instance.platform = value;
    return this;
  }
  
  providerId(value: string): CustomersGetCommissionQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersDeleteGroupCommand {
  name: string;
  
  constructor() {
  }
  
  static build(): CustomersDeleteGroupCommandBuilder {
    return new CustomersDeleteGroupCommandBuilder();
  }
}

class CustomersDeleteGroupCommandBuilder {
  private instance: CustomersDeleteGroupCommand;
  
  constructor() {
    this.instance = new CustomersDeleteGroupCommand();
  }
  
  name(value: string): CustomersDeleteGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetGroupCommand {
  name: string;
  agreements: Array<Agreement>;
  
  constructor() {
  }
  
  static build(): CustomersSetGroupCommandBuilder {
    return new CustomersSetGroupCommandBuilder();
  }
}

class CustomersSetGroupCommandBuilder {
  private instance: CustomersSetGroupCommand;
  
  constructor() {
    this.instance = new CustomersSetGroupCommand();
  }
  
  name(value: string): CustomersSetGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  agreements(value: Array<Agreement>): CustomersSetGroupCommandBuilder {
    this.instance.agreements = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersAddCardCommand {
  card: Card;
  
  constructor() {
  }
  
  static build(): CustomersAddCardCommandBuilder {
    return new CustomersAddCardCommandBuilder();
  }
}

class CustomersAddCardCommandBuilder {
  private instance: CustomersAddCardCommand;
  
  constructor() {
    this.instance = new CustomersAddCardCommand();
  }
  
  card(value: Card): CustomersAddCardCommandBuilder {
    this.instance.card = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersRemoveCardCommand {
  cardId: string;
  
  constructor() {
  }
  
  static build(): CustomersRemoveCardCommandBuilder {
    return new CustomersRemoveCardCommandBuilder();
  }
}

class CustomersRemoveCardCommandBuilder {
  private instance: CustomersRemoveCardCommand;
  
  constructor() {
    this.instance = new CustomersRemoveCardCommand();
  }
  
  cardId(value: string): CustomersRemoveCardCommandBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersSetFavoritesCommand {
  type: FavoriteType;
  agreements: Array<Agreement>;
  
  constructor() {
  }
  
  static build(): CustomersSetFavoritesCommandBuilder {
    return new CustomersSetFavoritesCommandBuilder();
  }
}

class CustomersSetFavoritesCommandBuilder {
  private instance: CustomersSetFavoritesCommand;
  
  constructor() {
    this.instance = new CustomersSetFavoritesCommand();
  }
  
  type(value: FavoriteType): CustomersSetFavoritesCommandBuilder {
    this.instance.type = value;
    return this;
  }
  
  agreements(value: Array<Agreement>): CustomersSetFavoritesCommandBuilder {
    this.instance.agreements = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersDebitAmountCommand {
  transactionId: string;
  amount: number;
  source: string;
  sourceType: SourceType;
  externalId: string;
  receiptId: string;
  
  constructor() {
  }
  
  static build(): CustomersDebitAmountCommandBuilder {
    return new CustomersDebitAmountCommandBuilder();
  }
}

class CustomersDebitAmountCommandBuilder {
  private instance: CustomersDebitAmountCommand;
  
  constructor() {
    this.instance = new CustomersDebitAmountCommand();
  }
  
  transactionId(value: string): CustomersDebitAmountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  amount(value: number): CustomersDebitAmountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  source(value: string): CustomersDebitAmountCommandBuilder {
    this.instance.source = value;
    return this;
  }
  
  sourceType(value: SourceType): CustomersDebitAmountCommandBuilder {
    this.instance.sourceType = value;
    return this;
  }
  
  externalId(value: string): CustomersDebitAmountCommandBuilder {
    this.instance.externalId = value;
    return this;
  }
  
  receiptId(value: string): CustomersDebitAmountCommandBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersCreditAmountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  commissionAmount: number;
  source: string;
  sourceType: SourceType;
  receiptId: string;
  
  constructor() {
  }
  
  static build(): CustomersCreditAmountCommandBuilder {
    return new CustomersCreditAmountCommandBuilder();
  }
}

class CustomersCreditAmountCommandBuilder {
  private instance: CustomersCreditAmountCommand;
  
  constructor() {
    this.instance = new CustomersCreditAmountCommand();
  }
  
  transactionId(value: string): CustomersCreditAmountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CustomersCreditAmountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CustomersCreditAmountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  commissionAmount(value: number): CustomersCreditAmountCommandBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  source(value: string): CustomersCreditAmountCommandBuilder {
    this.instance.source = value;
    return this;
  }
  
  sourceType(value: SourceType): CustomersCreditAmountCommandBuilder {
    this.instance.sourceType = value;
    return this;
  }
  
  receiptId(value: string): CustomersCreditAmountCommandBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersChangeNotificationStateCommand {
  messageId: string;
  state: DeliveryState;
  
  constructor() {
  }
  
  static build(): CustomersChangeNotificationStateCommandBuilder {
    return new CustomersChangeNotificationStateCommandBuilder();
  }
}

class CustomersChangeNotificationStateCommandBuilder {
  private instance: CustomersChangeNotificationStateCommand;
  
  constructor() {
    this.instance = new CustomersChangeNotificationStateCommand();
  }
  
  messageId(value: string): CustomersChangeNotificationStateCommandBuilder {
    this.instance.messageId = value;
    return this;
  }
  
  state(value: DeliveryState): CustomersChangeNotificationStateCommandBuilder {
    this.instance.state = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersDeleteFCMTokenCommand {
  
  constructor() {
  }
  
  static build(): CustomersDeleteFCMTokenCommandBuilder {
    return new CustomersDeleteFCMTokenCommandBuilder();
  }
}

class CustomersDeleteFCMTokenCommandBuilder {
  private instance: CustomersDeleteFCMTokenCommand;
  
  constructor() {
    this.instance = new CustomersDeleteFCMTokenCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersDeleteNotificationCommand {
  notificationId: string;
  
  constructor() {
  }
  
  static build(): CustomersDeleteNotificationCommandBuilder {
    return new CustomersDeleteNotificationCommandBuilder();
  }
}

class CustomersDeleteNotificationCommandBuilder {
  private instance: CustomersDeleteNotificationCommand;
  
  constructor() {
    this.instance = new CustomersDeleteNotificationCommand();
  }
  
  notificationId(value: string): CustomersDeleteNotificationCommandBuilder {
    this.instance.notificationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersFindCustomerByEmailQuery {
  email: string;
  
  constructor() {
  }
  
  static build(): CustomersFindCustomerByEmailQueryBuilder {
    return new CustomersFindCustomerByEmailQueryBuilder();
  }
}

class CustomersFindCustomerByEmailQueryBuilder {
  private instance: CustomersFindCustomerByEmailQuery;
  
  constructor() {
    this.instance = new CustomersFindCustomerByEmailQuery();
  }
  
  email(value: string): CustomersFindCustomerByEmailQueryBuilder {
    this.instance.email = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetAllNotificationsQuery {
  page: Page;
  
  constructor() {
  }
  
  static build(): CustomersGetAllNotificationsQueryBuilder {
    return new CustomersGetAllNotificationsQueryBuilder();
  }
}

class CustomersGetAllNotificationsQueryBuilder {
  private instance: CustomersGetAllNotificationsQuery;
  
  constructor() {
    this.instance = new CustomersGetAllNotificationsQuery();
  }
  
  page(value: Page): CustomersGetAllNotificationsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetTransactionsQuery {
  receiptId: string;
  providerId: string;
  type: Array<UserTransactionType>;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  source: string;
  sourceType: SourceType;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): CustomersGetTransactionsQueryBuilder {
    return new CustomersGetTransactionsQueryBuilder();
  }
}

class CustomersGetTransactionsQueryBuilder {
  private instance: CustomersGetTransactionsQuery;
  
  constructor() {
    this.instance = new CustomersGetTransactionsQuery();
  }
  
  receiptId(value: string): CustomersGetTransactionsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  providerId(value: string): CustomersGetTransactionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  type(value: Array<UserTransactionType>): CustomersGetTransactionsQueryBuilder {
    this.instance.type = value;
    return this;
  }
  
  dateFrom(value: Date): CustomersGetTransactionsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): CustomersGetTransactionsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): CustomersGetTransactionsQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): CustomersGetTransactionsQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  source(value: string): CustomersGetTransactionsQueryBuilder {
    this.instance.source = value;
    return this;
  }
  
  sourceType(value: SourceType): CustomersGetTransactionsQueryBuilder {
    this.instance.sourceType = value;
    return this;
  }
  
  page(value: Page): CustomersGetTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): CustomersGetTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetFavoritesQuery {
  type: FavoriteType;
  
  constructor() {
  }
  
  static build(): CustomersGetFavoritesQueryBuilder {
    return new CustomersGetFavoritesQueryBuilder();
  }
}

class CustomersGetFavoritesQueryBuilder {
  private instance: CustomersGetFavoritesQuery;
  
  constructor() {
    this.instance = new CustomersGetFavoritesQuery();
  }
  
  type(value: FavoriteType): CustomersGetFavoritesQueryBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetCardsQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetCardsQueryBuilder {
    return new CustomersGetCardsQueryBuilder();
  }
}

class CustomersGetCardsQueryBuilder {
  private instance: CustomersGetCardsQuery;
  
  constructor() {
    this.instance = new CustomersGetCardsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersFindGroupQuery {
  name: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): CustomersFindGroupQueryBuilder {
    return new CustomersFindGroupQueryBuilder();
  }
}

class CustomersFindGroupQueryBuilder {
  private instance: CustomersFindGroupQuery;
  
  constructor() {
    this.instance = new CustomersFindGroupQuery();
  }
  
  name(value: string): CustomersFindGroupQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  page(value: Page): CustomersFindGroupQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): CustomersFindGroupQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetBalanceQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetBalanceQueryBuilder {
    return new CustomersGetBalanceQueryBuilder();
  }
}

class CustomersGetBalanceQueryBuilder {
  private instance: CustomersGetBalanceQuery;
  
  constructor() {
    this.instance = new CustomersGetBalanceQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetBranchesQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetBranchesQueryBuilder {
    return new CustomersGetBranchesQueryBuilder();
  }
}

class CustomersGetBranchesQueryBuilder {
  private instance: CustomersGetBranchesQuery;
  
  constructor() {
    this.instance = new CustomersGetBranchesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetBalanceLimitQuery {
  
  constructor() {
  }
  
  static build(): CustomersGetBalanceLimitQueryBuilder {
    return new CustomersGetBalanceLimitQueryBuilder();
  }
}

class CustomersGetBalanceLimitQueryBuilder {
  private instance: CustomersGetBalanceLimitQuery;
  
  constructor() {
    this.instance = new CustomersGetBalanceLimitQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetMerchantTransactionQuery {
  merchantCashboxId: string;
  
  constructor() {
  }
  
  static build(): CustomersGetMerchantTransactionQueryBuilder {
    return new CustomersGetMerchantTransactionQueryBuilder();
  }
}

class CustomersGetMerchantTransactionQueryBuilder {
  private instance: CustomersGetMerchantTransactionQuery;
  
  constructor() {
    this.instance = new CustomersGetMerchantTransactionQuery();
  }
  
  merchantCashboxId(value: string): CustomersGetMerchantTransactionQueryBuilder {
    this.instance.merchantCashboxId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersRetryHangingTransactionsCommand {
  
  constructor() {
  }
  
  static build(): CustomersRetryHangingTransactionsCommandBuilder {
    return new CustomersRetryHangingTransactionsCommandBuilder();
  }
}

class CustomersRetryHangingTransactionsCommandBuilder {
  private instance: CustomersRetryHangingTransactionsCommand;
  
  constructor() {
    this.instance = new CustomersRetryHangingTransactionsCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersContactUsCommand {
  from: string;
  phone: string;
  name: string;
  body: string;
  
  constructor() {
  }
  
  static build(): CustomersContactUsCommandBuilder {
    return new CustomersContactUsCommandBuilder();
  }
}

class CustomersContactUsCommandBuilder {
  private instance: CustomersContactUsCommand;
  
  constructor() {
    this.instance = new CustomersContactUsCommand();
  }
  
  from(value: string): CustomersContactUsCommandBuilder {
    this.instance.from = value;
    return this;
  }
  
  phone(value: string): CustomersContactUsCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  name(value: string): CustomersContactUsCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  body(value: string): CustomersContactUsCommandBuilder {
    this.instance.body = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomersGetByNumberQuery {
  number: string;
  
  constructor() {
  }
  
  static build(): CustomersGetByNumberQueryBuilder {
    return new CustomersGetByNumberQueryBuilder();
  }
}

class CustomersGetByNumberQueryBuilder {
  private instance: CustomersGetByNumberQuery;
  
  constructor() {
    this.instance = new CustomersGetByNumberQuery();
  }
  
  number(value: string): CustomersGetByNumberQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorAuthenticateCommand {
  authenticationId: string;
  cashboxId: string;
  userName: string;
  password: string;
  
  constructor() {
  }
  
  static build(): OperatorAuthenticateCommandBuilder {
    return new OperatorAuthenticateCommandBuilder();
  }
}

class OperatorAuthenticateCommandBuilder {
  private instance: OperatorAuthenticateCommand;
  
  constructor() {
    this.instance = new OperatorAuthenticateCommand();
  }
  
  authenticationId(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  cashboxId(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  userName(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.userName = value;
    return this;
  }
  
  password(value: string): OperatorAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorUpdateTokenAccessedCommand {
  tokenId: string;
  
  constructor() {
  }
  
  static build(): OperatorUpdateTokenAccessedCommandBuilder {
    return new OperatorUpdateTokenAccessedCommandBuilder();
  }
}

class OperatorUpdateTokenAccessedCommandBuilder {
  private instance: OperatorUpdateTokenAccessedCommand;
  
  constructor() {
    this.instance = new OperatorUpdateTokenAccessedCommand();
  }
  
  tokenId(value: string): OperatorUpdateTokenAccessedCommandBuilder {
    this.instance.tokenId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCreateGroupCommand {
  name: string;
  
  constructor() {
  }
  
  static build(): OperatorCreateGroupCommandBuilder {
    return new OperatorCreateGroupCommandBuilder();
  }
}

class OperatorCreateGroupCommandBuilder {
  private instance: OperatorCreateGroupCommand;
  
  constructor() {
    this.instance = new OperatorCreateGroupCommand();
  }
  
  name(value: string): OperatorCreateGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorUpdateGroupCommand {
  name: string;
  groupId: string;
  
  constructor() {
  }
  
  static build(): OperatorUpdateGroupCommandBuilder {
    return new OperatorUpdateGroupCommandBuilder();
  }
}

class OperatorUpdateGroupCommandBuilder {
  private instance: OperatorUpdateGroupCommand;
  
  constructor() {
    this.instance = new OperatorUpdateGroupCommand();
  }
  
  name(value: string): OperatorUpdateGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  groupId(value: string): OperatorUpdateGroupCommandBuilder {
    this.instance.groupId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorDeleteGroupCommand {
  name: string;
  
  constructor() {
  }
  
  static build(): OperatorDeleteGroupCommandBuilder {
    return new OperatorDeleteGroupCommandBuilder();
  }
}

class OperatorDeleteGroupCommandBuilder {
  private instance: OperatorDeleteGroupCommand;
  
  constructor() {
    this.instance = new OperatorDeleteGroupCommand();
  }
  
  name(value: string): OperatorDeleteGroupCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCreateTokenCommand {
  cashbox: string;
  email: string;
  password: string;
  
  constructor() {
  }
  
  static build(): OperatorCreateTokenCommandBuilder {
    return new OperatorCreateTokenCommandBuilder();
  }
}

class OperatorCreateTokenCommandBuilder {
  private instance: OperatorCreateTokenCommand;
  
  constructor() {
    this.instance = new OperatorCreateTokenCommand();
  }
  
  cashbox(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.cashbox = value;
    return this;
  }
  
  email(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  password(value: string): OperatorCreateTokenCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorChangePasswordCommand {
  email: string;
  oldPass: string;
  newPass: string;
  
  constructor() {
  }
  
  static build(): OperatorChangePasswordCommandBuilder {
    return new OperatorChangePasswordCommandBuilder();
  }
}

class OperatorChangePasswordCommandBuilder {
  private instance: OperatorChangePasswordCommand;
  
  constructor() {
    this.instance = new OperatorChangePasswordCommand();
  }
  
  email(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  oldPass(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.oldPass = value;
    return this;
  }
  
  newPass(value: string): OperatorChangePasswordCommandBuilder {
    this.instance.newPass = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorAddContractCommand {
  groupName: string;
  contract: Contract;
  
  constructor() {
  }
  
  static build(): OperatorAddContractCommandBuilder {
    return new OperatorAddContractCommandBuilder();
  }
}

class OperatorAddContractCommandBuilder {
  private instance: OperatorAddContractCommand;
  
  constructor() {
    this.instance = new OperatorAddContractCommand();
  }
  
  groupName(value: string): OperatorAddContractCommandBuilder {
    this.instance.groupName = value;
    return this;
  }
  
  contract(value: Contract): OperatorAddContractCommandBuilder {
    this.instance.contract = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorRemoveContractCommand {
  groupName: string;
  subscriber: string;
  branchId: string;
  providerId: string;
  
  constructor() {
  }
  
  static build(): OperatorRemoveContractCommandBuilder {
    return new OperatorRemoveContractCommandBuilder();
  }
}

class OperatorRemoveContractCommandBuilder {
  private instance: OperatorRemoveContractCommand;
  
  constructor() {
    this.instance = new OperatorRemoveContractCommand();
  }
  
  groupName(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.groupName = value;
    return this;
  }
  
  subscriber(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.subscriber = value;
    return this;
  }
  
  branchId(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerId(value: string): OperatorRemoveContractCommandBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashInCommand {
  customer: string;
  amount: number;
  
  constructor() {
  }
  
  static build(): OperatorCashInCommandBuilder {
    return new OperatorCashInCommandBuilder();
  }
}

class OperatorCashInCommandBuilder {
  private instance: OperatorCashInCommand;
  
  constructor() {
    this.instance = new OperatorCashInCommand();
  }
  
  customer(value: string): OperatorCashInCommandBuilder {
    this.instance.customer = value;
    return this;
  }
  
  amount(value: number): OperatorCashInCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutCommand {
  customer: string;
  amount: number;
  
  constructor() {
  }
  
  static build(): OperatorCashOutCommandBuilder {
    return new OperatorCashOutCommandBuilder();
  }
}

class OperatorCashOutCommandBuilder {
  private instance: OperatorCashOutCommand;
  
  constructor() {
    this.instance = new OperatorCashOutCommand();
  }
  
  customer(value: string): OperatorCashOutCommandBuilder {
    this.instance.customer = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCloseCashboxCommand {
  cashboxId: string;
  cashIn: number;
  cashOut: number;
  incassation: number;
  finalBalance: number;
  
  constructor() {
  }
  
  static build(): OperatorCloseCashboxCommandBuilder {
    return new OperatorCloseCashboxCommandBuilder();
  }
}

class OperatorCloseCashboxCommandBuilder {
  private instance: OperatorCloseCashboxCommand;
  
  constructor() {
    this.instance = new OperatorCloseCashboxCommand();
  }
  
  cashboxId(value: string): OperatorCloseCashboxCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  cashIn(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.cashIn = value;
    return this;
  }
  
  cashOut(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.cashOut = value;
    return this;
  }
  
  incassation(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.incassation = value;
    return this;
  }
  
  finalBalance(value: number): OperatorCloseCashboxCommandBuilder {
    this.instance.finalBalance = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorAddToBasketCommand {
  providerId: string;
  contractIdentity: string;
  
  constructor() {
  }
  
  static build(): OperatorAddToBasketCommandBuilder {
    return new OperatorAddToBasketCommandBuilder();
  }
}

class OperatorAddToBasketCommandBuilder {
  private instance: OperatorAddToBasketCommand;
  
  constructor() {
    this.instance = new OperatorAddToBasketCommand();
  }
  
  providerId(value: string): OperatorAddToBasketCommandBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  contractIdentity(value: string): OperatorAddToBasketCommandBuilder {
    this.instance.contractIdentity = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorDeleteFromBasketCommand {
  basketId: string;
  
  constructor() {
  }
  
  static build(): OperatorDeleteFromBasketCommandBuilder {
    return new OperatorDeleteFromBasketCommandBuilder();
  }
}

class OperatorDeleteFromBasketCommandBuilder {
  private instance: OperatorDeleteFromBasketCommand;
  
  constructor() {
    this.instance = new OperatorDeleteFromBasketCommand();
  }
  
  basketId(value: string): OperatorDeleteFromBasketCommandBuilder {
    this.instance.basketId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashboxPayCommand {
  cashboxPaymentId: string;
  totalAmount: number;
  serviceQuantity: number;
  inAmount: number;
  cash: boolean;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashboxPayCommandBuilder {
    return new OperatorCashboxPayCommandBuilder();
  }
}

class OperatorCashboxPayCommandBuilder {
  private instance: OperatorCashboxPayCommand;
  
  constructor() {
    this.instance = new OperatorCashboxPayCommand();
  }
  
  cashboxPaymentId(value: string): OperatorCashboxPayCommandBuilder {
    this.instance.cashboxPaymentId = value;
    return this;
  }
  
  totalAmount(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.totalAmount = value;
    return this;
  }
  
  serviceQuantity(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.serviceQuantity = value;
    return this;
  }
  
  inAmount(value: number): OperatorCashboxPayCommandBuilder {
    this.instance.inAmount = value;
    return this;
  }
  
  cash(value: boolean): OperatorCashboxPayCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashboxPayCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashboxInCommand {
  orderId: string;
  amount: number;
  type: EncashmentType;
  notes: string;
  username: string;
  contract: string;
  details: string;
  
  constructor() {
  }
  
  static build(): OperatorCashboxInCommandBuilder {
    return new OperatorCashboxInCommandBuilder();
  }
}

class OperatorCashboxInCommandBuilder {
  private instance: OperatorCashboxInCommand;
  
  constructor() {
    this.instance = new OperatorCashboxInCommand();
  }
  
  orderId(value: string): OperatorCashboxInCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashboxInCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  type(value: EncashmentType): OperatorCashboxInCommandBuilder {
    this.instance.type = value;
    return this;
  }
  
  notes(value: string): OperatorCashboxInCommandBuilder {
    this.instance.notes = value;
    return this;
  }
  
  username(value: string): OperatorCashboxInCommandBuilder {
    this.instance.username = value;
    return this;
  }
  
  contract(value: string): OperatorCashboxInCommandBuilder {
    this.instance.contract = value;
    return this;
  }
  
  details(value: string): OperatorCashboxInCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashboxOutCommand {
  orderId: string;
  amount: number;
  type: EncashmentType;
  notes: string;
  username: string;
  contract: string;
  details: string;
  
  constructor() {
  }
  
  static build(): OperatorCashboxOutCommandBuilder {
    return new OperatorCashboxOutCommandBuilder();
  }
}

class OperatorCashboxOutCommandBuilder {
  private instance: OperatorCashboxOutCommand;
  
  constructor() {
    this.instance = new OperatorCashboxOutCommand();
  }
  
  orderId(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashboxOutCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  type(value: EncashmentType): OperatorCashboxOutCommandBuilder {
    this.instance.type = value;
    return this;
  }
  
  notes(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.notes = value;
    return this;
  }
  
  username(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.username = value;
    return this;
  }
  
  contract(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.contract = value;
    return this;
  }
  
  details(value: string): OperatorCashboxOutCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutIdramCommand {
  orderId: string;
  amount: number;
  document: CashoutDocument;
  idramWallet: string;
  details: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashOutIdramCommandBuilder {
    return new OperatorCashOutIdramCommandBuilder();
  }
}

class OperatorCashOutIdramCommandBuilder {
  private instance: OperatorCashOutIdramCommand;
  
  constructor() {
    this.instance = new OperatorCashOutIdramCommand();
  }
  
  orderId(value: string): OperatorCashOutIdramCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutIdramCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  document(value: CashoutDocument): OperatorCashOutIdramCommandBuilder {
    this.instance.document = value;
    return this;
  }
  
  idramWallet(value: string): OperatorCashOutIdramCommandBuilder {
    this.instance.idramWallet = value;
    return this;
  }
  
  details(value: string): OperatorCashOutIdramCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashOutIdramCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutLoanGoodCreditCommand {
  orderId: string;
  amount: number;
  document: CashoutDocument;
  aggreementNumber: string;
  details: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashOutLoanGoodCreditCommandBuilder {
    return new OperatorCashOutLoanGoodCreditCommandBuilder();
  }
}

class OperatorCashOutLoanGoodCreditCommandBuilder {
  private instance: OperatorCashOutLoanGoodCreditCommand;
  
  constructor() {
    this.instance = new OperatorCashOutLoanGoodCreditCommand();
  }
  
  orderId(value: string): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  document(value: CashoutDocument): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.document = value;
    return this;
  }
  
  aggreementNumber(value: string): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.aggreementNumber = value;
    return this;
  }
  
  details(value: string): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashOutLoanGoodCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutLoanVarksAmCommand {
  orderId: string;
  amount: number;
  document: CashoutDocument;
  aggreementNumber: string;
  details: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashOutLoanVarksAmCommandBuilder {
    return new OperatorCashOutLoanVarksAmCommandBuilder();
  }
}

class OperatorCashOutLoanVarksAmCommandBuilder {
  private instance: OperatorCashOutLoanVarksAmCommand;
  
  constructor() {
    this.instance = new OperatorCashOutLoanVarksAmCommand();
  }
  
  orderId(value: string): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  document(value: CashoutDocument): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.document = value;
    return this;
  }
  
  aggreementNumber(value: string): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.aggreementNumber = value;
    return this;
  }
  
  details(value: string): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashOutLoanVarksAmCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutLoanGlobalCreditCommand {
  orderId: string;
  amount: number;
  document: CashoutDocument;
  aggreementNumber: string;
  details: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashOutLoanGlobalCreditCommandBuilder {
    return new OperatorCashOutLoanGlobalCreditCommandBuilder();
  }
}

class OperatorCashOutLoanGlobalCreditCommandBuilder {
  private instance: OperatorCashOutLoanGlobalCreditCommand;
  
  constructor() {
    this.instance = new OperatorCashOutLoanGlobalCreditCommand();
  }
  
  orderId(value: string): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  document(value: CashoutDocument): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.document = value;
    return this;
  }
  
  aggreementNumber(value: string): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.aggreementNumber = value;
    return this;
  }
  
  details(value: string): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashOutLoanGlobalCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCashOutUcomPhoneByBackServiceCommand {
  orderId: string;
  amount: number;
  totalPrice: number;
  name: string;
  model: string;
  imei: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    return new OperatorCashOutUcomPhoneByBackServiceCommandBuilder();
  }
}

class OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
  private instance: OperatorCashOutUcomPhoneByBackServiceCommand;
  
  constructor() {
    this.instance = new OperatorCashOutUcomPhoneByBackServiceCommand();
  }
  
  orderId(value: string): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  totalPrice(value: number): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.totalPrice = value;
    return this;
  }
  
  name(value: string): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.name = value;
    return this;
  }
  
  model(value: string): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.model = value;
    return this;
  }
  
  imei(value: string): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.imei = value;
    return this;
  }
  
  contactNum(value: string): OperatorCashOutUcomPhoneByBackServiceCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorArmsoftImportCommand {
  
  constructor() {
  }
  
  static build(): OperatorArmsoftImportCommandBuilder {
    return new OperatorArmsoftImportCommandBuilder();
  }
}

class OperatorArmsoftImportCommandBuilder {
  private instance: OperatorArmsoftImportCommand;
  
  constructor() {
    this.instance = new OperatorArmsoftImportCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGenerateCashboxReportCommand {
  
  constructor() {
  }
  
  static build(): OperatorGenerateCashboxReportCommandBuilder {
    return new OperatorGenerateCashboxReportCommandBuilder();
  }
}

class OperatorGenerateCashboxReportCommandBuilder {
  private instance: OperatorGenerateCashboxReportCommand;
  
  constructor() {
    this.instance = new OperatorGenerateCashboxReportCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomMobileBranchDailyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomMobileBranchDailyCommandBuilder {
    return new OperatorReportUcomMobileBranchDailyCommandBuilder();
  }
}

class OperatorReportUcomMobileBranchDailyCommandBuilder {
  private instance: OperatorReportUcomMobileBranchDailyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomMobileBranchDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomMobileWalletDailyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomMobileWalletDailyCommandBuilder {
    return new OperatorReportUcomMobileWalletDailyCommandBuilder();
  }
}

class OperatorReportUcomMobileWalletDailyCommandBuilder {
  private instance: OperatorReportUcomMobileWalletDailyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomMobileWalletDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomMobileMonthlyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomMobileMonthlyCommandBuilder {
    return new OperatorReportUcomMobileMonthlyCommandBuilder();
  }
}

class OperatorReportUcomMobileMonthlyCommandBuilder {
  private instance: OperatorReportUcomMobileMonthlyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomMobileMonthlyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomFixedBranchDailyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomFixedBranchDailyCommandBuilder {
    return new OperatorReportUcomFixedBranchDailyCommandBuilder();
  }
}

class OperatorReportUcomFixedBranchDailyCommandBuilder {
  private instance: OperatorReportUcomFixedBranchDailyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomFixedBranchDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomFixedWalletDailyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomFixedWalletDailyCommandBuilder {
    return new OperatorReportUcomFixedWalletDailyCommandBuilder();
  }
}

class OperatorReportUcomFixedWalletDailyCommandBuilder {
  private instance: OperatorReportUcomFixedWalletDailyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomFixedWalletDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorReportUcomFixedMonthlyCommand {
  
  constructor() {
  }
  
  static build(): OperatorReportUcomFixedMonthlyCommandBuilder {
    return new OperatorReportUcomFixedMonthlyCommandBuilder();
  }
}

class OperatorReportUcomFixedMonthlyCommandBuilder {
  private instance: OperatorReportUcomFixedMonthlyCommand;
  
  constructor() {
    this.instance = new OperatorReportUcomFixedMonthlyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetAccessTokenQuery {
  authenticationId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetAccessTokenQueryBuilder {
    return new OperatorGetAccessTokenQueryBuilder();
  }
}

class OperatorGetAccessTokenQueryBuilder {
  private instance: OperatorGetAccessTokenQuery;
  
  constructor() {
    this.instance = new OperatorGetAccessTokenQuery();
  }
  
  authenticationId(value: string): OperatorGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorVerifyTokenQuery {
  token: string;
  
  constructor() {
  }
  
  static build(): OperatorVerifyTokenQueryBuilder {
    return new OperatorVerifyTokenQueryBuilder();
  }
}

class OperatorVerifyTokenQueryBuilder {
  private instance: OperatorVerifyTokenQuery;
  
  constructor() {
    this.instance = new OperatorVerifyTokenQuery();
  }
  
  token(value: string): OperatorVerifyTokenQueryBuilder {
    this.instance.token = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorFindGroupQuery {
  name: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorFindGroupQueryBuilder {
    return new OperatorFindGroupQueryBuilder();
  }
}

class OperatorFindGroupQueryBuilder {
  private instance: OperatorFindGroupQuery;
  
  constructor() {
    this.instance = new OperatorFindGroupQuery();
  }
  
  name(value: string): OperatorFindGroupQueryBuilder {
    this.instance.name = value;
    return this;
  }
  
  page(value: Page): OperatorFindGroupQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorFindGroupQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCashboxBalanceQuery {
  cashboxId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCashboxBalanceQueryBuilder {
    return new OperatorGetCashboxBalanceQueryBuilder();
  }
}

class OperatorGetCashboxBalanceQueryBuilder {
  private instance: OperatorGetCashboxBalanceQuery;
  
  constructor() {
    this.instance = new OperatorGetCashboxBalanceQuery();
  }
  
  cashboxId(value: string): OperatorGetCashboxBalanceQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCurrentBalanceQuery {
  cashboxId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCurrentBalanceQueryBuilder {
    return new OperatorGetCurrentBalanceQueryBuilder();
  }
}

class OperatorGetCurrentBalanceQueryBuilder {
  private instance: OperatorGetCurrentBalanceQuery;
  
  constructor() {
    this.instance = new OperatorGetCurrentBalanceQuery();
  }
  
  cashboxId(value: string): OperatorGetCurrentBalanceQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCashboxTransactionsQuery {
  cashboxId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetCashboxTransactionsQueryBuilder {
    return new OperatorGetCashboxTransactionsQueryBuilder();
  }
}

class OperatorGetCashboxTransactionsQueryBuilder {
  private instance: OperatorGetCashboxTransactionsQuery;
  
  constructor() {
    this.instance = new OperatorGetCashboxTransactionsQuery();
  }
  
  cashboxId(value: string): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  page(value: Page): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetCashboxTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetTransactionsByPaymentQuery {
  paymentId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetTransactionsByPaymentQueryBuilder {
    return new OperatorGetTransactionsByPaymentQueryBuilder();
  }
}

class OperatorGetTransactionsByPaymentQueryBuilder {
  private instance: OperatorGetTransactionsByPaymentQuery;
  
  constructor() {
    this.instance = new OperatorGetTransactionsByPaymentQuery();
  }
  
  paymentId(value: string): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  page(value: Page): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetTransactionsByPaymentQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetTransactionQuery {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetTransactionQueryBuilder {
    return new OperatorGetTransactionQueryBuilder();
  }
}

class OperatorGetTransactionQueryBuilder {
  private instance: OperatorGetTransactionQuery;
  
  constructor() {
    this.instance = new OperatorGetTransactionQuery();
  }
  
  transactionId(value: string): OperatorGetTransactionQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCashboxLogsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetCashboxLogsQueryBuilder {
    return new OperatorGetCashboxLogsQueryBuilder();
  }
}

class OperatorGetCashboxLogsQueryBuilder {
  private instance: OperatorGetCashboxLogsQuery;
  
  constructor() {
    this.instance = new OperatorGetCashboxLogsQuery();
  }
  
  page(value: Page): OperatorGetCashboxLogsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetCashboxLogsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCurrentBranchQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetCurrentBranchQueryBuilder {
    return new OperatorGetCurrentBranchQueryBuilder();
  }
}

class OperatorGetCurrentBranchQueryBuilder {
  private instance: OperatorGetCurrentBranchQuery;
  
  constructor() {
    this.instance = new OperatorGetCurrentBranchQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetProviderDescriptorsQuery {
  providerId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetProviderDescriptorsQueryBuilder {
    return new OperatorGetProviderDescriptorsQueryBuilder();
  }
}

class OperatorGetProviderDescriptorsQueryBuilder {
  private instance: OperatorGetProviderDescriptorsQuery;
  
  constructor() {
    this.instance = new OperatorGetProviderDescriptorsQuery();
  }
  
  providerId(value: string): OperatorGetProviderDescriptorsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetProfileQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetProfileQueryBuilder {
    return new OperatorGetProfileQueryBuilder();
  }
}

class OperatorGetProfileQueryBuilder {
  private instance: OperatorGetProfileQuery;
  
  constructor() {
    this.instance = new OperatorGetProfileQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetOperatorByIdQuery {
  operatorId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetOperatorByIdQueryBuilder {
    return new OperatorGetOperatorByIdQueryBuilder();
  }
}

class OperatorGetOperatorByIdQueryBuilder {
  private instance: OperatorGetOperatorByIdQuery;
  
  constructor() {
    this.instance = new OperatorGetOperatorByIdQuery();
  }
  
  operatorId(value: string): OperatorGetOperatorByIdQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetBasketItemsCountQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetBasketItemsCountQueryBuilder {
    return new OperatorGetBasketItemsCountQueryBuilder();
  }
}

class OperatorGetBasketItemsCountQueryBuilder {
  private instance: OperatorGetBasketItemsCountQuery;
  
  constructor() {
    this.instance = new OperatorGetBasketItemsCountQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetBasketItemsQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetBasketItemsQueryBuilder {
    return new OperatorGetBasketItemsQueryBuilder();
  }
}

class OperatorGetBasketItemsQueryBuilder {
  private instance: OperatorGetBasketItemsQuery;
  
  constructor() {
    this.instance = new OperatorGetBasketItemsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetDebtQuery {
  providerId: string;
  contract: Contract;
  
  constructor() {
  }
  
  static build(): OperatorGetDebtQueryBuilder {
    return new OperatorGetDebtQueryBuilder();
  }
}

class OperatorGetDebtQueryBuilder {
  private instance: OperatorGetDebtQuery;
  
  constructor() {
    this.instance = new OperatorGetDebtQuery();
  }
  
  providerId(value: string): OperatorGetDebtQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  contract(value: Contract): OperatorGetDebtQueryBuilder {
    this.instance.contract = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorNum2TextQuery {
  number: number;
  
  constructor() {
  }
  
  static build(): OperatorNum2TextQueryBuilder {
    return new OperatorNum2TextQueryBuilder();
  }
}

class OperatorNum2TextQueryBuilder {
  private instance: OperatorNum2TextQuery;
  
  constructor() {
    this.instance = new OperatorNum2TextQuery();
  }
  
  number(value: number): OperatorNum2TextQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetProviderCommissionsQuery {
  branchId: string;
  providerId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetProviderCommissionsQueryBuilder {
    return new OperatorGetProviderCommissionsQueryBuilder();
  }
}

class OperatorGetProviderCommissionsQueryBuilder {
  private instance: OperatorGetProviderCommissionsQuery;
  
  constructor() {
    this.instance = new OperatorGetProviderCommissionsQuery();
  }
  
  branchId(value: string): OperatorGetProviderCommissionsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerId(value: string): OperatorGetProviderCommissionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCommissionsForBranchQuery {
  branchId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCommissionsForBranchQueryBuilder {
    return new OperatorGetCommissionsForBranchQueryBuilder();
  }
}

class OperatorGetCommissionsForBranchQueryBuilder {
  private instance: OperatorGetCommissionsForBranchQuery;
  
  constructor() {
    this.instance = new OperatorGetCommissionsForBranchQuery();
  }
  
  branchId(value: string): OperatorGetCommissionsForBranchQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetOrderNumberQuery {
  branchId: string;
  in: boolean;
  
  constructor() {
  }
  
  static build(): OperatorGetOrderNumberQueryBuilder {
    return new OperatorGetOrderNumberQueryBuilder();
  }
}

class OperatorGetOrderNumberQueryBuilder {
  private instance: OperatorGetOrderNumberQuery;
  
  constructor() {
    this.instance = new OperatorGetOrderNumberQuery();
  }
  
  branchId(value: string): OperatorGetOrderNumberQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  in(value: boolean): OperatorGetOrderNumberQueryBuilder {
    this.instance.in = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetOperatorLogQuery {
  userId: string;
  providerName: string;
  paymentId: string;
  userNum: string;
  receiptId: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  cash: boolean;
  posAuthcode: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetOperatorLogQueryBuilder {
    return new OperatorGetOperatorLogQueryBuilder();
  }
}

class OperatorGetOperatorLogQueryBuilder {
  private instance: OperatorGetOperatorLogQuery;
  
  constructor() {
    this.instance = new OperatorGetOperatorLogQuery();
  }
  
  userId(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.userId = value;
    return this;
  }
  
  providerName(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  paymentId(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  userNum(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  receiptId(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  dateFrom(value: Date): OperatorGetOperatorLogQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): OperatorGetOperatorLogQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): OperatorGetOperatorLogQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): OperatorGetOperatorLogQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  cash(value: boolean): OperatorGetOperatorLogQueryBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): OperatorGetOperatorLogQueryBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  page(value: Page): OperatorGetOperatorLogQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetOperatorLogQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCustomerTransactionByReceiptIdQuery {
  receiptId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCustomerTransactionByReceiptIdQueryBuilder {
    return new OperatorGetCustomerTransactionByReceiptIdQueryBuilder();
  }
}

class OperatorGetCustomerTransactionByReceiptIdQueryBuilder {
  private instance: OperatorGetCustomerTransactionByReceiptIdQuery;
  
  constructor() {
    this.instance = new OperatorGetCustomerTransactionByReceiptIdQuery();
  }
  
  receiptId(value: string): OperatorGetCustomerTransactionByReceiptIdQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorIsCancellationClaimAcceptableQuery {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): OperatorIsCancellationClaimAcceptableQueryBuilder {
    return new OperatorIsCancellationClaimAcceptableQueryBuilder();
  }
}

class OperatorIsCancellationClaimAcceptableQueryBuilder {
  private instance: OperatorIsCancellationClaimAcceptableQuery;
  
  constructor() {
    this.instance = new OperatorIsCancellationClaimAcceptableQuery();
  }
  
  transactionId(value: string): OperatorIsCancellationClaimAcceptableQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorOpenCustomerCancellationClaimCommand {
  id: string;
  transactionId: string;
  notes: string;
  
  constructor() {
  }
  
  static build(): OperatorOpenCustomerCancellationClaimCommandBuilder {
    return new OperatorOpenCustomerCancellationClaimCommandBuilder();
  }
}

class OperatorOpenCustomerCancellationClaimCommandBuilder {
  private instance: OperatorOpenCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new OperatorOpenCustomerCancellationClaimCommand();
  }
  
  id(value: string): OperatorOpenCustomerCancellationClaimCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): OperatorOpenCustomerCancellationClaimCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  notes(value: string): OperatorOpenCustomerCancellationClaimCommandBuilder {
    this.instance.notes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorOpenCancellationClaimCommand {
  id: string;
  transactionId: string;
  cancellationClaimType: CancellationClaimType;
  attachmentKey: string;
  notes: string;
  customerClaimDescription: string;
  customerPassportInfo: string;
  customerPhone: string;
  operatorClaimType: OperatorClaimType;
  
  constructor() {
  }
  
  static build(): OperatorOpenCancellationClaimCommandBuilder {
    return new OperatorOpenCancellationClaimCommandBuilder();
  }
}

class OperatorOpenCancellationClaimCommandBuilder {
  private instance: OperatorOpenCancellationClaimCommand;
  
  constructor() {
    this.instance = new OperatorOpenCancellationClaimCommand();
  }
  
  id(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  transactionId(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  cancellationClaimType(value: CancellationClaimType): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.cancellationClaimType = value;
    return this;
  }
  
  attachmentKey(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.attachmentKey = value;
    return this;
  }
  
  notes(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.notes = value;
    return this;
  }
  
  customerClaimDescription(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.customerClaimDescription = value;
    return this;
  }
  
  customerPassportInfo(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.customerPassportInfo = value;
    return this;
  }
  
  customerPhone(value: string): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.customerPhone = value;
    return this;
  }
  
  operatorClaimType(value: OperatorClaimType): OperatorOpenCancellationClaimCommandBuilder {
    this.instance.operatorClaimType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetNumberOfClaimsToCompleteQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetNumberOfClaimsToCompleteQueryBuilder {
    return new OperatorGetNumberOfClaimsToCompleteQueryBuilder();
  }
}

class OperatorGetNumberOfClaimsToCompleteQueryBuilder {
  private instance: OperatorGetNumberOfClaimsToCompleteQuery;
  
  constructor() {
    this.instance = new OperatorGetNumberOfClaimsToCompleteQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorCompleteCancellationClaimCommand {
  id: string;
  
  constructor() {
  }
  
  static build(): OperatorCompleteCancellationClaimCommandBuilder {
    return new OperatorCompleteCancellationClaimCommandBuilder();
  }
}

class OperatorCompleteCancellationClaimCommandBuilder {
  private instance: OperatorCompleteCancellationClaimCommand;
  
  constructor() {
    this.instance = new OperatorCompleteCancellationClaimCommand();
  }
  
  id(value: string): OperatorCompleteCancellationClaimCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCustomersQuery {
  surname: string;
  forename: string;
  phone: string;
  number: string;
  passport: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetCustomersQueryBuilder {
    return new OperatorGetCustomersQueryBuilder();
  }
}

class OperatorGetCustomersQueryBuilder {
  private instance: OperatorGetCustomersQuery;
  
  constructor() {
    this.instance = new OperatorGetCustomersQuery();
  }
  
  surname(value: string): OperatorGetCustomersQueryBuilder {
    this.instance.surname = value;
    return this;
  }
  
  forename(value: string): OperatorGetCustomersQueryBuilder {
    this.instance.forename = value;
    return this;
  }
  
  phone(value: string): OperatorGetCustomersQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  number(value: string): OperatorGetCustomersQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  passport(value: string): OperatorGetCustomersQueryBuilder {
    this.instance.passport = value;
    return this;
  }
  
  page(value: Page): OperatorGetCustomersQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetCustomersQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorUpdateCustomerInfoCommand {
  number: string;
  passportScan: string;
  forename: string;
  surname: string;
  
  constructor() {
  }
  
  static build(): OperatorUpdateCustomerInfoCommandBuilder {
    return new OperatorUpdateCustomerInfoCommandBuilder();
  }
}

class OperatorUpdateCustomerInfoCommandBuilder {
  private instance: OperatorUpdateCustomerInfoCommand;
  
  constructor() {
    this.instance = new OperatorUpdateCustomerInfoCommand();
  }
  
  number(value: string): OperatorUpdateCustomerInfoCommandBuilder {
    this.instance.number = value;
    return this;
  }
  
  passportScan(value: string): OperatorUpdateCustomerInfoCommandBuilder {
    this.instance.passportScan = value;
    return this;
  }
  
  forename(value: string): OperatorUpdateCustomerInfoCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): OperatorUpdateCustomerInfoCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorSetIdentificationDataCommand {
  customerId: string;
  docType: DocumentType;
  nameAm: string;
  surnameAm: string;
  middleNameAm: string;
  nameEn: string;
  surnameEn: string;
  sex: string;
  citizen: string;
  documentNumber: string;
  dateOfBirth: Date;
  dateOfIssue: Date;
  dateOfExpiry: Date;
  authority: string;
  address: string;
  city: string;
  country: string;
  media: string;
  socCardNumber: string;
  
  constructor() {
  }
  
  static build(): OperatorSetIdentificationDataCommandBuilder {
    return new OperatorSetIdentificationDataCommandBuilder();
  }
}

class OperatorSetIdentificationDataCommandBuilder {
  private instance: OperatorSetIdentificationDataCommand;
  
  constructor() {
    this.instance = new OperatorSetIdentificationDataCommand();
  }
  
  customerId(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  docType(value: DocumentType): OperatorSetIdentificationDataCommandBuilder {
    this.instance.docType = value;
    return this;
  }
  
  nameAm(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.nameAm = value;
    return this;
  }
  
  surnameAm(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.surnameAm = value;
    return this;
  }
  
  middleNameAm(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.middleNameAm = value;
    return this;
  }
  
  nameEn(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.nameEn = value;
    return this;
  }
  
  surnameEn(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.surnameEn = value;
    return this;
  }
  
  sex(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.sex = value;
    return this;
  }
  
  citizen(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.citizen = value;
    return this;
  }
  
  documentNumber(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.documentNumber = value;
    return this;
  }
  
  dateOfBirth(value: Date): OperatorSetIdentificationDataCommandBuilder {
    this.instance.dateOfBirth = value;
    return this;
  }
  
  dateOfIssue(value: Date): OperatorSetIdentificationDataCommandBuilder {
    this.instance.dateOfIssue = value;
    return this;
  }
  
  dateOfExpiry(value: Date): OperatorSetIdentificationDataCommandBuilder {
    this.instance.dateOfExpiry = value;
    return this;
  }
  
  authority(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.authority = value;
    return this;
  }
  
  address(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.address = value;
    return this;
  }
  
  city(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.city = value;
    return this;
  }
  
  country(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.country = value;
    return this;
  }
  
  media(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.media = value;
    return this;
  }
  
  socCardNumber(value: string): OperatorSetIdentificationDataCommandBuilder {
    this.instance.socCardNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetIdentificationDataQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetIdentificationDataQueryBuilder {
    return new OperatorGetIdentificationDataQueryBuilder();
  }
}

class OperatorGetIdentificationDataQueryBuilder {
  private instance: OperatorGetIdentificationDataQuery;
  
  constructor() {
    this.instance = new OperatorGetIdentificationDataQuery();
  }
  
  customerId(value: string): OperatorGetIdentificationDataQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetArchivedDocumentsQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetArchivedDocumentsQueryBuilder {
    return new OperatorGetArchivedDocumentsQueryBuilder();
  }
}

class OperatorGetArchivedDocumentsQueryBuilder {
  private instance: OperatorGetArchivedDocumentsQuery;
  
  constructor() {
    this.instance = new OperatorGetArchivedDocumentsQuery();
  }
  
  customerId(value: string): OperatorGetArchivedDocumentsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorTerminateCustomerCommand {
  customerId: string;
  reason: string;
  
  constructor() {
  }
  
  static build(): OperatorTerminateCustomerCommandBuilder {
    return new OperatorTerminateCustomerCommandBuilder();
  }
}

class OperatorTerminateCustomerCommandBuilder {
  private instance: OperatorTerminateCustomerCommand;
  
  constructor() {
    this.instance = new OperatorTerminateCustomerCommand();
  }
  
  customerId(value: string): OperatorTerminateCustomerCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  reason(value: string): OperatorTerminateCustomerCommandBuilder {
    this.instance.reason = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCustomerQuery {
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCustomerQueryBuilder {
    return new OperatorGetCustomerQueryBuilder();
  }
}

class OperatorGetCustomerQueryBuilder {
  private instance: OperatorGetCustomerQuery;
  
  constructor() {
    this.instance = new OperatorGetCustomerQuery();
  }
  
  customerId(value: string): OperatorGetCustomerQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): OperatorGetCustomerQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCustomerTransactionsQuery {
  number: string;
  receiptId: string;
  providerId: string;
  type: Array<UserTransactionType>;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetCustomerTransactionsQueryBuilder {
    return new OperatorGetCustomerTransactionsQueryBuilder();
  }
}

class OperatorGetCustomerTransactionsQueryBuilder {
  private instance: OperatorGetCustomerTransactionsQuery;
  
  constructor() {
    this.instance = new OperatorGetCustomerTransactionsQuery();
  }
  
  number(value: string): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  receiptId(value: string): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  providerId(value: string): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  type(value: Array<UserTransactionType>): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.type = value;
    return this;
  }
  
  dateFrom(value: Date): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  page(value: Page): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetCustomerTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetTransactionByReceiptIdQuery {
  receiptId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetTransactionByReceiptIdQueryBuilder {
    return new OperatorGetTransactionByReceiptIdQueryBuilder();
  }
}

class OperatorGetTransactionByReceiptIdQueryBuilder {
  private instance: OperatorGetTransactionByReceiptIdQuery;
  
  constructor() {
    this.instance = new OperatorGetTransactionByReceiptIdQuery();
  }
  
  receiptId(value: string): OperatorGetTransactionByReceiptIdQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetActivityLogByCustomerIdQuery {
  customerId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): OperatorGetActivityLogByCustomerIdQueryBuilder {
    return new OperatorGetActivityLogByCustomerIdQueryBuilder();
  }
}

class OperatorGetActivityLogByCustomerIdQueryBuilder {
  private instance: OperatorGetActivityLogByCustomerIdQuery;
  
  constructor() {
    this.instance = new OperatorGetActivityLogByCustomerIdQuery();
  }
  
  customerId(value: string): OperatorGetActivityLogByCustomerIdQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  page(value: Page): OperatorGetActivityLogByCustomerIdQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): OperatorGetActivityLogByCustomerIdQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCashboxInfoQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetCashboxInfoQueryBuilder {
    return new OperatorGetCashboxInfoQueryBuilder();
  }
}

class OperatorGetCashboxInfoQueryBuilder {
  private instance: OperatorGetCashboxInfoQuery;
  
  constructor() {
    this.instance = new OperatorGetCashboxInfoQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCustomerCardsQuery {
  customerNum: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCustomerCardsQueryBuilder {
    return new OperatorGetCustomerCardsQueryBuilder();
  }
}

class OperatorGetCustomerCardsQueryBuilder {
  private instance: OperatorGetCustomerCardsQuery;
  
  constructor() {
    this.instance = new OperatorGetCustomerCardsQuery();
  }
  
  customerNum(value: string): OperatorGetCustomerCardsQueryBuilder {
    this.instance.customerNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCountriesQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetCountriesQueryBuilder {
    return new OperatorGetCountriesQueryBuilder();
  }
}

class OperatorGetCountriesQueryBuilder {
  private instance: OperatorGetCountriesQuery;
  
  constructor() {
    this.instance = new OperatorGetCountriesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetCitiesQuery {
  countryId: string;
  
  constructor() {
  }
  
  static build(): OperatorGetCitiesQueryBuilder {
    return new OperatorGetCitiesQueryBuilder();
  }
}

class OperatorGetCitiesQueryBuilder {
  private instance: OperatorGetCitiesQuery;
  
  constructor() {
    this.instance = new OperatorGetCitiesQuery();
  }
  
  countryId(value: string): OperatorGetCitiesQueryBuilder {
    this.instance.countryId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class OperatorGetProviderLabelTreeQuery {
  
  constructor() {
  }
  
  static build(): OperatorGetProviderLabelTreeQueryBuilder {
    return new OperatorGetProviderLabelTreeQueryBuilder();
  }
}

class OperatorGetProviderLabelTreeQueryBuilder {
  private instance: OperatorGetProviderLabelTreeQuery;
  
  constructor() {
    this.instance = new OperatorGetProviderLabelTreeQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsBindCardCommand {
  cardId: string;
  
  constructor() {
  }
  
  static build(): CardsBindCardCommandBuilder {
    return new CardsBindCardCommandBuilder();
  }
}

class CardsBindCardCommandBuilder {
  private instance: CardsBindCardCommand;
  
  constructor() {
    this.instance = new CardsBindCardCommand();
  }
  
  cardId(value: string): CardsBindCardCommandBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsIsBoundQuery {
  cardId: string;
  
  constructor() {
  }
  
  static build(): CardsIsBoundQueryBuilder {
    return new CardsIsBoundQueryBuilder();
  }
}

class CardsIsBoundQueryBuilder {
  private instance: CardsIsBoundQuery;
  
  constructor() {
    this.instance = new CardsIsBoundQuery();
  }
  
  cardId(value: string): CardsIsBoundQueryBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsGetBindingUrlQuery {
  cardId: string;
  
  constructor() {
  }
  
  static build(): CardsGetBindingUrlQueryBuilder {
    return new CardsGetBindingUrlQueryBuilder();
  }
}

class CardsGetBindingUrlQueryBuilder {
  private instance: CardsGetBindingUrlQuery;
  
  constructor() {
    this.instance = new CardsGetBindingUrlQuery();
  }
  
  cardId(value: string): CardsGetBindingUrlQueryBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsGetTopUpUrlQuery {
  operationId: string;
  
  constructor() {
  }
  
  static build(): CardsGetTopUpUrlQueryBuilder {
    return new CardsGetTopUpUrlQueryBuilder();
  }
}

class CardsGetTopUpUrlQueryBuilder {
  private instance: CardsGetTopUpUrlQuery;
  
  constructor() {
    this.instance = new CardsGetTopUpUrlQuery();
  }
  
  operationId(value: string): CardsGetTopUpUrlQueryBuilder {
    this.instance.operationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsProcessBindingCommand {
  orderId: string;
  
  constructor() {
  }
  
  static build(): CardsProcessBindingCommandBuilder {
    return new CardsProcessBindingCommandBuilder();
  }
}

class CardsProcessBindingCommandBuilder {
  private instance: CardsProcessBindingCommand;
  
  constructor() {
    this.instance = new CardsProcessBindingCommand();
  }
  
  orderId(value: string): CardsProcessBindingCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsProcessPaymentCommand {
  orderId: string;
  
  constructor() {
  }
  
  static build(): CardsProcessPaymentCommandBuilder {
    return new CardsProcessPaymentCommandBuilder();
  }
}

class CardsProcessPaymentCommandBuilder {
  private instance: CardsProcessPaymentCommand;
  
  constructor() {
    this.instance = new CardsProcessPaymentCommand();
  }
  
  orderId(value: string): CardsProcessPaymentCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CardsTopUpCommand {
  operationId: string;
  amount: number;
  cardId: string;
  
  constructor() {
  }
  
  static build(): CardsTopUpCommandBuilder {
    return new CardsTopUpCommandBuilder();
  }
}

class CardsTopUpCommandBuilder {
  private instance: CardsTopUpCommand;
  
  constructor() {
    this.instance = new CardsTopUpCommand();
  }
  
  operationId(value: string): CardsTopUpCommandBuilder {
    this.instance.operationId = value;
    return this;
  }
  
  amount(value: number): CardsTopUpCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  cardId(value: string): CardsTopUpCommandBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayCommand {
  transactionId: string;
  amount: number;
  
  constructor() {
  }
  
  static build(): CashPayCommandBuilder {
    return new CashPayCommandBuilder();
  }
}

class CashPayCommandBuilder {
  private instance: CashPayCommand;
  
  constructor() {
    this.instance = new CashPayCommand();
  }
  
  transactionId(value: string): CashPayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  amount(value: number): CashPayCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEvocaCommand {
  agreementCode: string;
  amount: number;
  
  constructor() {
  }
  
  static build(): CashPayEvocaCommandBuilder {
    return new CashPayEvocaCommandBuilder();
  }
}

class CashPayEvocaCommandBuilder {
  private instance: CashPayEvocaCommand;
  
  constructor() {
    this.instance = new CashPayEvocaCommand();
  }
  
  agreementCode(value: string): CashPayEvocaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  amount(value: number): CashPayEvocaCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class EvocaGetLoanDebtQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): EvocaGetLoanDebtQueryBuilder {
    return new EvocaGetLoanDebtQueryBuilder();
  }
}

class EvocaGetLoanDebtQueryBuilder {
  private instance: EvocaGetLoanDebtQuery;
  
  constructor() {
    this.instance = new EvocaGetLoanDebtQuery();
  }
  
  agreementCode(value: string): EvocaGetLoanDebtQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class VivacellObtainBillQuery {
  phone: string;
  
  constructor() {
  }
  
  static build(): VivacellObtainBillQueryBuilder {
    return new VivacellObtainBillQueryBuilder();
  }
}

class VivacellObtainBillQueryBuilder {
  private instance: VivacellObtainBillQuery;
  
  constructor() {
    this.instance = new VivacellObtainBillQuery();
  }
  
  phone(value: string): VivacellObtainBillQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayVivaCellCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  phone: string;
  type: number;
  
  constructor() {
  }
  
  static build(): CashPayVivaCellCommandBuilder {
    return new CashPayVivaCellCommandBuilder();
  }
}

class CashPayVivaCellCommandBuilder {
  private instance: CashPayVivaCellCommand;
  
  constructor() {
    this.instance = new CashPayVivaCellCommand();
  }
  
  transactionId(value: string): CashPayVivaCellCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayVivaCellCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayVivaCellCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayVivaCellCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayVivaCellCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayVivaCellCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayVivaCellCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayVivaCellCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayVivaCellCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  phone(value: string): CashPayVivaCellCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  type(value: number): CashPayVivaCellCommandBuilder {
    this.instance.type = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BeelineObtainBillQuery {
  phone: string;
  
  constructor() {
  }
  
  static build(): BeelineObtainBillQueryBuilder {
    return new BeelineObtainBillQueryBuilder();
  }
}

class BeelineObtainBillQueryBuilder {
  private instance: BeelineObtainBillQuery;
  
  constructor() {
    this.instance = new BeelineObtainBillQuery();
  }
  
  phone(value: string): BeelineObtainBillQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class BeelineTelephoneObtainBillQuery {
  telephone: string;
  
  constructor() {
  }
  
  static build(): BeelineTelephoneObtainBillQueryBuilder {
    return new BeelineTelephoneObtainBillQueryBuilder();
  }
}

class BeelineTelephoneObtainBillQueryBuilder {
  private instance: BeelineTelephoneObtainBillQuery;
  
  constructor() {
    this.instance = new BeelineTelephoneObtainBillQuery();
  }
  
  telephone(value: string): BeelineTelephoneObtainBillQueryBuilder {
    this.instance.telephone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class HiLineBillQuery {
  phoneOrSubscriberNumber: string;
  
  constructor() {
  }
  
  static build(): HiLineBillQueryBuilder {
    return new HiLineBillQueryBuilder();
  }
}

class HiLineBillQueryBuilder {
  private instance: HiLineBillQuery;
  
  constructor() {
    this.instance = new HiLineBillQuery();
  }
  
  phoneOrSubscriberNumber(value: string): HiLineBillQueryBuilder {
    this.instance.phoneOrSubscriberNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayBeelineCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): CashPayBeelineCommandBuilder {
    return new CashPayBeelineCommandBuilder();
  }
}

class CashPayBeelineCommandBuilder {
  private instance: CashPayBeelineCommand;
  
  constructor() {
    this.instance = new CashPayBeelineCommand();
  }
  
  transactionId(value: string): CashPayBeelineCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayBeelineCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayBeelineCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayBeelineCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayBeelineCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayBeelineCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayBeelineCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayBeelineCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayBeelineCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  phone(value: string): CashPayBeelineCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayBeelineTelephoneCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  telephone: string;
  
  constructor() {
  }
  
  static build(): CashPayBeelineTelephoneCommandBuilder {
    return new CashPayBeelineTelephoneCommandBuilder();
  }
}

class CashPayBeelineTelephoneCommandBuilder {
  private instance: CashPayBeelineTelephoneCommand;
  
  constructor() {
    this.instance = new CashPayBeelineTelephoneCommand();
  }
  
  transactionId(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayBeelineTelephoneCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayBeelineTelephoneCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayBeelineTelephoneCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayBeelineTelephoneCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  telephone(value: string): CashPayBeelineTelephoneCommandBuilder {
    this.instance.telephone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayHiLineCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  phoneOrSubscriberNumber: string;
  
  constructor() {
  }
  
  static build(): CashPayHiLineCommandBuilder {
    return new CashPayHiLineCommandBuilder();
  }
}

class CashPayHiLineCommandBuilder {
  private instance: CashPayHiLineCommand;
  
  constructor() {
    this.instance = new CashPayHiLineCommand();
  }
  
  transactionId(value: string): CashPayHiLineCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayHiLineCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayHiLineCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayHiLineCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayHiLineCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayHiLineCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayHiLineCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayHiLineCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayHiLineCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  phoneOrSubscriberNumber(value: string): CashPayHiLineCommandBuilder {
    this.instance.phoneOrSubscriberNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtKarabakhTelekomQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtKarabakhTelekomQueryBuilder {
    return new GetDebtKarabakhTelekomQueryBuilder();
  }
}

class GetDebtKarabakhTelekomQueryBuilder {
  private instance: GetDebtKarabakhTelekomQuery;
  
  constructor() {
    this.instance = new GetDebtKarabakhTelekomQuery();
  }
  
  customerId(value: string): GetDebtKarabakhTelekomQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayKarabakhTelecomCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayKarabakhTelecomCommandBuilder {
    return new CashPayKarabakhTelecomCommandBuilder();
  }
}

class CashPayKarabakhTelecomCommandBuilder {
  private instance: CashPayKarabakhTelecomCommand;
  
  constructor() {
    this.instance = new CashPayKarabakhTelecomCommand();
  }
  
  transactionId(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayKarabakhTelecomCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayKarabakhTelecomCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayKarabakhTelecomCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayKarabakhTelecomCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayKarabakhTelecomCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtUcomMobileQuery {
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtUcomMobileQueryBuilder {
    return new GetDebtUcomMobileQueryBuilder();
  }
}

class GetDebtUcomMobileQueryBuilder {
  private instance: GetDebtUcomMobileQuery;
  
  constructor() {
    this.instance = new GetDebtUcomMobileQuery();
  }
  
  phone(value: string): GetDebtUcomMobileQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtUcomCorporateQuery {
  account: string;
  
  constructor() {
  }
  
  static build(): GetDebtUcomCorporateQueryBuilder {
    return new GetDebtUcomCorporateQueryBuilder();
  }
}

class GetDebtUcomCorporateQueryBuilder {
  private instance: GetDebtUcomCorporateQuery;
  
  constructor() {
    this.instance = new GetDebtUcomCorporateQuery();
  }
  
  account(value: string): GetDebtUcomCorporateQueryBuilder {
    this.instance.account = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayUcomMobileCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  phone: string;
  account: string;
  payMethod: string;
  
  constructor() {
  }
  
  static build(): CashPayUcomMobileCommandBuilder {
    return new CashPayUcomMobileCommandBuilder();
  }
}

class CashPayUcomMobileCommandBuilder {
  private instance: CashPayUcomMobileCommand;
  
  constructor() {
    this.instance = new CashPayUcomMobileCommand();
  }
  
  transactionId(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayUcomMobileCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayUcomMobileCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayUcomMobileCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayUcomMobileCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  phone(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.phone = value;
    return this;
  }
  
  account(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.account = value;
    return this;
  }
  
  payMethod(value: string): CashPayUcomMobileCommandBuilder {
    this.instance.payMethod = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayUcomCorporateCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  account: string;
  payMethod: string;
  
  constructor() {
  }
  
  static build(): CashPayUcomCorporateCommandBuilder {
    return new CashPayUcomCorporateCommandBuilder();
  }
}

class CashPayUcomCorporateCommandBuilder {
  private instance: CashPayUcomCorporateCommand;
  
  constructor() {
    this.instance = new CashPayUcomCorporateCommand();
  }
  
  transactionId(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayUcomCorporateCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayUcomCorporateCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayUcomCorporateCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayUcomCorporateCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  account(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.account = value;
    return this;
  }
  
  payMethod(value: string): CashPayUcomCorporateCommandBuilder {
    this.instance.payMethod = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtUcomFixedQuery {
  number: string;
  
  constructor() {
  }
  
  static build(): GetDebtUcomFixedQueryBuilder {
    return new GetDebtUcomFixedQueryBuilder();
  }
}

class GetDebtUcomFixedQueryBuilder {
  private instance: GetDebtUcomFixedQuery;
  
  constructor() {
    this.instance = new GetDebtUcomFixedQuery();
  }
  
  number(value: string): GetDebtUcomFixedQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayUcomFixedCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  number: string;
  
  constructor() {
  }
  
  static build(): CashPayUcomFixedCommandBuilder {
    return new CashPayUcomFixedCommandBuilder();
  }
}

class CashPayUcomFixedCommandBuilder {
  private instance: CashPayUcomFixedCommand;
  
  constructor() {
    this.instance = new CashPayUcomFixedCommand();
  }
  
  transactionId(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayUcomFixedCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayUcomFixedCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayUcomFixedCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayUcomFixedCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  number(value: string): CashPayUcomFixedCommandBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtElectricityQuery {
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtElectricityQueryBuilder {
    return new GetDebtElectricityQueryBuilder();
  }
}

class GetDebtElectricityQueryBuilder {
  private instance: GetDebtElectricityQuery;
  
  constructor() {
    this.instance = new GetDebtElectricityQuery();
  }
  
  customerId(value: string): GetDebtElectricityQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): GetDebtElectricityQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetMultiDebtElectricityQuery {
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetMultiDebtElectricityQueryBuilder {
    return new GetMultiDebtElectricityQueryBuilder();
  }
}

class GetMultiDebtElectricityQueryBuilder {
  private instance: GetMultiDebtElectricityQuery;
  
  constructor() {
    this.instance = new GetMultiDebtElectricityQuery();
  }
  
  customerId(value: string): GetMultiDebtElectricityQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): GetMultiDebtElectricityQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayElectricityCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayElectricityCommandBuilder {
    return new CashPayElectricityCommandBuilder();
  }
}

class CashPayElectricityCommandBuilder {
  private instance: CashPayElectricityCommand;
  
  constructor() {
    this.instance = new CashPayElectricityCommand();
  }
  
  transactionId(value: string): CashPayElectricityCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayElectricityCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayElectricityCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayElectricityCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayElectricityCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayElectricityCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayElectricityCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayElectricityCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayElectricityCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayElectricityCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetGasBranchesQuery {
  
  constructor() {
  }
  
  static build(): GetGasBranchesQueryBuilder {
    return new GetGasBranchesQueryBuilder();
  }
}

class GetGasBranchesQueryBuilder {
  private instance: GetGasBranchesQuery;
  
  constructor() {
    this.instance = new GetGasBranchesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetGazpromBranchesQuery {
  
  constructor() {
  }
  
  static build(): GetGazpromBranchesQueryBuilder {
    return new GetGazpromBranchesQueryBuilder();
  }
}

class GetGazpromBranchesQueryBuilder {
  private instance: GetGazpromBranchesQuery;
  
  constructor() {
    this.instance = new GetGazpromBranchesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGasQuery {
  branchId: number;
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtGasQueryBuilder {
    return new GetDebtGasQueryBuilder();
  }
}

class GetDebtGasQueryBuilder {
  private instance: GetDebtGasQuery;
  
  constructor() {
    this.instance = new GetDebtGasQuery();
  }
  
  branchId(value: number): GetDebtGasQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  customerId(value: string): GetDebtGasQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): GetDebtGasQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGazpromArmeniaQuery {
  branchId: number;
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtGazpromArmeniaQueryBuilder {
    return new GetDebtGazpromArmeniaQueryBuilder();
  }
}

class GetDebtGazpromArmeniaQueryBuilder {
  private instance: GetDebtGazpromArmeniaQuery;
  
  constructor() {
    this.instance = new GetDebtGazpromArmeniaQuery();
  }
  
  branchId(value: number): GetDebtGazpromArmeniaQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  customerId(value: string): GetDebtGazpromArmeniaQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): GetDebtGazpromArmeniaQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGasCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  branchId: number;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayGasCommandBuilder {
    return new CashPayGasCommandBuilder();
  }
}

class CashPayGasCommandBuilder {
  private instance: CashPayGasCommand;
  
  constructor() {
    this.instance = new CashPayGasCommand();
  }
  
  transactionId(value: string): CashPayGasCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGasCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGasCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGasCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGasCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGasCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGasCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGasCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGasCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  branchId(value: number): CashPayGasCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  customerId(value: string): CashPayGasCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGasServiceCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  branchId: number;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayGasServiceCommandBuilder {
    return new CashPayGasServiceCommandBuilder();
  }
}

class CashPayGasServiceCommandBuilder {
  private instance: CashPayGasServiceCommand;
  
  constructor() {
    this.instance = new CashPayGasServiceCommand();
  }
  
  transactionId(value: string): CashPayGasServiceCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGasServiceCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGasServiceCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGasServiceCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGasServiceCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGasServiceCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGasServiceCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGasServiceCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGasServiceCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  branchId(value: number): CashPayGasServiceCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  customerId(value: string): CashPayGasServiceCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGazpromArmeniaCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  branchId: number;
  branchName: string;
  address: string;
  customerName: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayGazpromArmeniaCommandBuilder {
    return new CashPayGazpromArmeniaCommandBuilder();
  }
}

class CashPayGazpromArmeniaCommandBuilder {
  private instance: CashPayGazpromArmeniaCommand;
  
  constructor() {
    this.instance = new CashPayGazpromArmeniaCommand();
  }
  
  transactionId(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGazpromArmeniaCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGazpromArmeniaCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGazpromArmeniaCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGazpromArmeniaCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  branchId(value: number): CashPayGazpromArmeniaCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  branchName(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.branchName = value;
    return this;
  }
  
  address(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.address = value;
    return this;
  }
  
  customerName(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  customerId(value: string): CashPayGazpromArmeniaCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGazpromArmeniaServiceCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  branchId: number;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayGazpromArmeniaServiceCommandBuilder {
    return new CashPayGazpromArmeniaServiceCommandBuilder();
  }
}

class CashPayGazpromArmeniaServiceCommandBuilder {
  private instance: CashPayGazpromArmeniaServiceCommand;
  
  constructor() {
    this.instance = new CashPayGazpromArmeniaServiceCommand();
  }
  
  transactionId(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  branchId(value: number): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  customerId(value: string): CashPayGazpromArmeniaServiceCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtArtsakhElectricityQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtArtsakhElectricityQueryBuilder {
    return new GetDebtArtsakhElectricityQueryBuilder();
  }
}

class GetDebtArtsakhElectricityQueryBuilder {
  private instance: GetDebtArtsakhElectricityQuery;
  
  constructor() {
    this.instance = new GetDebtArtsakhElectricityQuery();
  }
  
  customerId(value: string): GetDebtArtsakhElectricityQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArtsakhElectricityCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayArtsakhElectricityCommandBuilder {
    return new CashPayArtsakhElectricityCommandBuilder();
  }
}

class CashPayArtsakhElectricityCommandBuilder {
  private instance: CashPayArtsakhElectricityCommand;
  
  constructor() {
    this.instance = new CashPayArtsakhElectricityCommand();
  }
  
  transactionId(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArtsakhElectricityCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArtsakhElectricityCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArtsakhElectricityCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArtsakhElectricityCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayArtsakhElectricityCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtArtsakhGasQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtArtsakhGasQueryBuilder {
    return new GetDebtArtsakhGasQueryBuilder();
  }
}

class GetDebtArtsakhGasQueryBuilder {
  private instance: GetDebtArtsakhGasQuery;
  
  constructor() {
    this.instance = new GetDebtArtsakhGasQuery();
  }
  
  customerId(value: string): GetDebtArtsakhGasQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArtsakhGasCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayArtsakhGasCommandBuilder {
    return new CashPayArtsakhGasCommandBuilder();
  }
}

class CashPayArtsakhGasCommandBuilder {
  private instance: CashPayArtsakhGasCommand;
  
  constructor() {
    this.instance = new CashPayArtsakhGasCommand();
  }
  
  transactionId(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArtsakhGasCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArtsakhGasCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArtsakhGasCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArtsakhGasCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayArtsakhGasCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtRostelecomQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtRostelecomQueryBuilder {
    return new GetDebtRostelecomQueryBuilder();
  }
}

class GetDebtRostelecomQueryBuilder {
  private instance: GetDebtRostelecomQuery;
  
  constructor() {
    this.instance = new GetDebtRostelecomQuery();
  }
  
  customerId(value: string): GetDebtRostelecomQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayRostelecomCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayRostelecomCommandBuilder {
    return new CashPayRostelecomCommandBuilder();
  }
}

class CashPayRostelecomCommandBuilder {
  private instance: CashPayRostelecomCommand;
  
  constructor() {
    this.instance = new CashPayRostelecomCommand();
  }
  
  transactionId(value: string): CashPayRostelecomCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayRostelecomCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayRostelecomCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayRostelecomCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayRostelecomCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayRostelecomCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayRostelecomCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayRostelecomCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayRostelecomCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayRostelecomCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtInteractiveQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtInteractiveQueryBuilder {
    return new GetDebtInteractiveQueryBuilder();
  }
}

class GetDebtInteractiveQueryBuilder {
  private instance: GetDebtInteractiveQuery;
  
  constructor() {
    this.instance = new GetDebtInteractiveQuery();
  }
  
  customerId(value: string): GetDebtInteractiveQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayInteractiveCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayInteractiveCommandBuilder {
    return new CashPayInteractiveCommandBuilder();
  }
}

class CashPayInteractiveCommandBuilder {
  private instance: CashPayInteractiveCommand;
  
  constructor() {
    this.instance = new CashPayInteractiveCommand();
  }
  
  transactionId(value: string): CashPayInteractiveCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayInteractiveCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayInteractiveCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayInteractiveCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayInteractiveCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayInteractiveCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayInteractiveCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayInteractiveCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayInteractiveCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayInteractiveCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtArpinetQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtArpinetQueryBuilder {
    return new GetDebtArpinetQueryBuilder();
  }
}

class GetDebtArpinetQueryBuilder {
  private instance: GetDebtArpinetQuery;
  
  constructor() {
    this.instance = new GetDebtArpinetQuery();
  }
  
  customerId(value: string): GetDebtArpinetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArpinetCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayArpinetCommandBuilder {
    return new CashPayArpinetCommandBuilder();
  }
}

class CashPayArpinetCommandBuilder {
  private instance: CashPayArpinetCommand;
  
  constructor() {
    this.instance = new CashPayArpinetCommand();
  }
  
  transactionId(value: string): CashPayArpinetCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArpinetCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArpinetCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArpinetCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArpinetCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArpinetCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArpinetCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArpinetCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArpinetCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayArpinetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtYournetQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtYournetQueryBuilder {
    return new GetDebtYournetQueryBuilder();
  }
}

class GetDebtYournetQueryBuilder {
  private instance: GetDebtYournetQuery;
  
  constructor() {
    this.instance = new GetDebtYournetQuery();
  }
  
  customerId(value: string): GetDebtYournetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayYourNetCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayYourNetCommandBuilder {
    return new CashPayYourNetCommandBuilder();
  }
}

class CashPayYourNetCommandBuilder {
  private instance: CashPayYourNetCommand;
  
  constructor() {
    this.instance = new CashPayYourNetCommand();
  }
  
  transactionId(value: string): CashPayYourNetCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayYourNetCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayYourNetCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayYourNetCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayYourNetCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayYourNetCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayYourNetCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayYourNetCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayYourNetCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayYourNetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtCtvQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtCtvQueryBuilder {
    return new GetDebtCtvQueryBuilder();
  }
}

class GetDebtCtvQueryBuilder {
  private instance: GetDebtCtvQuery;
  
  constructor() {
    this.instance = new GetDebtCtvQuery();
  }
  
  customerId(value: string): GetDebtCtvQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayCtvCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayCtvCommandBuilder {
    return new CashPayCtvCommandBuilder();
  }
}

class CashPayCtvCommandBuilder {
  private instance: CashPayCtvCommand;
  
  constructor() {
    this.instance = new CashPayCtvCommand();
  }
  
  transactionId(value: string): CashPayCtvCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayCtvCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayCtvCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayCtvCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayCtvCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayCtvCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayCtvCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayCtvCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayCtvCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayCtvCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtWaterQuery {
  customerId: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtWaterQueryBuilder {
    return new GetDebtWaterQueryBuilder();
  }
}

class GetDebtWaterQueryBuilder {
  private instance: GetDebtWaterQuery;
  
  constructor() {
    this.instance = new GetDebtWaterQuery();
  }
  
  customerId(value: string): GetDebtWaterQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  phone(value: string): GetDebtWaterQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayWaterCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayWaterCommandBuilder {
    return new CashPayWaterCommandBuilder();
  }
}

class CashPayWaterCommandBuilder {
  private instance: CashPayWaterCommand;
  
  constructor() {
    this.instance = new CashPayWaterCommand();
  }
  
  transactionId(value: string): CashPayWaterCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayWaterCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayWaterCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayWaterCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayWaterCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayWaterCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayWaterCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayWaterCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayWaterCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayWaterCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtKamurjQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtKamurjQueryBuilder {
    return new GetDebtKamurjQueryBuilder();
  }
}

class GetDebtKamurjQueryBuilder {
  private instance: GetDebtKamurjQuery;
  
  constructor() {
    this.instance = new GetDebtKamurjQuery();
  }
  
  agreementCode(value: string): GetDebtKamurjQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGoodCreditQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtGoodCreditQueryBuilder {
    return new GetDebtGoodCreditQueryBuilder();
  }
}

class GetDebtGoodCreditQueryBuilder {
  private instance: GetDebtGoodCreditQuery;
  
  constructor() {
    this.instance = new GetDebtGoodCreditQuery();
  }
  
  agreementCode(value: string): GetDebtGoodCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtFincaQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtFincaQueryBuilder {
    return new GetDebtFincaQueryBuilder();
  }
}

class GetDebtFincaQueryBuilder {
  private instance: GetDebtFincaQuery;
  
  constructor() {
    this.instance = new GetDebtFincaQuery();
  }
  
  agreementCode(value: string): GetDebtFincaQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtFastCreditQuery {
  agreementCode: string;
  debtType: string;
  
  constructor() {
  }
  
  static build(): GetDebtFastCreditQueryBuilder {
    return new GetDebtFastCreditQueryBuilder();
  }
}

class GetDebtFastCreditQueryBuilder {
  private instance: GetDebtFastCreditQuery;
  
  constructor() {
    this.instance = new GetDebtFastCreditQuery();
  }
  
  agreementCode(value: string): GetDebtFastCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  debtType(value: string): GetDebtFastCreditQueryBuilder {
    this.instance.debtType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGlobalCreditQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtGlobalCreditQueryBuilder {
    return new GetDebtGlobalCreditQueryBuilder();
  }
}

class GetDebtGlobalCreditQueryBuilder {
  private instance: GetDebtGlobalCreditQuery;
  
  constructor() {
    this.instance = new GetDebtGlobalCreditQuery();
  }
  
  agreementCode(value: string): GetDebtGlobalCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtAregakQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtAregakQueryBuilder {
    return new GetDebtAregakQueryBuilder();
  }
}

class GetDebtAregakQueryBuilder {
  private instance: GetDebtAregakQuery;
  
  constructor() {
    this.instance = new GetDebtAregakQuery();
  }
  
  agreementCode(value: string): GetDebtAregakQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtNormanCreditQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtNormanCreditQueryBuilder {
    return new GetDebtNormanCreditQueryBuilder();
  }
}

class GetDebtNormanCreditQueryBuilder {
  private instance: GetDebtNormanCreditQuery;
  
  constructor() {
    this.instance = new GetDebtNormanCreditQuery();
  }
  
  agreementCode(value: string): GetDebtNormanCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtCardAgrocreditQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtCardAgrocreditQueryBuilder {
    return new GetDebtCardAgrocreditQueryBuilder();
  }
}

class GetDebtCardAgrocreditQueryBuilder {
  private instance: GetDebtCardAgrocreditQuery;
  
  constructor() {
    this.instance = new GetDebtCardAgrocreditQuery();
  }
  
  agreementCode(value: string): GetDebtCardAgrocreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtFarmCreditQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtFarmCreditQueryBuilder {
    return new GetDebtFarmCreditQueryBuilder();
  }
}

class GetDebtFarmCreditQueryBuilder {
  private instance: GetDebtFarmCreditQuery;
  
  constructor() {
    this.instance = new GetDebtFarmCreditQuery();
  }
  
  agreementCode(value: string): GetDebtFarmCreditQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtBlessQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtBlessQueryBuilder {
    return new GetDebtBlessQueryBuilder();
  }
}

class GetDebtBlessQueryBuilder {
  private instance: GetDebtBlessQuery;
  
  constructor() {
    this.instance = new GetDebtBlessQuery();
  }
  
  agreementCode(value: string): GetDebtBlessQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtSefInternationalQuery {
  agreementCode: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): GetDebtSefInternationalQueryBuilder {
    return new GetDebtSefInternationalQueryBuilder();
  }
}

class GetDebtSefInternationalQueryBuilder {
  private instance: GetDebtSefInternationalQuery;
  
  constructor() {
    this.instance = new GetDebtSefInternationalQuery();
  }
  
  agreementCode(value: string): GetDebtSefInternationalQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  passport(value: string): GetDebtSefInternationalQueryBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGlobalCreditCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayGlobalCreditCommandBuilder {
    return new CashPayGlobalCreditCommandBuilder();
  }
}

class CashPayGlobalCreditCommandBuilder {
  private instance: CashPayGlobalCreditCommand;
  
  constructor() {
    this.instance = new CashPayGlobalCreditCommand();
  }
  
  transactionId(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGlobalCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGlobalCreditCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGlobalCreditCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGlobalCreditCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayGlobalCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayKamurjCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayKamurjCommandBuilder {
    return new CashPayKamurjCommandBuilder();
  }
}

class CashPayKamurjCommandBuilder {
  private instance: CashPayKamurjCommand;
  
  constructor() {
    this.instance = new CashPayKamurjCommand();
  }
  
  transactionId(value: string): CashPayKamurjCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayKamurjCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayKamurjCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayKamurjCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayKamurjCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayKamurjCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayKamurjCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayKamurjCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayKamurjCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayKamurjCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGoodCreditCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayGoodCreditCommandBuilder {
    return new CashPayGoodCreditCommandBuilder();
  }
}

class CashPayGoodCreditCommandBuilder {
  private instance: CashPayGoodCreditCommand;
  
  constructor() {
    this.instance = new CashPayGoodCreditCommand();
  }
  
  transactionId(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGoodCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGoodCreditCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGoodCreditCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGoodCreditCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayGoodCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAregakCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayAregakCommandBuilder {
    return new CashPayAregakCommandBuilder();
  }
}

class CashPayAregakCommandBuilder {
  private instance: CashPayAregakCommand;
  
  constructor() {
    this.instance = new CashPayAregakCommand();
  }
  
  transactionId(value: string): CashPayAregakCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAregakCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAregakCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAregakCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAregakCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAregakCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAregakCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAregakCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAregakCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayAregakCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayNormanCreditCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayNormanCreditCommandBuilder {
    return new CashPayNormanCreditCommandBuilder();
  }
}

class CashPayNormanCreditCommandBuilder {
  private instance: CashPayNormanCreditCommand;
  
  constructor() {
    this.instance = new CashPayNormanCreditCommand();
  }
  
  transactionId(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayNormanCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayNormanCreditCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayNormanCreditCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayNormanCreditCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayNormanCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayCardAgrocreditCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayCardAgrocreditCommandBuilder {
    return new CashPayCardAgrocreditCommandBuilder();
  }
}

class CashPayCardAgrocreditCommandBuilder {
  private instance: CashPayCardAgrocreditCommand;
  
  constructor() {
    this.instance = new CashPayCardAgrocreditCommand();
  }
  
  transactionId(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayCardAgrocreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayCardAgrocreditCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayCardAgrocreditCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayCardAgrocreditCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayCardAgrocreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFarmCreditArmeniaCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayFarmCreditArmeniaCommandBuilder {
    return new CashPayFarmCreditArmeniaCommandBuilder();
  }
}

class CashPayFarmCreditArmeniaCommandBuilder {
  private instance: CashPayFarmCreditArmeniaCommand;
  
  constructor() {
    this.instance = new CashPayFarmCreditArmeniaCommand();
  }
  
  transactionId(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayFarmCreditArmeniaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayBlessCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayBlessCommandBuilder {
    return new CashPayBlessCommandBuilder();
  }
}

class CashPayBlessCommandBuilder {
  private instance: CashPayBlessCommand;
  
  constructor() {
    this.instance = new CashPayBlessCommand();
  }
  
  transactionId(value: string): CashPayBlessCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayBlessCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayBlessCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayBlessCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayBlessCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayBlessCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayBlessCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayBlessCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayBlessCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayBlessCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPaySefInternationalCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPaySefInternationalCommandBuilder {
    return new CashPaySefInternationalCommandBuilder();
  }
}

class CashPaySefInternationalCommandBuilder {
  private instance: CashPaySefInternationalCommand;
  
  constructor() {
    this.instance = new CashPaySefInternationalCommand();
  }
  
  transactionId(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPaySefInternationalCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPaySefInternationalCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPaySefInternationalCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPaySefInternationalCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  passport(value: string): CashPaySefInternationalCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFincaCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayFincaCommandBuilder {
    return new CashPayFincaCommandBuilder();
  }
}

class CashPayFincaCommandBuilder {
  private instance: CashPayFincaCommand;
  
  constructor() {
    this.instance = new CashPayFincaCommand();
  }
  
  transactionId(value: string): CashPayFincaCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFincaCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFincaCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFincaCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFincaCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFincaCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFincaCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFincaCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFincaCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayFincaCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFastCreditCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  paymentType: string;
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): CashPayFastCreditCommandBuilder {
    return new CashPayFastCreditCommandBuilder();
  }
}

class CashPayFastCreditCommandBuilder {
  private instance: CashPayFastCreditCommand;
  
  constructor() {
    this.instance = new CashPayFastCreditCommand();
  }
  
  transactionId(value: string): CashPayFastCreditCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFastCreditCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFastCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFastCreditCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFastCreditCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFastCreditCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFastCreditCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFastCreditCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFastCreditCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  paymentType(value: string): CashPayFastCreditCommandBuilder {
    this.instance.paymentType = value;
    return this;
  }
  
  agreementCode(value: string): CashPayFastCreditCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtVarksAmQuery {
  agreementCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtVarksAmQueryBuilder {
    return new GetDebtVarksAmQueryBuilder();
  }
}

class GetDebtVarksAmQueryBuilder {
  private instance: GetDebtVarksAmQuery;
  
  constructor() {
    this.instance = new GetDebtVarksAmQuery();
  }
  
  agreementCode(value: string): GetDebtVarksAmQueryBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayVarksAmCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  agreementCode: string;
  invoice: string;
  term: string;
  
  constructor() {
  }
  
  static build(): CashPayVarksAmCommandBuilder {
    return new CashPayVarksAmCommandBuilder();
  }
}

class CashPayVarksAmCommandBuilder {
  private instance: CashPayVarksAmCommand;
  
  constructor() {
    this.instance = new CashPayVarksAmCommand();
  }
  
  transactionId(value: string): CashPayVarksAmCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayVarksAmCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayVarksAmCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayVarksAmCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayVarksAmCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayVarksAmCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayVarksAmCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayVarksAmCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayVarksAmCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  agreementCode(value: string): CashPayVarksAmCommandBuilder {
    this.instance.agreementCode = value;
    return this;
  }
  
  invoice(value: string): CashPayVarksAmCommandBuilder {
    this.instance.invoice = value;
    return this;
  }
  
  term(value: string): CashPayVarksAmCommandBuilder {
    this.instance.term = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtVivaroBetQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtVivaroBetQueryBuilder {
    return new GetDebtVivaroBetQueryBuilder();
  }
}

class GetDebtVivaroBetQueryBuilder {
  private instance: GetDebtVivaroBetQuery;
  
  constructor() {
    this.instance = new GetDebtVivaroBetQuery();
  }
  
  customerId(value: string): GetDebtVivaroBetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayVivarobetCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayVivarobetCommandBuilder {
    return new CashPayVivarobetCommandBuilder();
  }
}

class CashPayVivarobetCommandBuilder {
  private instance: CashPayVivarobetCommand;
  
  constructor() {
    this.instance = new CashPayVivarobetCommand();
  }
  
  transactionId(value: string): CashPayVivarobetCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayVivarobetCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayVivarobetCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayVivarobetCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayVivarobetCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayVivarobetCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayVivarobetCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayVivarobetCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayVivarobetCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayVivarobetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtVivaroCasinoQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtVivaroCasinoQueryBuilder {
    return new GetDebtVivaroCasinoQueryBuilder();
  }
}

class GetDebtVivaroCasinoQueryBuilder {
  private instance: GetDebtVivaroCasinoQuery;
  
  constructor() {
    this.instance = new GetDebtVivaroCasinoQuery();
  }
  
  customerId(value: string): GetDebtVivaroCasinoQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayVivarocasinoCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayVivarocasinoCommandBuilder {
    return new CashPayVivarocasinoCommandBuilder();
  }
}

class CashPayVivarocasinoCommandBuilder {
  private instance: CashPayVivarocasinoCommand;
  
  constructor() {
    this.instance = new CashPayVivarocasinoCommand();
  }
  
  transactionId(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayVivarocasinoCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayVivarocasinoCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayVivarocasinoCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayVivarocasinoCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayVivarocasinoCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtTotoSportQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtTotoSportQueryBuilder {
    return new GetDebtTotoSportQueryBuilder();
  }
}

class GetDebtTotoSportQueryBuilder {
  private instance: GetDebtTotoSportQuery;
  
  constructor() {
    this.instance = new GetDebtTotoSportQuery();
  }
  
  customerId(value: string): GetDebtTotoSportQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayTotoGamingSportCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayTotoGamingSportCommandBuilder {
    return new CashPayTotoGamingSportCommandBuilder();
  }
}

class CashPayTotoGamingSportCommandBuilder {
  private instance: CashPayTotoGamingSportCommand;
  
  constructor() {
    this.instance = new CashPayTotoGamingSportCommand();
  }
  
  transactionId(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayTotoGamingSportCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayTotoGamingSportCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayTotoGamingSportCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayTotoGamingSportCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayTotoGamingSportCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtTotoCasinoQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtTotoCasinoQueryBuilder {
    return new GetDebtTotoCasinoQueryBuilder();
  }
}

class GetDebtTotoCasinoQueryBuilder {
  private instance: GetDebtTotoCasinoQuery;
  
  constructor() {
    this.instance = new GetDebtTotoCasinoQuery();
  }
  
  customerId(value: string): GetDebtTotoCasinoQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayTotoGamingCasinoCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayTotoGamingCasinoCommandBuilder {
    return new CashPayTotoGamingCasinoCommandBuilder();
  }
}

class CashPayTotoGamingCasinoCommandBuilder {
  private instance: CashPayTotoGamingCasinoCommand;
  
  constructor() {
    this.instance = new CashPayTotoGamingCasinoCommand();
  }
  
  transactionId(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayTotoGamingCasinoCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtEurofootballQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtEurofootballQueryBuilder {
    return new GetDebtEurofootballQueryBuilder();
  }
}

class GetDebtEurofootballQueryBuilder {
  private instance: GetDebtEurofootballQuery;
  
  constructor() {
    this.instance = new GetDebtEurofootballQuery();
  }
  
  customerId(value: string): GetDebtEurofootballQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEurofootballCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayEurofootballCommandBuilder {
    return new CashPayEurofootballCommandBuilder();
  }
}

class CashPayEurofootballCommandBuilder {
  private instance: CashPayEurofootballCommand;
  
  constructor() {
    this.instance = new CashPayEurofootballCommand();
  }
  
  transactionId(value: string): CashPayEurofootballCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEurofootballCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEurofootballCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEurofootballCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEurofootballCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEurofootballCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEurofootballCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEurofootballCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEurofootballCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayEurofootballCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtAdjarabetQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtAdjarabetQueryBuilder {
    return new GetDebtAdjarabetQueryBuilder();
  }
}

class GetDebtAdjarabetQueryBuilder {
  private instance: GetDebtAdjarabetQuery;
  
  constructor() {
    this.instance = new GetDebtAdjarabetQuery();
  }
  
  customerId(value: string): GetDebtAdjarabetQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAdjarabetCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayAdjarabetCommandBuilder {
    return new CashPayAdjarabetCommandBuilder();
  }
}

class CashPayAdjarabetCommandBuilder {
  private instance: CashPayAdjarabetCommand;
  
  constructor() {
    this.instance = new CashPayAdjarabetCommand();
  }
  
  transactionId(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAdjarabetCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAdjarabetCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAdjarabetCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAdjarabetCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayAdjarabetCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGoodwinQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtGoodwinQueryBuilder {
    return new GetDebtGoodwinQueryBuilder();
  }
}

class GetDebtGoodwinQueryBuilder {
  private instance: GetDebtGoodwinQuery;
  
  constructor() {
    this.instance = new GetDebtGoodwinQuery();
  }
  
  customerId(value: string): GetDebtGoodwinQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayGoodWinCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayGoodWinCommandBuilder {
    return new CashPayGoodWinCommandBuilder();
  }
}

class CashPayGoodWinCommandBuilder {
  private instance: CashPayGoodWinCommand;
  
  constructor() {
    this.instance = new CashPayGoodWinCommand();
  }
  
  transactionId(value: string): CashPayGoodWinCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayGoodWinCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayGoodWinCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayGoodWinCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayGoodWinCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayGoodWinCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayGoodWinCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayGoodWinCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayGoodWinCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayGoodWinCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtSocialOkQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtSocialOkQueryBuilder {
    return new GetDebtSocialOkQueryBuilder();
  }
}

class GetDebtSocialOkQueryBuilder {
  private instance: GetDebtSocialOkQuery;
  
  constructor() {
    this.instance = new GetDebtSocialOkQuery();
  }
  
  customerId(value: string): GetDebtSocialOkQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPaySocialOkCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPaySocialOkCommandBuilder {
    return new CashPaySocialOkCommandBuilder();
  }
}

class CashPaySocialOkCommandBuilder {
  private instance: CashPaySocialOkCommand;
  
  constructor() {
    this.instance = new CashPaySocialOkCommand();
  }
  
  transactionId(value: string): CashPaySocialOkCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPaySocialOkCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPaySocialOkCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPaySocialOkCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPaySocialOkCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPaySocialOkCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPaySocialOkCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPaySocialOkCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPaySocialOkCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPaySocialOkCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGamesTankiOnlineQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtGamesTankiOnlineQueryBuilder {
    return new GetDebtGamesTankiOnlineQueryBuilder();
  }
}

class GetDebtGamesTankiOnlineQueryBuilder {
  private instance: GetDebtGamesTankiOnlineQuery;
  
  constructor() {
    this.instance = new GetDebtGamesTankiOnlineQuery();
  }
  
  customerId(value: string): GetDebtGamesTankiOnlineQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayTankiOnlineCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayTankiOnlineCommandBuilder {
    return new CashPayTankiOnlineCommandBuilder();
  }
}

class CashPayTankiOnlineCommandBuilder {
  private instance: CashPayTankiOnlineCommand;
  
  constructor() {
    this.instance = new CashPayTankiOnlineCommand();
  }
  
  transactionId(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayTankiOnlineCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayTankiOnlineCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayTankiOnlineCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayTankiOnlineCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayTankiOnlineCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGamesWorldOfTanksQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtGamesWorldOfTanksQueryBuilder {
    return new GetDebtGamesWorldOfTanksQueryBuilder();
  }
}

class GetDebtGamesWorldOfTanksQueryBuilder {
  private instance: GetDebtGamesWorldOfTanksQuery;
  
  constructor() {
    this.instance = new GetDebtGamesWorldOfTanksQuery();
  }
  
  customerId(value: string): GetDebtGamesWorldOfTanksQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayWorldOfTanksCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayWorldOfTanksCommandBuilder {
    return new CashPayWorldOfTanksCommandBuilder();
  }
}

class CashPayWorldOfTanksCommandBuilder {
  private instance: CashPayWorldOfTanksCommand;
  
  constructor() {
    this.instance = new CashPayWorldOfTanksCommand();
  }
  
  transactionId(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayWorldOfTanksCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayWorldOfTanksCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayWorldOfTanksCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayWorldOfTanksCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayWorldOfTanksCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGamesWorldOfWarPlainsQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtGamesWorldOfWarPlainsQueryBuilder {
    return new GetDebtGamesWorldOfWarPlainsQueryBuilder();
  }
}

class GetDebtGamesWorldOfWarPlainsQueryBuilder {
  private instance: GetDebtGamesWorldOfWarPlainsQuery;
  
  constructor() {
    this.instance = new GetDebtGamesWorldOfWarPlainsQuery();
  }
  
  customerId(value: string): GetDebtGamesWorldOfWarPlainsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayWorldOfWarPlainsCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayWorldOfWarPlainsCommandBuilder {
    return new CashPayWorldOfWarPlainsCommandBuilder();
  }
}

class CashPayWorldOfWarPlainsCommandBuilder {
  private instance: CashPayWorldOfWarPlainsCommand;
  
  constructor() {
    this.instance = new CashPayWorldOfWarPlainsCommand();
  }
  
  transactionId(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayWorldOfWarPlainsCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtGamesWorldOfWarShipsQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtGamesWorldOfWarShipsQueryBuilder {
    return new GetDebtGamesWorldOfWarShipsQueryBuilder();
  }
}

class GetDebtGamesWorldOfWarShipsQueryBuilder {
  private instance: GetDebtGamesWorldOfWarShipsQuery;
  
  constructor() {
    this.instance = new GetDebtGamesWorldOfWarShipsQuery();
  }
  
  customerId(value: string): GetDebtGamesWorldOfWarShipsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayWorldOfWarShipsCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayWorldOfWarShipsCommandBuilder {
    return new CashPayWorldOfWarShipsCommandBuilder();
  }
}

class CashPayWorldOfWarShipsCommandBuilder {
  private instance: CashPayWorldOfWarShipsCommand;
  
  constructor() {
    this.instance = new CashPayWorldOfWarShipsCommand();
  }
  
  transactionId(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayWorldOfWarShipsCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtMaryKayQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtMaryKayQueryBuilder {
    return new GetDebtMaryKayQueryBuilder();
  }
}

class GetDebtMaryKayQueryBuilder {
  private instance: GetDebtMaryKayQuery;
  
  constructor() {
    this.instance = new GetDebtMaryKayQuery();
  }
  
  customerId(value: string): GetDebtMaryKayQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayMaryKayCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayMaryKayCommandBuilder {
    return new CashPayMaryKayCommandBuilder();
  }
}

class CashPayMaryKayCommandBuilder {
  private instance: CashPayMaryKayCommand;
  
  constructor() {
    this.instance = new CashPayMaryKayCommand();
  }
  
  transactionId(value: string): CashPayMaryKayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayMaryKayCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayMaryKayCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayMaryKayCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayMaryKayCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayMaryKayCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayMaryKayCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayMaryKayCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayMaryKayCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayMaryKayCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtOriflameQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtOriflameQueryBuilder {
    return new GetDebtOriflameQueryBuilder();
  }
}

class GetDebtOriflameQueryBuilder {
  private instance: GetDebtOriflameQuery;
  
  constructor() {
    this.instance = new GetDebtOriflameQuery();
  }
  
  customerId(value: string): GetDebtOriflameQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayOriflameCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayOriflameCommandBuilder {
    return new CashPayOriflameCommandBuilder();
  }
}

class CashPayOriflameCommandBuilder {
  private instance: CashPayOriflameCommand;
  
  constructor() {
    this.instance = new CashPayOriflameCommand();
  }
  
  transactionId(value: string): CashPayOriflameCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayOriflameCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayOriflameCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayOriflameCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayOriflameCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayOriflameCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayOriflameCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayOriflameCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayOriflameCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayOriflameCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtFaberlicQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtFaberlicQueryBuilder {
    return new GetDebtFaberlicQueryBuilder();
  }
}

class GetDebtFaberlicQueryBuilder {
  private instance: GetDebtFaberlicQuery;
  
  constructor() {
    this.instance = new GetDebtFaberlicQuery();
  }
  
  customerId(value: string): GetDebtFaberlicQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFaberlicCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayFaberlicCommandBuilder {
    return new CashPayFaberlicCommandBuilder();
  }
}

class CashPayFaberlicCommandBuilder {
  private instance: CashPayFaberlicCommand;
  
  constructor() {
    this.instance = new CashPayFaberlicCommand();
  }
  
  transactionId(value: string): CashPayFaberlicCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFaberlicCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFaberlicCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFaberlicCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFaberlicCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFaberlicCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFaberlicCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFaberlicCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFaberlicCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayFaberlicCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtNtvPlusQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtNtvPlusQueryBuilder {
    return new GetDebtNtvPlusQueryBuilder();
  }
}

class GetDebtNtvPlusQueryBuilder {
  private instance: GetDebtNtvPlusQuery;
  
  constructor() {
    this.instance = new GetDebtNtvPlusQuery();
  }
  
  customerId(value: string): GetDebtNtvPlusQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayNtvPlusCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayNtvPlusCommandBuilder {
    return new CashPayNtvPlusCommandBuilder();
  }
}

class CashPayNtvPlusCommandBuilder {
  private instance: CashPayNtvPlusCommand;
  
  constructor() {
    this.instance = new CashPayNtvPlusCommand();
  }
  
  transactionId(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayNtvPlusCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayNtvPlusCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayNtvPlusCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayNtvPlusCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayNtvPlusCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtEkengQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtEkengQueryBuilder {
    return new GetDebtEkengQueryBuilder();
  }
}

class GetDebtEkengQueryBuilder {
  private instance: GetDebtEkengQuery;
  
  constructor() {
    this.instance = new GetDebtEkengQuery();
  }
  
  customerId(value: string): GetDebtEkengQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEkengCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayEkengCommandBuilder {
    return new CashPayEkengCommandBuilder();
  }
}

class CashPayEkengCommandBuilder {
  private instance: CashPayEkengCommand;
  
  constructor() {
    this.instance = new CashPayEkengCommand();
  }
  
  transactionId(value: string): CashPayEkengCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEkengCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEkengCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEkengCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEkengCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEkengCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEkengCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEkengCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEkengCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayEkengCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtPoliceQuery {
  pinCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtPoliceQueryBuilder {
    return new GetDebtPoliceQueryBuilder();
  }
}

class GetDebtPoliceQueryBuilder {
  private instance: GetDebtPoliceQuery;
  
  constructor() {
    this.instance = new GetDebtPoliceQuery();
  }
  
  pinCode(value: string): GetDebtPoliceQueryBuilder {
    this.instance.pinCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtPolicemanQuery {
  actNum: string;
  customerName: string;
  badgeId: string;
  
  constructor() {
  }
  
  static build(): GetDebtPolicemanQueryBuilder {
    return new GetDebtPolicemanQueryBuilder();
  }
}

class GetDebtPolicemanQueryBuilder {
  private instance: GetDebtPolicemanQuery;
  
  constructor() {
    this.instance = new GetDebtPolicemanQuery();
  }
  
  actNum(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.actNum = value;
    return this;
  }
  
  customerName(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  badgeId(value: string): GetDebtPolicemanQueryBuilder {
    this.instance.badgeId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayPoliceCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  actNum: string;
  customerName: string;
  badgeId: string;
  
  constructor() {
  }
  
  static build(): CashPayPoliceCommandBuilder {
    return new CashPayPoliceCommandBuilder();
  }
}

class CashPayPoliceCommandBuilder {
  private instance: CashPayPoliceCommand;
  
  constructor() {
    this.instance = new CashPayPoliceCommand();
  }
  
  transactionId(value: string): CashPayPoliceCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayPoliceCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayPoliceCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayPoliceCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayPoliceCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayPoliceCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayPoliceCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayPoliceCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayPoliceCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  actNum(value: string): CashPayPoliceCommandBuilder {
    this.instance.actNum = value;
    return this;
  }
  
  customerName(value: string): CashPayPoliceCommandBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  badgeId(value: string): CashPayPoliceCommandBuilder {
    this.instance.badgeId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayPolicemanCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  actNum: string;
  customerName: string;
  badgeId: string;
  
  constructor() {
  }
  
  static build(): CashPayPolicemanCommandBuilder {
    return new CashPayPolicemanCommandBuilder();
  }
}

class CashPayPolicemanCommandBuilder {
  private instance: CashPayPolicemanCommand;
  
  constructor() {
    this.instance = new CashPayPolicemanCommand();
  }
  
  transactionId(value: string): CashPayPolicemanCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayPolicemanCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayPolicemanCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayPolicemanCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayPolicemanCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayPolicemanCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayPolicemanCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayPolicemanCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayPolicemanCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  actNum(value: string): CashPayPolicemanCommandBuilder {
    this.instance.actNum = value;
    return this;
  }
  
  customerName(value: string): CashPayPolicemanCommandBuilder {
    this.instance.customerName = value;
    return this;
  }
  
  badgeId(value: string): CashPayPolicemanCommandBuilder {
    this.instance.badgeId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtParkingQuery {
  customerId: string;
  tariffId: number;
  
  constructor() {
  }
  
  static build(): GetDebtParkingQueryBuilder {
    return new GetDebtParkingQueryBuilder();
  }
}

class GetDebtParkingQueryBuilder {
  private instance: GetDebtParkingQuery;
  
  constructor() {
    this.instance = new GetDebtParkingQuery();
  }
  
  customerId(value: string): GetDebtParkingQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  tariffId(value: number): GetDebtParkingQueryBuilder {
    this.instance.tariffId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayParkingCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  tariffId: number;
  
  constructor() {
  }
  
  static build(): CashPayParkingCommandBuilder {
    return new CashPayParkingCommandBuilder();
  }
}

class CashPayParkingCommandBuilder {
  private instance: CashPayParkingCommand;
  
  constructor() {
    this.instance = new CashPayParkingCommand();
  }
  
  transactionId(value: string): CashPayParkingCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayParkingCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayParkingCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayParkingCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayParkingCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayParkingCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayParkingCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayParkingCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayParkingCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayParkingCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  tariffId(value: number): CashPayParkingCommandBuilder {
    this.instance.tariffId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtParkingFineQuery {
  pinCode: string;
  
  constructor() {
  }
  
  static build(): GetDebtParkingFineQueryBuilder {
    return new GetDebtParkingFineQueryBuilder();
  }
}

class GetDebtParkingFineQueryBuilder {
  private instance: GetDebtParkingFineQuery;
  
  constructor() {
    this.instance = new GetDebtParkingFineQuery();
  }
  
  pinCode(value: string): GetDebtParkingFineQueryBuilder {
    this.instance.pinCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayParkingFineCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  pinCode: string;
  
  constructor() {
  }
  
  static build(): CashPayParkingFineCommandBuilder {
    return new CashPayParkingFineCommandBuilder();
  }
}

class CashPayParkingFineCommandBuilder {
  private instance: CashPayParkingFineCommand;
  
  constructor() {
    this.instance = new CashPayParkingFineCommand();
  }
  
  transactionId(value: string): CashPayParkingFineCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayParkingFineCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayParkingFineCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayParkingFineCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayParkingFineCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayParkingFineCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayParkingFineCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayParkingFineCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayParkingFineCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  pinCode(value: string): CashPayParkingFineCommandBuilder {
    this.instance.pinCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtEfuGamesQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtEfuGamesQueryBuilder {
    return new GetDebtEfuGamesQueryBuilder();
  }
}

class GetDebtEfuGamesQueryBuilder {
  private instance: GetDebtEfuGamesQuery;
  
  constructor() {
    this.instance = new GetDebtEfuGamesQuery();
  }
  
  customerId(value: string): GetDebtEfuGamesQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEfuGamesCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayEfuGamesCommandBuilder {
    return new CashPayEfuGamesCommandBuilder();
  }
}

class CashPayEfuGamesCommandBuilder {
  private instance: CashPayEfuGamesCommand;
  
  constructor() {
    this.instance = new CashPayEfuGamesCommand();
  }
  
  transactionId(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEfuGamesCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEfuGamesCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEfuGamesCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEfuGamesCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayEfuGamesCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtIdramWalletQuery {
  accountId: string;
  
  constructor() {
  }
  
  static build(): GetDebtIdramWalletQueryBuilder {
    return new GetDebtIdramWalletQueryBuilder();
  }
}

class GetDebtIdramWalletQueryBuilder {
  private instance: GetDebtIdramWalletQuery;
  
  constructor() {
    this.instance = new GetDebtIdramWalletQuery();
  }
  
  accountId(value: string): GetDebtIdramWalletQueryBuilder {
    this.instance.accountId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayIdramWalletCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  accountId: string;
  
  constructor() {
  }
  
  static build(): CashPayIdramWalletCommandBuilder {
    return new CashPayIdramWalletCommandBuilder();
  }
}

class CashPayIdramWalletCommandBuilder {
  private instance: CashPayIdramWalletCommand;
  
  constructor() {
    this.instance = new CashPayIdramWalletCommand();
  }
  
  transactionId(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayIdramWalletCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayIdramWalletCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayIdramWalletCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayIdramWalletCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  accountId(value: string): CashPayIdramWalletCommandBuilder {
    this.instance.accountId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtUpayIdramWalletQuery {
  accountId: string;
  
  constructor() {
  }
  
  static build(): GetDebtUpayIdramWalletQueryBuilder {
    return new GetDebtUpayIdramWalletQueryBuilder();
  }
}

class GetDebtUpayIdramWalletQueryBuilder {
  private instance: GetDebtUpayIdramWalletQuery;
  
  constructor() {
    this.instance = new GetDebtUpayIdramWalletQuery();
  }
  
  accountId(value: string): GetDebtUpayIdramWalletQueryBuilder {
    this.instance.accountId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayUpayIdramWalletCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  accountId: string;
  
  constructor() {
  }
  
  static build(): CashPayUpayIdramWalletCommandBuilder {
    return new CashPayUpayIdramWalletCommandBuilder();
  }
}

class CashPayUpayIdramWalletCommandBuilder {
  private instance: CashPayUpayIdramWalletCommand;
  
  constructor() {
    this.instance = new CashPayUpayIdramWalletCommand();
  }
  
  transactionId(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayUpayIdramWalletCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayUpayIdramWalletCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayUpayIdramWalletCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayUpayIdramWalletCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  accountId(value: string): CashPayUpayIdramWalletCommandBuilder {
    this.instance.accountId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtTestQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetDebtTestQueryBuilder {
    return new GetDebtTestQueryBuilder();
  }
}

class GetDebtTestQueryBuilder {
  private instance: GetDebtTestQuery;
  
  constructor() {
    this.instance = new GetDebtTestQuery();
  }
  
  code(value: string): GetDebtTestQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayTestCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  reaction: string;
  
  constructor() {
  }
  
  static build(): CashPayTestCommandBuilder {
    return new CashPayTestCommandBuilder();
  }
}

class CashPayTestCommandBuilder {
  private instance: CashPayTestCommand;
  
  constructor() {
    this.instance = new CashPayTestCommand();
  }
  
  transactionId(value: string): CashPayTestCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayTestCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayTestCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayTestCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayTestCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayTestCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayTestCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayTestCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayTestCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayTestCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  reaction(value: string): CashPayTestCommandBuilder {
    this.instance.reaction = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetBillQuery {
  agreement: Agreement;
  
  constructor() {
  }
  
  static build(): GetBillQueryBuilder {
    return new GetBillQueryBuilder();
  }
}

class GetBillQueryBuilder {
  private instance: GetBillQuery;
  
  constructor() {
    this.instance = new GetBillQuery();
  }
  
  agreement(value: Agreement): GetBillQueryBuilder {
    this.instance.agreement = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class PayBillCommand {
  transactionId: string;
  paymentId: string;
  platform: Platform;
  agreement: Agreement;
  
  constructor() {
  }
  
  static build(): PayBillCommandBuilder {
    return new PayBillCommandBuilder();
  }
}

class PayBillCommandBuilder {
  private instance: PayBillCommand;
  
  constructor() {
    this.instance = new PayBillCommand();
  }
  
  transactionId(value: string): PayBillCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): PayBillCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  platform(value: Platform): PayBillCommandBuilder {
    this.instance.platform = value;
    return this;
  }
  
  agreement(value: Agreement): PayBillCommandBuilder {
    this.instance.agreement = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class SearchUcomFixedByPhoneQuery {
  number: string;
  
  constructor() {
  }
  
  static build(): SearchUcomFixedByPhoneQueryBuilder {
    return new SearchUcomFixedByPhoneQueryBuilder();
  }
}

class SearchUcomFixedByPhoneQueryBuilder {
  private instance: SearchUcomFixedByPhoneQuery;
  
  constructor() {
    this.instance = new SearchUcomFixedByPhoneQuery();
  }
  
  number(value: string): SearchUcomFixedByPhoneQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayUpayCustomerCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  number: string;
  
  constructor() {
  }
  
  static build(): CashPayUpayCustomerCommandBuilder {
    return new CashPayUpayCustomerCommandBuilder();
  }
}

class CashPayUpayCustomerCommandBuilder {
  private instance: CashPayUpayCustomerCommand;
  
  constructor() {
    this.instance = new CashPayUpayCustomerCommand();
  }
  
  transactionId(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayUpayCustomerCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayUpayCustomerCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayUpayCustomerCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayUpayCustomerCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  number(value: string): CashPayUpayCustomerCommandBuilder {
    this.instance.number = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CustomerCashoutCommand {
  customerNumber: string;
  transactionId: string;
  orderId: string;
  amount: number;
  document: CashoutDocument;
  commission: number;
  clientData: string;
  contactNum: string;
  
  constructor() {
  }
  
  static build(): CustomerCashoutCommandBuilder {
    return new CustomerCashoutCommandBuilder();
  }
}

class CustomerCashoutCommandBuilder {
  private instance: CustomerCashoutCommand;
  
  constructor() {
    this.instance = new CustomerCashoutCommand();
  }
  
  customerNumber(value: string): CustomerCashoutCommandBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  transactionId(value: string): CustomerCashoutCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  orderId(value: string): CustomerCashoutCommandBuilder {
    this.instance.orderId = value;
    return this;
  }
  
  amount(value: number): CustomerCashoutCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  document(value: CashoutDocument): CustomerCashoutCommandBuilder {
    this.instance.document = value;
    return this;
  }
  
  commission(value: number): CustomerCashoutCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  clientData(value: string): CustomerCashoutCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  contactNum(value: string): CustomerCashoutCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAmeriaContractCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayAmeriaContractCommandBuilder {
    return new CashPayAmeriaContractCommandBuilder();
  }
}

class CashPayAmeriaContractCommandBuilder {
  private instance: CashPayAmeriaContractCommand;
  
  constructor() {
    this.instance = new CashPayAmeriaContractCommand();
  }
  
  transactionId(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAmeriaContractCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAmeriaContractCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAmeriaContractCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAmeriaContractCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayAmeriaContractCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFincaContractRegularCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayFincaContractRegularCommandBuilder {
    return new CashPayFincaContractRegularCommandBuilder();
  }
}

class CashPayFincaContractRegularCommandBuilder {
  private instance: CashPayFincaContractRegularCommand;
  
  constructor() {
    this.instance = new CashPayFincaContractRegularCommand();
  }
  
  transactionId(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFincaContractRegularCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFincaContractRegularCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFincaContractRegularCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFincaContractRegularCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayFincaContractRegularCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayFincaContractPrepayCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayFincaContractPrepayCommandBuilder {
    return new CashPayFincaContractPrepayCommandBuilder();
  }
}

class CashPayFincaContractPrepayCommandBuilder {
  private instance: CashPayFincaContractPrepayCommand;
  
  constructor() {
    this.instance = new CashPayFincaContractPrepayCommand();
  }
  
  transactionId(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayFincaContractPrepayCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayFincaContractPrepayCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayFincaContractPrepayCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayFincaContractPrepayCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayFincaContractPrepayCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayNormanCreditContractRegularCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayNormanCreditContractRegularCommandBuilder {
    return new CashPayNormanCreditContractRegularCommandBuilder();
  }
}

class CashPayNormanCreditContractRegularCommandBuilder {
  private instance: CashPayNormanCreditContractRegularCommand;
  
  constructor() {
    this.instance = new CashPayNormanCreditContractRegularCommand();
  }
  
  transactionId(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayNormanCreditContractRegularCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayNormanCreditContractPrepayCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayNormanCreditContractPrepayCommandBuilder {
    return new CashPayNormanCreditContractPrepayCommandBuilder();
  }
}

class CashPayNormanCreditContractPrepayCommandBuilder {
  private instance: CashPayNormanCreditContractPrepayCommand;
  
  constructor() {
    this.instance = new CashPayNormanCreditContractPrepayCommand();
  }
  
  transactionId(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayNormanCreditContractPrepayCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEvocaContractRegularCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayEvocaContractRegularCommandBuilder {
    return new CashPayEvocaContractRegularCommandBuilder();
  }
}

class CashPayEvocaContractRegularCommandBuilder {
  private instance: CashPayEvocaContractRegularCommand;
  
  constructor() {
    this.instance = new CashPayEvocaContractRegularCommand();
  }
  
  transactionId(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEvocaContractRegularCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEvocaContractRegularCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEvocaContractRegularCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEvocaContractRegularCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayEvocaContractRegularCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEvocaContractPrepayCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayEvocaContractPrepayCommandBuilder {
    return new CashPayEvocaContractPrepayCommandBuilder();
  }
}

class CashPayEvocaContractPrepayCommandBuilder {
  private instance: CashPayEvocaContractPrepayCommand;
  
  constructor() {
    this.instance = new CashPayEvocaContractPrepayCommand();
  }
  
  transactionId(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayEvocaContractPrepayCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetAmeriaContractsQuery {
  code: string;
  passport: string;
  creditCode: string;
  
  constructor() {
  }
  
  static build(): GetAmeriaContractsQueryBuilder {
    return new GetAmeriaContractsQueryBuilder();
  }
}

class GetAmeriaContractsQueryBuilder {
  private instance: GetAmeriaContractsQuery;
  
  constructor() {
    this.instance = new GetAmeriaContractsQuery();
  }
  
  code(value: string): GetAmeriaContractsQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): GetAmeriaContractsQueryBuilder {
    this.instance.passport = value;
    return this;
  }
  
  creditCode(value: string): GetAmeriaContractsQueryBuilder {
    this.instance.creditCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAmeriaAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  aim: string;
  payer: string;
  
  constructor() {
  }
  
  static build(): CashPayAmeriaAccountCommandBuilder {
    return new CashPayAmeriaAccountCommandBuilder();
  }
}

class CashPayAmeriaAccountCommandBuilder {
  private instance: CashPayAmeriaAccountCommand;
  
  constructor() {
    this.instance = new CashPayAmeriaAccountCommand();
  }
  
  transactionId(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAmeriaAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAmeriaAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAmeriaAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAmeriaAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  aim(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.aim = value;
    return this;
  }
  
  payer(value: string): CashPayAmeriaAccountCommandBuilder {
    this.instance.payer = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAmeriaAccountLegalCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  aim: string;
  payer: string;
  
  constructor() {
  }
  
  static build(): CashPayAmeriaAccountLegalCommandBuilder {
    return new CashPayAmeriaAccountLegalCommandBuilder();
  }
}

class CashPayAmeriaAccountLegalCommandBuilder {
  private instance: CashPayAmeriaAccountLegalCommand;
  
  constructor() {
    this.instance = new CashPayAmeriaAccountLegalCommand();
  }
  
  transactionId(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  aim(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.aim = value;
    return this;
  }
  
  payer(value: string): CashPayAmeriaAccountLegalCommandBuilder {
    this.instance.payer = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetAmeriaAccountQuery {
  code: string;
  isAccount: string;
  passport: string;
  mobile: string;
  withBlockInfo: string;
  withPhoneInfo: string;
  
  constructor() {
  }
  
  static build(): GetAmeriaAccountQueryBuilder {
    return new GetAmeriaAccountQueryBuilder();
  }
}

class GetAmeriaAccountQueryBuilder {
  private instance: GetAmeriaAccountQuery;
  
  constructor() {
    this.instance = new GetAmeriaAccountQuery();
  }
  
  code(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  isAccount(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.isAccount = value;
    return this;
  }
  
  passport(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.passport = value;
    return this;
  }
  
  mobile(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.mobile = value;
    return this;
  }
  
  withBlockInfo(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.withBlockInfo = value;
    return this;
  }
  
  withPhoneInfo(value: string): GetAmeriaAccountQueryBuilder {
    this.instance.withPhoneInfo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetEvocaAccountQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetEvocaAccountQueryBuilder {
    return new GetEvocaAccountQueryBuilder();
  }
}

class GetEvocaAccountQueryBuilder {
  private instance: GetEvocaAccountQuery;
  
  constructor() {
    this.instance = new GetEvocaAccountQuery();
  }
  
  code(value: string): GetEvocaAccountQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEvocaAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  
  constructor() {
  }
  
  static build(): CashPayEvocaAccountCommandBuilder {
    return new CashPayEvocaAccountCommandBuilder();
  }
}

class CashPayEvocaAccountCommandBuilder {
  private instance: CashPayEvocaAccountCommand;
  
  constructor() {
    this.instance = new CashPayEvocaAccountCommand();
  }
  
  transactionId(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEvocaAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEvocaAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEvocaAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEvocaAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayEvocaAccountCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayEvocaLegalAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  code: string;
  passport: string;
  taxPayer: string;
  aim: string;
  
  constructor() {
  }
  
  static build(): CashPayEvocaLegalAccountCommandBuilder {
    return new CashPayEvocaLegalAccountCommandBuilder();
  }
}

class CashPayEvocaLegalAccountCommandBuilder {
  private instance: CashPayEvocaLegalAccountCommand;
  
  constructor() {
    this.instance = new CashPayEvocaLegalAccountCommand();
  }
  
  transactionId(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  code(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.code = value;
    return this;
  }
  
  passport(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.passport = value;
    return this;
  }
  
  taxPayer(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.taxPayer = value;
    return this;
  }
  
  aim(value: string): CashPayEvocaLegalAccountCommandBuilder {
    this.instance.aim = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayTransferToCardCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  cardNumber: string;
  surname: string;
  forename: string;
  cardNumberOriginal: string;
  bank: string;
  
  constructor() {
  }
  
  static build(): CashPayTransferToCardCommandBuilder {
    return new CashPayTransferToCardCommandBuilder();
  }
}

class CashPayTransferToCardCommandBuilder {
  private instance: CashPayTransferToCardCommand;
  
  constructor() {
    this.instance = new CashPayTransferToCardCommand();
  }
  
  transactionId(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayTransferToCardCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayTransferToCardCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayTransferToCardCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayTransferToCardCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  cardNumber(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  surname(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.surname = value;
    return this;
  }
  
  forename(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.forename = value;
    return this;
  }
  
  cardNumberOriginal(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.cardNumberOriginal = value;
    return this;
  }
  
  bank(value: string): CashPayTransferToCardCommandBuilder {
    this.instance.bank = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtStroyMasterDamafonQuery {
  customerId: string;
  
  constructor() {
  }
  
  static build(): GetDebtStroyMasterDamafonQueryBuilder {
    return new GetDebtStroyMasterDamafonQueryBuilder();
  }
}

class GetDebtStroyMasterDamafonQueryBuilder {
  private instance: GetDebtStroyMasterDamafonQuery;
  
  constructor() {
    this.instance = new GetDebtStroyMasterDamafonQuery();
  }
  
  customerId(value: string): GetDebtStroyMasterDamafonQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayStroyMasterDamafonCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): CashPayStroyMasterDamafonCommandBuilder {
    return new CashPayStroyMasterDamafonCommandBuilder();
  }
}

class CashPayStroyMasterDamafonCommandBuilder {
  private instance: CashPayStroyMasterDamafonCommand;
  
  constructor() {
    this.instance = new CashPayStroyMasterDamafonCommand();
  }
  
  transactionId(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  customerId(value: string): CashPayStroyMasterDamafonCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetArdshinLoanClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetArdshinLoanClientDataQueryBuilder {
    return new GetArdshinLoanClientDataQueryBuilder();
  }
}

class GetArdshinLoanClientDataQueryBuilder {
  private instance: GetArdshinLoanClientDataQuery;
  
  constructor() {
    this.instance = new GetArdshinLoanClientDataQuery();
  }
  
  productId(value: string): GetArdshinLoanClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetArdshinDepositClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetArdshinDepositClientDataQueryBuilder {
    return new GetArdshinDepositClientDataQueryBuilder();
  }
}

class GetArdshinDepositClientDataQueryBuilder {
  private instance: GetArdshinDepositClientDataQuery;
  
  constructor() {
    this.instance = new GetArdshinDepositClientDataQuery();
  }
  
  productId(value: string): GetArdshinDepositClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetArdshinCardClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetArdshinCardClientDataQueryBuilder {
    return new GetArdshinCardClientDataQueryBuilder();
  }
}

class GetArdshinCardClientDataQueryBuilder {
  private instance: GetArdshinCardClientDataQuery;
  
  constructor() {
    this.instance = new GetArdshinCardClientDataQuery();
  }
  
  productId(value: string): GetArdshinCardClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetArdshinAccountClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetArdshinAccountClientDataQueryBuilder {
    return new GetArdshinAccountClientDataQueryBuilder();
  }
}

class GetArdshinAccountClientDataQueryBuilder {
  private instance: GetArdshinAccountClientDataQuery;
  
  constructor() {
    this.instance = new GetArdshinAccountClientDataQuery();
  }
  
  productId(value: string): GetArdshinAccountClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetArdshinLegalClientDataQuery {
  productId: string;
  taxPayer: string;
  
  constructor() {
  }
  
  static build(): GetArdshinLegalClientDataQueryBuilder {
    return new GetArdshinLegalClientDataQueryBuilder();
  }
}

class GetArdshinLegalClientDataQueryBuilder {
  private instance: GetArdshinLegalClientDataQuery;
  
  constructor() {
    this.instance = new GetArdshinLegalClientDataQuery();
  }
  
  productId(value: string): GetArdshinLegalClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  taxPayer(value: string): GetArdshinLegalClientDataQueryBuilder {
    this.instance.taxPayer = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CheckCardQuery {
  cardNumber: string;
  embossedName: string;
  
  constructor() {
  }
  
  static build(): CheckCardQueryBuilder {
    return new CheckCardQueryBuilder();
  }
}

class CheckCardQueryBuilder {
  private instance: CheckCardQuery;
  
  constructor() {
    this.instance = new CheckCardQuery();
  }
  
  cardNumber(value: string): CheckCardQueryBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  embossedName(value: string): CheckCardQueryBuilder {
    this.instance.embossedName = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetFincaContractRegularQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetFincaContractRegularQueryBuilder {
    return new GetFincaContractRegularQueryBuilder();
  }
}

class GetFincaContractRegularQueryBuilder {
  private instance: GetFincaContractRegularQuery;
  
  constructor() {
    this.instance = new GetFincaContractRegularQuery();
  }
  
  code(value: string): GetFincaContractRegularQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetFincaContractPrepayQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetFincaContractPrepayQueryBuilder {
    return new GetFincaContractPrepayQueryBuilder();
  }
}

class GetFincaContractPrepayQueryBuilder {
  private instance: GetFincaContractPrepayQuery;
  
  constructor() {
    this.instance = new GetFincaContractPrepayQuery();
  }
  
  code(value: string): GetFincaContractPrepayQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetNormanCreditContractRegularQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetNormanCreditContractRegularQueryBuilder {
    return new GetNormanCreditContractRegularQueryBuilder();
  }
}

class GetNormanCreditContractRegularQueryBuilder {
  private instance: GetNormanCreditContractRegularQuery;
  
  constructor() {
    this.instance = new GetNormanCreditContractRegularQuery();
  }
  
  code(value: string): GetNormanCreditContractRegularQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetNormanCreditContractPrepayQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetNormanCreditContractPrepayQueryBuilder {
    return new GetNormanCreditContractPrepayQueryBuilder();
  }
}

class GetNormanCreditContractPrepayQueryBuilder {
  private instance: GetNormanCreditContractPrepayQuery;
  
  constructor() {
    this.instance = new GetNormanCreditContractPrepayQuery();
  }
  
  code(value: string): GetNormanCreditContractPrepayQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetEvocaContractRegularQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetEvocaContractRegularQueryBuilder {
    return new GetEvocaContractRegularQueryBuilder();
  }
}

class GetEvocaContractRegularQueryBuilder {
  private instance: GetEvocaContractRegularQuery;
  
  constructor() {
    this.instance = new GetEvocaContractRegularQuery();
  }
  
  code(value: string): GetEvocaContractRegularQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetEvocaContractPrepayQuery {
  code: string;
  
  constructor() {
  }
  
  static build(): GetEvocaContractPrepayQueryBuilder {
    return new GetEvocaContractPrepayQueryBuilder();
  }
}

class GetEvocaContractPrepayQueryBuilder {
  private instance: GetEvocaContractPrepayQuery;
  
  constructor() {
    this.instance = new GetEvocaContractPrepayQuery();
  }
  
  code(value: string): GetEvocaContractPrepayQueryBuilder {
    this.instance.code = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArdshinLoanCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayArdshinLoanCommandBuilder {
    return new CashPayArdshinLoanCommandBuilder();
  }
}

class CashPayArdshinLoanCommandBuilder {
  private instance: CashPayArdshinLoanCommand;
  
  constructor() {
    this.instance = new CashPayArdshinLoanCommand();
  }
  
  transactionId(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArdshinLoanCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArdshinLoanCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArdshinLoanCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArdshinLoanCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayArdshinLoanCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArdshinDepositCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayArdshinDepositCommandBuilder {
    return new CashPayArdshinDepositCommandBuilder();
  }
}

class CashPayArdshinDepositCommandBuilder {
  private instance: CashPayArdshinDepositCommand;
  
  constructor() {
    this.instance = new CashPayArdshinDepositCommand();
  }
  
  transactionId(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArdshinDepositCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArdshinDepositCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArdshinDepositCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArdshinDepositCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayArdshinDepositCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArdshinCardCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  cardNumber: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayArdshinCardCommandBuilder {
    return new CashPayArdshinCardCommandBuilder();
  }
}

class CashPayArdshinCardCommandBuilder {
  private instance: CashPayArdshinCardCommand;
  
  constructor() {
    this.instance = new CashPayArdshinCardCommand();
  }
  
  transactionId(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArdshinCardCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArdshinCardCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArdshinCardCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArdshinCardCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  cardNumber(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  currRate(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayArdshinCardCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArdshinAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayArdshinAccountCommandBuilder {
    return new CashPayArdshinAccountCommandBuilder();
  }
}

class CashPayArdshinAccountCommandBuilder {
  private instance: CashPayArdshinAccountCommand;
  
  constructor() {
    this.instance = new CashPayArdshinAccountCommand();
  }
  
  transactionId(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArdshinAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArdshinAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArdshinAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArdshinAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayArdshinAccountCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayArdshinLegalCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  taxPayer: string;
  receivName: string;
  purpose: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayArdshinLegalCommandBuilder {
    return new CashPayArdshinLegalCommandBuilder();
  }
}

class CashPayArdshinLegalCommandBuilder {
  private instance: CashPayArdshinLegalCommand;
  
  constructor() {
    this.instance = new CashPayArdshinLegalCommand();
  }
  
  transactionId(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayArdshinLegalCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayArdshinLegalCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayArdshinLegalCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayArdshinLegalCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  taxPayer(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.taxPayer = value;
    return this;
  }
  
  receivName(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.receivName = value;
    return this;
  }
  
  purpose(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.purpose = value;
    return this;
  }
  
  currRate(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayArdshinLegalCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetIdBankLoanClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetIdBankLoanClientDataQueryBuilder {
    return new GetIdBankLoanClientDataQueryBuilder();
  }
}

class GetIdBankLoanClientDataQueryBuilder {
  private instance: GetIdBankLoanClientDataQuery;
  
  constructor() {
    this.instance = new GetIdBankLoanClientDataQuery();
  }
  
  productId(value: string): GetIdBankLoanClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetIdBankCardClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetIdBankCardClientDataQueryBuilder {
    return new GetIdBankCardClientDataQueryBuilder();
  }
}

class GetIdBankCardClientDataQueryBuilder {
  private instance: GetIdBankCardClientDataQuery;
  
  constructor() {
    this.instance = new GetIdBankCardClientDataQuery();
  }
  
  productId(value: string): GetIdBankCardClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetIdBankAccountClientDataQuery {
  productId: string;
  
  constructor() {
  }
  
  static build(): GetIdBankAccountClientDataQueryBuilder {
    return new GetIdBankAccountClientDataQueryBuilder();
  }
}

class GetIdBankAccountClientDataQueryBuilder {
  private instance: GetIdBankAccountClientDataQuery;
  
  constructor() {
    this.instance = new GetIdBankAccountClientDataQuery();
  }
  
  productId(value: string): GetIdBankAccountClientDataQueryBuilder {
    this.instance.productId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayIdBankLoanCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayIdBankLoanCommandBuilder {
    return new CashPayIdBankLoanCommandBuilder();
  }
}

class CashPayIdBankLoanCommandBuilder {
  private instance: CashPayIdBankLoanCommand;
  
  constructor() {
    this.instance = new CashPayIdBankLoanCommand();
  }
  
  transactionId(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayIdBankLoanCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayIdBankLoanCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayIdBankLoanCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayIdBankLoanCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayIdBankLoanCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayIdBankCardCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  cardNumber: string;
  
  constructor() {
  }
  
  static build(): CashPayIdBankCardCommandBuilder {
    return new CashPayIdBankCardCommandBuilder();
  }
}

class CashPayIdBankCardCommandBuilder {
  private instance: CashPayIdBankCardCommand;
  
  constructor() {
    this.instance = new CashPayIdBankCardCommand();
  }
  
  transactionId(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayIdBankCardCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayIdBankCardCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayIdBankCardCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayIdBankCardCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  cardNumber(value: string): CashPayIdBankCardCommandBuilder {
    this.instance.cardNumber = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayIdBankAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  productId: string;
  currRate: string;
  currencyCode: string;
  
  constructor() {
  }
  
  static build(): CashPayIdBankAccountCommandBuilder {
    return new CashPayIdBankAccountCommandBuilder();
  }
}

class CashPayIdBankAccountCommandBuilder {
  private instance: CashPayIdBankAccountCommand;
  
  constructor() {
    this.instance = new CashPayIdBankAccountCommand();
  }
  
  transactionId(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayIdBankAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayIdBankAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayIdBankAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayIdBankAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  productId(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.productId = value;
    return this;
  }
  
  currRate(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.currRate = value;
    return this;
  }
  
  currencyCode(value: string): CashPayIdBankAccountCommandBuilder {
    this.instance.currencyCode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetAcbaAccountProductQuery {
  identifier: string;
  
  constructor() {
  }
  
  static build(): GetAcbaAccountProductQueryBuilder {
    return new GetAcbaAccountProductQueryBuilder();
  }
}

class GetAcbaAccountProductQueryBuilder {
  private instance: GetAcbaAccountProductQuery;
  
  constructor() {
    this.instance = new GetAcbaAccountProductQuery();
  }
  
  identifier(value: string): GetAcbaAccountProductQueryBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetAcbaCardProductQuery {
  identifier: string;
  
  constructor() {
  }
  
  static build(): GetAcbaCardProductQueryBuilder {
    return new GetAcbaCardProductQueryBuilder();
  }
}

class GetAcbaCardProductQueryBuilder {
  private instance: GetAcbaCardProductQuery;
  
  constructor() {
    this.instance = new GetAcbaCardProductQuery();
  }
  
  identifier(value: string): GetAcbaCardProductQueryBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetAcbaLoanProductQuery {
  identifier: string;
  identifierType: AcbaLoanIdentifierType;
  
  constructor() {
  }
  
  static build(): GetAcbaLoanProductQueryBuilder {
    return new GetAcbaLoanProductQueryBuilder();
  }
}

class GetAcbaLoanProductQueryBuilder {
  private instance: GetAcbaLoanProductQuery;
  
  constructor() {
    this.instance = new GetAcbaLoanProductQuery();
  }
  
  identifier(value: string): GetAcbaLoanProductQueryBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  identifierType(value: AcbaLoanIdentifierType): GetAcbaLoanProductQueryBuilder {
    this.instance.identifierType = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAcbaAccountCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  identifier: string;
  docNum: string;
  currency: string;
  
  constructor() {
  }
  
  static build(): CashPayAcbaAccountCommandBuilder {
    return new CashPayAcbaAccountCommandBuilder();
  }
}

class CashPayAcbaAccountCommandBuilder {
  private instance: CashPayAcbaAccountCommand;
  
  constructor() {
    this.instance = new CashPayAcbaAccountCommand();
  }
  
  transactionId(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAcbaAccountCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAcbaAccountCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAcbaAccountCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAcbaAccountCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  identifier(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  docNum(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.docNum = value;
    return this;
  }
  
  currency(value: string): CashPayAcbaAccountCommandBuilder {
    this.instance.currency = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAcbaCardCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  identifier: string;
  docNum: string;
  currency: string;
  
  constructor() {
  }
  
  static build(): CashPayAcbaCardCommandBuilder {
    return new CashPayAcbaCardCommandBuilder();
  }
}

class CashPayAcbaCardCommandBuilder {
  private instance: CashPayAcbaCardCommand;
  
  constructor() {
    this.instance = new CashPayAcbaCardCommand();
  }
  
  transactionId(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAcbaCardCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAcbaCardCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAcbaCardCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAcbaCardCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  identifier(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  docNum(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.docNum = value;
    return this;
  }
  
  currency(value: string): CashPayAcbaCardCommandBuilder {
    this.instance.currency = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CashPayAcbaLoanCommand {
  transactionId: string;
  paymentId: string;
  amount: number;
  debt: number;
  clientData: string;
  commission: number;
  cash: boolean;
  posAuthcode: string;
  contactNum: string;
  identifier: string;
  identifierType: AcbaLoanIdentifierType;
  docNum: string;
  currency: string;
  
  constructor() {
  }
  
  static build(): CashPayAcbaLoanCommandBuilder {
    return new CashPayAcbaLoanCommandBuilder();
  }
}

class CashPayAcbaLoanCommandBuilder {
  private instance: CashPayAcbaLoanCommand;
  
  constructor() {
    this.instance = new CashPayAcbaLoanCommand();
  }
  
  transactionId(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  paymentId(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.paymentId = value;
    return this;
  }
  
  amount(value: number): CashPayAcbaLoanCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  debt(value: number): CashPayAcbaLoanCommandBuilder {
    this.instance.debt = value;
    return this;
  }
  
  clientData(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.clientData = value;
    return this;
  }
  
  commission(value: number): CashPayAcbaLoanCommandBuilder {
    this.instance.commission = value;
    return this;
  }
  
  cash(value: boolean): CashPayAcbaLoanCommandBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  contactNum(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.contactNum = value;
    return this;
  }
  
  identifier(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.identifier = value;
    return this;
  }
  
  identifierType(value: AcbaLoanIdentifierType): CashPayAcbaLoanCommandBuilder {
    this.instance.identifierType = value;
    return this;
  }
  
  docNum(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.docNum = value;
    return this;
  }
  
  currency(value: string): CashPayAcbaLoanCommandBuilder {
    this.instance.currency = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class CheckAcbaPaymentStatusQuery {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): CheckAcbaPaymentStatusQueryBuilder {
    return new CheckAcbaPaymentStatusQueryBuilder();
  }
}

class CheckAcbaPaymentStatusQueryBuilder {
  private instance: CheckAcbaPaymentStatusQuery;
  
  constructor() {
    this.instance = new CheckAcbaPaymentStatusQuery();
  }
  
  transactionId(value: string): CheckAcbaPaymentStatusQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class GetDebtUpayCustomerQuery {
  number: string;
  phone: string;
  
  constructor() {
  }
  
  static build(): GetDebtUpayCustomerQueryBuilder {
    return new GetDebtUpayCustomerQueryBuilder();
  }
}

class GetDebtUpayCustomerQueryBuilder {
  private instance: GetDebtUpayCustomerQuery;
  
  constructor() {
    this.instance = new GetDebtUpayCustomerQuery();
  }
  
  number(value: string): GetDebtUpayCustomerQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  phone(value: string): GetDebtUpayCustomerQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantAuthenticateCommand {
  authenticationId: string;
  accountantId: string;
  username: string;
  password: string;
  
  constructor() {
  }
  
  static build(): AccountantAuthenticateCommandBuilder {
    return new AccountantAuthenticateCommandBuilder();
  }
}

class AccountantAuthenticateCommandBuilder {
  private instance: AccountantAuthenticateCommand;
  
  constructor() {
    this.instance = new AccountantAuthenticateCommand();
  }
  
  authenticationId(value: string): AccountantAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  accountantId(value: string): AccountantAuthenticateCommandBuilder {
    this.instance.accountantId = value;
    return this;
  }
  
  username(value: string): AccountantAuthenticateCommandBuilder {
    this.instance.username = value;
    return this;
  }
  
  password(value: string): AccountantAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantUpdateTokenAccessedCommand {
  tokenId: string;
  
  constructor() {
  }
  
  static build(): AccountantUpdateTokenAccessedCommandBuilder {
    return new AccountantUpdateTokenAccessedCommandBuilder();
  }
}

class AccountantUpdateTokenAccessedCommandBuilder {
  private instance: AccountantUpdateTokenAccessedCommand;
  
  constructor() {
    this.instance = new AccountantUpdateTokenAccessedCommand();
  }
  
  tokenId(value: string): AccountantUpdateTokenAccessedCommandBuilder {
    this.instance.tokenId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetAccessTokenQuery {
  authenticationId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetAccessTokenQueryBuilder {
    return new AccountantGetAccessTokenQueryBuilder();
  }
}

class AccountantGetAccessTokenQueryBuilder {
  private instance: AccountantGetAccessTokenQuery;
  
  constructor() {
    this.instance = new AccountantGetAccessTokenQuery();
  }
  
  authenticationId(value: string): AccountantGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetProfileQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetProfileQueryBuilder {
    return new AccountantGetProfileQueryBuilder();
  }
}

class AccountantGetProfileQueryBuilder {
  private instance: AccountantGetProfileQuery;
  
  constructor() {
    this.instance = new AccountantGetProfileQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantChangePasswordCommand {
  email: string;
  oldPass: string;
  newPass: string;
  
  constructor() {
  }
  
  static build(): AccountantChangePasswordCommandBuilder {
    return new AccountantChangePasswordCommandBuilder();
  }
}

class AccountantChangePasswordCommandBuilder {
  private instance: AccountantChangePasswordCommand;
  
  constructor() {
    this.instance = new AccountantChangePasswordCommand();
  }
  
  email(value: string): AccountantChangePasswordCommandBuilder {
    this.instance.email = value;
    return this;
  }
  
  oldPass(value: string): AccountantChangePasswordCommandBuilder {
    this.instance.oldPass = value;
    return this;
  }
  
  newPass(value: string): AccountantChangePasswordCommandBuilder {
    this.instance.newPass = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCashboxReportQuery {
  id: string;
  
  constructor() {
  }
  
  static build(): AccountantGetCashboxReportQueryBuilder {
    return new AccountantGetCashboxReportQueryBuilder();
  }
}

class AccountantGetCashboxReportQueryBuilder {
  private instance: AccountantGetCashboxReportQuery;
  
  constructor() {
    this.instance = new AccountantGetCashboxReportQuery();
  }
  
  id(value: string): AccountantGetCashboxReportQueryBuilder {
    this.instance.id = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCashboxReportsQuery {
  cashboxId: string;
  branchId: string;
  operatorId: string;
  dateFrom: Date;
  dateTo: Date;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetCashboxReportsQueryBuilder {
    return new AccountantGetCashboxReportsQueryBuilder();
  }
}

class AccountantGetCashboxReportsQueryBuilder {
  private instance: AccountantGetCashboxReportsQuery;
  
  constructor() {
    this.instance = new AccountantGetCashboxReportsQuery();
  }
  
  cashboxId(value: string): AccountantGetCashboxReportsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetCashboxReportsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  operatorId(value: string): AccountantGetCashboxReportsQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetCashboxReportsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetCashboxReportsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  page(value: Page): AccountantGetCashboxReportsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetCashboxReportsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCashboxReportsSummaryQuery {
  cashboxId: string;
  branchId: string;
  operatorId: string;
  dateFrom: Date;
  dateTo: Date;
  
  constructor() {
  }
  
  static build(): AccountantGetCashboxReportsSummaryQueryBuilder {
    return new AccountantGetCashboxReportsSummaryQueryBuilder();
  }
}

class AccountantGetCashboxReportsSummaryQueryBuilder {
  private instance: AccountantGetCashboxReportsSummaryQuery;
  
  constructor() {
    this.instance = new AccountantGetCashboxReportsSummaryQuery();
  }
  
  cashboxId(value: string): AccountantGetCashboxReportsSummaryQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetCashboxReportsSummaryQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  operatorId(value: string): AccountantGetCashboxReportsSummaryQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetCashboxReportsSummaryQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetCashboxReportsSummaryQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDownloadCashboxReportsQuery {
  cashboxId: string;
  branchId: string;
  operatorId: string;
  dateFrom: Date;
  dateTo: Date;
  
  constructor() {
  }
  
  static build(): AccountantDownloadCashboxReportsQueryBuilder {
    return new AccountantDownloadCashboxReportsQueryBuilder();
  }
}

class AccountantDownloadCashboxReportsQueryBuilder {
  private instance: AccountantDownloadCashboxReportsQuery;
  
  constructor() {
    this.instance = new AccountantDownloadCashboxReportsQuery();
  }
  
  cashboxId(value: string): AccountantDownloadCashboxReportsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantDownloadCashboxReportsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  operatorId(value: string): AccountantDownloadCashboxReportsQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantDownloadCashboxReportsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantDownloadCashboxReportsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetProvidersQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetProvidersQueryBuilder {
    return new AccountantGetProvidersQueryBuilder();
  }
}

class AccountantGetProvidersQueryBuilder {
  private instance: AccountantGetProvidersQuery;
  
  constructor() {
    this.instance = new AccountantGetProvidersQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetServiceNamesQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetServiceNamesQueryBuilder {
    return new AccountantGetServiceNamesQueryBuilder();
  }
}

class AccountantGetServiceNamesQueryBuilder {
  private instance: AccountantGetServiceNamesQuery;
  
  constructor() {
    this.instance = new AccountantGetServiceNamesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetTransactionsQuery {
  receiptId: string;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  providerName: string;
  userNum: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  cash: boolean;
  posAuthcode: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetTransactionsQueryBuilder {
    return new AccountantGetTransactionsQueryBuilder();
  }
}

class AccountantGetTransactionsQueryBuilder {
  private instance: AccountantGetTransactionsQuery;
  
  constructor() {
    this.instance = new AccountantGetTransactionsQuery();
  }
  
  receiptId(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  operatorId(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  cashboxId(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerName(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  userNum(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetTransactionsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetTransactionsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): AccountantGetTransactionsQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantGetTransactionsQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  cash(value: boolean): AccountantGetTransactionsQueryBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): AccountantGetTransactionsQueryBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  page(value: Page): AccountantGetTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetTransactionsSummaryQuery {
  receiptId: string;
  branchId: string;
  providerName: string;
  userNum: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  
  constructor() {
  }
  
  static build(): AccountantGetTransactionsSummaryQueryBuilder {
    return new AccountantGetTransactionsSummaryQueryBuilder();
  }
}

class AccountantGetTransactionsSummaryQueryBuilder {
  private instance: AccountantGetTransactionsSummaryQuery;
  
  constructor() {
    this.instance = new AccountantGetTransactionsSummaryQuery();
  }
  
  receiptId(value: string): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerName(value: string): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  userNum(value: string): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantGetTransactionsSummaryQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDownloadTransactionsReportQuery {
  receiptId: string;
  operatorId: string;
  cashboxId: string;
  branchId: string;
  providerName: string;
  userNum: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  cash: boolean;
  posAuthcode: string;
  
  constructor() {
  }
  
  static build(): AccountantDownloadTransactionsReportQueryBuilder {
    return new AccountantDownloadTransactionsReportQueryBuilder();
  }
}

class AccountantDownloadTransactionsReportQueryBuilder {
  private instance: AccountantDownloadTransactionsReportQuery;
  
  constructor() {
    this.instance = new AccountantDownloadTransactionsReportQuery();
  }
  
  receiptId(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  operatorId(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  cashboxId(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerName(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  userNum(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  cash(value: boolean): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.cash = value;
    return this;
  }
  
  posAuthcode(value: string): AccountantDownloadTransactionsReportQueryBuilder {
    this.instance.posAuthcode = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantApproveClaimCommand {
  claimId: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): AccountantApproveClaimCommandBuilder {
    return new AccountantApproveClaimCommandBuilder();
  }
}

class AccountantApproveClaimCommandBuilder {
  private instance: AccountantApproveClaimCommand;
  
  constructor() {
    this.instance = new AccountantApproveClaimCommand();
  }
  
  claimId(value: string): AccountantApproveClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  comment(value: string): AccountantApproveClaimCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRejectClaimCommand {
  claimId: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): AccountantRejectClaimCommandBuilder {
    return new AccountantRejectClaimCommandBuilder();
  }
}

class AccountantRejectClaimCommandBuilder {
  private instance: AccountantRejectClaimCommand;
  
  constructor() {
    this.instance = new AccountantRejectClaimCommand();
  }
  
  claimId(value: string): AccountantRejectClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  comment(value: string): AccountantRejectClaimCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCustomersQuery {
  surname: string;
  forename: string;
  phone: string;
  number: string;
  passportId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetCustomersQueryBuilder {
    return new AccountantGetCustomersQueryBuilder();
  }
}

class AccountantGetCustomersQueryBuilder {
  private instance: AccountantGetCustomersQuery;
  
  constructor() {
    this.instance = new AccountantGetCustomersQuery();
  }
  
  surname(value: string): AccountantGetCustomersQueryBuilder {
    this.instance.surname = value;
    return this;
  }
  
  forename(value: string): AccountantGetCustomersQueryBuilder {
    this.instance.forename = value;
    return this;
  }
  
  phone(value: string): AccountantGetCustomersQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  number(value: string): AccountantGetCustomersQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  passportId(value: string): AccountantGetCustomersQueryBuilder {
    this.instance.passportId = value;
    return this;
  }
  
  page(value: Page): AccountantGetCustomersQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetCustomersQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportPhoneBuyBackServiceDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportPhoneBuyBackServiceDailyCommandBuilder {
    return new AccountantReportPhoneBuyBackServiceDailyCommandBuilder();
  }
}

class AccountantReportPhoneBuyBackServiceDailyCommandBuilder {
  private instance: AccountantReportPhoneBuyBackServiceDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportPhoneBuyBackServiceDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportPhoneBuyBackServiceMonthlyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportPhoneBuyBackServiceMonthlyCommandBuilder {
    return new AccountantReportPhoneBuyBackServiceMonthlyCommandBuilder();
  }
}

class AccountantReportPhoneBuyBackServiceMonthlyCommandBuilder {
  private instance: AccountantReportPhoneBuyBackServiceMonthlyCommand;
  
  constructor() {
    this.instance = new AccountantReportPhoneBuyBackServiceMonthlyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportAmeriaLoanDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportAmeriaLoanDailyCommandBuilder {
    return new AccountantReportAmeriaLoanDailyCommandBuilder();
  }
}

class AccountantReportAmeriaLoanDailyCommandBuilder {
  private instance: AccountantReportAmeriaLoanDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportAmeriaLoanDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportAmeriaAccountDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportAmeriaAccountDailyCommandBuilder {
    return new AccountantReportAmeriaAccountDailyCommandBuilder();
  }
}

class AccountantReportAmeriaAccountDailyCommandBuilder {
  private instance: AccountantReportAmeriaAccountDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportAmeriaAccountDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportArdshinDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportArdshinDailyCommandBuilder {
    return new AccountantReportArdshinDailyCommandBuilder();
  }
}

class AccountantReportArdshinDailyCommandBuilder {
  private instance: AccountantReportArdshinDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportArdshinDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportFincaRegularDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportFincaRegularDailyCommandBuilder {
    return new AccountantReportFincaRegularDailyCommandBuilder();
  }
}

class AccountantReportFincaRegularDailyCommandBuilder {
  private instance: AccountantReportFincaRegularDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportFincaRegularDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportFincaPrepayDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportFincaPrepayDailyCommandBuilder {
    return new AccountantReportFincaPrepayDailyCommandBuilder();
  }
}

class AccountantReportFincaPrepayDailyCommandBuilder {
  private instance: AccountantReportFincaPrepayDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportFincaPrepayDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportNormanCreditRegularDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportNormanCreditRegularDailyCommandBuilder {
    return new AccountantReportNormanCreditRegularDailyCommandBuilder();
  }
}

class AccountantReportNormanCreditRegularDailyCommandBuilder {
  private instance: AccountantReportNormanCreditRegularDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportNormanCreditRegularDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportNormanCreditPrepayDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportNormanCreditPrepayDailyCommandBuilder {
    return new AccountantReportNormanCreditPrepayDailyCommandBuilder();
  }
}

class AccountantReportNormanCreditPrepayDailyCommandBuilder {
  private instance: AccountantReportNormanCreditPrepayDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportNormanCreditPrepayDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportIdBankDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportIdBankDailyCommandBuilder {
    return new AccountantReportIdBankDailyCommandBuilder();
  }
}

class AccountantReportIdBankDailyCommandBuilder {
  private instance: AccountantReportIdBankDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportIdBankDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportAcbaBankDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportAcbaBankDailyCommandBuilder {
    return new AccountantReportAcbaBankDailyCommandBuilder();
  }
}

class AccountantReportAcbaBankDailyCommandBuilder {
  private instance: AccountantReportAcbaBankDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportAcbaBankDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportGlobalCreditDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportGlobalCreditDailyCommandBuilder {
    return new AccountantReportGlobalCreditDailyCommandBuilder();
  }
}

class AccountantReportGlobalCreditDailyCommandBuilder {
  private instance: AccountantReportGlobalCreditDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportGlobalCreditDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportGoodCreditDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportGoodCreditDailyCommandBuilder {
    return new AccountantReportGoodCreditDailyCommandBuilder();
  }
}

class AccountantReportGoodCreditDailyCommandBuilder {
  private instance: AccountantReportGoodCreditDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportGoodCreditDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportEvocaDailyCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportEvocaDailyCommandBuilder {
    return new AccountantReportEvocaDailyCommandBuilder();
  }
}

class AccountantReportEvocaDailyCommandBuilder {
  private instance: AccountantReportEvocaDailyCommand;
  
  constructor() {
    this.instance = new AccountantReportEvocaDailyCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetFailedTransactionsQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetFailedTransactionsQueryBuilder {
    return new AccountantGetFailedTransactionsQueryBuilder();
  }
}

class AccountantGetFailedTransactionsQueryBuilder {
  private instance: AccountantGetFailedTransactionsQuery;
  
  constructor() {
    this.instance = new AccountantGetFailedTransactionsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetFailedBranchTransactionsQuery {
  receiptId: string;
  branchId: string;
  providerId: string;
  userNum: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetFailedBranchTransactionsQueryBuilder {
    return new AccountantGetFailedBranchTransactionsQueryBuilder();
  }
}

class AccountantGetFailedBranchTransactionsQueryBuilder {
  private instance: AccountantGetFailedBranchTransactionsQuery;
  
  constructor() {
    this.instance = new AccountantGetFailedBranchTransactionsQuery();
  }
  
  receiptId(value: string): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  providerId(value: string): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  userNum(value: string): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  page(value: Page): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetFailedBranchTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetFailedCustomerTransactionsQuery {
  accNumber: string;
  phone: string;
  receiptId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetFailedCustomerTransactionsQueryBuilder {
    return new AccountantGetFailedCustomerTransactionsQueryBuilder();
  }
}

class AccountantGetFailedCustomerTransactionsQueryBuilder {
  private instance: AccountantGetFailedCustomerTransactionsQuery;
  
  constructor() {
    this.instance = new AccountantGetFailedCustomerTransactionsQuery();
  }
  
  accNumber(value: string): AccountantGetFailedCustomerTransactionsQueryBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  phone(value: string): AccountantGetFailedCustomerTransactionsQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  receiptId(value: string): AccountantGetFailedCustomerTransactionsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  page(value: Page): AccountantGetFailedCustomerTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetFailedCustomerTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRetryTransactionCommand {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantRetryTransactionCommandBuilder {
    return new AccountantRetryTransactionCommandBuilder();
  }
}

class AccountantRetryTransactionCommandBuilder {
  private instance: AccountantRetryTransactionCommand;
  
  constructor() {
    this.instance = new AccountantRetryTransactionCommand();
  }
  
  transactionId(value: string): AccountantRetryTransactionCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRetryCustomerTransactionCommand {
  customerTransactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantRetryCustomerTransactionCommandBuilder {
    return new AccountantRetryCustomerTransactionCommandBuilder();
  }
}

class AccountantRetryCustomerTransactionCommandBuilder {
  private instance: AccountantRetryCustomerTransactionCommand;
  
  constructor() {
    this.instance = new AccountantRetryCustomerTransactionCommand();
  }
  
  customerTransactionId(value: string): AccountantRetryCustomerTransactionCommandBuilder {
    this.instance.customerTransactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantCompleteTransactionCommand {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantCompleteTransactionCommandBuilder {
    return new AccountantCompleteTransactionCommandBuilder();
  }
}

class AccountantCompleteTransactionCommandBuilder {
  private instance: AccountantCompleteTransactionCommand;
  
  constructor() {
    this.instance = new AccountantCompleteTransactionCommand();
  }
  
  transactionId(value: string): AccountantCompleteTransactionCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantCompleteCustomerTransactionCommand {
  customerTransactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantCompleteCustomerTransactionCommandBuilder {
    return new AccountantCompleteCustomerTransactionCommandBuilder();
  }
}

class AccountantCompleteCustomerTransactionCommandBuilder {
  private instance: AccountantCompleteCustomerTransactionCommand;
  
  constructor() {
    this.instance = new AccountantCompleteCustomerTransactionCommand();
  }
  
  customerTransactionId(value: string): AccountantCompleteCustomerTransactionCommandBuilder {
    this.instance.customerTransactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRefundTransactionCommand {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantRefundTransactionCommandBuilder {
    return new AccountantRefundTransactionCommandBuilder();
  }
}

class AccountantRefundTransactionCommandBuilder {
  private instance: AccountantRefundTransactionCommand;
  
  constructor() {
    this.instance = new AccountantRefundTransactionCommand();
  }
  
  transactionId(value: string): AccountantRefundTransactionCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRefundCustomerTransactionCommand {
  transactionId: string;
  
  constructor() {
  }
  
  static build(): AccountantRefundCustomerTransactionCommandBuilder {
    return new AccountantRefundCustomerTransactionCommandBuilder();
  }
}

class AccountantRefundCustomerTransactionCommandBuilder {
  private instance: AccountantRefundCustomerTransactionCommand;
  
  constructor() {
    this.instance = new AccountantRefundCustomerTransactionCommand();
  }
  
  transactionId(value: string): AccountantRefundCustomerTransactionCommandBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetOperatorsQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetOperatorsQueryBuilder {
    return new AccountantGetOperatorsQueryBuilder();
  }
}

class AccountantGetOperatorsQueryBuilder {
  private instance: AccountantGetOperatorsQuery;
  
  constructor() {
    this.instance = new AccountantGetOperatorsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCashboxesQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetCashboxesQueryBuilder {
    return new AccountantGetCashboxesQueryBuilder();
  }
}

class AccountantGetCashboxesQueryBuilder {
  private instance: AccountantGetCashboxesQuery;
  
  constructor() {
    this.instance = new AccountantGetCashboxesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetBranchesQuery {
  
  constructor() {
  }
  
  static build(): AccountantGetBranchesQueryBuilder {
    return new AccountantGetBranchesQueryBuilder();
  }
}

class AccountantGetBranchesQueryBuilder {
  private instance: AccountantGetBranchesQuery;
  
  constructor() {
    this.instance = new AccountantGetBranchesQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetOperatorQuery {
  operatorId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetOperatorQueryBuilder {
    return new AccountantGetOperatorQueryBuilder();
  }
}

class AccountantGetOperatorQueryBuilder {
  private instance: AccountantGetOperatorQuery;
  
  constructor() {
    this.instance = new AccountantGetOperatorQuery();
  }
  
  operatorId(value: string): AccountantGetOperatorQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetPartnerReportQuery {
  providerName: string;
  cashboxId: string;
  amountFrom: number;
  amountTo: number;
  dateFrom: Date;
  dateTo: Date;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetPartnerReportQueryBuilder {
    return new AccountantGetPartnerReportQueryBuilder();
  }
}

class AccountantGetPartnerReportQueryBuilder {
  private instance: AccountantGetPartnerReportQuery;
  
  constructor() {
    this.instance = new AccountantGetPartnerReportQuery();
  }
  
  providerName(value: string): AccountantGetPartnerReportQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  cashboxId(value: string): AccountantGetPartnerReportQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  amountFrom(value: number): AccountantGetPartnerReportQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantGetPartnerReportQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetPartnerReportQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetPartnerReportQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  page(value: Page): AccountantGetPartnerReportQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetPartnerReportQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDownloadPartnerReportQuery {
  providerName: string;
  cashboxId: string;
  amountFrom: number;
  amountTo: number;
  dateFrom: Date;
  dateTo: Date;
  
  constructor() {
  }
  
  static build(): AccountantDownloadPartnerReportQueryBuilder {
    return new AccountantDownloadPartnerReportQueryBuilder();
  }
}

class AccountantDownloadPartnerReportQueryBuilder {
  private instance: AccountantDownloadPartnerReportQuery;
  
  constructor() {
    this.instance = new AccountantDownloadPartnerReportQuery();
  }
  
  providerName(value: string): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  cashboxId(value: string): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  amountFrom(value: number): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantDownloadPartnerReportQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetPartnerReportSummaryQuery {
  providerName: string;
  cashboxId: string;
  amountFrom: number;
  amountTo: number;
  dateFrom: Date;
  dateTo: Date;
  
  constructor() {
  }
  
  static build(): AccountantGetPartnerReportSummaryQueryBuilder {
    return new AccountantGetPartnerReportSummaryQueryBuilder();
  }
}

class AccountantGetPartnerReportSummaryQueryBuilder {
  private instance: AccountantGetPartnerReportSummaryQuery;
  
  constructor() {
    this.instance = new AccountantGetPartnerReportSummaryQuery();
  }
  
  providerName(value: string): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  cashboxId(value: string): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  amountFrom(value: number): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetPartnerReportSummaryQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetPendingCancellationClaimsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetPendingCancellationClaimsQueryBuilder {
    return new AccountantGetPendingCancellationClaimsQueryBuilder();
  }
}

class AccountantGetPendingCancellationClaimsQueryBuilder {
  private instance: AccountantGetPendingCancellationClaimsQuery;
  
  constructor() {
    this.instance = new AccountantGetPendingCancellationClaimsQuery();
  }
  
  page(value: Page): AccountantGetPendingCancellationClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetPendingCancellationClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantCountPendingCancellationClaimsQuery {
  
  constructor() {
  }
  
  static build(): AccountantCountPendingCancellationClaimsQueryBuilder {
    return new AccountantCountPendingCancellationClaimsQueryBuilder();
  }
}

class AccountantCountPendingCancellationClaimsQueryBuilder {
  private instance: AccountantCountPendingCancellationClaimsQuery;
  
  constructor() {
    this.instance = new AccountantCountPendingCancellationClaimsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetClaimInfoQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetClaimInfoQueryBuilder {
    return new AccountantGetClaimInfoQueryBuilder();
  }
}

class AccountantGetClaimInfoQueryBuilder {
  private instance: AccountantGetClaimInfoQuery;
  
  constructor() {
    this.instance = new AccountantGetClaimInfoQuery();
  }
  
  claimId(value: string): AccountantGetClaimInfoQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetFilteredClaimsQuery {
  status: ClaimStatus;
  providerName: string;
  customerId: string;
  branchId: string;
  userNum: string;
  receiptId: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetFilteredClaimsQueryBuilder {
    return new AccountantGetFilteredClaimsQueryBuilder();
  }
}

class AccountantGetFilteredClaimsQueryBuilder {
  private instance: AccountantGetFilteredClaimsQuery;
  
  constructor() {
    this.instance = new AccountantGetFilteredClaimsQuery();
  }
  
  status(value: ClaimStatus): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.status = value;
    return this;
  }
  
  providerName(value: string): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  customerId(value: string): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  branchId(value: string): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  userNum(value: string): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  receiptId(value: string): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  page(value: Page): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetFilteredClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantAcceptCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantAcceptCancellationClaimCommandBuilder {
    return new AccountantAcceptCancellationClaimCommandBuilder();
  }
}

class AccountantAcceptCancellationClaimCommandBuilder {
  private instance: AccountantAcceptCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantAcceptCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantAcceptCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantAcceptCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDismissCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantDismissCancellationClaimCommandBuilder {
    return new AccountantDismissCancellationClaimCommandBuilder();
  }
}

class AccountantDismissCancellationClaimCommandBuilder {
  private instance: AccountantDismissCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantDismissCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantDismissCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantDismissCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantPreApproveCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantPreApproveCancellationClaimCommandBuilder {
    return new AccountantPreApproveCancellationClaimCommandBuilder();
  }
}

class AccountantPreApproveCancellationClaimCommandBuilder {
  private instance: AccountantPreApproveCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantPreApproveCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantPreApproveCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantPreApproveCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantPreRejectCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantPreRejectCancellationClaimCommandBuilder {
    return new AccountantPreRejectCancellationClaimCommandBuilder();
  }
}

class AccountantPreRejectCancellationClaimCommandBuilder {
  private instance: AccountantPreRejectCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantPreRejectCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantPreRejectCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantPreRejectCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantApproveCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantApproveCancellationClaimCommandBuilder {
    return new AccountantApproveCancellationClaimCommandBuilder();
  }
}

class AccountantApproveCancellationClaimCommandBuilder {
  private instance: AccountantApproveCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantApproveCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantApproveCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantApproveCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRejectCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantRejectCancellationClaimCommandBuilder {
    return new AccountantRejectCancellationClaimCommandBuilder();
  }
}

class AccountantRejectCancellationClaimCommandBuilder {
  private instance: AccountantRejectCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantRejectCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantRejectCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantRejectCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantSearchCustomerCancellationClaimsQuery {
  transactionId: string;
  providerId: string;
  operatorId: string;
  receiptId: string;
  userNum: string;
  dateFrom: Date;
  dateTo: Date;
  amountFrom: number;
  amountTo: number;
  phone: string;
  forename: string;
  surname: string;
  number: string;
  accNumber: string;
  userType: UserType;
  type: CustomerClaimType;
  status: ClaimStatus;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    return new AccountantSearchCustomerCancellationClaimsQueryBuilder();
  }
}

class AccountantSearchCustomerCancellationClaimsQueryBuilder {
  private instance: AccountantSearchCustomerCancellationClaimsQuery;
  
  constructor() {
    this.instance = new AccountantSearchCustomerCancellationClaimsQuery();
  }
  
  transactionId(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  providerId(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.providerId = value;
    return this;
  }
  
  operatorId(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.operatorId = value;
    return this;
  }
  
  receiptId(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.receiptId = value;
    return this;
  }
  
  userNum(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.userNum = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  amountFrom(value: number): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.amountFrom = value;
    return this;
  }
  
  amountTo(value: number): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.amountTo = value;
    return this;
  }
  
  phone(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.surname = value;
    return this;
  }
  
  number(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.number = value;
    return this;
  }
  
  accNumber(value: string): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.accNumber = value;
    return this;
  }
  
  userType(value: UserType): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.userType = value;
    return this;
  }
  
  type(value: CustomerClaimType): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.type = value;
    return this;
  }
  
  status(value: ClaimStatus): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.status = value;
    return this;
  }
  
  page(value: Page): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantSearchCustomerCancellationClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetPendingCustomerCancellationClaimsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetPendingCustomerCancellationClaimsQueryBuilder {
    return new AccountantGetPendingCustomerCancellationClaimsQueryBuilder();
  }
}

class AccountantGetPendingCustomerCancellationClaimsQueryBuilder {
  private instance: AccountantGetPendingCustomerCancellationClaimsQuery;
  
  constructor() {
    this.instance = new AccountantGetPendingCustomerCancellationClaimsQuery();
  }
  
  page(value: Page): AccountantGetPendingCustomerCancellationClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetPendingCustomerCancellationClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantCountPendingCustomerCancellationClaimsQuery {
  
  constructor() {
  }
  
  static build(): AccountantCountPendingCustomerCancellationClaimsQueryBuilder {
    return new AccountantCountPendingCustomerCancellationClaimsQueryBuilder();
  }
}

class AccountantCountPendingCustomerCancellationClaimsQueryBuilder {
  private instance: AccountantCountPendingCustomerCancellationClaimsQuery;
  
  constructor() {
    this.instance = new AccountantCountPendingCustomerCancellationClaimsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCustomerClaimQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetCustomerClaimQueryBuilder {
    return new AccountantGetCustomerClaimQueryBuilder();
  }
}

class AccountantGetCustomerClaimQueryBuilder {
  private instance: AccountantGetCustomerClaimQuery;
  
  constructor() {
    this.instance = new AccountantGetCustomerClaimQuery();
  }
  
  claimId(value: string): AccountantGetCustomerClaimQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantAcceptCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantAcceptCustomerCancellationClaimCommandBuilder {
    return new AccountantAcceptCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantAcceptCustomerCancellationClaimCommandBuilder {
  private instance: AccountantAcceptCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantAcceptCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantAcceptCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantAcceptCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDismissCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantDismissCustomerCancellationClaimCommandBuilder {
    return new AccountantDismissCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantDismissCustomerCancellationClaimCommandBuilder {
  private instance: AccountantDismissCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantDismissCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantDismissCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantDismissCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantPreApproveCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantPreApproveCustomerCancellationClaimCommandBuilder {
    return new AccountantPreApproveCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantPreApproveCustomerCancellationClaimCommandBuilder {
  private instance: AccountantPreApproveCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantPreApproveCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantPreApproveCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantPreApproveCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantPreRejectCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantPreRejectCustomerCancellationClaimCommandBuilder {
    return new AccountantPreRejectCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantPreRejectCustomerCancellationClaimCommandBuilder {
  private instance: AccountantPreRejectCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantPreRejectCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantPreRejectCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantPreRejectCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantApproveCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantApproveCustomerCancellationClaimCommandBuilder {
    return new AccountantApproveCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantApproveCustomerCancellationClaimCommandBuilder {
  private instance: AccountantApproveCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantApproveCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantApproveCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantApproveCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRejectCustomerCancellationClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantRejectCustomerCancellationClaimCommandBuilder {
    return new AccountantRejectCustomerCancellationClaimCommandBuilder();
  }
}

class AccountantRejectCustomerCancellationClaimCommandBuilder {
  private instance: AccountantRejectCustomerCancellationClaimCommand;
  
  constructor() {
    this.instance = new AccountantRejectCustomerCancellationClaimCommand();
  }
  
  claimId(value: string): AccountantRejectCustomerCancellationClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantRejectCustomerCancellationClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetClaimHistoryQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetClaimHistoryQueryBuilder {
    return new AccountantGetClaimHistoryQueryBuilder();
  }
}

class AccountantGetClaimHistoryQueryBuilder {
  private instance: AccountantGetClaimHistoryQuery;
  
  constructor() {
    this.instance = new AccountantGetClaimHistoryQuery();
  }
  
  claimId(value: string): AccountantGetClaimHistoryQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetCustomerClaimHistoryQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetCustomerClaimHistoryQueryBuilder {
    return new AccountantGetCustomerClaimHistoryQueryBuilder();
  }
}

class AccountantGetCustomerClaimHistoryQueryBuilder {
  private instance: AccountantGetCustomerClaimHistoryQuery;
  
  constructor() {
    this.instance = new AccountantGetCustomerClaimHistoryQuery();
  }
  
  claimId(value: string): AccountantGetCustomerClaimHistoryQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantAddReportCommand {
  providerName: string;
  amount: number;
  date: Date;
  cashboxName: string;
  cashboxId: string;
  branchId: string;
  commissionAmount: number;
  
  constructor() {
  }
  
  static build(): AccountantAddReportCommandBuilder {
    return new AccountantAddReportCommandBuilder();
  }
}

class AccountantAddReportCommandBuilder {
  private instance: AccountantAddReportCommand;
  
  constructor() {
    this.instance = new AccountantAddReportCommand();
  }
  
  providerName(value: string): AccountantAddReportCommandBuilder {
    this.instance.providerName = value;
    return this;
  }
  
  amount(value: number): AccountantAddReportCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  date(value: Date): AccountantAddReportCommandBuilder {
    this.instance.date = value;
    return this;
  }
  
  cashboxName(value: string): AccountantAddReportCommandBuilder {
    this.instance.cashboxName = value;
    return this;
  }
  
  cashboxId(value: string): AccountantAddReportCommandBuilder {
    this.instance.cashboxId = value;
    return this;
  }
  
  branchId(value: string): AccountantAddReportCommandBuilder {
    this.instance.branchId = value;
    return this;
  }
  
  commissionAmount(value: number): AccountantAddReportCommandBuilder {
    this.instance.commissionAmount = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportInitRepoCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportInitRepoCommandBuilder {
    return new AccountantReportInitRepoCommandBuilder();
  }
}

class AccountantReportInitRepoCommandBuilder {
  private instance: AccountantReportInitRepoCommand;
  
  constructor() {
    this.instance = new AccountantReportInitRepoCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportRestoreCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportRestoreCommandBuilder {
    return new AccountantReportRestoreCommandBuilder();
  }
}

class AccountantReportRestoreCommandBuilder {
  private instance: AccountantReportRestoreCommand;
  
  constructor() {
    this.instance = new AccountantReportRestoreCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportBackupCommand {
  
  constructor() {
  }
  
  static build(): AccountantReportBackupCommandBuilder {
    return new AccountantReportBackupCommandBuilder();
  }
}

class AccountantReportBackupCommandBuilder {
  private instance: AccountantReportBackupCommand;
  
  constructor() {
    this.instance = new AccountantReportBackupCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantReportRecalculateCommand {
  date: string;
  
  constructor() {
  }
  
  static build(): AccountantReportRecalculateCommandBuilder {
    return new AccountantReportRecalculateCommandBuilder();
  }
}

class AccountantReportRecalculateCommandBuilder {
  private instance: AccountantReportRecalculateCommand;
  
  constructor() {
    this.instance = new AccountantReportRecalculateCommand();
  }
  
  date(value: string): AccountantReportRecalculateCommandBuilder {
    this.instance.date = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantFreezeAccountCommand {
  customerNumber: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): AccountantFreezeAccountCommandBuilder {
    return new AccountantFreezeAccountCommandBuilder();
  }
}

class AccountantFreezeAccountCommandBuilder {
  private instance: AccountantFreezeAccountCommand;
  
  constructor() {
    this.instance = new AccountantFreezeAccountCommand();
  }
  
  customerNumber(value: string): AccountantFreezeAccountCommandBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  comment(value: string): AccountantFreezeAccountCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantUnfreezeAccountCommand {
  customerNumber: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): AccountantUnfreezeAccountCommandBuilder {
    return new AccountantUnfreezeAccountCommandBuilder();
  }
}

class AccountantUnfreezeAccountCommandBuilder {
  private instance: AccountantUnfreezeAccountCommand;
  
  constructor() {
    this.instance = new AccountantUnfreezeAccountCommand();
  }
  
  customerNumber(value: string): AccountantUnfreezeAccountCommandBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  comment(value: string): AccountantUnfreezeAccountCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantOpenUnfreezeAccountClaimCommand {
  id: string;
  customerNumber: string;
  attachmentKey: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): AccountantOpenUnfreezeAccountClaimCommandBuilder {
    return new AccountantOpenUnfreezeAccountClaimCommandBuilder();
  }
}

class AccountantOpenUnfreezeAccountClaimCommandBuilder {
  private instance: AccountantOpenUnfreezeAccountClaimCommand;
  
  constructor() {
    this.instance = new AccountantOpenUnfreezeAccountClaimCommand();
  }
  
  id(value: string): AccountantOpenUnfreezeAccountClaimCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  customerNumber(value: string): AccountantOpenUnfreezeAccountClaimCommandBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  attachmentKey(value: string): AccountantOpenUnfreezeAccountClaimCommandBuilder {
    this.instance.attachmentKey = value;
    return this;
  }
  
  comment(value: string): AccountantOpenUnfreezeAccountClaimCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantAcceptUnfreezeAccountClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantAcceptUnfreezeAccountClaimCommandBuilder {
    return new AccountantAcceptUnfreezeAccountClaimCommandBuilder();
  }
}

class AccountantAcceptUnfreezeAccountClaimCommandBuilder {
  private instance: AccountantAcceptUnfreezeAccountClaimCommand;
  
  constructor() {
    this.instance = new AccountantAcceptUnfreezeAccountClaimCommand();
  }
  
  claimId(value: string): AccountantAcceptUnfreezeAccountClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantAcceptUnfreezeAccountClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDismissUnfreezeAccountClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantDismissUnfreezeAccountClaimCommandBuilder {
    return new AccountantDismissUnfreezeAccountClaimCommandBuilder();
  }
}

class AccountantDismissUnfreezeAccountClaimCommandBuilder {
  private instance: AccountantDismissUnfreezeAccountClaimCommand;
  
  constructor() {
    this.instance = new AccountantDismissUnfreezeAccountClaimCommand();
  }
  
  claimId(value: string): AccountantDismissUnfreezeAccountClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantDismissUnfreezeAccountClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantApproveUnfreezeAccountClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantApproveUnfreezeAccountClaimCommandBuilder {
    return new AccountantApproveUnfreezeAccountClaimCommandBuilder();
  }
}

class AccountantApproveUnfreezeAccountClaimCommandBuilder {
  private instance: AccountantApproveUnfreezeAccountClaimCommand;
  
  constructor() {
    this.instance = new AccountantApproveUnfreezeAccountClaimCommand();
  }
  
  claimId(value: string): AccountantApproveUnfreezeAccountClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantApproveUnfreezeAccountClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantRejectUnfreezeAccountClaimCommand {
  claimId: string;
  examinationNotes: string;
  
  constructor() {
  }
  
  static build(): AccountantRejectUnfreezeAccountClaimCommandBuilder {
    return new AccountantRejectUnfreezeAccountClaimCommandBuilder();
  }
}

class AccountantRejectUnfreezeAccountClaimCommandBuilder {
  private instance: AccountantRejectUnfreezeAccountClaimCommand;
  
  constructor() {
    this.instance = new AccountantRejectUnfreezeAccountClaimCommand();
  }
  
  claimId(value: string): AccountantRejectUnfreezeAccountClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  examinationNotes(value: string): AccountantRejectUnfreezeAccountClaimCommandBuilder {
    this.instance.examinationNotes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantSendSingleNotificationCommand {
  customerId: string;
  title: string;
  body: string;
  
  constructor() {
  }
  
  static build(): AccountantSendSingleNotificationCommandBuilder {
    return new AccountantSendSingleNotificationCommandBuilder();
  }
}

class AccountantSendSingleNotificationCommandBuilder {
  private instance: AccountantSendSingleNotificationCommand;
  
  constructor() {
    this.instance = new AccountantSendSingleNotificationCommand();
  }
  
  customerId(value: string): AccountantSendSingleNotificationCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  title(value: string): AccountantSendSingleNotificationCommandBuilder {
    this.instance.title = value;
    return this;
  }
  
  body(value: string): AccountantSendSingleNotificationCommandBuilder {
    this.instance.body = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantSendGroupNotificationCommand {
  customerId: Array<string>;
  title: string;
  body: string;
  
  constructor() {
  }
  
  static build(): AccountantSendGroupNotificationCommandBuilder {
    return new AccountantSendGroupNotificationCommandBuilder();
  }
}

class AccountantSendGroupNotificationCommandBuilder {
  private instance: AccountantSendGroupNotificationCommand;
  
  constructor() {
    this.instance = new AccountantSendGroupNotificationCommand();
  }
  
  customerId(value: Array<string>): AccountantSendGroupNotificationCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  title(value: string): AccountantSendGroupNotificationCommandBuilder {
    this.instance.title = value;
    return this;
  }
  
  body(value: string): AccountantSendGroupNotificationCommandBuilder {
    this.instance.body = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetUnfreezeAccountClaimQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): AccountantGetUnfreezeAccountClaimQueryBuilder {
    return new AccountantGetUnfreezeAccountClaimQueryBuilder();
  }
}

class AccountantGetUnfreezeAccountClaimQueryBuilder {
  private instance: AccountantGetUnfreezeAccountClaimQuery;
  
  constructor() {
    this.instance = new AccountantGetUnfreezeAccountClaimQuery();
  }
  
  claimId(value: string): AccountantGetUnfreezeAccountClaimQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantGetPendingUnfreezeAccountClaimsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantGetPendingUnfreezeAccountClaimsQueryBuilder {
    return new AccountantGetPendingUnfreezeAccountClaimsQueryBuilder();
  }
}

class AccountantGetPendingUnfreezeAccountClaimsQueryBuilder {
  private instance: AccountantGetPendingUnfreezeAccountClaimsQuery;
  
  constructor() {
    this.instance = new AccountantGetPendingUnfreezeAccountClaimsQuery();
  }
  
  page(value: Page): AccountantGetPendingUnfreezeAccountClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantGetPendingUnfreezeAccountClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantCountPendingUnfreezeAccountClaimsQuery {
  
  constructor() {
  }
  
  static build(): AccountantCountPendingUnfreezeAccountClaimsQueryBuilder {
    return new AccountantCountPendingUnfreezeAccountClaimsQueryBuilder();
  }
}

class AccountantCountPendingUnfreezeAccountClaimsQueryBuilder {
  private instance: AccountantCountPendingUnfreezeAccountClaimsQuery;
  
  constructor() {
    this.instance = new AccountantCountPendingUnfreezeAccountClaimsQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantSearchUnfreezeAccountClaimsQuery {
  customerNumber: string;
  dateFrom: Date;
  dateTo: Date;
  phone: string;
  forename: string;
  surname: string;
  userType: UserType;
  status: ClaimStatus;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    return new AccountantSearchUnfreezeAccountClaimsQueryBuilder();
  }
}

class AccountantSearchUnfreezeAccountClaimsQueryBuilder {
  private instance: AccountantSearchUnfreezeAccountClaimsQuery;
  
  constructor() {
    this.instance = new AccountantSearchUnfreezeAccountClaimsQuery();
  }
  
  customerNumber(value: string): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.customerNumber = value;
    return this;
  }
  
  dateFrom(value: Date): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.dateFrom = value;
    return this;
  }
  
  dateTo(value: Date): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.dateTo = value;
    return this;
  }
  
  phone(value: string): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.phone = value;
    return this;
  }
  
  forename(value: string): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.forename = value;
    return this;
  }
  
  surname(value: string): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.surname = value;
    return this;
  }
  
  userType(value: UserType): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.userType = value;
    return this;
  }
  
  status(value: ClaimStatus): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.status = value;
    return this;
  }
  
  page(value: Page): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): AccountantSearchUnfreezeAccountClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantNotifySingleCommand {
  customerId: string;
  title: string;
  body: string;
  priority: string;
  registrationId: string;
  
  constructor() {
  }
  
  static build(): AccountantNotifySingleCommandBuilder {
    return new AccountantNotifySingleCommandBuilder();
  }
}

class AccountantNotifySingleCommandBuilder {
  private instance: AccountantNotifySingleCommand;
  
  constructor() {
    this.instance = new AccountantNotifySingleCommand();
  }
  
  customerId(value: string): AccountantNotifySingleCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  title(value: string): AccountantNotifySingleCommandBuilder {
    this.instance.title = value;
    return this;
  }
  
  body(value: string): AccountantNotifySingleCommandBuilder {
    this.instance.body = value;
    return this;
  }
  
  priority(value: string): AccountantNotifySingleCommandBuilder {
    this.instance.priority = value;
    return this;
  }
  
  registrationId(value: string): AccountantNotifySingleCommandBuilder {
    this.instance.registrationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantNotifyGroupCommand {
  title: string;
  body: string;
  priority: string;
  customerFcmToken: Array<Recipient>;
  
  constructor() {
  }
  
  static build(): AccountantNotifyGroupCommandBuilder {
    return new AccountantNotifyGroupCommandBuilder();
  }
}

class AccountantNotifyGroupCommandBuilder {
  private instance: AccountantNotifyGroupCommand;
  
  constructor() {
    this.instance = new AccountantNotifyGroupCommand();
  }
  
  title(value: string): AccountantNotifyGroupCommandBuilder {
    this.instance.title = value;
    return this;
  }
  
  body(value: string): AccountantNotifyGroupCommandBuilder {
    this.instance.body = value;
    return this;
  }
  
  priority(value: string): AccountantNotifyGroupCommandBuilder {
    this.instance.priority = value;
    return this;
  }
  
  customerFcmToken(value: Array<Recipient>): AccountantNotifyGroupCommandBuilder {
    this.instance.customerFcmToken = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDeleteNotificationsCommand {
  messageId: string;
  
  constructor() {
  }
  
  static build(): AccountantDeleteNotificationsCommandBuilder {
    return new AccountantDeleteNotificationsCommandBuilder();
  }
}

class AccountantDeleteNotificationsCommandBuilder {
  private instance: AccountantDeleteNotificationsCommand;
  
  constructor() {
    this.instance = new AccountantDeleteNotificationsCommand();
  }
  
  messageId(value: string): AccountantDeleteNotificationsCommandBuilder {
    this.instance.messageId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class AccountantDeleteNotificationCommand {
  messageId: string;
  customerId: string;
  
  constructor() {
  }
  
  static build(): AccountantDeleteNotificationCommandBuilder {
    return new AccountantDeleteNotificationCommandBuilder();
  }
}

class AccountantDeleteNotificationCommandBuilder {
  private instance: AccountantDeleteNotificationCommand;
  
  constructor() {
    this.instance = new AccountantDeleteNotificationCommand();
  }
  
  messageId(value: string): AccountantDeleteNotificationCommandBuilder {
    this.instance.messageId = value;
    return this;
  }
  
  customerId(value: string): AccountantDeleteNotificationCommandBuilder {
    this.instance.customerId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistAuthenticateCommand {
  authenticationId: string;
  claimistId: string;
  username: string;
  password: string;
  
  constructor() {
  }
  
  static build(): ClaimistAuthenticateCommandBuilder {
    return new ClaimistAuthenticateCommandBuilder();
  }
}

class ClaimistAuthenticateCommandBuilder {
  private instance: ClaimistAuthenticateCommand;
  
  constructor() {
    this.instance = new ClaimistAuthenticateCommand();
  }
  
  authenticationId(value: string): ClaimistAuthenticateCommandBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  claimistId(value: string): ClaimistAuthenticateCommandBuilder {
    this.instance.claimistId = value;
    return this;
  }
  
  username(value: string): ClaimistAuthenticateCommandBuilder {
    this.instance.username = value;
    return this;
  }
  
  password(value: string): ClaimistAuthenticateCommandBuilder {
    this.instance.password = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistUpdateTokenAccessedCommand {
  tokenId: string;
  
  constructor() {
  }
  
  static build(): ClaimistUpdateTokenAccessedCommandBuilder {
    return new ClaimistUpdateTokenAccessedCommandBuilder();
  }
}

class ClaimistUpdateTokenAccessedCommandBuilder {
  private instance: ClaimistUpdateTokenAccessedCommand;
  
  constructor() {
    this.instance = new ClaimistUpdateTokenAccessedCommand();
  }
  
  tokenId(value: string): ClaimistUpdateTokenAccessedCommandBuilder {
    this.instance.tokenId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistGetAccessTokenQuery {
  authenticationId: string;
  
  constructor() {
  }
  
  static build(): ClaimistGetAccessTokenQueryBuilder {
    return new ClaimistGetAccessTokenQueryBuilder();
  }
}

class ClaimistGetAccessTokenQueryBuilder {
  private instance: ClaimistGetAccessTokenQuery;
  
  constructor() {
    this.instance = new ClaimistGetAccessTokenQuery();
  }
  
  authenticationId(value: string): ClaimistGetAccessTokenQueryBuilder {
    this.instance.authenticationId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistGetClaimsQuery {
  status: ClaimStatus;
  transactionId: string;
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): ClaimistGetClaimsQueryBuilder {
    return new ClaimistGetClaimsQueryBuilder();
  }
}

class ClaimistGetClaimsQueryBuilder {
  private instance: ClaimistGetClaimsQuery;
  
  constructor() {
    this.instance = new ClaimistGetClaimsQuery();
  }
  
  status(value: ClaimStatus): ClaimistGetClaimsQueryBuilder {
    this.instance.status = value;
    return this;
  }
  
  transactionId(value: string): ClaimistGetClaimsQueryBuilder {
    this.instance.transactionId = value;
    return this;
  }
  
  page(value: Page): ClaimistGetClaimsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): ClaimistGetClaimsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistApproveClaimCommand {
  claimId: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): ClaimistApproveClaimCommandBuilder {
    return new ClaimistApproveClaimCommandBuilder();
  }
}

class ClaimistApproveClaimCommandBuilder {
  private instance: ClaimistApproveClaimCommand;
  
  constructor() {
    this.instance = new ClaimistApproveClaimCommand();
  }
  
  claimId(value: string): ClaimistApproveClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  comment(value: string): ClaimistApproveClaimCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistRejectClaimCommand {
  claimId: string;
  comment: string;
  
  constructor() {
  }
  
  static build(): ClaimistRejectClaimCommandBuilder {
    return new ClaimistRejectClaimCommandBuilder();
  }
}

class ClaimistRejectClaimCommandBuilder {
  private instance: ClaimistRejectClaimCommand;
  
  constructor() {
    this.instance = new ClaimistRejectClaimCommand();
  }
  
  claimId(value: string): ClaimistRejectClaimCommandBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  comment(value: string): ClaimistRejectClaimCommandBuilder {
    this.instance.comment = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class ClaimistGetClaimDetailsQuery {
  claimId: string;
  
  constructor() {
  }
  
  static build(): ClaimistGetClaimDetailsQueryBuilder {
    return new ClaimistGetClaimDetailsQueryBuilder();
  }
}

class ClaimistGetClaimDetailsQueryBuilder {
  private instance: ClaimistGetClaimDetailsQuery;
  
  constructor() {
    this.instance = new ClaimistGetClaimDetailsQuery();
  }
  
  claimId(value: string): ClaimistGetClaimDetailsQueryBuilder {
    this.instance.claimId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantTestAuthenticateQuery {
  
  constructor() {
  }
  
  static build(): MerchantTestAuthenticateQueryBuilder {
    return new MerchantTestAuthenticateQueryBuilder();
  }
}

class MerchantTestAuthenticateQueryBuilder {
  private instance: MerchantTestAuthenticateQuery;
  
  constructor() {
    this.instance = new MerchantTestAuthenticateQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantCreateMerchantTransactionCommand {
  id: string;
  amount: number;
  notes: string;
  
  constructor() {
  }
  
  static build(): MerchantCreateMerchantTransactionCommandBuilder {
    return new MerchantCreateMerchantTransactionCommandBuilder();
  }
}

class MerchantCreateMerchantTransactionCommandBuilder {
  private instance: MerchantCreateMerchantTransactionCommand;
  
  constructor() {
    this.instance = new MerchantCreateMerchantTransactionCommand();
  }
  
  id(value: string): MerchantCreateMerchantTransactionCommandBuilder {
    this.instance.id = value;
    return this;
  }
  
  amount(value: number): MerchantCreateMerchantTransactionCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  notes(value: string): MerchantCreateMerchantTransactionCommandBuilder {
    this.instance.notes = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantGetInProcessTransactionQuery {
  
  constructor() {
  }
  
  static build(): MerchantGetInProcessTransactionQueryBuilder {
    return new MerchantGetInProcessTransactionQueryBuilder();
  }
}

class MerchantGetInProcessTransactionQueryBuilder {
  private instance: MerchantGetInProcessTransactionQuery;
  
  constructor() {
    this.instance = new MerchantGetInProcessTransactionQuery();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantCancelInProcessTransactionCommand {
  
  constructor() {
  }
  
  static build(): MerchantCancelInProcessTransactionCommandBuilder {
    return new MerchantCancelInProcessTransactionCommandBuilder();
  }
}

class MerchantCancelInProcessTransactionCommandBuilder {
  private instance: MerchantCancelInProcessTransactionCommand;
  
  constructor() {
    this.instance = new MerchantCancelInProcessTransactionCommand();
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class MerchantGetMerchantTransactionsQuery {
  page: Page;
  order: Order;
  
  constructor() {
  }
  
  static build(): MerchantGetMerchantTransactionsQueryBuilder {
    return new MerchantGetMerchantTransactionsQueryBuilder();
  }
}

class MerchantGetMerchantTransactionsQueryBuilder {
  private instance: MerchantGetMerchantTransactionsQuery;
  
  constructor() {
    this.instance = new MerchantGetMerchantTransactionsQuery();
  }
  
  page(value: Page): MerchantGetMerchantTransactionsQueryBuilder {
    this.instance.page = value;
    return this;
  }
  
  order(value: Order): MerchantGetMerchantTransactionsQueryBuilder {
    this.instance.order = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export enum WalletOwnerType {
  CUSTOMER = "CUSTOMER",
  PROVIDER = "PROVIDER",
  CASHBOX = "CASHBOX",
}

export enum WalletWalletType {
  VIRTUAL = "VIRTUAL",
  CARD = "CARD",
  COMMISSION = "COMMISSION",
}

export enum WalletWalletState {
  ACTIVE = "ACTIVE",
  CLOSED = "CLOSED",
}

export enum WalletLogType {
  OPEN = "OPEN",
  CREDIT = "CREDIT",
  DEBIT = "DEBIT",
  RESERVE_CREDIT = "RESERVE_CREDIT",
  RESERVE_DEBIT = "RESERVE_DEBIT",
  RELEASE_CREDIT = "RELEASE_CREDIT",
  RELEASE_DEBIT = "RELEASE_DEBIT",
  CLOSE = "CLOSE",
}

// @dynamic
export class WalletWallet {
  id: string;
  type: WalletWalletType;
  state: WalletWalletState;
  ownerId: string;
  ownerType: WalletOwnerType;
  balance: number;
  reservedDebit: number;
  reservedCredit: number;
  availableToDebit: number;
  availableToCredit: number;
  minimum: number;
  maximum: number;
  accountNo: string;
  cardId: string;
  
  constructor() {
  }
  
  static build(): WalletWalletBuilder {
    return new WalletWalletBuilder();
  }
}

class WalletWalletBuilder {
  private instance: WalletWallet;
  
  constructor() {
    this.instance = new WalletWallet();
  }
  
  id(value: string): WalletWalletBuilder {
    this.instance.id = value;
    return this;
  }
  
  type(value: WalletWalletType): WalletWalletBuilder {
    this.instance.type = value;
    return this;
  }
  
  state(value: WalletWalletState): WalletWalletBuilder {
    this.instance.state = value;
    return this;
  }
  
  ownerId(value: string): WalletWalletBuilder {
    this.instance.ownerId = value;
    return this;
  }
  
  ownerType(value: WalletOwnerType): WalletWalletBuilder {
    this.instance.ownerType = value;
    return this;
  }
  
  balance(value: number): WalletWalletBuilder {
    this.instance.balance = value;
    return this;
  }
  
  reservedDebit(value: number): WalletWalletBuilder {
    this.instance.reservedDebit = value;
    return this;
  }
  
  reservedCredit(value: number): WalletWalletBuilder {
    this.instance.reservedCredit = value;
    return this;
  }
  
  availableToDebit(value: number): WalletWalletBuilder {
    this.instance.availableToDebit = value;
    return this;
  }
  
  availableToCredit(value: number): WalletWalletBuilder {
    this.instance.availableToCredit = value;
    return this;
  }
  
  minimum(value: number): WalletWalletBuilder {
    this.instance.minimum = value;
    return this;
  }
  
  maximum(value: number): WalletWalletBuilder {
    this.instance.maximum = value;
    return this;
  }
  
  accountNo(value: string): WalletWalletBuilder {
    this.instance.accountNo = value;
    return this;
  }
  
  cardId(value: string): WalletWalletBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletWalletLog {
  id: string;
  saga: string;
  walletId: string;
  loggedAt: Date;
  type: WalletLogType;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletWalletLogBuilder {
    return new WalletWalletLogBuilder();
  }
}

class WalletWalletLogBuilder {
  private instance: WalletWalletLog;
  
  constructor() {
    this.instance = new WalletWalletLog();
  }
  
  id(value: string): WalletWalletLogBuilder {
    this.instance.id = value;
    return this;
  }
  
  saga(value: string): WalletWalletLogBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletWalletLogBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  loggedAt(value: Date): WalletWalletLogBuilder {
    this.instance.loggedAt = value;
    return this;
  }
  
  type(value: WalletLogType): WalletWalletLogBuilder {
    this.instance.type = value;
    return this;
  }
  
  amount(value: number): WalletWalletLogBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletWalletLogBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletOpenCommand {
  commandId: string;
  walletId: string;
  walletType: WalletWalletType;
  ownerId: string;
  ownerType: WalletOwnerType;
  minimum: number;
  maximum: number;
  accountNo: string;
  cardId: string;
  
  constructor() {
  }
  
  static build(): WalletOpenCommandBuilder {
    return new WalletOpenCommandBuilder();
  }
}

class WalletOpenCommandBuilder {
  private instance: WalletOpenCommand;
  
  constructor() {
    this.instance = new WalletOpenCommand();
  }
  
  commandId(value: string): WalletOpenCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  walletId(value: string): WalletOpenCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  walletType(value: WalletWalletType): WalletOpenCommandBuilder {
    this.instance.walletType = value;
    return this;
  }
  
  ownerId(value: string): WalletOpenCommandBuilder {
    this.instance.ownerId = value;
    return this;
  }
  
  ownerType(value: WalletOwnerType): WalletOpenCommandBuilder {
    this.instance.ownerType = value;
    return this;
  }
  
  minimum(value: number): WalletOpenCommandBuilder {
    this.instance.minimum = value;
    return this;
  }
  
  maximum(value: number): WalletOpenCommandBuilder {
    this.instance.maximum = value;
    return this;
  }
  
  accountNo(value: string): WalletOpenCommandBuilder {
    this.instance.accountNo = value;
    return this;
  }
  
  cardId(value: string): WalletOpenCommandBuilder {
    this.instance.cardId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletReserveDebitCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletReserveDebitCommandBuilder {
    return new WalletReserveDebitCommandBuilder();
  }
}

class WalletReserveDebitCommandBuilder {
  private instance: WalletReserveDebitCommand;
  
  constructor() {
    this.instance = new WalletReserveDebitCommand();
  }
  
  commandId(value: string): WalletReserveDebitCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletReserveDebitCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletReserveDebitCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletReserveDebitCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletReserveDebitCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletReserveCreditCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletReserveCreditCommandBuilder {
    return new WalletReserveCreditCommandBuilder();
  }
}

class WalletReserveCreditCommandBuilder {
  private instance: WalletReserveCreditCommand;
  
  constructor() {
    this.instance = new WalletReserveCreditCommand();
  }
  
  commandId(value: string): WalletReserveCreditCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletReserveCreditCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletReserveCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletReserveCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletReserveCreditCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletReleaseDebitCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletReleaseDebitCommandBuilder {
    return new WalletReleaseDebitCommandBuilder();
  }
}

class WalletReleaseDebitCommandBuilder {
  private instance: WalletReleaseDebitCommand;
  
  constructor() {
    this.instance = new WalletReleaseDebitCommand();
  }
  
  commandId(value: string): WalletReleaseDebitCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletReleaseDebitCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletReleaseDebitCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletReleaseDebitCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletReleaseDebitCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletReleaseCreditCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletReleaseCreditCommandBuilder {
    return new WalletReleaseCreditCommandBuilder();
  }
}

class WalletReleaseCreditCommandBuilder {
  private instance: WalletReleaseCreditCommand;
  
  constructor() {
    this.instance = new WalletReleaseCreditCommand();
  }
  
  commandId(value: string): WalletReleaseCreditCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletReleaseCreditCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletReleaseCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletReleaseCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletReleaseCreditCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletDebitCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletDebitCommandBuilder {
    return new WalletDebitCommandBuilder();
  }
}

class WalletDebitCommandBuilder {
  private instance: WalletDebitCommand;
  
  constructor() {
    this.instance = new WalletDebitCommand();
  }
  
  commandId(value: string): WalletDebitCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletDebitCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletDebitCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletDebitCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletDebitCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletCreditCommand {
  commandId: string;
  saga: string;
  walletId: string;
  amount: number;
  details: string;
  
  constructor() {
  }
  
  static build(): WalletCreditCommandBuilder {
    return new WalletCreditCommandBuilder();
  }
}

class WalletCreditCommandBuilder {
  private instance: WalletCreditCommand;
  
  constructor() {
    this.instance = new WalletCreditCommand();
  }
  
  commandId(value: string): WalletCreditCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  saga(value: string): WalletCreditCommandBuilder {
    this.instance.saga = value;
    return this;
  }
  
  walletId(value: string): WalletCreditCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  amount(value: number): WalletCreditCommandBuilder {
    this.instance.amount = value;
    return this;
  }
  
  details(value: string): WalletCreditCommandBuilder {
    this.instance.details = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletCloseCommand {
  commandId: string;
  walletId: string;
  
  constructor() {
  }
  
  static build(): WalletCloseCommandBuilder {
    return new WalletCloseCommandBuilder();
  }
}

class WalletCloseCommandBuilder {
  private instance: WalletCloseCommand;
  
  constructor() {
    this.instance = new WalletCloseCommand();
  }
  
  commandId(value: string): WalletCloseCommandBuilder {
    this.instance.commandId = value;
    return this;
  }
  
  walletId(value: string): WalletCloseCommandBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class WalletGetBalanceQuery {
  walletId: string;
  
  constructor() {
  }
  
  static build(): WalletGetBalanceQueryBuilder {
    return new WalletGetBalanceQueryBuilder();
  }
}

class WalletGetBalanceQueryBuilder {
  private instance: WalletGetBalanceQuery;
  
  constructor() {
    this.instance = new WalletGetBalanceQuery();
  }
  
  walletId(value: string): WalletGetBalanceQueryBuilder {
    this.instance.walletId = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

// @dynamic
export class Foo {
  bar: Page;
  baz: Order;
  orders: Array<Order>;
  integers: Array<number>;
  string: string;
  
  constructor() {
  }
  
  static build(): FooBuilder {
    return new FooBuilder();
  }
}

class FooBuilder {
  private instance: Foo;
  
  constructor() {
    this.instance = new Foo();
  }
  
  bar(value: Page): FooBuilder {
    this.instance.bar = value;
    return this;
  }
  
  baz(value: Order): FooBuilder {
    this.instance.baz = value;
    return this;
  }
  
  orders(value: Array<Order>): FooBuilder {
    this.instance.orders = value;
    return this;
  }
  
  integers(value: Array<number>): FooBuilder {
    this.instance.integers = value;
    return this;
  }
  
  string(value: string): FooBuilder {
    this.instance.string = value;
    return this;
  }
  
  get() {
    return this.instance;
  }
}

export class AdminApi extends BaseApi {
  
  authenticate(data: AdminAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/authenticate', data, options);
  }
  
  getAccessToken(data: AdminGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-access-token', data, options);
  }
  
  createAdmin(data: AdminCreateAdminCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-admin', data, options);
  }
  
  createBranch(data: AdminCreateBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-branch', data, options);
  }
  
  updateBranch(data: AdminUpdateBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-branch', data, options);
  }
  
  enableBranch(data: AdminEnableBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-branch', data, options);
  }
  
  disableBranch(data: AdminDisableBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-branch', data, options);
  }
  
  getBranch(data: AdminGetBranchQuery, options?: any): Observable<Branch> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-branch', data, options);
  }
  
  findBranches(data: AdminFindBranchesQuery, options?: any): Observable<Paged<Branch>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/find-branches', data, options);
  }
  
  createCashbox(data: AdminCreateCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-cashbox', data, options);
  }
  
  updateCashbox(data: AdminUpdateCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-cashbox', data, options);
  }
  
  enableCashbox(data: AdminEnableCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-cashbox', data, options);
  }
  
  disableCashbox(data: AdminDisableCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-cashbox', data, options);
  }
  
  getCashbox(data: AdminGetCashboxQuery, options?: any): Observable<Cashbox> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-cashbox', data, options);
  }
  
  getCashboxes(data: AdminGetCashboxesQuery, options?: any): Observable<Paged<Cashbox>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-cashboxes', data, options);
  }
  
  createOperator(data: AdminCreateOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-operator', data, options);
  }
  
  updateOperator(data: AdminUpdateOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-operator', data, options);
  }
  
  enableOperator(data: AdminEnableOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-operator', data, options);
  }
  
  disableOperator(data: AdminDisableOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-operator', data, options);
  }
  
  getOperator(data: AdminGetOperatorQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operator', data, options);
  }
  
  getOperators(data: AdminGetOperatorsQuery, options?: any): Observable<Paged<OperatorInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operators', data, options);
  }
  
  attachOperator(data: AdminAttachOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/attach-operator', data, options);
  }
  
  detachOperator(data: AdminDetachOperatorCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/detach-operator', data, options);
  }
  
  updateProvider(data: AdminUpdateProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-provider', data, options);
  }
  
  enableProvider(data: AdminEnableProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-provider', data, options);
  }
  
  disableProvider(data: AdminDisableProviderCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-provider', data, options);
  }
  
  getProvider(data: AdminGetProviderQuery, options?: any): Observable<Provider> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider', data, options);
  }
  
  getProviders(data: AdminGetProvidersQuery, options?: any): Observable<Paged<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-providers', data, options);
  }
  
  createProviderLabel(data: AdminCreateProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-provider-label', data, options);
  }
  
  updateProviderLabel(data: AdminUpdateProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-provider-label', data, options);
  }
  
  getProviderLabel(data: AdminGetProviderLabelQuery, options?: any): Observable<ProviderLabel> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-label', data, options);
  }
  
  getProviderLabels(data: AdminGetProviderLabelsQuery, options?: any): Observable<Paged<ProviderLabel>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-labels', data, options);
  }
  
  deleteProviderLabel(data: AdminDeleteProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-provider-label', data, options);
  }
  
  setOrderingProviderLabel(data: AdminSetOrderingProviderLabelCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/set-ordering-provider-label', data, options);
  }
  
  saveProviderLabels(data: AdminSaveProviderLabelsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/save-provider-labels', data, options);
  }
  
  getProviderLabelTree(data: AdminGetProviderLabelTreeQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-label-tree', data, options);
  }
  
  saveCommission(data: AdminSaveCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/save-commission', data, options);
  }
  
  deleteCommission(data: AdminDeleteCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-commission', data, options);
  }
  
  getCommission(data: AdminGetCommissionQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission', data, options);
  }
  
  getCommissions(data: AdminGetCommissionsQuery, options?: any): Observable<Paged<Commission>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commissions', data, options);
  }
  
  getOperatorScopes(data: AdminGetOperatorScopesQuery, options?: any): Observable<Array<Scope>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-operator-scopes', data, options);
  }
  
  getScopes(data: AdminGetScopesQuery, options?: any): Observable<Array<Scope>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-scopes', data, options);
  }
  
  resetPassword(data: AdminResetPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/reset-password', data, options);
  }
  
  createCommissionPolicy(data: AdminCreateCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-commission-policy', data, options);
  }
  
  updateCommissionPolicy(data: AdminUpdateCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-commission-policy', data, options);
  }
  
  deleteCommissionPolicy(data: AdminDeleteCommissionPolicyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-commission-policy', data, options);
  }
  
  getCommissionPolicy(data: AdminGetCommissionPolicyQuery, options?: any): Observable<CommissionPolicy> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission-policy', data, options);
  }
  
  getCommissionPolicies(data: AdminGetCommissionPoliciesQuery, options?: any): Observable<Paged<CommissionPolicy>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-commission-policies', data, options);
  }
  
  approveCommission(data: AdminApproveCommissionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/approve-commission', data, options);
  }
  
  getBranchCommission(data: AdminGetBranchCommissionQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-branch-commission', data, options);
  }
  
  createAccountant(data: AdminCreateAccountantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-accountant', data, options);
  }
  
  updateAccountant(data: AdminUpdateAccountantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-accountant', data, options);
  }
  
  disableAccountant(data: AdminDisableAccountantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/disable-accountant', data, options);
  }
  
  enableAccountant(data: AdminEnableAccountantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/enable-accountant', data, options);
  }
  
  accountantResetPassword(data: AdminAccountantResetPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/accountant-reset-password', data, options);
  }
  
  getAccountant(data: AdminGetAccountantQuery, options?: any): Observable<Accountant> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-accountant', data, options);
  }
  
  getAccountants(data: AdminGetAccountantsQuery, options?: any): Observable<Paged<Accountant>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-accountants', data, options);
  }
  
  createMerchant(data: AdminCreateMerchantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-merchant', data, options);
  }
  
  updateMerchant(data: AdminUpdateMerchantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-merchant', data, options);
  }
  
  deleteMerchant(data: AdminDeleteMerchantCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-merchant', data, options);
  }
  
  getMerchant(data: AdminGetMerchantQuery, options?: any): Observable<Merchant> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-merchant', data, options);
  }
  
  getAllMerchants(data: AdminGetAllMerchantsQuery, options?: any): Observable<Paged<Merchant>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-all-merchants', data, options);
  }
  
  getMerchantCashbox(data: AdminGetMerchantCashboxQuery, options?: any): Observable<MerchantCashbox> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-merchant-cashbox', data, options);
  }
  
  getAllMerchantCashboxes(data: AdminGetAllMerchantCashboxesQuery, options?: any): Observable<Paged<MerchantCashbox>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-all-merchant-cashboxes', data, options);
  }
  
  createMerchantCashBox(data: AdminCreateMerchantCashBoxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-merchant-cash-box', data, options);
  }
  
  updateMerchantCashBox(data: AdminUpdateMerchantCashBoxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-merchant-cash-box', data, options);
  }
  
  deleteMerchantCashBox(data: AdminDeleteMerchantCashBoxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-merchant-cash-box', data, options);
  }
  
  updateMerchantBranch(data: AdminUpdateMerchantBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/update-merchant-branch', data, options);
  }
  
  getAllMerchantBranches(data: AdminGetAllMerchantBranchesQuery, options?: any): Observable<Paged<MerchantBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-all-merchant-branches', data, options);
  }
  
  getMerchantBranch(data: AdminGetMerchantBranchQuery, options?: any): Observable<MerchantBranch> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-merchant-branch', data, options);
  }
  
  createMerchantBranch(data: AdminCreateMerchantBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/create-merchant-branch', data, options);
  }
  
  deleteMerchantBranch(data: AdminDeleteMerchantBranchCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/admin/delete-merchant-branch', data, options);
  }
}

export class CustomerApi extends BaseApi {
  
  signUp(data: CustomersSignUpCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/sign-up', data, options);
  }
  
  sendActivationCode(data: CustomersSendActivationCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-activation-code', data, options);
  }
  
  activate(data: CustomersActivateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/activate', data, options);
  }
  
  authenticate(data: CustomersAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/authenticate', data, options);
  }
  
  refresh(data: CustomersRefreshCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/refresh', data, options);
  }
  
  getToken(data: CustomersGetTokenQuery, options?: any): Observable<TokenInfo> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-token', data, options);
  }
  
  forgotPassword(data: CustomersForgotPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/forgot-password', data, options);
  }
  
  confirmCode(data: CustomersConfirmCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/confirm-code', data, options);
  }
  
  resetPassword(data: CustomersResetPasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/reset-password', data, options);
  }
  
  signOut(data: CustomersSignOutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/sign-out', data, options);
  }
  
  setData(data: CustomersSetDataCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-data', data, options);
  }
  
  getData(data: CustomersGetDataQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-data', data, options);
  }
  
  sendCode(data: CustomersSendCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-code', data, options);
  }
  
  confirm(data: CustomersConfirmCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/confirm', data, options);
  }
  
  changeEmail(data: CustomersChangeEmailCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/change-email', data, options);
  }
  
  changePhone(data: CustomersChangePhoneCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/change-phone', data, options);
  }
  
  changePassword(data: CustomersChangePasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/change-password', data, options);
  }
  
  sendEmailCode(data: CustomersSendEmailCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-email-code', data, options);
  }
  
  sendSmsCode(data: CustomersSendSmsCodeCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/send-sms-code', data, options);
  }
  
  setLanguage(data: CustomersSetLanguageCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-language', data, options);
  }
  
  setFcmToken(data: CustomersSetFcmTokenCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-fcm-token', data, options);
  }
  
  setNotificationFlag(data: CustomersSetNotificationFlagCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-notification-flag', data, options);
  }
  
  getProviders(data: CustomersGetProvidersQuery, options?: any): Observable<Array<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-providers', data, options);
  }
  
  getMerchantTransaction(data: CustomersGetMerchantTransactionQuery, options?: any): Observable<MerchantTransaction> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-merchant-transaction', data, options);
  }
  
  getProviderLabels(data: CustomersGetProviderLabelsQuery, options?: any): Observable<Array<ProviderLabel>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-provider-labels', data, options);
  }
  
  getCommission(data: CustomersGetCommissionQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-commission', data, options);
  }
  
  deleteGroup(data: CustomersDeleteGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/delete-group', data, options);
  }
  
  setGroup(data: CustomersSetGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-group', data, options);
  }
  
  changeNotificationState(data: CustomersChangeNotificationStateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/change-notification-state', data, options);
  }
  
  deleteFCMToken(data: CustomersDeleteFCMTokenCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/delete-f-c-m-token', data, options);
  }
  
  getAllNotifications(data: CustomersGetAllNotificationsQuery, options?: any): Observable<Paged<Notification>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-all-notifications', data, options);
  }
  
  deleteNotification(data: CustomersDeleteNotificationCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/delete-notification', data, options);
  }
  
  setFavorites(data: CustomersSetFavoritesCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/set-favorites', data, options);
  }
  
  getProfile(data: CustomersGetProfileQuery, options?: any): Observable<CustomerInfo> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-profile', data, options);
  }
  
  findCustomerByEmail(data: CustomersFindCustomerByEmailQuery, options?: any): Observable<Customer> {
    return this.post(BaseApi.apiUrl + '/upay/customers/find-customer-by-email', data, options);
  }
  
  getFavorites(data: CustomersGetFavoritesQuery, options?: any): Observable<Array<Agreement>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-favorites', data, options);
  }
  
  getCards(data: CustomersGetCardsQuery, options?: any): Observable<Array<CardInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-cards', data, options);
  }
  
  findGroup(data: CustomersFindGroupQuery, options?: any): Observable<Paged<CustomerGroup>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/find-group', data, options);
  }
  
  debitAmount(data: CustomersDebitAmountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/debit-amount', data, options);
  }
  
  getBalance(data: CustomersGetBalanceQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-balance', data, options);
  }
  
  getTransactions(data: CustomersGetTransactionsQuery, options?: any): Observable<Paged<CustomerTransaction>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-transactions', data, options);
  }
  
  getBranches(data: CustomersGetBranchesQuery, options?: any): Observable<Array<BranchInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-branches', data, options);
  }
  
  getBill(data: GetBillQuery, options?: any): Observable<Bill> {
    return this.post(BaseApi.apiUrl + '/upay/get-bill', data, options);
  }
  
  payBill(data: PayBillCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/pay-bill', data, options);
  }
  
  getGasBranches(data: GetGasBranchesQuery, options?: any): Observable<Array<GasBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/get-gas-branches', data, options);
  }
  
  getGazpromBranches(data: GetGazpromBranchesQuery, options?: any): Observable<Array<GasBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/get-gazprom-branches', data, options);
  }
  
  bindCard(data: CardsBindCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cards/bind-card', data, options);
  }
  
  getBindingUrl(data: CardsGetBindingUrlQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/cards/get-binding-url', data, options);
  }
  
  getTopUpUrl(data: CardsGetTopUpUrlQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/cards/get-top-up-url', data, options);
  }
  
  isBound(data: CardsIsBoundQuery, options?: any): Observable<boolean> {
    return this.post(BaseApi.apiUrl + '/upay/cards/is-bound', data, options);
  }
  
  processBinding(data: CardsProcessBindingCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cards/process-binding', data, options);
  }
  
  processPayment(data: CardsProcessPaymentCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cards/process-payment', data, options);
  }
  
  topUp(data: CardsTopUpCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cards/top-up', data, options);
  }
  
  removeCard(data: CustomersRemoveCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/remove-card', data, options);
  }
  
  getBalanceLimit(data: CustomersGetBalanceLimitQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-balance-limit', data, options);
  }
  
  retryHangingTransactions(data: CustomersRetryHangingTransactionsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/retry-hanging-transactions', data, options);
  }
  
  getByNumber(data: CustomersGetByNumberQuery, options?: any): Observable<CustomerPeerInfo> {
    return this.post(BaseApi.apiUrl + '/upay/customers/get-by-number', data, options);
  }
  
  contactUs(data: CustomersContactUsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customers/contact-us', data, options);
  }
}

export class OfficeApi extends BaseApi {
  
  authenticate(data: OperatorAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/authenticate', data, options);
  }
  
  getAccessToken(data: OperatorGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-access-token', data, options);
  }
  
  changePassword(data: OperatorChangePasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/change-password', data, options);
  }
  
  createGroup(data: OperatorCreateGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/create-group', data, options);
  }
  
  updateGroup(data: OperatorUpdateGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/update-group', data, options);
  }
  
  findGroup(data: OperatorFindGroupQuery, options?: any): Observable<Paged<Group>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/find-group', data, options);
  }
  
  deleteGroup(data: OperatorDeleteGroupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/delete-group', data, options);
  }
  
  addContract(data: OperatorAddContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/add-contract', data, options);
  }
  
  removeContract(data: OperatorRemoveContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/remove-contract', data, options);
  }
  
  closeCashbox(data: OperatorCloseCashboxCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/close-cashbox', data, options);
  }
  
  cashIn(data: OperatorCashInCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-in', data, options);
  }
  
  cashOut(data: OperatorCashOutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out', data, options);
  }
  
  createToken(data: OperatorCreateTokenCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/create-token', data, options);
  }
  
  getCashboxBalance(data: OperatorGetCashboxBalanceQuery, options?: any): Observable<CashboxBalance> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-balance', data, options);
  }
  
  getCurrentBalance(data: OperatorGetCurrentBalanceQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-current-balance', data, options);
  }
  
  getProviderDescriptors(data: OperatorGetProviderDescriptorsQuery, options?: any): Observable<Array<ContractDesc>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-provider-descriptors', data, options);
  }
  
  getProfile(data: OperatorGetProfileQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-profile', data, options);
  }
  
  getOperatorById(data: OperatorGetOperatorByIdQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-operator-by-id', data, options);
  }
  
  addToBasket(data: OperatorAddToBasketCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/add-to-basket', data, options);
  }
  
  deleteFromBasket(data: OperatorDeleteFromBasketCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/delete-from-basket', data, options);
  }
  
  getBasketItemsCount(data: OperatorGetBasketItemsCountQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-basket-items-count', data, options);
  }
  
  getBasketItems(data: OperatorGetBasketItemsQuery, options?: any): Observable<Array<Basket>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-basket-items', data, options);
  }
  
  cashboxPay(data: OperatorCashboxPayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-pay', data, options);
  }
  
  cashboxIn(data: OperatorCashboxInCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-in', data, options);
  }
  
  cashboxOut(data: OperatorCashboxOutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cashbox-out', data, options);
  }
  
  getCashboxTransactions(data: OperatorGetCashboxTransactionsQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-transactions', data, options);
  }
  
  getTransactionsByPayment(data: OperatorGetTransactionsByPaymentQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-transactions-by-payment', data, options);
  }
  
  getTransaction(data: OperatorGetTransactionQuery, options?: any): Observable<Transaction> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-transaction', data, options);
  }
  
  getCurrentBranch(data: OperatorGetCurrentBranchQuery, options?: any): Observable<Branch> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-current-branch', data, options);
  }
  
  num2Text(data: OperatorNum2TextQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/num2-text', data, options);
  }
  
  generateCashboxReport(data: OperatorGenerateCashboxReportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/generate-cashbox-report', data, options);
  }
  
  getCashboxLogs(data: OperatorGetCashboxLogsQuery, options?: any): Observable<Paged<CashboxLog>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-logs', data, options);
  }
  
  getOperatorLog(data: OperatorGetOperatorLogQuery, options?: any): Observable<Paged<OperatorLogEntry>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-operator-log', data, options);
  }
  
  getOrderNumber(data: OperatorGetOrderNumberQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-order-number', data, options);
  }
  
  cashOutIdram(data: OperatorCashOutIdramCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out-idram', data, options);
  }
  
  cashOutLoanGoodCredit(data: OperatorCashOutLoanGoodCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out-loan-good-credit', data, options);
  }
  
  cashOutLoanVarksAm(data: OperatorCashOutLoanVarksAmCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out-loan-varks-am', data, options);
  }
  
  cashOutLoanGlobalCredit(data: OperatorCashOutLoanGlobalCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out-loan-global-credit', data, options);
  }
  
  cashOutUcomPhoneByBackService(data: OperatorCashOutUcomPhoneByBackServiceCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/cash-out-ucom-phone-by-back-service', data, options);
  }
  
  getCustomers(data: OperatorGetCustomersQuery, options?: any): Observable<Paged<CustomerInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-customers', data, options);
  }
  
  updateCustomerInfo(data: OperatorUpdateCustomerInfoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/update-customer-info', data, options);
  }
  
  getCashboxInfo(data: OperatorGetCashboxInfoQuery, options?: any): Observable<CashboxInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cashbox-info', data, options);
  }
  
  armsoftImport(data: OperatorArmsoftImportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/armsoft-import', data, options);
  }
  
  reportUcomMobileBranchDaily(data: OperatorReportUcomMobileBranchDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-mobile-branch-daily', data, options);
  }
  
  reportUcomMobileWalletDaily(data: OperatorReportUcomMobileWalletDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-mobile-wallet-daily', data, options);
  }
  
  reportUcomMobileMonthly(data: OperatorReportUcomMobileMonthlyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-mobile-monthly', data, options);
  }
  
  reportUcomFixedBranchDaily(data: OperatorReportUcomFixedBranchDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-fixed-branch-daily', data, options);
  }
  
  reportUcomFixedWalletDaily(data: OperatorReportUcomFixedWalletDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-fixed-wallet-daily', data, options);
  }
  
  reportUcomFixedMonthly(data: OperatorReportUcomFixedMonthlyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/report-ucom-fixed-monthly', data, options);
  }
  
  getProvider(data: AdminGetProviderQuery, options?: any): Observable<Provider> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider', data, options);
  }
  
  getProviders(data: AdminGetProvidersQuery, options?: any): Observable<Paged<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-providers', data, options);
  }
  
  getProviderLabel(data: AdminGetProviderLabelQuery, options?: any): Observable<ProviderLabel> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-label', data, options);
  }
  
  getProviderLabels(data: AdminGetProviderLabelsQuery, options?: any): Observable<Paged<ProviderLabel>> {
    return this.post(BaseApi.apiUrl + '/upay/admin/get-provider-labels', data, options);
  }
  
  getProviderLabelTree(data: OperatorGetProviderLabelTreeQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-provider-label-tree', data, options);
  }
  
  getProviderCommissions(data: OperatorGetProviderCommissionsQuery, options?: any): Observable<Commission> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-provider-commissions', data, options);
  }
  
  getCommissionsForBranch(data: OperatorGetCommissionsForBranchQuery, options?: any): Observable<Array<ProviderCommission>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-commissions-for-branch', data, options);
  }
  
  isCancellationClaimAcceptable(data: OperatorIsCancellationClaimAcceptableQuery, options?: any): Observable<boolean> {
    return this.post(BaseApi.apiUrl + '/upay/operator/is-cancellation-claim-acceptable', data, options);
  }
  
  openCancellationClaim(data: OperatorOpenCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/open-cancellation-claim', data, options);
  }
  
  openCustomerCancellationClaim(data: OperatorOpenCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/open-customer-cancellation-claim', data, options);
  }
  
  completeCancellationClaim(data: OperatorCompleteCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/complete-cancellation-claim', data, options);
  }
  
  getClaimHistory(data: AccountantGetClaimHistoryQuery, options?: any): Observable<Array<ClaimHistory>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-claim-history', data, options);
  }
  
  getCustomerClaimHistory(data: AccountantGetCustomerClaimHistoryQuery, options?: any): Observable<Array<CustomerClaimHistory>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-customer-claim-history', data, options);
  }
  
  getClaimInfo(data: AccountantGetClaimInfoQuery, options?: any): Observable<Claim> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-claim-info', data, options);
  }
  
  getFilteredClaims(data: AccountantGetFilteredClaimsQuery, options?: any): Observable<Paged<Claim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-filtered-claims', data, options);
  }
  
  getNumberOfClaimsToComplete(data: OperatorGetNumberOfClaimsToCompleteQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-number-of-claims-to-complete', data, options);
  }
  
  openUnfreezeAccountClaim(data: AccountantOpenUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/open-unfreeze-account-claim', data, options);
  }
  
  searchUnfreezeAccountClaims(data: AccountantSearchUnfreezeAccountClaimsQuery, options?: any): Observable<Paged<UnfreezeAccountClaim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/search-unfreeze-account-claims', data, options);
  }
  
  getUnfreezeAccountClaim(data: AccountantGetUnfreezeAccountClaimQuery, options?: any): Observable<UnfreezeAccountClaim> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-unfreeze-account-claim', data, options);
  }
  
  setIdentificationData(data: OperatorSetIdentificationDataCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/set-identification-data', data, options);
  }
  
  getIdentificationData(data: OperatorGetIdentificationDataQuery, options?: any): Observable<Array<Document>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-identification-data', data, options);
  }
  
  getArchivedDocuments(data: OperatorGetArchivedDocumentsQuery, options?: any): Observable<Array<Document>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-archived-documents', data, options);
  }
  
  getCustomer(data: OperatorGetCustomerQuery, options?: any): Observable<CustomerInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-customer', data, options);
  }
  
  getTransactionByReceiptId(data: OperatorGetTransactionByReceiptIdQuery, options?: any): Observable<Transaction> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-transaction-by-receipt-id', data, options);
  }
  
  getCustomerTransactionByReceiptId(data: OperatorGetCustomerTransactionByReceiptIdQuery, options?: any): Observable<CustomerTransaction> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-customer-transaction-by-receipt-id', data, options);
  }
  
  getActivityLogByCustomerId(data: OperatorGetActivityLogByCustomerIdQuery, options?: any): Observable<Paged<ActivityLog>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-activity-log-by-customer-id', data, options);
  }
  
  getCustomerCards(data: OperatorGetCustomerCardsQuery, options?: any): Observable<Array<CardInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-customer-cards', data, options);
  }
  
  getCountries(data: OperatorGetCountriesQuery, options?: any): Observable<Array<Country>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-countries', data, options);
  }
  
  getCities(data: OperatorGetCitiesQuery, options?: any): Observable<Array<City>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-cities', data, options);
  }
  
  terminateCustomer(data: OperatorTerminateCustomerCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/operator/terminate-customer', data, options);
  }
  
  getCustomerTransactions(data: OperatorGetCustomerTransactionsQuery, options?: any): Observable<Paged<CustomerTransaction>> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-customer-transactions', data, options);
  }
  
  getBranches(data: AccountantGetBranchesQuery, options?: any): Observable<Array<BranchName>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-branches', data, options);
  }
  
  getCashboxes(data: AccountantGetCashboxesQuery, options?: any): Observable<Array<CashboxName>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-cashboxes', data, options);
  }
  
  freezeAccount(data: AccountantFreezeAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/freeze-account', data, options);
  }
  
  unfreezeAccount(data: AccountantUnfreezeAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/unfreeze-account', data, options);
  }
  
  customerCashout(data: CustomerCashoutCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/customer-cashout', data, options);
  }
  
  evocaGetLoanDebt(data: EvocaGetLoanDebtQuery, options?: any): Observable<LoanInfo> {
    return this.post(BaseApi.apiUrl + '/upay/evoca-get-loan-debt', data, options);
  }
  
  searchUcomFixedByPhone(data: SearchUcomFixedByPhoneQuery, options?: any): Observable<Array<UcomFixedCustomer>> {
    return this.post(BaseApi.apiUrl + '/upay/search-ucom-fixed-by-phone', data, options);
  }
  
  getAmeriaContracts(data: GetAmeriaContractsQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-ameria-contracts', data, options);
  }
  
  getAmeriaAccount(data: GetAmeriaAccountQuery, options?: any): Observable<ArmSoftBankCheckAccount> {
    return this.post(BaseApi.apiUrl + '/upay/get-ameria-account', data, options);
  }
  
  getFincaContractRegular(data: GetFincaContractRegularQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-finca-contract-regular', data, options);
  }
  
  getFincaContractPrepay(data: GetFincaContractPrepayQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-finca-contract-prepay', data, options);
  }
  
  getNormanCreditContractRegular(data: GetNormanCreditContractRegularQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-norman-credit-contract-regular', data, options);
  }
  
  getNormanCreditContractPrepay(data: GetNormanCreditContractPrepayQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-norman-credit-contract-prepay', data, options);
  }
  
  checkCard(data: CheckCardQuery, options?: any): Observable<DebtCard> {
    return this.post(BaseApi.apiUrl + '/upay/check-card', data, options);
  }
  
  getArdshinLoanClientData(data: GetArdshinLoanClientDataQuery, options?: any): Observable<ArdshinProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-ardshin-loan-client-data', data, options);
  }
  
  getArdshinDepositClientData(data: GetArdshinDepositClientDataQuery, options?: any): Observable<ArdshinProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-ardshin-deposit-client-data', data, options);
  }
  
  getArdshinCardClientData(data: GetArdshinCardClientDataQuery, options?: any): Observable<ArdshinProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-ardshin-card-client-data', data, options);
  }
  
  getArdshinAccountClientData(data: GetArdshinAccountClientDataQuery, options?: any): Observable<ArdshinProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-ardshin-account-client-data', data, options);
  }
  
  getArdshinLegalClientData(data: GetArdshinLegalClientDataQuery, options?: any): Observable<ArdshinLegalProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-ardshin-legal-client-data', data, options);
  }
  
  getIdBankLoanClientData(data: GetIdBankLoanClientDataQuery, options?: any): Observable<IdBankProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-id-bank-loan-client-data', data, options);
  }
  
  getIdBankCardClientData(data: GetIdBankCardClientDataQuery, options?: any): Observable<IdBankProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-id-bank-card-client-data', data, options);
  }
  
  getIdBankAccountClientData(data: GetIdBankAccountClientDataQuery, options?: any): Observable<IdBankProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-id-bank-account-client-data', data, options);
  }
  
  getEvocaContractRegular(data: GetEvocaContractRegularQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-evoca-contract-regular', data, options);
  }
  
  getEvocaContractPrepay(data: GetEvocaContractPrepayQuery, options?: any): Observable<ArmSoftBankContractDebts> {
    return this.post(BaseApi.apiUrl + '/upay/get-evoca-contract-prepay', data, options);
  }
  
  getEvocaAccount(data: GetEvocaAccountQuery, options?: any): Observable<ArmSoftBankCheckAccount> {
    return this.post(BaseApi.apiUrl + '/upay/get-evoca-account', data, options);
  }
  
  getAcbaAccountProduct(data: GetAcbaAccountProductQuery, options?: any): Observable<AcbaProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-acba-account-product', data, options);
  }
  
  getAcbaCardProduct(data: GetAcbaCardProductQuery, options?: any): Observable<AcbaProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-acba-card-product', data, options);
  }
  
  getAcbaLoanProduct(data: GetAcbaLoanProductQuery, options?: any): Observable<AcbaProduct> {
    return this.post(BaseApi.apiUrl + '/upay/get-acba-loan-product', data, options);
  }
  
  checkAcbaPaymentStatus(data: CheckAcbaPaymentStatusQuery, options?: any): Observable<AcbaPaymentStatus> {
    return this.post(BaseApi.apiUrl + '/upay/check-acba-payment-status', data, options);
  }
  
  getGasBranches(data: GetGasBranchesQuery, options?: any): Observable<Array<GasBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/get-gas-branches', data, options);
  }
  
  getGazpromBranches(data: GetGazpromBranchesQuery, options?: any): Observable<Array<GasBranch>> {
    return this.post(BaseApi.apiUrl + '/upay/get-gazprom-branches', data, options);
  }
  
  hiLineBill(data: HiLineBillQuery, options?: any): Observable<BeelineBill> {
    return this.post(BaseApi.apiUrl + '/upay/hi-line-bill', data, options);
  }
  
  vivacellObtainBill(data: VivacellObtainBillQuery, options?: any): Observable<MobileDebt> {
    return this.post(BaseApi.apiUrl + '/upay/vivacell-obtain-bill', data, options);
  }
  
  beelineObtainBill(data: BeelineObtainBillQuery, options?: any): Observable<MobileDebt> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-obtain-bill', data, options);
  }
  
  beelineTelephoneObtainBill(data: BeelineTelephoneObtainBillQuery, options?: any): Observable<BeelineBill> {
    return this.post(BaseApi.apiUrl + '/upay/beeline-telephone-obtain-bill', data, options);
  }
  
  getDebtUcomMobile(data: GetDebtUcomMobileQuery, options?: any): Observable<DebtUcomMobile> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ucom-mobile', data, options);
  }
  
  getDebtUcomCorporate(data: GetDebtUcomCorporateQuery, options?: any): Observable<DebtUcomMobile> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ucom-corporate', data, options);
  }
  
  getDebtUcomFixed(data: GetDebtUcomFixedQuery, options?: any): Observable<UpayFixedDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ucom-fixed', data, options);
  }
  
  getDebtElectricity(data: GetDebtElectricityQuery, options?: any): Observable<DebtElectricity> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-electricity', data, options);
  }
  
  getDebtArtsakhElectricity(data: GetDebtArtsakhElectricityQuery, options?: any): Observable<DebtElectricity> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-artsakh-electricity', data, options);
  }
  
  getMultiDebtElectricity(data: GetMultiDebtElectricityQuery, options?: any): Observable<Array<DebtElectricity>> {
    return this.post(BaseApi.apiUrl + '/upay/get-multi-debt-electricity', data, options);
  }
  
  getDebtGas(data: GetDebtGasQuery, options?: any): Observable<GasDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-gas', data, options);
  }
  
  getDebtGazpromArmenia(data: GetDebtGazpromArmeniaQuery, options?: any): Observable<Array<GasDebt>> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-gazprom-armenia', data, options);
  }
  
  getDebtArtsakhGas(data: GetDebtArtsakhGasQuery, options?: any): Observable<GasDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-artsakh-gas', data, options);
  }
  
  getDebtRostelecom(data: GetDebtRostelecomQuery, options?: any): Observable<DebtRostelecom> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-rostelecom', data, options);
  }
  
  getDebtInteractive(data: GetDebtInteractiveQuery, options?: any): Observable<DebtInteractive> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-interactive', data, options);
  }
  
  getDebtArpinet(data: GetDebtArpinetQuery, options?: any): Observable<DebtArpinet> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-arpinet', data, options);
  }
  
  getDebtYournet(data: GetDebtYournetQuery, options?: any): Observable<DebtYournet> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-yournet', data, options);
  }
  
  getDebtCtv(data: GetDebtCtvQuery, options?: any): Observable<DebtCtv> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ctv', data, options);
  }
  
  getDebtWater(data: GetDebtWaterQuery, options?: any): Observable<DebtWater> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-water', data, options);
  }
  
  getDebtKarabakhTelekom(data: GetDebtKarabakhTelekomQuery, options?: any): Observable<MobileDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-karabakh-telekom', data, options);
  }
  
  getDebtGlobalCredit(data: GetDebtGlobalCreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-global-credit', data, options);
  }
  
  getDebtAregak(data: GetDebtAregakQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-aregak', data, options);
  }
  
  getDebtNormanCredit(data: GetDebtNormanCreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-norman-credit', data, options);
  }
  
  getDebtCardAgrocredit(data: GetDebtCardAgrocreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-card-agrocredit', data, options);
  }
  
  getDebtKamurj(data: GetDebtKamurjQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-kamurj', data, options);
  }
  
  getDebtGoodCredit(data: GetDebtGoodCreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-good-credit', data, options);
  }
  
  getDebtFinca(data: GetDebtFincaQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-finca', data, options);
  }
  
  getDebtFastCredit(data: GetDebtFastCreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-fast-credit', data, options);
  }
  
  getDebtFarmCredit(data: GetDebtFarmCreditQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-farm-credit', data, options);
  }
  
  getDebtBless(data: GetDebtBlessQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-bless', data, options);
  }
  
  getDebtSefInternational(data: GetDebtSefInternationalQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-sef-international', data, options);
  }
  
  getDebtVarksAm(data: GetDebtVarksAmQuery, options?: any): Observable<DebtLoan> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-varks-am', data, options);
  }
  
  getDebtVivaroBet(data: GetDebtVivaroBetQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-vivaro-bet', data, options);
  }
  
  getDebtVivaroCasino(data: GetDebtVivaroCasinoQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-vivaro-casino', data, options);
  }
  
  getDebtTotoSport(data: GetDebtTotoSportQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-toto-sport', data, options);
  }
  
  getDebtTotoCasino(data: GetDebtTotoCasinoQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-toto-casino', data, options);
  }
  
  getDebtEurofootball(data: GetDebtEurofootballQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-eurofootball', data, options);
  }
  
  getDebtAdjarabet(data: GetDebtAdjarabetQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-adjarabet', data, options);
  }
  
  getDebtGoodwin(data: GetDebtGoodwinQuery, options?: any): Observable<GoodwinResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-goodwin', data, options);
  }
  
  getDebtSocialOk(data: GetDebtSocialOkQuery, options?: any): Observable<DebtSocialOk> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-social-ok', data, options);
  }
  
  getDebtGamesTankiOnline(data: GetDebtGamesTankiOnlineQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-tanki-online', data, options);
  }
  
  getDebtGamesWorldOfTanks(data: GetDebtGamesWorldOfTanksQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-tanks', data, options);
  }
  
  getDebtGamesWorldOfWarPlains(data: GetDebtGamesWorldOfWarPlainsQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-war-plains', data, options);
  }
  
  getDebtGamesWorldOfWarShips(data: GetDebtGamesWorldOfWarShipsQuery, options?: any): Observable<DebtOnlineGames> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-games-world-of-war-ships', data, options);
  }
  
  getDebtMaryKay(data: GetDebtMaryKayQuery, options?: any): Observable<DebtMaryKay> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-mary-kay', data, options);
  }
  
  getDebtOriflame(data: GetDebtOriflameQuery, options?: any): Observable<DebtOriflame> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-oriflame', data, options);
  }
  
  getDebtFaberlic(data: GetDebtFaberlicQuery, options?: any): Observable<DebtFaberlic> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-faberlic', data, options);
  }
  
  getDebtNtvPlus(data: GetDebtNtvPlusQuery, options?: any): Observable<DebtNtvPlus> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ntv-plus', data, options);
  }
  
  getDebtEkeng(data: GetDebtEkengQuery, options?: any): Observable<DebtEkeng> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-ekeng', data, options);
  }
  
  getDebtPolice(data: GetDebtPoliceQuery, options?: any): Observable<PoliceDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-police', data, options);
  }
  
  getDebtPoliceman(data: GetDebtPolicemanQuery, options?: any): Observable<PoliceDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-policeman', data, options);
  }
  
  getDebtParking(data: GetDebtParkingQuery, options?: any): Observable<ParkingFineDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-parking', data, options);
  }
  
  getDebtParkingFine(data: GetDebtParkingFineQuery, options?: any): Observable<ParkingFineDebt> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-parking-fine', data, options);
  }
  
  getDebtTest(data: GetDebtTestQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-test', data, options);
  }
  
  getDebtEfuGames(data: GetDebtEfuGamesQuery, options?: any): Observable<BetResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-efu-games', data, options);
  }
  
  getDebtIdramWallet(data: GetDebtIdramWalletQuery, options?: any): Observable<IdramWalletResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-idram-wallet', data, options);
  }
  
  getDebtUpayIdramWallet(data: GetDebtUpayIdramWalletQuery, options?: any): Observable<IdramWalletResponse> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-upay-idram-wallet', data, options);
  }
  
  getDebtStroyMasterDamafon(data: GetDebtStroyMasterDamafonQuery, options?: any): Observable<DebtStroyMasterDamafon> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-stroy-master-damafon', data, options);
  }
  
  getDebtUpayCustomer(data: GetDebtUpayCustomerQuery, options?: any): Observable<CustomerInfo> {
    return this.post(BaseApi.apiUrl + '/upay/get-debt-upay-customer', data, options);
  }
  
  cashPayVivaCell(data: CashPayVivaCellCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-viva-cell', data, options);
  }
  
  cashPayUcomMobile(data: CashPayUcomMobileCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ucom-mobile', data, options);
  }
  
  cashPayUcomCorporate(data: CashPayUcomCorporateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ucom-corporate', data, options);
  }
  
  cashPayUcomFixed(data: CashPayUcomFixedCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ucom-fixed', data, options);
  }
  
  cashPayElectricity(data: CashPayElectricityCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-electricity', data, options);
  }
  
  cashPayArtsakhElectricity(data: CashPayArtsakhElectricityCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-artsakh-electricity', data, options);
  }
  
  cashPayGas(data: CashPayGasCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-gas', data, options);
  }
  
  cashPayGasService(data: CashPayGasServiceCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-gas-service', data, options);
  }
  
  cashPayGazpromArmenia(data: CashPayGazpromArmeniaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-gazprom-armenia', data, options);
  }
  
  cashPayGazpromArmeniaService(data: CashPayGazpromArmeniaServiceCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-gazprom-armenia-service', data, options);
  }
  
  cashPayArtsakhGas(data: CashPayArtsakhGasCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-artsakh-gas', data, options);
  }
  
  cashPayRostelecom(data: CashPayRostelecomCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-rostelecom', data, options);
  }
  
  cashPayInteractive(data: CashPayInteractiveCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-interactive', data, options);
  }
  
  cashPayArpinet(data: CashPayArpinetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-arpinet', data, options);
  }
  
  cashPayYourNet(data: CashPayYourNetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-your-net', data, options);
  }
  
  cashPayCtv(data: CashPayCtvCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ctv', data, options);
  }
  
  cashPayWater(data: CashPayWaterCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-water', data, options);
  }
  
  cashPayBeeline(data: CashPayBeelineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-beeline', data, options);
  }
  
  cashPayBeelineTelephone(data: CashPayBeelineTelephoneCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-beeline-telephone', data, options);
  }
  
  cashPayHiLine(data: CashPayHiLineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-hi-line', data, options);
  }
  
  cashPayKarabakhTelecom(data: CashPayKarabakhTelecomCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-karabakh-telecom', data, options);
  }
  
  cashPayEvoca(data: CashPayEvocaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca', data, options);
  }
  
  cashPayGlobalCredit(data: CashPayGlobalCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-global-credit', data, options);
  }
  
  cashPayAregak(data: CashPayAregakCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-aregak', data, options);
  }
  
  cashPayNormanCredit(data: CashPayNormanCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-norman-credit', data, options);
  }
  
  cashPayCardAgrocredit(data: CashPayCardAgrocreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-card-agrocredit', data, options);
  }
  
  cashPayKamurj(data: CashPayKamurjCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-kamurj', data, options);
  }
  
  cashPayGoodCredit(data: CashPayGoodCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-good-credit', data, options);
  }
  
  cashPayFinca(data: CashPayFincaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-finca', data, options);
  }
  
  cashPayFastCredit(data: CashPayFastCreditCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-fast-credit', data, options);
  }
  
  cashPayFarmCreditArmenia(data: CashPayFarmCreditArmeniaCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-farm-credit-armenia', data, options);
  }
  
  cashPayBless(data: CashPayBlessCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-bless', data, options);
  }
  
  cashPaySefInternational(data: CashPaySefInternationalCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-sef-international', data, options);
  }
  
  cashPayVarksAm(data: CashPayVarksAmCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-varks-am', data, options);
  }
  
  cashPayVivarobet(data: CashPayVivarobetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-vivarobet', data, options);
  }
  
  cashPayVivarocasino(data: CashPayVivarocasinoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-vivarocasino', data, options);
  }
  
  cashPayTotoGamingSport(data: CashPayTotoGamingSportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-toto-gaming-sport', data, options);
  }
  
  cashPayTotoGamingCasino(data: CashPayTotoGamingCasinoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-toto-gaming-casino', data, options);
  }
  
  cashPayEurofootball(data: CashPayEurofootballCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-eurofootball', data, options);
  }
  
  cashPayAdjarabet(data: CashPayAdjarabetCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-adjarabet', data, options);
  }
  
  cashPayGoodWin(data: CashPayGoodWinCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-good-win', data, options);
  }
  
  cashPaySocialOk(data: CashPaySocialOkCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-social-ok', data, options);
  }
  
  cashPayTankiOnline(data: CashPayTankiOnlineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-tanki-online', data, options);
  }
  
  cashPayWorldOfTanks(data: CashPayWorldOfTanksCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-world-of-tanks', data, options);
  }
  
  cashPayWorldOfWarPlains(data: CashPayWorldOfWarPlainsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-world-of-war-plains', data, options);
  }
  
  cashPayWorldOfWarShips(data: CashPayWorldOfWarShipsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-world-of-war-ships', data, options);
  }
  
  cashPayMaryKay(data: CashPayMaryKayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-mary-kay', data, options);
  }
  
  cashPayOriflame(data: CashPayOriflameCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-oriflame', data, options);
  }
  
  cashPayFaberlic(data: CashPayFaberlicCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-faberlic', data, options);
  }
  
  cashPayNtvPlus(data: CashPayNtvPlusCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ntv-plus', data, options);
  }
  
  cashPayEkeng(data: CashPayEkengCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ekeng', data, options);
  }
  
  cashPayPolice(data: CashPayPoliceCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-police', data, options);
  }
  
  cashPayPoliceman(data: CashPayPolicemanCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-policeman', data, options);
  }
  
  cashPayParking(data: CashPayParkingCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-parking', data, options);
  }
  
  cashPayParkingFine(data: CashPayParkingFineCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-parking-fine', data, options);
  }
  
  cashPayTest(data: CashPayTestCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-test', data, options);
  }
  
  cashPayEfuGames(data: CashPayEfuGamesCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-efu-games', data, options);
  }
  
  cashPayIdramWallet(data: CashPayIdramWalletCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-idram-wallet', data, options);
  }
  
  cashPayUpayIdramWallet(data: CashPayUpayIdramWalletCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-upay-idram-wallet', data, options);
  }
  
  cashPayFincaContractRegular(data: CashPayFincaContractRegularCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-finca-contract-regular', data, options);
  }
  
  cashPayFincaContractPrepay(data: CashPayFincaContractPrepayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-finca-contract-prepay', data, options);
  }
  
  cashPayNormanCreditContractRegular(data: CashPayNormanCreditContractRegularCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-norman-credit-contract-regular', data, options);
  }
  
  cashPayNormanCreditContractPrepay(data: CashPayNormanCreditContractPrepayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-norman-credit-contract-prepay', data, options);
  }
  
  cashPayAmeriaContract(data: CashPayAmeriaContractCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ameria-contract', data, options);
  }
  
  cashPayAmeriaAccount(data: CashPayAmeriaAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ameria-account', data, options);
  }
  
  cashPayAmeriaAccountLegal(data: CashPayAmeriaAccountLegalCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ameria-account-legal', data, options);
  }
  
  cashPayTransferToCard(data: CashPayTransferToCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-transfer-to-card', data, options);
  }
  
  cashPayStroyMasterDamafon(data: CashPayStroyMasterDamafonCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-stroy-master-damafon', data, options);
  }
  
  cashPayArdshinLoan(data: CashPayArdshinLoanCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ardshin-loan', data, options);
  }
  
  cashPayArdshinDeposit(data: CashPayArdshinDepositCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ardshin-deposit', data, options);
  }
  
  cashPayArdshinCard(data: CashPayArdshinCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ardshin-card', data, options);
  }
  
  cashPayArdshinAccount(data: CashPayArdshinAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ardshin-account', data, options);
  }
  
  cashPayArdshinLegal(data: CashPayArdshinLegalCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-ardshin-legal', data, options);
  }
  
  cashPayUpayCustomer(data: CashPayUpayCustomerCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-upay-customer', data, options);
  }
  
  cashPayIdBankLoan(data: CashPayIdBankLoanCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-id-bank-loan', data, options);
  }
  
  cashPayIdBankCard(data: CashPayIdBankCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-id-bank-card', data, options);
  }
  
  cashPayIdBankAccount(data: CashPayIdBankAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-id-bank-account', data, options);
  }
  
  cashPayEvocaContractRegular(data: CashPayEvocaContractRegularCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca-contract-regular', data, options);
  }
  
  cashPayEvocaContractPrepay(data: CashPayEvocaContractPrepayCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca-contract-prepay', data, options);
  }
  
  cashPayEvocaAccount(data: CashPayEvocaAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca-account', data, options);
  }
  
  cashPayEvocaLegalAccount(data: CashPayEvocaLegalAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-evoca-legal-account', data, options);
  }
  
  cashPayAcbaAccount(data: CashPayAcbaAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-acba-account', data, options);
  }
  
  cashPayAcbaCard(data: CashPayAcbaCardCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-acba-card', data, options);
  }
  
  cashPayAcbaLoan(data: CashPayAcbaLoanCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/cash-pay-acba-loan', data, options);
  }
}

export class AccountantApi extends BaseApi {
  
  authenticate(data: AccountantAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/authenticate', data, options);
  }
  
  getAccessToken(data: AccountantGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-access-token', data, options);
  }
  
  getProfile(data: AccountantGetProfileQuery, options?: any): Observable<AccountantInfo> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-profile', data, options);
  }
  
  changePassword(data: AccountantChangePasswordCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/change-password', data, options);
  }
  
  getCashboxReport(data: AccountantGetCashboxReportQuery, options?: any): Observable<CashboxReport> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-cashbox-report', data, options);
  }
  
  getCashboxReports(data: AccountantGetCashboxReportsQuery, options?: any): Observable<Paged<CashboxReport>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-cashbox-reports', data, options);
  }
  
  getCashboxReportsSummary(data: AccountantGetCashboxReportsSummaryQuery, options?: any): Observable<CashboxReportSummary> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-cashbox-reports-summary', data, options);
  }
  
  getProviders(data: AccountantGetProvidersQuery, options?: any): Observable<Array<Provider>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-providers', data, options);
  }
  
  getServiceNames(data: AccountantGetServiceNamesQuery, options?: any): Observable<Array<string>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-service-names', data, options);
  }
  
  getTransactions(data: AccountantGetTransactionsQuery, options?: any): Observable<Paged<OperatorLogEntry>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-transactions', data, options);
  }
  
  getTransactionsSummary(data: AccountantGetTransactionsSummaryQuery, options?: any): Observable<TransactionReportSummary> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-transactions-summary', data, options);
  }
  
  getClaims(data: ClaimistGetClaimsQuery, options?: any): Observable<Paged<Claim>> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/get-claims', data, options);
  }
  
  approveClaim(data: AccountantApproveClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/approve-claim', data, options);
  }
  
  rejectClaim(data: AccountantRejectClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/reject-claim', data, options);
  }
  
  getClaimInfo(data: AccountantGetClaimInfoQuery, options?: any): Observable<Claim> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-claim-info', data, options);
  }
  
  getFilteredClaims(data: AccountantGetFilteredClaimsQuery, options?: any): Observable<Paged<Claim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-filtered-claims', data, options);
  }
  
  getClaimHistory(data: AccountantGetClaimHistoryQuery, options?: any): Observable<Array<ClaimHistory>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-claim-history', data, options);
  }
  
  getCustomerClaimHistory(data: AccountantGetCustomerClaimHistoryQuery, options?: any): Observable<Array<CustomerClaimHistory>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-customer-claim-history', data, options);
  }
  
  getCustomers(data: AccountantGetCustomersQuery, options?: any): Observable<Paged<CustomerInfo>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-customers', data, options);
  }
  
  sendSingleNotification(data: AccountantSendSingleNotificationCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/send-single-notification', data, options);
  }
  
  sendGroupNotification(data: AccountantSendGroupNotificationCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/send-group-notification', data, options);
  }
  
  deleteNotifications(data: AccountantDeleteNotificationsCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/delete-notifications', data, options);
  }
  
  deleteNotification(data: AccountantDeleteNotificationCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/delete-notification', data, options);
  }
  
  reportPhoneBuyBackServiceDaily(data: AccountantReportPhoneBuyBackServiceDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-phone-buy-back-service-daily', data, options);
  }
  
  reportPhoneBuyBackServiceMonthly(data: AccountantReportPhoneBuyBackServiceMonthlyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-phone-buy-back-service-monthly', data, options);
  }
  
  reportAmeriaLoanDaily(data: AccountantReportAmeriaLoanDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-ameria-loan-daily', data, options);
  }
  
  reportAmeriaAccountDaily(data: AccountantReportAmeriaAccountDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-ameria-account-daily', data, options);
  }
  
  reportArdshinDaily(data: AccountantReportArdshinDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-ardshin-daily', data, options);
  }
  
  reportFincaRegularDaily(data: AccountantReportFincaRegularDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-finca-regular-daily', data, options);
  }
  
  reportFincaPrepayDaily(data: AccountantReportFincaPrepayDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-finca-prepay-daily', data, options);
  }
  
  reportNormanCreditRegularDaily(data: AccountantReportNormanCreditRegularDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-norman-credit-regular-daily', data, options);
  }
  
  reportNormanCreditPrepayDaily(data: AccountantReportNormanCreditPrepayDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-norman-credit-prepay-daily', data, options);
  }
  
  reportIdBankDaily(data: AccountantReportIdBankDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-id-bank-daily', data, options);
  }
  
  reportAcbaBankDaily(data: AccountantReportAcbaBankDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-acba-bank-daily', data, options);
  }
  
  reportGlobalCreditDaily(data: AccountantReportGlobalCreditDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-global-credit-daily', data, options);
  }
  
  reportGoodCreditDaily(data: AccountantReportGoodCreditDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-good-credit-daily', data, options);
  }
  
  reportEvocaDaily(data: AccountantReportEvocaDailyCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-evoca-daily', data, options);
  }
  
  getFailedTransactions(data: AccountantGetFailedTransactionsQuery, options?: any): Observable<Array<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-failed-transactions', data, options);
  }
  
  getFailedBranchTransactions(data: AccountantGetFailedBranchTransactionsQuery, options?: any): Observable<Paged<Transaction>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-failed-branch-transactions', data, options);
  }
  
  getFailedCustomerTransactions(data: AccountantGetFailedCustomerTransactionsQuery, options?: any): Observable<Paged<CustomerTransactionData>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-failed-customer-transactions', data, options);
  }
  
  retryTransaction(data: AccountantRetryTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/retry-transaction', data, options);
  }
  
  retryCustomerTransaction(data: AccountantRetryCustomerTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/retry-customer-transaction', data, options);
  }
  
  completeTransaction(data: AccountantCompleteTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/complete-transaction', data, options);
  }
  
  completeCustomerTransaction(data: AccountantCompleteCustomerTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/complete-customer-transaction', data, options);
  }
  
  refundTransaction(data: AccountantRefundTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/refund-transaction', data, options);
  }
  
  refundCustomerTransaction(data: AccountantRefundCustomerTransactionCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/refund-customer-transaction', data, options);
  }
  
  getOperator(data: AccountantGetOperatorQuery, options?: any): Observable<OperatorName> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-operator', data, options);
  }
  
  getOperators(data: AccountantGetOperatorsQuery, options?: any): Observable<Array<OperatorName>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-operators', data, options);
  }
  
  getCashboxes(data: AccountantGetCashboxesQuery, options?: any): Observable<Array<CashboxName>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-cashboxes', data, options);
  }
  
  getBranches(data: AccountantGetBranchesQuery, options?: any): Observable<Array<BranchName>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-branches', data, options);
  }
  
  addReport(data: AccountantAddReportCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/add-report', data, options);
  }
  
  reportRestore(data: AccountantReportRestoreCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-restore', data, options);
  }
  
  reportBackup(data: AccountantReportBackupCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-backup', data, options);
  }
  
  reportInitRepo(data: AccountantReportInitRepoCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-init-repo', data, options);
  }
  
  reportRecalculate(data: AccountantReportRecalculateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/report-recalculate', data, options);
  }
  
  getPartnerReport(data: AccountantGetPartnerReportQuery, options?: any): Observable<Paged<PartnerReport>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-partner-report', data, options);
  }
  
  getPartnerReportSummary(data: AccountantGetPartnerReportSummaryQuery, options?: any): Observable<PartnerReportSummary> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-partner-report-summary', data, options);
  }
  
  getPendingCancellationClaims(data: AccountantGetPendingCancellationClaimsQuery, options?: any): Observable<Paged<Claim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-pending-cancellation-claims', data, options);
  }
  
  countPendingCancellationClaims(data: AccountantCountPendingCancellationClaimsQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/count-pending-cancellation-claims', data, options);
  }
  
  acceptCancellationClaim(data: AccountantAcceptCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/accept-cancellation-claim', data, options);
  }
  
  dismissCancellationClaim(data: AccountantDismissCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/dismiss-cancellation-claim', data, options);
  }
  
  preApproveCancellationClaim(data: AccountantPreApproveCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/pre-approve-cancellation-claim', data, options);
  }
  
  preRejectCancellationClaim(data: AccountantPreRejectCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/pre-reject-cancellation-claim', data, options);
  }
  
  approveCancellationClaim(data: AccountantApproveCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/approve-cancellation-claim', data, options);
  }
  
  rejectCancellationClaim(data: AccountantRejectCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/reject-cancellation-claim', data, options);
  }
  
  getPendingCustomerCancellationClaims(data: AccountantGetPendingCustomerCancellationClaimsQuery, options?: any): Observable<Paged<CustomerClaim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-pending-customer-cancellation-claims', data, options);
  }
  
  countPendingCustomerCancellationClaims(data: AccountantCountPendingCustomerCancellationClaimsQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/count-pending-customer-cancellation-claims', data, options);
  }
  
  acceptCustomerCancellationClaim(data: AccountantAcceptCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/accept-customer-cancellation-claim', data, options);
  }
  
  dismissCustomerCancellationClaim(data: AccountantDismissCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/dismiss-customer-cancellation-claim', data, options);
  }
  
  preApproveCustomerCancellationClaim(data: AccountantPreApproveCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/pre-approve-customer-cancellation-claim', data, options);
  }
  
  preRejectCustomerCancellationClaim(data: AccountantPreRejectCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/pre-reject-customer-cancellation-claim', data, options);
  }
  
  approveCustomerCancellationClaim(data: AccountantApproveCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/approve-customer-cancellation-claim', data, options);
  }
  
  rejectCustomerCancellationClaim(data: AccountantRejectCustomerCancellationClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/reject-customer-cancellation-claim', data, options);
  }
  
  getCustomerClaim(data: AccountantGetCustomerClaimQuery, options?: any): Observable<CustomerClaim> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-customer-claim', data, options);
  }
  
  searchCustomerCancellationClaims(data: AccountantSearchCustomerCancellationClaimsQuery, options?: any): Observable<Paged<CustomerClaim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/search-customer-cancellation-claims', data, options);
  }
  
  freezeAccount(data: AccountantFreezeAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/freeze-account', data, options);
  }
  
  unfreezeAccount(data: AccountantUnfreezeAccountCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/unfreeze-account', data, options);
  }
  
  openUnfreezeAccountClaim(data: AccountantOpenUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/open-unfreeze-account-claim', data, options);
  }
  
  acceptUnfreezeAccountClaim(data: AccountantAcceptUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/accept-unfreeze-account-claim', data, options);
  }
  
  dismissUnfreezeAccountClaim(data: AccountantDismissUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/dismiss-unfreeze-account-claim', data, options);
  }
  
  approveUnfreezeAccountClaim(data: AccountantApproveUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/approve-unfreeze-account-claim', data, options);
  }
  
  rejectUnfreezeAccountClaim(data: AccountantRejectUnfreezeAccountClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/reject-unfreeze-account-claim', data, options);
  }
  
  searchUnfreezeAccountClaims(data: AccountantSearchUnfreezeAccountClaimsQuery, options?: any): Observable<Paged<UnfreezeAccountClaim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/search-unfreeze-account-claims', data, options);
  }
  
  getUnfreezeAccountClaim(data: AccountantGetUnfreezeAccountClaimQuery, options?: any): Observable<UnfreezeAccountClaim> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-unfreeze-account-claim', data, options);
  }
  
  getPendingUnfreezeAccountClaims(data: AccountantGetPendingUnfreezeAccountClaimsQuery, options?: any): Observable<Paged<UnfreezeAccountClaim>> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/get-pending-unfreeze-account-claims', data, options);
  }
  
  countPendingUnfreezeAccountClaims(data: AccountantCountPendingUnfreezeAccountClaimsQuery, options?: any): Observable<number> {
    return this.post(BaseApi.apiUrl + '/upay/accountant/count-pending-unfreeze-account-claims', data, options);
  }
}

export class ClaimistApi extends BaseApi {
  
  authenticate(data: ClaimistAuthenticateCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/authenticate', data, options);
  }
  
  getAccessToken(data: ClaimistGetAccessTokenQuery, options?: any): Observable<string> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/get-access-token', data, options);
  }
  
  getClaims(data: ClaimistGetClaimsQuery, options?: any): Observable<Paged<Claim>> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/get-claims', data, options);
  }
  
  approveClaim(data: ClaimistApproveClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/approve-claim', data, options);
  }
  
  rejectClaim(data: ClaimistRejectClaimCommand, options?: any): Observable<void> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/reject-claim', data, options);
  }
  
  getClaimDetails(data: ClaimistGetClaimDetailsQuery, options?: any): Observable<ClaimDetails> {
    return this.post(BaseApi.apiUrl + '/upay/claimist/get-claim-details', data, options);
  }
  
  getOperatorById(data: OperatorGetOperatorByIdQuery, options?: any): Observable<OperatorInfo> {
    return this.post(BaseApi.apiUrl + '/upay/operator/get-operator-by-id', data, options);
  }
}
