import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseApi} from './base-api';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class FileDownload {
  constructor(private http: HttpClient,
              private baseApi: BaseApi) {
  }


  // downloadData(url, requestData): Observable<any> {
  //   return new Observable(obs => {
  //     var oReq = new XMLHttpRequest();
  //     oReq.open("POST", url, true);
  //     oReq.setRequestHeader("content-type", "application/json");
  //     const authToken = localStorage.getItem('authToken');
  //     oReq.setRequestHeader('Authorization', `Bearer ${authToken}`);
  //     oReq.setRequestHeader("content-type", "application/json");
  //     oReq.responseType = "arraybuffer";
  //
  //     oReq.onload =  (oEvent) => {
  //       var arrayBuffer = oReq.response;
  //       var byteArray = new Uint8Array(arrayBuffer);
  //       obs.next(byteArray);
  //     };
  //
  //     const body = JSON.stringify(requestData);
  //     oReq.send(body);
  //   });
  // }

  post<T>(url: string, data: any) {
    // this.downloadData(url, data).subscribe(result=>{
    //   // console.log(result);
    //   this.downloadFile(result, 'export.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    // });

    const options: any = {};
    options.headers = this.baseApi.getAuthorizationHeader();
    options.responseType = 'arraybuffer';
    options.contentType = 'text/plain';
    options.observe = 'response';

    return this.http.post(url, data, options)
      .pipe(map((res: any) => {
        let fileName = res.headers.get('Content-Disposition');
        fileName = fileName.substring(fileName.indexOf('"') + 1, fileName.lastIndexOf('"'));
        const fileType =  res.headers.get('Content-Type');

        let state = false;

        if ( fileName && fileType ) {
          // this.forceDownloadFile(res.body, fileName, fileType);
          this.downloadFile(res.body, fileName, fileType);
          state = true;
        }
        return state;
    }));
  }


  downloadFile(data, name = 'file', type = 'text/plain') {
    // const { createElement } = document; // depreciated
    const { URL: { createObjectURL, revokeObjectURL }, setTimeout } = window;

    const blob = new Blob([data], { type });
    const url = createObjectURL(blob);

    const anchor = document.createElement('a');
    // const anchor = createElement('a');
    anchor.setAttribute('href', url);
    anchor.setAttribute('download', name);
    anchor.click();

    setTimeout(() => { revokeObjectURL(url); }, 100);
  }

  forceDownloadFile(data, fileName, fileType) {
    // for BOM, as for excell default encoding is ANSI, for more info
    // https://stackoverflow.com/questions/42462764/javascript-export-csv-encoding-utf-8-issue
    const universalBOM = '';

    const anchor = document.createElement('a');
    anchor.style.display = 'none';
    document.body.appendChild(anchor);


    anchor.href = 'data:' + fileType + '; charset=utf-8,' + encodeURIComponent(universalBOM + data);
    anchor.download = `${fileName}`;

    anchor.click();

    console.log('anchor', anchor);
  }
}
