import {forkJoin, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {TranslateLoader} from '@ngx-translate/core';
import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class TranslationService implements TranslateLoader {
  constructor(private http: HttpClient) {  }

  getTranslation(lang: string): Observable<any> {
    return forkJoin(
      this.http.get<any>(environment.APIEndpoint.resourceProviderUrl + '/api/resources/upay-merchant-back-office'),
    ).pipe(map(response => {
      const merchentBackOffice = response[0].okPayload.map[lang];
      const margetTranslationsList = [...merchentBackOffice];
      const translateData = {};
      margetTranslationsList.map (ds => {
        translateData[ds.key] = ds.value;
      });
      return translateData;
    }));
  }
}
