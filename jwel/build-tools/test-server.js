var path = require("path");
var express = require("express");

var app = express();
var filePath =path.join(__dirname, '../dist/out');
app.use(express.static(filePath));

// app.get('/',function(req,res){
//     res.sendFile(path.join(__dirname+'/./mobile_payment_en.html'));
//     //__dirname : It will resolve to your project folder.
// });

app.use("*", function(req, resp) {
  resp.sendFile(path.join(__dirname,"../dist/out/index.html"));
});
app.listen(3000);
console.log("Server running at Port 3000", filePath);
