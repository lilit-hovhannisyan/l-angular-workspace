$(document).ready(function () {
    if(window.location.hash) {
        $(window.location.hash).scrollTo(1000);
    }

    $('a.scroll-to').on('click', function (e) {
        e.preventDefault();
        let id = $(this).attr('href');
        $(id).scrollTo(1000);
    });

    $('[data-confirm]').confirmAction();
    $('nav.sidebar').autoCollapse();
    $('[role="tablist"]').autoOpenTabOnError('.with-errors');
    $('[data-copy]').copyText();
    $('select.styled-select').customSelect();

    $('a[data-content]').popover({
        'trigger': 'click',
        'placement': 'auto',
        'html' : true,
        'template' : '<div class="popover info-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
    });
});

window.onbeforeunload = function (e) {
    showOverlay();
};