"use strict";

(function($) {
    $.fn.extend({
        extendVisibility: function($source, dataAttr, className) {
            let $fields = $(this);
            className = className || 'hidden';
            dataAttr = dataAttr || 'visible';

            if (typeof $source === 'string') {
                $source = $($source);
            }
            if ($source.length !== 1 || $source.get(0).tagName !== 'SELECT') {
                return;
            }
            let check;
            if ($source.get(0).hasAttribute('multiple')) {
                check = function ($source, $select, reset) {
                    reset = reset || false;
                    let sourceVal = $source.val() || [];
                    let $options = $select.find('option, optgroup');

                    $options.each(function() {
                        let $option = $(this);
                        if ($option.data(dataAttr) && sourceVal.indexOf(''+$option.data(dataAttr)+'') === -1) {
                            $option.addClass(className);
                        } else {
                            $option.removeClass(className);
                        }
                    });

                    if (reset && sourceVal.indexOf(''+$options.filter('[value="'+$select.val()+'"]').data(dataAttr)+'') === -1) {
                        $select.val('').customSelect();
                    }
                };
            } else {
                check = function ($source, $select, reset) {
                    reset = reset || false;
                    let sourceVal = $source.val();
                    let $options = $select.find('option, optgroup').filter('[data-'+dataAttr+']').addClass(className).filter('[data-'+dataAttr+'="'+sourceVal+'"]').removeClass(className);
                    if ($options.length === 1 && $options.get(0).tagName === 'SELECT') {
                        $options.prop('selected', true);
                    } else if (reset && $options.filter('[value="'+$select.val()+'"]').length === 0) {
                        $select.val('');
                    }
                    $select.customSelect();
                };
            }

            setTimeout(function() {
                $fields.each(function() {
                    let $select = $(this);
                    if (this.tagName !== 'SELECT') {
                        return;
                    }
                    check($source, $select);
                    $source.on('change', function() {
                        check($source, $select, true);
                    });
                });
            }, 0);
            return $fields;
        }
    });
})(jQuery);