@extends('adminarea.layout')

@section('title')
    @if($model->exists)
        Update User {{ $model->email }}
    @else
        Create User
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="element-wrapper">
                <h6 class="element-header">
                    @if($model->exists)
                        Update User {{ $model->email }}
                    @else
                        Create User
                    @endif
                </h6>
                <div class="element-box">
                    <form method="post" action="{{ $model->exists ? route('adminarea.user.update', ['id' => $model->id]) : route('adminarea.user.store') }}">
                        @csrf
                        @if ($model->exists)
                            @method('PUT')
                        @endif

                        <div class="form-group {{ $errors->has('email') ? ' has-error has-danger' : '' }}">
                            <label for="">Email</label>
                            <input type="text" class="form-control {{ $errors->has('email') ? ' error-field' : '' }}" name="email" value="{{ old('email', $model->email) }}" placeholder="User email">
                            @if ($errors->has('email'))
                                <span class="help-block form-text with-errors">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('name') ? ' has-error has-danger' : '' }}">
                            <label for="">Name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? ' error-field' : '' }}" name="name" value="{{ old('name', $model->name) }}" placeholder="User name">
                            @if ($errors->has('name'))
                                <span class="help-block form-text with-errors">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ ($errors->has('roles') || $errors->has('roles.*')) ? ' has-error has-danger' : '' }}">
                            <label for="">Roles</label>
                            <select name="roles[]" class="form-control select2" multiple>
                                @foreach(\App\Models\Role::all() as $role)
                                    <option value="{{ $role->id }}" {{ in_array($role->id, old('roles', $model->roles->pluck('id')->all())) ? 'selected' : '' }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('roles'))
                                <span class="help-block form-text with-errors">{{ $errors->first('roles') }}</span>
                            @endif
                            @if ($errors->has('roles.*'))
                                <span class="help-block form-text with-errors">{{ $errors->first('roles.*') }}</span>
                            @endif
                        </div>

                        @if (!$model->exists)
                            <fieldset class="form-group">
                                <legend><span>Password section</span></legend>
                                <div class="form-group {{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control {{ $errors->has('password') ? ' error-field' : '' }}" name="password" placeholder="User's new password">
                                    @if ($errors->has('password'))
                                        <span class="help-block form-text with-errors">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error has-danger' : '' }}">
                                    <label for="">Repeat password</label>
                                    <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' error-field' : '' }}" name="password_confirmation" placeholder="Repeat password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block form-text with-errors">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </fieldset>
                        @endif

                        <div class="form-buttons-w">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endsection

@section('custom-js')
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('select.select2').select2({
            placeholder: 'Choose user roles',
        });
    </script>
@endsection