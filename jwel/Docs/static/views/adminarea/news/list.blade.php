@extends('adminarea.layout')

@section('title', 'News')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="element-wrapper">
                <h6 class="element-header">
                    News
                </h6>
                <div class="element-box">
                    <div class="controls-above-table">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <a class="btn btn-sm btn-primary" href="{{ route('adminarea.news.create') }}">Add news</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        {!! $grid->show('news-table') !!}
                    </div>
                    <nav class="text-right mt-3">
                        {!! $models->links() !!}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style type="text/css">
        .img-thumbnail {
            max-width: 200px;
            max-height: 200px;
        }
    </style>
@endsection
