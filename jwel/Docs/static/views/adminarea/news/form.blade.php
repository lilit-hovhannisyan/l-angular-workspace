@extends('adminarea.layout')

@section('title')
    @if($model->exists)
        Update News {{ $model->slug }}
    @else
        Create News
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="element-wrapper">
                <h6 class="element-header">
                    @if($model->exists)
                        Update News {{ $model->slug }}
                    @else
                        Create News
                    @endif
                </h6>
                <div class="element-box">
                    <form method="post" enctype="multipart/form-data" action="{{ $model->exists ? route('adminarea.news.update', ['id' => $model->id]) : route('adminarea.news.store') }}">
                        @csrf
                        @if ($model->exists)
                            @method('PUT')
                        @endif

                        <div class="form-group {{ $errors->has('slug') ? ' has-error has-danger' : '' }}">
                            <label for="">Slug</label>
                            <input type="text" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" {{--@if($model->exists) disabled @endif--}} name="slug" value="{{ old('slug', $model->slug) }}" placeholder="Slug">
                            @if ($errors->has('slug'))
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('slug') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('new_image') ? ' has-error has-danger' : '' }}">
                            <label for="">Image</label>
                            <div class="image-file-block">
                                @if ($model->exists && $model->image)
                                    <img src="{{ asset('storage/' . $model->image) }}" class="img-thumbnail">
                                @endif
                                <input type="file" accept="image/png, image/jpeg" class="image-file form-control {{ $errors->has('new_image') ? ' is-invalid' : '' }}" name="new_image" placeholder="Image file">
                                @if ($errors->has('new_image'))
                                    <span class="help-block form-text invalid-feedback">{{ $errors->first('new_image') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('publish_date') ? ' has-error has-danger' : '' }}">
                            <label for="">Publish date</label>
                            <input type="text" class="form-control date-input {{ $errors->has('publish_date') ? ' is-invalid' : '' }}" name="publish_date" value="{{ old('publish_date', $model->publish_date) }}" placeholder="Publish date">
                            @if ($errors->has('publish_date'))
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('publish_date') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('i18n_languages') ? ' has-error has-danger' : '' }}">
                            <label for="">Languages <em>(Choose languages which will be available)</em></label>
                            <div class="languages-list">
                                @foreach($model->getI18nAllowedLanguages() as $langCode)
                                    <label class="mr-4">
                                        <input type="checkbox" name="i18n_languages[]" value="{{ $langCode }}" {{ in_array($langCode, old('i18n_languages', $model->getI18nLanguages())) ? 'checked' : '' }}>
                                        {{ $langCode }}
                                    </label>
                                @endforeach
                            </div>
                            @if ($errors->has('i18n_languages.*'))
                                <span class="form-control is-invalid d-none hidden"></span>
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('i18n_languages.*') }}</span>
                            @endif
                            @if ($errors->has('i18n_languages'))
                                <span class="form-control is-invalid d-none hidden"></span>
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('i18n_languages') }}</span>
                            @endif
                        </div>
                        <nav>
                            <div class="nav nav-tabs mb-4" id="lang-tabs" role="tablist">
                                @foreach($model->getI18nAllowedLanguages() as $langCode)
                                    <a class="nav-item nav-link" id="lang-{{ $langCode }}-tab" data-toggle="tab" href="#lang-{{ $langCode }}" role="tab" aria-controls="lang-{{ $langCode }}" aria-selected="false" data-language="{{ $langCode }}">{{ strtoupper($langCode) }}</a>
                                @endforeach
                            </div>
                        </nav>
                        <div class="tab-content" id="lang-tab-content">
                            @foreach($model->getI18nAllowedLanguages() as $langCode)
                                <div class="tab-pane fade show" id="lang-{{ $langCode }}" role="tabpanel" aria-labelledby="lang-{{ $langCode }}-tab">
                                    <div class="form-group {{ $errors->has('title.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Title ({{ $langCode }})</label>
                                        <input type="text" class="form-control {{ $errors->has('title.'.$langCode) ? ' is-invalid' : '' }}" name="title[{{ $langCode }}]" value="{{ old("title.{$langCode}", $model->getI18nAttribute('title', $langCode)) }}" placeholder="Title">
                                        @if ($errors->has('title.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('title.'.$langCode) }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('intro_text.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Intro text ({{ $langCode }})</label>
                                        <textarea type="text" class="form-control {{ $errors->has('intro_text.'.$langCode) ? ' is-invalid' : '' }}" name="intro_text[{{ $langCode }}]" placeholder="Intro text">{{ old("intro_text.{$langCode}", $model->getI18nAttribute('intro_text', $langCode)) }}</textarea>
                                        @if ($errors->has('intro_text.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('intro_text.'.$langCode) }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('content.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Content ({{ $langCode }})</label>
                                        <textarea type="text" class="form-control html-editor {{ $errors->has('content.'.$langCode) ? ' is-invalid' : '' }}" name="content[{{ $langCode }}]" placeholder="Content">{{ old("content.{$langCode}", $model->getI18nAttribute('content', $langCode)) }}</textarea>
                                        @if ($errors->has('content.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('content.'.$langCode) }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-buttons-w">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="//cdn.ckeditor.com/4.9.0/full/ckeditor.js"></script>
    {{--<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>--}}
    <script type="application/javascript">
        if (CKEDITOR.env.ie && CKEDITOR.env.version < 9) {
            CKEDITOR.tools.enableHtml5Elements(document);
        }
        const editorConfig = {
            fullPage: false,
            height: 300,
            width: 'auto',
            language: 'en',
            toolbarGroups: [
                { name: 'tools', groups: [ 'mode', 'document' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'links' },
                { name: 'insert' },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'align' ] },
                '/',
                { name: 'styles' },
                { name: 'colors' }
            ],
            removeButtons: 'Save,Anchor,Flash,Table,Smiley,Iframe,PageBreak,NewPage,Print',
            allowedContent: true
        };

        $(document).ready(function() {
            CKEDITOR.replaceAll(function(template, config) {
                if ($(template).hasClass('html-editor')) {
                    $.extend(config, editorConfig);
                    return true;
                } else {
                    return false;
                }
            });

            const initThumb = function($input, dataSrc) {
                dataSrc = dataSrc || null;
                let $img = $input.closest('.image-file-block').find('img');

                if (dataSrc) {
                    if ($img.length === 0) {
                        $img = $('<img class="img-thumbnail"> src=""').insertBefore($input);
                    }

                    $img.attr('src', dataSrc);
                } else {
                    $input.val('');
                    $img.remove();
                }
            };

            $('input[type="file"].image-file').each(function (i) {
                const input = this;
                const $input = $(input);
                $input.on('change', function (e) {
                    if (input.files.length) {
                        const reader = new FileReader();
                        reader.onload = (function(file) {
                            if (file.type.indexOf('image') === 0) {
                                const dataURL = URL.createObjectURL(file);
                                initThumb($input, dataURL);
                            } else {
                                initThumb($input)
                            }
                        })(input.files[0]);

                        reader.readAsDataURL(input.files[0]);
                    } else {
                        initThumb($input);
                    }
                });
            });

            const processLanguages = function($checkboxes) {
                const $langs = $checkboxes.filter(':checked').map(function() {
                    return this.value;
                }).get();

                const $tabsBlock = $('#lang-tabs');

                $tabsBlock.find('[data-toggle="tab"]').each(function () {
                    let $item = $(this);
                    let $block = $($item.data('target') || $item.attr('href'));
                    if ($langs.indexOf($item.data('language')) === -1) {
                        $item.addClass('hidden');
                        $block.addClass('hidden');
                    } else {
                        $item.removeClass('hidden');
                        $block.removeClass('hidden');
                    }
                });

                $tabsBlock.autoOpenTabOnError('.invalid-feedback');
            };

            const $checkboxes = $('.languages-list').find('input[type="checkbox"]');
            processLanguages($checkboxes);
            $checkboxes.on('change', function(e) {
                processLanguages($checkboxes);
            });

            $('input.date-input').each(function (i) {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: false,
                    minYear: parseInt(moment().subtract(1, 'years').format('YYYY'), 10),
                    maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                });
            });
        });
    </script>
@endsection

@section('custom-css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style type="text/css">
        .img-thumbnail {
            max-width: 300px;
            max-height: 200px;
        }
        body .daterangepicker th.prev,
        body .daterangepicker th.next {
            text-align: center !important;
        }
        body .daterangepicker.single {
            min-width: inherit;
        }
    </style>
@endsection
