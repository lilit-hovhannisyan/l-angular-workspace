@extends('adminarea.layout')

@section('title')
    Update profile
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="element-wrapper">
                <h6 class="element-header">Update profile</h6>
                <div class="element-box">
                    <form method="post" action="{{ route('adminarea.profile.updatePassword') }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group {{ $errors->has('current_password') ? ' has-error has-danger' : '' }}">
                            <label for="">Current Password</label>
                            <input type="password" class="form-control {{ $errors->has('current_password') ? ' error-field' : '' }}" name="current_password" placeholder="Current password">
                            @if ($errors->has('current_password'))
                                <span class="help-block form-text with-errors">{{ $errors->first('current_password') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                            <label for="">Password</label>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' error-field' : '' }}" name="password" placeholder="New password">
                            @if ($errors->has('password'))
                                <span class="help-block form-text with-errors">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error has-danger' : '' }}">
                            <label for="">Repeat Password</label>
                            <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' error-field' : '' }}" name="password_confirmation" placeholder="Repeat password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block form-text with-errors">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>

                        <div class="form-buttons-w">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection