@extends('adminarea.layout')

@section('title')
    Update profile
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="element-wrapper">
                <h6 class="element-header">Update profile</h6>
                <div class="element-box">
                    <form method="post" action="{{ route('adminarea.profile.update') }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group {{ $errors->has('email') ? ' has-error has-danger' : '' }}">
                            <label for="">Email</label>
                            <input type="text" class="form-control {{ $errors->has('email') ? ' error-field' : '' }}" name="email" value="{{ old('email', $model->email) }}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block form-text with-errors">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('name') ? ' has-error has-danger' : '' }}">
                            <label for="">Name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? ' error-field' : '' }}" name="name" value="{{ old('name', $model->name) }}" placeholder="Your Name">
                            @if ($errors->has('name'))
                                <span class="help-block form-text with-errors">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="form-buttons-w">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection