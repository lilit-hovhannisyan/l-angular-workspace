<section class="container pl-0">
    <div id="notifications-block">
        @if (session('alert-success'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                {{ session('alert-success') }}
            </div>
        @endif
        @if (session('alert-warning'))
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                {{ session('alert-warning') }}
            </div>
        @endif
        @if (session('alert-info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                {{ session('alert-info') }}
            </div>
        @endif
        @if (session('alert-danger'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                {{ session('alert-danger') }}
            </div>
        @endif
    </div>
</section>