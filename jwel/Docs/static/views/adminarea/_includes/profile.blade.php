<div class="logged-user-i" style="padding-left: 10rem;">
    <div class="avatar-w" style="font-size: 1.8rem;">
        <i class="fa fa-user-circle"></i>
    </div>
    <div class="logged-user-menu color-style-dark">
        <div class="logged-user-avatar-info">
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{ auth()->check() ? auth()->user()->name : '' }}</div>
                <div class="logged-user-role">{{ auth()->check() ? auth()->user()->roles->implode('name', ' | ') : 'guest' }}</div>
            </div>
        </div>
        <ul>
            <li>
                <a href="{{ route('adminarea.profile.edit') }}">
                    <i class="fa fa-pencil-alt"></i><span>Change profile</span>
                </a>
            </li>
            <li>
                <a href="{{ route('adminarea.profile.editPassword') }}">
                    <i class="fa fa-key"></i><span>Change password</span>
                </a>
            </li>
            <li>
                <a data-href="{{ route('logout') }}" data-confirm="Are you sure to logout?" data-method="post">
                    <i class="fa fa-sign-out-alt"></i><span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>