<div class="menu-w selected-menu-color-dark menu-has-selected-link menu-activated-on-click color-scheme-light color-style-default sub-menu-color-light menu-position-side menu-side-left menu-layout-compact sub-menu-style-inside">
    <div class="logo-w">
        <a class="logo" href="{{ route('adminarea') }}">
            <div class="logo-element"></div>
            <div class="logo-label">
                Admin area
            </div>
        </a>
    </div>

    <ul class="main-menu">
        <li class="sub-header">
            <span>Main menu</span>
        </li>
        <li class="">
            <a href="{{ route('adminarea.news') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>News</span>
            </a>
        </li>
        <li class="">
            <a href="{{ route('adminarea.banner') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-image"></div>
                </div>
                <span>Banners/Sliders</span>
            </a>
        </li>
        <li class="">
            <a href="{{ route('adminarea.resource') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file"></div>
                </div>
                <span>I18n Resources</span>
            </a>
        </li>
        <li class="sub-header">
            <span>User-Roles management</span>
        </li>
        <li class="">
            <a href="{{ route('adminarea.users') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-users"></div>
                </div>
                <span>Users</span>
            </a>
        </li>
        <li class="">
            <a href="{{ route('adminarea.roles') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-robot-2"></div>
                </div>
                <span>Roles</span>
            </a>
        </li>
        <li class="">
            <a href="{{ route('adminarea.access_rules') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-documents-17"></div>
                </div>
                <span>Access Rules</span>
            </a>
        </li>
    </ul>
</div>