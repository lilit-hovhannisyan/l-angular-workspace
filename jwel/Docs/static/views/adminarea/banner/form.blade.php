@extends('adminarea.layout')

@section('title')
    @if($model->exists)
        Update Banner
    @else
        Create Banner
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="element-wrapper">
                <h6 class="element-header">
                    @if($model->exists)
                        Update Banner {{ $model->screen }} : {{ $model->screen_position }}
                    @else
                        Create Banner
                    @endif
                </h6>
                <div class="element-box">
                    <form method="post" enctype="multipart/form-data" action="{{ $model->exists ? route('adminarea.banner.update', ['id' => $model->id]) : route('adminarea.banner.store') }}">
                        @csrf
                        @if ($model->exists)
                            @method('PUT')
                        @endif

                        <div class="form-group">
                            <label for="">Where to show</label>
                            <select id="target-list" name="target" class="form-control styled-select {{ $errors->has('target') ? ' is-invalid' : '' }}">
                                <option value="">Select target</option>
                                @foreach(\App\Models\Banner::TARGET as $k => $v)
                                    <option value="{{ $k }}" @if(old('target', $model->target) === $k) selected @endif>{{ $v }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('target'))
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('target') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('screen') ? ' has-error has-danger' : '' }}">
                            <label for="">Screen slug</label>

                            <select id="screen-list" name="screen" data-placeholder="Select slug" class="form-control {{ $errors->has('screen') ? ' is-invalid' : '' }}">
                                <option value="">Select screen</option>
                                @foreach(\App\Models\Banner::TARGET as $k => $v)
                                    <optgroup label="{{ $v }}" data-extend="{{ $k }}">
                                        @foreach(($options['positionsMap'][$k] ?? []) as $screen => $posList)
                                            <option value="{{ $screen }}" data-extend="{{ $k }}" @if(old('screen', $model->screen) === $screen && old('target', $model->target) === $k) selected @endif>{{ $screen }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>

                            @if ($errors->has('screen'))
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('screen') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('screen_position') ? ' has-error has-danger' : '' }}">
                            <label for="">Position slug in screen</label>

                            <select id="position-list" name="screen_position" data-placeholder="Select position" class="form-control {{ $errors->has('screen_position') ? ' is-invalid' : '' }}">
                                <option value="">Select position</option>
                                @foreach(\App\Models\Banner::TARGET as $k => $v)
                                    @foreach($options['positionsMap'][$k] as $screen => $positions)
                                        <optgroup label="{{ $v }} ({{ $screen }})" data-extend="{{ $screen }}" data-target="{{ $k }}">
                                        @foreach($positions as $position)
                                            <option value="{{ $position }}" data-extend="{{ $screen }}" data-target="{{ $k }}" @if(old('screen_position', $model->screen_position) === $position && old('screen', $model->screen) === $screen && old('target', $model->target) === $k) selected @endif>{{ $position }}</option>
                                        @endforeach
                                        </optgroup>
                                    @endforeach
                                @endforeach
                            </select>

                            @if ($errors->has('screen_position'))
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('screen_position') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('new_image') ? ' has-error has-danger' : '' }}">
                            <label for="">Image</label>
                            <div class="image-file-block">
                                @if ($model->exists && $model->image)
                                    <img src="{{ asset('storage/' . $model->image) }}" class="img-thumbnail">
                                @endif
                                <input type="file" accept="image/png, image/jpeg" class="image-file form-control {{ $errors->has('new_image') ? ' is-invalid' : '' }}" name="new_image" placeholder="Image file">
                                @if ($errors->has('new_image'))
                                    <span class="help-block form-text invalid-feedback">{{ $errors->first('new_image') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('i18n_languages') ? ' has-error has-danger' : '' }}">
                            <label for="">Languages <em>(Choose languages which will be available)</em></label>
                            <div class="languages-list">
                                @foreach($model->getI18nAllowedLanguages() as $langCode)
                                    <label class="mr-4">
                                        <input type="checkbox" name="i18n_languages[]" value="{{ $langCode }}" {{ in_array($langCode, old('i18n_languages', $model->getI18nLanguages())) ? 'checked' : '' }}>
                                        {{ $langCode }}
                                    </label>
                                @endforeach
                            </div>
                            @if ($errors->has('i18n_languages.*'))
                                <span class="form-control is-invalid d-none hidden"></span>
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('i18n_languages.*') }}</span>
                            @endif
                            @if ($errors->has('i18n_languages'))
                                <span class="form-control is-invalid d-none hidden"></span>
                                <span class="help-block form-text invalid-feedback">{{ $errors->first('i18n_languages') }}</span>
                            @endif
                        </div>

                        <nav>
                            <div class="nav nav-tabs mb-4" id="lang-tabs" role="tablist">
                                @foreach($model->getI18nAllowedLanguages() as $langCode)
                                    <a class="nav-item nav-link" id="lang-{{ $langCode }}-tab" data-toggle="tab" href="#lang-{{ $langCode }}" role="tab" aria-controls="lang-{{ $langCode }}" aria-selected="false" data-language="{{ $langCode }}">{{ strtoupper($langCode) }}</a>
                                @endforeach
                            </div>
                        </nav>
                        <div class="tab-content" id="lang-tab-content">
                            @foreach($model->getI18nAllowedLanguages() as $langCode)
                                <div class="tab-pane fade show" id="lang-{{ $langCode }}" role="tabpanel" aria-labelledby="lang-{{ $langCode }}-tab">
                                    <div class="form-group {{ $errors->has('title.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Title ({{ $langCode }})</label>
                                        <input type="text" class="form-control {{ $errors->has('title.'.$langCode) ? ' is-invalid' : '' }}" name="title[{{ $langCode }}]" value="{{ old("title.{$langCode}", $model->getI18nAttribute('title', $langCode)) }}" placeholder="Title">
                                        @if ($errors->has('title.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('title.'.$langCode) }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('text.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Text ({{ $langCode }})</label>
                                        <textarea type="text" class="form-control html-editor {{ $errors->has('text.'.$langCode) ? ' is-invalid' : '' }}" name="text[{{ $langCode }}]" placeholder="Text">{{ old("text.{$langCode}", $model->getI18nAttribute('text', $langCode)) }}</textarea>
                                        @if ($errors->has('text.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('text.'.$langCode) }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('url.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">URL to open ({{ $langCode }})</label>
                                        <input type="text" class="form-control {{ $errors->has('url.'.$langCode) ? ' is-invalid' : '' }}" name="url[{{ $langCode }}]" value="{{ old("url.{$langCode}", $model->getI18nAttribute('url', $langCode)) }}" placeholder="Title">
                                        @if ($errors->has('url.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('url.'.$langCode) }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('label.'.$langCode) ? ' has-error has-danger' : '' }}">
                                        <label for="">Button label ({{ $langCode }})</label>
                                        <input type="text" class="form-control {{ $errors->has('label.'.$langCode) ? ' is-invalid' : '' }}" name="label[{{ $langCode }}]" value="{{ old("label.{$langCode}", $model->getI18nAttribute('label', $langCode)) }}" placeholder="Title">
                                        @if ($errors->has('label.'.$langCode))
                                            <span class="help-block form-text invalid-feedback">{{ $errors->first('label.'.$langCode) }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-buttons-w">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.ckeditor.com/4.9.0/full/ckeditor.js"></script>
    {{--<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>--}}

    <script type="application/javascript">
        if (CKEDITOR.env.ie && CKEDITOR.env.version < 9) {
            CKEDITOR.tools.enableHtml5Elements(document);
        }
        const editorConfig = {
            fullPage: false,
            height: 300,
            width: 'auto',
            language: 'en',
            toolbarGroups: [
                { name: 'tools', groups: [ 'mode', 'document' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'links' },
                { name: 'insert' },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'align' ] },
                '/',
                { name: 'styles' },
                { name: 'colors' }
            ],
            removeButtons: 'Save,Anchor,Flash,Table,Smiley,Iframe,PageBreak,NewPage,Print',
            allowedContent: true
        };

        $(document).ready(function() {
            $('#lang-tabs').autoOpenTabOnError('.invalid-feedback');

            CKEDITOR.replaceAll(function(template, config) {
                if ($(template).hasClass('html-editor')) {
                    $.extend(config, editorConfig);
                    return true;
                } else {
                    return false;
                }
            });

            const initThumb = function($input, dataSrc) {
                dataSrc = dataSrc || null;
                let $img = $input.closest('.image-file-block').find('img');

                if (dataSrc) {
                    if ($img.length === 0) {
                        $img = $('<img class="img-thumbnail"> src=""').insertBefore($input);
                    }

                    $img.attr('src', dataSrc);
                } else {
                    $input.val('');
                    $img.remove();
                }
            };

            $('input[type="file"].image-file').each(function (i) {
                const input = this;
                const $input = $(input);
                $input.on('change', function (e) {
                    if (input.files.length) {
                        const reader = new FileReader();
                        reader.onload = (function(file) {
                            if (file.type.indexOf('image') === 0) {
                                const dataURL = URL.createObjectURL(file);
                                initThumb($input, dataURL);
                            } else {
                                initThumb($input)
                            }
                        })(input.files[0]);

                        reader.readAsDataURL(input.files[0]);
                    } else {
                        initThumb($input);
                    }
                });
            });

            const processLanguages = function($checkboxes) {
                const $langs = $checkboxes.filter(':checked').map(function() {
                    return this.value;
                }).get();

                const $tabsBlock = $('#lang-tabs');

                $tabsBlock.find('[data-toggle="tab"]').each(function () {
                    let $item = $(this);
                    let $block = $($item.data('target') || $item.attr('href'));
                    if ($langs.indexOf($item.data('language')) === -1) {
                        $item.addClass('hidden');
                        $block.addClass('hidden');
                    } else {
                        $item.removeClass('hidden');
                        $block.removeClass('hidden');
                    }
                });

                $tabsBlock.autoOpenTabOnError('.invalid-feedback');
            };

            const $checkboxes = $('.languages-list').find('input[type="checkbox"]');
            processLanguages($checkboxes);
            $checkboxes.on('change', function(e) {
                processLanguages($checkboxes);
            });

            $('#screen-list').extendVisibility('#target-list', 'extend');
            $('#position-list').extendVisibility('#screen-list', 'extend').extendVisibility('#target-list', 'target', 'd-none');
        });
    </script>
@endsection

@section('custom-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">

    <style type="text/css">
        .img-thumbnail {
            max-width: 300px;
            max-height: 200px;
        }
    </style>
@endsection
