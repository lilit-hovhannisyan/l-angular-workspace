<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="U!Pay adminarea" name="keywords">
    <meta content="Taron" name="author">
    <meta content="Admin area of U!Pay" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="/favicon.ico" rel="shortcut icon">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">

    <link rel="stylesheet" media="all" href="{{ asset('assets/plugins/fontawesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" media="all" href="{{ asset(mix('assets/adminarea/css/app.css')) }}">

    @yield('custom-css')
</head>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
<div class="all-wrapper with-side-panel solid-bg-all">
    <div class="layout-w">
        <!--------------------
        START - Main Menu
        -------------------->
        @include('adminarea._includes.menu')
        <!--------------------
        END - Main Menu
        -------------------->

        <div class="content-w">

            <!--------------------
            START - Top Bar
            -------------------->
            <div class="top-bar color-scheme-light" style="height: 54px;">
                @include('adminarea._includes.notifications')
                <!--------------------
                START - Top Menu Controls
                -------------------->
                <div class="top-menu-controls">
                    <!--------------------
                    START - User avatar and menu in secondary top menu
                    -------------------->
                    <div class="logged-user-w">
                        @include('adminarea._includes.profile')
                    </div>
                    <!--------------------
                    END - User avatar and menu in secondary top menu
                    -------------------->
                </div>
                <!--------------------
                END - Top Menu Controls
                -------------------->
            </div>
            <!--------------------
            END - Top Bar
            -------------------->

            <!--------------------
            START - Breadcrumbs
            -------------------->
            @include('adminarea._includes.breadcrumb')
            <!--------------------
            END - Breadcrumbs
            -------------------->

            <div class="content-i">
                <div class="content-box">
                    @yield('content')
                </div>

                @hasSection('sidebar')
                    <!--------------------
                    START - Sidebar
                    -------------------->
                    <div class="content-panel">
                        @yield('sidebar')
                    </div>
                    <!--------------------
                    END - Sidebar
                    -------------------->
                @endif
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="assetsUrlForJS" value="{!! asset('assets') !!}/">

<script src="{{ asset(mix('assets/js/manifest.js')) }}"></script>
<script src="{{ asset(mix('assets/js/vendor.js')) }}"></script>
<script src="{{ asset(mix('assets/adminarea/js/app.js')) }}"></script>

@yield('custom-js')

</body>
</html>