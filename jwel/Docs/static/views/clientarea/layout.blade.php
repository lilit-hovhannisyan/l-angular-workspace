<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="U!Pay" name="keywords">
    <meta content="Taron" name="author">
    <meta content="U!Pay" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" media="all" href="{{ asset(mix('assets/clientarea/css/app.css')) }}">

    @yield('custom-css')
</head>
<body>

<header class="fixed-top header-bg ">
    <div class="container">
        @include('clientarea._includes.menu')
    </div>
</header>

<main role="main">
   <div class="clientarea">
    @yield('content')
   </div>
</main>

<!-- FOOTER -->
<footer class="container-fluid">

        @include('clientarea._includes.footer')

</footer>

{{-- This part must be added in end of body --}}

<input type="hidden" id="assetsUrlForJS" value="{!! asset('assets') !!}/">

<script src="{{ asset(mix('assets/js/manifest.js')) }}"></script>
<script src="{{ asset(mix('assets/js/vendor.js')) }}"></script>
<script src="{{ asset(mix('assets/clientarea/js/app.js')) }}"></script>

@yield('custom-js')

{{-- End of scropts --}}

</body>
</html>