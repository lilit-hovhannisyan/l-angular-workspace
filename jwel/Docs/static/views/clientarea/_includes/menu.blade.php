<nav class="navbar navbar-expand-md static-psn">
    <a class="navbar-brand" href="#" style="flex: 0 0 82px;">
        <img src="{{ asset('assets/clientarea/images/Upay-logo-cl.svg') }}" alt="UPay">
    </a>

    <div class="menu" style="flex: 0 0 290px;">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item static-psn">
                <a class="nav-link" href="#">Cards </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Groups</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">History</a>
            </li>
        </ul>
    </div>

    <div class="right-menu">
        <!--search box-->
        <form class="form-inline mt-2 mt-md-0" >
            <div class="dropdown static-psn">
                <a class="dropdown-toggle aft-none" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="icon_search lens"></i>
                </a>
                <div class="search-box dropdown-menu w-100" aria-labelledby="searchDropdown">
                    <a href="javascript:void(0);" class="hidden-sm hidden-md hidden-lg hidden-xl"><i class="icon_close"></i></a>
                    <input class="form-control search-input" type="text" placeholder="Site Search..." aria-label="Search">
                </div>
            </div>
        </form>
        <!--languages box--->
        <div class="lang dropdown" >
            <a class="dropdown-toggle aft-none" href="#" id="langDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>Arm</span><i class="icon_arrow-down"></i>
            </a>
            <div class="lang-box dropdown-menu" aria-labelledby="langDropdown">
                <a href="javascript:void(0);" class="lang-box-item active">Arm</a>
                <div class="dropdown-divider"></div>
                <a href="javascript:void(0);" class="lang-box-item">Eng</a>
                <div class="dropdown-divider"></div>
                <a href="javascript:void(0);" class="lang-box-item">Rus</a>
            </div>
        </div>
        <!--notifications-->
        <div class="notes">
            <i class="icon_notification-16"></i>
            <span class="notes-number">2</span>
        </div>
        <!--user info-->
        <div class="user-block">
            <div class="user-foto pull-left">
               <span><i class="icon_user-profile"></i></span>
            </div>
            <div class="user-info pull-left dropdown">
                <a class="dropdown-toggle aft-none" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="phone">+37490006960</span>
                    <span class="pull-right">
                        <i class="icon_arrow-down"></i>
                        <span class="amount money-value">156.000</span>
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="profileDropdown">

                    <ul>
                        <li class="dropdown-divider"></li>
                        <li class="current">
                            <button type="button" name="" class="btn">View Profile</button>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="current">
                           <a href=""><i class="icon_settings"></i><span>Settings</span> </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="current">
                            <a href=""><i class="icon_logout"></i><span>Log out</span> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>