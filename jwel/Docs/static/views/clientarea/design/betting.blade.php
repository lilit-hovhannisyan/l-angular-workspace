@extends('clientarea.layout')

@section('title', 'betting')

@section('content')

    <div class="container payment">
        <!--tabs content-->
        <div class="row mt-4 ">
            <!--left tabs line-->
            <div class="side-nav col-1 pl-0 pr-0" >
                    <div class="side-nav-item">
                        <a href="" class="link-btn">
                            <i class="icon_home-br"></i>
                            <span>Home</span>
                        </a>
                    </div>
                    <div class="dropdown-divider "></div>
                    <div class="side-nav-item">
                        <a href="" class="link-btn active">
                            <i class="icon_pay-46"></i>
                            <span>Pay</span>
                        </a>
                    </div>
                    <div class="dropdown-divider "></div>
                    <div class="side-nav-item">
                        <a href="" class="link-btn">
                            <i class="icon_topup"></i>
                            <span>Top up</span>
                        </a>
                    </div>
                    <div class="dropdown-divider "></div>
                    <div class="side-nav-item">
                        <a href="" class="link-btn">
                            <i class="icon_transfer"></i>
                            <span>Transfer</span>
                        </a>
                    </div>
            </div>
            <!--./left tabs line-->
            <!--desktop variant tabs-tabpanels-->
            <div class="nav flex-column nav-pills tabs col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                    <i class="icon_mobile"></i> <span>Mobile connection</span>
                </a>
                <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-utility" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                    <i class="icon_utility"></i> <span>Utility payments</span></a>
                <a class="nav-link" id="v-pills-internet-tab" data-toggle="pill" href="#v-pills-internet" role="tab" aria-controls="v-pills-internet" aria-selected="false">
                    <i class="icon_internet"></i><span>Internet</span></a>
                <a class="nav-link" id="v-pills-tv-tab" data-toggle="pill" href="#v-pills-tv" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_tv"></i> <span>TV</span>
                </a>
                <a class="nav-link" id="v-pills-parking-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_parking"></i> <span>Parking</span>
                </a>
                <a class="nav-link" id="v-pills-loan-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_loan-repayment"></i> <span>Loan repayment</span>
                </a>
                <a class="nav-link" id="v-pills-property-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_property"></i> <span>Property and lond tax</span>
                </a>
                <a class="nav-link active" id="v-pills-betting-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_betting"></i> <span>Betting</span>
                </a>
                <a class="nav-link" id="v-pills-games-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_online-games"></i> <span>Online games</span>
                </a>
                <a class="nav-link" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_social"></i> <span>Social media</span>
                </a>
                <a class="nav-link" id="v-pills-qrcode-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_qr-code"></i> <span>Payment via QR-code</span>
                </a>
                <a class="nav-link" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_mobile"></i> <span>Other</span>
                </a>
            </div>
            <div class="tab-content col-8" id="v-pills-tabContent">
                <div class="tab-pane fade" id="v-pills-mobile" role="tabpanel" aria-labelledby="v-pills-mobile-tab">
                    comming soon
                </div>
                <div class="tab-pane fadeshow  active" id="v-pills-utility" role="tabpanel" aria-labelledby="v-pills-utility-tab">
                    <div class="top-payment-nav">
                        <a href="" class="top-link">My Groups</a>
                        <a href="" class="top-link">Favorites</a>
                        <a href="" class="top-link">Regular Payment</a>
                    </div>
                    <div class="form-group sr-group">
                        <input class="form-control search-input" type="text" placeholder="search..." />
                    </div>
                    <div class="d-flex justify-content-around">
                        <div class="row betting-list">
                        <div class="col-2 ">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Vivarobet</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Vivaro Casino</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Toto Casino</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Eurofootball</span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Adjarabet</span>
                            </div>
                        </div>
                        <div class="col-2 ">
                            <div class="whitebox">
                            <i class="icon_damafon round-box"></i>
                            <span >Eurofootball</span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-internet" role="tabpanel" aria-labelledby="v-pills-internet-tab">
                    comming soon
                </div>
            </div>
            <!--./END -->
        </div>
        <!--./tabs content-->

    </div>

@endsection