@extends('clientarea.layout')

@section('title', 'Example')

@section('content')

    <div class="container text-center container-pad">
        <h2>Example</h2>
        <div class="row row-redirect">
          <div class="col-8">
              <img src="{{ asset('assets/clientarea/images/Upay-logo-cl.svg') }}" alt="Generic placeholder image" >
          </div>
            <div class="col-4">
                <form>
                    <ul>
                        <li>
                            <div class="form-group">
                                <label class="has-float-label">
                                    <input class="form-control" id="groupName" name="" placeholder="Group name" />
                                    <span>Group name</span>
                                </label>
                            </div>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>

@endsection