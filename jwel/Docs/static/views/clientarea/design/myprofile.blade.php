@extends('clientarea.layout')

@section('title', 'profile')

@section('content')

    <div class="container profile">

        <!--top tabs line-->
        <div class="top-nav">
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home "><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--desktop variant tabs-tabpanels-->
            <div class="nav flex-column nav-pills tabs col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <div class="whitebox-br">
                    <a class="nav-link active" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                        <span>My profile</span>
                    </a>
                    <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-utility" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                        <span>Password</span></a>
                    <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                        <span>Mobile number</span>
                    </a>
                    <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-utility" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                        <span>Email</span></a>
                </div>
            </div>

            <div class="tab-content col-9" id="v-pills-tabContent">
                <div class="tab-pane fadeshow  active" id="v-pills-utility" role="tabpanel" aria-labelledby="v-pills-utility-tab">
                    <div class="whitebox">
                        <ul class="profile-box">
                            <li class="current">
                                <div class="user-foto pull-left">
                                    <span><i class="icon_user-profile"></i></span>
                                </div>
                                <div class="user-info pull-left">
                                    <div class="user-name mb-2">YOUR AVATAR</div>
                                    <form>
                                        <div style="position:relative;">
                                            <a class='btn btn-primary' href='javascript:;'>
                                                Upload File...
                                                <input type="file"
                                                       style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);
                                                       -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
                                                       opacity:0;background-color:transparent;color:transparent;'
                                                       name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                            </a>
                                            &nbsp;
                                            <span class='label label-info' id="upload-file-info"></span>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="current">
                                <p><label class="pull-left">Upay ID</label><span class="pull-right"><b>415987456</b></span></p><br/>
                                <p><label class="pull-left">Status</label><span class="pull-right"><b>Premium</b></span></p>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="current">
                                <label class="gray-txt fnt-14">NAME</label><br/><span>Aida Hambarcumyan</span>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="current">
                                <label class="gray-txt fnt-14">MOBILE</label><br/><span>+37493236589</span>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="current">
                                <label class="gray-txt fnt-14">EMAIL</label><br/><span>sdfrg@szdfgs.com</span>
                            </li>
                            {{--<li class="dropdown-divider"></li>--}}
                            {{--<li class="current text-center">--}}
                                {{--<button type="button" class="btn btn-primary">Update</button>--}}

                            {{--</li>--}}
                        </ul>
                    </div>
                </div>

            </div>
            <!--./END -->

        </div>

    </div>

@endsection