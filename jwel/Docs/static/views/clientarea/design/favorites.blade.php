@extends('clientarea.layout')

@section('title', 'favorites')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home"><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user activity side-->
            <div class="col-md-8 col-lg-9">

                <div class="form-group sr-group">
                    <input class="form-control search-input white-bg" type="text" placeholder="Search favorite" />
                </div>
                <div class="whitebox tab-block choose-payment mt-4 pb-0">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Groups</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <p class="border-dot"><i class="icon_favorite"></i> Add your favorite payment from payment list</p>

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <ul class="tab-list mt-3">
                                <li class="current fav-list selected">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTransfer">
                                            <i class="icon_transfer pull-left"></i>
                                            <span class="pull-left service-name">Transfer between cards</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                            <div class="custom-control custom-checkbox checked pull-right">
                                                 <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked="checked">
                                                <label class="custom-control-label" for="customRadio1"></label>
                                            </div>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseTransfer">
                                        <div class="collapse-content">
                                            <ul class="customer-info">
                                                <li>
                                                    <label>Subscriber number </label><span>0688914</span>
                                                </li>
                                                <li>
                                                    <label>User </label><span>Hovhannes Hovhannisyan</span>
                                                </li>
                                                <li>
                                                    <label> Telephone </label><span>010263656</span>
                                                </li>
                                                <li>
                                                    <label>Address</label><span>Nor Norq, Er.7 str., Yerevan, Armenia</span>
                                                </li>
                                            </ul>

                                            <p class="title-1 ml-3">As of 01 March, 2018</p>
                                            <ul>
                                                <li>
                                                    <label>Daytime conumption</label><span>255 kWh</span>
                                                </li>
                                                <li>
                                                    <label> Nightime conumption</label><span>255 kWh</span>
                                                </li>
                                                <li>
                                                    <label>  Subsidized amount</label><span>0.00</span>
                                                </li>
                                                <li>
                                                    <label>To be paid</label><span>10.000</span>
                                                </li>
                                                <li>
                                                    <label>From 01.03 paid via UP</label><span>10.000</span>
                                                </li>
                                            </ul>

                                            <div class="dropdown-divider"></div>

                                            <p class="title-1 ml-3">As of 01 June, 2018</p>
                                            <ul>
                                                <li>
                                                    <label>Dept</label><span>40.000 amd</span>
                                                </li>
                                                <li>
                                                    <label>Has been paid</label><span>10.000 amd</span>
                                                </li>
                                            </ul>

                                            <div class="dropdown-divider"></div>

                                            <p class="title-1 ml-3">Payment method</p>
                                            <ul>
                                                <li>
                                                    <div class="row ">
                                                        <div class="col-4">
                                                            <div class="card-type">
                                                                <img src="{{ asset('assets/clientarea/images/uwallet-card.svg') }}" alt="Generic placeholder image" >
                                                                <span>Your balance<br>120.000 AMD</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="card-type">
                                                                <img src="{{ asset('assets/clientarea/images/mastercard-card.svg') }}" alt="Generic placeholder image" >
                                                                <span>**** 1245</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="card-type">
                                                                <img src="{{ asset('assets/clientarea/images/visa-card.svg') }}" alt="Generic placeholder image" >
                                                                <span>**** 6589</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                            <div class="dropdown-divider"></div>

                                            <p class="title-1 ml-3">Amount</p>

                                            <ul>
                                                <li>
                                                    <div class="form-group d-flex mb-1">
                                                        <input type="text" name="" class="form-control mr-4" placeholder="00.0 AMD"/>
                                                        <button type="button" name="" class="btn w-20" >Pay</button>
                                                    </div>
                                                    <div >
                                                        <span class="small pull-left"><b>With comission</b> <br/>2% Comission</span>
                                                        <span class="small pull-left ml-5">00.0 AMD</span>
                                                    </div>
                                                </li>
                                            </ul>

                                            <div class="dropdown-divider"></div>

                                            <div class="ml-3 mt-3">
                                                <button type="button" name="" class="btn pull-left btn-white"><i class="icon_favoriteadded orange-txt"></i> <span>Add to favorite</span></button>

                                            </div>

                                        </div>
                                    </div>
                                </li>
                                <li class="current fav-list">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                            <i class="icon_internet pull-left"></i>
                                            <span class="pull-left service-name">Internet & TV payment</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                            <div class="custom-control custom-checkbox pull-right">
                                                 <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                 <label class="custom-control-label" for="customRadio2"></label>
                                            </div>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseInternet">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                                <li class="current fav-list">
                                    <div class="activity-head">
                                        <a class=" collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                            <i class="icon_utility pull-left"></i>
                                            <span class="pull-left service-name">Electricy payment</span>
                                            <i class="icon_arrow-right pull-right"></i>
                                            <div class="custom-control custom-checkbox pull-right">
                                                 <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio3"></label>
                                            </div>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseElectricy">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    </div>
                </div>
            </div>

            <!--user info side-->
            <div class="col-md-4 col-lg-3">

                <div class="whitebox text-center">
                    <img src="{{ asset('assets/clientarea/images/payment.jpg') }}" class="w-75"  alt="Generic placeholder"/>
                    <p class="title-1 mb-0">PAY WITH PROFIT</p>
                    <p class="small">The amount of the cashback will be reflected in your profile in the UPay Bonus.</p>
                    <button type="button" class="btn btn-default pay" name="">Pay</button>
                </div>

                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->

        </div>
        <!--./tabs content-->
    </div>

@endsection