@extends('clientarea.layout')

@section('title', 'pay')

@section('content')

    <div class="container payment">
        <!--top tabs line-->
        <div class="top-nav">
            <div class="row row-justify">
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn home "><span>Dashboard</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn pay active"><span>Pay</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn topup"><span>Top up</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn transfer"><span>Transfer</span></a>
            </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--desktop variant tabs-tabpanels-->
                <div class="nav flex-column nav-pills tabs col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                        <i class="icon_mobile"></i> <span>Mobile connection</span>
                    </a>
                    <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-utility" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                        <i class="icon_utility"></i> <span>Utility payments</span></a>
                    <a class="nav-link" id="v-pills-internet-tab" data-toggle="pill" href="#v-pills-internet" role="tab" aria-controls="v-pills-internet" aria-selected="false">
                        <i class="icon_internet"></i><span>Internet</span></a>
                    <a class="nav-link" id="v-pills-tv-tab" data-toggle="pill" href="#v-pills-tv" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_tv"></i> <span>TV</span>
                    </a>
                    <a class="nav-link" id="v-pills-parking-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_parking"></i> <span>Parking</span>
                    </a>
                    <a class="nav-link" id="v-pills-loan-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_loan-repayment"></i> <span>Loan repayment</span>
                    </a>
                    <a class="nav-link" id="v-pills-property-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_property"></i> <span>Property and lond tax</span>
                    </a>
                    <a class="nav-link" id="v-pills-betting-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_betting"></i> <span>Betting</span>
                    </a>
                    <a class="nav-link" id="v-pills-games-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_online-games"></i> <span>Online games</span>
                    </a>
                    <a class="nav-link" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_social"></i> <span>Social media</span>
                    </a>
                    <a class="nav-link" id="v-pills-qrcode-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_qr-code"></i> <span>Payment via QR-code</span>
                    </a>
                    <a class="nav-link" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="icon_mobile"></i> <span>Other</span>
                    </a>
                </div>
                <div class="tab-content col-9" id="v-pills-tabContent">
                    <div class="tab-pane fade" id="v-pills-mobile" role="tabpanel" aria-labelledby="v-pills-mobile-tab">
                       comming soon
                    </div>
                    <div class="tab-pane fadeshow  active" id="v-pills-utility" role="tabpanel" aria-labelledby="v-pills-utility-tab">
                        <div class="form-group sr-group">
                            <input class="form-control search-input" type="text" placeholder="search..." />
                        </div>
                        <div class="d-flex">
                            <div class="whitebox w-20">
                                <i class="icon_gas round-box"></i>
                                <span>Gas</span>
                            </div>
                            <div class="whitebox w-20">
                                <i class="icon_jur round-box"></i>
                                <span>Water</span>
                            </div>
                            <div class="whitebox w-20">
                                <i class="icon_utility round-box"></i>
                                <span>Electricity</span>
                            </div>
                            <div class="whitebox w-20">
                                <i class="icon_telephone round-box"></i>
                                <span>Fixed telephone</span>
                            </div>
                            <div class="whitebox w-20">
                                <i class="icon_damafon round-box"></i>
                                <span>Damafon</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-internet" role="tabpanel" aria-labelledby="v-pills-internet-tab">
                        comming soon
                    </div>
                </div>
            <!--./END -->
        </div>
        <!--./tabs content-->

    </div>

@endsection