@extends('clientarea.layout')

@section('title', 'history')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home"><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user activity side-->
            <div class="col-md-8 col-lg-9">
                <div class="whitebox">
                    <p class="title-2 ">Activity</p>
                    <ul class="activity">
                        <li class="date-line"> <p class="title-1">Today</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTransfer">
                                    <i class="icon_transfer pull-left"></i>
                                    <span class="pull-left">Transfer between cards<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt mr-3">5000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTransfer">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt mr-3">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseInternet">
                                <ul>
                                    <li>
                                        <label>Transaction number</label><span>0168987456321</span>
                                        <a class="pull-right mr-3" data-toggle="modal" data-target="#receiptModal"><i class="icon_show"></i><span class="green-txt">Show receipt</span></a>
                                    </li>
                                    <li>
                                        <label>Status</label><span>025580</span>
                                    </li>
                                    <li>
                                        <label>Account</label><span><b>51.254.00</b></span>
                                    </li>
                                    <li>
                                        <label>Commission</label><span>0.00</span>
                                    </li>
                                    <li>
                                        <span> <button type="button" name="" class="btn pull-left">Pay again</button></span>
                                        <span> <button type="button" name="" class="btn pull-left btn-white"><i class="icon_favoriteadded orange-txt"></i> <span>Add to favorite</span></button></span>

                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="date-line"> <p class="title-1">28 Apr. 2018</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                    <i class="icon_utility pull-left"></i>
                                    <span class="pull-left">Electricy payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt mr-3">5000 </span>
                                </a>
                            </div>

                            <div class="collapse activity-body" id="collapseElectricy">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTV" role="button" aria-expanded="false" aria-controls="collapseTV">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt mr-3">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTV">
                                bla bla
                            </div>
                        </li>
                    </ul>
                    <a href="" class="see-all text-center"> See All Activities</a>
                </div>
            </div>

            <!--user info side-->
            <div class="col-md-4 col-lg-3">
                <div class=" whitebox ">
                   <p class="title-2 ">Select a period</p>
                   <ul>
                       <li class="mb-3 pl-2">
                           <a href="" class="color-txt-icon pr-5"><i class="icon_calendar"></i><span>From</span></a>
                           <a href="" class="color-txt-icon"><i class="icon_calendar"></i><span>To</span></a>
                       </li>
                       <li class="dropdown-divider mt-4 mb-4"></li>
                       <li class="mb-3 mt-3  pl-2">
                           <p class="title-1">Filter by</p>
                       </li>
                       <li class="mb-3 pl-45">
                           <div class="custom-control custom-checkbox">
                               <input type="checkbox" class="custom-control-input" id="customCheck1">
                               <label class="custom-control-label" for="customCheck1">Current</label>
                           </div>
                       </li>
                       <li class="mb-3 pl-45">
                           <div class="custom-control custom-checkbox ">
                               <input type="checkbox" class="custom-control-input" id="customCheck2">
                               <label class="custom-control-label" for="customCheck2">Incomming</label>
                           </div>
                       </li>
                       <li class="mb-3 pl-45">
                           <div class="custom-control custom-checkbox">
                               <input type="checkbox" class="custom-control-input" id="customCheck3">
                               <label class="custom-control-label" for="customCheck3">Outgoing</label>
                           </div>
                       </li>
                   </ul>
                </div>
            </div>
            <!--./user info side-->
        </div>
        <!--./tabs content-->


    </div>

@endsection