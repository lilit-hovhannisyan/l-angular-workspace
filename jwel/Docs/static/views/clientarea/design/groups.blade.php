@extends('clientarea.layout')

@section('title', 'groups')

@section('content')

    <div class="container groups">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home"><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user activity side-->
            <div class="col-md-8 col-lg-9">

                <div class="form-group sr-group">
                    <input class="form-control search-input white-bg" type="text" placeholder="Search favorite" />
                </div>
                <div class="whitebox tab-block choose-payment mt-4 pb-0">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Favorites</a>
                        </li>
                        <li class="nav-item  active">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Groups</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <p class="border-dot"><span class="add-group">+</span><a href="" class="add-group-txt" data-toggle="modal" data-target="#newgroupModal">Add new group</a> </p>

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <ul class="tab-list mt-3">
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseGroup1" role="button" aria-expanded="false" aria-controls="collapseGroup1">
                                            <span class="pull-left pl-0 ">Home-01</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseGroup1">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseGroup2" role="button" aria-expanded="false" aria-controls="collapseGroup2">
                                            <span class="pull-left pl-0 ">Home-02</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseGroup2">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseGroup3" role="button" aria-expanded="false" aria-controls="collapseGroup3">
                                            <span class="pull-left pl-0 ">Home-03</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseGroup3">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    </div>
                </div>
            </div>

            <!--user info side-->
            <div class="col-md-4 col-lg-3">

                <div class="whitebox text-center">
                    <img src="{{ asset('assets/clientarea/images/payment.jpg') }}" class="w-75"  alt="Generic placeholder"/>
                    <p class="title-1 mb-0">PAY WITH PROFIT</p>
                    <p class="small">The amount of the cashback will be reflected in your profile in the UPay Bonus.</p>
                    <button type="button" class="btn btn-default pay" name="">Pay</button>
                </div>

                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->

        </div>
        <!--./tabs content-->

    </div>
    <!--login modal-->

    <div class="modal fade" id="newgroupModal" tabindex="-1" role="dialog" aria-labelledby="newgroupModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">CREATE NEW GROUP</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <label class="has-float-label">
                                        <input class="form-control" id="groupName" name="" placeholder="Group name" />
                                        <span>Group name</span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary">Create group</button>
                </div>
            </div>
        </div>
    </div>

    <!--./login modal-->
@endsection