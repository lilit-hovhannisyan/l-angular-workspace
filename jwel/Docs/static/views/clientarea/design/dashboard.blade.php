@extends('clientarea.layout')

@section('title', 'dashboard')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
        <!--top tabs line-->
        <div class="row row-justify">
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn home active"><span>Dashboard</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn pay"><span>Pay</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn topup"><span>Top up</span></a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="" class="link-btn transfer"><span>Transfer</span></a>
            </div>
        </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user info side-->
            <div class="col-md-4 col-lg-3">
                <div class=" whitebox">
                    <ul class="user-data">
                        <li>
                            <!--user info-->
                            <div class="user-block">
                                <div class="user-foto ">
                                    <span><i class="icon_user-profile"></i></span>
                                </div>
                                <div class="user-info ">
                                    <span class="user-name">Aida Hambardzumyan</span>
                                    <span class="phone">+37490006960</span>
                                    <a href=""><i class="icon_edit"></i></a>
                                </div>
                            </div>
                        </li>

                        <li>
                            <p>
                                <label class="pull-left">Upay ID</label><span class="pull-right">415365987</span>
                            </p>
                            <p >
                                <label class="pull-left">Status</label><span class="pull-right">Premium</span>
                            </p>
                        </li>

                        <li >
                            <i class="icon_upay-logo-cl balance"></i>
                            <p class="line-h">
                                <label class="pull-left"><b><span class="hidden-sm hidden-md">Your</span> Balance</b></label><span class="money-value green-txt">100.00</span>
                            </p>
                        </li>

                        <li >
                            <i class="icon_bonus bonus"></i>
                            <p class="line-h">
                                <label class="pull-left"><b><span class="hidden-sm hidden-md">U!Pay</span> Bonus</b></label><span class="money-value green-txt">1500.00</span>
                            </p>
                            <p class="small">a sum of money added to a person's wages as a reward for good performance.</p>
                        </li>
                    </ul>
                </div>
                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->

            <!--user activity side-->
            <div class="col-md-8 col-lg-9">
                <div class=" whitebox">
                    <p class="title-2 text-center">You have successfully joined to Upay wallet!</p>
                    <div class="start-pay">
                        <button type="button" class="btn btn-default col-md-4 orange">ADD NEW CARD <i class="icon_add-payment"></i></button>
                        <img src="{{ asset('assets/clientarea/images/wallet.png') }}" class="col-md-4 " alt="Generic placeholder image" />
                        <button type="button" class="btn btn-default col-md-4 ">START PAYMENT <i class="icon_pay-46"></i></button>
                    </div>
                </div>
                <div class="mt-4 whitebox tab-block choose-payment">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Groups</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <p class="border-dot"><i class="icon_favorite"></i> Add your favorite payment from payment list</p>
                            <p class="text-center mt-4"><img src="{{ asset('assets/clientarea/images/choose-payment.png') }}" alt="Generic placeholder image" /></p>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    </div>
                </div>
            </div>
        </div>
        <!--./tabs content-->
    </div>

@endsection