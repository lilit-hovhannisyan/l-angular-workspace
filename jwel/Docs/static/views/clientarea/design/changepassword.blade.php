@extends('clientarea.layout')

@section('title', 'changepassword')

@section('content')

    <div class="container profile">

        <!--top tabs line-->
        <div class="top-nav">
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home "><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--desktop variant tabs-tabpanels-->
            <div class="nav flex-column nav-pills tabs col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <div class="whitebox-br">
                    <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                        <span>My profile</span>
                    </a>
                    <a class="nav-link active" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                        <span>Password</span></a>
                    <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                        <span>Mobile number</span>
                    </a>
                    <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-email" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                        <span>Email</span></a>
                </div>
            </div>

            <div class="tab-content col-9" id="v-pills-tabContent">
                <div class="tab-pane fadeshow  active" id="v-pills-utility" role="tabpanel" aria-labelledby="v-pills-password">
                    <div class="whitebox">
                        <div class="tab-block">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item active">
                                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Change password</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Password recovery</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <ul class="profile-box">
                                        <li class="current">
                                            <div class="form-group">
                                                <label class="has-float-label">
                                                    <input class="form-control" id="current-password" name="" placeholder="Current password" />
                                                    <span>Current password</span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="current">
                                            <div class="form-group">
                                                <label class="has-float-label">
                                                    <input class="form-control" id="new-password" name="" placeholder="New password" />
                                                    <span>New password</span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="current">
                                            <div class="form-group">
                                                <label class="has-float-label">
                                                    <input class="form-control" id="confirm-password" name="" placeholder="Confirm new password" />
                                                    <span>Confirm new password</span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="current text-center">
                                            <div class="form-group ">
                                                <button type="button" name="" class="btn btn-primary">Change</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
