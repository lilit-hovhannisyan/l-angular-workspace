@extends('clientarea.layout')

@section('title', 'payutility')

@section('content')

    <div class="container payment">
        <!--top tabs line-->
        <div class="top-nav">
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home "><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay active"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--desktop variant tabs-tabpanels-->
            <div class="nav flex-column nav-pills tabs col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <div class="whitebox-br">
                <a class="nav-link" id="v-pills-mobile-tab" data-toggle="pill" href="#v-pills-mobile" role="tab" aria-controls="v-pills-mobile" aria-selected="true">
                    <i class="icon_mobile"></i> <span>Mobile connection</span>
                </a>
                <a class="nav-link" id="v-pills-utility-tab" data-toggle="pill" href="#v-pills-utility" role="tab" aria-controls="v-pills-utility" aria-selected="false">
                    <i class="icon_utility"></i> <span>Utility payments</span></a>
                <a class="nav-link" id="v-pills-internet-tab" data-toggle="pill" href="#v-pills-internet" role="tab" aria-controls="v-pills-internet" aria-selected="false">
                    <i class="icon_internet"></i><span>Internet</span></a>
                <a class="nav-link" id="v-pills-tv-tab" data-toggle="pill" href="#v-pills-tv" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_tv"></i> <span>TV</span>
                </a>
                <a class="nav-link" id="v-pills-parking-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_parking"></i> <span>Parking</span>
                </a>
                <a class="nav-link" id="v-pills-loan-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_loan-repayment"></i> <span>Loan repayment</span>
                </a>
                <a class="nav-link" id="v-pills-property-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_property"></i> <span>Property and lond tax</span>
                </a>
                <a class="nav-link" id="v-pills-betting-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_betting"></i> <span>Betting</span>
                </a>
                <a class="nav-link" id="v-pills-games-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_online-games"></i> <span>Online games</span>
                </a>
                <a class="nav-link" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_social"></i> <span>Social media</span>
                </a>
                <a class="nav-link" id="v-pills-qrcode-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_qr-code"></i> <span>Payment via QR-code</span>
                </a>
                <a class="nav-link" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <i class="icon_mobile"></i> <span>Other</span>
                </a>

                </div></div>
            <div class="tab-content col-9" id="v-pills-tabContent">
                <div class="tab-pane fadeshow  active" id="v-pills-utility" role="tabpanel" aria-labelledby="v-pills-utility-tab">
                    <div class="whitebox">
                        <div class="utility-payment">
                            <i class="icon_arrow-left mr-5"></i> <i class="icon_utility mr-3 round-box"></i> <span><b>Electricity</b></span>
                        </div>
                        <!--search input------------------------>
                        <div class=" lines">
                            <p>Search with phone or with customer ID</p>
                            <div class="form-group">
                                <input type="text" name="" class="form-control mr-4" placeholder="010235698"/>
                                <button type="button" name="" class="btn w-20" >Search</button>
                            </div>
                        </div>
                        <!--search results --------------------->
                        <div class="search-result">
                            <p class="ml-3 mt-4 fnt-14">If details requested by phone number are incorrect or not found, please, send a request using your subscriber number.</p>
                            <ul class="customer-info">
                                <li>
                                    <label>Subscriber number </label><span>0688914</span>
                                </li>
                                <li>
                                    <label>User </label><span>Hovhannes Hovhannisyan</span>
                                </li>
                                <li>
                                    <label> Telephone </label><span>010263656</span>
                                </li>
                                <li>
                                    <label>Address</label><span>Nor Norq, Er.7 str., Yerevan, Armenia</span>
                                </li>
                            </ul>

                            <p class="title-1 ml-3">As of 01 March, 2018</p>
                            <ul>
                                <li>
                                    <label>Daytime conumption</label><span>255 kWh</span>
                                </li>
                                <li>
                                    <label> Nightime conumption</label><span>255 kWh</span>
                                </li>
                                <li>
                                    <label>  Subsidized amount</label><span>0.00</span>
                                </li>
                                <li>
                                    <label>To be paid</label><span>10.000</span>
                                </li>
                                <li>
                                    <label>From 01.03 paid via UP</label><span>10.000</span>
                                </li>
                            </ul>

                            <div class="dropdown-divider"></div>

                            <p class="title-1 ml-3">As of 01 June, 2018</p>
                            <ul>
                                <li>
                                    <label>Dept</label><span>40.000 amd</span>
                                </li>
                                <li>
                                    <label>Has been paid</label><span>10.000 amd</span>
                                </li>
                            </ul>

                            <div class="dropdown-divider"></div>

                            <p class="title-1 ml-3">Payment method</p>
                            <ul>
                                <li>
                                    <div class="row ">
                                        <div class="col-4">
                                            <div class="card-type">
                                                <img src="{{ asset('assets/clientarea/images/uwallet-card.svg') }}" alt="Generic placeholder image" >
                                                <span>Your balance<br>120.000 AMD</span>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card-type">
                                                <img src="{{ asset('assets/clientarea/images/mastercard-card.svg') }}" alt="Generic placeholder image" >
                                                <span>**** 1245</span>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card-type">
                                                <img src="{{ asset('assets/clientarea/images/visa-card.svg') }}" alt="Generic placeholder image" >
                                                <span>**** 6589</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <div class="dropdown-divider"></div>

                             <p class="title-1 ml-3">Amount</p>

                              <ul>
                                  <li>
                                      <div class="form-group d-flex mb-1">
                                          <input type="text" name="" class="form-control mr-4" placeholder="00.0 AMD"/>
                                          <button type="button" name="" class="btn w-20" >Pay</button>
                                      </div>
                                      <div >
                                          <span class="small pull-left"><b>With comission</b> <br/>2% Comission</span>
                                          <span class="small pull-left ml-5">00.0 AMD</span>
                                      </div>
                                  </li>
                              </ul>

                            <div class="dropdown-divider"></div>

                            <div class="ml-3 mt-3">
                                <button type="button" name="" class="btn pull-left btn-white"><i class="icon_favoriteadded orange-txt"></i> <span>Add to favorite</span></button>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
            <!--./END -->
        </div>
        <!--./tabs content-->

    </div>

@endsection