@extends('clientarea.layout')

@section('title', 'cards')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home "><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user info side-->
            <div class="col-md-4 col-lg-3">
                <div class=" whitebox">
                    <ul class="user-data text-center">
                        <li>
                            <p class="txt-uppercase">AIDA HAMBARCUMYAN</p>
                        </li>
                        <li>
                            <p class="txt-uppercase">CARD NUMBER</p>
                            <p><b>5554 xxxx xxxx 1236</b></p>
                        </li>
                        <li>
                            <p>Expiration date</p>
                            <p><b>Valid 07/20</b></p>
                        </li>
                        <li>
                            <a href="" class="red-txt">Remove card</a>
                        </li>
                    </ul>
                </div>
                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->
            <!--user activity side-->
            <div class="col-md-8 col-lg-9">
                <div class="whitebox">
                    <p class="title-2 ">Activity</p>
                    <ul class="activity">
                        <li class="date-line"> <p class="title-1">Today</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTransfer">
                                    <i class="icon_transfer pull-left"></i>
                                    <span class="pull-left">Transfer between cards<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt">5000 </span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTransfer">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseInternet">
                                <ul>
                                    <li>
                                        <label>Transaction number</label><span>0168987456321</span>
                                        <a class="pull-right mr-3" data-toggle="modal" data-target="#receiptModal"><i class="icon_show"></i><span class="green-txt">Show receipt</span></a>
                                    </li>
                                    <li>
                                        <label>Status</label><span>025580</span>
                                    </li>
                                    <li>
                                        <label>Account</label><span><b>51.254.00</b></span>
                                    </li>
                                    <li>
                                        <label>Commission</label><span>0.00</span>
                                    </li>
                                    <li>
                                        <span> <button type="button" name="" class="btn pull-left">Pay again</button></span>
                                        <span> <button type="button" name="" class="btn pull-left btn-white"><i class="icon_favoriteadded orange-txt"></i> <span>Add to favorite</span></button></span>

                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="date-line"> <p class="title-1">28 Apr. 2018</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                    <i class="icon_utility pull-left"></i>
                                    <span class="pull-left">Electricy payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt">5000</span>
                                </a>
                            </div>

                            <div class="collapse activity-body" id="collapseElectricy">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTV" role="button" aria-expanded="false" aria-controls="collapseTV">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTV">
                                bla bla
                            </div>
                        </li>
                    </ul>
                    <a href="" class="see-all text-center"> See All Activities</a>
                </div>
            </div>

        </div>
        <!--./tabs content-->


    </div>

@endsection