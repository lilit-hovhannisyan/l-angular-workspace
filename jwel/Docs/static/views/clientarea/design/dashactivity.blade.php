@extends('clientarea.layout')

@section('title', 'activity')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home active"><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user info side-->
            <div class="col-md-4 col-lg-3">
                <div class=" whitebox">
                    <ul class="user-data">
                        <li>
                            <!--user info-->
                            <div class="user-block">
                                <div class="user-foto ">
                                    <span><i class="icon_user-profile"></i></span>
                                </div>
                                <div class="user-info ">
                                    <span class="user-name">Aida Hambardzumyan</span>
                                    <span class="phone">+37490006960</span>
                                    <a href=""><i class="icon_edit"></i></a>
                                </div>
                            </div>
                        </li>

                        <li>
                            <p>
                                <label class="pull-left">Upay ID</label><span class="pull-right">415365987</span>
                            </p>
                            <p >
                                <label class="pull-left">Status</label><span class="pull-right">Premium</span>
                            </p>
                        </li>

                        <li >
                            <i class="icon_upay-logo-cl balance"></i>
                            <p class="line-h">
                                <label class="pull-left"><b><span class="hidden-sm hidden-md">Your</span> Balance</b></label><span class="money-value green-txt">100.00 </span>
                            </p>
                        </li>

                        <li >
                            <i class="icon_bonus bonus"></i>
                            <p class="line-h">
                                <label class="pull-left"><b><span class="hidden-sm hidden-md">U!Pay</span> Bonus</b></label><span class="money-value green-txt">1500.00 </span>
                            </p>
                            <p class="small">a sum of money added to a person's wages as a reward for good performance.</p>
                        </li>
                    </ul>
                </div>
                <div class=" whitebox mt-4">
                    <div class="title-2">Saved Cards </div>
                    <div class="cards-wallet">
                        <ul>
                            <li class="empty-card">
                                <p><a href="" class="pull-left">Add new card</a><i class="icon_add-payment pull-right"></i></p>
                            </li>
                            <li class="visa-card"><p >**** 1256</p></li>
                            <li class="master-card"><p>**** 1256</p></li>
                            <li class="arca-card"><p>**** 1256</p></li>
                            <li class="diners-card"><p>**** 1256</p></li>
                            <li class="american-card"><p>**** 1256</p></li>
                        </ul>
                    </div>
                </div>
                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->

            <!--user activity side-->
            <div class="col-md-8 col-lg-9">
                <div class="whitebox">
                    <p class="title-2 ">Activity</p>
                    <ul class="activity">
                        <li class="date-line"> <p class="title-1">Today</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTransfer">
                                    <i class="icon_transfer pull-left"></i>
                                    <span class="pull-left">Transfer between cards<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt mr-3">5000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTransfer">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt mr-3">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseInternet">
                               <ul>
                                   <li>
                                       <label>Transaction number</label><span>0168987456321</span>
                                       <a class="pull-right mr-3" data-toggle="modal" data-target="#receiptModal"><i class="icon_show"></i><span class="green-txt">Show receipt</span></a>
                                   </li>
                                   <li>
                                       <label>Status</label><span>025580</span>
                                   </li>
                                   <li>
                                       <label>Account</label><span><b>51.254.00</b></span>
                                   </li>
                                   <li>
                                       <label>Commission</label><span>0.00</span>
                                   </li>
                                   <li>
                                       <span> <button type="button" name="" class="btn pull-left">Pay again</button></span>
                                       <span> <button type="button" name="" class="btn pull-left btn-white"><i class="icon_favoriteadded orange-txt"></i> <span>Add to favorite</span></button></span>

                                   </li>
                               </ul>
                            </div>
                        </li>
                        <li class="date-line"> <p class="title-1">28 Apr. 2018</p></li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                    <i class="icon_utility pull-left"></i>
                                    <span class="pull-left">Electricy payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value green-txt mr-3">5000</span>
                                </a>
                            </div>

                            <div class="collapse activity-body" id="collapseElectricy">
                                bla bla
                            </div>
                        </li>
                        <li class="current">
                            <div class="activity-head">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTV" role="button" aria-expanded="false" aria-controls="collapseTV">
                                    <i class="icon_internet pull-left"></i>
                                    <span class="pull-left">Internet & TV payment<br><span class="small"> 15:32:20</span> </span>
                                    <i class="icon_arrow-right pull-right"></i>
                                    <span class="money-value red-txt mr-3">-25000</span>
                                </a>
                            </div>
                            <div class="collapse activity-body" id="collapseTV">
                                bla bla
                            </div>
                        </li>
                    </ul>
                    <a href="" class="see-all text-center"> See All Activities</a>
                </div>

                <!-- adds box-->
                <div class="d-fb mt-4">
                    <div class="advers col-6 ">
                        <img src="{{ asset('assets/clientarea/images/addes.jpg') }}" class="w-100"  alt="Generic placeholder"/>
                    </div>
                    <div class="whitebox profit-box col-6">
                        <img src="{{ asset('assets/clientarea/images/payment.jpg') }}" class="profit-pic "  alt="Generic placeholder"/>
                        <div class="profit-txt">
                            <p class="title-1 mb-0">PAY WITH PROFIT</p>
                            <p class="small">The amount of the cashback will be reflected in your profile in the UPay Bonus. <button type="button" class="btn btn-default pay" name="">Pay</button></p>

                        </div>
                    </div>
                </div>

                <!--favorite-groups box ----->
                <div class="mt-4 whitebox tab-block choose-payment">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Groups</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <p class="border-dot"><i class="icon_favorite"></i> Add your favorite payment from payment list</p>

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <ul class="paymentlist">
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTV">
                                            <i class="icon_transfer pull-left"></i>
                                            <span class="pull-left service-name">Transfer between cards</span>
                                            <i class="icon_arrow-right pull-right"></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseTransfer">
                                        bla bla
                                    </div>
                                </li>
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                            <i class="icon_internet pull-left"></i>
                                            <span class="pull-left service-name">Internet & TV payment</span>
                                            <i class="icon_arrow-right pull-right"></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseInternet">
                                        bla bla
                                    </div>
                                </li>
                                <li class="current">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                            <i class="icon_utility pull-left"></i>
                                            <span class="pull-left service-name">Electricy payment</span>
                                            <i class="icon_arrow-right pull-right"></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseElectricy">
                                        bla bla
                                    </div>
                                </li>
                            </ul>
                            <div class="dropdown-divider mt-0 mb-0"></div>
                            <a href="" class="see-all text-center mt-3"> See All Activities</a>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    </div>
                </div>
            </div>
        </div>
        <!--./tabs content-->

        <!--receipt modal-->

        <div class="modal fade" id="receiptModal" tabindex="-1" role="dialog" aria-labelledby="receiptModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="logo-md">
                            <img src="{{ asset('assets/clientarea/images/Upay-logo-cl.svg') }}" alt="UPay">
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ul>
                            <li><label><b>«Յուքոմ» ՓԲԸ  </b></label> <span>ՀՎՀՀ 02588245</span></li>
                            <li><label>Հեռ. 011 700 800</label> </li>
                            <li><label>19.08.2018 15:35:16</label></li>

                            <li class="dropdown-divider"></li>

                            <li><label>Անդորրագիր  </label>  <span>18081900015845</span> </li>

                            <li class="dropdown-divider"></li>

                            <li><label>Ստացող</label> <span>«Յուքոմ» ՓԲԸ</span> </li>

                            <li class="dropdown-divider"></li>

                            <li><label>Բաժանորդային համար </label><span><b>0650024</b></span></li>
                            <li><label> Բաժանորդ	</label><span>  Աիդա Համբարձումյան</span></li>
                            <li><label>Պարտք առ 01.08.2018  </label><span> 9210.00 դր.</span></li>
                            <li><label>Միջնորդավճար </label><span>   0.00 դր</span></li>

                            <li class="dropdown-divider"></li>

                            <li><label> Ընդամենը վճարվել է </label><span><b>10000.00 դր.</b></span>
                            <li><label>Մնացորդ	 </label> <span> 790.00 դր.</span>  </li>

                            <li class="dropdown-divider"></li>

                            <li><label>Շնորհակալություն</label></li>

                            <li class="dropdown-divider"></li>

                            <li><label>19.08.2018 15:35:16</label></li>

                        </ul>
                    </div>
                    <div class="modal-footer">
                        <p>GET YOUR RECEIPT</p>
                        <p class="footer-links">
                            <a href="" ><i class="icon_print"></i><span>Print</span></a>
                            <a href="" ><i class="icon_download"></i><span>Download</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!--./receipt modal-->

    </div>

@endsection
