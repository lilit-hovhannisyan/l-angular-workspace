@extends('clientarea.layout')

@section('title', 'favoritesSelected')

@section('content')

    <div class="container dashboard">
        <div class="top-nav">
            <!--top tabs line-->
            <div class="row row-justify">
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn home"><span>Dashboard</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn pay"><span>Pay</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn topup"><span>Top up</span></a>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a href="" class="link-btn transfer"><span>Transfer</span></a>
                </div>
            </div>
        </div>
        <!--./top tabs line-->

        <!--tabs content-->
        <div class="row mt-4 row-justify">
            <!--user activity side-->
            <div class="col-md-8 col-lg-9">

                <div class="form-group sr-group">
                    <input class="form-control search-input white-bg" type="text" placeholder="Search favorite" />
                </div>
                <div class="whitebox tab-block choose-payment mt-4 pb-0">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Groups</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <p class="border-dot" style="display: none;"><i class="icon_favorite"></i> Add your favorite payment from payment list</p>
                        <div class="selected-list ">
                            <span class="select-number pull-left"> 2</span>
                            <span class="pull-left">SELECTED</span>
                            <button type="button" class="btn btn-primary pull-left">Add to Group</button>

                            <i class="icon_delete pull-right"></i>
                            <span class="deselect pull-right">-</span>
                        </div>
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <ul class="tab-list">
                                <li class="current fav-list">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseTransfer" role="button" aria-expanded="false" aria-controls="collapseTransfer">
                                            <i class="icon_transfer pull-left"></i>
                                            <span class="pull-left service-name">Transfer between cards</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseTransfer">
                                        <div class="collapse-content">
                                            bla bla

                                        </div>
                                    </div>
                                </li>
                                <li class="current fav-list">
                                    <div class="activity-head">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseInternet" role="button" aria-expanded="false" aria-controls="collapseInternet">
                                            <i class="icon_internet pull-left"></i>
                                            <span class="pull-left service-name">Internet & TV payment</span>
                                            <i class="icon_arrow-right pull-right "></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseInternet">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                                <li class="current fav-list">
                                    <div class="activity-head">
                                        <a class=" collapsed" data-toggle="collapse" href="#collapseElectricy" role="button" aria-expanded="false" aria-controls="collapseElectricy">
                                            <i class="icon_utility pull-left"></i>
                                            <span class="pull-left service-name">Electricy payment</span>
                                            <i class="icon_arrow-right pull-right"></i>
                                            <span class="pull-right mr-5">1360025</span>
                                        </a>
                                    </div>
                                    <div class="collapse activity-body" id="collapseElectricy">
                                        <div class="collapse-content">
                                            bla bla
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    </div>
                </div>
            </div>

            <!--user info side-->
            <div class="col-md-4 col-lg-3">

                <div class="whitebox text-center">
                    <img src="{{ asset('assets/clientarea/images/payment.jpg') }}" class="w-75"  alt="Generic placeholder"/>
                    <p class="title-1 mb-0">PAY WITH PROFIT</p>
                    <p class="small">The amount of the cashback will be reflected in your profile in the UPay Bonus.</p>
                    <button type="button" class="btn btn-default pay" name="">Pay</button>
                </div>

                <div class=" whitebox mt-4 text-center">
                    <img src="{{ asset('assets/clientarea/images/transfert.png') }}" class="w-100"  alt="Generic placeholder"/>
                    <p class="title-2 mb-0">TRANSFER MONEY</p>
                    <p class="text-upper">safely and easily with guaranteed exchange rates & low fees!</p>
                    <button type="button" class="btn btn-default" name="">Transfer</button>
                </div>
            </div>
            <!--./user info side-->

        </div>
        <!--./tabs content-->
    </div>

@endsection