@extends('publicarea.layout')

@section('title', __('i18n::web.title.homepage'))

@section('content')

    @if($slides->count())
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @for($i = 0; $i < $slides->count(); $i++)
            <li data-target="#myCarousel" data-slide-to="{{ $i }}" @if ($i === 0) class="active" @endif></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @foreach($slides as $slide)
                <div class="carousel-item @if($loop->first) active @endif" style="background-image: url('{{ asset('storage/' . $slide->image) }}');">
                    <div class="container">
                        <div class="carousel-caption ">
                            <h1>
                                {{ $slide->title }}
                            </h1>
                            <div class="w-50">
                                {!! $slide->text !!}
                            </div>
                            @if($slide->url)
                            <p><a class="btn btn-primary big" href="{{ $slide->url }}">{{ $slide->label ? $slide->label : __('i18n::web.read_more') }}</a></p>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"><i class="icon_home-arrow-left"></i></span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"><i class="icon_home-arrow-right"></i></span>
        </a>
    </div>
    @endif


    <!-- Services box  ================================================== -->
    <div class="container-fluid services">
        <div class="container text-center">
            <h2>@lang('i18n::web.services.title')</h2>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6">
                    <div class="uwallet white-box uw-box">
                        <h3>@lang('i18n::web.services.1.title')</h3>
                        <p>@lang('i18n::web.services.1.intro')</p>
                        <a href="" class="read-more">@lang('i18n::web.read_more')</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6">
                    <div class="ubranch white-box uw-box">
                        <h3>@lang('i18n::web.services.1.title')</h3>
                        <p>@lang('i18n::web.services.1.intro')</p>
                        <a href="" class="read-more">@lang('i18n::web.read_more')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Use-pay box  ================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="use-upay">
                @lang('i18n::web.homepage.money_transfer')
                <button class="btn btn-primary big mt-3" type="button">@lang('i18n::web.enter')</button> </p>
            </div>
        </div>
    </div>

    <!-- Upay mobile app box  ================================================== -->
    <div class="container upeymobile hidden-xs">
        <div class="row">
            <div class="col-6">
                <ul class="up-mob">
                    <li class="card-title">
                        <h3>
                            @lang('i18n::web.homepage.advantages.title')
                        </h3>
                    </li>
                    <li class="current c-1">
                        <p class="card-subtitle">@lang('i18n::web.homepage.advantages.1.title')</p>
                        <p>@lang('i18n::web.homepage.advantages.1.intro')</p>
                    </li>
                    <li class="current c-2">
                        <p class="card-subtitle">@lang('i18n::web.homepage.advantages.2.title')</p>
                        <p>@lang('i18n::web.homepage.advantages.2.title')</p>
                    </li>
                    <li class="current c-3">
                        <p class="card-subtitle">@lang('i18n::web.homepage.advantages.3.title')</p>
                        <p>@lang('i18n::web.homepage.advantages.3.title')</p>
                    </li>
                    <li class="current c-4">
                        <p class="card-subtitle active">@lang('i18n::web.homepage.advantages.4.title')</p>
                        <p>@lang('i18n::web.homepage.advantages.4.intro')</p>
                    </li>
                </ul>
            </div>
            <div class="col-6">
                <div class="slider">SLIDER</div>
            </div>
        </div>
    </div>

    <!-- Upay applications box  ================================================== -->
    <div class="container-fluid upay-applications hidden-xs">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-md-8 col-lg-8">
                        <div class="w-85 mrg-auto">
                            <p>@lang('i18n::web.homepage.app_download')</p>
                            <p>
                                <button type="button" class="btn register"><i class="icon_appstore "></i></button>
                                <button type="button" class="btn register"><i class="icon_googleplay"></i></button>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-4 col-lg-4">
                        <div class="w-85 mrg-auto">
                            <p><button type="button" class="btn big register pull-right">@lang('i18n::web.register')</button> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- News box  ================================================== -->
    <div class="container news text-center">
        <h2>@lang('i18n::web.news.title')</h2>
        <div class="row justify-content-between ">
            @foreach($news as $n)
                <div class="col-xs-12 col-sm-4 col-lg-3 news-item">
                    <img src="{{ asset('storage/'.$n->image) }}" alt="{{ $n->title }}" >
                    <data value="">{{ $n->publish_date->format('d/m/Y') }}</data>
                    <p>{{ $n->intro_text }}</p>
                    <a href="{{ route('news.details', ['slug' => $n->slug]) }}" class="read-more pull-right">@lang('i18n::web.read_more')</a>
                </div><!-- /.col-lg-4 -->
            @endforeach
        </div><!-- /.row -->
    </div>

@endsection