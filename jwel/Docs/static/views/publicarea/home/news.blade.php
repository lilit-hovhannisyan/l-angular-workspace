@extends('publicarea.layout')

@section('title', 'News')

@section('content')

    <div class="container text-center container-pad">
        <h2>@lang('i18n::web.news.title')</h2>
        <div class="row row-redirect">
            @forelse($models as $model)
                <div class="col-4">
                    <div class="card" >
                        <img src="{{ asset('storage/'.$model->image) }}" alt="{{ $model->title }}" >
                        <div class="card-body">
                            <h4 class="card-title">{{ $model->title }}</h4>
                            <data value="">{{ $model->publish_date->format('d/m/Y') }}</data>
                            <p>{{ $model->intro_text }}</p>
                            <a href="{{ route('news.details', ['slug' => $model->slug]) }}" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12"><p>@lang('i18n::web.news.empty_message')</p></div>
            @endforelse
        </div>
        <div class="row justify-content-center">
            {!! $models->links() !!}
        </div>
    </div>

@endsection