@extends('publicarea.layout')

@section('title', $news->title)

@section('content')

    <div class="container text-center container-pad">
        <div class="row row-redirect">
            <div class="col-7 text-left">
                <h2 class="hidden-sm hidden-md hidden-lg mb-4">@lang('i18n::web.news.details.list_title')</h2>
                <div class="news-content ">
                    <h3>{{ $news->title }}</h3>
                    <div class="news-banner">
                        <img src="{{ asset('storage/'.$news->image) }}" />
                    </div>
                    <div class="clearfix">
                        <data value="" class="pull-left mt-2"><b>{{ $news->publish_date->format('d/m/Y') }}</b></data>
                        <div class="pull-right">
                            <i class="icon_fb soc-icon"></i>
                            <i class="icon_twitter soc-icon">
                            </i><i class="icon_G soc-icon"></i>
                        </div>
                    </div>
                    <div class="content-text">{!! $news->content !!}</div>
                </div>
            </div>
            <div class="col-5">
                <div class="news-list text-left">
                    <h2 class="hidden-xs">@lang('i18n::web.news.details.list_title')</h2>

                    @foreach($models as $model)
                        <div class="card" >
                            <img src="{{ asset('storage/'.$model->image) }}" alt="{{ $model->title }}" >
                            <div class="card-body">
                                <h4 class="card-title">{{ $model->title }}</h4>
                                <data value="">{{ $model->publish_date->format('d/m/Y') }}</data>
                                <p>{{ $model->intro_text }}</p>
                                <a href="{{ route('news.details', ['slug' => $model->slug]) }}" class="read-more pull-right">@lang('i18n::web.read_more')</a>
                            </div>
                        </div>
                        @if (!$loop->last || $loop->index < 3)
                            <div class="dropdown-divider"></div>
                        @endif
                        @if (($loop->count < 3 && $loop->last) || $loop->index == 2)
                            <div class="adds">
                                <img src="{{ asset('assets/images/adds.png') }}" alt="Generic placeholder image" >
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection