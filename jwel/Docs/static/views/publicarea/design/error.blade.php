@extends('publicarea.layout')

@section('title', 'error')

@section('content')

    <div class="container text-center">
        <div class="page-error">
            <div class="error-number">
                <p>404</p>
                <p>Oops, Sorry we can’t find that page!</p>
            </div>
            <div class="back-home">
                <button type="button" class="btn btn-primary w-25">Home</button>
            </div>
        </div>
    </div>

@endsection