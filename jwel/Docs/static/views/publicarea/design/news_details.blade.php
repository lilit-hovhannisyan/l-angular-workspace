@extends('publicarea.layout')

@section('title', 'News')

@section('content')
<div class="container-fluid gray-bg-15">
    <div class="container text-center ">
        <div class="row row-redirect">
            <div class="col-7 text-left">
                <h2 class="hidden-sm hidden-md hidden-lg mb-4">ՆՈՐՈՒԹՅՈՒՆՆԵՐ</h2>
                <div class="news-content ">
                    <h3>UCOM-Ի ՀՈՎԱՆԱՎՈՐՈՒԹՅԱՄԲ ԻՐԱԿԱՆԱՑՎՈՂ
                        ՌՈԲՈՏՆԵՐԻ 11-ՐԴ ԱՌԱՋՆՈՒԹՅՈՒՆԸ ԸՆԴՈՒՆՈՒՄ Է
                        ՄԱՍՆԱԿՑՈՒԹՅԱՆ ՀԱՅՏԵՐ</h3>
                    <div class="news-banner">
                        <img src="{{ asset('assets/images/online-payment-systems.jpg') }}" />
                    </div>
                    <div class="clearfix">
                        <data value="" class="pull-left mt-2"><b>12/06/2018</b></data>
                        <div class="pull-right">
                            <i class="icon_fb soc-icon"></i>
                            <i class="icon_twitter soc-icon">
                            </i><i class="icon_G soc-icon"></i>
                        </div>
                    </div>
                    <div class="content-text">
                        Մեկնարկում է Ռոբոտների հայաստանյան 11-րդ առաջնությունը, որի շրջանակում կմրցեն արգելքներ հաղթահարող և լաբիրինթոսից ելք գտնող ռոբոտները: Այս մասին այսօր լրագրողների հետ հանդիպմանը հայտարարեցին «ԱրմՌոբոտիքս» մրցույթի կազմակերպիչ՝ Ինֆորմացիոն տեխնոլոգիաների ձեռնարկությունների միության (ԻՏՁՄ) գործադիր տնօրեն Կարեն Վարդանյանը, մրցույթի գլխավոր հովանավոր՝ Ucom ընկերության տնօրեն Հայկ Եսայանը և հովանավոր՝ Վորլդ Վիժն Հայաստանի տնօրեն Ժիրայր Էդիլյանը:

                        «Ucom ընկերության կորպորատիվ սոցիալական պատասխանատվության ռազմավարության մեջ առանցքային նշանակություն ունեն այն բոլոր նախագծերը, որոնք կապված են ինժեներական մտածողության, ծրագրավորման և ռոբոտաշինության հմտությունների զարգացման հետ։ Մեր խորին համոզմամբ հենց այսպիսի պատանիները կկարողանան լրացնել աշխարհի մակարդակով ՏՀՏ ոլորտում 3մլն մասնագետների պահանջարկը», - նշեց Ucom ընկերության համահիմնադիր և գլխավոր տնօրեն Հայկ Եսայանը։

                        ԻՏՁՄ գործադիր տնօրեն Կարեն Վարդանյանը կարևորում է ռոբոտաշինության մրցույթների անցկացումը, քանի որ այն մի կողմից երեխաների մոտ զարգացնում է մրցակցային ոգին, մյուս կողմից նոր հմտությունների ձեռք բերման առիթ է հանդիսում, ինչի շնորհիվ նրանք միջազգային հարթակներում մրցունակ մասնագետներ են դառնում:

                        «Ռոբոտաշինութունն ու ավտոմացման համակարգերի մշակումը կարող է դառնալ մեր երկրում ՏՏ ոլորտի զարգացման հիմնաքարերից մեկը: Այս պատանիները վաղը փոխելու են մեր կյանքը: Վորլդ Վիժն Հայաստանը տարիներ շարունակ աջակցում է Հայաստանի մարզերում ռոբոտաշինության խմբակների աշխատանքին: Մենք համոզված ենք, որ գործող խմբակները բացառիկ հնարավորություն են տալու պատանիներին ապագայում ունենալ պահանջված աշխատանք ու արժանավայել կյանք»,- ասաց Վորլդ Վիժն Հայաստանի տնօրեն Ժիրայր Էդիլյանը:

                        Արգելքներ հաղթահարող և լաբիրինթոսից ելք գտնող ռոբոտների մրցույթի նպատակն է ներգրավել պատանիներին ռոբոտաշինության, ավտոմատացման ու տարբեր IoT (Internet of things) սարքերի ստեղծման գործընթացներում, խրախուսել վերջիններիս, ինչպես նաև ներկայացնել   հանրությանը ոլորտի զարգացման անհրաժեշտությունն ու համաշխարհային տեխնո տենդենցներին մոտենալու ճանապարհները:

                        Մրցույթին կարող են մասնակցել 10-18 տարեկան պատանիները՝ ներկայացնելով իրենց ռոբոտների նկարագիրն ու գաղափարները: Մրցույթի հայտերի ընդունման վերջնաժամկետը հուլիսի 11-ն է: Եզրափակիչ փուլը կանկացկացվի սեպտեմբեր ամսին: Մրցույթի տեխնիկական առաջադրանքներն ու կանոնակարգը ներկայացված են այստեղ, իսկ գրանցման հայտը հասանելի է այստեղ:

                        Մասնակիցները իրենց ստեղծած ռոբոտները կներկայացնեն ժյուրիի մասնագիտացված անդամներին, լավագույնները կարժանանան դրամական պարգևի Ucom-ի և Վորլդ Վիժն Հայաստանի կողմից:

                        «ԱրմՌոբոտիքս» ռոբոտաշինության զարգացման և աջակցման ծրագիրը Հայաստանում իրականացվում է 2008թ-ից՝ ԻՏՁՄ-ի կողմից:
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="news-list text-left">
                    <h2 class="hidden-xs">ՆՈՐՈՒԹՅՈՒՆՆԵՐ</h2>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="adds">
                        <img src="{{ asset('assets/images/adds.png') }}" alt="Generic placeholder image" >
                    </div>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="card" >
                        <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                        <div class="card-body">
                            <h4 class="card-title">News Title</h4>
                            <data value="">11/06/2018</data>
                            <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                            <a href="" class="read-more pull-right">read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection