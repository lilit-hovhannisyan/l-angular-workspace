@extends('publicarea.layout')

@section('title', 'Homepage')

{{--@section('custom-css')

@endsection

@section('custom-js')

@endsection--}}

@section('content')

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url('{{ asset('assets/images/slide1.png') }}');">
                {{--<img class="first-slide" src="{{ asset('assets/images/slide1.png') }}" alt="First slide" width="1920" height="720">--}}
                <div class="container">
                    <div class="carousel-caption ">
                        <h1>
                            <b>USE IT</b><br/>
                            ANYWHERE, ANYTIME
                        </h1>
                        <div class="w-50">
                        <p >U!Pay-ը  վիրտուալ դրամապանակ է, որն ընձեռում է առցանց վճարումներ և դրամական փոխանցումներ կատարելու հնարավորություն:</p>
                        </div>
                        <p><button class="btn btn-primary big" type="button">Read more</button></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('{{ asset('assets/images/slide1.png') }}');">
                {{--<img class="second-slide" src="{{ asset('assets/images/slide1.png') }}" alt="Second slide" width="1920" height="720">--}}
                <div class="container">
                    <div class="carousel-caption ">
                        <h1>
                            <b>USE IT</b><br/>
                            ANYWHERE, ANYTIME
                        </h1>
                        <div class="w-50">
                            <p >U!Pay-ը  վիրտուալ դրամապանակ է, որն ընձեռում է առցանց վճարումներ և դրամական փոխանցումներ կատարելու հնարավորություն:</p>
                        </div>
                        <p><button class="btn btn-primary big" type="button">Read more</button></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('{{ asset('assets/images/slide1.png') }}');">
                {{--<img class="third-slide" src="{{ asset('assets/images/slide1.png') }}" alt="Third slide" width="1920" height="720">--}}
                <div class="container">
                    <div class="carousel-caption ">
                        <h1>
                            <b>USE IT</b><br/>
                            ANYWHERE, ANYTIME
                        </h1>
                        <div class="w-50">
                            <p >U!Pay-ը  վիրտուալ դրամապանակ է, որն ընձեռում է առցանց վճարումներ և դրամական փոխանցումներ կատարելու հնարավորություն:</p>
                        </div>
                        <p><button class="btn btn-primary big" type="button">Read more</button></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"><i class="icon_home-arrow-left"></i></span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"><i class="icon_home-arrow-right"></i></span>
            </a>
    </div>


    <!-- Services box  ================================================== -->
    <div class="container-fluid services">
        <div class="container text-center">
            <h2>SERVICES</h2>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6">
                    <div class="uwallet white-box uw-box">
                        <h3>U WALLET</h3>
                        <p>is simply dummy text of the printing and typesetting industry. Text of the printing.</p>
                        <a href="" class="read-more">read more</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6">
                    <div class="ubranch white-box uw-box">
                        <h3>U WALLET</h3>
                        <p>is simply dummy text of the printing and typesetting industry. Text of the printing.</p>
                        <a href="" class="read-more">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Use-pay box  ================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="use-upay">
                <p>Use the <b>Upay mobile app</b> to easily<br/>
                    request money from around the world.<br/>
                <button class="btn btn-primary big mt-3" type="button">Enter</button> </p>
            </div>
        </div>
    </div>

    <!-- Upay mobile app box  ================================================== -->
    <div class="container upeymobile hidden-xs">
        <div class="row">
            <div class="col-6">
                <ul class="up-mob">
                    <li class="card-title"><h3>SAVE YOUR TIME BY USING <b>UPAY MOBILE APP</b></h3></li>
                    <li class="current c-1">
                        <p class="card-subtitle">PAY</p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of.</p>
                    </li>
                    <li class="current c-2">
                        <p class="card-subtitle">TOP up</p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of.</p>
                    </li>
                    <li class="current c-3">
                        <p class="card-subtitle">TOP up</p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of.</p>
                    </li>
                    <li class="current c-4">
                        <p class="card-subtitle active">Add your favorite </p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of.</p>
                    </li>
                </ul>
            </div>
            <div class="col-6">
                <div class="slider">SLIDER</div>
            </div>
        </div>
    </div>

    <!-- Upay applications box  ================================================== -->
    <div class="container-fluid upay-applications hidden-xs">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-md-8 col-lg-8">
                        <div class="w-85 mrg-auto">
                            <p>Ներբեռնե՛ք հավելվածն ու գրանցեք Ձեր U!Pay հաշիվն անվճար:</p>
                            <p>
                                <button type="button" class="btn register"><i class="icon_appstore "></i></button>
                                <button type="button" class="btn register"><i class="icon_googleplay"></i></button>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-4 col-lg-4">
                        <div class="w-85 mrg-auto">
                            <p><button type="button" class="btn big register pull-right">Register</button> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- News box  ================================================== -->
    <div class="container news text-center">
        <h2>NEWS</h2>
        <div class="row justify-content-between ">
            <div class="col-xs-12 col-sm-4 col-lg-3 news-item">
                <img src="{{ asset('assets/images/news1.jpg') }}" alt="Generic placeholder image" >
                <data value="">11/06/2018</data>
                <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                <a href="" class="read-more pull-right">read more</a>
            </div><!-- /.col-lg-4 -->
            <div class="col-xs-12 col-sm-4 col-lg-3 news-item">
                <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                <data value="">11/06/2018</data>
                <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                <a href="" class="read-more pull-right">read more</a>
            </div><!-- /.col-lg-4 -->
            <div class="col-xs-12 col-sm-4 col-lg-3 news-item">
                <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                <data value="">11/06/2018</data>
                <p>Ucom-ի բաժանորդների շնորհիվ կարախանյանների ընտանիքը տոնեց բնակարանամուտը:</p>
                <a href="" class="read-more pull-right">read more</a>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div>

@endsection