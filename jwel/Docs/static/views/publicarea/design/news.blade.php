@extends('publicarea.layout')

@section('title', 'News')

@section('content')
    <div class="container-fluid gray-bg-15">
    <div class="container text-center">
        <h2>News</h2>
        <div class="row row-redirect">
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" >
                    <img src="{{ asset('assets/images/new2.jpg') }}" alt="Generic placeholder image" >
                    <div class="card-body">
                        <h4 class="card-title">News Title</h4>
                        <data value="">11/06/2018</data>
                        <p>Ucom-ի եւ «Դասավանդի՛ր Հայաստան»-ի գործակցության շնորհիվ բագարանի դպրոցում կգործի «Արմաթ» ինժեներական լաբորատորիան</p>
                        <a href="" class="read-more pull-right">read more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <ul class="pagination" >
                <li class="page-item disabled"><a class="page-link" href="#"><i class="icon_arrow-left left"></i></a></li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#"><i class="icon_arrow-right right"></i></a></li>
            </ul>
        </div>
    </div>
    </div>
    <!--login modal-->

    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="logo-md">
                        <img src="{{ asset('assets/publicarea/images/Upay-logo-cl.svg') }}" alt="UPay">
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                    <ul>
                        <li>
                            <div class="form-group">
                                <label class="has-float-label">
                                    <input class="form-control" id="email-mobile" name="" placeholder="Email / Mobile" />
                                    <span>Email / Mobile</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label class="has-float-label">
                                    <input class="form-control" id="password" name="" placeholder="Password" />
                                    <span>Password</span>
                                </label>
                            </div>
                        </li>

                        <li><p class="fnt-12 ml-2"><b>Forgot password?</b></p> </li>
                        <li class="text-center">
                            <div class="form-group mt-5">
                                <button type="button" class="btn btn-primary w-75">SIGN IN</button>
                            </div>
                        </li>
                        <li class="text-center ">
                            <p class="fnt-12">By signing in, you are agreeing to the<br/> Privacy Policy and Term of Service.</p>
                        </li>
                    </ul>
                    </form>
                </div>
                <div class="modal-footer text-center">
                    <p class="fnt-14">Don't have an account yet? <a href="">Sign Up</a></p>
                </div>
            </div>
        </div>
    </div>

    <!--./login modal-->

@endsection