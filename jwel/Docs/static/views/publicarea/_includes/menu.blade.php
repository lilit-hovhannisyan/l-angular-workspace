<nav class="navbar navbar-expand-md static-psn">
    <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <i class="icon_menu"></i>
        <i class="icon_close"></i>
    </button>
    <a class="navbar-brand order-2" href="{{ route('home') }}">
        <img src="{{ asset('assets/images/Upay-logo-cl.svg') }}" alt="UPay">
    </a>
    <div class="right-menu order-4">
        <!--search box-->
        <form class="form-inline mt-2 mt-md-0" style="flex: 0 1 55px;">
            <div class="dropdown static-psn">
                <a class="dropdown-toggle aft-none" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="icon_search lens"></i>
                </a>
                <div class="search-box dropdown-menu w-100" aria-labelledby="searchDropdown">
                    <a href="javascript:void(0);" class="hidden-sm hidden-md hidden-lg hidden-xl"><i class="icon_close"></i></a>
                    <input class="form-control search-input" type="text" placeholder="Site Search..." aria-label="Search">
                </div>
            </div>
        </form>
        <!--languages box--->
        <div class="lang dropdown hidden-xs" style="flex: 0 1 70px;">
            <a class="dropdown-toggle aft-none" href="#" id="langDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>{{ languages(true)[locale()] ?? locale() }}</span><i class="icon_arrow-down"></i>
            </a>
            <div class="lang-box dropdown-menu" aria-labelledby="langDropdown">
                @foreach(languages(true) as $code => $name)
                    <a href="{{ route(request()->route()->getName(), ['locale' => $code]) }}" class="lang-box-item @if($code === locale()) active @endif">{{ $name }}</a>
                    @if(!$loop->last)
                    <div class="dropdown-divider"></div>
                    @endif
                @endforeach
            </div>
        </div>
        <!--login-->
        <button type="button" class="btn btn-primary hidden-xs hidden-sm" data-toggle="modal" data-target="#loginModal">@lang('i18n::web.login')</button>
        <a href="javascript:void(0);" class="hidden-md hidden-lg hidden-xl"  style="flex: 0 1 35px;"><i class="icon_user"></i></a>
        <!--register-->
        <button type="button" class="btn hidden-xs hidden-sm register">@lang('i18n::web.register')</button>
        <a href="javascript:void(0);" class="hidden-xs hidden-md hidden-lg hidden-xl"  style="flex: 0 1 35px;"><i class="icon_user"></i></a>
    </div>
    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item hidden-sm hidden-md hidden-lg">
                <div class="lang-box">
                    @foreach(languages(true) as $code => $name)
                        <a href="{{ route(request()->route()->getName(), ['locale' => $code]) }}" class="lang-box-item @if($code === locale()) active @endif">{{ $name }}</a>
                    @endforeach
                </div>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item active dropdown static-psn">
                <a class="nav-link dropdown-toggle aft-none active" href="#" id="generalDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    General
                </a>
                <div class="dropdown-menu w-100 bg-light shad-box" aria-labelledby="generalDropdown">
                    <div class="container cont-size">
                        <a class="dropdown-item bullet" href="#">Ընկերության մասին</a>
                        <a class="dropdown-item bullet" href="#">Կառուցվածք</a>
                        <a class="dropdown-item bullet" href="#">Հաշվետվություններ</a>
                        <a class="dropdown-item bullet" href="#">Բաժնետերեր և ղեկավարներ</a>
                        <a class="dropdown-item bullet" href="#">Լիցենզիա </a>
                        <a class="dropdown-item bullet" href="#">Ներքին փաստաթղթեր</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">U Wallet</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">U Branch</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Contacts</a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item hidden-sm hidden-md hidden-lg hidden-xl text-center">
                <button type="button" class="btn register">Register</button>
            </li>
        </ul>
    </div>
</nav>


