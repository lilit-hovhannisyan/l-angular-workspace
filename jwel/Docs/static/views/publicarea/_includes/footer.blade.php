<div class="row">
    <div class="container">
        {{--<p class="float-right"><a href="#">Back to top</a></p>--}}
        <div class="row footer-flx">
            <ul class="order-2">
                <li class="title">About</li>
                <li><a href="">About us</a></li>
                <li><a href="">Contact us</a></li>
                <li><a href="">General terms and<br/> Condition</a></li>
                <li><a href="">Responsible Gaming</a></li>
                <li><a href="">Privacy policy</a></li>
            </ul>
            <ul class="order-3">
                <li class="title">Help</li>
                <li><a href="">FAQs</a></li>
                <li><a href="">Pay</a></li>
                <li><a href="">Transfer</a></li>
                <li><a href="">Top Up</a></li>
                <li><a href="">Pay by QR</a></li>
            </ul>
            <ul class="order-4">
                <li class="title">Promotion</li>
                <li><a href="">Bonus</a></li>
                <li><a href="">Cashback</a></li>
                <li><a href="">Accumulator Bonus</a></li>
            </ul>
            <ul class="order-5">
                <li class="title">Products</li>
                <li><a href="">FAQs</a></li>
                <li><a href="">Affiliate Programs</a></li>
            </ul>
            <ul class="order-6">
                <li>
                     Զանգահարեք մեզ<br/>
                    <span class="text-success">444, 011 444 444</span>
                    <span><button type="button" class="btn btn-primary ml-4">FEEDBACK</button></span>
                </li>
                <li class="dropdown-divider"></li>
                <li>
                    <p>Ներբեռնել դրամապանակ</p>
                    <a href="" class="app-icon"><i class="icon_appstore "></i></a>
                    <a href="" class="app-icon"><i class="icon_googleplay"></i></a>
                </li>
            </ul>
            <div class="row justify-content-center hidden-sm hidden-md hidden-lg order-7">
                <div class="text-center">
                    <i class="icon_fb soc-icon"></i>
                    <i class="icon_twitter soc-icon"></i>
                    <i class="icon_G soc-icon"></i>
                </div>
            </div>
        </div>
        <div class="row justify-content-center hidden-xs">
            <div class="text-center">
                <i class="icon_fb soc-icon"></i><i class="icon_twitter soc-icon"></i><i class="icon_G soc-icon"></i>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="footer-bottom">
        <p><span>© 2018</span><br/>
            Powered by <span>IUnetworks.</span> All rights reserved . <span>License information.</span>
        </p>
    </div>
</div>