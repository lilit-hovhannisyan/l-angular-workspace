var http = require('http');
var fs = require('fs');

var path = '/Claudia/origin/feature/EWT-1089-back-end-upay-wallet-cashout-add-customer-data-in-operator-logs/js/';
var currentVersion = 1;

var varsionsUrldata = {
    host: '10.180.8.78',
    port: '8086',
    path: path,
    method: 'GET'
};

var updateVersion = (response) => {
  var data = '';
  response.on('data', function(chunk) {
    data += chunk;
  });

  response.on('end', () => {
    fs.writeFile('projects/upay-lib/src/lib/api/index.ts', data, (err) => {
        if (!err){
          console.log('The file has been saved! Version: ' + currentVersion);
        } else {
          console.log('OOPS, error');
        }
    });
  })

}

var versionSuccess = (response) => {
  var data = '';
  response.on('data', function(chunk) {
      data += chunk;
  });

  response.on('end', () => {
    var versions = data.substring(data.indexOf('Parent Directory</a></li>'));
    versions = versions.replace(/\D+/g, ' ').split(' ');
    currentVersion = Math.max(...versions);

    var currentVarsionsUrldata = {
      host: '10.180.8.78',
      port: '8086',
      path: `${path}${currentVersion}/upay.ts`,
      method: 'GET'
    };
    http.request(currentVarsionsUrldata, updateVersion).end();
  });
 }

http.request(varsionsUrldata, versionSuccess).end();
