# U Pay Web Workspace
 
# U Pay Client App
## Development
  - Run `npm run update-libs`
  - Run `npm run start:client`
  - Open [http://localhost:4200/](http://localhost:4200/)

# U Pay Operator App
## Development
  - Run `npm run update-libs`
  - Run `npm run start:operator`
  - Open [http://localhost:4201/](http://localhost:4201/)
## Deployment
### Pre Live
  - Run `npm run prelive:build:operator`
### Production
  - Run `npm run prod:build:operator`
  
# U Pay Accountant App
## Development
  - Run `npm run update-libs`
  - Run `npm run start:accountant`
  - Open [http://localhost:4202/](http://localhost:4201/)
## Deployment
### Pre Live
  - Run `npm run prelive:build:accountant`
### Production
  - Run `npm run prod:build:accountant`

# U Pay Lib
## Development
  - Shared code between u pay apps. 

# Iu Ui Lib
## Development
  - Based on [atomic design methodology](http://atomicdesign.bradfrost.com/chapter-2/)
  - Use [Angular Material](https://material.angular.io/)
  - Every time if you add or change some code you need  run `npm run update-libs` 
