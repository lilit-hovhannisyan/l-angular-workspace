import {moleculeStory} from '../ui-molecules.stories';
import {UiCountryPickerComponent} from './ui-country-picker.component';

moleculeStory.add('Country Picker', () => ({
  component: UiCountryPickerComponent,
  props: {},
}));
