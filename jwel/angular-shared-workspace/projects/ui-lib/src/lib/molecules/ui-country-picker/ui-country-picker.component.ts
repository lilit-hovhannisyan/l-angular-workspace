import {Component, EventEmitter, Input, Output} from '@angular/core';
import countries from 'world-countries';

@Component({
  selector: 'ui-country-picker',
  templateUrl: './ui-country-picker.component.html',
  styleUrls: ['./ui-country-picker.component.less'],
})
export class UiCountryPickerComponent {

  countryData = countries;
  debugger;
}
