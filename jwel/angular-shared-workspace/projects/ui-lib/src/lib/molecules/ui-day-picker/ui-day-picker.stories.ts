import { moleculeStory } from '../ui-molecules.stories';
import {UiDayPickerComponent} from './ui-day-picker.component';

moleculeStory.add('Day Picker', () => ({
  component: UiDayPickerComponent,
  props: {
    startYear: 1930,
    endYear: 2100,
  },
}));
