import { Component, EventEmitter, Input, Output } from '@angular/core';

const  isLeapYear = (year) => {
  return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
};

const dayStr = (day) => {
  if (day < 10) {
    return  '0' + day;
  } else {
    return day;
  }
};

const createMonthsData =  () => {
  return [
    {value: 0, label: '01'},
    {value: 1, label: '02'},
    {value: 2, label: '03'},
    {value: 3, label: '04'},
    {value: 4, label: '05'},
    {value: 5, label: '06'},
    {value: 6, label: '07'},
    {value: 7, label: '08'},
    {value: 8, label: '09'},
    {value: 9, label: '10'},
    {value: 10, label: '11'},
    {value: 11, label: '12'}
  ];
};

const createYearsData = (yearFrom, yearTo) => {
  yearFrom = +yearFrom;
  yearTo = +yearTo;
  const yearsData = [];

  for (; yearFrom <= yearTo; ) {
    yearsData.push({
      label: yearTo + '',
      value: yearTo--
    });
  }
  return yearsData;
};

const createDaysData = (month, year) => {
  const daysData = [];
  let dayCount;

  if (1 === month) {
    dayCount = 28;
    if (isLeapYear(year)) {
      dayCount++;
    }
  }
  if (3 === month || 5 === month  || 8 === month  ||  10 === month) {
    dayCount = 30;
  }
  if (0 === month || 2 === month || 4 === month || 6 === month || 7 === month || 9 === month || 11 === month) {
    dayCount = 31;
  }
  for (let i = 1; i <= dayCount; i++) {
    daysData.push({
      value: i,
      label: dayStr(i)
    });
  }
  return daysData;
};

@Component({
  selector: 'ui-day-picker',
  templateUrl: './ui-day-picker.component.html',
  styleUrls: ['./ui-day-picker.component.less'],
})
export class UiDayPickerComponent {
    @Input() startYear: number; // = 1930;
    @Input() endYear: number; // = 2100;

    yearsData = createYearsData(this.startYear, this.endYear);
    monthsData = createMonthsData();
    daysData = createDaysData( 0, 2019);
}
