import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { UiCountryPickerComponent } from './ui-country-picker/ui-country-picker.component';
import { UiAtomsModule } from '../atoms/ui-atoms.module';
import { UiDayPickerComponent } from './ui-day-picker/ui-day-picker.component';


@NgModule({
  declarations: [
    UiCountryPickerComponent,
    UiDayPickerComponent
  ],
  imports: [
    CommonModule,
    UiAtomsModule
  ],
  exports: [
    UiCountryPickerComponent,
    UiDayPickerComponent
  ]
})
export class UiMoleculesModule { }
