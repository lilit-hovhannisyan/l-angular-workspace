import {storiesOf, moduleMetadata} from '@storybook/angular';
import {UiAtomsModule} from '../atoms/ui-atoms.module';

export const moleculeStory = storiesOf('IU UI/Molecules', module)
  .addDecorator(
    moduleMetadata({
      imports: [UiAtomsModule],
      providers: [],
    }));
