import {Component, Input, Output, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';


export type sortDirection = 'asc' | 'desc';
export type filterComparision = 'startsWith' | 'endWith';
export type filterComponent = 'stringInput' | 'numberInput' | 'datePicker' | 'select';

export interface IPage {
  index: number;
  size: number;
}

export interface ISort {
  by: string;
  direction: sortDirection;
}

export interface IFilter {
  by: string;
  comparison: filterComparision;
  value: any;
}

export interface IColumnFilterConfig {
  component: filterComponent;
  comparisons: filterComparision[];
}

export  interface IQueryParams {
  page: IPage;
  sort?: ISort;
  filterCombinationsGroup?: IFilter[][];
}


export interface IColumnConfig {
  displayHeaderKay: string;
  valueKey: string;
  filter?: IColumnFilterConfig;
}

@Component({
  selector: 'ui-data-table',
  templateUrl: './ui-data-table.component.html'
})
export class UiDataTableComponent implements OnInit {
  @Input() dataSource: any[];
  @Input() columnsConfig: IColumnConfig[];
  @Input() pageSizeOptions: number[] = [10, 20, 30, 40, 50];
  @Input() selectedPageSize = 10;
  @Input() totalData = 0;
  @Output() getTableDataSource = new EventEmitter<IQueryParams>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort; // {static: true}

  displayedColumns = [];
  pageParams: IPage;
  sortParams: ISort;
  filterCombinationsGroup: IFilter[][] = [[]]; // todo hetagayi hamar e [[]] sarqac vor karoxananq bazmutyunneri miavorum anenq
  activeColumnFilterKey: string;

  ngOnInit(): void {
    this.setDisplayedColumns();
    this.setPageParams(0, this.selectedPageSize);
    this.onUpdateTableDataSource();
  }

  setPageParams(index, size) {
    this.pageParams = {
      index,
      size
    };
  }

  setSortParams(by, direction) {
    this.sortParams = {
      by,
      direction
    };
  }

  setFilterParams(by, comparison, value, filterGroupIndex = 0) {
    const selectedFilterGroup = this.filterCombinationsGroup[filterGroupIndex];
    const filterByKey = selectedFilterGroup.find(filterData => {
      return filterData.by === by;
    });

    //todo sarqel vor add edit karoxananq anenq
    // filtersByKeyArr.map()
    if (filterByKey) {
      filterByKey.by = by;
      filterByKey.comparison = comparison;
      filterByKey.value = value;
    } else {
      selectedFilterGroup.push({
        by,
        comparison,
        value
      });
    }

    // selectedFilterGroup.map(filterData => {
    //   if (filterData.by === by) {
    //     if(filterData.comparison === comparison)
    //   }
    // });
    // todo end
  }

  onSetActiveColumnFilterKey(event, columnConfig: IColumnConfig) {
    event.stopPropagation();
    if (this.activeColumnFilterKey === columnConfig.valueKey) {
      this.activeColumnFilterKey = '';
    } else {
      this.activeColumnFilterKey = columnConfig.valueKey;
    }
  }

  setDisplayedColumns() {
    this.displayedColumns = this.columnsConfig.map((columnConfig: IColumnConfig) => {
      return columnConfig.valueKey;
    });
  }

  getQueryParams(): IQueryParams {
    return {
      page: this.pageParams,
      sort: this.sortParams,
      filterCombinationsGroup: this.filterCombinationsGroup,
    };
  }

  onUpdateTableDataSource() {
    this.getTableDataSource.emit(this.getQueryParams());
  }

  onPageParamsChange(pageParams) {
    this.setPageParams(pageParams.pageIndex, pageParams.pageSize);
    this.onUpdateTableDataSource();
  }

  onSortParamsChange(sortParams) {
    this.setSortParams(sortParams.active, sortParams.direction);
    this.onUpdateTableDataSource();
  }

  onFilterParamsChange(filterParams: IFilter) {
    this.setFilterParams(filterParams.by, filterParams.comparison, filterParams.value);
    this.onUpdateTableDataSource();
    console.log(filterParams);
  }
}
