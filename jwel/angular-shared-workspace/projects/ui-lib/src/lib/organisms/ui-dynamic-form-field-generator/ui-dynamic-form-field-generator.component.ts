import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-dynamic-form-field-generator',
  templateUrl: './ui-dynamic-form-field-generator.component.html',
  styleUrls: ['./ui-dynamic-form-field-generator.component.less'],
})
export class UiDynamicFormFieldGeneratorComponent {
  // @Input() text: string;
  // @Input() type: 'contained' | 'text' | 'outlined' = 'contained';
  // @Input() disabled: boolean = false;
  // @Output() onClick = new EventEmitter<void>();

  @Input() fieldConfig: any;
}
