import { storiesOf } from '@storybook/angular';
import {UiDynamicFormComponent} from '../ui-dynamic-form/ui-dynamic-form.component';
// import { UiDynamicFormComponent } from './ui-dynamic-form.component';

storiesOf('IU UI/Organisms/Dynamic Form', module)
  .add('Contained Button', () => ({
    component: UiDynamicFormComponent,
    props: {
      fieldsConfig: [{
        component: 'input',
        name: 'username',
        label: 'User name',
        validationRules: [
          {
            type: 'required',
            message: 'User name is required'
          }
        ]
      },
        {
          component: 'input',
          name: 'username',
          label: 'User name',
          validationRules: [
            {
              type: 'required',
              message: 'User name is required'
            }
          ]
        }],

    },
  }));
