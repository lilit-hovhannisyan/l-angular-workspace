
export const requiredValidator  = (value: any) => {
  return (value === null || value === undefined || value.toString().trim() === '' || value === false);
};

export const patternValidator = (value: string, ruleConf: {pattern: string, flags?: string}) => {
  let { pattern, flags } = ruleConf;
  pattern = decodeURIComponent(pattern);
  flags = flags || '';
  const regExp = new RegExp(pattern, flags);
  return regExp.test(value);
};

export const mailValidator = (value) => {
  const ruleConfig = {
    pattern: '^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}([.](([a-zA-Z0-9]){2,63})+)+$'
  };
  return patternValidator(value, ruleConfig);
};



