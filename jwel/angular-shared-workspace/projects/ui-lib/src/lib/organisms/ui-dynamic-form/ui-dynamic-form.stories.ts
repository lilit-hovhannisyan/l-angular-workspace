import { storiesOf } from '@storybook/angular';
import { UiDynamicFormComponent } from './ui-dynamic-form.component';
import {UiOrganismsModule} from '../ui-organisms.module';

storiesOf('IU UI/Organisms/Dynamic Form', module)
  .add('Contained Button', () => ({
    component: UiDynamicFormComponent,
    imports: [ UiOrganismsModule ],
    // providers: [ MyExampleService ]
    props: {
      fieldsConfig: [{
        component: 'input',
        name: 'username',
        label: 'User name',
        validationRules: [
          {
            type: 'required',
            message: 'User name is required'
          }
        ]
      }],

    },
  }));
