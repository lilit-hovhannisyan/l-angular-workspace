import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-dynamic-form',
  templateUrl: './ui-dynamic-form.component.html',
  styleUrls: ['./ui-dynamic-form.component.less'],
})
export class UiDynamicFormComponent {
  // @Input() text: string;
  // @Input() type: 'contained' | 'text' | 'outlined' = 'contained';
  // @Input() disabled: boolean = false;
  // @Output() onClick = new EventEmitter<void>();

  @Input() fieldsConfig: [];
}
