import { NgModule } from '@angular/core';

import { UiDynamicFormComponent } from './ui-dynamic-form/ui-dynamic-form.component';
import {CommonModule} from '@angular/common';
import {UiAtomsModule} from '../atoms/ui-atoms.module';
import {UiMoleculesModule} from '../molecules/ui-molecules.module';
import {UiDynamicFormFieldGeneratorComponent} from './ui-dynamic-form-field-generator/ui-dynamic-form-field-generator.component';

import {MatTableModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';

import {UiDataTableComponent} from './ui-data-table/ui-data-table.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    UiDynamicFormFieldGeneratorComponent,
    UiDynamicFormComponent,
    UiDataTableComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,

    NoopAnimationsModule,
    UiAtomsModule,
    UiMoleculesModule,
  ],
  exports: [
    UiDynamicFormComponent,
    UiDynamicFormFieldGeneratorComponent,
    UiDataTableComponent
  ]
})
export class UiOrganismsModule { }
