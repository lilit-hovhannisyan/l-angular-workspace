import { NgModule } from '@angular/core';

import { UiAtomsModule } from './atoms/ui-atoms.module';
import { UiOrganismsModule } from './organisms/ui-organisms.module';
import { UiMoleculesModule } from './molecules/ui-molecules.module';

@NgModule({
  declarations: [],
  imports: [
    UiAtomsModule,
    UiMoleculesModule,
    UiOrganismsModule
  ],
  exports: [
    UiAtomsModule,
    UiMoleculesModule,
    UiOrganismsModule
  ]
})
export class UiLibModule { }
