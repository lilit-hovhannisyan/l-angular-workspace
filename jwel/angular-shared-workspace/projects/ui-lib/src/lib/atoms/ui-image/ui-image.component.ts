import { Component, Input } from '@angular/core';

@Component ({
  selector: 'ui-image',
  templateUrl: './ui-image.component.html',
  styleUrls: ['./ui-image.component.less']
})

export class UiImageComponent {
  @Input() src: string;
  @Input() alt = 'image';
  @Input() width;
  @Input() height;

  getProportion () {
      if ( this.width < this.height ) {
        return 'heightStreach';
      } else {
        return 'widthStreach';
      }
  }
}
