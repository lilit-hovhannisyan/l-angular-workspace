import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component ({
  selector: 'ui-button-image',
  templateUrl: './ui-button-image.component.html',
  styleUrls: ['./ui-button-image.component.less']
})

export class UiButtonImageComponent {

  @Input() text: string;
  @Input() src: string;

  @Output() onClick = new EventEmitter<any>();

  click(e) {
    this.onClick.emit(e);
  }

}
