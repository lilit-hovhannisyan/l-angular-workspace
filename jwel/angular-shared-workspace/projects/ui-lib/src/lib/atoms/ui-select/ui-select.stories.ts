import { storiesOf } from '@storybook/angular';
import { UiSelectComponent } from './ui-select.component';

storiesOf('IU UI/Atoms', module)
  .add('Select', () => ({
    component: UiSelectComponent,
    props: {
      valueKey: 'id',
      viewKey: 'name',
      options: [
        {
          name: 'BMW',
          id: 1,
        },
        {
          name: 'Mercedes',
          id: 2,
        },
        {
          name: 'Audi',
          id: 3,
        }
      ]
    },
  }));

