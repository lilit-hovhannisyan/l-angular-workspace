import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';

////todo move to shared code
function deepFind(obj, path) {
  const paths = path.split('.');
  let current = obj;
  let i;

  for (i = 0; i < paths.length; ++i) {
    if (current[paths[i]] === undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;
}
////

@Component({
  selector: 'ui-select',
  templateUrl: './ui-select.component.html',
  styleUrls: ['./ui-select.component.less'],
})
export class UiSelectComponent implements OnInit, OnDestroy {

  @Input() options: any[];
  @Input() value: any;
  @Input() valueKey = 'value';
  @Input() viewKey = 'name';
  @Input() placeholder: string;
  @Input() getOptions: Function;

  @Output() onChange = new EventEmitter<any>();

  optionsSub =  Subscription.EMPTY;

  ngOnInit(): void {
    if (this.getOptions) {
      this.optionsSub = this.getOptions().subscribe(data => {
        this.options = data;
        this.setValue(this.value);
        this.optionsSub.unsubscribe()
      });
    } else {
      this.setValue(this.value);
    }
  }

  ngOnDestroy(): void {
    this.optionsSub.unsubscribe();
  }

  change(e) {
    this.setValue(e.target.value);
    this.onChange.emit(this.value);
  }

  setValue(value){
    debugger
    if(value == undefined) {
      this.value = '';
    } else {
      this.value = value;
    }
  }

  getValueKey(data) {
    return deepFind(data, this.valueKey);
  }

  getViewKey(data) {
    return deepFind(data, this.viewKey);
  }
}
