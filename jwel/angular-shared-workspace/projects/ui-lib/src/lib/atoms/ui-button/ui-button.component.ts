import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: './ui-button.component.html',
  styleUrls: ['./ui-button.component.less'],
})
export class UiButtonComponent {

  @Input() text: string;
  @Input() disabled: boolean = false;
  @Output() onClick = new EventEmitter<void>();

  click(e) {
    this.onClick.emit();
  }

}
