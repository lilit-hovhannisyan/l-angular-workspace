import { storiesOf } from '@storybook/angular';
import { UiButtonComponent } from './ui-button.component';

storiesOf('IU UI/Atoms/Buttons', module)
  .add('Contained Button', () => ({
    component: UiButtonComponent,
    props: {
      text: 'Contained Button',
      disabled: true
    },
  }))
  .add('Text Button', () => ({
    component: UiButtonComponent,
    props: {
      text: 'Text Button',
      type: 'text'
    },
  }))
  .add('Outlined Button', () => ({
    component: UiButtonComponent,
    props: {
      text: 'Ok',
      type: 'outlined'
    },
  }));
