import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiButtonComponent } from './ui-button/ui-button.component';
import { UiSelectComponent } from './ui-select/ui-select.component';
import { UiImageComponent } from './ui-image/ui-image.component';
import { UiButtonImageComponent } from './ui-button-image/ui-button-image.component';
import { UiInputComponent } from './ui-input/ui-input.component';

@NgModule({
  declarations: [
    UiButtonComponent,
    UiInputComponent,
    UiSelectComponent,
    UiImageComponent,
    UiButtonImageComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    UiButtonComponent,
    UiInputComponent,
    UiSelectComponent,
    UiImageComponent,
    UiButtonImageComponent,
  ]
})
export class UiAtomsModule { }
