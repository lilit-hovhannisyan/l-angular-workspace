import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ui-input',
  templateUrl: './ui-input.component.html',
  styleUrls: ['./ui-input.component.less']
})
export class UiInputComponent implements OnInit {
  @Input() placeholder? : string;
  @Input() type = 'text';
  @Input() value?: any;

  @Output() onChange = new EventEmitter< string | number >();
  @Output() onClickEnter = new EventEmitter<void>();

  ngOnInit(): void {
    this.setValue(this.value);
  }

  setValue (value) {
    if(value === undefined) {
      this.value = null;
    }
  }

  change(e) {
    this.onChange.emit(e.target.value);
  }

  clickEnter(e) {
    if (e.keyCode === 13) {
      this.onClickEnter.emit();
    }
  }

}
