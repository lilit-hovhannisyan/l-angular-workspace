import { TestBed } from '@angular/core/testing';

import { UiLibService } from './ui-lib.service';

describe('UiLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UiLibService = TestBed.get(UiLibService);
    expect(service).toBeTruthy();
  });
});
