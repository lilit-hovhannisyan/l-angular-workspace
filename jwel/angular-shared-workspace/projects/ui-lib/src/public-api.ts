/*
 * Public API Surface of ui-lib
 */

// export * from './lib/ui-lib.service';
// export * from './lib/ui-lib.component';
export * from './lib/ui-lib.module';

export * from './lib/atoms/ui-atoms.module';
export * from './lib/molecules/ui-molecules.module';
export * from './lib/organisms/ui-organisms.module';

