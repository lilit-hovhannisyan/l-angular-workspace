import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {UiMoleculesModule} from '../../../ui-lib/src/lib/molecules/ui-molecules.module';
import {UiOrganismsModule} from '../../../ui-lib/src/lib/organisms/ui-organisms.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UiMoleculesModule,
    UiOrganismsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
