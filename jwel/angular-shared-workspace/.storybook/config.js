/* eslint-disable global-require */
import { configure, addParameters, addDecorator } from '@storybook/angular';
// import { withA11y } from '@storybook/addon-a11y';
// import addCssWarning from '../src/cssWarning';
//
// addDecorator(withA11y);
// addCssWarning();

addParameters({
  options: {
    hierarchyRootSeparator: /\|/,
  },
});

function loadStories() {

  // automatically import all story ts files that end with *.stories.ts from projects
  const req = require.context('../projects', true, /\.stories\.ts$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
