import {Component, Input} from '@angular/core';

@Component({
  selector: 'lavash-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.less']
})
export class LinkComponent {
  @Input() routingLink: boolean;

  @Input() href: string;
  @Input() text: string;
  @Input() target: string;
  @Input() icon: string = null;
}
