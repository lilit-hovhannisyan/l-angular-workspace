import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lavash-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.less']
})

export class CheckboxComponent {
  @Input() icon: string;
  @Input() option: CheckboxOption = {
    key: 'no-key',
    value: '',
    selected: false
  };

  @Output() selectOption = new EventEmitter<CheckboxOption>();

  onSelectOption() {
    this.option.selected = !this.option.selected;
    this.selectOption.emit(this.option);
  }
}

export interface CheckboxOption {
  key: string;
  value: string;
  selected?: boolean;
}
