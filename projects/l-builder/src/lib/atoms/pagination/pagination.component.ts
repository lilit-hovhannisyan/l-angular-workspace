import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lavash-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent implements OnInit {
  @Input() forceStartPageIcon?: string;
  @Input() previousIcon?: string;
  @Input() nextIcon: string;
  @Input() forceEndPageIcon: string;

  @Input() totalItems;
  @Input() itemsPerPage;

  @Output() pageChange = new EventEmitter<number>();

  paginationButtonList;
  @Input() basePage = 1;
  @Input() currentPage;

  @Input() renderPaginationButtonsCount = 0;
  buttonsMin;
  buttonsMax;

  ngOnInit() {
    this.paginationButtonList = numberToNumericList(this.totalItems / this.itemsPerPage);
    this.currentPage = this.basePage;
    this.getActiveRangeOfRenderedButtons(this.currentPage);
  }

  onPageChange(page) {
    this.currentPage = page;
    this.getActiveRangeOfRenderedButtons(page);
    this.pageChange.emit(page);
  }

  getActiveRangeOfRenderedButtons(page) {
    if (!this.renderPaginationButtonsCount) {
      return;
    }

    if (page === this.buttonsMax) {
        this.basePage = page;
    }

    if (page === this.buttonsMin) {
        this.basePage = page - this.renderPaginationButtonsCount + 1;
    }

    if (page ===  1 ) {
        this.basePage = 1;
    }
    if (page ===  this.paginationButtonList.length ) {
        this.basePage = this.paginationButtonList.length - this.renderPaginationButtonsCount + 1;
    }
    this.buttonsMin = this.basePage;
    this.buttonsMax = this.basePage + this.renderPaginationButtonsCount - 1;
  }
 }

export const numberToArray = (number) => {
  return number.toString().split('');
};

export const numberToNumericList = (number, startNumber = 1) => {
  const result = [];
  while (number > 0) {
    result.push(startNumber);
    startNumber++;
    number--;
  }
  return result;
};
