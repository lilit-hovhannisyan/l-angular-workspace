import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Option} from '../atoms.interfaces';
import {optionList} from '../atoms.testData';

@Component({
  selector: 'lavash-select-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.less']
})

export class SelectAutocompleteComponent implements OnInit {
  // select !!readonly input
  @Input() selectionPlaceholder = 'Select';
  @Input() selectedOptionsList: Option[] = [];
  @Input() selectedOptionsRenderString: string;

  // options :: dropdown
  @Input() selectOptions: Option[] = optionList;

  // autocomplete
  @Input() hasAutocomplete = false;
  @Input() autocompletePlaceholder: string;
  autocompleteValue: string;
  @Input() autocompleteErrMsg = 'No record is found';
  @Input() autocompleteIconClass: string; // todo

  // manipulate data
  optionsVisible = false;
  private autocompleteOptions = [];


  // *** todo \fucking\ bug with autocomplete delete btn, it behaves as outside click, so i have to fake it
  private autocompleteDeleteBtnFaking;

  // miltiselect
  @Input() isMultiselect = false;
  @Output() selectEvent = new EventEmitter<Option[]>();

  ngOnInit() {
    // Object.assign(this.initialSelectList, this.selectOptions);
    this.selectedOptionsList.map(option => {
      this.renderSelectedOptions(option);
    });

    // copy of initial object
    this.autocompleteOptions = JSON.parse(JSON.stringify(this.selectOptions));
  }

  toggleSelect(event) {
    this.optionsVisible = !this.optionsVisible;
  }

  // single select
  onSelectOption(key) {
    this.selectOptions.map( option => {
      option.selected = false;

      if (option.key === key) {
        option.selected = true;
          this.optionsVisible = false;
          const _selectedOptionsList = [
              option
          ];
          // debugger
          this.renderSelectedOptions (_selectedOptionsList);
      }
    });
  }

  // multi select
  onSelectCheckboxOption(optionList) {
    const _selectedOptionsList = [];
    optionList.map( option => {
      if (option.selected) {
          _selectedOptionsList.push(option);
      }
    });

    this.renderSelectedOptions(_selectedOptionsList, true);

  }

  // delete select
  onDeleteSelect(event) {
    this.selectedOptionsRenderString = null;
    this.removeAllHoverStates();
    this.selectOptions.map( i => delete i.selected);
  }

  onAutocomplete(event) {
    this.selectOptions = [];
    this.autocompleteValue = event;

    if (!event) {
      this.selectOptions = this.autocompleteOptions;
      return;
    }

    this.autocompleteOptions.map( option => {
      const searchedStr = event.toLowerCase();
      const optionValue = option.value.toLocaleLowerCase();

      if (optionValue.indexOf(searchedStr) >= 0 ) {
        this.selectOptions.push(option);
      }
    });

  }

  onDeleteAutoSelect(event) {
    // ***
    this.autocompleteDeleteBtnFaking = true;
    // End ***
    this.autocompleteValue = null;
  }

  renderSelectedOptions(optionList, multiselect?: boolean) {
    this.selectedOptionsRenderString = null;
    this.selectedOptionsList = optionList;

    optionList.map( option => {
        if (this.selectedOptionsRenderString) {
            this.selectedOptionsRenderString += ', ' + option.value;
        } else {
            this.selectedOptionsRenderString = option.value;
        }
    });

    this.selectEvent.emit(this.selectedOptionsList);
  }

  fireClickOutside() {
      this.optionsVisible = false;
      this.selectEvent.emit(this.selectedOptionsList);
  }

  fireArrowUpDown(keyCode) {
    let lastSelectedOrHoveredIndex = this.selectOptions.findIndex( o => o.hover === true);
    lastSelectedOrHoveredIndex = lastSelectedOrHoveredIndex === -1 ?
                                 this.selectOptions.findIndex( o => o.selected === true) :
                                 lastSelectedOrHoveredIndex;

    let newStateIndex = lastSelectedOrHoveredIndex === -1 ? 0 :
                        keyCode === 38 ? lastSelectedOrHoveredIndex - 1 :
                        lastSelectedOrHoveredIndex + 1;

    newStateIndex = newStateIndex === -1 ? this.selectOptions.length - 1 :
                    newStateIndex === this.selectOptions.length ? 0 :
                    newStateIndex;


    this.removeAllHoverStates();
    this.selectOptions[newStateIndex].hover = true;

  }

  removeAllHoverStates() {
    this.selectOptions = this.autocompleteOptions;
    this.autocompleteValue = null;
    this.selectOptions.map( o => {
      delete o.hover;
    });
  }

  onClickOutSideSelect(event) {
    // ***
    if (this.autocompleteDeleteBtnFaking) {
      this.autocompleteDeleteBtnFaking = false;
      return;
    }
    // end ***

    if (!event.targetContains) {
        this.removeAllHoverStates();
        this.fireClickOutside();
    }
  }

  onUpAndDown(event) {
    if (this.optionsVisible) {
      if (event.e.keyCode === 38 || event.e.keyCode === 40) {
        // up && down
        this.fireArrowUpDown(event.keyCode);
      } else if (event.keyCode === 13) {
        // enter
        const hoveredItemIndex = this.selectOptions.findIndex(o => o.hover === true);
        this.onSelectOption(this.selectOptions[hoveredItemIndex].key);
        delete this.selectOptions[hoveredItemIndex].hover;
      }
    }
  }
}
