import {Component, ElementRef, EventEmitter, HostListener, Output, ViewChild} from '@angular/core';

@Component({
    selector: 'lavash-event-container',
    templateUrl: './event-container.component.html',
    styleUrls: ['./event-container.component.less']
})

export class EventContainerComponent {
    top = 0;
    left = 0;

    windowWidthInit = window.innerWidth;

    @Output() keyboardEvent = new EventEmitter();
    @Output() mouseEvent = new EventEmitter();
    @Output() scrollEvent = new EventEmitter();
    @Output() resizeEvent = new EventEmitter();
    @Output() fullscreenchangeEvent = new EventEmitter();

    @Output() wheelEvent = new EventEmitter();
    @Output() mouseenterEvent = new EventEmitter();
    @Output() mouseleaveEvent = new EventEmitter();
    @Output() mousedownEvent = new EventEmitter();
    @Output() mouseupEvent = new EventEmitter();

    private targetContains;

    @ViewChild('keyboardActionComponent', {read: ElementRef})
    keyboardActionComponent: ElementRef;

    // touch events
    clientXStart = 0; // horizontal
    clientYStart = 0; // vertical

    clientXEnd = 0; // horizontal
    clientYEnd = 0; // vertical

    touchTargetStart;
    touchTargetEnd;

    @Output() touchEvent = new EventEmitter();

    onInint() {
        // this.targetContains = this.keyboardActionComponent.nativeElement.contains(event.target);
        // this.windowWidthInit = window.innerWidth;
    }

    onFireEvent(event) {
        // console.log('kb', event.type, event);
        // this.targetContains = this.keyboardActionComponent.nativeElement.contains(event.target);
        const emitData = {e: event, targetContains: this.targetContains};
        this[event.type + 'Event'].emit(emitData);
    }

    @HostListener('document:keydown', ['$event'])
    @HostListener('document:fullscreenchange', ['$event'])
    @HostListener('document:click', ['$event'])
    @HostListener('document:mousedown', ['$event'])
    @HostListener('document:mousemove', ['$event'])
    @HostListener('window:scroll', ['$event'])
    @HostListener('window:resize', ['$event'])
    fireEvent(event) {
        const emitData = {e: event};


        if ( event instanceof Event) {
            if (event.type === 'scroll') {
                this.emitScrollEvent(emitData);
            }

            if (event.type === 'resize') {
                this.emitResizeEvent(emitData, event);
            }

            if (event.type === 'fullscreenchange') {
                this.fullscreenchangeEvent.emit(emitData);
            }
        }

        if ( event instanceof MouseEvent) {
            emitData['targetContains'] = this.keyboardActionComponent.nativeElement.contains(event.target);
            this.mouseEvent.emit(emitData);
        }

        if ( event instanceof KeyboardEvent) {
            this.keyboardEvent.emit(emitData);
        }
    }

    emitScrollEvent(emitData) {
        const wondowCurrentTop = window.pageYOffset;
        const wondowCurrentLeft = window.pageXOffset;

        const scrollDirection: any = {};

        scrollDirection.down = wondowCurrentTop > this.top;
        scrollDirection.right = wondowCurrentLeft > this.left;
        scrollDirection.windowY = wondowCurrentTop;
        scrollDirection.windowX = wondowCurrentLeft;

        emitData['scrollDirection'] = scrollDirection;
        this.scrollEvent.emit(emitData);
        this.top = wondowCurrentTop;
        // console.log('scroll', emitData['scrollDirection']);
    }

    emitResizeEvent(emitData, event) {
        if (this.windowWidthInit === window.innerWidth) {
            // IMPORTANT: FAKE RESIZE on mobile devices
            // IMPORTANT: on mobile devices on scroll because the mobile chorme bar disappears
            // chrome behaves as if resize happens
            // which brings to errors, so we check up wheather the window actual width is changed or not
            // if not, than we are in mobile device
            return;
        }

        this.windowWidthInit = event.target['innerWidth'];
        emitData['windowWidth'] = event.target['innerWidth'];
        emitData['windowHeight'] = event.target['innerHeight'];
        this.resizeEvent.emit(emitData);
    }

    // touch events

    onTouchstart(event) {
        this.clientXStart = event.touches[0].clientX;
        this.clientYStart = event.touches[0].clientY;

        this.touchTargetStart = event.target.className;
    }

    onTouchend(event) {
        this.clientXEnd = event.changedTouches[0].clientX;
        this.clientYEnd = event.changedTouches[0].clientY;

        this.touchEvent.emit({
            toUp: this.clientYStart > this.clientYEnd,
            toLeft: this.clientXStart > this.clientXEnd,
            touchTargetStart: this.touchTargetStart,
            touchTargetEnd: event.target.className,
            tauchTargetMatch: this.touchTargetStart === event.target.className,
            e: event
        });

    }
}
