import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'lavash-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.less']
})
export class TextComponent implements OnInit {
  @Input() text: string;
  _text;

  constructor(private domSanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this._text = this.domSanitizer.bypassSecurityTrustHtml(this.text);
  }
}
