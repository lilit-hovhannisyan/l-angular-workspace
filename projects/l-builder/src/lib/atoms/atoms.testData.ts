import {Option} from './atoms.interfaces';

export const optionList: Option[] = [
    {
        key: '1',
        value: 'Test data'
    },
    {
        key: '2',
        value: 'Another test data'
    },
    {
        key: '3',
        value: 'Final test data'
    }
];