import {Component,  Input} from '@angular/core';

@Component({
  selector: 'lavash-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.less']
})
export class TooltipComponent {
  @Input() text: string;
  @Input() direction: 'top' | 'right' | 'bottom' | 'left';
  @Input() _margin = 0;

  _tooltipVisibility: boolean;
  _tooltipTop = 0;
  _tooltipLeft = 0;
  _tooltipWidth = null;


  onmMuseenter(event, containerEl, tooltipEl) {
    this.calc(containerEl, tooltipEl);
  }

  onMouseleave(event) {
    this._tooltipVisibility = false;

    if (this.direction === 'top') {
        this._tooltipTop = this._tooltipTop + 30;
        this._tooltipLeft = 0;
    }

    if (this.direction === 'bottom') {
        this._tooltipTop = this._tooltipTop - 30;
        this._tooltipLeft = 0;
    }

    if (this.direction === 'left') {
        this._tooltipLeft = this._tooltipLeft + 30;
    }

    if (this.direction === 'right') {
        this._tooltipLeft = this._tooltipLeft - 30;
    }

  }

  calc(containerEl, tooltipEl) {
      const containerElWidth = containerEl.getBoundingClientRect().width;
      const containerElHeight = containerEl.getBoundingClientRect().height;
      const containerElTop = containerEl.getBoundingClientRect().top;
      const containerElLeft = containerEl.getBoundingClientRect().left;

      const tooltipElWidth = tooltipEl.getBoundingClientRect().width;
      const tooltipElHeight = tooltipEl.getBoundingClientRect().height;


      const _tooltipTop = this.direction === 'bottom' ? containerElTop :
                          this.direction === 'top' ? containerElTop :
                          containerElTop + (containerElHeight - tooltipElHeight) / 2; // common left, right

      const _tooltipLeft = this.direction === 'right' ? containerElLeft :
                           this.direction === 'left' ? containerElLeft :
                           containerElLeft - tooltipElWidth / 2 + containerElWidth / 2; // common top, bottom

      this._tooltipTop = _tooltipTop;
      this._tooltipLeft = _tooltipLeft;
      this._tooltipWidth = tooltipElWidth;

      const animate = () => {
          this._tooltipVisibility = true;

          if (this.direction === 'left') {
              this._tooltipLeft = (containerElLeft - tooltipElWidth - this._margin);
          }

          if (this.direction === 'right') {
              this._tooltipLeft = (containerElLeft + containerElWidth + this._margin);
          }

          if (this.direction === 'top') {
              this._tooltipTop = (containerElTop - tooltipElHeight - this._margin);
          }

          if (this.direction === 'bottom') {
              this._tooltipTop = (containerElTop + tooltipElHeight + this._margin);
          }

          /*if (this.direction === 'left') {
              this._tooltipTop = containerElTop + (containerElHeight - tooltipElHeight) / 2;
              this._tooltipLeft = (containerElLeft - tooltipElWidth - this._margin);
          }

          if (this.direction === 'right') {
              this._tooltipTop = containerElTop + (containerElHeight - tooltipElHeight) / 2;
              this._tooltipLeft = (containerElLeft + containerElWidth + this._margin);
          }

          if (this.direction === 'top') {
              this._tooltipTop = (containerElTop - tooltipElHeight - this._margin);
              this._tooltipLeft = containerElLeft - tooltipElWidth / 2 + containerElWidth / 2;
          }

          if (this.direction === 'bottom') {
              this._tooltipTop = (containerElTop + tooltipElHeight + this._margin);
              this._tooltipLeft = containerElLeft - tooltipElWidth / 2 + containerElWidth / 2;
          }*/
      };

      setTimeout(animate, 500);
  }

}
