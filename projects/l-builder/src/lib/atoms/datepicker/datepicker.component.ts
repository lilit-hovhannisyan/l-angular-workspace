import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'lavash-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.less']
})

export class DatepickerComponent implements OnInit {
  // ui
  @Input() calendarIcon = 'icon-calendar';
  @Input() calendarIconPosition = 'right';
  @Input() leftArrowIcon;
  @Input() rightArrowIcon;

  // logic
  datePickerVisible = false;
  @Input() startsFromMonday = false;

  @Input() spliter = '/';
  @Input() dateFormat = 'dd/mm/yyyy';
  placeholder = '';
  selectedDay = null;
  selectedDate = null;
  d = new Date();

  mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  curMonth;
  curMonthIndex;
  staticCurMonthIndex;
  selectedMonthIndex;

  curYear;
  staticYear;
  selectedYear;
  manualChangeYear;

  wDl = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
  wDStep = 0;
  weekDays: any = [];
  curDay;
  daysInCurMonth;
  firstDayInMonthWeekIndex;

  @Output() dateEvent = new EventEmitter();

  ngOnInit() {
    if ( this.startsFromMonday ) {
      this.wDl = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun'];
      this.wDStep = -1;
    }
    this.initDatePicker();
  }

  setDateInfo() {
    return {
        weekDayslist: this.wDl,
        weekDaysMap: this.weekDays,
        currentDay: this.curDay,
        currentMonth: this.curMonth,
        currentYear: this.curYear,
        selectedDay: this.selectedDay,
        selectedMonth: this.mL[this.selectedMonthIndex],
        selectedYear: this.selectedYear,
        selectedDate: this.selectedDate
    };
  }

  initDatePicker() {
    // init month
    this.curMonthIndex = this.d.getMonth();
    this.staticCurMonthIndex = this.d.getMonth();
    this.curMonth = this.mL[this.curMonthIndex];

    // init year
    this.curYear = this.d.getFullYear();
    this.staticYear = this.d.getFullYear();

    this.initDaysOfMonth();

  }

  initDaysOfMonth() {
    // reset
    this.weekDays = [];

    // init days
    this.curDay = this.d.getDate(); // today date
    this.daysInCurMonth = new Date(this.curYear, this.curMonthIndex + 1, 0).getDate();
    this.firstDayInMonthWeekIndex = new Date(this.curYear, this.curMonthIndex, 1).getDay() + this.wDStep;

    // first week
    const firstWeekInMonth = [];
    const daysInFirstWeek = 7 - this.firstDayInMonthWeekIndex;
    let counter = 7;
    while (counter) {
      if ( counter > 7 - this.firstDayInMonthWeekIndex ) {
        firstWeekInMonth.push('');
      }
      counter--;
    }

    for (let i = 1; i <= daysInFirstWeek; i++) {
      firstWeekInMonth.push(i);
    }
    this.weekDays.push(firstWeekInMonth);

    // rest weeks
    const secondWeekStart = daysInFirstWeek + 1;
    const restDaysInMonth = this.daysInCurMonth - daysInFirstWeek;

    const weeksInMonth =  Math.floor(restDaysInMonth / 7);
    let week = [];
    for (let i = secondWeekStart; i <= this.daysInCurMonth; i++ ) {
      week.push(i);
      if ( week.length === 7) {
        this.weekDays.push(week);
        week = [];
      }
    }

    // last week if incomplete
    if ( this.weekDays.length === weeksInMonth + 1 && this.weekDays[weeksInMonth][6] !== this.daysInCurMonth) {
      const lastWeek = [];
      let _counter = 7;
      let start = this.weekDays[weeksInMonth][6] + 1;

      while ( _counter ) {
        if ( start <= this.daysInCurMonth ) {
          lastWeek.push(start);
        } else {
          lastWeek.push('');
        }

        start++;
        _counter--;
      }

      this.weekDays.push(lastWeek);

      console.log(this.weekDays)

    }
  }

  changeMonth(index) {
    const curMonthIndex = this.curMonthIndex + index;
    this.curMonthIndex = curMonthIndex;
    if ( curMonthIndex === -1 ) {
      this.curMonthIndex = 11;
      this.curYear = this.curYear - 1;
    } else if ( curMonthIndex === 11 ) {
      this.curMonthIndex = 1;
      this.curYear = this.curYear + 1;
    }

    this.curMonth = this.mL[this.curMonthIndex];
    this.initDaysOfMonth();

    this.dateEvent.emit({action: 'changeMonth', dateInfo: this.setDateInfo()});
  }

  changeYear(index) {
    this.curYear += index;
    this.initDaysOfMonth();

    this.dateEvent.emit({action: 'changeYear', dateInfo: this.setDateInfo()});
  }

  onYearManualFocus() {
    console.log('focus', this.curYear);
    this.manualChangeYear = this.curYear;
  }

  onYearManualChange() {
    if ( this.curYear.length > 4 || this.curYear.length < 4 ) {
      this.curYear = this.manualChangeYear;
      return;
    }

    this.initDaysOfMonth();
  }

  onSelectDate(d) {
    if (!d) {
      return;
    }
    let _date = d, month = this.curMonthIndex + 1;



    this.selectedDay = d;
    this.selectedMonthIndex = this.curMonthIndex;
    this.selectedYear = this.curYear;

    if ( d < 10 ) {
      _date = '0' + d;
    }

    if ( month < 10 ) {
      month = '0' + month;
    }

    this.selectedDate = this.dateFormat.replace(/\//g, this.spliter)
                                        .replace('dd', _date)
                                        .replace('mm', month)
                                        .replace('yyyy', this.curYear);
                                        // _date + this.spliter + month  + this.spliter + this.curYear;

    this.datePickerVisible = false;

    this.dateEvent.emit({action: 'selectDateFromPicker', dateInfo: this.setDateInfo()});
  }

  onDateChangeFromInput() {
    if (!this.selectedDay) {
      this.onSelectDate(this.curDay);
    }
    const dateParts = this.selectedDate.split(this.spliter);
    this.placeholder = `
            <p class="flex">
                <p contenteditable="true">${dateParts[0]}</p> ${this.spliter}
                <p contenteditable="false">${dateParts[1]}</p> ${this.spliter}
                <p contenteditable="false">${dateParts[2]}</p>
            </p>`;
  }

  onClearDateFromInput() {
    this.selectedDay = null;
    this.selectedDate = null;
    this.dateEvent.emit({action: 'onClearDateFromInput', dateInfo: this.setDateInfo()});
  }

  onToggleDatePicker() {
    this.datePickerVisible = !this.datePickerVisible;
  }

  onDatePickerClickout(event) {
    if (!event.targetContains) {
        this.datePickerVisible = false;
    }
  }
}
