import {Component, EventEmitter, Input, Output} from '@angular/core';

export enum LibLContainerType {
  col12 = 'col-12',
  col11 = 'col-11',
  col10 = 'col-10',
  col9 = 'col-9',
  col6 = 'col-6',
  FLEX =  'flex',
  GRID =  'grid',
}

@Component({
  selector: 'lavash-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.less']
})
export class ContainerComponent {
  @Input() type: LibLContainerType;
}
