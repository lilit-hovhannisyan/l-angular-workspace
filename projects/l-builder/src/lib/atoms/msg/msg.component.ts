import {Component, Input} from '@angular/core';

@Component({
  selector: 'lavash-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.less']
})

export class MsgComponent {
  @Input() message = 'Error occurs';
  @Input() messageType: 'error' | 'success' | 'info' = 'error';
}
