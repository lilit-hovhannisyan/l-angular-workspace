import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Field} from '../../molecules/molecules.interfaces';
import {ValidationService} from '../../services/validation.service';

@Component({
  selector: 'lavash-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.less']
})
export class FormBuilderComponent {
  @Input() fieldsConfig: Field[] = [];
  @Input() submitBtnText = 'Submit';
  @Output() submitEvent = new EventEmitter();

  @Input() hasFormErrorMsg;
  @Input() hasFormSuccessMsg = true;
  @Input() formErrorMsg = 'Form has errors';
  @Input() formSuccessMsg = 'Congratulations, your message is successfully sent';

  showFormMsg;
  formMsg;
  formMsgType;


  formSubmitTrialOnce: boolean;
  formErrorCount = 0;
  formData = {};

  constructor(private validationService: ValidationService) {}

  // input, textarea
  onTypedDataEvent(event, field) {
    this.formData[field.name] = event;

    if (this.formSubmitTrialOnce) {
        this.validateField(field);
        //console.log('ffff',field)
    }
  }

  validateFields() {
    this.fieldsConfig.map( field => {
      this.validateField(field);
    });
  }

  validateField(field) {
      const value = this.formData[field.name]; // field value


      // STEP 1: check default patterns
      const defaultPattern =  field.descriptors.type ?
          this.validationService.defaultValidationpatterns[field.descriptors.type] :
          null;

      if (this.formData[field.name] && defaultPattern) {

          console.log('default pattern', this.formData[field.name], defaultPattern)
          // check if does not have any rules but field is dirty (touched by user)
          const ruleFunctionFactory = this.validationService.validationRules.regExp; // check against default pattern
          const hasError = !ruleFunctionFactory(value, defaultPattern.rule);


          // todo es ktory u verevi ktory mi hat funkcional sarqel
          field.hasError = hasError;
          field.errorMessage = defaultPattern.customErrMsg ||
              this.validationService.defaultErrorMsgMap['regExp'] ||
              null;

          this.countFormErrors();
      }

      // STEP 2: check custom rules
      if (field.validationRules && Object.keys(field.validationRules).length) {
          // has rules
          for (const key of Object.keys(field.validationRules)) {
              delete field.hasError;
              delete field.errorMessage;

              const rule = field.validationRules[key];
              const ruleFunctionFactory = this.validationService.validationRules[key];

              const hasError = !ruleFunctionFactory(value, rule.value);

              field.hasError = hasError;
              field.errorMessage = rule.customErrMsg ||
                  this.validationService.defaultErrorMsgMap[key] ||
                  null;

              this.countFormErrors();

              if (field.hasError) {
                  break;
              }
          }
      }
  }

  countFormErrors() {
      this.formErrorCount = 0;
      this.fieldsConfig.map( field => {
          this.formErrorCount = field.hasError ? this.formErrorCount + 1 : this.formErrorCount;
      });
  }

  onSubmit($event) {
    this.formSubmitTrialOnce = true;

    this.validateFields();

    this.submitEvent.emit({
        formdata: this.formData,
        formErrorCount: this.formErrorCount
    });

    if (this.formErrorCount > 0 && this.hasFormErrorMsg ) {
        this.showFormMsg = true;
        this.formMsg = this.formErrorMsg;
        this.formMsgType = 'error';
    }

    // clear if form is submited
    if (this.formErrorCount === 0) {
        // reset form
        this.formSubmitTrialOnce = false;
        this.formData = {};

        this.showFormMsg = true;
        this.formMsg = this.formSuccessMsg;
        this.formMsgType = 'success';

        setTimeout( () => {
            this.showFormMsg = false;
        }, 2000);
    }
  }
}
