import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LavashAtomsModule} from '../atoms/atoms.module';
import {LavashMoleculesModule} from '../molecules/molecules.module';
import {FormBuilderComponent} from './form-builder/form-builder.component';
import {GridContainerComponent} from './grid-container/grid-container.component';

@NgModule({
   declarations: [
       FormBuilderComponent,
       GridContainerComponent
   ],
   imports: [
       CommonModule,
       LavashAtomsModule,
       LavashMoleculesModule
   ],
   providers: [],
   exports: [
       FormBuilderComponent,
       GridContainerComponent
   ],
})

export class LavashOrganismsModule {
}
