import {NgModule} from '@angular/core';
import {LBuilderComponent} from './l-builder.component';

import {TranslateModule} from '@ngx-translate/core';
import {TranslateService} from '@ngx-translate/core';
import {ValidationService} from './services/validation.service';

import {LavashAtomsModule} from './atoms/atoms.module';
import {LavashMoleculesModule} from './molecules/molecules.module';
import {LavashOrganismsModule} from './organisms/organisms.module';

@NgModule({
  declarations: [LBuilderComponent],
  imports: [
    TranslateModule,
    LavashAtomsModule,
    LavashMoleculesModule,
    LavashOrganismsModule
  ],
  providers: [
    TranslateService,
    ValidationService
  ],
  exports: [LBuilderComponent]
})
export class LavashBuilderModule { }
