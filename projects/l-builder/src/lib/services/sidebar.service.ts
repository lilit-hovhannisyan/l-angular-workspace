import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SidebarService {
    _sidebarState = false;
    sidebarState = new BehaviorSubject(this._sidebarState);

    changeState(state) {
        this._sidebarState = state;
        this.sidebarState.next(this._sidebarState);
    }

    openSidebar() {
        this.changeState(true);
    }

    closeSidebar() {
        this.changeState(false);
    }
}
