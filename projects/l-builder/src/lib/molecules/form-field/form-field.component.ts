import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Field} from '../molecules.interfaces';

@Component({
  selector: 'lavash-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.less']
})
export class FormFieldComponent {
  @Input() field: Field;
  @Input() fieldDataObject: any;
  @Output() typedDataEvent = new EventEmitter(); // input, textarea

  onTypedDataChange(event) {
    this.typedDataEvent.emit(event);
  }
}
