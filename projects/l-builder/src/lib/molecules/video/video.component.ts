import {Component, Input} from '@angular/core';

@Component({
  selector: 'lavash-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.less']
})
export class VideoComponent {
  // media control component
  @Input() videoSrc = '';
  @Input() videoPath = '';
  @Input() videoPosterPath = '';
  @Input() videoPosterSrc = '';
  @Input() hasSpeedControl;

  // specefic to this component
  @Input() showControls = true; // fixed on video or visible on hover
}
