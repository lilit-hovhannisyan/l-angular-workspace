import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'lavash-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.less']
})
export class ArticleCardComponent implements OnInit {
  @Input() article;
  @Input() articleImgPath?;
  @Input() viewMode: 'shorthand' | 'full' = 'full';
  @Output() cardBtnClickEvent = new EventEmitter();

  @Input() readMoreBtnText = 'Read more';
  @Input() readMoreBtnIcon = 'l-icon-corner-up-right';
  @Input() readMoreBtnClassNames = 'no-background animated';

  @ViewChild('articleDescriptionEl')
  articleDescriptionEl: ElementRef;

  @Input() articleFullModeContainerPxHeight = window.innerHeight - 300;
  @Input() articleFullModeContainerPxWidth = window.innerWidth - 50;


  ngOnInit() {
    //console.log("eee", this.articleDescriptionEl.nativeElement.getBoundingClientRect().top);
  }

  onCardBtnClick(article) {
    this.cardBtnClickEvent.emit(article);
  }
}
