import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'lavash-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.less']
})
export class AudioComponent implements OnInit {
    @Input() audioSrc = '../assets/audio/2 Cellos - Benedictus.mp3';
    @Input() audioPath = '';
    @Input() audioTitle = '2 Cellos - Benedictus';
    @Input() hasSpeedControl;

    showControls = false;

    @ViewChild('equilizer')
    equilizer: ElementRef<HTMLCanvasElement>;

    @ViewChild('audio')
    audio: ElementRef<HTMLAudioElement>;

    html;
    _equilizer;
    equilizerList = [];
    barsSpace = 1;
    barsWidth = 5;

    canvasHeight

    constructor(private sanitizer: DomSanitizer) {}

    ngOnInit() {
        this.initbars();
    }

    onPauseStop(event) {
        console.log('stoped')
        clearInterval(this._equilizer);
    }

    onPlay(event) {
        this.mySolution();
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    initbars() {
        const canvas = this.equilizer.nativeElement;
        const canvasWidth = canvas.offsetWidth;
        this.canvasHeight = canvas.offsetHeight;
        const barsCount = canvasWidth / (this.barsWidth + this.barsSpace);

        for (let i = 0; i < barsCount; i++) {
            const barHeight = this.getRandomArbitrary(0, this.canvasHeight);

            this.equilizerList.push({
                height: barHeight
            });
        }
    }

    mySolution() {
        const animateEquilizer = () => {
            this.equilizerList.map( i => {
                i.height = this.getRandomArbitrary(0, this.canvasHeight);
            });
        };

        this._equilizer = setInterval(animateEquilizer, 500  );
    }
}
