import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PopupService} from '../../services/popup.service';
import {RouteManagementService} from '../../services/route-management.service';

@Component({
  selector: 'lavash-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.less']
})

export class PopupComponent implements OnInit {
  popupIsActive = false;
  queryObj; // bind url with popup
  popupEmitData; // popupName, popupData


  @Input() closeBtnText = '';
  @Input() closeBtnIcon = 'l-icon-times';
  @Output() popupOpenEvent = new EventEmitter();
  @Output() popupCloseEvent = new EventEmitter();


  constructor(private popupService: PopupService,
              private routeManagementService: RouteManagementService) { }

  ngOnInit () {
      this.popupService.popupData.subscribe( observer => {
          if (observer) {
              const {popupName, data, queryObj} = observer;
              this.popupIsActive = true;

              this.popupIsActive = true;

              this.popupEmitData = {popupName, data, queryObj};
              this.queryObj = queryObj;

              this.popupOpenEvent.emit(this.popupEmitData);
          }
      });
  }

  closePopup() {
    this.popupIsActive = false;

    if (this.queryObj) {
      this.routeManagementService.setRouteQuery(this.queryObj.query, this.queryObj.param);
    }

    this.popupCloseEvent.emit(this.popupEmitData);
    this.popupEmitData = null;
  }

  onKeydown(event) {
    if (this.popupIsActive && event.e.keyCode === 27 ) {
      this.closePopup();
    }
  }
}
