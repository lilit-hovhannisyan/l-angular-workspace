import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {appName, routeConf, errorPh} from '../../../shared-layer/l-hovhanni/dal/app.config';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {RouteManagementService} from 'l-builder';
import {ProjectsService} from './services/projects.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
    photographer;
    fixedTopBlock = false;
    absoluteFooter = false;


    constructor(private title: Title,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private routeManagementService: RouteManagementService,
                private projectService: ProjectsService) {

        router.events.subscribe(val => {

            if (val instanceof NavigationEnd) {
                // query management
                this.projectService.setProjectQuery();
                this.projectService.setCurrentPhotoQuery();

                // route managment
                const curRoute = val.url.split('?')[0].replace('/', '');
                console.log('curRoute', curRoute);
                const confData = routeConf.find( i => i.route === curRoute);
                this.photographer = confData ? confData.photographer : errorPh;
                const _title = confData ? confData.title : '404';

                this.title.setTitle(`${_title} ${appName}`);
            }
        });
    }

    ngOnInit() {
        console.log('l-hovhanni-app --v1.0.0');
    }

    onScroll(event) {
        const diff = event.scrollDirection.down ? 100 : 50;
        const windowHeightFrom = window.innerHeight;

        if (event.scrollDirection.windowY > 5) {
            this.absoluteFooter = true;
        } else {
            this.absoluteFooter = false;
        }



        if (event.scrollDirection.windowY >= windowHeightFrom - diff ) {
            this.fixedTopBlock = true;
        } else {
            this.fixedTopBlock = false;
        }
    }
}