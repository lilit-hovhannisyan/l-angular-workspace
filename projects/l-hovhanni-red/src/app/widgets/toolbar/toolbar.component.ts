import {Component} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent {
    fixToolBar = false;

    onScroll(event) {
        const windowHeightFrom = window.innerHeight;
        if (event.scrollDirection.windowY >= windowHeightFrom ) {
            this.fixToolBar = true;
        } else {
            this.fixToolBar = false;
        }
    }
}
