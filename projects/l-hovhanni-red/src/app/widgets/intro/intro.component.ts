import { Component, OnInit } from '@angular/core';

@Component ({
  selector: 'app-intro-component',
  templateUrl : './intro.component.html',
  styleUrls : ['./intro.component.less']

})

export class IntroComponent implements OnInit {
  anime: boolean = true;

  ngOnInit () {
    setTimeout( () => { this.anime = !this.anime }, 2300);
  }
}



