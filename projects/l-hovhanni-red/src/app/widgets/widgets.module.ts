import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {LavashAtomsModule, LavashMoleculesModule} from 'l-builder';


import {IntroComponent} from './intro/intro.component';
import {LogoComponent} from './logo/logo.component';
import {NavBarComponent} from './navBar/nav-bar.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {SocialLinksComponent} from './social-links/social-links.component';
import {PhLinkComponent} from './ph-link/ph-link.component';
import {GridContainerComponent } from './grid-container/grid-container.component';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {PopupComponent} from './app-popup/app-popup.component';
import {ImgSliderComponent} from './img-slider/img-slider.component';
import {LavashOrganismsModule} from 'l-builder';
import {TopBlockComponent} from './top-block/top-block.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ProjectComponent} from './project/project.component';
import {ProjectListComponent} from './project-list/project-list.component';

@NgModule({
    declarations: [
        IntroComponent,
        HeaderComponent,
        NavBarComponent,
        LogoComponent,
        FooterComponent,
        SocialLinksComponent,
        PopupComponent,
        PhLinkComponent,
        GridContainerComponent,
        ToolbarComponent,
        ImgSliderComponent,
        TopBlockComponent,
        SidebarComponent,
        ProjectComponent,
        ProjectListComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        LavashAtomsModule,
        LavashMoleculesModule,
        LavashOrganismsModule
    ],
    providers: [],
    exports: [
        IntroComponent,
        HeaderComponent,
        NavBarComponent,
        LogoComponent,
        FooterComponent,
        SocialLinksComponent,
        PopupComponent,
        PhLinkComponent,
        GridContainerComponent,
        ToolbarComponent,
        TopBlockComponent
    ]
})

export class WidgetsModule {
}
