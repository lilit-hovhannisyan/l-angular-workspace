import {Component, Input, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {SidebarService} from 'l-builder';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.less']
})
export class ProjectComponent {
  @Input() project;
  @Input() currentProject?;

  imgPath = '../l-hovhanni-assets/img/portfolio/';

  constructor(private projectsService: ProjectsService,
              private sidebarService: SidebarService) {
  }

  changeProject(project) {
    this.projectsService.setCurrentProject(project.key);

    this.projectsService.setProjectQuery();
    this.projectsService.setCurrentPhotoQuery();

    this.sidebarService.closeSidebar();
  }

}
