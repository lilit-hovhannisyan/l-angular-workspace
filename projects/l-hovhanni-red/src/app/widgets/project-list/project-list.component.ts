import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProjectsService} from '../../services/projects.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.less']
})
export class ProjectListComponent implements OnInit {
    projects;
    currentProject;


    constructor(private projectsService: ProjectsService) {
    }

    ngOnInit() {
        this.projects = this.projectsService.getProjects();
        this.projectsService.getCurrentProject();
        this.projectsService.currentProject.subscribe(project => {
            this.currentProject = project;
        });
    }
}



