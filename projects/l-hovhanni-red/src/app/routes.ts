import { Routes } from '@angular/router';

import {
    PortfolioComponent,
    AboutComponent,
    ContactComponent,
    NotFoundComponent, VideosComponent, StoryTellingComponent
} from './pages';

export const appRoutes: Routes = [
  {path: 'portfolio', component: PortfolioComponent },
  {path: 'about', component: AboutComponent },
  {path: 'contact', component: ContactComponent },
  {path: 'videos', component: VideosComponent },
  {path: 'story-telling', component: StoryTellingComponent },
  {path: '', redirectTo: 'portfolio', pathMatch: 'full'},
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', redirectTo: 'not-found'}
];



