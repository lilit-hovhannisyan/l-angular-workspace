import { Component } from '@angular/core';
import {stories} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {ActivatedRoute} from '@angular/router';
import {RouteManagementService, PopupService} from 'l-builder';

@Component({
  selector: 'app-story-telling',
  templateUrl: './story-telling.component.html',
  styleUrls: ['./story-telling.component.less']
})
export class StoryTellingComponent {
    columns = 3;
    stories = stories;
    story = null;

    constructor(public routeManagementService: RouteManagementService,
                private popupService: PopupService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        // force popup
        const activeStory = this.activatedRoute.snapshot.queryParams.story;
        if (activeStory) {
            const currentStory = this.stories.find(story => story.title === activeStory);
            if (currentStory) {
                this.onOpenPopup(currentStory);
            }
        }
    }

    onOpenPopup(story) {
        this.story = story;
        this.popupService.openPopup(
            'STORY',
            {
                story: story,
            });
        // this.routeManagementService.setRouteQuery('story', this.story.title);
    }
}
