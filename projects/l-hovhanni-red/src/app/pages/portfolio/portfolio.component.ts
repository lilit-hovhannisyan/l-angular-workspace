import {Component, OnInit} from '@angular/core';
import {PopupService} from 'l-builder';
import {Router} from '@angular/router';
import {ProjectsService} from '../../services/projects.service';

@Component ({
  selector : 'app-portfolio',
  templateUrl : './portfolio.component.html',
  styleUrls : ['./portfolio.component.less']
})

export class PortfolioComponent implements OnInit {
  portfolioItems: any;
  portfolioItemsSelected = [];
  portfolioGroupType;


  constructor(private popupService: PopupService,
              private router: Router,
              private projectsService: ProjectsService) {}

  columns = 4;
  colMax = 12;
  _columns;

  ngOnInit() {
    this.onResize({windowWidth: window.innerWidth});
    this.groupImages();
  }

  groupImages() {
      this.projectsService.getCurrentProject();
      this.projectsService.currentProject.subscribe(observer => {
          this.portfolioItemsSelected = observer.items;
      });
  }

  changeMode(count) {
    this.columns = count;
  }

  onResize(event) {
      if (event.windowWidth <= 600) {
        this.columns = 1;
        this.colMax = 6;
      } else if (event.windowWidth <= 800) {
          this.columns = 2;
          this.colMax = 8;
      } else {
          this.columns = 4;
          this.colMax = 12;
      }

      this._columns = +this.columns;
  }

  onNavigateTo(page) {
      this.router.navigate([page]);
  }
}



