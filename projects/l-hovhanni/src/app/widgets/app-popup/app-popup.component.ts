import { Component, OnInit } from '@angular/core';
import {storyPath} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';

const popupNameList = {
  IMG_SLIDER: 'IMG_SLIDER',
  STORY: 'STORY'
};

@Component({
  selector: 'app-popup',
  templateUrl: './app-popup.component.html',
  styleUrls: ['./app-popup.component.less']
})
export class PopupComponent  {
  popupName;
  popupData;

  popupNameList = popupNameList;

  articleImgPath = storyPath + '/';

  onPopupOpen(event) {
    this.popupName = event.popupName;
    this.popupData = event.data;
  }

  onPopupClose(event) {
      this.popupName = null;
      this.popupData = null;
  }
}
