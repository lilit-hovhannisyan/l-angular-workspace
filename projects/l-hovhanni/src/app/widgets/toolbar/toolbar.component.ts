import {Component} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent {
    fixToolBar = false;

    onScroll(event) {
        if (event.scrollDirection.windowY >= 100 ) {
            this.fixToolBar = true;
        } else {
            this.fixToolBar = false;
        }
    }
}
