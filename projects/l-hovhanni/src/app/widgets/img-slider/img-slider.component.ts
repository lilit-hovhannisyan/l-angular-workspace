import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {portfolioPath} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {RouteManagementService} from 'l-builder';

@Component({
  selector: 'app-img-slider',
  templateUrl: './img-slider.component.html',
  styleUrls: ['./img-slider.component.less']
})
export class ImgSliderComponent {
  @Input() sliderItem;
  @Input() sliderItems;

  constructor(private routeManagementService: RouteManagementService) {}

  alt = 'L-hovhanni image';
  portfolioPath = portfolioPath + '/';

  onSlideEvent (event) {
    this.sliderItem = event;

    this.routeManagementService.setRouteQuery('photo',  event.src, false);
  }
}
