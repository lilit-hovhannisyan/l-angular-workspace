import { Component } from '@angular/core';
import { navItems } from '../../../../../shared-layer/l-hovhanni/dal/app.config';

@Component ({
  selector: 'app-nav-bar',
  templateUrl : './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less']
})

export class NavBarComponent {
  navItems = navItems;
}

