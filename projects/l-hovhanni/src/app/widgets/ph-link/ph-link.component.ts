import {Component, Input} from '@angular/core';
import {ph, phLink, exLink, topics} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';

@Component({
  selector: 'app-ph-link',
  templateUrl: './ph-link.component.html',
  styleUrls: ['./ph-link.component.less']
})
export class PhLinkComponent {
  @Input() introText = '';
  @Input() photographer;
  @Input() photo?;

  ph = ph;
  phLink = phLink;
  exLink = exLink;
  topics = topics;
}
