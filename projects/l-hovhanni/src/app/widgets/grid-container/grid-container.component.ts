import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {storyPath} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';

@Component({
  selector: 'app-grid-container',
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.less']
})
export class GridContainerComponent {
    @Input() columns;
    @Input() items = [];
    @Output() itemClickEvent = new EventEmitter();
    @Output() cardBtnClickEvent = new EventEmitter();
    @Output() windowResizeEvent = new EventEmitter();
    @Output() pageScrollEvent = new EventEmitter();
    @Output() imageLoadEvent = new EventEmitter();

    dynamicItemData;
    dynamicItemDataType;
    introText = '';

    onItemClick(item) {
       this.itemClickEvent.emit(item);
    }

    onItemHover(event) {
       this.dynamicItemData = event.dynamicItemData;
       this.dynamicItemDataType = event.type;
       this.introText = event.type === 'img' || event.type === 'card' ? 'Photo created with:' : 'Video created with:';
    }

    onCardBtnClick(cardItem) {
        this.cardBtnClickEvent.emit(cardItem);
    }

    onResize(event) {
        this.windowResizeEvent.emit(event);
    }

    onImageLoad(event) {
        this.imageLoadEvent.emit(event);
    }
}
