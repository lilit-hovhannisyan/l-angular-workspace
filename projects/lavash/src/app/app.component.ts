import {Component, Input, OnInit} from '@angular/core';
import {libStructure, routes} from '../../l-builder.structure';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {from, Observable, of} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
    libStructure = libStructure;

    title;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router) {
        router.events.subscribe( event => {
            if (event instanceof NavigationEnd) {
                const route = event.url.replace('/', '');
                this.title = routes.find( r => r.path === route).component;
            }
        });
    }

    ngOnInit() {
        // console.log(this.activatedRoute.snapshot.url);
        const a = [1, 2 , 3];

        const x = new Observable((subscriber) => {
            a.map( i => {
                subscriber.next(i);
            });
        });

        const y = of(true);

        const z = new Observable( (subscriber) => {
           subscriber.next(true);
        });


        const t = Observable.create( subscriber => {
            subscriber.next('hello');
        })
        t.subscribe(
            observer => { console.log('hey form observer', observer); },
            err => {},
            () => {}
        );

        console.log('ayyyyy')

    }
}
