import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {LavashAtomsModule, LavashMoleculesModule} from 'l-builder';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {UserDataService} from './services/user-data.service';

// import {UserApi} from 'l-builder';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
const routes = [
    // {path: 'dd', component: LoginComponent}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    /*TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),*/
    LavashAtomsModule,
    LavashMoleculesModule
  ],
  providers: [
    // TranslateService
      UserDataService,
     // {provide: UserApi, useExisting: UserDataService}

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
