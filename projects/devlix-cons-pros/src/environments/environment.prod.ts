export const environment = {
  production: true,
  apiEndpoint: {
     base: 'https://avetiq-test.firebaseapp.com'
  }
};
