import {BaseApi, baseUrl} from './base-api.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DevlixApi extends BaseApi {
    getGroup (groupId) {
        return this.get(baseUrl + `/group/${groupId}`);
    }

    getUser (userId) {
        return this.get(baseUrl + `/user/${userId}`);
    }

    getList (groupId, userId) {
        return this.get(baseUrl + `/proscons/group/${groupId}/user/${userId}`);
    }

    updateList (groupId, userId, data) {
        return this.put(baseUrl + `/proscons/group/${groupId}/user/${userId}`, data);
    }
}