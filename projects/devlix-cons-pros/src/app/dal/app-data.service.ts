import {Injectable} from '@angular/core';
import {DevlixApi} from './endpoints';
import {BehaviorSubject} from 'rxjs';
import {ErrorHandler} from './error-handler';
import {Router} from '@angular/router';
import {LoadingService} from '../widgets/loading/loading.service';

@Injectable({
    providedIn: 'root',
})
export class AppDataService {
    _appData = {};
    appData = new BehaviorSubject(this._appData);

    constructor(private api: DevlixApi,
                private errorHandler: ErrorHandler,
                private router: Router,
                private loadingService: LoadingService) {
    }

    getGroup(groupId, nextRouteName) {
       return this.api.getGroup(groupId).subscribe( res => {
           this._setAppData(res, nextRouteName);
       });
    }

    getUser(userId, nextRouteName) {
       return this.api.getUser(userId).subscribe( res => {
           this._setAppData(res, nextRouteName);
       });
    }

    getList(groupId, userId) {
       return this.api.getList(groupId, userId);
    }

    updateList(groupId, userId, data) {
       return this.api.updateList(groupId, userId, data);
    }

    _setAppData(data, nextRouteName) {
        this.errorHandler.clear();

        if (data) {
            this._appData = {
                ...this._appData,
                ...data
            };
            this.appData.next(this._appData);

            this.router.navigate([nextRouteName]);
            this.loadingService.showLoader(false);
        }

    }
}
