import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

export interface IErrorData {
  code: string;
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class ErrorHandler {
  _errorData?: IErrorData = null;
  errorData = new BehaviorSubject(this._errorData);

  handle(error) {
    this._errorData = {
      code: error.code,
      message: error.message
    };
    this.errorData.next(this._errorData);
    throw error;
  }

  clear() {
    // debugger
    this._errorData = null;
    this.errorData.next(this._errorData);
  }
}
