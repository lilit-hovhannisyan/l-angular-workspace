import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {ErrorHandler} from './error-handler';
import {throwError} from 'rxjs/index';
import {tap, catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';


export const baseUrl =  environment.apiEndpoint.base;

@Injectable({
    providedIn: 'root'
})
export class BaseApi {
    constructor(public http: HttpClient,
                public errorHandler: ErrorHandler) {
    }

    put<T>(url: string, data: any, options?: any) {
        return this.http.put(url, data, options)
            .pipe(map((res: any) => {
                if (res.error) {
                    throwError(res.error);
                    this.errorHandler.handle(res.error);
                } else {
                    return res.data;
                }
        }));
    }

    get(url: string, queryOptions?: any) {
        return this.http.get(url, queryOptions).pipe(
            tap(response => {
            }),
            catchError((error: any) => {
                if (error) {
                    const _error = {
                        code: error.status,
                        message: error.message
                    };
                    this.errorHandler.handle(_error);
                    return throwError(error.message);
                }
            })


        );
    }

}
