import {Component, AfterViewInit, OnInit} from '@angular/core';
import {ErrorHandler} from '../../dal/error-handler';

@Component({
  selector: 'app-global-error',
  templateUrl: './global-error.component.html',
  styleUrls: ['./global-error.component.less']
})
export class GlobalErrorComponent implements OnInit {
  errorMessage = '';

  constructor(private errorHandler: ErrorHandler) { }

  ngOnInit() {
    this.errorHandler.errorData.subscribe( error => {
       this.errorMessage = error ? error.message : '';
    });
  }
}
