import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {LavashAtomsModule, LavashMoleculesModule, LavashOrganismsModule} from 'l-builder';
import { SelectFormComponent } from './select-form/select-form.component';
import { GlobalErrorComponent } from './global-error/global-error.component';
import {LoadingComponent} from './loading/loading.component';

@NgModule({
    declarations: [
        SelectFormComponent,
        GlobalErrorComponent,
        LoadingComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        LavashAtomsModule,
        LavashMoleculesModule,
        LavashOrganismsModule
    ],
    providers: [],
    exports: [
        SelectFormComponent,
        GlobalErrorComponent,
        LoadingComponent
    ]
})

export class WidgetsModule {
}
