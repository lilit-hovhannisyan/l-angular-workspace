import { Component, OnInit } from '@angular/core';
import { LoadingService } from './loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.less']
})
export class LoadingComponent implements OnInit {
  loadingClass = '';

  constructor(private loadingService: LoadingService) {}

  ngOnInit() {
    this.loadingService.getLoaderState().subscribe( boolean => {
      if (boolean) {
        this.loadingClass = boolean + '';
      } else {
        this.loadingClass = '';
      }
    });
  }

}
