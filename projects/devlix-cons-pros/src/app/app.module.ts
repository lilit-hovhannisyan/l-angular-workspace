import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient} from '@angular/common/http';

import { ListComponent,
         SelectGroupComponent,
         SelectUserComponent
} from './pages';
import { appRoutes } from './app.routes';

import {LavashAtomsModule, LavashMoleculesModule, LavashOrganismsModule} from 'l-builder';
import {WidgetsModule} from './widgets/widgets.module';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    SelectGroupComponent,
    SelectUserComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    LavashAtomsModule,
    LavashMoleculesModule,
    LavashOrganismsModule,
    WidgetsModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
